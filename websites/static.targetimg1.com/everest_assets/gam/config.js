var makePing = function() {};

$(document).ready(function() {
    var envURL = Target.globals.gamURL || Target.controller.header.cookie.read("gmod") || "http://static.targetimg1.com/everest_assets/gam/gam-secure.target.com",
		envURL_login = "http://static.targetimg1.com/everest_assets/gam/gam-secure.target.com",
        pingURL = (document.location.protocol === "https:")? "http://static.targetimg1.com/everest_assets/gam/gam-secure.target.com": "http://static.targetimg1.com/everest_assets/gam/gam.target.com",
		isGam = Target.controller.header.cookie.read("SSGM") !== null ? Target.controller.header.cookie.read("SSGM") === "1" ? true: false : false,
		gmod = Target.controller.header.cookie.read("gmod") !== null ? true: false;

    setTimeout( function() {
        try{           
            $('input[name=FF_FIND_STORE], input[name=FF_FIND_MORE_STORE]').val(function(){
					var store = $(this).val();
					return (store.indexOf("http") == -1)?'http://gam.target.com' + store : store;					
				});
            }catch(e){}   
    }, 1000); 
    try {
		envURL = envURL.match(/:\/\/(.[^/]+)/)[1];
    } catch(e){}
	
	if(envURL === "http://static.targetimg1.com/everest_assets/gam/www.target.com" || envURL === "http://static.targetimg1.com/everest_assets/gam/www-secure.target.com") {
		envURL_login = "http://static.targetimg1.com/everest_assets/gam/gam-secure.target.com";
        envURL = "http://static.targetimg1.com/everest_assets/gam/gam-secure.target.com";        
	}

    if(isGam || gmod) {

                var urlMod = Target.globals.gamURL === undefined ? document.location.protocol + '//'+ pingURL +'/gam-webapp/json/ping/guests/v3/' + (($.browser.msie && $.browser.version < 10) ?'proxy_':'') + 'ping':Target.globals.gamURL;
                var makePings = function() {

                    if( Target.controller.header.cookie.read("guestDisplayName") !== null) {
                        $.ajax({
                                    url: urlMod,
                                    curtain: false,
                                    contentType: "application/json",    
                                    dataType: "jsonp", 
                                    jsonpCallback: "pingCallback",               
                                    success: function() {
                                      console.log('extend');
                                    }
                            });
                    }
                };

                // call this method when session is active
                makePings();
                $(document).ajaxComplete(function(event, xhr, settings) {
                    if ( settings.url !== urlMod && xhr.status !== 404 && settings.url.match(/\/ping/g) === null) {              
                        makePings();
                    }       
                });
                 //make calls on extend session
                $("body").delegate("#extendSession","click", function(evt) {
                     makePings();
                });

        Target.globals.loginWrapperVert = {

            "guestUser": {
                "welcomeGuestUser": "Hi guest",
                "welcome": {
                    "id": "OpenLoginPopup",
                    "title": "sign in / account",
                    "url": "https://" + envURL + "/gam-webapp/login",
                    "text": "sign in / account"
                },
                "myAccount": {
                    "id": "headerMyAccount",
                    "title": "my account",
                    "url": "https://" + envURL + "/gam-webapp/login?langId=-1&storeId=10151&catalogId=10051&redirectToURL=UserHome&lnk=gnav_myaccount",
                    "text": "my account"
                },
                "loginStatus": {
                    "id": "headerGuest",
                    "title": "new guest",
                    "url": "https://" + envURL + "/create-account?langId=-1&storeId=10151&catalogId=10051&lnk=gnav_newguest",
                    "text": "new guest?"
                }
            },
            "LoggedUser": {
                "welcomeLoggedUser": "Hi ",
                "welcome": {
                    "id": "headerWelcome",
                    "title": "",
                    "url": "",
                    "text": "Hi "
                },
                "loginStatus": {
                    "id": "headerMyAccount",
                    "title": "my account",
                    "url": "https://" + envURL + "/dashboard?langId=-1&storeId=10151&catalogId=10051&lnk=gnav_myaccount",
                    "text": "my account"
                },
                "myAccount": {
                    "id": "headerGuest",
                    "title": "sign out",
                    "url": "https://" + envURL + "/gam-webapp/logout?langId=-1&storeId=10151&catalogId=10051&cmdName=LogOff&URL=EverestLoginView",
                    "text": "sign out"
                },
                "manageAccount": {
                    "id": "manageAccount",
                    "title": "manage my account",
                    "url": "https://" + envURL + "/gam-webapp/dashboard?langId=-1&storeId=10151&catalogId=10051&lnk=gnav_myaccount",
                    "text": "manage my account"
                },
                "returnItem": {
                    "id": "returnItem",
                    "title": "return an item",
                    "url": "https://" + envURL + "/gam-webapp/returns?returnsLanding&lnk=gnav_returnitem",
                    "text": "return an item"
                },
                "trackOrder": {
                    "id": "trackOrder",
                    "title": "track an order",
                    "url": "https://" + envURL + "/gam-webapp/order-history?lnk=account_orderhistory&langId=-1&storeId=10151&catalogId=10051&lnk=gnav_trackorder",
                    "text": "track an order"
                },
                "signOut": {
                    "id": "signOut",
                    "title": "sign out",
                    "url": "https://" + envURL + "/gam-webapp/logout?langId=-1&storeId=10151&catalogId=10051&cmdName=LogOff&URL=EverestLoginView",
                    "text": "sign out"
                }
            }

        }


        var userName = null,
            headerCookie = Target.controller.header;
        if (headerCookie.cookie.read('guestDisplayName') != null) {
            var _cookieVal = headerCookie.cookie.read('guestDisplayName');
            var _match = _cookieVal.match(/GDN\=\w+\|?/);
            if (_match != null) userName = escape(_match[0].replace('GDN=', '').replace('|', ''));
            else userName = escape(_cookieVal);
            if (userName.indexOf("%20") != -1) {
                userName = userName.replace(/%20/g, " ");
            }
            if (userName.indexOf("%27") != -1) {
                userName = userName.replace(/%27/g, "'");
            }

            var manageAccountUrl = '',
                manageAccountId = '',
                manageAccountTitle = '',
                manageAccountText = 'manage my account',
                trackOrderUrl = '',
                trackOrderId = '',
                trackOrderTitle = '',
                trackOrderText = 'track an order',
                returnItemUrl = '',
                returnItemId = '',
                returnItemTitle = '',
                returnItemText = 'return an item',
                signOutUrl = '',
                signOutId = '',
                signOutTitle = '',
                signOutText = 'sign out';
            if (typeof Target.globals["loginWrapperVert"] !== 'undefined') {
                var LogingWrapperRes = Target.globals["loginWrapperVert"];
                if (typeof LogingWrapperRes.LoggedUser !== 'undefined') {
                    var LoggedUserRes = LogingWrapperRes.LoggedUser;

                    if (LoggedUserRes.manageAccount !== 'undefined') {
                        var manageAccountRes = LoggedUserRes.manageAccount;

                        if (typeof manageAccountRes.id !== 'undefined') {
                            manageAccountId = manageAccountRes.id;
                        }
                        if (typeof manageAccountRes.url !== 'undefined') {
                            manageAccountUrl = manageAccountRes.url;
                        }
                        if (typeof manageAccountRes.title !== 'undefined') {
                            manageAccountTitle = manageAccountRes.title;
                        }
                        if (typeof manageAccountRes.text !== 'undefined') {
                            manageAccountText = manageAccountRes.text;
                        }

                        var trackOrderRes = LoggedUserRes.trackOrder;


                        if (typeof trackOrderRes.id !== 'undefined') {
                            trackOrderId = trackOrderRes.id;
                        }
                        if (typeof trackOrderRes.url !== 'undefined') {
                            trackOrderUrl = trackOrderRes.url;
                        }
                        if (typeof trackOrderRes.title !== 'undefined') {
                            trackOrderTitle = trackOrderRes.title;
                        }
                        if (typeof trackOrderRes.text !== 'undefined') {
                            trackOrderText = trackOrderRes.text;
                        }

                        var returnItemRes = LoggedUserRes.returnItem;

                        if (typeof returnItemRes.id !== 'undefined') {
                            returnItemId = returnItemRes.id;
                        }
                        if (typeof returnItemRes.url !== 'undefined') {
                            returnItemUrl = returnItemRes.url;
                        }
                        if (typeof returnItemRes.title !== 'undefined') {
                            returnItemTitle = returnItemRes.title;
                        }
                        if (typeof returnItemRes.text !== 'undefined') {
                            returnItemText = returnItemRes.text;
                        }

                        var signOutRes = LoggedUserRes.signOut;



                        if (typeof signOutRes.id !== 'undefined') {
                            signOutId = signOutRes.id;
                        }
                        if (typeof signOutRes.url !== 'undefined') {
                            signOutUrl = signOutRes.url;
                        }
                        if (typeof signOutRes.title !== 'undefined') {
                            signOutTitle = signOutRes.title;
                        }
                        if (typeof signOutRes.text !== 'undefined') {
                            signOutText = signOutRes.text;
                        }
                    }

                }
            }
            var manageAccountLink = "<a id ='" + manageAccountId + "' href='" + manageAccountUrl + "' title='" + manageAccountTitle + "'>" + manageAccountText + "</a>",
                trackOrderLink = "<a id ='" + trackOrderId + "' href='" + trackOrderUrl + "' title='" + trackOrderTitle + "'>" + trackOrderText + "</a>",
                returnItemLink = "<a id ='" + returnItemId + "' href='" + returnItemUrl + "' title='" + returnItemTitle + "'>" + returnItemText + "</a>",
                signOutLink = "<a id ='" + signOutId + "' href='" + signOutUrl + "' title='" + signOutTitle + "'>" + signOutText + "</a>";

            $("#UserMenu li > #OpenLoginPopup").remove();
            $("#UserMenu li").html("<a href='javascript:void(0);' id='OpenLogedinPopup' rel='nofollow' aria-hidden='false'>" + userName + "</a>");
            var signingFlyout = "<div id='Logedin-container' style='display: none; visibility: hidden;'>" +
                "<div class='closeLogedin'><a href='javascript:void(0);' id='CloseLogedinPopup' aria-hidden='false'></a> " +
                "<span class='up-arrow'></span></div><div class='box'><p class='section'> " +
                manageAccountLink + "</p><div class='section'><p>order history</p> " +
                "<p>" + trackOrderLink + "</p>" +
                "<p>" + returnItemLink + "</p></div> " +
                "<p>" + signOutLink + "</p>" +
                "<p> <a class='signinClose' href='#'>&nbsp; collapse</a> </p></div></div>";

            $(signingFlyout).insertAfter("#OpenLogedinPopup");
        }
        if (userName) {


            var spanScreenReader = "<span class='screen-reader-only'>&nbsp; expand</span><span class='down-arrow'></span>",
                spanScreenReaderClose = "<span class='screen-reader-only'>&nbsp; collapse</span>";

            $(document).trigger('session-start.framework'); 
              
             $(".signOut").attr({href: signOutUrl }); // replaces the signout url for checkout

            if (userName.length > 10) {
                $("#OpenLogedinPopup").html("Hi " + $.trim(userName.substring(0, 10)) + "..." + spanScreenReader);
                $("#CloseLogedinPopup").html("Hi " + $.trim(userName.substring(0, 10)) + "..." + spanScreenReaderClose);
            } else {
                $("#OpenLogedinPopup").html("Hi " + userName + spanScreenReader);
                $("#CloseLogedinPopup").html("Hi " + userName + spanScreenReaderClose);
            }
            $('#OpenLogedinPopup').css('textTransform', 'capitalize');
            $('#CloseLogedinPopup').css('textTransform', 'capitalize');
        } else {
            $("#OpenLoginPopup, #NewOpenLoginPopup").attr({id: 'openLogin',href: 'https://' + envURL_login + '/gam-webapp/login' });
            
        }

    }
	
	try{
		var str = Target.controller.header.cookie.read("fiatsCookie").split("|"),
			str_1 = str[0].split("_"),
			str_2 = str[1].split("_"),
			str_3 = str_2[1].replace(/ /g, "-"),
			urlStr = "";

		urlStr = "http://gam.target.com/store-locator/sl/"+str_3+"/"+str_1[1];

		$("#storeNameDefault > a").attr("href", urlStr).html("").html(str_2[1]);
		$("#storeNameDefault").attr("id", "storeNameDefault-gam");

		$("#storeNameDefault-gam").css("padding-bottom", "8px");
		$("#storeNameDefault-gam > a").css({
		   'color' : '#494949',
		   'font-size' : '20px',
		   'background' : '#fff',
		   'line-height' : '20px'
		});
	}catch(e){}
});