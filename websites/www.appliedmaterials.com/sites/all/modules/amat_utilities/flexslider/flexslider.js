(function ($) {
  $(function($) {
    $('.flexslider').flexslider({
        controlNav: 'thumbnails',
        pauseOnHover: true,
        animationLoop: false,        //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
        useCSS: true,                   //{NEW} Boolean: Slider will use CSS3 transitions if available
        touch: true                      //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
    });
    
    //Pause and Play HTML 5 Video functionality
    $('#video').click(function(){
        var paused = $(this).get(0).paused;
        if(paused){
          $(this).get(0).play();
        }
        else{
          $(this).get(0).pause();
        }
    });
    //Pause Video if slide nav is clicked
    $('.flex-direction-nav a').click(function(){
      var video_exists = $('.slides li').children('#video').length;
    
      if(video_exists){
        $('#video').get(0).pause();
      }
    });
    $('.flex-control-nav img').click(function(){
      var video_exists = $('.slides li').children('#video').length;
    
      if(video_exists){
        $('#video').get(0).pause();
      }
    });
  });
})(jQuery);
