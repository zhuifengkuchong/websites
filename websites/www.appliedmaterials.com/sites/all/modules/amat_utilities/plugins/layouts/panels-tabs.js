(function ($) {

  $(function() {

    var panel_tab_titles = $(".panel-tabs .pane-title");

    if (panel_tab_titles.length > 0) {
      var check_ipe = $('body.panels-ipe');
      var ipe = false;
      if (check_ipe.length > 0) {
        ipe = true;
      }
      var page_manager = $('body.page-node-panelizer-page-manager');
      if (page_manager.length > 0){
        return;
      }
      var tab_headers = document.createElement("ul");
      $(tab_headers).addClass("nav nav-tabs");
      $(panel_tab_titles).each(function(i){
        var title = $(this).text();
        var prep = title.toLowerCase().replace(/ /g,"-").replace(/[\.\',-\/#!$\?%\^&\*;:{}=\-_`~()]/g,"");
        $(this).parent().attr("id", prep);
        var state = "";
        if (i == 0 || ipe) {
          state=" class='active'";
          $(this).parent().addClass('active');
        }
        $(tab_headers).append('<li' + state + '><a data-toggle="tab" href="#' + prep + '">' + title + '</a></li>');
        if (!ipe) {
          $(this).remove();
        }
      });
      if (ipe){
        $(tab_headers).append('<li><a><em>(Inactivate During Edit)</em></a></li>');
      }
      $(".panel-tabs").prepend(tab_headers);
      $(tab_headers).tab();
    }

  });

})(jQuery);
