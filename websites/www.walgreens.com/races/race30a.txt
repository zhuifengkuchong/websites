<script id = "race30a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race30={};
	myVars.races.race30.varName="#document[0x7feb03d36400].keydown";
	myVars.races.race30.varType="@varType@";
	myVars.races.race30.repairType = "@RepairType";
	myVars.races.race30.event1={};
	myVars.races.race30.event2={};
	myVars.races.race30.event1.id = "Lu_Id_script_24";
	myVars.races.race30.event1.type = "Lu_Id_script_24__parsed";
	myVars.races.race30.event1.loc = "Lu_Id_script_24_LOC";
	myVars.races.race30.event1.isRead = "False";
	myVars.races.race30.event1.eventType = "@event1EventType@";
	myVars.races.race30.event2.id = "carousel-example-generic";
	myVars.races.race30.event2.type = "onkeydown";
	myVars.races.race30.event2.loc = "carousel-example-generic_LOC";
	myVars.races.race30.event2.isRead = "True";
	myVars.races.race30.event2.eventType = "@event2EventType@";
	myVars.races.race30.event1.executed= false;// true to disable, false to enable
	myVars.races.race30.event2.executed= false;// true to disable, false to enable
</script>

