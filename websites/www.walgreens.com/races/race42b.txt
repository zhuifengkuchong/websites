<script id = "race42b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race42={};
	myVars.races.race42.varName="Object[7423].uuid";
	myVars.races.race42.varType="@varType@";
	myVars.races.race42.repairType = "@RepairType";
	myVars.races.race42.event1={};
	myVars.races.race42.event2={};
	myVars.races.race42.event1.id = "ntt-placeholder";
	myVars.races.race42.event1.type = "onkeypress";
	myVars.races.race42.event1.loc = "ntt-placeholder_LOC";
	myVars.races.race42.event1.isRead = "True";
	myVars.races.race42.event1.eventType = "@event1EventType@";
	myVars.races.race42.event2.id = "Lu_Id_input_1";
	myVars.races.race42.event2.type = "onkeypress";
	myVars.races.race42.event2.loc = "Lu_Id_input_1_LOC";
	myVars.races.race42.event2.isRead = "False";
	myVars.races.race42.event2.eventType = "@event2EventType@";
	myVars.races.race42.event1.executed= false;// true to disable, false to enable
	myVars.races.race42.event2.executed= false;// true to disable, false to enable
</script>

