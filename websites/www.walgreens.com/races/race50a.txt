<script id = "race50a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race50={};
	myVars.races.race50.varName="Object[7423].uuid";
	myVars.races.race50.varType="@varType@";
	myVars.races.race50.repairType = "@RepairType";
	myVars.races.race50.event1={};
	myVars.races.race50.event2={};
	myVars.races.race50.event1.id = "2";
	myVars.races.race50.event1.type = "onchange";
	myVars.races.race50.event1.loc = "2_LOC";
	myVars.races.race50.event1.isRead = "False";
	myVars.races.race50.event1.eventType = "@event1EventType@";
	myVars.races.race50.event2.id = "Lu_Id_input_6";
	myVars.races.race50.event2.type = "onchange";
	myVars.races.race50.event2.loc = "Lu_Id_input_6_LOC";
	myVars.races.race50.event2.isRead = "True";
	myVars.races.race50.event2.eventType = "@event2EventType@";
	myVars.races.race50.event1.executed= false;// true to disable, false to enable
	myVars.races.race50.event2.executed= false;// true to disable, false to enable
</script>

