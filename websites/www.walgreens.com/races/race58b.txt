<script id = "race58b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race58={};
	myVars.races.race58.varName="Object[7423].uuid";
	myVars.races.race58.varType="@varType@";
	myVars.races.race58.repairType = "@RepairType";
	myVars.races.race58.event1={};
	myVars.races.race58.event2={};
	myVars.races.race58.event1.id = "Lu_Id_input_1";
	myVars.races.race58.event1.type = "onfocus";
	myVars.races.race58.event1.loc = "Lu_Id_input_1_LOC";
	myVars.races.race58.event1.isRead = "True";
	myVars.races.race58.event1.eventType = "@event1EventType@";
	myVars.races.race58.event2.id = "pharmacyUrlContext";
	myVars.races.race58.event2.type = "onchange";
	myVars.races.race58.event2.loc = "pharmacyUrlContext_LOC";
	myVars.races.race58.event2.isRead = "False";
	myVars.races.race58.event2.eventType = "@event2EventType@";
	myVars.races.race58.event1.executed= false;// true to disable, false to enable
	myVars.races.race58.event2.executed= false;// true to disable, false to enable
</script>

