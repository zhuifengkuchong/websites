<script id = "race48b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race48={};
	myVars.races.race48.varName="Object[7423].uuid";
	myVars.races.race48.varType="@varType@";
	myVars.races.race48.repairType = "@RepairType";
	myVars.races.race48.event1={};
	myVars.races.race48.event2={};
	myVars.races.race48.event1.id = "3";
	myVars.races.race48.event1.type = "onchange";
	myVars.races.race48.event1.loc = "3_LOC";
	myVars.races.race48.event1.isRead = "True";
	myVars.races.race48.event1.eventType = "@event1EventType@";
	myVars.races.race48.event2.id = "header-search";
	myVars.races.race48.event2.type = "onchange";
	myVars.races.race48.event2.loc = "header-search_LOC";
	myVars.races.race48.event2.isRead = "False";
	myVars.races.race48.event2.eventType = "@event2EventType@";
	myVars.races.race48.event1.executed= false;// true to disable, false to enable
	myVars.races.race48.event2.executed= false;// true to disable, false to enable
</script>

