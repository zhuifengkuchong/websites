<script id = "race57a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race57={};
	myVars.races.race57.varName="Object[7423].uuid";
	myVars.races.race57.varType="@varType@";
	myVars.races.race57.repairType = "@RepairType";
	myVars.races.race57.event1={};
	myVars.races.race57.event2={};
	myVars.races.race57.event1.id = "ntt-placeholder";
	myVars.races.race57.event1.type = "onchange";
	myVars.races.race57.event1.loc = "ntt-placeholder_LOC";
	myVars.races.race57.event1.isRead = "False";
	myVars.races.race57.event1.eventType = "@event1EventType@";
	myVars.races.race57.event2.id = "pharmacyUrlContext";
	myVars.races.race57.event2.type = "onchange";
	myVars.races.race57.event2.loc = "pharmacyUrlContext_LOC";
	myVars.races.race57.event2.isRead = "True";
	myVars.races.race57.event2.eventType = "@event2EventType@";
	myVars.races.race57.event1.executed= false;// true to disable, false to enable
	myVars.races.race57.event2.executed= false;// true to disable, false to enable
</script>

