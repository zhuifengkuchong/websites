<script id = "race34b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race34={};
	myVars.races.race34.varName="Object[7423].uuid";
	myVars.races.race34.varType="@varType@";
	myVars.races.race34.repairType = "@RepairType";
	myVars.races.race34.event1={};
	myVars.races.race34.event2={};
	myVars.races.race34.event1.id = "3";
	myVars.races.race34.event1.type = "onkeypress";
	myVars.races.race34.event1.loc = "3_LOC";
	myVars.races.race34.event1.isRead = "True";
	myVars.races.race34.event1.eventType = "@event1EventType@";
	myVars.races.race34.event2.id = "header-search";
	myVars.races.race34.event2.type = "onkeypress";
	myVars.races.race34.event2.loc = "header-search_LOC";
	myVars.races.race34.event2.isRead = "False";
	myVars.races.race34.event2.eventType = "@event2EventType@";
	myVars.races.race34.event1.executed= false;// true to disable, false to enable
	myVars.races.race34.event2.executed= false;// true to disable, false to enable
</script>

