<script id = "race6b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race6={};
	myVars.races.race6.varName="3__onkeypress";
	myVars.races.race6.varType="@varType@";
	myVars.races.race6.repairType = "@RepairType";
	myVars.races.race6.event1={};
	myVars.races.race6.event2={};
	myVars.races.race6.event1.id = "3";
	myVars.races.race6.event1.type = "onkeypress";
	myVars.races.race6.event1.loc = "3_LOC";
	myVars.races.race6.event1.isRead = "True";
	myVars.races.race6.event1.eventType = "@event1EventType@";
	myVars.races.race6.event2.id = "Lu_DOM";
	myVars.races.race6.event2.type = "onDOMContentLoaded";
	myVars.races.race6.event2.loc = "Lu_DOM_LOC";
	myVars.races.race6.event2.isRead = "False";
	myVars.races.race6.event2.eventType = "@event2EventType@";
	myVars.races.race6.event1.executed= false;// true to disable, false to enable
	myVars.races.race6.event2.executed= false;// true to disable, false to enable
</script>

