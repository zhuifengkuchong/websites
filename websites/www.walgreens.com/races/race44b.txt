<script id = "race44b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race44={};
	myVars.races.race44.varName="Object[7423].uuid";
	myVars.races.race44.varType="@varType@";
	myVars.races.race44.repairType = "@RepairType";
	myVars.races.race44.event1={};
	myVars.races.race44.event2={};
	myVars.races.race44.event1.id = "wag-ssn-tout-overlay-logout";
	myVars.races.race44.event1.type = "onchange";
	myVars.races.race44.event1.loc = "wag-ssn-tout-overlay-logout_LOC";
	myVars.races.race44.event1.isRead = "True";
	myVars.races.race44.event1.eventType = "@event1EventType@";
	myVars.races.race44.event2.id = "pharmacyUrlContext";
	myVars.races.race44.event2.type = "onkeypress";
	myVars.races.race44.event2.loc = "pharmacyUrlContext_LOC";
	myVars.races.race44.event2.isRead = "False";
	myVars.races.race44.event2.eventType = "@event2EventType@";
	myVars.races.race44.event1.executed= false;// true to disable, false to enable
	myVars.races.race44.event2.executed= false;// true to disable, false to enable
</script>

