/** (c) Walgreen Co. All rights reserved.**/
function shareSession() {
	var hostArr = hostNameRegexes;
	var sessionVal = Get_Cookie('JSESSIONID');
	for ( var j = 0; j < hostArr.length; j++) {
		if (window.location.hostname.indexOf(hostArr[j]) == -1) {
		if(!checkAndRedirectForSafari(hostArr[j])) return;
		var subDomainLength = window.location.host.split('.').length;
		var url = "";
		for(var i=0 ; i < subDomainLength-2 ;i++){
		url += window.location.host.split('.')[i]+".";
		}
		var sUrl = location.protocol + '//' +url +hostArr[j] + (location.port ? ':'+ location.port: '')
			+ '/siteInit?jid=' + sessionVal;
		var mHtml = '<img src="' + sUrl 
		+ '" id="redirectIframeSite' + j
		+ '" name="redirectIframeSite' + j
		+ '" style="width:0px; height:0px; border: 0px"/>';
		var sessShareIFrame = $(mHtml, { css: { 'display': 'none' }});
		$('body').append(sessShareIFrame);
		}
	}
	var serverName = window.location.hostname;
	var dot = ".";
	if(serverName!=null){
		var idx = serverName.lastIndexOf(dot);
		idx = serverName.lastIndexOf(dot, idx-1);
		serverName = serverName.substring(idx + 1);
	}
	Set_Cookie("SESSION_SHRD", "true", -1, "/", serverName, false, false);
}

function checkAndRedirectForSafari(hostName) {
	if (navigator.userAgent.indexOf("Safari") > -1) {
		var cVal = Get_Cookie('domainshared');
		if ('yes' != cVal) {
			Set_Cookie('domainshared', 'yes', 365, '/', null, false, false);// 1 year
			var sUrl = location.protocol + '//' +window.location.host.split('.')[0] +"." + hostName 
						+ (location.port ? ':'+ location.port: '') + 	'/common/site/sfrcookie.html';
			window.location.replace(sUrl);
			return false;
		}
	}
	return true;
}

function Get_Cookie(check_name) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split(';');
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f

	for (var i = 0; i < a_all_cookies.length; i++) {
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split('=');

		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if (cookie_name == check_name) {
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no =
			// sign, that is):
			if (a_temp_cookie.length > 1) {
				cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
			}
			// note that in cases where cookie is initialized but no value, null
			// is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if (!b_cookie_found) {
		return null;
	}
}
function Set_Cookie(name, value, expires, path, domain, secure, httpOnly) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime(today.getTime());

	if (expires && (expires != -1)) {
		expires = expires * 1000 * 60;
	}
	var expires_date = new Date(today.getTime() + expires);

	document.cookie = name + "=" + value
	+ ((expires != -1) ? ";expires=" + expires_date.toGMTString() : "")
	+ ((path) ? ";path=" + path : "")
	+ ((domain) ? ";domain=" + domain : "")
	+ ((secure) ? ";secure" : "") + ((httpOnly) ? ";HttpOnly" : "");
}