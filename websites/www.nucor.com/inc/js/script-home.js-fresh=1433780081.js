$(function()
{
	if ( typeof(m_aszBanners) != "undefined" ) loadBanner(1);

	if ( !isMobile ){
		$('.banner nav').fadeTo(1000, 0.6);
		
		$('.banner').mouseenter(function(){
			$('.banner nav').fadeTo(250, 1);
		});
		
		$('.banner').mouseleave(function(){
			$('.banner nav').fadeTo(1000, 0.6);
		});
	}

	if ( $('http://www.nucor.com/inc/js/div.bug').length ){
		if ( !isMobile ){
			$('http://www.nucor.com/inc/js/div.bug').delay(1000).animate({'bottom': '-30px'}, 500);
			
			$('http://www.nucor.com/inc/js/div.bug').mouseenter(function(){
				$('http://www.nucor.com/inc/js/div.bug').stop();
				$('http://www.nucor.com/inc/js/div.bug').animate({'bottom': '0'}, {duration: 250, queue: false});
			});
			
			$('http://www.nucor.com/inc/js/div.bug').mouseleave(function(){
				$('http://www.nucor.com/inc/js/div.bug').stop();
				$('http://www.nucor.com/inc/js/div.bug').animate({'bottom': '-30px'}, {duration: 500, queue: false});
			});
		}

		if ( $('.bug h2 a').length ){
			$('http://www.nucor.com/inc/js/div.bug').css({'cursor': 'pointer'});

			$('http://www.nucor.com/inc/js/div.bug').click(function(){
				if ( $('.bug h2 a').attr('target') == "_blank" ) window.open($('.bug h2 a').attr('href'));
				else window.location = $('.bug h2 a').attr('href');
			});
		}
		
		$('.bug').after('<div class="bug-shadow"></div>');
		//jQuery.each(jQuery.browser,function(i, val){$("<div>" + i + " : <span>" + val + "</span>").appendTo( document.body );});
	}
	
	$('.banner .lt').click(function()
	{
		var nBannerNext = m_nCurrentBanner == 1 ? m_nTotalBanners : m_nCurrentBanner - 1;
		slideBanner(nBannerNext)
		updateBannerNav(nBannerNext);
	});
		
	$('.banner .rt').click(function()
	{
		var nBannerNext = m_nCurrentBanner == m_nTotalBanners ? 1 : m_nCurrentBanner + 1;
		slideBanner(nBannerNext)
		updateBannerNav(nBannerNext);
	});
	
	

	isModernBrowser = $('html').hasClass('csstransitions');
	isMobile = $('html').hasClass('touch');
	isTablet = ( isMobile && ( window.screen.width > 760 ) )
	isPhone = isMobile && !isTablet;
	
	if ( navigator.userAgent.match(/iPad;.*CPU.*OS 7_\d/i) )
	{
		$('html').addClass('ipad ios7');
	}

	$('.overlay-acuity header button').click(closeHandler);
	
	$('.overlay-acuity header nav li a').click(function(e)
	{
		e.preventDefault();
		var nNav = $(this).parent().index() + ( $(this).parent().index() > 0 ? 2 : 1 );
		var nSectionHeight = $('.overlay-acuity section').outerHeight();
		var nScrollTop = nSectionHeight * nNav;
		var nDuration = isMobile ? 0 : .5;
		TweenMax.to($('div.content-overlay'), nDuration, {scrollTo:{y:nScrollTop}, ease:Power4.easeOut});
		
		//if ( $(window).width() <= 900 ) { $('.overlay-acuity header, .overlay-acuity div.content-overlay').removeClass('navon'); }
	});
	
	var nLatency = isMobile ? 50 : 250;
	$('.overlay-acuity div.content-overlay').scrollsnap({snaps: 'section', proximity: 50, onSnap: updateSection, easing: 'easeOutQuad', latency: nLatency});
	
	var resizeCheck;

	$(window).resize(function()
	{
		clearTimeout(resizeCheck);
  		resizeCheck = setTimeout(resizeEnd, 100);
	});
	
	//** front **//
	$('.front button').click(function()
	{
		$('header nav li:first-child a').trigger('click');
		$(this).blur();
	});
	
	
	//** intro **//


	
	//** FlagpoleFacts **//


	
	//** SteelFacts **//
	$('.overlay-acuity .steel-facts .scrollable').customScrollbar({ skin: 'default-skin', hScroll: false, updateOnWindowResize: true, preventDefaultScroll: true });


	//** CommunityImpact **//





	if ( window.location.hash ) 
	{
		var sHash = window.location.hash.toLowerCase().substring(1);

		if ( sHash == sAutoPlay.toLowerCase() ) $('div.deeresite').trigger('click');
	}
});

var sAutoPlay = "flagpole";

var isModernBrowser, isMobile, isPhone, isTablet;
var nSection = 0;
var nScrollTopPrev;
var nScrollLeftPrev;

function showAcuityFlagpoleOverlay()
{
	nScrollTopPrev = $(window).scrollTop();
	nScrollLeftPrev = $(window).scrollLeft();
	$('body').css({'overflow': 'hidden'});
		
	$('meta[name=viewport]').attr('content', 'width=device-width, initial-scale=1, user-scalable=no');
	if ( ( $(window).scrollTop() > 0 ) || ( $(window).scrollLeft() > 0 ) ) 	{ TweenMax.to(window, .5, {scrollTo:{y:0, x:0}, ease:Power4.easeOut, onComplete:showFlagpole}); }
	else																	{ TweenMax.to(window, 0, {scrollTo:{y:0, x:0}, ease:Power4.easeOut, onComplete:showFlagpole}); }
}

$(window).scroll(function(e)
{
	//console.log(e.target);
});

var keys = [37, 38, 39, 40];

function preventDefault(e)
{
	e = e || window.event;

	if ( e.preventDefault ) e.preventDefault();

	e.returnValue = false;  
}

function keydown(e)
{
	for (var i = keys.length; i--;)
	{
        if (e.keyCode === keys[i]) {
            preventDefault(e);
            return;
        }
    }
}

function wheel(e)
{
	preventDefault(e);
}

function disableScroll()
{
	if ( window.addEventListener )
	{
		window.addEventListener('DOMMouseScroll', wheel, false);
	}

	window.onmousewheel = document.onmousewheel = wheel;
	document.onkeydown = keydown;
}

function allowScroll()
{
	if ( window.removeEventListener )
	{
		window.removeEventListener('DOMMouseScroll', wheel, false);
	}
    
    window.onmousewheel = document.onmousewheel = document.onkeydown = null;  
    
	$('div.overlay-acuity').removeClass('on');
	$('body').css({'overflow': 'visible'});
	$('meta[name=viewport]').attr('content', 'width=device-width, initial-scale=1');
	resetIntro();
	resetFlagpoleFacts();
	resetSteelFacts();
	resetCommunityImpact();
	$('.overlay-acuity div.content-overlay').scrollTop(0);
	
	if ( ( nScrollTopPrev > 0 ) || ( nScrollLeftPrev > 0 ) ) { TweenMax.to(window, 1, {scrollTo:{y:nScrollTopPrev, x:nScrollLeftPrev}, ease:Power4.easeOut}); }
}

function resizeEnd()
{
	if ( !$('div.overlay-acuity').hasClass('on') ) return;
	
	var nScrollTop = $('div.content-overlay').scrollTop() + 1;
	TweenMax.to($('div.content-overlay'), 0, {scrollTo:{y:nScrollTop}});
}

function updateSection(eSection)
{
	$('.content-overlay section').removeClass('active');
	eSection.addClass('active');
	$('nav.scroll').removeClass('hide');

	if ( $('section.front').hasClass('active') )
	{
		nSection = 0;

		resetIntro();
		resetFlagpoleFacts();
		resetSteelFacts();
		resetCommunityImpact();
	}
	else if ( $('section.intro').hasClass('active') || $('section.intro2').hasClass('active') )
	{
		nSection = 1;

		resetFront();
		resetFlagpoleFacts();
		resetSteelFacts();
		resetCommunityImpact();
	}
	else if ( $('section.flagpole-facts').hasClass('active') )
	{		
		nSection = 2;
		
		resetFront();
		resetIntro();
		resetSteelFacts();
		resetCommunityImpact();
	}
	else if ( $('section.steel-facts').hasClass('active') )
	{
		nSection = 3;
		
		resetFront();
		resetIntro();
		resetFlagpoleFacts();
		resetCommunityImpact();
		//$('.overlay-acuity .steel-facts .scrollable').customScrollbar({ skin: 'default-skin', hScroll: false, updateOnWindowResize: true, preventDefaultScroll: true });
	}
	else if ( $('section.community-impact').hasClass('active') )
	{
		nSection = 4;

		resetFront();
		resetIntro();
		resetSteelFacts();
		resetFlagpoleFacts();
	}
	else
	{
		console.log("ERROR: can't determine active section");
	}
	
	$('header nav li').removeClass('active');
	$('header nav li:nth-child(' + nSection + ')').addClass('active');
}

function showFlagpole()
{
	if ( isModernBrowser )	{ var e = $('div.overlay-acuity'); e.addClass('on'); var transitionEvent = whichTransitionEvent(); transitionEvent && e.one(transitionEvent, overlayOn); }
	else 					{ $('div.overlay-acuity').addClass('on'); TweenMax.to($('div.overlay-acuity'), 1, {css:{'top': '0', 'bottom': '0', 'right': '0', 'left': '0'}, onComplete: showFront}); }
	
	$('div.overlay-acuity').blur();
}

function overlayOn()
{
	$(window).trigger('resize');
	showFront();
}

function showFront()
{

}

function resetFront()
{

}

function resetIntro()
{

}

function resetFlagpoleFacts()
{

}

function resetSteelFacts()
{

}

function resetCommunityImpact()
{
	
}


var closeHandler = function()
{
	TweenMax.to($('.overlay-acuity div.content-overlay'), .5, {scrollTo:{y:0}});
	$('.acuity-overlay header nav li, .acuity-overlay section').removeClass('active');
	
	if ( isModernBrowser )	{ $('div.overlay-acuity').removeClass('on'); var e = $('div.overlay-acuity'); var transitionEvent = whichTransitionEvent(); transitionEvent && e.one(transitionEvent, allowScroll); }
	else					{ TweenMax.to($('div.overlay-acuity'), 1, {css:{top: '-100%', 'bottom': '100%'}, onComplete: allowScroll}); }		
}

function whichTransitionEvent()
{
    var t;
    var el = document.createElement('fakeelement');
    var transitions = { 'transition':'transitionend', 'OTransition':'oTransitionEnd', 'MozTransition':'transitionend', 'WebkitTransition':'webkitTransitionEnd' }

    for ( t in transitions )
    {
        if ( el.style[t] !== undefined )
        {
            return transitions[t];
        }
    }
}






var m_szTitle = document.title;

var m_nBanner = 1;
var m_nCurrentBanner = 1;

var m_abBannerReady = new Array();

if(m_nTotalBanners != undefined)
{
	for ( d = 1; d <= m_nTotalBanners; d++ )
	{
		m_abBannerReady[d] = false;
	}
}
else
{
	var m_nTotalBanners = 1;
}

function loadBanner(n)
{
	$('.banners li:nth-child(' + n + ')').load('/inc/banners/' + m_aszBanners[n - 1] + '&ver=' + Math.random() + ' .banner' + n + '', function(response, status, xhr){
		var nBannerLoaded = n;
		
		if ( status == "error" ){
			var msg = "Sorry but there was an error loading banner #" + nBannerLoaded + ": ";
			$('.banner').append('<p class="error">' + msg + xhr.status + ' ' + xhr.statusText + '</p>');
		} else {
			var szFuncName = ((m_aszBanners[n - 1].substr(3)).split(".php"))[0]; //Takes a name like 01_WorkToBeDone.php and strips out the '01_' and the '.php'

			window["playBanner" + n] = new Function();
			window["playBanner" + n] = window["play" + szFuncName];

			window["stopBanner" + n] = new Function();
			window["stopBanner" + n] = window["stop" + szFuncName];

			window["resetBanner" + n] = new Function();
			window["resetBanner" + n] = window["reset" + szFuncName];

			if ( nBannerLoaded == 1 ) $('.banner1').queryLoader2({ barColor: "#000", backgroundColor: "#333", percentage: false, barHeight: 1, completeAnimation: "grow", minimumTime: 100, onComplete: playBanner1 });
			if( nBannerLoaded > 0 ) setBannerReady( nBannerLoaded );

			$('.banners li:nth-child(' + nBannerLoaded + ')').delay(1000).fadeIn('slow');

			m_nBanner = nBannerLoaded + 1;

			console.log("loaded " + nBannerLoaded + " of " + m_nTotalBanners);
			if ( m_nBanner <= m_nTotalBanners ){
				loadBanner(m_nBanner);
			} else {
				if ( isMobile ){
					$('ul.banners').wrap('<div id="swipe"></div>');
					var oSwipe = new Swipe(document.getElementById('swipe'), { callback: function(e, nIndex, oElem){ updateBannerNav(nIndex + 1); }});
				} else {
					$('ul.banners li').each(function(){
						$(this).css({'position': 'absolute', 'left': ( $(this).index() * 804 ) + 'px'});
					});
				}
				
				var nNavLeft = 394 - ( Math.floor($('.banner nav').width() / 2) );
				$('.banner nav').css('right', nNavLeft + 'px');
				$('.banner nav').animate({'top': 0}, 500);
			}
		}
	});
}

function setBannerReady(n){
	//alert("banner " + n + " ready!");
	m_abBannerReady[n] = true;
	setTimeout(function(){ addBannerNavClickEvent(n) }, 100);
}

function addBannerNavClickEvent(n){
	document.title = m_szTitle;
	
	$('.banner nav li:nth-child(' + n + ') a').animate({'top': 0}, 250);
	$('.banner nav li:nth-child(' + n + ') a').click(function(e){
		e.preventDefault();
		var nNut = $(this).parent().index() + 1;
	
		if ( nNut == m_nCurrentBanner ){ return; }
		
		window["stopBanner" + m_nCurrentBanner]();
				
		if ( isMobile ) {
			oSwipe.slide((nNut - 1), 500);
		} else {
			slideBanner(nNut);
		}
				
		updateBannerNav(nNut);
		
		// code for tracking clicks
		$.ajax({
			url: "http://www.nucor.com/inc/php/banner-tracker.php",
			type: "POST",
			data: "bid=" + n
		});
	});
}

function slideBanner(nIndex){
	if ( nIndex == m_nCurrentBanner ) return;
	
	//console.log("coming:" + nIndex + " - going:" + m_nCurrentBanner);
	
	var sDistance = ( ( ( m_nCurrentBanner - nIndex ) < 0 ) ? "-=" : "+=" ) + ( Math.abs( m_nCurrentBanner - nIndex ) * 804 ) + "px"; 
	//console.log(sDistance);
	TweenMax.to($('ul.banners li'), .5, {css:{left: sDistance}, onComplete:function(){}});
}

function updateBannerNav(nIndex){
	if ( nIndex == m_nCurrentBanner ) return;

	$('.banner nav li:nth-child(' + m_nCurrentBanner + ') a').removeClass('on');
	$('.banner nav li:nth-child(' + nIndex + ') a').addClass('on');

	m_nCurrentBanner = nIndex;
	
	window["resetBanner" + m_nCurrentBanner]();
	setTimeout(function(){window["playBanner" + m_nCurrentBanner]();}, 500);
}
