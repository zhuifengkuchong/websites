function playStakeInTheFire(){
	if ( !m_abBannerReady[m_nCurrentBanner]) setBannerReady(m_nCurrentBanner);

	//console.log("playing: StakeInTheFire");
	TweenMax.to($('.bnr_StakeInTheFire div.bg1'), 2.0, {css:{autoAlpha:1}, delay: 1});
	TweenMax.to($('.bnr_StakeInTheFire .frame1 h1'), 1.0, {css:{autoAlpha:1}, delay:2.0});
	TweenMax.to($('.bnr_StakeInTheFire .frame1 h2'), 1.0, {css:{autoAlpha:1}, delay:3.0});
	TweenMax.to($('.bnr_StakeInTheFire .frame1 h3'), 1.0, {css:{autoAlpha:1}, delay:4.0});
	
	TweenMax.to($('.bnr_StakeInTheFire div.bg2'), 1.0, {css:{autoAlpha:0.66}, delay:8});
	TweenMax.to($('.bnr_StakeInTheFire .frame1'), 1.0, {css:{autoAlpha:0}, delay:8, onComplete: frame2});	
	
	function frame2(){
		TweenMax.to($('.bnr_StakeInTheFire .frame2'), 2, {css:{autoAlpha:1}});
	}
};

function stopStakeInTheFire(){
	//console.log("stopping: StakeInTheFire");
	TweenMax.killAll();
};

function resetStakeInTheFire(){
	//console.log("resetting: StakeInTheFire");

	TweenMax.set($('.bnr_StakeInTheFire div.bg1'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_StakeInTheFire div.bg2'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_StakeInTheFire .frame1'), {css:{autoAlpha:1}});
	TweenMax.set($('.bnr_StakeInTheFire .frame1 h1'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_StakeInTheFire .frame1 h2'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_StakeInTheFire .frame1 h3'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_StakeInTheFire .frame2'), {css:{autoAlpha:0}});
};
