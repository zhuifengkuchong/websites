function playFreeTrade(){
	if ( !m_abBannerReady[m_nCurrentBanner]) setBannerReady(m_nCurrentBanner);

	//console.log("playing: FreeTrade");
	TweenMax.to($('.bnr_FreeTrade div.bg1'), 2.0, {css:{autoAlpha:1, top:0}, delay: 1});
	TweenMax.to($('.bnr_FreeTrade .frame1'), 0.5, {css:{autoAlpha:1}, delay:3.5});
	
	TweenMax.to($('.bnr_FreeTrade div.bg2'), 1.0, {css:{autoAlpha:0.66}, delay:6.5});
	TweenMax.to($('.bnr_FreeTrade .frame1'), 1.0, {css:{autoAlpha:0}, delay:6.5, onComplete: frame2});	
	
	function frame2(){
		TweenMax.to($('.bnr_FreeTrade .frame2'), 2, {css:{autoAlpha:1}});
	}
};

function stopFreeTrade(){
	//console.log("stopping: FreeTrade");
	TweenMax.killAll();
};

function resetFreeTrade(){
	//console.log("resetting: FreeTrade");

	TweenMax.set($('.bnr_FreeTrade div.bg1'), {css:{autoAlpha:0, top:-100}});
	TweenMax.set($('.bnr_FreeTrade div.bg2'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_FreeTrade .frame1'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_FreeTrade .frame2'), {css:{autoAlpha:0}});
};
