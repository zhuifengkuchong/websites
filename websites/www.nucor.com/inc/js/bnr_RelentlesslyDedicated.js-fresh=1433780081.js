var oTwinkle;

function playRelentlesslyDedicated(){
	if ( !m_abBannerReady[m_nCurrentBanner]) setBannerReady(m_nCurrentBanner);

	//console.log("playing banner6");
	TweenMax.to($('.bnr_RelentlesslyDedicated div.bg1'), 2, {css:{autoAlpha:1}});
	TweenMax.to($('.bnr_RelentlesslyDedicated div.bg2'), 3, {css:{autoAlpha:1}, delay:0});
	TweenMax.to($('.bnr_RelentlesslyDedicated .frame1 h1'), 0.5, {css:{autoAlpha:1}, delay:2.0});
	TweenMax.to($('.bnr_RelentlesslyDedicated .frame1 h2'), 0.5, {css:{autoAlpha:1}, delay:2.0});
	TweenMax.to($('.bnr_RelentlesslyDedicated .frame1 h3'), 0.5, {css:{autoAlpha:1}, delay:2.0});
	TweenMax.to($('.bnr_RelentlesslyDedicated .frame1 h4'), 0.5, {css:{autoAlpha:1}, delay:3.5});
	TweenMax.to($('.bnr_RelentlesslyDedicated .frame1 h1'), 0.5, {css:{left:'530px'}, delay:3.5});
	TweenMax.to($('.bnr_RelentlesslyDedicated .frame1 h2'), 0.5, {css:{left:'55px'}, delay:3.5});
	TweenMax.to($('.bnr_RelentlesslyDedicated .frame1 h3'), 0.5, {css:{left:'465px'}, delay:3.5});
	TweenMax.to($('.bnr_RelentlesslyDedicated div.bg3'), 1, {css:{autoAlpha:1}, delay:3.5, onComplete:twinkle});
	
	TweenMax.to($('.bnr_RelentlesslyDedicated div.bg4'), 2, {css:{autoAlpha:0.66}, delay:6.0, onComplete:killTwinkle});
	TweenMax.to($('.bnr_RelentlesslyDedicated .frame1'), 2, {css:{autoAlpha:0}, delay:6.0, onComplete: frame2});	
	
	function twinkle(){
		$('.bnr_RelentlesslyDedicated div.bg3').css({'opacity': (randNum(90,100) / 100)});
		oTwinkle = setTimeout(twinkle, 50);
	}
	
	function killTwinkle(){
		clearTimeout(oTwinkle);
	}
	
	function frame2(){
		//console.log("starting frame 2 sequence!");
		TweenMax.to($('.bnr_RelentlesslyDedicated .frame2'), 2, {css:{autoAlpha:1}});
	}
}

function stopRelentlesslyDedicated(){
	//console.log("stopping banner6");
	clearTimeout(oTwinkle);
	TweenMax.killAll();
}

function resetRelentlesslyDedicated(){
	//console.log("resetting banner6");
	TweenMax.set([
		$('.bnr_RelentlesslyDedicated div.bg1'),
		$('.bnr_RelentlesslyDedicated div.bg2'),
		$('.bnr_RelentlesslyDedicated div.bg3'),
		$('.bnr_RelentlesslyDedicated div.bg4'),
		$('.bnr_RelentlesslyDedicated .frame1 h4'),
		$('.bnr_RelentlesslyDedicated div.frame2')
	], {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_RelentlesslyDedicated .frame1 h1'), {css:{autoAlpha: 0, left:'466px'}});
	TweenMax.set($('.bnr_RelentlesslyDedicated .frame1 h2'), {css:{autoAlpha: 0, left:'140px'}});
	TweenMax.set($('.bnr_RelentlesslyDedicated .frame1 h3'), {css:{autoAlpha: 0, left:'400px'}});
	TweenMax.set($('.bnr_RelentlesslyDedicated .frame1'), {css:{autoAlpha:1}});
	TweenMax.set($('.bnr_RelentlesslyDedicated .frame2'), {css:{autoAlpha:0}});
}
