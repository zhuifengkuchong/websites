//(function(a){$.browser.mobile=/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

//var isMobile = $.browser.mobile;
var isMobile = ( /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) );

var isIphone = navigator.userAgent.toLowerCase().indexOf("iphone") < 0 ? false : true;
var isIpad = navigator.userAgent.toLowerCase().indexOf("ipad") < 0 ? false : true;
var isIpod = navigator.userAgent.toLowerCase().indexOf("ipod") < 0 ? false : true;
var isAndroid = navigator.userAgent.toLowerCase().indexOf("android") < 0 ? false : true;
var isBlackberry = navigator.userAgent.toLowerCase().indexOf("blackberry") < 0 ? false : true;

$(function()
{
	// adjust sub-sub nav item widths based on the number of items in the given list
	$('.content nav.sub ul li ul').css('display', 'none');
	$('.content nav.sub ul li').each(function()
	{
		if ( $(this).find('> ul').length > 0 ) $(this).addClass('more');
	});

	$('.content nav.sub ul li ul').each(function()
	{
		var nChildren = $(this).children().length;
		var nWidth = 100 / nChildren;

		if ( $(this).parent().parent().parent().hasClass('sub') )
		{
			$(this).find('> li').css('width', nWidth + '%');
		}
		
		$(this).find('li a[target="_blank"]').addClass('external');
	});
	
	$('.content nav.sub ul li').mouseover(function()
	{
		if ( $(this).find('> ul').length > 0 ) $(this).find('> ul').show(); /*$(this).find('> ul').stop().animate({'top': '40px', 'opacity': '1'}, {'duration': 125});*/
	});
	
	$('.content nav.sub ul li').mouseout(function()
	{
		//if ( $(this).find('> ul').length > 0 ) $(this).find('> ul').hide(); /*$(this).find('> ul').stop().delay(500).animate({'top': '10px', 'opacity': '0'}, {'duration': 250});*/
		if ($(this).hasClass('more'))
		{
			$(this).find('ul').hide();
		}
	});

	$('.copyright li:first-child a').click(function(e)
	{
		e.preventDefault();
		var sURL = $(this).attr('href');
		window.open(sURL, 'contact', 'width=330,height=420,status=0,scrollbars=auto,location=0,menubar=0,resizable=0,toolbar=0');
	});
	
	/*
	$('.links li:nth-child(3) a').click(function(e)
	{
		e.preventDefault();
		var sURL = $(this).attr('href');
		window.open(sURL, 'alerts', 'width=450,height=420,status=0,scrollbars=auto,location=0,menubar=0,resizable=0,toolbar=0');
	});
	*/
	
	initDateTime();
	if ( typeof(aTicker) != "undefined" ) initTicker();
	
	if ( $('.content nav.sub').length != 0 )
	{
		$('.content nav.sub').horizontalNav({});
		//$('.content nav.sub ul li').not('.content nav.sub ul li ul li').horizontalNav({});
		var nSubnavTop = $('.content nav.sub').offset().top - parseFloat($('.content nav.sub').css('margin-top').replace(/auto/, 12)) + $('.content nav.sub').height();
		
        $(function()
        {
			var isMSIE6 = $.browser == 'msie' && $.browser.version < 7;
           	
           	if ( !isMSIE6 )
           	{
                $(window).scroll(function(e)
                {
                    // what the y position of the scroll is
                    var nY = $(this).scrollTop();
                    
                    // whether that's below the form
                    if ( nY >= nSubnavTop )
                    { 
                        $('.content nav.sub').addClass('fixed');
                        //$('div.content').css('padding-top', '50px');
                    }
                    else
                    {
                        $('.content nav.sub').removeClass('fixed');
                        //$('div.content').css('padding-top', '0');
                    }
                });
            }
        });
        
        //$('.content nav.sub').delay(250).animate({'top': '0'}, {'duration': 500});
        
    	$('<img />').load(function(){$('.content nav.sub').delay(250).animate({'top': '0'}, {'duration': 250});}).attr('src', '../../img/bg-subnav.png'/*tpa=http://www.nucor.com/img/bg-subnav.png*/);
    }
    
    // +--------------------[ smooth scrolling ]--------------------+ //
	function filterPath(string)
	{
  		return string
    	.replace(/^\//,'')
    	.replace(/(index|default).[a-zA-Z]{3,4}$/,'')
    	.replace(/\/$/,'');
  	}
  	
  	var locationPath = filterPath(location.pathname);
  	var scrollElem = scrollableElement('html', 'body');

  	$('a[href*=#]').each(function()
  	{
    	var thisPath = filterPath(this.pathname) || locationPath;
    
    	if (  locationPath == thisPath && ( location.hostname == this.hostname || !this.hostname ) && this.hash.replace(/#/,'') )
    	{
      		var $target = $(this.hash), target = this.hash;
      
      		if ( $target.length )
      		{
        		var targetOffset = $target.offset().top;
        		$(this).click(function(event)
        		{
          			event.preventDefault();
          			
          			$(scrollElem).animate({scrollTop: targetOffset}, 400, function()
          			{
            			location.hash = target == "#top" ? "" : target;
          			});
        		});
      		}
    	}
    });

  	// use the first element that is "scrollable"
  	function scrollableElement(els)
  	{
    	for ( var i = 0, argLength = arguments.length; i < argLength; i++ )
    	{
      		var el = arguments[i],
          	$scrollElement = $(el);
      
      		if ( $scrollElement.scrollTop() > 0 )
      		{
        		return el;
      		}
      		else
      		{
        		$scrollElement.scrollTop(1);
        		var isScrollable = $scrollElement.scrollTop() > 0;
        		$scrollElement.scrollTop(0);
        		
        		if ( isScrollable )
        		{
          			return el;
        		}
      		}
    	}
    	
    	return [];
  	}
  	
  	$('.search span').append('<input type="submit">');
  	
  	$('.search form').submit(function(e)
  	{
  		if ( $('.search input[type=text]').val() == "" ) e.preventDefault();
  	});
  	
  	if ( !hasPlaceholderSupport() )
	{
		$('input[placeholder]').each(function()
		{
			if ( $(this).val() == "" ) $(this).val($(this).attr('placeholder')).css('color', '#666');
			
			$(this).focus(function()
			{
				if ( $(this).val() == $(this).attr('placeholder') ) $(this).val('').css('color', '#000');
			});
			
			$(this).blur(function()
			{
				if ( $(this).val() == "" ) $(this).val($(this).attr('placeholder')).css('color', '#666');
			});
		});
	}
});

aMonth = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

function datetime()
{
    oToday = new Date();
    
    nTimezoneOffset = Math.floor(oToday.getTimezoneOffset() / 60); // number of hours difference between UTC and user's machine
    nHourDiff = ( nTimezoneOffset - 4 ) * 1;
    
    oToday.setTime(oToday.getTime()); // + ( nHourDiff * 3600000 ));

    sMonth = aMonth[oToday.getMonth()];
    nDate = oToday.getDate();
    nYear = oToday.getFullYear();
    
    nHour = oToday.getHours();
    nMinute = oToday.getMinutes();
        
    sDatetime = sMonth + " " + nDate + ", " + nYear + " " + twelveHour(nHour) + ":" + zeroPad(nMinute) + " " + amPm(nHour);
    $('.datetime-ticker').children().eq(0).empty();
    $('.datetime-ticker').children().eq(0).append(sDatetime);
}

function twelveHour(n)
{
    return n > 12 ? n - 12 : n;
}

function zeroPad(n)
{
    return n < 10 ? "0" + n : n;
}

function amPm(n)
{
    return n > 11 ? "PM" : "AM";
}

function initDateTime()
{
    datetime();
    hDatetime = setInterval("datetime()", 5000);
}

var nHeadline = 0;
var hTimer;

function newsTicker()
{
	nHeadline++;
	if ( nHeadline == aTicker.length ) nHeadline = 0;
	//console.log($('.h2').css('top'));
	var oDivGoing = $('.h1').css('top') == "0px" ? $('.h1') : $('.h2');
	var oDivComing = $('.h1').css('top') == "20px" ? $('.h1') : $('.h2');
	//console.log("going: " + oDivGoing.attr('class') + " - coming: " + oDivComing.attr('class'));
	oDivComing.empty();
	oDivComing.append(aTicker[nHeadline]);
	oDivComing.css('top', '-20px');
	//oDivGoing.fadeOut('slow', function(){$(this).css('top', '20px');});
	oDivGoing.animate({'top': '20px'}, {'duration': 250});
	//oDivComing.show();
	oDivComing.animate({'top': '0px'}, {'duration': 250});
	hTicker = setTimeout("newsTicker()", 5000);
}

function initTicker()
{
	$('.datetime-ticker').children().eq(1).append('<div class="h1"></div>');
	$('.datetime-ticker').children().eq(1).append('<div class="h2"></div>');
	$('.h1').append(aTicker[0]);
	
	$('.h1').mouseover(function()
	{
		clearTimeout(hTicker);
	});
	
	$('.h1').mouseout(function()
	{
		hTicker = setTimeout("newsTicker()", 2500);
	});
	
	$('.h2').mouseover(function()
	{
		clearTimeout(hTicker);
	});
	
	$('.h2').mouseout(function()
	{
		hTicker = setTimeout("newsTicker()", 2500);
	});

	hTicker = setTimeout("newsTicker()", 5000);
}

$('.panel a').not('.panel div a').attr('target', '_blank');

var hCloseMenu;
var nActiveMenu = -1;
var isOverMenu = false;

$('.panel').mouseenter(function()
{
	isOverMenu = true;
});

$('.panel').mouseleave(function()
{
	isOverMenu = false;
	closeMe();
});

$('.menu li a').click(function(e)
{
	e.preventDefault();
	var nIndex = $(this).parent().index() + 1;
	
	if ( ( nActiveMenu == -1 ) || ( nIndex != nActiveMenu ) )
	{
		closeOthers(nIndex);
		var nHeight = $('.p' + nIndex).height();
		$('.w' + nIndex).animate({'top': '-' + ( nHeight + 13 ) + 'px', 'height' : ( nHeight + 13 ) + 'px'}, 250);
		nActiveMenu = nIndex;
		hCloseMenu = setTimeout("closeMe()", 5000);
		$(this).css('color', '#fff');
		var sText = "[&#8211;]" + $(this).text().substr(3);
		$(this).empty();
		$(this).append(sText);
		$('.menu li:nth-child(' + nActiveMenu + ') a')
	}
	else
	{
		closeMe();
	}
});

$('.panel div a').click(function(e)
{
	e.preventDefault();
	var oMenu = $(this).parent().parent().parent();
	var oListItem = $('.menu li:nth-child(' + nActiveMenu + ') a');
	oListItem.css('color', '#000');
	var sText = "[+]" + oListItem.text().substr(3);
	oListItem.empty();
	oListItem.append(sText);
	oMenu.animate({'top': 0, 'height': 0}, 250);
	clearTimeout(hCloseMenu);
});

function closeOthers(nIndexOpen)
{
	for ( var n = 1; n < $('.menu').children('li').length + 1; n++ )
	{
		if ( n != nIndexOpen )
		{
			$('.p' + n + ' div a').trigger('click');
			//$('.menu li:nth-child(' + n + ') a').css('color', '#000');
		}
	}
}

function closeMe()
{
	if ( !isOverMenu )
	{
		clearTimeout(hCloseMenu);
		$('.p' + nActiveMenu + ' div a').trigger('click');
		//$('.menu li:nth-child(' + nActiveMenu + ') a').css('color', '#000');
		nActiveMenu = -1;
	}
}

function randNum(nMin, nMax)
{
	return Math.floor(Math.random() * (nMax - nMin + 1)) + nMin;
}

jQuery.fn.justtext = function()
{
   return $(this).clone().children().remove().end().text();
 
};

function hasPlaceholderSupport()
{
	var oInput = document.createElement('input');
	return ('placeholder' in oInput);
}


function showGraphs(oGraphs)
{
	oGraphs.addClass('on');

	var nNationalMax = Math.max(oGraphs.find('figure:nth-child(3) div:nth-child(1) span p').text(), oGraphs.find('figure:nth-child(4) div:nth-child(1) span p').text(), oGraphs.find('figure:nth-child(5) div:nth-child(1) span p').text());
	var nNucorMax =    Math.max(oGraphs.find('figure:nth-child(3) div:nth-child(2) span p').text(), oGraphs.find('figure:nth-child(4) div:nth-child(2) span p').text(), oGraphs.find('figure:nth-child(5) div:nth-child(2) span p').text());
	var nMax = Math.max(nNationalMax, nNucorMax);
	var nMinPx = 45;
	
	var nMaxHeight = oGraphs.find('figure').height() - 10;
	var nPercent = nMax / nMaxHeight;
	
	showGraph(oGraphs.find('figure:nth-child(3)'), nMinPx, nPercent);
	showGraph(oGraphs.find('figure:nth-child(4)'), nMinPx, nPercent);
	showGraph(oGraphs.find('figure:nth-child(5)'), nMinPx, nPercent);
}

function showGraph(oFigure, nMin, nPercent)
{
	var nHeightNational = Math.max(nMin, oFigure.find('div:nth-child(1) span p').text() / nPercent);
	oFigure.find('div:nth-child(1) span').animate({'height': nHeightNational + 'px'}, 1000);
	oFigure.find('div:nth-child(1) span p').fadeIn('slow');
	
	var nHeightNucor = Math.max(nMin, oFigure.find('div:nth-child(2) span p').text() / nPercent);
	oFigure.find('div:nth-child(2) span').animate({'height': nHeightNucor + 'px'}, 1000);
	oFigure.find('div:nth-child(2) span p').fadeIn('slow');
}

function showGraphs2(oGraphs)
{
	oGraphs.addClass('on');
	oGraphs.find('figure:nth-child(2) div span').animate({'width': '152px'}, 50, 'linear').delay(0).animate({'width': '96px'}, 500, 'linear').animate({'width': '90px'}, 50, 'linear').delay(0).animate({'width': '34px'}, 500, 'linear').animate({'width': '27px'}, 50, 'linear');
	oGraphs.find('figure:nth-child(3) div span').delay(1250).animate({'width': '152px'}, 50, 'linear').delay(0).animate({'width': '96px'}, 500, 'linear').animate({'width': '90px'}, 50, 'linear').delay(0).animate({'width': '34px'}, 500, 'linear').animate({'width': '27px'}, 50, 'linear');
	oGraphs.find('figure:nth-child(4) div span').delay(2500).animate({'width': '152px'}, 50, 'linear').delay(0).animate({'width': '96px'}, 500, 'linear').animate({'width': '90px'}, 50, 'linear').delay(0).animate({'width': '34px'}, 500, 'linear').animate({'width': '27px'}, 50, 'linear');
}