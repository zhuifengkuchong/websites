if ( typeof HD == "undefined" || !HD) {
	var HD = {}
}
HD.Language = {
	basePath : HD.lib.getURL('hdbase_hdLanguage_basePath'),
	pathOverride : null,
	useMinJS : "-min",
	events : {
		handleLanguageFileLoad : function(a) {
			HD.Language.events.handleLanguageFileLoadSuccess();        

        },
		handleLanguageFileLoadSuccess : function(d) {

			if (HD.Language.data.IsAvailable == "YES") {
				if (HD.Language.pathOverride) {
					var a = HD.util.Localization.getLocale() || "en_US"; 
					var f = {
						success : function() {
							HD.Language.events.handleLanguageFileOrverrideLoad(a)
						},
						failure : function() {
							HD.Language.events.handleProfileLoad()
						}
					};
					try {
						var c = YAHOO.util.Connect.asyncRequest("GET", HD.util.CoreUrl.getCoreReplacementDomain(HD.Language.pathOverride + a + HD.Language.useMinJS + ".js"), f)
					} catch (b) {
						HD.Language.events.handleProfileLoad()
					}
				} else {
					HD.Language.events.handleProfileLoad()
				}
			} else {
				HD.Language.events.handleLanguageFileLoad("en_US")
			}
		},
		handleProfileLoad : function() {
			var b = {
				onSuccess : HD.Language.events.handleProfileScriptLoadSuccess
			};
			var a = {
				onSuccess : HD.Language.events.handleNavScriptLoadSuccess
			};
			
            YAHOO.util.Get.script(HD.Language.getFinalURL('hdbase_hdProfile'), b);
			YAHOO.util.Get.script(HD.Language.getFinalURL('hdbase_hdNav'), a)


            

          /*  $.getScript( HD.Language.getFinalURL('hdbase_hdProfile')).done(function(script, textStatus) {
               HD.Language.events.handleProfileScriptLoadSuccess();
            }).fail(function(jqxhr, settings, exception) {
                console.log(exception);
            });

            var url2 = HD.lib.getURL('hdbase_hdNav') + HD.Language.useMinJS + ".js"            
            

            $.getScript(HD.Language.getFinalURL('hdbase_hdNav')).done(function(script, textStatus) {
                HD.Language.events.handleNavScriptLoadSuccess();
            }).fail(function(jqxhr, settings, exception) {
                console.log(exception);
            });*/

        },
		handleLanguageFileOrverrideLoad : function(a) {
			var c = {
				onSuccess : HD.Language.events.handleLanguageFileOrverrideLoadSuccess
			};
			var b = YAHOO.util.Get.script(HD.Language.getFinalURL(HD.Language.pathOverride,a), c)

            /*$.getScript(HD.Language.getFinalURL('HD.Language.pathOverride',a)).done(function(script, textStatus) {
                HD.Language.events.handleLanguageFileOrverrideLoadSuccess();
            }).fail(function(jqxhr, settings, exception) {
                console.log(exception);
            });*/

        },
		handleLanguageFileOrverrideLoadSuccess : function(b) {
			if (HD.Language.data.IsAvailable == "YES") {
				var c = {
					onSuccess : HD.Language.events.handleProfileScriptLoadSuccess
				};
				var a = {
					onSuccess : HD.Language.events.handleNavScriptLoadSuccess
				};
				YAHOO.util.Get.script(HD.Language.getFinalURL('hdbase_hdProfile'), c);
				YAHOO.util.Get.script(HD.Language.getFinalURL('hdbase_hdNav'), a);
			    
          /* $.getScript(HD.Language.getFinalURL('hdbase_hdProfile')).done(function(script, textStatus) {
                HD.Language.events.handleProfileScriptLoadSuccess();
            }).fail(function(jqxhr, settings, exception) {
                console.log(exception);
            });

            $.getScript(HD.Language.getFinalURL('hdbase_hdNav')).done(function(script, textStatus) {
                HD.Language.events.handleNavScriptLoadSuccess();
            }).fail(function(jqxhr, settings, exception) {
                console.log(exception);
            });*/


            } else {
				HD.Language.events.handleLanguageFileLoad("en_US")
			}
		},
		handleNavScriptLoadSuccess : function(a) {
			HD.Nav.displayNav()
		},
		handleProfileScriptLoadSuccess : function(a) {
			var b = {
				onSuccess : HD.Language.events.handleProfileModelScriptLoadSuccess
			};
			YAHOO.util.Get.script(HD.Language.getFinalURL('hdbase_hdProfile_content'), b)
		
           /* $.getScript(HD.Language.getFinalURL('hdbase_hdProfile_content')).done(function(script, textStatus) {
                HD.Language.events.handleProfileModelScriptLoadSuccess();
            }).fail(function(jqxhr, settings, exception) {
                console.log(exception);
            });*/
        },
		handleProfileModelScriptLoadSuccess : function(b) {
			if (!HD.Profile.profile) {
				var a = HD.Config.profile || {};
				var c = a.initCallback || "";
				HD.Profile.profile = new HD.Profile(a);
				HD.Profile.profile.model.init(c)
			}
		}
	},
	init : function(a) {
		//HD.Language.events.handleLanguageFileLoad(a);
        HD.Language.data=profileData;
        HD.Language.events.handleLanguageFileLoadSuccess(); 
	},
    getFinalURL : function(path ,locale){
        var url;
        if(path === HD.Language.basePath){
            url=HD.Language.basePath + (locale || '') + HD.Language.useMinJS + ".js";
        }else if(path === HD.Language.pathOverride){
            url=HD.Language.pathOverride + (locale || '') + HD.Language.useMinJS + ".js";
        }else{
           url= HD.lib.getURL(path) + HD.Language.useMinJS + ".js";
        }

       //var url = ((path === HD.Language.basePath) ? HD.Language.basePath : HD.lib.getURL(path)) + (locale || '') + HD.Language.useMinJS + ".js";
       // url= ((path === HD.Language.pathOverride) ? HD.Language.pathOverride : HD.lib.getURL(path));
        return  finalUrl = $('#externalSite')[0] ? $('#externalSite').attr('data-lang-url')+ url : HD.util.CoreUrl.getCoreReplacementDomain(url);

    }
};
YAHOO.util.Event.onDOMReady(function() {
	var a = HD.util.Localization.getLocale() || "en_US";
	var b = HD.util.Common.getRequestParam("hdjsdebug");
	if (b == "true" || b == true) {
		HD.Language.useMinJS = ""
	}
	HD.Language.init(a)
}); 