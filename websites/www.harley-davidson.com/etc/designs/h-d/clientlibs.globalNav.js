/*!
 * hoverIntent r7 // 2013.03.11 // jQuery 1.9.1+
 * http://cherne.net/brian/resources/jquery.hoverIntent.html
 *
 * You may use hoverIntent under the terms of the MIT license.
 * Copyright 2007, 2013 Brian Cherne
 */
(function(e){e.fn.hoverIntent=function(t,n,r){var i={interval:100,sensitivity:7,timeout:0};if(typeof t==="object"){i=e.extend(i,t)}else if(e.isFunction(n)){i=e.extend(i,{over:t,out:n,selector:r})}else{i=e.extend(i,{over:t,out:t,selector:n})}var s,o,u,a;var f=function(e){s=e.pageX;o=e.pageY};var l=function(t,n){n.hoverIntent_t=clearTimeout(n.hoverIntent_t);if(Math.abs(u-s)+Math.abs(a-o)<i.sensitivity){e(n).off("mousemove.hoverIntent",f);n.hoverIntent_s=1;return i.over.apply(n,[t])}else{u=s;a=o;n.hoverIntent_t=setTimeout(function(){l(t,n)},i.interval)}};var c=function(e,t){t.hoverIntent_t=clearTimeout(t.hoverIntent_t);t.hoverIntent_s=0;return i.out.apply(t,[e])};var h=function(t){var n=jQuery.extend({},t);var r=this;if(r.hoverIntent_t){r.hoverIntent_t=clearTimeout(r.hoverIntent_t)}if(t.type=="mouseenter"){u=n.pageX;a=n.pageY;e(r).on("mousemove.hoverIntent",f);if(r.hoverIntent_s!=1){r.hoverIntent_t=setTimeout(function(){l(n,r)},i.interval)}}else{e(r).off("mousemove.hoverIntent",f);if(r.hoverIntent_s==1){r.hoverIntent_t=setTimeout(function(){c(n,r)},i.timeout)}}};return this.on({"mouseenter.hoverIntent":h,"mouseleave.hoverIntent":h},i.selector)}})(jQuery);
/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 *
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 *
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

; (function ($) {
    var tmp, loading, overlay, wrap, outer, content, close, title, nav_left, nav_right,

        selectedIndex = 0, selectedOpts = {}, selectedArray = [], currentIndex = 0, currentOpts = {}, currentArray = [],

        ajaxLoader = null, imgPreloader = new Image(), imgRegExp = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, swfRegExp = /[^\.]\.(swf)\s*$/i,

        loadingTimer, loadingFrame = 1,

        titleHeight = 0, titleStr = '', start_pos, final_pos, busy = false, fx = $.extend($('<div/>')[0], { prop: 0 }),

        isIE6 = $.browser.msie && $.browser.version < 7 && !window.XMLHttpRequest,

        /*
         * Private methods 
         */

        _abort = function () {
            loading.hide();

            imgPreloader.onerror = imgPreloader.onload = null;

            if (ajaxLoader) {
                ajaxLoader.abort();
            }

            tmp.empty();
        },

        _error = function () {
            if (false === selectedOpts.onError(selectedArray, selectedIndex, selectedOpts)) {
                loading.hide();
                busy = false;
                return;
            }

            selectedOpts.titleShow = false;

            selectedOpts.width = 'auto';
            selectedOpts.height = 'auto';

            tmp.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>');

            _process_inline();
        },

        _start = function () {
            var obj = selectedArray[selectedIndex],
                href,
                type,
                title,
                str,
                emb,
                ret;

            _abort();

            selectedOpts = $.extend({}, $.fn.fancybox.defaults, (typeof $(obj).data('fancybox') == 'undefined' ? selectedOpts : $(obj).data('fancybox')));

            ret = selectedOpts.onStart(selectedArray, selectedIndex, selectedOpts);

            if (ret === false) {
                busy = false;
                return;
            } else if (typeof ret == 'object') {
                selectedOpts = $.extend(selectedOpts, ret);
            }

            title = selectedOpts.title || (obj.nodeName ? $(obj).attr('title') : obj.title) || '';

            if (obj.nodeName && !selectedOpts.orig) {
                selectedOpts.orig = $(obj).children("img:first").length ? $(obj).children("img:first") : $(obj);
            }

            if (title === '' && selectedOpts.orig && selectedOpts.titleFromAlt) {
                title = selectedOpts.orig.attr('alt');
            }

            href = selectedOpts.href || (obj.nodeName ? $(obj).attr('href') : obj.href) || null;

            if ((/^(?:javascript)/i).test(href) || href == '#') {
                href = null;
            }

            if (selectedOpts.type) {
                type = selectedOpts.type;

                if (!href) {
                    href = selectedOpts.content;
                }

            } else if (selectedOpts.content) {
                type = 'html';

            } else if (href) {
                if (href.match(imgRegExp)) {
                    type = 'image';

                } else if (href.match(swfRegExp)) {
                    type = 'swf';

                } else if ($(obj).hasClass("iframe")) {
                    type = 'iframe';

                } else if (href.indexOf("#") === 0) {
                    type = 'inline';

                } else {
                    type = 'ajax';
                }
            }

            if (!type) {
                _error();
                return;
            }

            if (type == 'inline') {
                obj = href.substr(href.indexOf("#"));
                type = $(obj).length > 0 ? 'inline' : 'ajax';
            }

            selectedOpts.type = type;
            selectedOpts.href = href;
            selectedOpts.title = title;

            if (selectedOpts.autoDimensions) {
                if (selectedOpts.type == 'html' || selectedOpts.type == 'inline' || selectedOpts.type == 'ajax') {
                    selectedOpts.width = 'auto';
                    selectedOpts.height = 'auto';
                } else {
                    selectedOpts.autoDimensions = false;
                }
            }

            if (selectedOpts.modal) {
                selectedOpts.overlayShow = true;
                selectedOpts.hideOnOverlayClick = false;
                selectedOpts.hideOnContentClick = false;
                selectedOpts.enableEscapeButton = false;
                selectedOpts.showCloseButton = false;
            }

            selectedOpts.padding = parseInt(selectedOpts.padding, 10);
            selectedOpts.margin = parseInt(selectedOpts.margin, 10);

            tmp.css('padding', (selectedOpts.padding + selectedOpts.margin));

            $('.fancybox-inline-tmp').unbind('fancybox-cancel').bind('fancybox-change', function () {
                $(this).replaceWith(content.children());
            });

            switch (type) {
                case 'html':
                    tmp.html(selectedOpts.content);
                    _process_inline();
                    break;

                case 'inline':
                    if ($(obj).parent().is('#fancybox-content') === true) {
                        busy = false;
                        return;
                    }

                    $('<div class="fancybox-inline-tmp" />')
                        .hide()
                        .insertBefore($(obj))
                        .bind('fancybox-cleanup', function () {
                            $(this).replaceWith(content.children());
                        }).bind('fancybox-cancel', function () {
                            $(this).replaceWith(tmp.children());
                        });

                    $(obj).appendTo(tmp);

                    _process_inline();
                    break;

                case 'image':
                    busy = false;

                    $.fancybox.showActivity();

                    imgPreloader = new Image();

                    imgPreloader.onerror = function () {
                        _error();
                    };

                    imgPreloader.onload = function () {
                        busy = true;

                        imgPreloader.onerror = imgPreloader.onload = null;

                        _process_image();
                    };

                    imgPreloader.src = href;
                    break;

                case 'swf':
                    selectedOpts.scrolling = 'no';

                    str = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + selectedOpts.width + '" height="' + selectedOpts.height + '"><param name="movie" value="' + href + '"></param>';
                    emb = '';

                    $.each(selectedOpts.swf, function (name, val) {
                        str += '<param name="' + name + '" value="' + val + '"></param>';
                        emb += ' ' + name + '="' + val + '"';
                    });

                    str += '<embed src="' + href + '" type="application/x-shockwave-flash" width="' + selectedOpts.width + '" height="' + selectedOpts.height + '"' + emb + '></embed></object>';

                    tmp.html(str);

                    _process_inline();
                    break;

                case 'ajax':
                    busy = false;

                    $.fancybox.showActivity();

                    selectedOpts.ajax.win = selectedOpts.ajax.success;

                    ajaxLoader = $.ajax($.extend({}, selectedOpts.ajax, {
                        url: href,
                        data: selectedOpts.ajax.data || {},
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            if (XMLHttpRequest.status > 0) {
                                _error();
                            }
                        },
                        success: function (data, textStatus, XMLHttpRequest) {
                            var o = typeof XMLHttpRequest == 'object' ? XMLHttpRequest : ajaxLoader;
                            if (o.status == 200) {
                                if (typeof selectedOpts.ajax.win == 'function') {
                                    ret = selectedOpts.ajax.win(href, data, textStatus, XMLHttpRequest);

                                    if (ret === false) {
                                        loading.hide();
                                        return;
                                    } else if (typeof ret == 'string' || typeof ret == 'object') {
                                        data = ret;
                                    }
                                }

                                tmp.html(data);
                                _process_inline();
                            }
                        }
                    }));

                    break;

                case 'iframe':
                    _show();
                    break;
            }
        },

        _process_inline = function () {
            var
                w = selectedOpts.width,
                h = selectedOpts.height;

            if (w.toString().indexOf('%') > -1) {
                w = parseInt(($(window).width() - (selectedOpts.margin * 2)) * parseFloat(w) / 100, 10) + 'px';

            } else {
                w = w == 'auto' ? 'auto' : w + 'px';
            }

            if (h.toString().indexOf('%') > -1) {
                h = parseInt(($(window).height() - (selectedOpts.margin * 2)) * parseFloat(h) / 100, 10) + 'px';

            } else {
                h = h == 'auto' ? 'auto' : h + 'px';
            }

            tmp.wrapInner('<div style="width:' + w + ';height:' + h + ';overflow: ' + (selectedOpts.scrolling == 'auto' ? 'auto' : (selectedOpts.scrolling == 'yes' ? 'scroll' : 'hidden')) + ';position:relative;"></div>');

            selectedOpts.width = tmp.width();
            selectedOpts.height = tmp.height();

            _show();
        },

        _process_image = function () {
            selectedOpts.width = imgPreloader.width;
            selectedOpts.height = imgPreloader.height;

            $("<img />").attr({
                'id': 'fancybox-img',
                'src': imgPreloader.src,
                'alt': selectedOpts.title
            }).appendTo(tmp);

            _show();
        },

        _show = function () {
            var pos, equal;

            loading.hide();

            if (wrap.is(":visible") && false === currentOpts.onCleanup(currentArray, currentIndex, currentOpts)) {
                $.event.trigger('fancybox-cancel');

                busy = false;
                return;
            }

            busy = true;

            $(content.add(overlay)).unbind();

            $(window).unbind("http://www.harley-davidson.com/etc/designs/h-d/resize.fb scroll.fb");
            $(document).unbind('http://www.harley-davidson.com/etc/designs/h-d/keydown.fb');

            if (wrap.is(":visible") && currentOpts.titlePosition !== 'outside') {
                wrap.css('height', wrap.height());
            }

            currentArray = selectedArray;
            currentIndex = selectedIndex;
            currentOpts = selectedOpts;

            if (currentOpts.overlayShow) {
                overlay.css({
                    'background-color': currentOpts.overlayColor,
                    'opacity': currentOpts.overlayOpacity,
                    'cursor': currentOpts.hideOnOverlayClick ? 'pointer' : 'auto',
                    'height': $(document).height()
                });

                if (!overlay.is(':visible')) {
                    if (isIE6) {
                        $('select:not(#fancybox-tmp select)').filter(function () {
                            return this.style.visibility !== 'hidden';
                        }).css({ 'visibility': 'hidden' }).one('fancybox-cleanup', function () {
                            this.style.visibility = 'inherit';
                        });
                    }

                    overlay.show();
                }
            } else {
                overlay.hide();
            }

            final_pos = _get_zoom_to();

            _process_title();

            if (wrap.is(":visible")) {
                $(close.add(nav_left).add(nav_right)).hide();

                pos = wrap.position(),

                start_pos = {
                    top: pos.top,
                    left: pos.left,
                    width: wrap.width(),
                    height: wrap.height()
                };

                equal = (start_pos.width == final_pos.width && start_pos.height == final_pos.height);

                content.fadeTo(currentOpts.changeFade, 0.3, function () {
                    var finish_resizing = function () {
                        content.html(tmp.contents()).fadeTo(currentOpts.changeFade, 1, _finish);
                    };

                    $.event.trigger('fancybox-change');

                    content
                        .empty()
                        .removeAttr('filter')
                        .css({
                            'border-width': currentOpts.padding,
                            'width': final_pos.width - currentOpts.padding * 2,
                            'height': selectedOpts.autoDimensions ? 'auto' : final_pos.height - titleHeight - currentOpts.padding * 2
                        });

                    if (equal) {
                        finish_resizing();

                    } else {
                        fx.prop = 0;

                        $(fx).animate({ prop: 1 }, {
                            duration: currentOpts.changeSpeed,
                            easing: currentOpts.easingChange,
                            step: _draw,
                            complete: finish_resizing
                        });
                    }
                });

                return;
            }

            wrap.removeAttr("style");

            content.css('border-width', currentOpts.padding);

            if (currentOpts.transitionIn == 'elastic') {
                start_pos = _get_zoom_from();

                content.html(tmp.contents());

                wrap.show();

                if (currentOpts.opacity) {
                    final_pos.opacity = 0;
                }

                fx.prop = 0;

                $(fx).animate({ prop: 1 }, {
                    duration: currentOpts.speedIn,
                    easing: currentOpts.easingIn,
                    step: _draw,
                    complete: _finish
                });

                return;
            }

            if (currentOpts.titlePosition == 'inside' && titleHeight > 0) {
                title.show();
            }

            wrap.css('visibility', 'hidden');

            content
                .css({
                    'width': final_pos.width - currentOpts.padding * 2,
                    'height': selectedOpts.autoDimensions ? 'auto' : final_pos.height - titleHeight - currentOpts.padding * 2
                })
                .html(tmp.contents());

            wrap
                .css(final_pos)
                .fadeIn(currentOpts.transitionIn == 'none' ? 0 : currentOpts.speedIn, _finish);
        },

        _format_title = function (title) {
            if (title && title.length) {
                if (currentOpts.titlePosition == 'float') {
                    return '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + title + '</td><td id="fancybox-title-float-right"></td></tr></table>';
                }

                return '<div id="fancybox-title-' + currentOpts.titlePosition + '">' + title + '</div>';
            }

            return false;
        },

        _process_title = function () {
            titleStr = currentOpts.title || '';
            titleHeight = 0;

            title
                .empty()
                .removeAttr('style')
                .removeClass();

            if (currentOpts.titleShow === false) {
                title.hide();
                return;
            }

            titleStr = $.isFunction(currentOpts.titleFormat) ? currentOpts.titleFormat(titleStr, currentArray, currentIndex, currentOpts) : _format_title(titleStr);

            if (!titleStr || titleStr === '') {
                title.hide();
                return;
            }

            title
                .addClass('fancybox-title-' + currentOpts.titlePosition)
                .html(titleStr)
                .appendTo('body')
                .show();

            switch (currentOpts.titlePosition) {
                case 'inside':
                    title
                        .css({
                            'width': final_pos.width - (currentOpts.padding * 2),
                            'marginLeft': currentOpts.padding,
                            'marginRight': currentOpts.padding
                        });

                    titleHeight = title.outerHeight(true);

                    title.appendTo(outer);

                    final_pos.height += titleHeight;
                    break;

                case 'over':
                    title
                        .css({
                            'marginLeft': currentOpts.padding,
                            'width': final_pos.width - (currentOpts.padding * 2),
                            'bottom': currentOpts.padding
                        })
                        .appendTo(outer);
                    break;

                case 'float':
                    title
                        .css('left', parseInt((title.width() - final_pos.width - 40) / 2, 10) * -1)
                        .appendTo(wrap);
                    break;

                default:
                    title
                        .css({
                            'width': final_pos.width - (currentOpts.padding * 2),
                            'paddingLeft': currentOpts.padding,
                            'paddingRight': currentOpts.padding
                        })
                        .appendTo(wrap);
                    break;
            }

            title.hide();
        },

        _set_navigation = function () {
            if (currentOpts.enableEscapeButton || currentOpts.enableKeyboardNav) {
                $(document).bind('http://www.harley-davidson.com/etc/designs/h-d/keydown.fb', function (e) {
                    if (e.keyCode == 27 && currentOpts.enableEscapeButton) {
                        e.preventDefault();
                        $.fancybox.close();

                    } else if ((e.keyCode == 37 || e.keyCode == 39) && currentOpts.enableKeyboardNav && e.target.tagName !== 'INPUT' && e.target.tagName !== 'TEXTAREA' && e.target.tagName !== 'SELECT') {
                        e.preventDefault();
                        $.fancybox[e.keyCode == 37 ? 'prev' : 'next']();
                    }
                });
            }

            if (!currentOpts.showNavArrows) {
                nav_left.hide();
                nav_right.hide();
                return;
            }

            if ((currentOpts.cyclic && currentArray.length > 1) || currentIndex !== 0) {
                nav_left.show();
            }

            if ((currentOpts.cyclic && currentArray.length > 1) || currentIndex != (currentArray.length - 1)) {
                nav_right.show();
            }
        },

        _finish = function () {
            if (!$.support.opacity) {
                content.get(0).style.removeAttribute('filter');
                wrap.get(0).style.removeAttribute('filter');
            }

            if (selectedOpts.autoDimensions) {
                content.css('height', 'auto');
            }

            wrap.css('height', 'auto');

            if (titleStr && titleStr.length) {
                title.show();
            }

            if (currentOpts.showCloseButton) {
                close.show();
            }

            _set_navigation();

            if (currentOpts.hideOnContentClick) {
                content.bind('click', $.fancybox.close);
            }

            if (currentOpts.hideOnOverlayClick) {
                overlay.bind('click', $.fancybox.close);
            }

            $(window).bind("http://www.harley-davidson.com/etc/designs/h-d/resize.fb", $.fancybox.resize);

            if (currentOpts.centerOnScroll) {
                $(window).bind("http://www.harley-davidson.com/etc/designs/h-d/scroll.fb", $.fancybox.center);
            }

            if (currentOpts.type == 'iframe') {
                $('<iframe id="fancybox-frame" name="fancybox-frame' + new Date().getTime() + '" frameborder="0" hspace="0" ' + ($.browser.msie ? 'allowtransparency="true""' : '') + ' scrolling="' + selectedOpts.scrolling + '" src="' + currentOpts.href + '"></iframe>').appendTo(content);
            }

            wrap.show();

            busy = false;

            $.fancybox.center();

            currentOpts.onComplete(currentArray, currentIndex, currentOpts);

            _preload_images();

            wrap.css('visibility', 'visible');
        },

        _preload_images = function () {
            var href,
                objNext;

            if ((currentArray.length - 1) > currentIndex) {
                href = currentArray[currentIndex + 1].href;

                if (typeof href !== 'undefined' && href.match(imgRegExp)) {
                    objNext = new Image();
                    objNext.src = href;
                }
            }

            if (currentIndex > 0) {
                href = currentArray[currentIndex - 1].href;

                if (typeof href !== 'undefined' && href.match(imgRegExp)) {
                    objNext = new Image();
                    objNext.src = href;
                }
            }
        },

        _draw = function (pos) {
            var dim = {
                width: parseInt(start_pos.width + (final_pos.width - start_pos.width) * pos, 10),
                height: parseInt(start_pos.height + (final_pos.height - start_pos.height) * pos, 10),

                top: parseInt(start_pos.top + (final_pos.top - start_pos.top) * pos, 10),
                left: parseInt(start_pos.left + (final_pos.left - start_pos.left) * pos, 10)
            };

            if (typeof final_pos.opacity !== 'undefined') {
                dim.opacity = pos < 0.5 ? 0.5 : pos;
            }

            wrap.css(dim);

            content.css({
                'width': dim.width - currentOpts.padding * 2,
                'height': dim.height - (titleHeight * pos) - currentOpts.padding * 2
            });
        },

        _get_viewport = function () {
            return [
                $(window).width() - (currentOpts.margin * 2),
                $(window).height() - (currentOpts.margin * 2),
                $(document).scrollLeft() + currentOpts.margin,
                $(document).scrollTop() + currentOpts.margin
            ];
        },

        _get_zoom_to = function () {
            var view = _get_viewport(),
                to = {},
                resize = currentOpts.autoScale,
                double_padding = currentOpts.padding * 2,
                ratio;

            if (currentOpts.width.toString().indexOf('%') > -1) {
                to.width = parseInt((view[0] * parseFloat(currentOpts.width)) / 100, 10);
            } else {
                to.width = currentOpts.width + double_padding;
            }

            if (currentOpts.height.toString().indexOf('%') > -1) {
                to.height = parseInt((view[1] * parseFloat(currentOpts.height)) / 100, 10);
            } else {
                to.height = currentOpts.height + double_padding;
            }

            if (resize && (to.width > view[0] || to.height > view[1])) {
                if (selectedOpts.type == 'image' || selectedOpts.type == 'swf') {
                    ratio = (currentOpts.width) / (currentOpts.height);

                    if ((to.width) > view[0]) {
                        to.width = view[0];
                        to.height = parseInt(((to.width - double_padding) / ratio) + double_padding, 10);
                    }

                    if ((to.height) > view[1]) {
                        to.height = view[1];
                        to.width = parseInt(((to.height - double_padding) * ratio) + double_padding, 10);
                    }

                } else {
                    to.width = Math.min(to.width, view[0]);
                    to.height = Math.min(to.height, view[1]);
                }
            }

            to.top = parseInt(Math.max(view[3] - 20, view[3] + ((view[1] - to.height - 40) * 0.5)), 10);
            to.left = parseInt(Math.max(view[2] - 20, view[2] + ((view[0] - to.width - 40) * 0.5)), 10);

            return to;
        },

        _get_obj_pos = function (obj) {
            var pos = obj.offset();

            pos.top += parseInt(obj.css('paddingTop'), 10) || 0;
            pos.left += parseInt(obj.css('paddingLeft'), 10) || 0;

            pos.top += parseInt(obj.css('border-top-width'), 10) || 0;
            pos.left += parseInt(obj.css('border-left-width'), 10) || 0;

            pos.width = obj.width();
            pos.height = obj.height();

            return pos;
        },

        _get_zoom_from = function () {
            var orig = selectedOpts.orig ? $(selectedOpts.orig) : false,
                from = {},
                pos,
                view;

            if (orig && orig.length) {
                pos = _get_obj_pos(orig);

                from = {
                    width: pos.width + (currentOpts.padding * 2),
                    height: pos.height + (currentOpts.padding * 2),
                    top: pos.top - currentOpts.padding - 20,
                    left: pos.left - currentOpts.padding - 20
                };

            } else {
                view = _get_viewport();

                from = {
                    width: currentOpts.padding * 2,
                    height: currentOpts.padding * 2,
                    top: parseInt(view[3] + view[1] * 0.5, 10),
                    left: parseInt(view[2] + view[0] * 0.5, 10)
                };
            }

            return from;
        },

        _animate_loading = function () {
            if (!loading.is(':visible')) {
                clearInterval(loadingTimer);
                return;
            }

            $('div', loading).css('top', (loadingFrame * -40) + 'px');

            loadingFrame = (loadingFrame + 1) % 12;
        };

    /*
     * Public methods 
     */

    $.fn.fancybox = function (options) {
        if (!$(this).length) {
            return this;
        }

        $(this)
            .data('fancybox', $.extend({}, options, ($.metadata ? $(this).metadata() : {})))
            .unbind('http://www.harley-davidson.com/etc/designs/h-d/click.fb')
            .bind('http://www.harley-davidson.com/etc/designs/h-d/click.fb', function (e) {
                e.preventDefault();

                if (busy) {
                    return;
                }

                busy = true;

                $(this).blur();

                selectedArray = [];
                selectedIndex = 0;

                var rel = $(this).attr('rel') || '';

                if (!rel || rel == '' || rel === 'nofollow') {
                    selectedArray.push(this);

                } else {
                    selectedArray = $("a[rel=" + rel + "], area[rel=" + rel + "]");
                    selectedIndex = selectedArray.index(this);
                }

                _start();

                return;
            });

        return this;
    };

    $.fancybox = function (obj) {
        var opts;

        if (busy) {
            return;
        }

        busy = true;
        opts = typeof arguments[1] !== 'undefined' ? arguments[1] : {};

        selectedArray = [];
        selectedIndex = parseInt(opts.index, 10) || 0;

        if ($.isArray(obj)) {
            for (var i = 0, j = obj.length; i < j; i++) {
                if (typeof obj[i] == 'object') {
                    $(obj[i]).data('fancybox', $.extend({}, opts, obj[i]));
                } else {
                    obj[i] = $({}).data('fancybox', $.extend({ content: obj[i] }, opts));
                }
            }

            selectedArray = jQuery.merge(selectedArray, obj);

        } else {
            if (typeof obj == 'object') {
                $(obj).data('fancybox', $.extend({}, opts, obj));
            } else {
                obj = $({}).data('fancybox', $.extend({ content: obj }, opts));
            }

            selectedArray.push(obj);
        }

        if (selectedIndex > selectedArray.length || selectedIndex < 0) {
            selectedIndex = 0;
        }

        _start();
    };

    $.fancybox.showActivity = function () {
        clearInterval(loadingTimer);

        loading.show();
        loadingTimer = setInterval(_animate_loading, 66);
    };

    $.fancybox.hideActivity = function () {
        loading.hide();
    };

    $.fancybox.next = function () {
        return $.fancybox.pos(currentIndex + 1);
    };

    $.fancybox.prev = function () {
        return $.fancybox.pos(currentIndex - 1);
    };

    $.fancybox.pos = function (pos) {
        if (busy) {
            return;
        }

        pos = parseInt(pos);

        selectedArray = currentArray;

        if (pos > -1 && pos < currentArray.length) {
            selectedIndex = pos;
            _start();

        } else if (currentOpts.cyclic && currentArray.length > 1) {
            selectedIndex = pos >= currentArray.length ? 0 : currentArray.length - 1;
            _start();
        }

        return;
    };

    $.fancybox.cancel = function () {
        if (busy) {
            return;
        }

        busy = true;

        $.event.trigger('fancybox-cancel');

        _abort();

        selectedOpts.onCancel(selectedArray, selectedIndex, selectedOpts);

        busy = false;
    };

    // Note: within an iframe use - parent.$.fancybox.close();
    $.fancybox.close = function () {
        if (busy || wrap.is(':hidden')) {
            return;
        }

        busy = true;

        if (currentOpts && false === currentOpts.onCleanup(currentArray, currentIndex, currentOpts)) {
            busy = false;
            return;
        }

        _abort();

        $(close.add(nav_left).add(nav_right)).hide();

        $(content.add(overlay)).unbind();

        $(window).unbind("http://www.harley-davidson.com/etc/designs/h-d/resize.fb scroll.fb");
        $(document).unbind('http://www.harley-davidson.com/etc/designs/h-d/keydown.fb');

        content.find('iframe').attr('src', isIE6 && /^https/i.test(window.location.href || '') ? 'javascript:void(false)' : 'about:blank');

        if (currentOpts.titlePosition !== 'inside') {
            title.empty();
        }

        wrap.stop();

        function _cleanup() {
            overlay.fadeOut('fast');

            title.empty().hide();
            wrap.hide();

            $.event.trigger('fancybox-cleanup');

            content.empty();

            currentOpts.onClosed(currentArray, currentIndex, currentOpts);

            currentArray = selectedOpts = [];
            currentIndex = selectedIndex = 0;
            currentOpts = selectedOpts = {};

            busy = false;
        }

        if (currentOpts.transitionOut == 'elastic') {
            start_pos = _get_zoom_from();

            var pos = wrap.position();

            final_pos = {
                top: pos.top,
                left: pos.left,
                width: wrap.width(),
                height: wrap.height()
            };

            if (currentOpts.opacity) {
                final_pos.opacity = 1;
            }

            title.empty().hide();

            fx.prop = 1;

            $(fx).animate({ prop: 0 }, {
                duration: currentOpts.speedOut,
                easing: currentOpts.easingOut,
                step: _draw,
                complete: _cleanup
            });

        } else {
            wrap.fadeOut(currentOpts.transitionOut == 'none' ? 0 : currentOpts.speedOut, _cleanup);
        }
    };

    $.fancybox.resize = function () {
        if (overlay.is(':visible')) {
            overlay.css('height', $(document).height());
        }

        $.fancybox.center(true);
    };

    $.fancybox.center = function () {
        var view, align;

        if (busy) {
            return;
        }

        align = arguments[0] === true ? 1 : 0;
        animate = arguments.length === 2 && arguments[1] === false ? 0 : 1;
        view = _get_viewport();

        if (!align && (wrap.width() > view[0] || wrap.height() > view[1])) {
            return;
        }

        var wrap_pos = {
            'top': parseInt(Math.max(view[3] - 20, view[3] + ((view[1] - content.height() - 40) * 0.5) - currentOpts.padding)),
            'left': parseInt(Math.max(view[2] - 20, view[2] + ((view[0] - content.width() - 40) * 0.5) - currentOpts.padding))
        };

        if (!animate) {
            wrap.stop().css(wrap_pos);
        } else {
            wrap.stop().animate(wrap_pos, typeof arguments[0] == 'number' ? arguments[0] : 200);
        }
    };

    $.fancybox.init = function () {
        if ($("#fancybox-wrap").length) {
            return;
        }

        $('body').append(
            tmp = $('<div id="fancybox-tmp"></div>'),
            loading = $('<div id="fancybox-loading"><div></div></div>'),
            overlay = $('<div id="fancybox-overlay"></div>'),
            wrap = $('<div id="fancybox-wrap"></div>')
        );

        outer = $('<div id="fancybox-outer"></div>')
            .append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>')
            .appendTo(wrap);

        outer.append(
            content = $('<div id="fancybox-content"></div>'),
            close = $('<a id="fancybox-close"></a>'),
            title = $('<div id="fancybox-title"></div>'),

            nav_left = $('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'),
            nav_right = $('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')
        );

        close.click($.fancybox.close);
        loading.click($.fancybox.cancel);

        nav_left.click(function (e) {
            e.preventDefault();
            $.fancybox.prev();
        });

        nav_right.click(function (e) {
            e.preventDefault();
            $.fancybox.next();
        });

        if ($.fn.mousewheel) {
            wrap.bind('http://www.harley-davidson.com/etc/designs/h-d/mousewheel.fb', function (e, delta) {
                if (busy) {
                    e.preventDefault();

                } else if ($(e.target).get(0).clientHeight == 0 || $(e.target).get(0).scrollHeight === $(e.target).get(0).clientHeight) {
                    e.preventDefault();
                    $.fancybox[delta > 0 ? 'prev' : 'next']();
                }
            });
        }

        if (!$.support.opacity) {
            wrap.addClass('fancybox-ie');
        }

        if (isIE6) {
            loading.addClass('fancybox-ie6');
            wrap.addClass('fancybox-ie6');

            $('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || '') ? 'javascript:void(false)' : 'about:blank') + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(outer);
        }
    };

    $.fn.fancybox.defaults = {
        padding: 10,
        margin: 40,
        opacity: false,
        modal: false,
        cyclic: false,
        scrolling: 'auto',	// 'auto', 'yes' or 'no'

        width: 560,
        height: 340,

        autoScale: true,
        autoDimensions: true,
        centerOnScroll: false,

        ajax: {},
        swf: { wmode: 'transparent' },

        hideOnOverlayClick: true,
        hideOnContentClick: false,

        overlayShow: true,
        overlayOpacity: 0.7,
        overlayColor: '#777',

        titleShow: true,
        titlePosition: 'float', // 'float', 'outside', 'inside' or 'over'
        titleFormat: null,
        titleFromAlt: false,

        transitionIn: 'fade', // 'elastic', 'fade' or 'none'
        transitionOut: 'fade', // 'elastic', 'fade' or 'none'

        speedIn: 300,
        speedOut: 300,

        changeSpeed: 300,
        changeFade: 'fast',

        easingIn: 'swing',
        easingOut: 'swing',

        showCloseButton: true,
        showNavArrows: true,
        enableEscapeButton: true,
        enableKeyboardNav: true,

        onStart: function () { },
        onCancel: function () { },
        onComplete: function () { },
        onCleanup: function () { },
        onClosed: function () { },
        onError: function () { }
    };

    $(document).ready(function () {
        $.fancybox.init();
    });

})(jQuery);
/*! A fix for the iOS orientationchange zoom bug. Script by @scottjehl, rebound by @wilto.MIT / GPLv2 License.*/(function(a){function m(){d.setAttribute("content",g),h=!0}function n(){d.setAttribute("content",f),h=!1}function o(b){l=b.accelerationIncludingGravity,i=Math.abs(l.x),j=Math.abs(l.y),k=Math.abs(l.z),(!a.orientation||a.orientation===180)&&(i>7||(k>6&&j<8||k<8&&j>6)&&i>5)?h&&n():h||m()}var b=navigator.userAgent;if(!(/iPhone|iPad|iPod/.test(navigator.platform)&&/OS [1-5]_[0-9_]* like Mac OS X/i.test(b)&&b.indexOf("AppleWebKit")>-1))return;var c=a.document;if(!c.querySelector)return;var d=c.querySelector("meta[name=viewport]"),e=d&&d.getAttribute("content"),f=e+",maximum-scale=1",g=e+",maximum-scale=10",h=!0,i,j,k,l;if(!d)return;a.addEventListener("orientationchange",m,!1),a.addEventListener("devicemotion",o,!1)})(this); 
(function ($) {

    var myTimer = false;
    var $globalNavLinks = $("#HD_global_nav a");
    var $subMenuContainer = $("#nav_submenu_container");
    var $subNavs = $("#nav_submenu_container div.subnav");
    var $navOverlay = $("#navOverlay");

    $("header.global-header").on("mouseleave blur", function (e) {
        if ($subMenuContainer.css("display") === "block") {
            myTimer = setTimeout(function () {
                $subMenuContainer.animate({
                    height: "toggle",
                    opacity: "toggle"
                },
                                   "500",
                                   "linear"
                                   );

            }, 400);

        }
        $('#navOverlay').hide();
        $globalNavLinks.removeClass("active");
    });


    $globalNavLinks.hoverIntent(function (e) {
        $(this).focus();
    },
    function (e) {
        $(this).blur();
    });

    $globalNavLinks.on("focus", function (e) {
        clearTimeout(myTimer);
        var index = $(this).parent().index();

        $globalNavLinks.removeClass("active");
        $(this).addClass("active");
        $("#motorcycle_subnav li a.active").removeClass("active");
        $("#motorcycle_subnav li:first-child a").addClass("active");

        $(".motorcycle_container").each(function () { $(this).removeClass("on").hide() });
        $(".motorcycle_container").first().show().addClass("on");

        if ($subNavs.eq(index).children().length == 0) {
            $subMenuContainer.addClass("empty");
            $subMenuContainer.hide();
            $(this).parent().attr("aria-haspopup", "false");
            $('#navOverlay').hide();
        } else {
            $subMenuContainer.removeClass("empty");
            $subNavs.hide().removeClass("on").eq(index).fadeIn().addClass("on");
            $(this).parent().attr("aria-haspopup", "false");

            if ($subMenuContainer.css("display") === "none") {

                $subNavs.eq(index).parent().stop(true, true).delay(250).animate({
                    height: "toggle",
                    opacity: "toggle"
                },
                                                                        "500",
                                                                        "swing"
                                                                        );
                $('#navOverlay').show();
            }

            $(this).keydown(function (e) {
                if (e.keyCode == 40) { //down arrow
                    $subNavs.eq(index).find("a:first").focus();
                    return false;
                }
            });
        }
    });


    $globalNavLinks.on("touchstart", function (e) {
        var index = $(this).parent().index();

        if ($(this).attr("data-clicked") && $(this).hasClass("active") || $subNavs.eq(index).children().length == 0) {
            //element has been tapped (hovered) or has no sub nav
            $(this).attr("data-clicked", "false");
            window.location = $(this).attr("href");
        } else {
            //element has not been tapped (hovered) yet
            e.preventDefault();
            $globalNavLinks.attr("data-clicked", "false");
            $(this).attr("data-clicked", "true");
            $(this).trigger("focus");
        }
    });


    $globalNavLinks.on("blur", function (e) {
        var index = $(this).parent().index();
        $subNavs.eq(index).parent().stop(true, true);

        if ($subNavs.eq(index).children().length == 0) {
            $(this).parent().attr("aria-haspopup", "false");
        } else {
            $(this).parent().attr("aria-haspopup", "true");
        }
    });

    $globalNavLinks.on("click", function (e) {
        if ($(this).attr("target") == "_blank") {
            e.preventDefault();
            $(this).removeClass("active");
            $(this).trigger("blur");
            window.open($(this).attr("href"), $(this).attr("target"));
        }
    });

    $("#nav_submenu_container .subnav a").keydown(function (e) {
        var $subNavOn = $("#nav_submenu_container div.subnav.on");
        var subIndex = $subNavOn.index();

        if (e.shiftKey && e.keyCode == 9) { //shift+tab
            if ($subNavOn.find("a:first").is(":focus")) {
                $("#HD_global_nav ul.primary-level li.yuimenubaritem a").eq(subIndex).focus();
            }
        }
        else if (e.keyCode == 9) {
            if ($subNavOn.find("a:last").is(":focus")) { //|| $(".motorcycle_container.on a:last").is(":focus")
                $("#HD_global_nav ul.primary-level li.yuimenubaritem a").eq(subIndex - 1).focus();
            }
        }
    });

    $("#motorcycle_subnav a").hoverIntent(function (e) {
        $(this).focus();
    }, function (e) {
        //
    }).on("focus", function (e) {
        var index = $(this).parent().index();
        var $motoContainer = $("div.motorcycle_container");
        $("#motorcycle_subnav a").removeClass("active");
        $(this).addClass("active");

        if (!$motoContainer.eq(index).hasClass("on")) {
            $motoContainer.removeClass("on").hide().eq(index).fadeIn().addClass("on");
        }

        $(this).keydown(function (e) {
            if (e.keyCode == 40) { //down arrow
                $motoContainer.eq(index).find("a:first").focus();
                return false;
            }
        });
    });


    $(".motorcycle_container a").keydown(function (e) {
        var $motoSubNavOn = $(".motorcycle_container.on");
        var motoSubIndex = $motoSubNavOn.index();

        if (e.shiftKey && e.keyCode == 9) { //shift+tab
            if ($motoSubNavOn.find("a:first").is(":focus")) {
                $("#motorcycle_subnav li a").eq(motoSubIndex).focus();
            }
        }
        else if (e.keyCode == 9) {
            if ($motoSubNavOn.find("a:last").is(":focus")) {
                $("#motorcycle_subnav li a").eq(motoSubIndex - 1).focus();
            }
        }
    });


    if ($("html.touch")) {
        $("#touch_close a").on("click", function (e) {
            e.preventDefault();
            $subMenuContainer.hide();
            clearTimeout(myTimer);
            $globalNavLinks.removeClass("active").attr("data-clicked", "false");
            $navOverlay.hide();
        });
    }


    $(document).on('click', '.badges a.generic-overlay', function (e) {
        $(this).removeClass("active");
        $(this).trigger("blur");
        clearTimeout(myTimer);
        $("#nav_submenu_container .subnav.on").removeClass("on").hide();
        $('#navOverlay').hide();
        $globalNavLinks.removeClass("active");
    });

    //#region ************* navigation.js :: global navigation highlighting based on page landing / navigation ********************
    $(document).ready(function () {
        var path = window.location.pathname,
            href = window.location.href,
            pathArr = path.split(/\/|\.(?=\w+$)/gi),
            $hdGlobalNav = $('#HD_global_nav, .company-sub-nav'),
            navHighlightClassName = 'highlight',
            navIdentifierAttrName = 'data-menu';

        if (pathArr && pathArr.length > 0 && $hdGlobalNav.length > 0) {
            var $hdGlobalNavMenuItems = $("a[" + navIdentifierAttrName + "!='']", $hdGlobalNav);
            if ($hdGlobalNavMenuItems.length > 0) {
                $hdGlobalNavMenuItems.removeClass(navHighlightClassName);
                var $currentNavItem = $hdGlobalNavMenuItems.filter(function (i, x) {
                    var $x = $(x),
                        xNavIdentifier = $x.attr(navIdentifierAttrName),
                        ppathArr = $.grep(pathArr, function (y, j) { 
                            return y.toLowerCase() === xNavIdentifier.toLowerCase() || xNavIdentifier.toLowerCase().indexOf(href.toLowerCase()) === 0;
                        });
                    return ppathArr.length > 0;
                });
                if ($currentNavItem.length > 0) {
                    var $highlightNavItem = $currentNavItem.last(),
                        $highlightNavItemParent = $highlightNavItem.parentsUntil('.company-sub-nav').filter('li').find('a:first');
                    $highlightNavItem.addClass(navHighlightClassName);
                    $highlightNavItemParent.addClass(navHighlightClassName);
                }
            }
        }
    });
    //#endregion
})(jQuery);
var iframeFlager=false;
(function ($) {
    $.fn.redraw = function () {
        //This extension will trigger 'has layout' in the browser to force recalculation / redrawing of the current page.  This addresses issues with changes in dynamic DOM layout (mainly applies to IE8 and below).  CMC 2013-10-16 11:06:40 AM
        var $body = $('body'), bodyZoom = $body.css('zoom'); $body.css({ 'zoom': 0, 'zoom': bodyZoom }); return this;
    };
    $.fn.getSize = function (includeMargin) {
        //This extension will return the size (width, height) for an element even if it is hidden (display:none;).  If it is not hidden, the outer width / height of the extended element is returned.
        var $this = $(this), $context = $context && $context.jquery ? $context : $this, $hiddenThis = $this.is(':visible') ? $this : $this.clone().appendTo('body'), width = $hiddenThis.outerWidth(includeMargin) || 0, height = $hiddenThis.outerHeight(includeMargin) || 0, size = { 'width': width, 'height': height };
        if ($this !== $hiddenThis) { $hiddenThis.remove(); }
        return size;
    };
    $.fn.getMatchedCSSRule = function (selector, firstMatchOnly) {
        //Find the associated CSS stylesheet styles (non-inlined)
        try {
            if (selector && selector.toString().length > 0) {
                firstMatchOnly = firstMatchOnly === true ? firstMatchOnly : false;
                var returnArray = firstMatchOnly ? null : [],
                    windowRules = window.getMatchedCSSRules ? window.getMatchedCSSRules(this[0]) : null,
                    windowRulesArray = windowRules ? Array.prototype.slice.apply(windowRules) : null;
                for (var i = 0, j = document.styleSheets, k = j.length; i < k; i++) {
                    var sSheet = j[i], sHref = sSheet.href || window.location.href;
                    var sRules = sSheet.cssRules ? sSheet.cssRules : sSheet.rules;
                    if (sRules && sRules.length > 0) {
                        for (var l = 0, m = sRules.length; l < m; l++) {
                            var rRule = sRules[l];
                            if (rRule) {
                                var rSelectorText = rRule.selectorText;
                                if (rSelectorText && rSelectorText.toLowerCase() == selector) {
                                    if (firstMatchOnly) { return rRule; } else { returnArray.push(rRule); }
                                }
                            }
                        }
                    }
                }
                if (windowRulesArray && returnArray) {
                    var returnSelectorsArray = $.map(returnArray, function (e, i) { return e.selectorText; });
                    returnArray = returnArray.concat($.grep(windowRulesArray, function (e, i) { return returnSelectorsArray.indexOf(e.selectorText) === -1; }));
                }
                return returnArray;
            } else { return null; }
        } catch (ex) {
            return null;
        }
    };
}(jQuery));

//#region ************* generic overlay (fancybox) ********************
var genericOverlay = (function ($) {
    /* 
     *  For generic overlay used across the site.
     *  usage : <a href="http://www.harley-davidson.com/etc/designs/h-d/file_to_be_loaded.html" class="generic-overlay" >...
     *          <a href="http://www.harley-davidson.com/etc/designs/h-d/file_to_be_loaded.html"><span class="generic-overlay">...
    */
    var init = function (el, opts) {
        var $el = el || $('.generic-overlay'),
            opts = opts || { width: 'auto', height: 'auto', padding: 5, autoDimensions: true, titlePosition: 'inside', centerOnScroll: true },
            closeButtonText = opts.closeButtonText ? opts.closeButtonText.toString() : '',
            fnOptsOnStart = opts.onStart,
            fnOptsOnComplete = opts.onComplete,
            fnOptsOnClosed = opts.onClosed,
            $fancyboxWrap = $('div#fancybox-wrap'),
            $fancyboxWrapIE = $('div#fancybox-wrap.fancybox-ie'),
            $fancyboxTitle = $('div#fancybox-title'),
            $fancyboxContent = $('div#fancybox-content'),
            $fancyboxClose = $('a#fancybox-close'),
            CONST_CSS_DISPLAY = 'display',
            CONST_DATA_HAS_TITLE = 'data-has-title',
            arrFancyboxClassVariationObjs = [ //array of objects with class names that if present on the trigger element, will be applied to div#fancybox-wrap to create fancybox style variations, in addition to modifying the default fancybox init options.  CMC 2013-12-03 11:58:48 AM
                { vClass: 'modal', vOpts: { padding: 12 } }
            ];

        opts.onComplete = function () {
            if (typeof fnOptsOnComplete === 'function') { fnOptsOnComplete.apply(this, arguments); }
            if (typeof loadEliteDealersDetailsInit === 'function') { loadEliteDealersDetailsInit(); } /* ironelite page lazyload issue fix */
            if (typeof jsScrollPaneAdd === 'function') { jsScrollPaneAdd.apply(this); };
            if (typeof loadhardcandycustom === 'function') { loadhardcandycustom.apply(this); };

            //initialize tabs for content loaded inside fancybox
            if ($('.tabs').length > 0 && $('#tabbedContent').length > 0) {
                var $tabs = $('.tabs-wrapper .tabs li').tabscomp('#tabbedContent.tabbed-content > div',
                    {
                        activeTabClassName: 'active',
                        tabsArgs: {
                            trackLastTab: true,
                            trackHeightDefect: $('html').hasClass("lt-ie9"),
                            fullSpanTabs: false
                        }
                    });
            }
            if ($fancyboxWrapIE && $fancyboxWrapIE.length > 0) { $.fancybox.resize(); }
            if(iframeFlager){
                $('#fancybox-wrap, #fancybox-outer, #fancybox-content').css({'height':'660','width':'990'});
            }else
            {
                $('#fancybox-wrap, #fancybox-outer, #fancybox-content').css('width', 'auto'); 
            }
            $.fancybox.center(true, false);
        };
        opts.onClosed = function () {
            if (typeof fnOptsOnClosed === 'function') { fnOptsOnClosed.apply(this, arguments); }
            if ($fancyboxTitle) { $fancyboxTitle.css(CONST_CSS_DISPLAY, 'none'); }
        };
        if ($el.length > 0) {
            $el.each(function (i, x) {
                var $x = $(x), $overlay, fnGetFancyboxClassVariationObjs = function ($context) { return $context ? $.grep(arrFancyboxClassVariationObjs, function (j, y) { return $context.hasClass(j.vClass); }) : []; }, aarrFancyboxClassVariationObjs = fnGetFancyboxClassVariationObjs($x);
                opts.onStart = function () {
                    if (typeof fnOptsOnStart === 'function') { fnOptsOnStart.apply(this, arguments); }
                    var $$x = $x, aarrFancyboxClassVariationObjs = fnGetFancyboxClassVariationObjs($$x);
                    if (aarrFancyboxClassVariationObjs.length > 0) { $fancyboxWrap.addClass($.map(aarrFancyboxClassVariationObjs, function (j, y) { return j.vClass; }).join(' ')); } else { $fancyboxWrap.removeClass(function () { return $(this).prev().attr('class'); }); }
                    if ($$x.attr('title')) { $fancyboxContent.attr(CONST_DATA_HAS_TITLE, ''); } else { $fancyboxContent.removeAttr(CONST_DATA_HAS_TITLE); }
                    if (closeButtonText.length > 0) { $fancyboxClose.text(closeButtonText); }
                };
                if (aarrFancyboxClassVariationObjs.length > 0) { $.each(arrFancyboxClassVariationObjs, function (j, y) { if (typeof y.vOpts === 'object') { for (var yvOpt in y.vOpts) { opts[yvOpt] = y.vOpts[yvOpt]; } } }); }
                if ($x.prop("tagName") == "A") { 
					$overlay = $x; 
					} 
				else if($x.closest('a').length > 0){
    				$overlay=$x.closest('a');
					}
				else if($x.find('a').length > 0){
    				$overlay=$x.find('a').first();
					}
                var href = $overlay.attr('href') || '';
                if (href.length > 0) {
                    opts.href = href;
                    if (href.indexOf('#') === 0 && href.length > 1) {
                        opts.type = 'inline';
                        if (opts.autoDimensions && opts.autoDimensions === true) {
                            //Fancybox autoDimensions bug fix:  Fix for fancybox autoDimensions of element that is inline and initially hidden with a width value defined in % unit and stabilization of elements that do not have a width defined.  CMC 2013-11-26 11:39:41 PM
                            var $fancyTarget = $(href),
                                fancyTargetCSSRule = $fancyTarget.getMatchedCSSRule(href, true),
                                hasWidth = false;
                            if (fancyTargetCSSRule && fancyTargetCSSRule.style) {
                                var styleWidth = fancyTargetCSSRule.style.width || '';
                                if (styleWidth.length > 0 && styleWidth[styleWidth.length - 1] === '%') {
                                    var fancyTargetSize = $fancyTarget.getSize(true);
                                    if (fancyTargetSize.width > 0) { $fancyTarget.css('width', fancyTargetSize.width + 'px'); hasWidth = true; }
                                }
                            }
                            if (!hasWidth && $fancyTarget.width() === 0) {
                                $fancyTarget.css('width', '60%'); //set a default width of 60%
                                var fancyTargetSize = $fancyTarget.getSize(true);
                                if (fancyTargetSize.width > 0) { $fancyTarget.css('width', fancyTargetSize.width + 'px'); hasWidth = true; }
                            }
                        }
                    } else {
                        if($overlay.find(".iframe").length >=1){
                            opts.fitToView = false;
                            opts.href =  $overlay.attr('href');
                            opts.type = 'iframe';
                            opts.scrolling= 'no';
                            iframeFlager = true;
                        }
                        else{
                            href = $overlay.attr('href').split('.html')[0] + "/jcr:content/par.html" || '';
                            opts.fitToView = false;
                            opts.minWidth = 800;
                            opts.href = href;
                            opts.type = 'ajax';
                        }
                    }
                }
                $overlay.fancybox(opts);
            });
        }
    };
    return { init: init };
}(jQuery));
//#endregion

//#region ************* generic popup window ********************
var genericPopup = (function ($) {
    var init = function (el, config) {
        var $el = el || $('.genericPopup'), elSelector = $el.selector, elConfig = $.extend(true, {}, config); /* NOTE:  Create a CLONED configuration object (elConfig) so that the configuration object is unique for each.  CMC 2014-03-05 04:46:49 PM */
        if (elSelector.length > 0) {
            var fnSetDefaultProps = function (defaultObj, contextObj) {
                var _undefined = typeof undefined, _object = typeof {}, _function = typeof function () { }, _string = typeof '';
                if (typeof (defaultObj) === _object) {
                    if (typeof (contextObj === _object)) {
                        for (var dProp in defaultObj) {
                            var dPropVal = defaultObj[dProp], dPropType = typeof dPropVal,
                                cPropVal = contextObj[dProp], cPropType = typeof cPropVal;
                            if (cPropType === _undefined) {
                                contextObj[dProp] = dPropVal;
                            } else {
                                if (dPropType === _object) {
                                    fnSetDefaultProps(dPropVal, cPropVal);
                                } else {
                                    if (cPropType === _function && dPropType !== _function) {
                                        /* NOTE:  Using this here means that the HTML (dProp === 'html') is getting loaded automatically, we want this to be called at the time of trigger.  CMC 2014-03-03 04:23:44 PM
                                        *  var fnResult = contextObj[dProp].apply(contextObj);
                                        *  if (typeof fnResult === dPropType) {
                                        *      contextObj[dProp] = fnResult;
                                        *  } else {
                                        *      contextObj[dProp] = dPropVal;
                                        *  }
                                        */
                                    } else {
                                        contextObj[dProp] = dPropVal;
                                    }
                                }
                            }
                        }
                    } else {
                        contextObj = defaultObj;
                    }
                }
            };
            $(document).on('click', elSelector, elConfig, function (event) {
                event.preventDefault();
                var $evTarget = $(this), $evTargetParent, evData = event.data;
				if($evTarget.closest('a').length > 0){
    				$evTargetParent = $evTarget.closest('a');
					}
				else if($evTarget.find('a').length > 0){
    				$evTargetParent = $evTarget.find('a').first();
					}

                (function configDefault(cconfig) {
                    var defaults = {
                        width: $evTarget.attr('data-width') || $evTargetParent.attr('data-width') || 598,
                        height: $evTarget.attr('data-height') || $evTargetParent.attr('data-height') || 540,
                        urls: { content: function () { return $evTarget.attr('href') || $evTargetParent.attr('href') || '' } },
                        html: ''
                    };
                    fnSetDefaultProps(defaults, cconfig);
                    return cconfig;
                })(evData);

                var eventURLs = (typeof evData.urls.content === 'function' ? evData.urls.content.apply(evData) : evData.urls.content).toString(),
                    eventHTML = (typeof evData.html === 'function' ? evData.html.apply(evData, [eventURLs]) : evData.html).toString(),
                    url = eventHTML && eventHTML.length > 0 ? '' : eventURLs,
                    properties = 'width=' + evData.width + ',height=' + evData.height + ',scrollbars=yes,top=120,left=250,resizable=yes,toolbar=no,location=no,menubar=no,directories=no,status=no',
                    w = window.open(url, '', properties);
                if (eventHTML && eventHTML.length > 0) {
                    w.document.write(eventHTML);
                    w.document.close();
                }
            });
        }
    };
    return { init: init };
})(jQuery);
//#endregion

$(document).ready(function () {
    genericOverlay.init(null, { width: 'auto', height: 'auto', padding: 5, autoDimensions: true, titlePosition: 'inside', centerOnScroll: true, closeButtonText: typeof fancybox_closebtntext !== 'undefined' ? fancybox_closebtntext : '' });
    genericPopup.init($('.genericPopup'), {});
    if (typeof (window.printpreviewinitialized) === 'undefined' || window.printpreviewinitialized === false) {
        window.printpreviewinitialized = true;
        var isLTENativeIE8Browser = function () { var n = document, t = n.createElement('svg'), i = t.cloneNode(!1), r = '\v' == "v" && i.outerHTML === '<:svg></:svg>', t = null, i = null; return r }(); //Ugly, but necessary in here since native IE8 (and below) has issues when it comes to cloning operations and HTML5 elements.  CMC 2014-03-14 03:36:05 PM
        var html5ElementsVal = 'abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|embed|figcaption|figure|footer|header|keygen|main|mark|math|menu|menuitem|meter|nav|output|progress|rp|rt|ruby|section|source|summary|template|svg|time|track|video|wbr';
        var fnSanitizeNativeIE8HTML5Html = function (htmlString) {
            /*  fnSanitizeNativeIE8HTML5Html:  This method fixes issues with native IE8's (nIE8's) inability to cope with HTML5 elements
            *   in a cloned DOM object.  nIE8 adds namespaced colons and in some cases escapes some HTML5 elements.
            *   Example 1:  <section>...</section> becomes <:section>...</section> in a cloned DOM object in nIE8.
            *   Example 2:  <section>...</section> becomes &lt;:section&gt;...</SECTION> in a cloned DOM object in nIE8.
            *   This method will unescape these escaped sequences, and also remove the stray namespacing colon (:) that nIE8
            *   adds to HTML5 elements.  CMC 2014-03-17 01:46:35 PM
            */
            var returnValue = '', htmlString = htmlString ? htmlString.toString() : '';
            if (htmlString.length > 0) {
                htmlString = htmlString.replace(/\&lt\;\:/gim, '<').replace(/\&gt\;/gim, '>');
                var html5SanitizationRegExp = new RegExp('(\\<\\/?)\\:(' + html5ElementsVal + ')(?=\\b)', 'gim');
                if (html5SanitizationRegExp.test(htmlString)) {
                    html5SanitizationRegExp.lastIndex = 0;
                    returnValue = htmlString.replace(html5SanitizationRegExp, function (rMatchVal) {
                        html5SanitizationRegExp.lastIndex = 0;
                        var rMatch = html5SanitizationRegExp.exec(rMatchVal) || [];
                        return (rMatch.length === 3) ? rMatch.slice(1, rMatch.length).join('') : '';
                    });
                }
            }
            return returnValue;
        };
        genericPopup.init($('.print-preview'), {
            fnGetPrintPreviewStyleTag: function () {
                var $links = $('link').filter(function (i, x) { var $x = $(x), xAttrHref = $x.attr('href') || '', xAttrCheckRegExp = /(clientlibs\.common|global)(?:\.min)?\.css$/i; return (xAttrHref.length > 0 && xAttrCheckRegExp.test(xAttrHref)); });
                if ($links.length > 0) {
                    var $link = $links.first(), linkHRef = $link.attr('href'), printPreviewRegExp = /(clientlibs\.common)(?:\.min)?\.css$/i, isClientLibs = printPreviewRegExp.test(linkHRef), printPreviewCSSHRef = '';
                    if (isClientLibs) {
                        printPreviewCSSHRef = linkHRef.replace(printPreviewRegExp, 'clientlibs.common/css/print-preview.css'/*tpa=http://www.harley-davidson.com/etc/designs/h-d/clientlibs.common/css/print-preview.css*/);
                    } else {
                        printPreviewRegExp = /global.css$/gim;
                        printPreviewCSSHRef = linkHRef.replace(printPreviewRegExp, 'Unknown_83_filename'/*tpa=http://www.harley-davidson.com/etc/designs/h-d/print-preview.css*/);
                    }
                    return $('<link href="' + printPreviewCSSHRef + '" rel="stylesheet" />');
                } else {
                    return null;
                }
            },
            fnGetPrintPreviewHTML: function (titleConfig, selector) {
                var myConfig = typeof titleConfig === 'undefined' ? { 'title': 'PressRelease', 'close': 'Close', 'print': 'Print' } : titleConfig;
                var $html = $($('html').get(0).cloneNode(true)),
                    $printPreviewStyleTag = this.fnGetPrintPreviewStyleTag(),
                    $htmlHeader = $html.find('header').eq(0),
                    $bodywrap = $htmlHeader.siblings('.bodywrap').eq(0),
                    $bodywrap = $bodywrap.length > 0 ? $bodywrap : $('<div class="bodywrap">'),
                    $htmlFooter = $html.find('footer').eq(0),
                    returnValue = '';

                //$html.find('.bodywrap.tmpl_article').append('<div class="fullsize-divider" style="background: transparent url(data:image/gif;base64,R0lGODlhAgAEAIABAGZmZv///yH5BAEAAAEALAAAAAACAAQAAAIEBIJhBQA7) repeat-x scroll 0 0 !important;"></div>');
                $html.find('body').addClass('print-preview-mode');
                $html.find('.page-wrap').addClass('width100');
                $html.find('.bodywrap.tmpl_article').addClass('print-preview-mode');
                $htmlFooter.addClass('width98');
                $html.find('.wrap .container > h2:first').html(myConfig.title);
                $html.find('a.print-preview-hide, a.print-preview').each(function (i, x) { $(x).replaceWith(''); });
                if ($printPreviewStyleTag) { $printPreviewStyleTag.appendTo($html.find('head:first')); }
                if ($htmlHeader) {
                    var $replacementHeader = $('<header class="global-header"><table class="popUpHeader section" style="width:100%;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td style="width: 22px; min-width: 22px;"><img width="22" height="17" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAARCAAAAAD82gNfAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNS4xIFdpbmRvd3MiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RUFGQ0ZDMEMyOUI3MTFFM0JFMjBEOEJDNkFGNzhDMDgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RUFGQ0ZDMEQyOUI3MTFFM0JFMjBEOEJDNkFGNzhDMDgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpFQUZDRkMwQTI5QjcxMUUzQkUyMEQ4QkM2QUY3OEMwOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpFQUZDRkMwQjI5QjcxMUUzQkUyMEQ4QkM2QUY3OEMwOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlKEhtEAAAD0SURBVCjPbZAtq8MwFIbze+K20IaYibjYyLqacdVUZVWJGFRcKESUXaipqhhhHCquKRRqAhUzhaqZ6f6G29uvcT+eA+HwHAi8LyL/grbNh/iPliHci8a++/ylpbG2SeWOB2BtHSyane5vkjkHzpkjvKJwJ832XisxZeNQuselxmzUPpQw2DSb+TBDDWVAkOqTNNHpRqJ1awg6XzF1v6F0fii+3CbtLtD5hC9AUDQ0dQVXs/JZ1c+CIKnUbdDqvBDZZ6y8KU7W8/X/Hc4eYg2fdycypuHcEab1Xp2oPheEuSzsSvGjKujyIG7a8Hexx7yCaCv2C7QbkE3Yh2M7AAAAAElFTkSuQmCC" alt="HD logo"></td><td><div class="logo-heading"><h2>HARLEY-DAVIDSON</h2></div></td><td align="right"><a href="javascript:void(0)" onclick="window.print()" id="print">' + (typeof myConfig.print !== 'undefined' ? myConfig.print : 'Print') + '</a><a href="javascript:void(0)" onclick="window.close()" id="close_window">' + (typeof myConfig.close !== 'undefined' ? myConfig.close : 'Close') + '</a></td></tr></tbody></table></header>');
                    $htmlHeader.html($replacementHeader.html());
                }
                if ($htmlFooter) {
                    var $replacementFooter = $htmlFooter.find('.footer-rights-block:first');
                    if ($replacementFooter) {
                        $replacementFooter.find('ul').remove();
                        $htmlFooter.html($replacementFooter.html());
                        $htmlFooter.attr('style', 'background: transparent url(data:image/gif;base64,R0lGODlhAgAEAIABAGZmZv///yH5BAEAAAEALAAAAAACAAQAAAIEBIJhBQA7) repeat-x scroll 0 0 !important;');
                        $htmlFooter.children().first().attr('style', 'padding: 10px !important;');
                    }
                }
                var $selector = (function (selector) { try { return (selector ? $(selector, $html) : null) } catch (ex) { return null; } })(selector);
                if ($selector && $selector.length > 0) { if ($bodywrap.length > 0) { $bodywrap.html($selector[0].outerHTML); } else { $bodywrap = $selector; } }
                var replacementHTML = $htmlHeader[0].outerHTML + (($bodywrap && $bodywrap.length > 0) ? $bodywrap[0].outerHTML : '') + $htmlFooter[0].outerHTML;
                $html.find('body').html(replacementHTML);

                /* NOTE:  We need to ensure that print-preview.css is the last referenced stylesheet in the document to ensure the print styles are cascaded properly.  CMC 2014-03-20 02:21:49 PM */
                var $printPreviewCSSLink = $('link[href$="Unknown_83_filename"/*tpa=http://www.harley-davidson.com/etc/designs/h-d/print-preview.css*/]', $html).eq(0), $lastCSSLink = $('link[rel="stylesheet"]', $html).eq(-1);
                if (!($printPreviewCSSLink.is($lastCSSLink))) { $printPreviewCSSLink.insertAfter($lastCSSLink); }

                returnValue = $html[0].outerHTML || '';
                if (isLTENativeIE8Browser) {
                    var html5ShivScriptArr = html5ElementsVal.split('|'), html5ShivScriptVal = '';
                    for (var i = 0, j = html5ShivScriptArr, k = j.length; i < k; i++) { html5ShivScriptVal += 'document.createElement("' + j[i] + '");'; }
                    var html5ShivString = '<script type="text/javascript">' + html5ShivScriptVal + '<\/script>';
                    returnValue = html5ShivString + fnSanitizeNativeIE8HTML5Html(returnValue);
                }
                return returnValue;
            },
            urls: { content: '' },
            html: function (selector) {
                if (window.location.href.indexOf('leadership') > -1) {
                    var titleTXT = $('.container .primary-head').html(),
                        closeTXT = (typeof pressReleaseClose !== 'undefined' ? pressReleaseClose : '').toString(),
                        printTXT = (typeof pressReleasePrint !== 'undefined' ? pressReleasePrint : '').toString();
                } else {
                    var titleTXT = (typeof pressReleaseTitle !== 'undefined' ? (pressReleaseTitle.length === 0 ? 'PRESS RELEASE' : pressReleaseTitle) : '').toString(),
                        closeTXT = (typeof pressReleaseClose !== 'undefined' ? pressReleaseClose : '').toString(),
                        printTXT = (typeof pressReleasePrint !== 'undefined' ? pressReleasePrint : '').toString();
                }

                return this.fnGetPrintPreviewHTML({
                    'title': titleTXT.length > 0 ? titleTXT : '',
                    'close': closeTXT.length > 0 ? closeTXT : 'Close',
                    'print': printTXT.length > 0 ? printTXT : 'Print',
                }, selector);
            }
        });
    }
});


$("document").ready(function() {

    // Code for fetching locale JSON data and popilate dropdown values at the footer 
	var locale;	
	
	var pageUrl = window.location.href;
	if(pageUrl.indexOf("/content/h-d/") != -1){
	  pageUrl = pageUrl.split("/");
	  locale = pageUrl[5];
	}
	else{
	  pageUrl = pageUrl.split("/");
	  locale = pageUrl[3];
	}	
    var jsonURL = '';
    if ($('#externalSite')[0]) {
        jsonURL = $('#externalSite').attr('data-lang-url') + '/content/h-d/'+currentLocale+'/footer/switch-countries/jcr:content/par/switchcountries.json';
    } else {
        jsonURL = '/content/h-d/'+locale+'/footer/switch-countries/jcr:content/par/switchcountries.json';
    }
    $.ajax({
        url:jsonURL,
        success: function(data) {
             console.log(data)
            var options="";
            for(var i =0; i<data.localecode.length;i++) {
                options += '<option value="'+data.pageurl[i]+'">'+data.localenames[i]+'</option>';
            }
            $('#sel_locale').append(options);
        }

    });
});

