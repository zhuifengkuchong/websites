var BrightcovePlayer = {};

//Check page protocol and load the correct includes
BrightcovePlayer.secure = (window.location.protocol === 'https:');
var brightcoveUrlMain = "../../../../../../../admin.brightcove.com/js/BrightcoveExperiences.js"/*tpa=http://admin.brightcove.com/js/BrightcoveExperiences.js*/;

if(BrightcovePlayer.secure) {
	brightcoveUrlMain = "https://sadmin.brightcove.com/js/BrightcoveExperiences.js";
}
$.getScript(brightcoveUrlMain);

/*
 * Data for player
 */
BrightcovePlayer.playerData = {
	"playerID" : brcPlayerID,
    "playerKey" : brcPlayerKey,
    "width" : "750",
    "height" : "360"
};

BrightcovePlayer.playerParams = HD.Config.Brightcove.playerParams ?
								HD.Config.Brightcove.playerParams :
								'<param name="playerID" value="' + BrightcovePlayer.playerData.playerID + '" /><param name="playerKey" value="' + BrightcovePlayer.playerData.playerKey + '" />';

/*
 * Template for the player object - will populate it with data using markup()
 */
BrightcovePlayer.playerTemplate = '<div style="display:none"></div><object id="myExperience" class="BrightcoveExperience"><param name="bgcolor" value="#FFFFFF" /><param name="width" value="{{width}}" /><param name="height" value="{{height}}" /><param name="isVid" value="true" /><param name="isUI" value="true" /><param name="dynamicStreaming" value="true" /><param name="@videoPlayer" value="ref:{{videoID}}" /><param name="includeAPI" value="true" /><param name="addRelatedVideos" value="false" /><param name="backgroundimageurl" value="{{image}}" /><param name="linkBaseURL" value="{{videoShareURL}}" /><param name="templateLoadHandler" value="HD.util.Analytics.BrightcoveLoader.onTemplateLoad"; /><param name="wmode" value="transparent" /><param name="autoStart" value="true" /><param name="templateReadyHandler" value="BrightcovePlayer.onTemplateReady" />' + BrightcovePlayer.playerParams;

//Add the secure parameter if needed
if(BrightcovePlayer.secure) {
	BrightcovePlayer.playerTemplate += '<param name="secureConnections" value="true" />';
}

BrightcovePlayer.playerTemplate += "</object>";

/*
 * Function to add the player
 */
BrightcovePlayer.addPlayer = function (container, idx) {
	var gallery = BrightcovePlayer.gallery,
		trackingId = gallery.data[ idx ].trackingId,
		trackingCaption = gallery.data[ idx ].caption,
		tracked = false,
		trackingTag = ( typeof gallery.data[ idx ].trackingTag === 'undefined' ) ? '' : gallery.data[ idx ].trackingTag;
	if ( !tracked ) {
		try {
			if ( trackingId ) {
				setVideoLoadAnalytics(trackingId, trackingTag, trackingCaption, true);
                tracked = true;
			}
		}
		catch (err) {}
	}
     var videoShareUrl = BrightcovePlayer.gallery.data[idx].videoShareURL,
         videoID;
    if(videoShareUrl.indexOf("referenceid") > -1){
        videoID = components.parseURL(videoShareUrl).params.referenceid;
    }else{
        videoID = videoShareUrl;
        videoShareUrl='http://'+location.host+"/content/h-d/en_US/video.html?referenceid="+videoShareUrl;
    }
       // var videoID = (videoShareUrl.indexOf("referenceid") > -1)?components.parseURL(videoShareUrl).params.referenceid : videoShareUrl;

	var videoImage = BrightcovePlayer.gallery.data[idx].image;
   // var videoShareURL =(videoShareUrl.indexOf("referenceid") > -1)? videoShareURL: location.protocol+'//'+location.host+"/content/h-d/en_US/video.html?referenceid="+videoShareUrl;
	//BrightcovePlayer.playerData.idx = idx;

	//Setup player
	if(window.brightcove !== undefined && typeof(brightcove.createExperiences) != "undefined") {
		var playerHTML = '';

		//Set the videoID to the selected video and populate the player object template
		BrightcovePlayer.playerData.videoID = videoID;
		BrightcovePlayer.playerData.image = videoImage;
		BrightcovePlayer.playerData.videoShareURL = videoShareUrl;
		playerHTML = BrightcovePlayer.markup(BrightcovePlayer.playerTemplate, BrightcovePlayer.playerData);

		//Inject the player code into the DOM
		container.innerHTML = playerHTML;

		//Instantiate the player
		brightcove.createExperiences();

	} else {
		setTimeout(function(){BrightcovePlayer.addPlayer(container, idx);},200);
	}
};

/*
 * Function to remove the player
 */
BrightcovePlayer.removePlayer = function () {
	if(BrightcovePlayer.isPlayerAdded == true) {
	  	BrightcovePlayer.isPlayerAdded = false;
		BrightcovePlayer.experienceModule.unload();
	}
};

/*
 * Player template ready handler
 */
BrightcovePlayer.onTemplateReady = function (id) {

	var gallery = BrightcovePlayer.gallery;
	var trackingId = gallery.data[ BrightcovePlayer.playerData.idx  ].trackingId,
		trackingCaption = gallery.data[ BrightcovePlayer.playerData.idx  ].caption,
		tracked = false,
		trackingTag = ( typeof gallery.data[ BrightcovePlayer.playerData.idx  ].trackingTag === 'undefined' ) ? '' : gallery.data[ BrightcovePlayer.playerData.idx  ].trackingTag;
	if ( !tracked ) {
		try {
			if ( trackingId ) {
				setVideoLoadAnalytics(trackingId, trackingTag, trackingCaption, true);
                tracked = true;
			}
		}
		catch (err) {}
	}

	// Notify HD observers
	var observeable = HD.util.Common.getObservable();
	observeable.notifyObservers('VideoPlayed', gallery.data[ BrightcovePlayer.playerData.idx  ]);
};

/*
 * HTML templating function
 *
*/
BrightcovePlayer.markup = function (html, data) {
    var m;
    var i = 0;
    var match = html.match(data instanceof Array ? /{{\d+}}/g : /{{\w+}}/g) || [];
    while (m = match[i++]) {
        html = html.replace(m, data[m.substr(2, m.length-4)]);
    }
    return html;
};