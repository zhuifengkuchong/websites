
var VideoGallery = {};
VideoGallery.Playlist = function(config) {
	this.config = config;
	Playlist.call(this, config);
};
extend(VideoGallery.Playlist, Playlist);

VideoGallery.Playlist.prototype.populate = function (el, gallery) {
	
	var container = el,
		html = [],
		cls,
		n = gallery.data.length,
		w = (n * (145 + 11)),
		tpl = '<li class="{3}"> \
					<a href="#{4}/{2}" class="thumb" id="video{2}"> \
						{1} \
						<span class="title">{0}</span> \
						<span class="active"> \
							<span>NOW PLAYING</span> \
						</span> \
						<span class="idle"> \
							<span class="playbutton"></span> \
						</span> \
					</a> \
				</li>';
		
	if($('#videoSelect').length && !$('#wrContainer.gallery').length) {
    tpl = '<li class="{3}"> \
          <a href="javascript:void(0)" onclick="$(\'#videoSelect\').val(\'{4}/{2}\').change();" class="thumb" id="video{2}"> \
            {1} \
            <span class="title">{0}</span> \
            <span class="active"> \
              <span>NOW PLAYING</span> \
            </span> \
            <span class="idle"> \
              <span class="playbutton"></span> \
            </span> \
          </a> \
        </li>';
    } 
	
	for( var i=0,item; item=gallery.data[i], i<n; i++ ) {
		cls = i===(n-1) ? 'last' : '';
		cls += i===0 ? ' active' : ' ';
		
		html[html.length] = tpl.format([
			(item.caption=="" ? ' ' :item.caption ), 
			( item.thumb ? '<img src="'+item.thumb+'" alt="'+item.alternateText+'" />' : '<img src=" " alt="'+item.alternateText+'" />' ),
			(i+1).toString(), 
			cls,
			gallery.id
		]);
	}
	
	el.html('<div class="playlist"><ul>'+html.join('')+'</ul></div>'+this.createControls());
	el.find('.playlist ul').css({
		width: w
	});
	
	if( n === 1 || gallery.showPlaylist === false ) {
		el.css( { display: 'none' } );
	} else {
		el.css( { display: 'block' } );
	}
	
	this.setThumbnails(el.find('.playlist li'));
	this.setWrapper(container.find('.playlist ul'));
	this.setContainer(container);
	
	this.bind(container, this);
	this.setButtonVisStates(container, this.getWrapper());
	
	return this;
};

VideoGallery.Playlist.prototype.activate = function (idx) {

	var thumbs = this.getThumbnails(),
		active, ml;
	
	if ( thumbs ){
		active = thumbs.filter('.active');
		active.removeClass('active');
		thumbs.filter(':nth-child('+(+idx + 1)+')').addClass('active');
		/*Gallery footer*/
		if(config.data[idx].videodescriptiontitle){
			$(".footer_title").html(config.data[idx].videodescriptiontitle)
		}
		if(config.data[idx].videodescription){
			$(".about_video").html(config.data[idx].videodescription);
			}
			
		if(config.data[idx].videobtnlabel){
			$(".footer_button .track").attr("href",config.data[idx].videbtncta);
			$(".footer_button .track").find(".cta-button").html(config.data[idx].videobtnlabel);
		}
		/*Gallery footer ends*/
		//this.moveIntoView(idx, active);
	}
};


VideoGallery.Stage = function (config) {
	Stage.call(this, config);
	
	this.wrap = null;
	this.config = config;
	this.setContainer = function (val) {
		this.container = val;
	};
	this.getContainer = function () {
		return this.container;
	};
};
extend(VideoGallery.Stage, Stage);

VideoGallery.Stage.prototype.wrap = null;
VideoGallery.Stage.prototype.activate = function (idx) {
	
	var gallery = this.gallery,
		queue = gallery.data;
	
	if(queue[idx].videoShareURL && BrightcovePlayer) {
		BrightcovePlayer.gallery = gallery;
		BrightcovePlayer.addPlayer(this.stage, idx);
	}
	
	$(document).trigger( 'MP.StageItemChanged', [queue[idx]]);
	
	this.setCurrent(idx);
};

VideoGallery.Stage.prototype.populate = function (el, gallery) {
		
	this.gallery = gallery;
	
	el.parent().attr('class', 'container').addClass('videogallery');
	el.html('<div id="videoPlayerContainer"></div>');
	
	this.stage = el.find('#videoPlayerContainer').get(0);
	
	return this;
};

VideoGallery.Masthead = function( config ) {
	this.config = config;
	
	Masthead.call(this, config);
};
extend(VideoGallery.Masthead, Masthead);
VideoGallery.Masthead.prototype.populate = function( el, config, idx ) {
	idx = typeof( idx ) === 'undefined' ? 0 : idx;
	var tpl = [];
	/*Gallery footer*/
	var description=[];
	var $parent=$(el).parent();
	var tpl_description=$("<div class='video_description'></div>");
	var ftr_button=$("<div class='footer_button'></div>");
	var ftr_wrap=$("<div class='footer_wrap'></div>");
	/*Gallery footer ends*/
	if ( el ) {
		$($parent).find('.footer_wrap').remove();
		tpl[tpl.length] = '<h1>'+ config.title +'</h1>';
		if ( config.showCaption && config.captionPosition === 'top' ) {
			tpl[tpl.length] = '<p class="description"></p>';
		}
		var footNote= config.footerNote;
		if(typeof footNote === 'undefined'){
			tpl[tpl.length] = '<p class="bottom-text"></p>';
		}
		else{
			tpl[tpl.length] = '<p class="bottom-text">'+ config.footerNote +'</p>';
		}
		
		el.html( tpl.join('') );
		this.view.caption = el.find( '.description' );
		this.view.setCaption( config.data[idx].caption );
		this.view.container = el;
		/*Gallery footer*/
		if(config.data[idx].videodescriptiontitle){
			description[description.length]="<p class='footer_title'>"+config.data[idx].videodescriptiontitle+"</p>";
		}
		if(config.data[idx].videodescription){
			description[description.length]="<p class='about_video'>"+config.data[idx].videodescription+"</p>";
			}
		$(tpl_description).html(description.join(''));
		if(config.data[idx].videobtnlabel){
			$(ftr_button).html("<a class='track' href='"+config.data[idx].videbtncta+"' target='_blank'><p><span class='cta-button grey right'>"+config.data[idx].videobtnlabel+"</span></p></a>");
			$(ftr_wrap).append(tpl_description);
			$(ftr_wrap).append(ftr_button);
			$($parent).append(ftr_wrap);
		}
		/*Gallery footer ends*/
	}
		return this;
};

VideoGallery.Masthead.prototype.view = {	
	setCaption: function( val ) {
		this.caption.html( val );
	},
	setLabels: function( val ) {
		this.setCaption( val.caption );
	}
};


