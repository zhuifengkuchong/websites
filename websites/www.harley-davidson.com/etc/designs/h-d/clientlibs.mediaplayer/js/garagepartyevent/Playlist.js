var Playlist = ( function () {

	/*
	* Creates custom events based off of the events array.
	* 
	* e.g. this.events = ['thumbnailClicked', 'bound', 'itemActivated'] would create:
	* onThumbnailClicked, doThumbnailClicked
	* onBound, doBound,
	* onItemActivated, doItemActivated
	*/
	function setupEvents(obj, evts) {
	
		for (var i=0, e; e=evts[i]; i++) {
			var camel = e.replace(/^\w/, function($1) { return $1.toUpperCase(); });
			obj.evtQ[e] = [];
			
			obj['on'+camel] = (function (custom) {
				return function (fn) {
					obj.evtQ[custom][obj.evtQ[custom].length] = fn;
				};
			}(e));
			
			obj['do'+camel] = (function (custom) {
				return function (ctx, params) {
					for( var i=0, fn; fn=obj.evtQ[custom][i++]; ) {
						fn(ctx, params);
					}			
				};
			}(e));
		}
	}
	
	function createControls () {
		return '\
			<a href="#next" class="next">get the next set</a> \
			<a href="#previous" style="display:none;" class="previous">get the previous set</a> \
		\ ';
	}
	
	function bind (container, context) {
	
	
		container.find('.next, .previous').click( (function (pl) {
			
			return function (evt) {
				
				pl.slide(this, function (to, start, end, viewable) {
					
					setButtonVisStates( pl.getContainer(), pl.getWrapper(), to, start, end, viewable );
				
				});
				return false;
			};
		}(context)) );
	}
	
	function setButtonVisStates (container, wrapper, ml, start, end, viewable) {
		
		ml = ml || parseInt(wrapper.css('margin-left'));
		start = start || 0;
		viewable = viewable || wrapper.parent().width();
		end = end || (viewable - wrapper.width());
		
		var previous = container.find('.previous'),
			next = container.find('.next');
		
		previous.show();
		next.show();
		
		if ( viewable >= wrapper.width() ) {
			previous.hide();
			next.hide();
		} else {
			 if ( ml === start ) {
				previous.hide();
			} else if ( ml === end ) {
				next.hide();
			} 
		}
	}
	
	
	return function (config) {
		
		var wrapper, thumbnails, container;
		this.id = config.id;
		this.events = ['thumbnailClicked'];
		this.evtQ = {};
		
		this.createControls = createControls;
		this.bind = bind;
		this.setButtonVisStates = setButtonVisStates;
		
		this.getWrapper = function() { return wrapper; }
		this.getThumbnails = function () { return thumbnails; }
		this.getContainer = function () { return container; }
		this.setWrapper = function (val) { wrapper = val; }
		this.setThumbnails = function (val) { thumbnails = val; }
		this.setContainer = function (val) { container = val; }
	
		
		setupEvents(this, this.events);
	};
}());

Playlist.prototype.slide = function(trigger, callback) {

	var wrapper = this.getWrapper(),
		container = this.getContainer(),
		leftMovement = trigger.className === 'previous',
		ml = parseInt(wrapper.css('margin-left')),
		animTo,
		start = 0,
		viewable = wrapper.parent().width(), 
		end = viewable - wrapper.width();
		
	animTo = leftMovement ? ml + viewable : ml - viewable;
	animTo = animTo > 0 ? 0 : ( animTo < end ? end : animTo);
	
	wrapper.stop().animate({
		
		marginLeft: animTo
		
	}, 700, function () {
	
		callback(animTo, start, end, viewable);
	});
};

Playlist.prototype.activate = function (idx) {

	var thumbs = this.getThumbnails(),
		active;
	
	if ( thumbs ){
		thumbs.filter('.active').removeClass('active');
		active = thumbs.filter(':nth-child('+(+idx + 1)+')');
		active.addClass('active');
		
		this.moveIntoView(idx, active);
	}
};


Playlist.prototype.moveIntoView = function (idx, element) {
	
	function slide (wrapper, target, context) {
		
		wrapper.animate({
			marginLeft: target
		}, 500, (function (ctx){
			return function () {
				ctx.setButtonVisStates(ctx.getContainer(), ctx.getWrapper());
			};
		}(context)));
	}
	
	var container = this.getContainer().find('.playlist'),
		wrapper = this.getWrapper();
		var ww = wrapper.width(),
		cw = container.width(),
		tw = (parseInt(element.outerWidth(true)));
		currentLeft = parseInt(wrapper.css('marginLeft')),
		bounds = { start: 0, end: (cw - ww) };

		var centrePoint = Math.floor((cw / 2) - (tw / 2));
		var targetLeft = (tw * idx),
		delta = centrePoint - (targetLeft + currentLeft),
		target = delta + currentLeft;
		target = (target <= bounds.start && target >= bounds.end) ? target : (target > bounds.start ? bounds.start : bounds.end );
		
	slide( wrapper, target, this );
	
	/* Rather than recalculate the data each time you move, I only want to do it the first time then continue. */
	this.moveIntoView = function (idx, element) {
        if ( location.href.indexOf("http://www.harley-davidson.com/etc/designs/h-d/clientlibs.mediaplayer/js/garagepartyevent/road-glide-reveal.html") === -1 ){
			starcomTrack("no","230669",this,"yes");
        }

		var wrapper = this.getWrapper(),
			currentLeft = parseInt(wrapper.css('marginLeft'));
			var targetLeft = ((parseInt(element.outerWidth(true))) * idx);
			
			var delta = centrePoint - (targetLeft + currentLeft);
			var target = delta + currentLeft;
			target = (target <= bounds.start && target >= bounds.end) ? target : (target > bounds.start ? bounds.start : bounds.end );
			
			
		slide( wrapper, target, this );
		return this;
		
	};
	return this;
}

Playlist.prototype.flush = function () {
	var es = this.evtQ;
	for( var evtId in es ) {
		es[evtId] = [];
	};
};

Playlist.prototype.populate = function (el, gallery) {
	
	var container = el,
		html = [],
		n = gallery.data.length,
		w = (n * ( gallery.thumbwidth || 128 ));
		
	for(var i=0,item; item=gallery.data[i], i<n; i++ ) {
		html[html.length] = '<li><a href="#photos/'+(i+1)+'" class="thumb" style="background-image: url('+item.thumb+');">hi</a></li>';
	}
	
	el.html('<div class="playlist"><ul>'+html.join('')+'</ul></div>'+this.createControls());

	el.find('.playlist ul').css({
		width: w
	});
	
	this.setThumbnails(el.find('.playlist li'));
	this.setWrapper(container.find('.playlist ul'));
	this.setContainer(container);
	
	this.bind(container, this);
	this.setButtonVisStates(container, this.getWrapper());
	
	return this;
};