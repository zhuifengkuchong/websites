var Stage = ( function () {

	function createNextPreviousHTML () {
		var html = ' \
		<a class="left-arrow" href="#"> \
		<span class="arw"></span> \
		<span class="preview"> \
		<img src="" /> \
		<span class="current-count"></span> \
		</span> \
		</a> \
		<a class="right-arrow" href="#"> \
		<span class="arw"></span> \
		<span class="preview"> \
		<img src="{1}" /> \
		<span class="current-count"></span> \
		</span> \
		</a>'; 

		return html;
	}

	function updateNextPrevious (el, idx, queue) {
//alert(el); alert(idx); alert(queue)
		var left = el.find('.left-arrow'),
		right = el.find('.right-arrow'),
		min = 0, max = queue.length - 1,
		prevIdx = (idx === min) ? max : idx - 1;
		nextIdx = (idx === max) ? 0 : idx + 1,
		leftThumb = queue[prevIdx].thumb,
		rightThumb = queue[nextIdx].thumb,
		leftCount = (prevIdx + 1) + '/' + (max + 1),
		rightCount = (nextIdx + 1) + '/' + (max + 1);

        if((window.location.href.indexOf("darkcustom") > -1) && (window.location.href.indexOf("en_US") > -1)) { 
        //    alert('dfd');

        resizeGallery();;
		responsiveGalleryStage();;
        }

		left.find('img').attr('src', leftThumb);
		right.find('img').attr('src', rightThumb);

		left.find('.current-count').html(leftCount);
		right.find('.current-count').html(rightCount);
	}

	function setupEvents(obj, evts) {
//alert(obj); alert(evts);
		for (var i=0, e; e=evts[i]; i++) {

			var camel = e.replace(/^\w/, function($1) { return $1.toUpperCase() });

			obj.evtQ[e] = [];

			obj['on'+camel] = (function (custom) {
				return function (fn) {
					obj.evtQ[custom][obj.evtQ[custom].length] = fn;
				};
			}(e))

			obj['do'+camel] = (function (custom) {
				return function (ctx, params) {
					for( var i=0, fn; fn=obj.evtQ[custom][i++]; ) {
						fn(ctx, params);
					}			
				}
			}(e));
		}
	}

	function bind (el, context) {

		el.find('.right-arrow, .left-arrow')
 			/*.mouseenter(function() {
	 			$(this).animate({
	 				width: 170
	 			}, 100);
	 		})

	 		.mouseleave( function(){
	 			$(this).animate({
	 				width: 45
	 			}, 100);
})*/

.click( (function(st) {

	return function (evt) {

		var diff = $(this).hasClass('left-arrow') ? -1 : 1,
		idx = st.getCurrent() + diff + 1,
		max = st.getQueue().length,
		min = 1,
		idx = (idx < min) ? max : (idx > max ? min : idx); 

		location.hash = st.id + '/' + idx;

		return false;
	};


}(context)));
}

return function (config) {

	var stage, queue = config.data;
	this.current = 0;

	this.id = config.id;
	this.events = ['itemChanged'];
	this.evtQ = {};

	this.flush = function () {
		var es = this.evtQ;
		for( var evtId in es ) {
			es[evtId] = [];
		};

	};

	this.getCurrent = function () { return this.current; };
	this.setCurrent = function (val) { this.current = val; };
	this.getQueue = function () { return queue; };
	this.setQueue = function (val) { queue = val; };
	this.updateNextPrevious = updateNextPrevious;
	this.addNextPreviousButton = function(el, id) { 

		if ( queue.length > 1 ) {
			el.append(createNextPreviousHTML(id)); 
			bind(el, this);
		}
	};

	setupEvents(this, this.events);
};
}());

 if((window.location.href.indexOf("darkcustom") > -1) && (window.location.href.indexOf("en_US") > -1)) {

$("#dc_gallerywrap-inner").swipe({
	swipe:function(event, direction, distance, duration, fingerCount) {
		if(direction=="left"){
			$( "#dc_gallerywrap-inner .right-arrow" ).trigger( "click" );
		}
		if(direction=="right"){
			$( "#dc_gallerywrap-inner .left-arrow" ).trigger( "click" );
		}

		if(direction=="up" || direction=="down"){
			return false;
		}
	}, allowPageScroll:"vertical"
});
function resizeGallery(){
var resizeTimer = null;
	var winWidth = $(window).width();;
	var winHeight = $(window).height();;

		resizeTimer && clearTimeout(resizeTimer); 

		resizeTimer = setTimeout(function() {
			var winNewWidth = $(window).width();;
			var winNewHeight = $(window).height();; 
			responsiveGalleryStage();;
           	if(winWidth!=winNewWidth || winHeight!=winNewHeight){
				responsiveGalleryStage();;
				winWidth = winNewWidth;
				winHeight = winNewHeight;
			}
		}, 200);
}

$(window).load(function(){  
	var resizeTimer = null;
	var winWidth = $(window).width();;
	var winHeight = $(window).height();;
	responsiveGalleryStage();;


	$(window).resize(resizeGallery);

});  

function responsiveGalleryStage(){
	if($(window).width()<768){ ;
		responsiveStage();
        resizeGallery();
	}
	else{
		cleanupResponsiveStage();
	}
}

function responsiveStage(){ ;
	if(!$('#transparent-img-stage').length){
		$(".stage-wrap").append( '<img id="transparent-img-stage" src="../../images/common/gallery/transparent.png"/*tpa=http://www.harley-davidson.com/etc/designs/h-d/clientlibs.mediaplayer/images/common/gallery/transparent.png*//>' );
	}
	var imgWidth = $('#transparent-img-stage').width();

	var imgHeight = imgWidth / (812/509);

	if($(window).width()<480){
		var imgHeight = imgWidth / (812/541);
	}

	if(imgHeight){
		if($(window).width()<480){
			$('#dc_gallerywrap-outer').css('height',imgHeight*2);
			$('.street-bob-content #dc_gallerywrap-outer').css('height',imgHeight*2.1);
			$('.hard-candy-custom-content #dc_gallerywrap-outer').css('height',imgHeight*1.4);
		}
		else{
			$('#dc_gallerywrap-outer').css('height',imgHeight*1.5);
			$('.street-bob-content #dc_gallerywrap-outer').css('height',imgHeight*1.6);
			$('.hard-candy-custom-content #dc_gallerywrap-outer').css('height',imgHeight*1.35);

		}

		$(".stage-wrap").css('height',imgHeight);
		$(".stage").css('height',imgHeight);

		$(".figure").width(imgWidth);;
		$(".figure").height(imgHeight);;

		$(".figure img").width(imgWidth);
		$(".figure img").height(imgHeight);

		$("#model_page_content").height(imgHeight);
	}
}

function cleanupResponsiveStage(){
	$(".figure img").width('');
	$(".figure img").height('');
	$(".figure").width('');
	$(".figure").height('');
	$(".stage").css('height','');
	$('#dc_gallerywrap-outer').css('height','');
	$(".galleryHeader-container").css('height','');
	$(".stage").css('height',"");
	$("#model_page_content").height('');

	if($('#transparent-img-stage').length){
		$('#transparent-img-stage').remove();
	}
}

//alert("your url contains the name franky");
    }



