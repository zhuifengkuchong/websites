var Masthead = function (){};

Masthead.prototype.populate = function( el, config, idx ) {
	
	idx = typeof( idx ) === 'undefined' ? 0 : idx;
	
	if ( el ) {
		el.append( '<h1>Harley-Davidson<sup>&#174;</sup> Motorcycles</h1><h2></h2><h3></h3><p class="description"></p>' );
		this.view.container = el;
		this.view.titlea = el.find( 'h2' );
		this.view.subtitle = el.find( 'h3' );
		this.view.desc = el.find( '.description' );
		this.view.setLabels( config.data[idx] );
	}
	
	return this;
};

Masthead.prototype.view = {
	
	setLabels: function ( data ) {
		this.setSubTitle( data.title, data.url );
		this.setDescription( data.subtitle );
	},
	
	setTitle: function ( val ) {
		this.titlea.html( val );
	},
	
	setSubTitle: function ( val, url ) {
		var btn = ' <a href="{0}" class="button"><span>See Details</span></a>';
		this.subtitle.html( val + btn.format( [url] ) );
	},
	
	setDescription: function ( val, url ) {
		this.desc.html( val );
	}
		
};