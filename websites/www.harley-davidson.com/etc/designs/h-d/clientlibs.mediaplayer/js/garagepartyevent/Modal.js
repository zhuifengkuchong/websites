if ( !MP ) var MP = {};

MP.Modal = (function($) {

	var created = false,
		wrap, content, 
		opened = false, 
		options = {		
			width: 600,
			overlayColor: '#000',
			type: 'inline',
			padding: 6,
			onClosed: function() {
				window.location.hash = '';
			}
		};
	
	function createOverlay () {
		
		var doc = $(document),
			bdy = $(document.body),
			viewport = [ doc.width(), doc.height() ],
			mask = '<div style="width:100%; height:{0}px;" class="overlay"></div>';
		
		bdy.append( mask.format([ viewport[1]]) );
		
		var overlay = ' \
		<div id="modal-wrap"> \
			<div class="content"> \
				<div class="header"></div> \
				<div class="container"></div> \
			</div>'
			+
			'<a href="#" class="close close-wrapper">' + ( window.closeText && closeText.length && closeText || 'Close' ) + '<span class="close"></span></a>'
			+
			//<a href="#" class="close">Close Modal Window</a>
		'</div> \
		\ ';
		
		bdy.append( overlay );
		wrap = $('#modal-wrap');
		created = true;
	}
	
	function open (o) {
		
		var doc = $(document),
			win = $(window),
			center = [(win.width()/2), (win.height()/2)],
			mt = -(wrap.height()/2) + doc.scrollTop(),
			ml = -(o.width/2);
			
		if ( o ) {
			wrap.css({ 
				marginTop: mt,
				marginLeft:  ml,
				width: o.width
			});
		}
		
		wrap.css({ opacity: 1, display: 'block' });
		$('.overlay').css({ opacity: .7, display: 'block' });
		
		opened = true;

		$('html').css('position', 'static');
        $('footer.global-footer').css('position', 'static');
		
		$(document).trigger('MP.ModalOpened');
	}
	
	function close () {
		
		if ( typeof jwplayer !== 'undefined' && jwplayer() ) {
			jwplayer().stop();
		}
		
		$('.overlay').animate({ opacity: 0.01 }, 500, function() {
			content.html('');
			header.html('');
			$(this).hide();
		});
		
		wrap.animate({ opacity: 0.01 }, 500, function() {
			$(this).hide();
		});
		
		open = false;
		
		$('html').css('position', 'relative');
        $('footer.global-footer').css('position', 'absolute');
        
		$(document).trigger('MP.ModalClosed');
	}
	
	function bind () {
	
		var start, mvmt, startCSS, md = false;
		
		wrap.find('.header').mousedown(function (evt) {
		
			start = [evt.pageX, evt.pageY];
			startCSS = [ parseInt(wrap.css('marginLeft')), parseInt(wrap.css('marginTop')) ];
			md = true;
			
			return false;
		});
		
		$(document).mousemove(function (evt) {
		
			if ( md ) {
				var mvmt = [ evt.pageX, evt.pageY ];
				diff = [ ( start[0] - mvmt[0] ), ( start[1] - mvmt[1] )];
				
				wrap.css({
					marginLeft: startCSS[0] - diff[0],
					marginTop: startCSS[1] - diff[1]
				})
			}
			return false;
			
		}).mouseup(function (evt) {
			md = false;
		});
	
		$(document).bind( 'keydown.modal', function (evt) {
		
			if(opened && evt.keyCode === 27) {
				close();
				return false;
			}
		});
		
		$('.close').click( function () {
			close();
			return false;
		});
	}
	
	return {
		
		create: function () {

			createOverlay();
			bind();
			
			return this;
		},
		
		setOptions: function (opts) {
			for(var key in opts) {
				options[key] = opts[key];
			}
			return this;
		},
		
		getContentNode: function() {
			
			if ( !created ) {
				this.create();
			}
			
			if( !content ) {
				content = wrap.find('.container');
				header = wrap.find('.header');
			}
			
			return { content: content, header: header };
		},
		
		open: function (options) {
			
			this.setOptions(options);
			open(options);
			
			this.open = open;
		},
		close: close
		
	};
}(jQuery));