var windowSize;
var originalTitleStyle;
var browserMode = 'new';

var changePage = function() {
				jQuery('.location-search h4').text('Enter City');
				jQuery('.location-search > input').attr('placeholder', 'Entry City');
}

var runMods = function (event) {
    

    // Code to modify spanish site
    if (window.location.href.indexOf("http://ampmspanish.rpahosting.com/js/wp/es.arco.com") > -1) {
		console.log('entering spanish logic');
        // In case we need a 2nd dev working on fixes, use the additional.js script
        var s = document.createElement('script');
        s.setAttribute('src', 'additional.js'/*tpa=http://ampmspanish.rpahosting.com/js/wp/additional.js*/);
        document.body.appendChild(s);

				
				
        // Attach events after query is ready
        jQuery(document).ready(function () {

            windowSize = jQuery(window).width(); console.log('original window size set to: "' + windowSize + '"'); console.log(windowSize); //alert(windowSize)
            originalTitleStyle = jQuery('.slider-caption h2').attr('style'); console.log('original style set to: "' + originalTitleStyle + '"');

            jQuery('.slider-caption a.blue-btn').attr('href', '/gasolina-calidad-top-tier/'); // Fix hyperlink for home page CTA button
            jQuery('div label.grunion-field-label span').html('(Requerida)'); // Change some text
            changeDisclaimerCopy(); // Change disclaimer copy

            // Start page manipulation
            setBackgroundImages();
            setFindAStationTopMenuItem();
            setHomeTitleSpanish();
            setDisclaimerText();
            setZipCodeText();
            setHomeIcon();

            // On Resize
            jQuery(window).resize(function () {
                windowSize = jQuery(window).width(); console.log(windowSize);

                // Restart page manipulation
                setBackgroundImages();
                setFindAStationTopMenuItem();
                setHomeTitleSpanish();
                setDisclaimerText();
                setZipCodeText();
                setHomeIcon();
            });
        });
				
				var path = location.pathname;
    
            switch (path) {
								case "/encuentra-una-estacion":
								case "/encuentra-una-estacion/":
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = '../../../arco.rpahosting.com/scripts/wp/detectmobilebrowser.jquery.js'/*tpa=http://arco.rpahosting.com/scripts/wp/detectmobilebrowser.jquery.js*/;
                    document.getElementsByTagName('head')[0].appendChild(script);
                
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = '../../../arco.rpahosting.com/scripts/wp/intersect.js'/*tpa=http://arco.rpahosting.com/scripts/wp/intersect.js*/;
                    document.getElementsByTagName('head')[0].appendChild(script);
                    
                    var css = document.createElement('link');
                    css.type = 'text/css';
                    css.rel = 'stylesheet';
                    css.href = '../../../www.arco.com/wp-content/themes/vip/tesoro/css/arco/intersect.css'/*tpa=http://www.arco.com/wp-content/themes/vip/tesoro/css/arco/intersect.css*/;
                    document.getElementsByTagName('head')[0].appendChild(css);
										
										var css = document.createElement('link');
                    css.type = 'text/css';
                    css.rel = 'stylesheet';
                    css.href = '../../../arco.rpahosting.com/css/spanish-locator.css'/*tpa=http://arco.rpahosting.com/css/spanish-locator.css*/;
                    document.getElementsByTagName('head')[0].appendChild(css);
                    
                    jQuery('body').addClass('page-id-22');
                    jQuery('body').addClass('page-find-a-station');
                    jQuery('div.post-meta').remove();
                    
                    jQuery('div.post-entry').css('padding-right','0px');
                    jQuery('#respond').remove();
                    
                break;
                
                
                default:
                break;
            }
				
    }
    else {//arco english
        
		if (window.location.host.substr(window.location.host.length - 8).toLowerCase() == 'http://ampmspanish.rpahosting.com/js/wp/arco.com'){
        //alert(window.location.host.substr(window.location.host.length - 8));
        console.log('entering english logic');

        jQuery(document).ready(function () {

            if (jQuery('body.error404').length > 0) {
                    // Add javascript error page script
                    
                    jQuery('#content-full').html('');
                    //alert("Page not found, please go to the Arco.com home page");

                    // Retrieve the HTML and inject it
                    jQuery('#content-full').load('http://arco.com/wp-content/themes/vip/tesoro/js/arco/error-page.html',
                            function(){
                                var script = document.createElement('script');
                                script.type = 'text/javascript';
                                script.src = '../../../arco.rpahosting.com/scripts/wp/error-page.js'/*tpa=http://arco.rpahosting.com/scripts/wp/error-page.js*/;
                                document.getElementsByTagName('head')[0].appendChild(script);      
                                jQuery('#content-full').css('backgroundColor','rgba(6, 102, 176, 0.0)')
                            }
                        );
                    //jQuery('#content-full').replaceWith();

                    
            }

            jQuery("#cpyrt-espanol").html("Español");
            windowSize = jQuery(window).width(); console.log('original window size set to: "' + windowSize + '"'); console.log(windowSize); //alert(windowSize)

            jQuery('#menu-item-365 a').html("Quality TOP TIER<sup>&reg;</sup> Gas");

            jQuery('body.page-faq #content-full').css("background-image","");

            setDisclaimerText();
            //setHomeTitleEnglsh();

                jQuery(window).resize(function () {
                windowSize = jQuery(window).width(); console.log(windowSize);

                // Restart page manipulation
                setDisclaimerText();
                //setHomeTitleEnglsh();
                });

                jQuery('.gm-style-iw div').css("width", "100%");

               /* Doesn't work because phone numbers arent visible at first load
                phonenumber = jQuery('.phone').text();

                jQuery('.phone').html("<a href=\"tel:" + phonenumber + "\">" + phonenumber + "</a>");
                
                */

                
            
            
              setTimeout(function(){  
                // Logic to update form fields
                if (jQuery('body.page-id-22').length > 0)
                {
                    jQuery('.location-search h4').text('Enter Your Location');
                    jQuery('.location-search > input').attr('placeholder', 'Enter a City and State or ZIP Code');
                }}, 1500);
                
            var path = location.pathname;
    
            switch (path) {
                case "/new-home":
                case "/new-home/":

                break;
                
                case "/find-a-station-2":
                case "/find-a-station-2/":
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = '../../../arco.rpahosting.com/scripts/wp/detectmobilebrowser.jquery.js'/*tpa=http://arco.rpahosting.com/scripts/wp/detectmobilebrowser.jquery.js*/;
                    document.getElementsByTagName('head')[0].appendChild(script);
                    
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = '../../../arco.rpahosting.com/scripts/wp/intersect.js'/*tpa=http://arco.rpahosting.com/scripts/wp/intersect.js*/;
                    document.getElementsByTagName('head')[0].appendChild(script);
                    
                    var css = document.createElement('link');
                    css.type = 'text/css';
                    css.rel = 'stylesheet';
                    css.href = '../../../www.arco.com/wp-content/themes/vip/tesoro/css/arco/intersect.css'/*tpa=http://www.arco.com/wp-content/themes/vip/tesoro/css/arco/intersect.css*/;
                    document.getElementsByTagName('head')[0].appendChild(css);
                    
                    jQuery('body').addClass('page-id-22');
                    jQuery('body').addClass('page-find-a-station');
                    jQuery('div.post-meta').remove();
                    
                    jQuery('div.post-entry').css('padding-right','0px');
                    jQuery('#respond').remove();
                    
                
                break;
                
                case "/find-a-station":
                case "/find-a-station/":
								case "/encuentra-una-estacion":
								case "/encuentra-una-estacion/":
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = '../../../arco.rpahosting.com/scripts/wp/detectmobilebrowser.jquery.js'/*tpa=http://arco.rpahosting.com/scripts/wp/detectmobilebrowser.jquery.js*/;
                    document.getElementsByTagName('head')[0].appendChild(script);
                
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = '../../../arco.rpahosting.com/scripts/wp/intersect.js'/*tpa=http://arco.rpahosting.com/scripts/wp/intersect.js*/;
                    document.getElementsByTagName('head')[0].appendChild(script);
                    
                    var css = document.createElement('link');
                    css.type = 'text/css';
                    css.rel = 'stylesheet';
                    css.href = '../../../www.arco.com/wp-content/themes/vip/tesoro/css/arco/intersect.css'/*tpa=http://www.arco.com/wp-content/themes/vip/tesoro/css/arco/intersect.css*/;
                    document.getElementsByTagName('head')[0].appendChild(css);
                    
                    jQuery('body').addClass('page-id-22');
                    jQuery('body').addClass('page-find-a-station');
                    jQuery('div.post-meta').remove();
                    
                    jQuery('div.post-entry').css('padding-right','0px');
                    jQuery('#respond').remove();
                    
                break;
                
                case "/quality-top-tier-gas":
                case "/quality-top-tier-gas/":
                    jQuery('.bannerText .pgInfoTitle').html('Quality TOP TIER<sup style="position: relative; top: -.25em">&reg;</sup> Gas');
                
                break;
                
                default:
                break;
            }
            

        });

        } // End of lastEightCars if statment
        

        
        
    }
}

if (document.addEventListener) {
	document.addEventListener("DOMContentLoaded",runMods);
}
else
{
	window.attachEvent("onload",runMods);
	browserMode = 'old';
}


// Change disclaimer copy
function changeDisclaimerCopy() {
	if (jQuery('.home-disclaimer').length > 0) {
		var newCopy = jQuery('.home-disclaimer').html().replace('Junio 2013-Junio 2014','Diciembre 2013-Diciembre 2014');
		jQuery('.home-disclaimer').html(newCopy)
	}
}

// reduce size of logo on smartphone portrait to prevent wrapping
function setHomeIcon() {
    jQuery('#logo').removeAttr('style');
    if (windowSize < 352){
        jQuery('#logo').attr('style','width:138px;')
    }
}

// Fix home page title formatting issues
function setHomeTitleSpanish() {
	console.log('setHomeTitleSpansih');
	
    // Reset affected styles
    jQuery('.home-sub-head').removeAttr("style");
	//jquery('.slider-caption a.blue-btn').text('VER MÁS');
	jQuery('.slider-caption a.blue-btn').text('VER MÁS');
	jQuery('.slider-caption a.view-vid-a').text('Ver Videos de ARCO');
	//Ver Videos de ARCO

    if (windowSize >= 240 && windowSize <= 479) {
        console.log('240-479');
        if (windowSize < 451) {
            jQuery('.home-sub-head').attr('style', 'margin-top: 20px !important;');
        }
        jQuery('.slider-caption h2').attr('style', originalTitleStyle + ';font-size: 18px !important; display:block; padding: 0px 3px; margin: 0px auto;');
    }
    else if (windowSize >= 480 && windowSize <= 579) {
        console.log('480-579');
        jQuery('.slider-caption h2').attr('style', originalTitleStyle + ';font-size: 24px !important; display:block; padding: 0px 3px; margin: 0px auto;');
    }
    else if (windowSize >= 580 && windowSize <= 1023) {
        console.log('580-1023');
        if (windowSize > 974) // Something weird happens after 974px, I need to add top padding so it appears on the page
        {
            jQuery('.slider-caption h2').attr('style', originalTitleStyle + ';font-size: 30px !important; display:block; padding-top: 77px; margin: 0px auto;');
        }
        else
        {
            jQuery('.slider-caption h2').attr('style', originalTitleStyle + ';font-size: 28px !important; display:block; margin: 0px auto;');
        }
		setSpecialLineHeightEnglish(68);console.log("580-1023");
    }
    else if (windowSize >= 1024 && windowSize <= 1280) {
        console.log('1024-1280');
        jQuery('.slider-caption h2').attr('style', originalTitleStyle + ';font-size: 40px !important; display:block; margin: 0px auto; line-height: 32px !important;');
		setSpecialLineHeightEnglish(68);console.log("1024-1280");
    }
    else if (windowSize > 1280) {
        console.log('+1280');
        jQuery('.slider-caption h2').attr('style', originalTitleStyle + ';font-size: 57px !important; display:block; margin: 0px auto;');
		setSpecialLineHeightEnglish(68);console.log("1280");
    }
    else
    {
        console.log("no title formatting occurred");
    }
}

function setHomeTitleEnglsh() {
	console.log('setHomeTitleEnglsh');
    // Reset affected styles
    jQuery('.home-sub-head').removeAttr("style");

    if (windowSize >= 240 && windowSize <= 479) {
        console.log('240-479');
    }
    else if (windowSize >= 480 && windowSize <= 579) {
        console.log('480-579');
    }
    else if (windowSize >= 580 && windowSize <= 1023) {
        console.log('580-1023');
		setSpecialLineHeightEnglish(68);
    }
    else if (windowSize >= 1024 && windowSize <= 1280) {
        console.log('1024-1280');
		setSpecialLineHeightEnglish(68);
    }
    else if (windowSize > 1280) {
        console.log('+1280');
		setSpecialLineHeightEnglish(68);
    }
    else
    {
        console.log("no title formatting occurred");
    }
}


function setSpecialLineHeightEnglish(heightVal) {
	if (!window.location.href.indexOf("http://ampmspanish.rpahosting.com/js/wp/es.arco.com") > -1) {
		jQuery('.slider-caption h2').css('line-height', heightVal+'px !important');
		console.log('setSpecialLineHeightEnglish');
	}	
}

// Fix overlapping disclaimer text
function setDisclaimerText() {
    // Reset affected styles
    jQuery('.home-disclaimer').removeAttr("style");
    jQuery('.home-disclaimer > a').removeAttr("style");

    if (windowSize >= 240 && windowSize < 499) {
        //alert('hello');
        jQuery('.home-disclaimer').attr('style', 'position: static; white-space: normal; margin-top: 40px; font-size: 11px !important; padding-bottom: 10px; line-height: 11px;');
        jQuery('.home-disclaimer > a').attr("style", "font-size: 11px;");
    }
    else if (windowSize >= 500 && windowSize < 875) {
        jQuery('.home-disclaimer').attr('style', 'font-size: 10px; position: static; white-space: normal; margin: 0px auto; padding: 0 1%; line-height: 11px; padding-bottom: 10px;');
        jQuery('.home-disclaimer > a').attr("style", "font-size: 10px;");
    }
    else if (windowSize >= 768 && windowSize < 875)
    {
        jQuery('.home-disclaimer').attr('style', 'position: static; white-space: normal; font-size: 11px; line-height: 12px; padding-bottom: 10px; margin: 0px auto; width: 700px;');
        jQuery('.home-disclaimer > a').attr("style", "font-size: 11px;");
    }
    else if (windowSize >= 875 && windowSize < 1024)
    {
        jQuery('.home-disclaimer').attr('style', 'position: static; white-space: normal; font-size: 11px; line-height: 12px; padding-bottom: 10px; margin: 0px auto; width: 800px');
        jQuery('.home-disclaimer > a').attr("style", "font-size: 11px;");
    }
    else
    {
        console.log("no disclaimer formatting occured");
    }
}

// Set background images on specified pages based on document URL
function setBackgroundImages() {
    if (document.URL == "http://es.arco.com/gasolina-calidad-top-tier-por-menos/" /*&& windowSize > 480*/) {
        jQuery('div#wrapper').css('background-image', 'url(https://spanisharcowebsite.files.wordpress.com/2014/11/top-tier-for-less.jpg)');
    }
    if (document.URL == "http://es.arco.com/gasolina-calidad-top-tier/" /*&& windowSize > 480*/) {
        jQuery('div#wrapper').css('background-image', 'url(https://spanisharcowebsite.files.wordpress.com/2014/11/gasoline-top-tier_02.png)');
    }
    if (document.URL == "http://es.arco.com/tarjetas-prepagadas-y-tarjetas-de-flota/" /*&& windowSize > 480*/) {
        jQuery('div#wrapper').css('background-image', 'url(https://spanisharcowebsite.files.wordpress.com/2014/11/fleet-card_02.png)');
    }
    if (document.URL == "http://es.arco.com/contactanos/" /*&& windowSize > 480*/) {
        jQuery('div#wrapper').css('background-image', 'url(https://spanisharcowebsite.files.wordpress.com/2014/11/top-tier-for-less.jpg)');
    }
}

function setFindAStationTopMenuItem(){
    jQuery('#menu-find-station-a').attr('href', '/encuentra-una-estacion'); //Change links of Navigation Buttons to point to Spanish URL's
    jQuery('#menu-find-station-a img').attr('src', 'https://spanisharcowebsite.files.wordpress.com/2014/11/arco_mobile_5_5_14-cb_espanol-2_022.png'); //change to spanish Find A Station Img
}

function setZipCodeText() {
    //Change Zip Code to Spanish text after a delay to ensure items are loaded
    var delay = 350;
    setTimeout(function () {
        jQuery('.location-search h4').html('Ingresa tu C&oacute;digo Postal');
        jQuery('.location-search input:text').attr('placeholder', jQuery('<div/>').html('Ingresa tu C&oacute;digo Postal').text());
        jQuery('.location-search input').css('height', '25px');
    }, delay);
}
