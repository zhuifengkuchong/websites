<script id = "race55a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race55={};
	myVars.races.race55.varName="Function[758].guid";
	myVars.races.race55.varType="@varType@";
	myVars.races.race55.repairType = "@RepairType";
	myVars.races.race55.event1={};
	myVars.races.race55.event2={};
	myVars.races.race55.event1.id = "block-search-form";
	myVars.races.race55.event1.type = "onmouseover";
	myVars.races.race55.event1.loc = "block-search-form_LOC";
	myVars.races.race55.event1.isRead = "False";
	myVars.races.race55.event1.eventType = "@event1EventType@";
	myVars.races.race55.event2.id = "Lu_Id_div_53";
	myVars.races.race55.event2.type = "onmousedown";
	myVars.races.race55.event2.loc = "Lu_Id_div_53_LOC";
	myVars.races.race55.event2.isRead = "True";
	myVars.races.race55.event2.eventType = "@event2EventType@";
	myVars.races.race55.event1.executed= false;// true to disable, false to enable
	myVars.races.race55.event2.executed= false;// true to disable, false to enable
</script>

