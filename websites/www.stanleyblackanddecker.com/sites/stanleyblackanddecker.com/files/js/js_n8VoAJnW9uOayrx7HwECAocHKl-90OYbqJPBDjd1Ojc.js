/* Modernizr 2.5.3 (Custom Build) | MIT & BSD
 * Build: http://www.modernizr.com/download/#-fontface-backgroundsize-borderimage-borderradius-boxshadow-flexbox-hsla-multiplebgs-opacity-rgba-textshadow-cssanimations-csscolumns-generatedcontent-cssgradients-cssreflections-csstransforms-csstransforms3d-csstransitions-applicationcache-canvas-canvastext-draganddrop-hashchange-history-audio-video-indexeddb-input-inputtypes-localstorage-postmessage-sessionstorage-websockets-websqldatabase-webworkers-geolocation-inlinesvg-smil-svg-svgclippaths-touch-webgl-shiv-mq-cssclasses-addtest-prefixed-teststyles-testprop-testallprops-hasevent-prefixes-domprefixes-load
 */
;window.Modernizr=function(a,b,c){function D(a){j.cssText=a}function E(a,b){return D(n.join(a+";")+(b||""))}function F(a,b){return typeof a===b}function G(a,b){return!!~(""+a).indexOf(b)}function H(a,b){for(var d in a)if(j[a[d]]!==c)return b=="pfx"?a[d]:!0;return!1}function I(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:F(f,"function")?f.bind(d||b):f}return!1}function J(a,b,c){var d=a.charAt(0).toUpperCase()+a.substr(1),e=(a+" "+p.join(d+" ")+d).split(" ");return F(b,"string")||F(b,"undefined")?H(e,b):(e=(a+" "+q.join(d+" ")+d).split(" "),I(e,b,c))}function L(){e.input=function(c){for(var d=0,e=c.length;d<e;d++)u[c[d]]=c[d]in k;return u.list&&(u.list=!!b.createElement("datalist")&&!!a.HTMLDataListElement),u}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),e.inputtypes=function(a){for(var d=0,e,f,h,i=a.length;d<i;d++)k.setAttribute("type",f=a[d]),e=k.type!=="text",e&&(k.value=l,k.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(f)&&k.style.WebkitAppearance!==c?(g.appendChild(k),h=b.defaultView,e=h.getComputedStyle&&h.getComputedStyle(k,null).WebkitAppearance!=="textfield"&&k.offsetHeight!==0,g.removeChild(k)):/^(search|tel)$/.test(f)||(/^(url|email)$/.test(f)?e=k.checkValidity&&k.checkValidity()===!1:/^color$/.test(f)?(g.appendChild(k),g.offsetWidth,e=k.value!=l,g.removeChild(k)):e=k.value!=l)),t[a[d]]=!!e;return t}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var d="2.5.3",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k=b.createElement("input"),l=":)",m={}.toString,n=" -webkit- -moz- -o- -ms- ".split(" "),o="Webkit Moz O ms",p=o.split(" "),q=o.toLowerCase().split(" "),r={svg:"http://www.w3.org/2000/svg"},s={},t={},u={},v=[],w=v.slice,x,y=function(a,c,d,e){var f,i,j,k=b.createElement("div"),l=b.body,m=l?l:b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),k.appendChild(j);return f=["&#173;","<style>",a,"</style>"].join(""),k.id=h,m.innerHTML+=f,m.appendChild(k),l||(m.style.background="",g.appendChild(m)),i=c(k,a),l?k.parentNode.removeChild(k):m.parentNode.removeChild(m),!!i},z=function(b){var c=a.matchMedia||a.msMatchMedia;if(c)return c(b).matches;var d;return y("@media "+b+" { #"+h+" { position: absolute; } }",function(b){d=(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle)["position"]=="absolute"}),d},A=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=F(e[d],"function"),F(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),B={}.hasOwnProperty,C;!F(B,"undefined")&&!F(B.call,"undefined")?C=function(a,b){return B.call(a,b)}:C=function(a,b){return b in a&&F(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=w.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(w.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(w.call(arguments)))};return e});var K=function(c,d){var f=c.join(""),g=d.length;y(f,function(c,d){var f=b.styleSheets[b.styleSheets.length-1],h=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"",i=c.childNodes,j={};while(g--)j[i[g].id]=i[g];e.touch="ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch||(j.touch&&j.touch.offsetTop)===9,e.csstransforms3d=(j.csstransforms3d&&j.csstransforms3d.offsetLeft)===9&&j.csstransforms3d.offsetHeight===3,e.generatedcontent=(j.generatedcontent&&j.generatedcontent.offsetHeight)>=1,e.fontface=/src/i.test(h)&&h.indexOf(d.split(" ")[0])===0},g,d)}(['@font-face {font-family:"font";src:url("https://")}',["@media (",n.join("touch-enabled),("),h,")","{#touch{top:9px;position:absolute}}"].join(""),["@media (",n.join("transform-3d),("),h,")","{#csstransforms3d{left:9px;position:absolute;height:3px;}}"].join(""),['#generatedcontent:after{content:"',l,'";visibility:hidden}'].join("")],["fontface","touch","csstransforms3d","generatedcontent"]);s.flexbox=function(){return J("flexOrder")},s.canvas=function(){var a=b.createElement("canvas");return!!a.getContext&&!!a.getContext("2d")},s.canvastext=function(){return!!e.canvas&&!!F(b.createElement("canvas").getContext("2d").fillText,"function")},s.webgl=function(){try{var d=b.createElement("canvas"),e;e=!(!a.WebGLRenderingContext||!d.getContext("experimental-webgl")&&!d.getContext("webgl")),d=c}catch(f){e=!1}return e},s.touch=function(){return e.touch},s.geolocation=function(){return!!navigator.geolocation},s.postmessage=function(){return!!a.postMessage},s.websqldatabase=function(){return!!a.openDatabase},s.indexedDB=function(){return!!J("indexedDB",a)},s.hashchange=function(){return A("hashchange",a)&&(b.documentMode===c||b.documentMode>7)},s.history=function(){return!!a.history&&!!history.pushState},s.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a},s.websockets=function(){for(var b=-1,c=p.length;++b<c;)if(a[p[b]+"WebSocket"])return!0;return"WebSocket"in a},s.rgba=function(){return D("background-color:rgba(150,255,150,.5)"),G(j.backgroundColor,"rgba")},s.hsla=function(){return D("background-color:hsla(120,40%,100%,.5)"),G(j.backgroundColor,"rgba")||G(j.backgroundColor,"hsla")},s.multiplebgs=function(){return D("background:url(https://),url(https://),red url(https://)"),/(url\s*\(.*?){3}/.test(j.background)},s.backgroundsize=function(){return J("backgroundSize")},s.borderimage=function(){return J("borderImage")},s.borderradius=function(){return J("borderRadius")},s.boxshadow=function(){return J("boxShadow")},s.textshadow=function(){return b.createElement("div").style.textShadow===""},s.opacity=function(){return E("opacity:.55"),/^0.55$/.test(j.opacity)},s.cssanimations=function(){return J("animationName")},s.csscolumns=function(){return J("columnCount")},s.cssgradients=function(){var a="background-image:",b="gradient(linear,left top,right bottom,from(#9f9),to(white));",c="linear-gradient(left top,#9f9, white);";return D((a+"-webkit- ".split(" ").join(b+a)+n.join(c+a)).slice(0,-a.length)),G(j.backgroundImage,"gradient")},s.cssreflections=function(){return J("boxReflect")},s.csstransforms=function(){return!!J("transform")},s.csstransforms3d=function(){var a=!!J("perspective");return a&&"webkitPerspective"in g.style&&(a=e.csstransforms3d),a},s.csstransitions=function(){return J("transition")},s.fontface=function(){return e.fontface},s.generatedcontent=function(){return e.generatedcontent},s.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c},s.audio=function(){var a=b.createElement("audio"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,"")}catch(d){}return c},s.localstorage=function(){try{return localStorage.setItem(h,h),localStorage.removeItem(h),!0}catch(a){return!1}},s.sessionstorage=function(){try{return sessionStorage.setItem(h,h),sessionStorage.removeItem(h),!0}catch(a){return!1}},s.webworkers=function(){return!!a.Worker},s.applicationcache=function(){return!!a.applicationCache},s.svg=function(){return!!b.createElementNS&&!!b.createElementNS(r.svg,"svg").createSVGRect},s.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==r.svg},s.smil=function(){return!!b.createElementNS&&/SVGAnimate/.test(m.call(b.createElementNS(r.svg,"animate")))},s.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(m.call(b.createElementNS(r.svg,"clipPath")))};for(var M in s)C(s,M)&&(x=M.toLowerCase(),e[x]=s[M](),v.push((e[x]?"":"no-")+x));return e.input||L(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)C(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,g.className+=" "+(b?"":"no-")+a,e[a]=b}return e},D(""),i=k=null,function(a,b){function g(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function h(){var a=k.elements;return typeof a=="string"?a.split(" "):a}function i(a){var b={},c=a.createElement,e=a.createDocumentFragment,f=e();a.createElement=function(a){var e=(b[a]||(b[a]=c(a))).cloneNode();return k.shivMethods&&e.canHaveChildren&&!d.test(a)?f.appendChild(e):e},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+h().join().replace(/\w+/g,function(a){return b[a]=c(a),f.createElement(a),'c("'+a+'")'})+");return n}")(k,f)}function j(a){var b;return a.documentShived?a:(k.shivCSS&&!e&&(b=!!g(a,"article,aside,details,figcaption,figure,footer,header,hgroup,nav,section{display:block}audio{display:none}canvas,video{display:inline-block;*display:inline;*zoom:1}[hidden]{display:none}audio[controls]{display:inline-block;*display:inline;*zoom:1}mark{background:#FF0;color:#000}")),f||(b=!i(a)),b&&(a.documentShived=b),a)}var c=a.html5||{},d=/^<|^(?:button|form|map|select|textarea)$/i,e,f;(function(){var a=b.createElement("a");a.innerHTML="<xyz></xyz>",e="hidden"in a,f=a.childNodes.length==1||function(){try{b.createElement("a")}catch(a){return!0}var c=b.createDocumentFragment();return typeof c.cloneNode=="undefined"||typeof c.createDocumentFragment=="undefined"||typeof c.createElement=="undefined"}()})();var k={elements:c.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:c.shivCSS!==!1,shivMethods:c.shivMethods!==!1,type:"default",shivDocument:j};a.html5=k,j(b)}(this,b),e._version=d,e._prefixes=n,e._domPrefixes=q,e._cssomPrefixes=p,e.mq=z,e.hasEvent=A,e.testProp=function(a){return H([a])},e.testAllProps=J,e.testStyles=y,e.prefixed=function(a,b,c){return b?J(a,b,c):J(a,"pfx")},g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+v.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return o.call(a)=="[object Function]"}function e(a){return typeof a=="string"}function f(){}function g(a){return!a||a=="loaded"||a=="complete"||a=="uninitialized"}function h(){var a=p.shift();q=1,a?a.t?m(function(){(a.t=="c"?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){a!="img"&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l={},o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};y[c]===1&&(r=1,y[c]=[],l=b.createElement(a)),a=="object"?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),a!="img"&&(r||y[c]===2?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i(b=="c"?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),p.length==1&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&o.call(a.opera)=="[object Opera]",l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return o.call(a)=="[object Array]"},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,i){var j=b(a),l=j.autoCallback;j.url.split(".").pop().split("?").shift(),j.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]||h),j.instead?j.instead(a,e,f,g,i):(y[j.url]?j.noexec=!0:y[j.url]=1,f.load(j.url,j.forceCSS||!j.forceJS&&"css"==j.url.split(".").pop().split("?").shift()?"c":c,j.noexec,j.attrs,j.timeout),(d(e)||d(l))&&f.load(function(){k(),e&&e(j.origUrl,i,g),l&&l(j.origUrl,i,g),y[j.url]=2})))}function i(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var j,l,m=this.yepnope.loader;if(e(a))g(a,0,m,0);else if(w(a))for(j=0;j<a.length;j++)l=a[j],e(l)?g(l,0,m,0):w(l)?B(l):Object(l)===l&&i(l,m);else Object(a)===a&&i(a,m)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,b.readyState==null&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};
;
// KLUNK & MILLAN NOTE:
// TOM DONAHUE REMOVED THE HEIGHT SETTING FOR FIXING RESPONSIVENESS
// // Sticky Plugin v1.0.0 for jQuery
// =============
// Author: Anthony Garand
// Improvements by German M. Bravo (Kronuz) and Ruud Kamphuis (ruudk)
// Improvements by Leonardo C. Daronco (daronco)
// Created: 2/14/2011
// Date: 2/12/2012
// Website: http://labs.anthonygarand.com/sticky
// Description: Makes an element on the page stick on the screen as you scroll
//       It will only set the 'top' and 'position' of your element, you
//       might need to adjust the width in some cases.

(function($) {
  var defaults = {
      topSpacing: 0,
      bottomSpacing: 0,
      className: 'is-sticky',
      wrapperClassName: 'sticky-wrapper',
      center: false,
      getWidthFrom: ''
    },
    $window = $(window),
    $document = $(document),
    sticked = [],
    windowHeight = $window.height(),
    scroller = function() {
      var scrollTop = $window.scrollTop(),
        documentHeight = $document.height(),
        dwh = documentHeight - windowHeight,
        extra = (scrollTop > dwh) ? dwh - scrollTop : 0;

      for (var i = 0; i < sticked.length; i++) {
        var s = sticked[i],
          elementTop = s.stickyWrapper.offset().top,
          etse = elementTop - s.topSpacing - extra;

        if (scrollTop <= etse) {
          if (s.currentTop !== null) {
            s.stickyElement
              .css('position', '')
              .css('top', '');
            s.stickyElement.parent().removeClass(s.className);
            s.currentTop = null;
          }
        }
        else {
          var newTop = documentHeight - s.stickyElement.outerHeight()
            - s.topSpacing - s.bottomSpacing - scrollTop - extra;
          if (newTop < 0) {
            newTop = newTop + s.topSpacing;
          } else {
            newTop = s.topSpacing;
          }
          if (s.currentTop != newTop) {
            s.stickyElement
              .css('position', 'fixed')
              .css('top', newTop);

            if (typeof s.getWidthFrom !== 'undefined') {
              s.stickyElement.css('width', $(s.getWidthFrom).width());
            }

            s.stickyElement.parent().addClass(s.className);
            s.currentTop = newTop;
          }
        }
      }
    },
    resizer = function() {
      windowHeight = $window.height();
    },
    methods = {
      init: function(options) {
        var o = $.extend(defaults, options);
        return this.each(function() {
          var stickyElement = $(this);

          var stickyId = stickyElement.attr('id');
          var wrapper = $('<div></div>')
            .attr('id', stickyId + '-sticky-wrapper')
            .addClass(o.wrapperClassName);
          stickyElement.wrapAll(wrapper);

          if (o.center) {
            stickyElement.parent().css({width:stickyElement.outerWidth(),marginLeft:"auto",marginRight:"auto"});
          }

          if (stickyElement.css("float") == "right") {
            stickyElement.css({"float":"none"}).parent().css({"float":"right"});
          }

          var stickyWrapper = stickyElement.parent();
          //stickyWrapper.css('height', stickyElement.outerHeight());
          sticked.push({
            topSpacing: o.topSpacing,
            bottomSpacing: o.bottomSpacing,
            stickyElement: stickyElement,
            currentTop: null,
            stickyWrapper: stickyWrapper,
            className: o.className,
            getWidthFrom: o.getWidthFrom
          });
        });
      },
      update: scroller
    };

  // should be more efficient than using $window.scroll(scroller) and $window.resize(resizer):
  if (window.addEventListener) {
    window.addEventListener('scroll', scroller, false);
    window.addEventListener('resize', resizer, false);
  } else if (window.attachEvent) {
    window.attachEvent('onscroll', scroller);
    window.attachEvent('onresize', resizer);
  }

  $.fn.sticky = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };
  $(function() {
    setTimeout(scroller, 0);
  });
})(jQuery);
;
/*
 * jQuery FlexSlider v2.2.2
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */
;
(function ($) {

  //FlexSlider: Object Instance
  $.flexslider = function(el, options) {
    var slider = $(el);

    // making variables public
    slider.vars = $.extend({}, $.flexslider.defaults, options);

    var namespace = slider.vars.namespace,
        msGesture = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
        touch = (( "ontouchstart" in window ) || msGesture || window.DocumentTouch && document instanceof DocumentTouch) && slider.vars.touch,
        // depricating this idea, as devices are being released with both of these events
        //eventType = (touch) ? "touchend" : "click",
        eventType = "click touchend MSPointerUp",
        watchedEvent = "",
        watchedEventClearTimer,
        vertical = slider.vars.direction === "vertical",
        reverse = slider.vars.reverse,
        carousel = (slider.vars.itemWidth > 0),
        fade = slider.vars.animation === "fade",
        asNav = slider.vars.asNavFor !== "",
        methods = {},
        focused = true;

    // Store a reference to the slider object
    $.data(el, "flexslider", slider);

    // Private slider methods
    methods = {
      init: function() {
        slider.animating = false;
        // Get current slide and make sure it is a number
        slider.currentSlide = parseInt( ( slider.vars.startAt ? slider.vars.startAt : 0), 10 );
        if ( isNaN( slider.currentSlide ) ) slider.currentSlide = 0;
        slider.animatingTo = slider.currentSlide;
        slider.atEnd = (slider.currentSlide === 0 || slider.currentSlide === slider.last);
        slider.containerSelector = slider.vars.selector.substr(0,slider.vars.selector.search(' '));
        slider.slides = $(slider.vars.selector, slider);
        slider.container = $(slider.containerSelector, slider);
        slider.count = slider.slides.length;
        // SYNC:
        slider.syncExists = $(slider.vars.sync).length > 0;
        // SLIDE:
        if (slider.vars.animation === "slide") slider.vars.animation = "swing";
        slider.prop = (vertical) ? "top" : "marginLeft";
        slider.args = {};
        // SLIDESHOW:
        slider.manualPause = false;
        slider.stopped = false;
        //PAUSE WHEN INVISIBLE
        slider.started = false;
        slider.startTimeout = null;
        // TOUCH/USECSS:
        slider.transitions = !slider.vars.video && !fade && slider.vars.useCSS && (function() {
          var obj = document.createElement('div'),
              props = ['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
          for (var i in props) {
            if ( obj.style[ props[i] ] !== undefined ) {
              slider.pfx = props[i].replace('Perspective','').toLowerCase();
              slider.prop = "-" + slider.pfx + "-transform";
              return true;
            }
          }
          return false;
        }());
        // CONTROLSCONTAINER:
        if (slider.vars.controlsContainer !== "") slider.controlsContainer = $(slider.vars.controlsContainer).length > 0 && $(slider.vars.controlsContainer);
        // MANUAL:
        if (slider.vars.manualControls !== "") slider.manualControls = $(slider.vars.manualControls).length > 0 && $(slider.vars.manualControls);

        // RANDOMIZE:
        if (slider.vars.randomize) {
          slider.slides.sort(function() { return (Math.round(Math.random())-0.5); });
          slider.container.empty().append(slider.slides);
        }

        slider.doMath();

        // INIT
        slider.setup("init");

        // CONTROLNAV:
        if (slider.vars.controlNav) methods.controlNav.setup();

        // DIRECTIONNAV:
        if (slider.vars.directionNav) methods.directionNav.setup();

        // KEYBOARD:
        if (slider.vars.keyboard && ($(slider.containerSelector).length === 1 || slider.vars.multipleKeyboard)) {
          $(document).bind('keyup', function(event) {
            var keycode = event.keyCode;
            if (!slider.animating && (keycode === 39 || keycode === 37)) {
              var target = (keycode === 39) ? slider.getTarget('next') :
                           (keycode === 37) ? slider.getTarget('prev') : false;
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }
          });
        }
        // MOUSEWHEEL:
        if (slider.vars.mousewheel) {
          slider.bind('mousewheel', function(event, delta, deltaX, deltaY) {
            event.preventDefault();
            var target = (delta < 0) ? slider.getTarget('next') : slider.getTarget('prev');
            slider.flexAnimate(target, slider.vars.pauseOnAction);
          });
        }

        // PAUSEPLAY
        if (slider.vars.pausePlay) methods.pausePlay.setup();

        //PAUSE WHEN INVISIBLE
        if (slider.vars.slideshow && slider.vars.pauseInvisible) methods.pauseInvisible.init();

        // SLIDSESHOW
        if (slider.vars.slideshow) {
          if (slider.vars.pauseOnHover) {
            slider.hover(function() {
              if (!slider.manualPlay && !slider.manualPause) slider.pause();
            }, function() {
              if (!slider.manualPause && !slider.manualPlay && !slider.stopped) slider.play();
            });
          }
          // initialize animation
          //If we're visible, or we don't use PageVisibility API
          if(!slider.vars.pauseInvisible || !methods.pauseInvisible.isHidden()) {
            (slider.vars.initDelay > 0) ? slider.startTimeout = setTimeout(slider.play, slider.vars.initDelay) : slider.play();
          }
        }

        // ASNAV:
        if (asNav) methods.asNav.setup();

        // TOUCH
        if (touch && slider.vars.touch) methods.touch();

        // FADE&&SMOOTHHEIGHT || SLIDE:
        if (!fade || (fade && slider.vars.smoothHeight)) $(window).bind("resize orientationchange focus", methods.resize);

        slider.find("img").attr("draggable", "false");

        // API: start() Callback
        setTimeout(function(){
          slider.vars.start(slider);
        }, 200);
      },
      asNav: {
        setup: function() {
          slider.asNav = true;
          slider.animatingTo = Math.floor(slider.currentSlide/slider.move);
          slider.currentItem = slider.currentSlide;
          slider.slides.removeClass(namespace + "active-slide").eq(slider.currentItem).addClass(namespace + "active-slide");
          if(!msGesture){
              slider.slides.on(eventType, function(e){
                e.preventDefault();
                var $slide = $(this),
                    target = $slide.index();
                var posFromLeft = $slide.offset().left - $(slider).scrollLeft(); // Find position of slide relative to left of slider container
                if( posFromLeft <= 0 && $slide.hasClass( namespace + 'active-slide' ) ) {
                  slider.flexAnimate(slider.getTarget("prev"), true);
                } else if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass(namespace + "active-slide")) {
                  slider.direction = (slider.currentItem < target) ? "next" : "prev";
                  slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                }
              });
          }else{
              el._slider = slider;
              slider.slides.each(function (){
                  var that = this;
                  that._gesture = new MSGesture();
                  that._gesture.target = that;
                  that.addEventListener("MSPointerDown", function (e){
                      e.preventDefault();
                      if(e.currentTarget._gesture)
                          e.currentTarget._gesture.addPointer(e.pointerId);
                  }, false);
                  that.addEventListener("MSGestureTap", function (e){
                      e.preventDefault();
                      var $slide = $(this),
                          target = $slide.index();
                      if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass('active')) {
                          slider.direction = (slider.currentItem < target) ? "next" : "prev";
                          slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                      }
                  });
              });
          }
        }
      },
      controlNav: {
        setup: function() {
          if (!slider.manualControls) {
            methods.controlNav.setupPaging();
          } else { // MANUALCONTROLS:
            methods.controlNav.setupManual();
          }
        },
        setupPaging: function() {
          var type = (slider.vars.controlNav === "thumbnails") ? 'control-thumbs' : 'control-paging',
              j = 1,
              item,
              slide;

          slider.controlNavScaffold = $('<ol class="'+ namespace + 'control-nav ' + namespace + type + '"></ol>');

          if (slider.pagingCount > 1) {
            for (var i = 0; i < slider.pagingCount; i++) {
              slide = slider.slides.eq(i);
              item = (slider.vars.controlNav === "thumbnails") ? '<img src="' + slide.attr( 'data-thumb' ) + '"/>' : '<a>' + j + '</a>';
              if ( 'thumbnails' === slider.vars.controlNav && true === slider.vars.thumbCaptions ) {
                var captn = slide.attr( 'data-thumbcaption' );
                if ( '' != captn && undefined != captn ) item += '<span class="' + namespace + 'caption">' + captn + '</span>';
              }
              slider.controlNavScaffold.append('<li>' + item + '</li>');
              j++;
            }
          }

          // CONTROLSCONTAINER:
          (slider.controlsContainer) ? $(slider.controlsContainer).append(slider.controlNavScaffold) : slider.append(slider.controlNavScaffold);
          methods.controlNav.set();

          methods.controlNav.active();

          slider.controlNavScaffold.delegate('a, img', eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                slider.direction = (target > slider.currentSlide) ? "next" : "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();

          });
        },
        setupManual: function() {
          slider.controlNav = slider.manualControls;
          methods.controlNav.active();

          slider.controlNav.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                (target > slider.currentSlide) ? slider.direction = "next" : slider.direction = "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        set: function() {
          var selector = (slider.vars.controlNav === "thumbnails") ? 'img' : 'a';
          slider.controlNav = $('.' + namespace + 'control-nav li ' + selector, (slider.controlsContainer) ? slider.controlsContainer : slider);
        },
        active: function() {
          slider.controlNav.removeClass(namespace + "active").eq(slider.animatingTo).addClass(namespace + "active");
        },
        update: function(action, pos) {
          if (slider.pagingCount > 1 && action === "add") {
            slider.controlNavScaffold.append($('<li><a>' + slider.count + '</a></li>'));
          } else if (slider.pagingCount === 1) {
            slider.controlNavScaffold.find('li').remove();
          } else {
            slider.controlNav.eq(pos).closest('li').remove();
          }
          methods.controlNav.set();
          (slider.pagingCount > 1 && slider.pagingCount !== slider.controlNav.length) ? slider.update(pos, action) : methods.controlNav.active();
        }
      },
      directionNav: {
        setup: function() {
          var directionNavScaffold = $('<ul class="' + namespace + 'direction-nav"><li><a class="' + namespace + 'prev" href="#">' + slider.vars.prevText + '</a></li><li><a class="' + namespace + 'next" href="#">' + slider.vars.nextText + '</a></li></ul>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            $(slider.controlsContainer).append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider.controlsContainer);
          } else {
            slider.append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider);
          }

          methods.directionNav.update();

          slider.directionNav.bind(eventType, function(event) {
            event.preventDefault();
            var target;

            if (watchedEvent === "" || watchedEvent === event.type) {
              target = ($(this).hasClass(namespace + 'next')) ? slider.getTarget('next') : slider.getTarget('prev');
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function() {
          var disabledClass = namespace + 'disabled';
          if (slider.pagingCount === 1) {
            slider.directionNav.addClass(disabledClass).attr('tabindex', '-1');
          } else if (!slider.vars.animationLoop) {
            if (slider.animatingTo === 0) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "prev").addClass(disabledClass).attr('tabindex', '-1');
            } else if (slider.animatingTo === slider.last) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "next").addClass(disabledClass).attr('tabindex', '-1');
            } else {
              slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
            }
          } else {
            slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
          }
        }
      },
      pausePlay: {
        setup: function() {
          var pausePlayScaffold = $('<div class="' + namespace + 'pauseplay"><a></a></div>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            slider.controlsContainer.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider.controlsContainer);
          } else {
            slider.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider);
          }

          methods.pausePlay.update((slider.vars.slideshow) ? namespace + 'pause' : namespace + 'play');

          slider.pausePlay.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              if ($(this).hasClass(namespace + 'pause')) {
                slider.manualPause = true;
                slider.manualPlay = false;
                slider.pause();
              } else {
                slider.manualPause = false;
                slider.manualPlay = true;
                slider.play();
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function(state) {
          (state === "play") ? slider.pausePlay.removeClass(namespace + 'pause').addClass(namespace + 'play').html(slider.vars.playText) : slider.pausePlay.removeClass(namespace + 'play').addClass(namespace + 'pause').html(slider.vars.pauseText);
        }
      },
      touch: function() {
        var startX,
          startY,
          offset,
          cwidth,
          dx,
          startT,
          scrolling = false,
          localX = 0,
          localY = 0,
          accDx = 0;

        if(!msGesture){
            el.addEventListener('touchstart', onTouchStart, false);

            function onTouchStart(e) {
              if (slider.animating) {
                e.preventDefault();
              } else if ( ( window.navigator.msPointerEnabled ) || e.touches.length === 1 ) {
                slider.pause();
                // CAROUSEL:
                cwidth = (vertical) ? slider.h : slider. w;
                startT = Number(new Date());
                // CAROUSEL:

                // Local vars for X and Y points.
                localX = e.touches[0].pageX;
                localY = e.touches[0].pageY;

                offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                         (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                         (carousel && slider.currentSlide === slider.last) ? slider.limit :
                         (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                         (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                startX = (vertical) ? localY : localX;
                startY = (vertical) ? localX : localY;

                el.addEventListener('touchmove', onTouchMove, false);
                el.addEventListener('touchend', onTouchEnd, false);
              }
            }

            function onTouchMove(e) {
              // Local vars for X and Y points.

              localX = e.touches[0].pageX;
              localY = e.touches[0].pageY;

              dx = (vertical) ? startX - localY : startX - localX;
              scrolling = (vertical) ? (Math.abs(dx) < Math.abs(localX - startY)) : (Math.abs(dx) < Math.abs(localY - startY));

              var fxms = 500;

              if ( ! scrolling || Number( new Date() ) - startT > fxms ) {
                e.preventDefault();
                if (!fade && slider.transitions) {
                  if (!slider.vars.animationLoop) {
                    dx = dx/((slider.currentSlide === 0 && dx < 0 || slider.currentSlide === slider.last && dx > 0) ? (Math.abs(dx)/cwidth+2) : 1);
                  }
                  slider.setProps(offset + dx, "setTouch");
                }
              }
            }

            function onTouchEnd(e) {
              // finish the touch by undoing the touch session
              el.removeEventListener('touchmove', onTouchMove, false);

              if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                var updateDx = (reverse) ? -dx : dx,
                    target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                  slider.flexAnimate(target, slider.vars.pauseOnAction);
                } else {
                  if (!fade) slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                }
              }
              el.removeEventListener('touchend', onTouchEnd, false);

              startX = null;
              startY = null;
              dx = null;
              offset = null;
            }
        }else{
            el.style.msTouchAction = "none";
            el._gesture = new MSGesture();
            el._gesture.target = el;
            el.addEventListener("MSPointerDown", onMSPointerDown, false);
            el._slider = slider;
            el.addEventListener("MSGestureChange", onMSGestureChange, false);
            el.addEventListener("MSGestureEnd", onMSGestureEnd, false);

            function onMSPointerDown(e){
                e.stopPropagation();
                if (slider.animating) {
                    e.preventDefault();
                }else{
                    slider.pause();
                    el._gesture.addPointer(e.pointerId);
                    accDx = 0;
                    cwidth = (vertical) ? slider.h : slider. w;
                    startT = Number(new Date());
                    // CAROUSEL:

                    offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                        (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                            (carousel && slider.currentSlide === slider.last) ? slider.limit :
                                (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                                    (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                }
            }

            function onMSGestureChange(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                var transX = -e.translationX,
                    transY = -e.translationY;

                //Accumulate translations.
                accDx = accDx + ((vertical) ? transY : transX);
                dx = accDx;
                scrolling = (vertical) ? (Math.abs(accDx) < Math.abs(-transX)) : (Math.abs(accDx) < Math.abs(-transY));

                if(e.detail === e.MSGESTURE_FLAG_INERTIA){
                    setImmediate(function (){
                        el._gesture.stop();
                    });

                    return;
                }

                if (!scrolling || Number(new Date()) - startT > 500) {
                    e.preventDefault();
                    if (!fade && slider.transitions) {
                        if (!slider.vars.animationLoop) {
                            dx = accDx / ((slider.currentSlide === 0 && accDx < 0 || slider.currentSlide === slider.last && accDx > 0) ? (Math.abs(accDx) / cwidth + 2) : 1);
                        }
                        slider.setProps(offset + dx, "setTouch");
                    }
                }
            }

            function onMSGestureEnd(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                    var updateDx = (reverse) ? -dx : dx,
                        target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                    if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                        slider.flexAnimate(target, slider.vars.pauseOnAction);
                    } else {
                        if (!fade) slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                    }
                }

                startX = null;
                startY = null;
                dx = null;
                offset = null;
                accDx = 0;
            }
        }
      },
      resize: function() {
        if (!slider.animating && slider.is(':visible')) {
          if (!carousel) slider.doMath();

          if (fade) {
            // SMOOTH HEIGHT:
            methods.smoothHeight();
          } else if (carousel) { //CAROUSEL:
            slider.slides.width(slider.computedW);
            slider.update(slider.pagingCount);
            slider.setProps();
          }
          else if (vertical) { //VERTICAL:
            slider.viewport.height(slider.h);
            slider.setProps(slider.h, "setTotal");
          } else {
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) methods.smoothHeight();
            slider.newSlides.width(slider.computedW);
            slider.setProps(slider.computedW, "setTotal");
          }
        }
      },
      smoothHeight: function(dur) {
        if (!vertical || fade) {
          var $obj = (fade) ? slider : slider.viewport;
          (dur) ? $obj.animate({"height": slider.slides.eq(slider.animatingTo).height()}, dur) : $obj.height(slider.slides.eq(slider.animatingTo).height());
        }
      },
      sync: function(action) {
        var $obj = $(slider.vars.sync).data("flexslider"),
            target = slider.animatingTo;

        switch (action) {
          case "animate": $obj.flexAnimate(target, slider.vars.pauseOnAction, false, true); break;
          case "play": if (!$obj.playing && !$obj.asNav) { $obj.play(); } break;
          case "pause": $obj.pause(); break;
        }
      },
      uniqueID: function($clone) {
        $clone.find( '[id]' ).each(function() {
          var $this = $(this);
          $this.attr( 'id', $this.attr( 'id' ) + '_clone' );
        });
        return $clone;
      },
      pauseInvisible: {
        visProp: null,
        init: function() {
          var prefixes = ['webkit','moz','ms','o'];

          if ('hidden' in document) return 'hidden';
          for (var i = 0; i < prefixes.length; i++) {
            if ((prefixes[i] + 'Hidden') in document)
            methods.pauseInvisible.visProp = prefixes[i] + 'Hidden';
          }
          if (methods.pauseInvisible.visProp) {
            var evtname = methods.pauseInvisible.visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
            document.addEventListener(evtname, function() {
              if (methods.pauseInvisible.isHidden()) {
                if(slider.startTimeout) clearTimeout(slider.startTimeout); //If clock is ticking, stop timer and prevent from starting while invisible
                else slider.pause(); //Or just pause
              }
              else {
                if(slider.started) slider.play(); //Initiated before, just play
                else (slider.vars.initDelay > 0) ? setTimeout(slider.play, slider.vars.initDelay) : slider.play(); //Didn't init before: simply init or wait for it
              }
            });
          }
        },
        isHidden: function() {
          return document[methods.pauseInvisible.visProp] || false;
        }
      },
      setToClearWatchedEvent: function() {
        clearTimeout(watchedEventClearTimer);
        watchedEventClearTimer = setTimeout(function() {
          watchedEvent = "";
        }, 3000);
      }
    };

    // public methods
    slider.flexAnimate = function(target, pause, override, withSync, fromNav) {
      if (!slider.vars.animationLoop && target !== slider.currentSlide) {
        slider.direction = (target > slider.currentSlide) ? "next" : "prev";
      }

      if (asNav && slider.pagingCount === 1) slider.direction = (slider.currentItem < target) ? "next" : "prev";

      if (!slider.animating && (slider.canAdvance(target, fromNav) || override) && slider.is(":visible")) {
        if (asNav && withSync) {
          var master = $(slider.vars.asNavFor).data('flexslider');
          slider.atEnd = target === 0 || target === slider.count - 1;
          master.flexAnimate(target, true, false, true, fromNav);
          slider.direction = (slider.currentItem < target) ? "next" : "prev";
          master.direction = slider.direction;

          if (Math.ceil((target + 1)/slider.visible) - 1 !== slider.currentSlide && target !== 0) {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            target = Math.floor(target/slider.visible);
          } else {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            return false;
          }
        }

        slider.animating = true;
        slider.animatingTo = target;

        // SLIDESHOW:
        if (pause) slider.pause();

        // API: before() animation Callback
        slider.vars.before(slider);

        // SYNC:
        if (slider.syncExists && !fromNav) methods.sync("animate");

        // CONTROLNAV
        if (slider.vars.controlNav) methods.controlNav.active();

        // !CAROUSEL:
        // CANDIDATE: slide active class (for add/remove slide)
        if (!carousel) slider.slides.removeClass(namespace + 'active-slide').eq(target).addClass(namespace + 'active-slide');

        // INFINITE LOOP:
        // CANDIDATE: atEnd
        slider.atEnd = target === 0 || target === slider.last;

        // DIRECTIONNAV:
        if (slider.vars.directionNav) methods.directionNav.update();

        if (target === slider.last) {
          // API: end() of cycle Callback
          slider.vars.end(slider);
          // SLIDESHOW && !INFINITE LOOP:
          if (!slider.vars.animationLoop) slider.pause();
        }

        // SLIDE:
        if (!fade) {
          var dimension = (vertical) ? slider.slides.filter(':first').height() : slider.computedW,
              margin, slideString, calcNext;

          // INFINITE LOOP / REVERSE:
          if (carousel) {
            //margin = (slider.vars.itemWidth > slider.w) ? slider.vars.itemMargin * 2 : slider.vars.itemMargin;
            margin = slider.vars.itemMargin;
            calcNext = ((slider.itemW + margin) * slider.move) * slider.animatingTo;
            slideString = (calcNext > slider.limit && slider.visible !== 1) ? slider.limit : calcNext;
          } else if (slider.currentSlide === 0 && target === slider.count - 1 && slider.vars.animationLoop && slider.direction !== "next") {
            slideString = (reverse) ? (slider.count + slider.cloneOffset) * dimension : 0;
          } else if (slider.currentSlide === slider.last && target === 0 && slider.vars.animationLoop && slider.direction !== "prev") {
            slideString = (reverse) ? 0 : (slider.count + 1) * dimension;
          } else {
            slideString = (reverse) ? ((slider.count - 1) - target + slider.cloneOffset) * dimension : (target + slider.cloneOffset) * dimension;
          }
          slider.setProps(slideString, "", slider.vars.animationSpeed);
          if (slider.transitions) {
            if (!slider.vars.animationLoop || !slider.atEnd) {
              slider.animating = false;
              slider.currentSlide = slider.animatingTo;
            }
            slider.container.unbind("webkitTransitionEnd transitionend");
            slider.container.bind("webkitTransitionEnd transitionend", function() {
              slider.wrapup(dimension);
            });
          } else {
            slider.container.animate(slider.args, slider.vars.animationSpeed, slider.vars.easing, function(){
              slider.wrapup(dimension);
            });
          }
        } else { // FADE:
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeOut(slider.vars.animationSpeed, slider.vars.easing);
            //slider.slides.eq(target).fadeIn(slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

            slider.slides.eq(slider.currentSlide).css({"zIndex": 1}).animate({"opacity": 0}, slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.eq(target).css({"zIndex": 2}).animate({"opacity": 1}, slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

          } else {
            slider.slides.eq(slider.currentSlide).css({ "opacity": 0, "zIndex": 1 });
            slider.slides.eq(target).css({ "opacity": 1, "zIndex": 2 });
            slider.wrapup(dimension);
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) methods.smoothHeight(slider.vars.animationSpeed);
      }
    };
    slider.wrapup = function(dimension) {
      // SLIDE:
      if (!fade && !carousel) {
        if (slider.currentSlide === 0 && slider.animatingTo === slider.last && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpEnd");
        } else if (slider.currentSlide === slider.last && slider.animatingTo === 0 && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpStart");
        }
      }
      slider.animating = false;
      slider.currentSlide = slider.animatingTo;
      // API: after() animation Callback
      slider.vars.after(slider);
    };

    // SLIDESHOW:
    slider.animateSlides = function() {
      if (!slider.animating && focused ) slider.flexAnimate(slider.getTarget("next"));
    };
    // SLIDESHOW:
    slider.pause = function() {
      clearInterval(slider.animatedSlides);
      slider.animatedSlides = null;
      slider.playing = false;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) methods.pausePlay.update("play");
      // SYNC:
      if (slider.syncExists) methods.sync("pause");
    };
    // SLIDESHOW:
    slider.play = function() {
      if (slider.playing) clearInterval(slider.animatedSlides);
      slider.animatedSlides = slider.animatedSlides || setInterval(slider.animateSlides, slider.vars.slideshowSpeed);
      slider.started = slider.playing = true;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) methods.pausePlay.update("pause");
      // SYNC:
      if (slider.syncExists) methods.sync("play");
    };
    // STOP:
    slider.stop = function () {
      slider.pause();
      slider.stopped = true;
    };
    slider.canAdvance = function(target, fromNav) {
      // ASNAV:
      var last = (asNav) ? slider.pagingCount - 1 : slider.last;
      return (fromNav) ? true :
             (asNav && slider.currentItem === slider.count - 1 && target === 0 && slider.direction === "prev") ? true :
             (asNav && slider.currentItem === 0 && target === slider.pagingCount - 1 && slider.direction !== "next") ? false :
             (target === slider.currentSlide && !asNav) ? false :
             (slider.vars.animationLoop) ? true :
             (slider.atEnd && slider.currentSlide === 0 && target === last && slider.direction !== "next") ? false :
             (slider.atEnd && slider.currentSlide === last && target === 0 && slider.direction === "next") ? false :
             true;
    };
    slider.getTarget = function(dir) {
      slider.direction = dir;
      if (dir === "next") {
        return (slider.currentSlide === slider.last) ? 0 : slider.currentSlide + 1;
      } else {
        return (slider.currentSlide === 0) ? slider.last : slider.currentSlide - 1;
      }
    };

    // SLIDE:
    slider.setProps = function(pos, special, dur) {
      var target = (function() {
        var posCheck = (pos) ? pos : ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo,
            posCalc = (function() {
              if (carousel) {
                return (special === "setTouch") ? pos :
                       (reverse && slider.animatingTo === slider.last) ? 0 :
                       (reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                       (slider.animatingTo === slider.last) ? slider.limit : posCheck;
              } else {
                switch (special) {
                  case "setTotal": return (reverse) ? ((slider.count - 1) - slider.currentSlide + slider.cloneOffset) * pos : (slider.currentSlide + slider.cloneOffset) * pos;
                  case "setTouch": return (reverse) ? pos : pos;
                  case "jumpEnd": return (reverse) ? pos : slider.count * pos;
                  case "jumpStart": return (reverse) ? slider.count * pos : pos;
                  default: return pos;
                }
              }
            }());

            return (posCalc * -1) + "px";
          }());

      if (slider.transitions) {
        target = (vertical) ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
        dur = (dur !== undefined) ? (dur/1000) + "s" : "0s";
        slider.container.css("-" + slider.pfx + "-transition-duration", dur);
         slider.container.css("transition-duration", dur);
      }

      slider.args[slider.prop] = target;
      if (slider.transitions || dur === undefined) slider.container.css(slider.args);

      slider.container.css('transform',target);
    };

    slider.setup = function(type) {
      // SLIDE:
      if (!fade) {
        var sliderOffset, arr;

        if (type === "init") {
          slider.viewport = $('<div class="' + namespace + 'viewport"></div>').css({"overflow": "hidden", "position": "relative"}).appendTo(slider).append(slider.container);
          // INFINITE LOOP:
          slider.cloneCount = 0;
          slider.cloneOffset = 0;
          // REVERSE:
          if (reverse) {
            arr = $.makeArray(slider.slides).reverse();
            slider.slides = $(arr);
            slider.container.empty().append(slider.slides);
          }
        }
        // INFINITE LOOP && !CAROUSEL:
        if (slider.vars.animationLoop && !carousel) {
          slider.cloneCount = 2;
          slider.cloneOffset = 1;
          // clear out old clones
          if (type !== "init") slider.container.find('.clone').remove();
          // slider.container.append(slider.slides.first().clone().addClass('clone').attr('aria-hidden', 'true')).prepend(slider.slides.last().clone().addClass('clone').attr('aria-hidden', 'true'));
		      methods.uniqueID( slider.slides.first().clone().addClass('clone').attr('aria-hidden', 'true') ).appendTo( slider.container );
		      methods.uniqueID( slider.slides.last().clone().addClass('clone').attr('aria-hidden', 'true') ).prependTo( slider.container );
        }
        slider.newSlides = $(slider.vars.selector, slider);

        sliderOffset = (reverse) ? slider.count - 1 - slider.currentSlide + slider.cloneOffset : slider.currentSlide + slider.cloneOffset;
        // VERTICAL:
        if (vertical && !carousel) {
          slider.container.height((slider.count + slider.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
          setTimeout(function(){
            slider.newSlides.css({"display": "block"});
            slider.doMath();
            slider.viewport.height(slider.h);
            slider.setProps(sliderOffset * slider.h, "init");
          }, (type === "init") ? 100 : 0);
        } else {
          slider.container.width((slider.count + slider.cloneCount) * 200 + "%");
          slider.setProps(sliderOffset * slider.computedW, "init");
          setTimeout(function(){
            slider.doMath();
            slider.newSlides.css({"width": slider.computedW, "float": "left", "display": "block"});
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) methods.smoothHeight();
          }, (type === "init") ? 100 : 0);
        }
      } else { // FADE:
        slider.slides.css({"width": "100%", "float": "left", "marginRight": "-100%", "position": "relative"});
        if (type === "init") {
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeIn(slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.css({ "opacity": 0, "display": "block", "zIndex": 1 }).eq(slider.currentSlide).css({"zIndex": 2}).animate({"opacity": 1},slider.vars.animationSpeed,slider.vars.easing);
          } else {
            slider.slides.css({ "opacity": 0, "display": "block", "webkitTransition": "opacity " + slider.vars.animationSpeed / 1000 + "s ease", "zIndex": 1 }).eq(slider.currentSlide).css({ "opacity": 1, "zIndex": 2});
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) methods.smoothHeight();
      }
      // !CAROUSEL:
      // CANDIDATE: active slide
      if (!carousel) slider.slides.removeClass(namespace + "active-slide").eq(slider.currentSlide).addClass(namespace + "active-slide");

      //FlexSlider: init() Callback
      slider.vars.init(slider);
    };

    slider.doMath = function() {
      var slide = slider.slides.first(),
          slideMargin = slider.vars.itemMargin,
          minItems = slider.vars.minItems,
          maxItems = slider.vars.maxItems;

      slider.w = (slider.viewport===undefined) ? slider.width() : slider.viewport.width();
      slider.h = slide.height();
      slider.boxPadding = slide.outerWidth() - slide.width();

      // CAROUSEL:
      if (carousel) {
        slider.itemT = slider.vars.itemWidth + slideMargin;
        slider.minW = (minItems) ? minItems * slider.itemT : slider.w;
        slider.maxW = (maxItems) ? (maxItems * slider.itemT) - slideMargin : slider.w;
        slider.itemW = (slider.minW > slider.w) ? (slider.w - (slideMargin * (minItems - 1)))/minItems :
                       (slider.maxW < slider.w) ? (slider.w - (slideMargin * (maxItems - 1)))/maxItems :
                       (slider.vars.itemWidth > slider.w) ? slider.w : slider.vars.itemWidth;

        slider.visible = Math.floor(slider.w/(slider.itemW));
        slider.move = (slider.vars.move > 0 && slider.vars.move < slider.visible ) ? slider.vars.move : slider.visible;
        slider.pagingCount = Math.ceil(((slider.count - slider.visible)/slider.move) + 1);
        slider.last =  slider.pagingCount - 1;
        slider.limit = (slider.pagingCount === 1) ? 0 :
                       (slider.vars.itemWidth > slider.w) ? (slider.itemW * (slider.count - 1)) + (slideMargin * (slider.count - 1)) : ((slider.itemW + slideMargin) * slider.count) - slider.w - slideMargin;
      } else {
        slider.itemW = slider.w;
        slider.pagingCount = slider.count;
        slider.last = slider.count - 1;
      }
      slider.computedW = slider.itemW - slider.boxPadding;
    };

    slider.update = function(pos, action) {
      slider.doMath();

      // update currentSlide and slider.animatingTo if necessary
      if (!carousel) {
        if (pos < slider.currentSlide) {
          slider.currentSlide += 1;
        } else if (pos <= slider.currentSlide && pos !== 0) {
          slider.currentSlide -= 1;
        }
        slider.animatingTo = slider.currentSlide;
      }

      // update controlNav
      if (slider.vars.controlNav && !slider.manualControls) {
        if ((action === "add" && !carousel) || slider.pagingCount > slider.controlNav.length) {
          methods.controlNav.update("add");
        } else if ((action === "remove" && !carousel) || slider.pagingCount < slider.controlNav.length) {
          if (carousel && slider.currentSlide > slider.last) {
            slider.currentSlide -= 1;
            slider.animatingTo -= 1;
          }
          methods.controlNav.update("remove", slider.last);
        }
      }
      // update directionNav
      if (slider.vars.directionNav) methods.directionNav.update();

    };

    slider.addSlide = function(obj, pos) {
      var $obj = $(obj);

      slider.count += 1;
      slider.last = slider.count - 1;

      // append new slide
      if (vertical && reverse) {
        (pos !== undefined) ? slider.slides.eq(slider.count - pos).after($obj) : slider.container.prepend($obj);
      } else {
        (pos !== undefined) ? slider.slides.eq(pos).before($obj) : slider.container.append($obj);
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.update(pos, "add");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      //FlexSlider: added() Callback
      slider.vars.added(slider);
    };
    slider.removeSlide = function(obj) {
      var pos = (isNaN(obj)) ? slider.slides.index($(obj)) : obj;

      // update count
      slider.count -= 1;
      slider.last = slider.count - 1;

      // remove slide
      if (isNaN(obj)) {
        $(obj, slider.slides).remove();
      } else {
        (vertical && reverse) ? slider.slides.eq(slider.last).remove() : slider.slides.eq(obj).remove();
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.doMath();
      slider.update(pos, "remove");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      // FlexSlider: removed() Callback
      slider.vars.removed(slider);
    };

    //FlexSlider: Initialize
    methods.init();
  };

  // Ensure the slider isn't focussed if the window loses focus.
  $( window ).blur( function ( e ) {
    focused = false;
  }).focus( function ( e ) {
    focused = true;
  });

  //FlexSlider: Default Settings
  $.flexslider.defaults = {
    namespace: "flex-",             //{NEW} String: Prefix string attached to the class of every element generated by the plugin
    selector: ".slides > li",       //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
    animation: "fade",              //String: Select your animation type, "fade" or "slide"
    easing: "swing",                //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
    direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
    reverse: false,                 //{NEW} Boolean: Reverse the animation direction
    animationLoop: true,            //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
    smoothHeight: false,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
    startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
    slideshow: true,                //Boolean: Animate slider automatically
    slideshowSpeed: 7000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 600,            //Integer: Set the speed of animations, in milliseconds
    initDelay: 0,                   //{NEW} Integer: Set an initialization delay, in milliseconds
    randomize: false,               //Boolean: Randomize slide order
    thumbCaptions: false,           //Boolean: Whether or not to put captions on thumbnails when using the "thumbnails" controlNav.

    // Usability features
    pauseOnAction: true,            //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
    pauseOnHover: false,            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
    pauseInvisible: true,   		//{NEW} Boolean: Pause the slideshow when tab is invisible, resume when visible. Provides better UX, lower CPU usage.
    useCSS: true,                   //{NEW} Boolean: Slider will use CSS3 transitions if available
    touch: true,                    //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
    video: false,                   //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

    // Primary Controls
    controlNav: true,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
    directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
    prevText: "Previous",           //String: Set the text for the "previous" directionNav item
    nextText: "Next",               //String: Set the text for the "next" directionNav item

    // Secondary Navigation
    keyboard: true,                 //Boolean: Allow slider navigating via keyboard left/right keys
    multipleKeyboard: false,        //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
    mousewheel: false,              //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
    pausePlay: false,               //Boolean: Create pause/play dynamic element
    pauseText: "Pause",             //String: Set the text for the "pause" pausePlay item
    playText: "Play",               //String: Set the text for the "play" pausePlay item

    // Special properties
    controlsContainer: "",          //{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
    manualControls: "",             //{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
    sync: "",                       //{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
    asNavFor: "",                   //{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

    // Carousel Options
    itemWidth: 0,                   //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
    itemMargin: 0,                  //{NEW} Integer: Margin between carousel items.
    minItems: 1,                    //{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
    maxItems: 0,                    //{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
    move: 0,                        //{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
    allowOneSlide: true,           //{NEW} Boolean: Whether or not to allow a slider comprised of a single slide

    // Callback API
    start: function(){},            //Callback: function(slider) - Fires when the slider loads the first slide
    before: function(){},           //Callback: function(slider) - Fires asynchronously with each slider animation
    after: function(){},            //Callback: function(slider) - Fires after each slider animation completes
    end: function(){},              //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
    added: function(){},            //{NEW} Callback: function(slider) - Fires after a slide is added
    removed: function(){},           //{NEW} Callback: function(slider) - Fires after a slide is removed
    init: function() {}             //{NEW} Callback: function(slider) - Fires after the slider is initially setup
  };

  //FlexSlider: Plugin Function
  $.fn.flexslider = function(options) {
    if (options === undefined) options = {};

    if (typeof options === "object") {
      return this.each(function() {
        var $this = $(this),
            selector = (options.selector) ? options.selector : ".slides > li",
            $slides = $this.find(selector);

      if ( ( $slides.length === 1 && options.allowOneSlide === true ) || $slides.length === 0 ) {
          $slides.fadeIn(400);
          if (options.start) options.start($this);
        } else if ($this.data('flexslider') === undefined) {
          new $.flexslider(this, options);
        }
      });
    } else {
      // Helper strings to quickly perform functions on the slider
      var $slider = $(this).data('flexslider');
      switch (options) {
        case "play": $slider.play(); break;
        case "pause": $slider.pause(); break;
        case "stop": $slider.stop(); break;
        case "next": $slider.flexAnimate($slider.getTarget("next"), true); break;
        case "prev":
        case "previous": $slider.flexAnimate($slider.getTarget("prev"), true); break;
        default: if (typeof options === "number") $slider.flexAnimate(options, true);
      }
    }
  };
})(jQuery);
;
/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158; 
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */;
/*! Copyright (c) 2013 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.1.3
 *
 * Requires: 1.2.2+
 */

(function (factory) {
    if ( typeof define === 'function' && define.amd ) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'];
    var toBind = 'onwheel' in document || document.documentMode >= 9 ? ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'];
    var lowestDelta, lowestDeltaXY;

    if ( $.event.fixHooks ) {
        for ( var i = toFix.length; i; ) {
            $.event.fixHooks[ toFix[--i] ] = $.event.mouseHooks;
        }
    }

    $.event.special.mousewheel = {
        setup: function() {
            if ( this.addEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.addEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = handler;
            }
        },

        teardown: function() {
            if ( this.removeEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.removeEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = null;
            }
        }
    };

    $.fn.extend({
        mousewheel: function(fn) {
            return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
        },

        unmousewheel: function(fn) {
            return this.unbind("mousewheel", fn);
        }
    });


    function handler(event) {
        var orgEvent = event || window.event,
            args = [].slice.call(arguments, 1),
            delta = 0,
            deltaX = 0,
            deltaY = 0,
            absDelta = 0,
            absDeltaXY = 0,
            fn;
        event = $.event.fix(orgEvent);
        event.type = "mousewheel";

        // Old school scrollwheel delta
        if ( orgEvent.wheelDelta ) { delta = orgEvent.wheelDelta; }
        if ( orgEvent.detail )     { delta = orgEvent.detail * -1; }

        // New school wheel delta (wheel event)
        if ( orgEvent.deltaY ) {
            deltaY = orgEvent.deltaY * -1;
            delta  = deltaY;
        }
        if ( orgEvent.deltaX ) {
            deltaX = orgEvent.deltaX;
            delta  = deltaX * -1;
        }

        // Webkit
        if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY; }
        if ( orgEvent.wheelDeltaX !== undefined ) { deltaX = orgEvent.wheelDeltaX * -1; }

        // Look for lowest delta to normalize the delta values
        absDelta = Math.abs(delta);
        if ( !lowestDelta || absDelta < lowestDelta ) { lowestDelta = absDelta; }
        absDeltaXY = Math.max(Math.abs(deltaY), Math.abs(deltaX));
        if ( !lowestDeltaXY || absDeltaXY < lowestDeltaXY ) { lowestDeltaXY = absDeltaXY; }

        // Get a whole value for the deltas
        fn = delta > 0 ? 'floor' : 'ceil';
        delta  = Math[fn](delta / lowestDelta);
        deltaX = Math[fn](deltaX / lowestDeltaXY);
        deltaY = Math[fn](deltaY / lowestDeltaXY);

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

}));
;
/**
 * Everslider - Responsive jQuery Carousel Plugin
 * http://plugins.gravitysign.com/everslider
 * Copyright (c) 2013 Roman Yurchuk
 * Version 1.6.1
*/

(function($){

	"use strict";
		
	// Function to get vendor-specific CSS3 style property (e.g. WebkitTransition)
	// ---------------------------------------------------------------------------
	
	function getVendorProperty(name){
		var property = false, prefix = ['Webkit','Moz','O','ms'];		
		var test = document.createElement('div');
		// check unprefixed property
		if(typeof test.style[name] === 'string') {
			property = name;
		} else {
			var name_u = name.charAt(0).toUpperCase() + name.substr(1);
			for(var p in prefix) {
				if(typeof test.style[prefix[p]+name_u] === 'string') {
					property = prefix[p]+name_u;
					break;
				}
			}
		}
		// prevent memory leaks in IE
		test = null;
		// return property name or 'false'
		return property;
	}
	
	
	// Function to get vendor specific CSS3 prefix (-webkit-,-moz-,-ms-,-o- ...)
	// ---------------------------------------------------------------------------
	
	function getVendorPrefix() {
		var prefix = {
			WebkitTransition: '-webkit-',
			MozTransition: '-moz-',
			msTransition: '-ms-',
			OTransition: '-o-',
			transition: ''
		};
		
		// return "-webkit-" prefix for Safari and Chrome
		if(/(Safari|Chrome)/.test(navigator.userAgent)) {
			return prefix['WebkitTransition'];
		}
		
		return prefix[getVendorProperty('transition')];
	}
	
	
	// Function to detect 3D support in browser
	// ---------------------------------------------------------------------------
	
	function checkTransform3D(){
		var support = false, test = document.createElement('div');
		var transform = getVendorProperty('transform');
		// apply 3d property and get its value back
		test.style[transform] = 'rotateY(45deg)';
		if(test.style[transform] !== '') {
			support = true;
		}
		// prevent memory leaks in IE
		test = null;
		return support;
	}
	
	
	// Function to get pixel offset for element (CSS3 "translate" or box model)
	// ---------------------------------------------------------------------------
	
	function getPixelOffset(element, cssok) {
		var transform = getVendorProperty('transform'); 
		var position = {left:0, top:0};
		if(transform && cssok) {
			var matrix = element.css(transform);
			if(matrix.indexOf('matrix') === 0) {
				matrix = matrix.split('(')[1].split(')')[0].split(/,\s*/);
				position.left = parseInt(matrix[4],10);
				position.top = parseInt(matrix[5],10);
			}
		} else {
			position = element.position();
		}
		return position;
	}
	
	
	// CSS3 variables
	// ---------------------------------------------------------------------------	
	
	var transition = getVendorProperty('transition');
	var transform = getVendorProperty('transform');
	var cssprefix = getVendorPrefix();
	var transform3d = checkTransform3D();	
	
	
	// Function to translate list position with "translate3d()"
	// ---------------------------------------------------------------------------		
	
	function translate(element, pixels, animate){
		if(typeof animate === 'object') {
			// setup transition for "transform" property
			var property = cssprefix + 'transform';
			setTransition(element, property, animate.duration, animate.easing, animate.delay, animate.complete);
			// run "animate.complete()" if there is no value change in CSS3 "transform" 
			// property (in this case "transitionend" event won't fire)
			if(pixels === getPixelOffset(element, true).left) {
				animate.complete.call(element, property);
			}
		}
		// apply style
		if( transform3d ) {
			element.css(transform, 'translate3d(' + parseInt(pixels, 10) + 'px, 0px, 0px)');
		} else {
			element.css(transform, 'translate(' + parseInt(pixels, 10) + 'px, 0px)');
		}
	}
	
	
	// Function to apply CSS3 transition to element. Additional "complete" function
	// can be provided to run as soon as transition ends
	// ---------------------------------------------------------------------------
	
	function setTransition(element, properties, duration, easing, delay, complete){
		// map jquery easing to cubic-bezier for CSS3 transition
		var easing_map = {
			'linear':         'linear',
			'swing':          'cubic-bezier(.02,.01,.47,1)',
			'easeOutCubic':   'cubic-bezier(.215,.61,.355,1)',
			'easeInOutCubic': 'cubic-bezier(.645,.045,.355,1)',
			'easeInCirc':     'cubic-bezier(.6,.04,.98,.335)',
			'easeOutCirc':    'cubic-bezier(.075,.82,.165,1)',
			'easeInOutCirc':  'cubic-bezier(.785,.135,.15,.86)',
			'easeInExpo':     'cubic-bezier(.95,.05,.795,.035)',
			'easeOutExpo':    'cubic-bezier(.19,1,.22,1)',
			'easeInOutExpo':  'cubic-bezier(1,0,0,1)',
			'easeInQuad':     'cubic-bezier(.55,.085,.68,.53)',
			'easeOutQuad':    'cubic-bezier(.25,.46,.45,.94)',
			'easeInOutQuad':  'cubic-bezier(.455,.03,.515,.955)',
			'easeInQuart':    'cubic-bezier(.895,.03,.685,.22)',
			'easeOutQuart':   'cubic-bezier(.165,.84,.44,1)',
			'easeInOutQuart': 'cubic-bezier(.77,0,.175,1)',
			'easeInQuint':    'cubic-bezier(.755,.05,.855,.06)',
			'easeOutQuint':   'cubic-bezier(.23,1,.32,1)',
			'easeInOutQuint': 'cubic-bezier(.86,0,.07,1)',
			'easeInSine':     'cubic-bezier(.47,0,.745,.715)',
			'easeOutSine':    'cubic-bezier(.39,.575,.565,1)',
			'easeInOutSine':  'cubic-bezier(.445,.05,.55,.95)',
			'easeInBack':     'cubic-bezier(.6,-.28,.735,.045)',
			'easeOutBack':    'cubic-bezier(.175, .885,.32,1.275)',
			'easeInOutBack':  'cubic-bezier(.68,-.55,.265,1.55)'
		};
		// map css3 transition property to vendor specific event name
		var event_map = {				
			'transition': 'transitionend',
			'OTransition': 'oTransitionEnd otransitionend',
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend'
		};
					
		// argument defaults
		properties = properties.split(/\s+/);
		duration = (parseInt(duration,10) / 1000 || 0) + 's';
		easing = easing_map[easing] || easing_map['swing'];
		// if delay has been ommited in favour of complete function
		if(typeof delay === 'function') {
			complete = delay;
			delay = 0;
		}
		delay = (parseInt(delay,10) / 1000 || 0) + 's';
		complete = complete || $.noop;
		
		// save old transition
		var transition_tmp = element.css(transition);
											
		// register "transitionend" event handler
		element.bind(event_map[transition], function(e){
			var event = e.originalEvent;
			// make sure we deal with event target but not its descendants
			if(event.target === this) {
				complete.call(element, event.propertyName);
				$(this).css(transition, transition_tmp).unbind(e);
			}
			e.stopPropagation();
		});
								
		// set transition for element
		var string = '';
		for(var n = 0; n < properties.length; n++) {
			string += properties[n] + ' ' + duration + ' ' + easing + ' ' + delay + ', ';
		}
		element.css(transition, string.slice(0,-2));
	}
		
			
	// Constructor
	// ---------------------------------------------------------------------------
	
	function Slider(container, settings){
		this.offset = 0;
		this.visible = 0;
		this.lock = false;
		this.timer = 0;
		this.api = {};
		this.settings = settings;
		this.container = $(container);		
		this.list = this.container.find('ul.es-slides');
		this.total = this.list.children('li').length;
		this.slide = this.list.children('li:first-child');
		this.cssok = getVendorProperty('transition') && this.settings.useCSS;
		var self = this;
		
		// return if .es-slides list doesn't exist or has no slides
		if(this.total === 0) {
			return null;
		}
				
		// set move slides number
		if(this.settings.moveSlides === 'auto') {
			this.settings.moveSlides = 9999; // will be forced to visible
		} else {
			this.settings.moveSlides = parseInt(this.settings.moveSlides,10) || 1;
		}
		
		// set slide width (only >0)
		this.settings.itemWidth = parseInt(this.settings.itemWidth,10) || 0;			
		if(this.settings.itemWidth > 0) {
			this.list.children().css('width', this.settings.itemWidth);
		}
		
		// set slide height (only >0)
		this.settings.itemHeight = parseInt(this.settings.itemHeight,10) || 0;
		if(this.settings.itemHeight > 0) {
			this.list.children().css('height', this.settings.itemHeight);
		}
		
		// set slide margin
		if(this.settings.itemMargin !== false) {
			this.list.children().css('margin-right', parseInt(this.settings.itemMargin,10) || 0);
		}
		
		// if 'itemKeepRatio' is false set slide height to 'auto'
		if( !this.settings.itemKeepRatio ) {
			this.list.children().css({height:'auto'});
		}		
		
		// save size of first slide for further calculations
		this.slide_width = this.slide.width();
		this.slide_margin = parseInt(this.slide.css('margin-right'), 10) || 0;
		this.slide_ratio = this.slide.height()/this.slide.width();
				
		// set viewport width
		if(this.settings.maxVisible > 0) {
			var max_width = this.settings.maxVisible * (this.slide_width + this.slide_margin) - this.slide_margin;
			this.container.css('max-width', max_width);
		} else {
			this.container.css('max-width', this.settings.maxWidth);
		}

		// carousel mode
		if(this.settings.mode === 'carousel') {		
			var cloned_before = this.list.children().clone(true), 
				cloned_after = this.list.children().clone(true);
			
			// add cloned slides before and after original slides
			this.list.prepend(document.createComment(' END CLONED ')).prepend(cloned_before).
			prepend(document.createComment(' BEGIN CLONED '));
			this.list.append(document.createComment(' BEGIN CLONED ')).append(cloned_after).
			append(document.createComment(' END CLONED '));
			
			// change offset and total
			this.offset = this.total;
			this.total = this.total * 3;
			
			// set initial list offset
			var p = this.offset * (this.slide_width + this.slide_margin);
			if(this.cssok) {
				translate(this.list, -p);
			} else {
				this.list.css('left', -p);
			}			
		}
		
		// ticker
		if(this.settings.ticker && this.settings.mode !== 'normal') {
			this.enableTicker();
		}
							
		// navigation
		if(this.settings.navigation) {
			this.container.append(['<div class="es-navigation">',
			'<a href="" class="es-prev">' + this.settings.prevNav + '</a>',
			'<a href="" class="es-next">' + this.settings.nextNav + '</a>',
			'</div>'].join('\n'));
			// update navigation 
			this.updateNavigation(this.offset);
			// assing click handlers
			this.container.find('.es-prev').click(function(e){
				self.slidePrevious();
				e.preventDefault();
			}).end().find('.es-next').click(function(e){
				self.slideNext();						
				e.preventDefault();
			}).end();
		}
		
		// pagination
		if(this.settings.pagination && this.settings.mode !== 'carousel') {
			this.container.append('<div class="es-pagination"></div>');
		}
		
		// touchSwipe navigation
		if(this.settings.touchSwipe) {
			this.enableTouchSwipe();
		}
		
		// mousewheel navigation
		if(this.settings.mouseWheel) {
			this.enableMouseWheel();
		}
		
		// keyboard navigation
		if(this.settings.keyboard) {
			this.enableKeyboard();
		}
		
		// slide auto resize
		$(window).bind('resize', function(){
			window.clearTimeout(self.timer);
			self.timer = window.setTimeout(function(){
				self.resizeSlides();
			}, self.settings.fitDelay);
		}).trigger('resize');
								
		// create slider API
		$.extend(this.api, {	
			slideNext: function(){
				self.slideNext();
			},
			slidePrevious: function(){
				self.slidePrevious();
			},
			slideTo: function(p) {
				self.slideTo(p);
			},
			isSliding: function(){
				return self.isSliding();
			},
			getVisibleSlides: function(){
				return self.getVisibleSlides();
			},
			tickerPause: function(){
				if('tickerPause' in self) {
					self.tickerPause();
				}
			},
			tickerPlay: function(){
				if('tickerPlay' in self) {
					self.tickerPlay();
				}
			}
		});
				
		// make API methods available through data and event
		this.container.data('everslider', this.api);
		this.container.bind('everslider', function(e, method, param){	
			if(method in self.api) {
				self.api[method](param);
			}
			return false;
		});
		
		// when slides ready
		window.setTimeout(function(){
			// assign ready class to the container
			self.container.addClass('es-slides-ready');
			// add 'es-after-slide' class to visible slides
			self.getVisibleSlides().addClass('es-after-slide');
			// run slidesReady() callback
			if(typeof self.settings.slidesReady === 'function') {
				self.settings.slidesReady.call(self.container.get(0), self.api);
			}
		}, parseInt(this.settings.fitDelay,10) + parseInt(this.settings.fitDuration,10));
	}
	
	
	// Method to move slider next (API method)
	// ---------------------------------------------------------------------------
	
	Slider.prototype.slideNext = function(){
		if( !this.lock ) {
			this.slideOffset(this.getOffset('next'));
		}
	};
	
	
	// Method to move slider previous (API method)
	// ---------------------------------------------------------------------------
	
	Slider.prototype.slidePrevious = function(){
		if( !this.lock ) {
			this.slideOffset(this.getOffset('prev'));
		}
	};
	
	
	// Method to move slider to specific zero-based position (API method)
	// ---------------------------------------------------------------------------
	
	Slider.prototype.slideTo = function(p) {
		// normalize position for "carousel" mode
		if(this.settings.mode === 'carousel') {
			p = this.total/3 + Math.min(p, this.total/3 - this.visible);
		}
		var position_offset = p - this.offset;
		var direction = position_offset > 0 ? 'next' : 'prev';
		// save original offset
		var offset_tmp = this.offset;
		// shift carousel to requested position N times to get new offset
		for(var n = 0; n < Math.abs(position_offset); n++){
			this.offset = this.getOffset(direction);
		}
		// save received offset
		var offset = this.offset;
		this.offset = offset_tmp;
		// start main carousel transition
		this.slideOffset(offset);
	};
	
	
	// Method to find out if carousel is sliding (API method)
	// ---------------------------------------------------------------------------
	
	Slider.prototype.isSliding = function(){
		return this.lock;
	};

			
	// Method to get subset of slides that are visible at the moment (API method)
	// ---------------------------------------------------------------------------
	
	Slider.prototype.getVisibleSlides = function(){
		return this.list.children().slice(this.offset, this.offset + this.visible);
	};
	
	
	// Method to get offset number based on slide direction
	// ---------------------------------------------------------------------------

	Slider.prototype.getOffset = function(direction){
		// don't move more slides than visible
		var slide_limit = Math.min(this.settings.moveSlides, this.visible);
		
		// get offset for "prev" direction
		if (direction === 'prev') {
			if(this.settings.mode === 'carousel' && this.offset === 0) {
				var p = this.total/3 * (this.slide.width() + this.slide_margin);
				if(this.cssok) {
					if(this.settings.effect !== 'fade') translate(this.list, -p);
				} else {
					if(this.settings.effect !== 'fade') this.list.css('left', -p); 
				}
				return this.total/3 - slide_limit;
			} else if(this.settings.mode === 'circular' && this.offset === 0) {
				return this.total - this.visible;
			} else {
				return this.offset - (this.offset > slide_limit ? slide_limit : this.offset);
			}
		}

		// get offset for "next" direction
		if(direction === 'next') {
			var left = this.total - (this.offset + this.visible);
			if(this.settings.mode === 'carousel' && left === 0) {
				var p = (this.offset - this.total/3) * (this.slide.width() + this.slide_margin);
				if(this.cssok) {
					if(this.settings.effect !== 'fade') translate(this.list, -p);
				} else {
					if(this.settings.effect !== 'fade') this.list.css('left', -p);
				}
				return this.offset - this.total/3 + slide_limit;
			} else if(this.settings.mode === 'circular' && left === 0) {
				return 0;
			} else {
				return this.offset + (left > slide_limit ? slide_limit : left);
			}
		} 
	};

	
	// Method to change slider position by the amount of offset
	// ---------------------------------------------------------------------------
	
	Slider.prototype.slideOffset = function(offset, force){
		// return if offset won't change (force param must be unset)
		if(!force && offset === this.offset) {
			return;
		}
		
		var self = this;
		var unlock = function(){
			// unlock slides
			self.lock = false;
			// save new offset
			self.offset = offset; 
			// callbacks and events that run after slide transition completes
			if(!force) {
				// sync container height
				self.syncContainerHeight();
				// when transition has completed add 'es-after-slide' class to visible items
				self.list.children('.es-after-slide').removeClass('es-after-slide');
				self.getVisibleSlides().removeClass('es-before-slide').addClass('es-after-slide').
				trigger('es-after-slide');
				// run "afterSlide" callback
				if(typeof self.settings.afterSlide === 'function') {
					self.settings.afterSlide.call(self.container.get(0), self.getVisibleSlides());
				}
			}
		};
		
		// set lock
		this.lock = true;
		
		// callbacks and events that run before slide transition begins
		if(!force) {
			// add 'es-before-slide' class to the new visible items as well as trigger custom event
			this.list.children().slice(offset, offset + this.visible).not('.es-after-slide').
			addClass('es-before-slide').trigger('es-before-slide');
			
			// run "beforeSlide" callback
			if(typeof this.settings.beforeSlide === 'function') {
				this.settings.beforeSlide.call(this.container.get(0), this.getVisibleSlides());
			}
		}
		
		// update pagination
		if(this.settings.pagination && this.settings.mode !== 'carousel') {
			var slide_limit = Math.min(this.settings.moveSlides, this.visible);
			var active_page = Math.ceil(offset / slide_limit);
			this.container.find('.es-pagination a:eq('+active_page+')').addClass('es-active').
			siblings().removeClass('es-active');
		}
		
		// update navigation 
		this.updateNavigation(offset);
								
		// get pixel offset by offset number
		var pixel_offset = offset * (this.slide.width() + this.slide_margin);
								
		// the code below performs main carousel transition using either slide or fade effect;
		// code is split into two blocks: if browser is CSS3 compliant and if it's not, in which case it uses jQuery $.animate;
		// at the end of transition unlock() function will be executed to run callbacks
		if( this.cssok ) {
			// 'fade' animation effect
			if(this.settings.effect === 'fade') {
				var now_visible = this.getVisibleSlides();
				var next_visible = this.list.children().slice(offset, offset + this.visible);
				// when 'fadeDirection' plugin option is set to -1 we reverse fade direction
				if(this.settings.fadeDirection * offset > this.offset * this.settings.fadeDirection) {
					next_visible = Array.prototype.reverse.call(next_visible);
					now_visible = Array.prototype.reverse.call(now_visible);
				}
				// hide currently visible slides using delayed fade-out animation
				$.each(now_visible, function(n){
					setTransition($(this), 'opacity', 
					self.settings.fadeDuration, self.settings.fadeEasing, self.settings.fadeDelay * n, function(){
						if(n < self.visible - 1) return; // wait for last slide
						// set opacity = 0 on slides that will be visible next moment; 
						// translate list by amount of offset, then run fade-in animation for next slides 
						next_visible.css('opacity',0); translate(self.list, -pixel_offset, { 
							duration: 0, easing: 'linear', delay: 15, complete: function(){
								$.each(next_visible, function(n){
									setTransition($(this), 'opacity', 
									self.settings.fadeDuration, self.settings.fadeEasing, 
									self.settings.fadeDelay * n, function(){ 
										if(n < self.visible - 1) return; // wait for last slide
										now_visible.add(next_visible).css('opacity', '');
										unlock();
									});
									$(this).css('opacity', 1); 
								});
							} 
						});
					});
					$(this).css('opacity', 0);
				});
			} else {
				// 'slide' animation effector
				translate(this.list, -pixel_offset, {
					duration: this.settings.slideDuration,
					easing: this.settings.slideEasing,
					delay: this.settings.slideDelay,
					complete: unlock
				});				
			}
		} else {
			// 'fade' animation effect (jQuery fallback)
			if(this.settings.effect === 'fade') {
				var now_visible = this.getVisibleSlides();
				var next_visible = this.list.children().slice(offset, offset + this.visible);
				// when 'fadeDirection' plugin option is set to -1 we reverse fade direction
				if(this.settings.fadeDirection * offset > this.offset * this.settings.fadeDirection) {
					next_visible = Array.prototype.reverse.call(next_visible);
					now_visible = Array.prototype.reverse.call(now_visible);
				}
				// hide currently visible slides using delayed fade-out animation
				$.each(now_visible, function(n){
					$(this).stop().delay(self.settings.fadeDelay * n).animate({opacity: 0},
					self.settings.fadeDuration, self.settings.fadeEasing, function(){
						if(n < self.visible - 1) return; // wait for last slide
						next_visible.css('opacity', 0); self.list.delay(10).queue(function(){
							$(this).css('left', -pixel_offset).dequeue();
							$.each(next_visible, function(n){
								$(this).stop().delay(self.settings.fadeDelay * n).animate({opacity: 1},
								self.settings.fadeDuration, self.settings.fadeEasing, function(){
									if(n < self.visible - 1) return; // wait for last slide
									now_visible.add(next_visible).css('opacity', '');
									unlock();
								});
							});						
						});
					});
				});
			} else {
				// 'slide' animation effect (jQuery fallback)
				this.list.stop().delay(this.settings.slideDelay).animate({left: -pixel_offset},
				this.settings.slideDuration, this.settings.slideEasing, unlock);
			}
		} // this.cssok
		
	};
	
					
	// Method to resize slides when browser width is changed
	// ---------------------------------------------------------------------------
	
	Slider.prototype.resizeSlides = function(){		
		// lock resize
		this.lock = true;
						
		// get number of slides that would fit into container viewport
		this.visible = this.container.width()/(this.slide_width + this.slide_margin);
		if( this.visible % 1 === 0 || this.visible % 1 < 0.5 ) {
			this.visible = Math.floor(this.visible) > 0 ? Math.floor(this.visible) : 1;
		} else {
			this.visible = Math.ceil(this.visible);
		}
									
		// calculate slide size using new "visible" value
		var width = (this.container.width() + this.slide_margin)/this.visible - this.slide_margin;
		var height = this.slide_ratio * width;
		var size = {width: Math.round(width)};
		if(this.settings.itemKeepRatio) {
			size.height = Math.round(height);
		}
		
		// correct slide list offset
		if(this.offset > 0) {
			// make sure offset number is correct after new "visible" value
			if(this.offset + this.visible > this.total) {
				this.offset = this.total - this.visible;
			}
			var pixel_offset = this.offset * (width + this.slide_margin);
			if(this.cssok) {
				translate(this.list, -pixel_offset);
			} else {
				this.list.css('left', -pixel_offset);
			}
		}
		
		// prepare to animation
		var self = this;
		var duration = this.settings.fitDuration;
		var	easing = this.settings.fitEasing;
		var unlock = function(){
			self.lock = false;
			self.syncContainerHeight();
		};

		// run animation to change slides size
		this.list.children().each(function(){
			if(self.cssok){
				// if width won't change run "unlock()" immediately
				if($(this).width() === Math.round(width)) {
					unlock(); 
				} else {				
					setTransition($(this), 'width height', duration, easing, unlock);
					$(this).css(size);
				}
			} else {
				$(this).stop().animate(size, duration, easing, unlock);
			}
		});
		
		// update pagination
		this.updatePagination();
	};
	
	
	// Method to sync container height with the highest visible slide
	// ---------------------------------------------------------------------------
	
	Slider.prototype.syncContainerHeight = function(){
		if(this.settings.syncHeight && !this.settings.itemKeepRatio) {
			// find highest visible slide
			var max_height = 0;
			$.each(this.getVisibleSlides(), function(){
				if($(this).height() > max_height) {
					max_height = $(this).height();
				}
			});
			// animate container height
			var duration = this.settings.syncHeightDuration, 
				easing = this.settings.syncHeightEasing;
			if(this.cssok) {
				setTransition(this.container, 'height', duration, easing);
				this.container.css('height', max_height);
			} else {
				this.container.stop().animate({height: max_height}, duration, easing);
			}
		}
	};
	
	
	// Method to create/update pagination bullets
	// ---------------------------------------------------------------------------
	
	Slider.prototype.updatePagination = function(){
		// do nothing if pagination is not enabled
		if( !this.settings.pagination || this.settings.mode === 'carousel') {
			return;
		}
		
		// calculate how many pages we need to display
		var self = this;
		var slide_limit = Math.min(this.settings.moveSlides, this.visible);
		var total_pages = Math.ceil(this.total * 2 / (slide_limit + this.visible));
					
		// re-add page bullets
		var pagination = this.container.find('.es-pagination').empty();
		for(var i = 0; i < total_pages; i++) {
			$('<a href="#">' + i + '</a>').click(
				(function(index){
					return function(e) {
						// check if not locked
						if(self.lock) { return; }
						// offset cannot be more then "total - visible" slides!
						var offset =  Math.min(index * slide_limit, self.total - self.visible);
						self.slideOffset(offset);
						e.preventDefault();
					};
				})(i)
			).appendTo(pagination);
		}
		
		// figure out active bullet
		var active_page = Math.ceil(this.offset / slide_limit);
		pagination.find('a:eq('+active_page+')').addClass('es-active').siblings().removeClass('es-active');
	};
	
	
	// Method to update navigation controls
	// ---------------------------------------------------------------------------
	
	Slider.prototype.updateNavigation = function(offset){
		// add classes to navigation to indicate reaching first/last slide
		if(this.settings.navigation && this.settings.mode === 'normal') {
			var	navigation = this.container.find('.es-navigation a');
			// add classes on first/last slide
			navigation.removeClass('es-first es-last');
            if(offset === 0) { 
                navigation.filter('.es-prev').addClass('es-first');
            }
			if(offset === this.total - this.visible){ 
                navigation.filter('.es-next').addClass('es-last'); 
            }
		}
	};
	
	
	// Method to enable touchSwipe functionality
	// ---------------------------------------------------------------------------
	
	Slider.prototype.enableTouchSwipe = function(){
		var self = this, swipe = false;
		var touch_x = 0, touch_y = 0, pixel_offset = 0;
		
		var swipeStart = function(e){
			var event = e;
			if(e.type.indexOf('touch') === 0) {
				event = e.originalEvent.changedTouches[0];
			}
			// start swipe if not locked
			if( !self.lock ) {
				swipe = true;
				// save touch point position
				touch_x = event.pageX; 
				touch_y = event.pageY;
				// save slides list position
				pixel_offset = getPixelOffset(self.list, self.cssok).left;
				// bind move handler
				self.container.bind('mousemove touchmove', swipeMove);
				// add grab class to container on swipe start
				self.container.addClass('es-swipe-grab');
			}
		};
										
		var swipeMove = function(e) {
			var event = e;
			if(e.type.indexOf('touch') === 0) {
				event = e.originalEvent.changedTouches[0];
			}
			// get distance from touch point to current pointer position
			var swipe_x = event.pageX - touch_x;
			var	swipe_y = event.pageY - touch_y;
			// check if threshold is exceeded
			if(Math.abs(swipe_x) < self.settings.swipeThreshold) {
				// apply visual offset only for "slide" effect
				if(self.settings.effect === 'slide'){
					if(self.cssok) {
						translate(self.list, pixel_offset + swipe_x);
					} else {
						self.list.css('left', pixel_offset + swipe_x);
					}
				}
			} else {
				// get swipe direction
				var swipe_direction = (swipe_x > 0) ? 'prev' : 'next';
				var offset = self.getOffset(swipe_direction);
				// main carousel transition
				self.slideOffset(offset);
				// remove move handler
				self.container.unbind('mousemove touchmove', swipeMove);
			}
			// check if vertical swipe should scroll page
			if( !self.settings.swipePage ) {
				e.preventDefault();
			}
		};
		
		var swipeEnd = function(){
			if( swipe ){
				// if carousel has moved slightly but threshold has not exceeded return it to previous position
				if(!self.lock && pixel_offset !== getPixelOffset(self.list, self.cssok).left) {
					self.slideOffset(self.offset, true);
				}
				// unbind move handler
				self.container.unbind('mousemove touchmove', swipeMove);
				swipe = false;
				// remove grab class from container on swipe end
				self.container.removeClass('es-swipe-grab');
			}
		};
		
		// register event listeners
		this.container.bind('mousedown touchstart', swipeStart);
		$('body').bind('mouseup touchend touchcancel', swipeEnd);
		
		// disable dragging
		this.container.bind('dragstart', function(e) {
			e.preventDefault();
		});
	};
	
	
	// Method to enable mousewheel
	// ---------------------------------------------------------------------------
	
	Slider.prototype.enableMouseWheel = function(){
		// check if mousewheel plugin is loaded
		if(typeof $.fn.mousewheel !== 'function') {
			return;
		}			
		// bind to mousewheel event
		var self = this;
		this.container.bind('mousewheel', function(e,delta){				
			if(delta > 0) {
				self.slidePrevious();
			} else {
				self.slideNext();
			}
			e.preventDefault();
		});
	};
	
	
	// Method to enable keyboard navigation
	// ---------------------------------------------------------------------------
	
	Slider.prototype.enableKeyboard	= function(){
		var self = this;
		$(document).bind('keydown', function(e){
			if(e.which === 39) {
				self.slideNext();
			} else if(e.which === 37) {
				self.slidePrevious();
			}
		});
	};
	
	
	// Method to enable ticker (autoplay)
	// ---------------------------------------------------------------------------
	
	Slider.prototype.enableTicker = function(){
		var self = this, first_run = true, ticker_timer, timeout;
		var delay = 0, duration = 0, ticker_timeout = parseInt(this.settings.tickerTimeout,10);
		
		// get delay and duration for animation effect
		if(this.settings.effect === 'fade') {
			delay = parseInt(this.settings.fadeDelay,10);
			duration = parseInt(this.settings.fadeDuration,10);
		} else {
			delay = parseInt(this.settings.slideDelay,10);
			duration = parseInt(this.settings.slideDuration,10);
		}
		
		// functions to start ticker
		this.tickerPlay = function(){
			this.container.find('.es-ticker a').hide().filter('.es-pause').show();
			// calculate timeout
			if( first_run ) {
				timeout = ticker_timeout;
			} else {
				if(self.settings.effect === 'fade') {
					timeout = ((self.visible - 1) * delay + self.visible * duration) + ticker_timeout;
				} else {
					timeout = (delay + duration) + ticker_timeout;
				}
			}
			// start timer
			window.clearInterval(ticker_timer);
			ticker_timer = window.setInterval(function(){
				self.slideNext();
				// after first run restart timer with new timeout
				if( first_run ) {
					first_run = false;
					self.tickerPlay();
				}
			}, timeout);
		};
		
		// function to pause ticker
		this.tickerPause = function(){
			this.container.find('.es-ticker a').hide().filter('.es-play').show();
			// stop timer
			window.clearInterval(ticker_timer);
			first_run = true;
		};
		
		// create play/pause controls
		this.container.append('<div class="es-ticker"></div>');
		$('<a href="#" class="es-play">' + this.settings.tickerPlay + '</a>').click(function(e){
			self.tickerPlay();
			e.preventDefault();
		}).appendTo(this.container.find('.es-ticker'));
		$('<a href="#" class="es-pause">' + this.settings.tickerPause + '</a>').click(function(e){
			self.tickerPause();
			e.preventDefault();
		}).appendTo(this.container.find('.es-ticker'));
		
		// pause ticker on hover (with delay)
		if(this.settings.tickerHover) {
			var hover_timer = 0;
			this.container.hover(function(){
				window.clearTimeout(hover_timer);
				hover_timer = window.setTimeout(function(){
					self.tickerPause();
				}, self.settings.tickerHoverDelay);
			}, function(){
				window.clearTimeout(hover_timer);
				hover_timer = window.setTimeout(function(){
					self.tickerPlay();
				}, self.settings.tickerHoverDelay);
			});
		}
		
		// stop ticker
		this.tickerPause();
		
		// start ticker if needed
		if(this.settings.tickerAutoStart) {
			this.tickerPlay();
		}
	};
	
	
	// ---------------------------------------------------------------------------
	// Register jQuery plugin
	// ---------------------------------------------------------------------------
	
	$.fn.everslider = function(c) {
		// default settings
		var s = $.extend({
			mode: 'normal',						// carousel mode - 'normal', 'circular' or 'carousel'
			effect: 'slide',					// animation effect - 'slide' or 'fade'
			useCSS: true,						// set false to disable CSS3 transitions
			itemWidth: false,					// slide width, px (or false for css value)
			itemHeight: false,					// slide height, px (or false for css value)
			itemMargin: false,					// slide margin, px (or false for css value)
			itemKeepRatio: true,				// during resize always retain side ratio of a slide
			maxWidth: '100%',					// max container width, px or %
			maxVisible: 0,						// show only N slides (overrides maxWidth, 0 to ignore)
			moveSlides: 1,						// number of slides to move or 'auto' to move all visible
			slideDelay: 0,						// slide effect initial delay, ms
			slideDuration: 500,					// slide effect duration, ms
			slideEasing: 'swing',				// slide effect easing
			fadeDelay: 200,						// fade effect delay, ms
			fadeDuration: 500,					// fade effect duration, ms
			fadeEasing: 'swing',				// fade effect easing
			fadeDirection: 1,					// 1 is default, set -1 to start fade from opposite side
			fitDelay: 300,						// slides fit (carousel resize) delay, ms
			fitDuration: 200,					// slides fit (carousel resize) duration, ms
			fitEasing: 'swing',					// slides fit (carousel resize) easing
			syncHeight: false,					// sync carousel height with largest visible slide
			syncHeightDuration: 200,			// carousel height sync duration, ms
			syncHeightEasing: 'swing',			// carousel height sync easing
			navigation: true,					// enable prev/next navigation
			nextNav: '<span>Next</span>',		// next navigation control, text/html
			prevNav: '<span>Previous</span>',	// previous navigation control, text/html
			pagination: true,					// enable pagination (only 'normal' or 'circular' mode)
			touchSwipe: true,					// enable touchSwipe
			swipeThreshold: 50,					// pixels to exceed to start slide transition
			swipePage: false,					// allow touchswipe to scroll page also
			mouseWheel: false,					// enable mousewheel (requires jquery-mousewheel plugin)
			keyboard: false,					// enable keyboard left/right key navigation
			ticker: false,						// enable ticker ('circular' or 'carousel' mode)
			tickerTimeout: 2000,				// ticker timeout, ms
			tickerAutoStart: true,				// start ticker when plugin loads
			tickerPlay: '<span>Play</span>',	// ticker play control, text/html
			tickerPause: '<span>Pause</span>',	// ticker pause control, text/html
			tickerHover: false,					// pause ticker on mousehover
			tickerHoverDelay: 300,				// delay before ticker will pause on mousehover
			slidesReady: function(){},			// slider ready callback
			beforeSlide: function(){},			// before slide callback
			afterSlide: function(){}			// after slide callback
		}, c);
			
		return this.each(function(){
			// create slider instance
			new Slider(this, s);
		});
	};

})(jQuery);;
/*
 * File: jquery.flexisel.js
 * Version: 1.0.2
 * Description: Responsive carousel jQuery plugin
 * Author: 9bit Studios
 * Copyright 2012, 9bit Studios
 * http://www.9bitstudios.com
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */
(function ($) {
    $.fn.flexisel = function(options) {
	
        var defaults = $.extend({
            visibleItems : 4,
            animationSpeed : 200,
            autoPlay : false,
            autoPlaySpeed : 3000,
            pauseOnHover : true,
            setMaxWidthAndHeight : false,
            enableResponsiveBreakpoints : true,
            clone : true,
            responsiveBreakpoints : {
                portrait: { 
                    changePoint:480,
                    visibleItems: 1
                }, 
                landscape: { 
                    changePoint:640,
                    visibleItems: 2
                },
                tablet: { 
                    changePoint:768,
                    visibleItems: 3
                }
            }
        }, options);
        
        /******************************
        Private Variables
         *******************************/
         
        var object = $(this);
        var settings = $.extend(defaults, options);
        var itemsWidth; // Declare the global width of each item in carousel
        var canNavigate = true;
        var itemsVisible = settings.visibleItems; // Get visible items
        var totalItems = object.children().length; // Get number of elements
        var responsivePoints = [];
        
        /******************************
        Public Methods
        *******************************/
        var methods = {
            init : function() {
                return this.each(function() {
                    methods.appendHTML();
                    methods.setEventHandlers();
                    methods.initializeItems();
                });
            },
		    
            /******************************
            Initialize Items
            Fully initialize everything. Plugin is loaded and ready after finishing execution
	    *******************************/
            initializeItems : function() {

                var listParent = object.parent();
                var innerHeight = listParent.height();
                var childSet = object.children();
                methods.sortResponsiveObject(settings.responsiveBreakpoints);
                
                var innerWidth = listParent.width(); // Set widths
                itemsWidth = (innerWidth) / itemsVisible;
                childSet.width(itemsWidth);        
                if (settings.clone) {
                    childSet.last().insertBefore(childSet.first());
                    childSet.last().insertBefore(childSet.first());
                    object.css({
                        'left' : -itemsWidth
                    });
                }

                object.fadeIn();
                $(window).trigger("resize"); // needed to position arrows correctly

            },
            
	    /******************************
            Append HTML
            Add additional markup needed by plugin to the DOM
	    *******************************/
            appendHTML : function() {
                object.addClass("nbs-flexisel-ul");
                object.wrap("<div class='nbs-flexisel-container'><div class='nbs-flexisel-inner'></div></div>");
                object.find("li").addClass("nbs-flexisel-item");

                if (settings.setMaxWidthAndHeight) {
                    var baseWidth = $(".nbs-flexisel-item img").width();
                    var baseHeight = $(".nbs-flexisel-item img").height();
                    $(".nbs-flexisel-item img").css("max-width", baseWidth);
                    $(".nbs-flexisel-item img").css("max-height", baseHeight);
                }
                $("<div class='nbs-flexisel-nav-left'></div><div class='nbs-flexisel-nav-right'></div>").insertAfter(object);
                if (settings.clone) {
                    var cloneContent = object.children().clone();
                    object.append(cloneContent);
                }
            },
            /******************************
            Set Event Handlers
	    Set events: click, resize, etc
            *******************************/
            setEventHandlers : function() {

                var listParent = object.parent();
                var childSet = object.children();
                var leftArrow = listParent.find($(".nbs-flexisel-nav-left"));
                var rightArrow = listParent.find($(".nbs-flexisel-nav-right"));

                $(window).on("resize", function(event) {

                    methods.setResponsiveEvents();

                    var innerWidth = $(listParent).width();
                    var innerHeight = $(listParent).height();

                    itemsWidth = (innerWidth) / itemsVisible;

                    childSet.width(itemsWidth);
                    if (settings.clone) {
                        object.css({
                            'left' : -itemsWidth                            
                        });
                    }else {
                        object.css({
                            'left' : 0
                        });
                    }

                    var halfArrowHeight = (leftArrow.height()) / 2;
                    var arrowMargin = (innerHeight / 2) - halfArrowHeight;
                    leftArrow.css("top", arrowMargin + "px");
                    rightArrow.css("top", arrowMargin + "px");

                });
                $(leftArrow).on("click", function(event) {
                    methods.scrollLeft();
                });
                $(rightArrow).on("click", function(event) {
                    methods.scrollRight();
                });
                if (settings.pauseOnHover == true) {
                    $(".nbs-flexisel-item").on({
                        mouseenter : function() {
                            canNavigate = false;
                        },
                        mouseleave : function() {
                            canNavigate = true;
                        }
                    });
                }
                if (settings.autoPlay == true) {

                    setInterval(function() {
                        if (canNavigate == true)
                            methods.scrollRight();
                    }, settings.autoPlaySpeed);
                }

            },
            /******************************
            Set Responsive Events
            Set breakpoints depending on responsiveBreakpoints
            *******************************/            
            
            setResponsiveEvents: function() {
                var contentWidth = $('html').width();
                
                if(settings.enableResponsiveBreakpoints) {
                    
                    var largestCustom = responsivePoints[responsivePoints.length-1].changePoint; // sorted array 
                    
                    for(var i in responsivePoints) {
                        
                        if(contentWidth >= largestCustom) { // set to default if width greater than largest custom responsiveBreakpoint 
                            itemsVisible = settings.visibleItems;
                            break;
                        }
                        else { // determine custom responsiveBreakpoint to use
                        
                            if(contentWidth < responsivePoints[i].changePoint) {
                                itemsVisible = responsivePoints[i].visibleItems;
                                break;
                            }
                            else
                                continue;
                        }
                    }
                }
            },

            /******************************
            Sort Responsive Object
            Gets all the settings in resposiveBreakpoints and sorts them into an array
            *******************************/            
            
            sortResponsiveObject: function(obj) {
                
                var responsiveObjects = [];
                
                for(var i in obj) {
                    responsiveObjects.push(obj[i]);
                }
                
                responsiveObjects.sort(function(a, b) {
                    return a.changePoint - b.changePoint;
                });
            
                responsivePoints = responsiveObjects;
            },
            
            /******************************
            Scroll Left
            *******************************/
            scrollLeft : function() {
                if (object.position().left < 0) {
                    if (canNavigate == true) {
                        canNavigate = false;

                        var listParent = object.parent();
                        var innerWidth = listParent.width();

                        itemsWidth = (innerWidth) / itemsVisible;

                        var childSet = object.children();

                        object.animate({
                            'left' : "+=" + itemsWidth
                        }, {
                            queue : false,
                            duration : settings.animationSpeed,
                            easing : "linear",
                            complete : function() {
                                if (settings.clone) {
                                    childSet.last().insertBefore(
                                            childSet.first()); // Get the first list item and put it after the last list item (that's how the infinite effects is made)                                   
                                }
                                methods.adjustScroll();
                                canNavigate = true;
                            }
                        });
                    }
                }
            },
            /******************************
            Scroll Right
            *******************************/            
            scrollRight : function() {
                var listParent = object.parent();
                var innerWidth = listParent.width();

                itemsWidth = (innerWidth) / itemsVisible;

                var difObject = (itemsWidth - innerWidth);
                var objPosition = (object.position().left + ((totalItems-itemsVisible)*itemsWidth)-innerWidth);    
                
                if((difObject <= Math.ceil(objPosition)) && (!settings.clone)){
                    if (canNavigate == true) {
                        canNavigate = false;                    
    
                        object.animate({
                            'left' : "-=" + itemsWidth
                        }, {
                            queue : false,
                            duration : settings.animationSpeed,
                            easing : "linear",
                            complete : function() {                                
                                methods.adjustScroll();
                                canNavigate = true;
                            }
                        });
                    }
                } else if(settings.clone){
                    if (canNavigate == true) {
                        canNavigate = false;
    
                        var childSet = object.children();
    
                        object.animate({
                            'left' : "-=" + itemsWidth
                        }, {
                            queue : false,
                            duration : settings.animationSpeed,
                            easing : "linear",
                            complete : function() {                                
                                    childSet.first().insertAfter(childSet.last()); // Get the first list item and put it after the last list item (that's how the infinite effects is made)                                
                                methods.adjustScroll();
                                canNavigate = true;
                            }
                        });
                    }
                };                
            },
            /******************************
            Adjust Scroll 
             *******************************/
            adjustScroll : function() {
                var listParent = object.parent();
                var childSet = object.children();

                var innerWidth = listParent.width();
                itemsWidth = (innerWidth) / itemsVisible;
                childSet.width(itemsWidth);
                if (settings.clone) {
                    object.css({
                        'left' : -itemsWidth
                    });
                }
            }
        };
        if (methods[options]) { // $("#element").pluginName('methodName', 'arg1', 'arg2');
            return methods[options].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof options === 'object' || !options) { // $("#element").pluginName({ option: 1, option:2 });
            return methods.init.apply(this);
        } else {
            $.error('Method "' + method + '" does not exist in flexisel plugin!');
        }
    };
})(jQuery);
;
/*!
 SlickNav Responsive Mobile Menu
 (c) 2014 Josh Cope
 licensed under MIT
 */
;(function ($, document, window) {
	var
	// default settings object.
		defaults = {
			label: 'MENU',
			duplicate: true,
			duration: 200,
			easingOpen: 'swing',
			easingClose: 'swing',
			closedSymbol: '&#9658;',
			openedSymbol: '&#9660;',
			prependTo: 'body',
			parentTag: 'a',
			closeOnClick: false,
			allowParentLinks: false,
			init: function(){},
			open: function(){},
			close: function(){}
		},
		mobileMenu = 'slicknav',
		prefix = 'slicknav';

	function Plugin( element, options ) {
		this.element = element;

		// jQuery has an extend method which merges the contents of two or
		// more objects, storing the result in the first object. The first object
		// is generally empty as we don't want to alter the default options for
		// future instances of the plugin
		this.settings = $.extend( {}, defaults, options) ;

		this._defaults = defaults;
		this._name = mobileMenu;

		this.init();
	}

	Plugin.prototype.init = function () {
		var $this = this;
		var menu = $(this.element);
		var settings = this.settings;

		// clone menu if needed
		if (settings.duplicate) {
			$this.mobileNav = menu.clone();
			//remove ids from clone to prevent css issues
			$this.mobileNav.removeAttr('id');
			$this.mobileNav.find('*').each(function(i,e){
				$(e).removeAttr('id');
			});
		}
		else
			$this.mobileNav = menu;

		// styling class for the button
		var iconClass = prefix+'_icon';

		if (settings.label == '') {
			iconClass += ' '+prefix+'_no-text';
		}

		if (settings.parentTag == 'a') {
			settings.parentTag = 'a href="#"';
		}

		// create menu bar
		$this.mobileNav.attr('class', prefix+'_nav');
		var menuBar = $('<div class="'+prefix+'_menu"></div>');
		$this.btn = $('<'+settings.parentTag+' aria-haspopup="true" tabindex="0" class="'+prefix+'_btn '+prefix+'_collapsed"><span class="'+prefix+'_menutxt">'+settings.label+'</span><span class="'+iconClass+'"><span class="'+prefix+'_icon-bar"></span><span class="'+prefix+'_icon-bar"></span><span class="'+prefix+'_icon-bar"></span></span></a>');
		$(menuBar).append($this.btn);
		$(settings.prependTo).prepend(menuBar);
		menuBar.append($this.mobileNav);

		// iterate over structure adding additional structure
		var items = $this.mobileNav.find('li');
		$(items).each(function () {
			var item = $(this);
			data = {};
			data.children = item.children('ul').attr('role','menu');
			item.data("menu", data);

			// if a list item has a nested menu
			if (data.children.length > 0) {

				// select all text before the child menu
				var a = item.contents();
				var nodes = [];
				$(a).each(function(){
					if(!$(this).is("ul")) {
						nodes.push(this);
					}
					else {
						return false;
					}
				});

				// wrap item text with tag and add classes
				var wrap = $(nodes).wrapAll('<'+settings.parentTag+' role="menuitem" aria-haspopup="true" tabindex="-1" class="'+prefix+'_item"/>').parent();

				item.addClass(prefix+'_collapsed');
				item.addClass(prefix+'_parent');

				// create parent arrow
				$(nodes).last().after('<span class="'+prefix+'_arrow">'+settings.closedSymbol+'</span>');


			} else if ( item.children().length == 0) {
				item.addClass(prefix+'_txtnode');
			}

			// accessibility for links
			item.children('a').attr('role', 'menuitem').click(function(event){
				//Emulate menu close if set
				//Ensure that it's not a parent
				if (settings.closeOnClick && !$(event.target).parent().closest('li').hasClass(prefix+'_parent'))
					$($this.btn).click();
			});

			//also close on click if parent links are set
			if (settings.closeOnClick && settings.allowParentLinks) {
				item.children('a').children('a').click(function(event){
					//Emulate menu close
					$($this.btn).click();
				});
			}
		});

		// structure is in place, now hide appropriate items
		$(items).each(function () {
			var data = $(this).data("menu");
			$this._visibilityToggle(data.children, false, null, true);
		});

		// finally toggle entire menu
		$this._visibilityToggle($this.mobileNav, false, 'init', true);

		// accessibility for menu button
		$this.mobileNav.attr('role','menu');

		// outline prevention when using mouse
		$(document).mousedown(function(){
			$this._outlines(false);
		});

		$(document).keyup(function(){
			$this._outlines(true);
		});

		// menu button click
		$($this.btn).click(function (e) {
			e.preventDefault();
			$this._menuToggle();
		});

		// click on menu parent
		$this.mobileNav.on('click', '.'+prefix+'_item', function(e){
			e.preventDefault();
			$this._itemClick($(this));
		});

		// check for enter key on menu button and menu parents
		$($this.btn).keydown(function (e) {
			var ev = e || event;
			if(ev.keyCode == 13) {
				e.preventDefault();
				$this._menuToggle();
			}
		});

		$this.mobileNav.on('keydown', '.'+prefix+'_item', function(e) {
			var ev = e || event;
			if(ev.keyCode == 13) {
				e.preventDefault();
				$this._itemClick($(e.target));
			}
		});

		// allow links clickable within parent tags if set
		if (settings.allowParentLinks) {
			$('.'+prefix+'_item a').click(function(e){
				e.stopImmediatePropagation();
			});
		}
	};

	//toggle menu
	Plugin.prototype._menuToggle = function(el){
		var $this = this;
		var btn = $this.btn;
		var mobileNav = $this.mobileNav;

		if (btn.hasClass(prefix+'_collapsed')) {
			btn.removeClass(prefix+'_collapsed');
			btn.addClass(prefix+'_open');
		} else {
			btn.removeClass(prefix+'_open');
			btn.addClass(prefix+'_collapsed');
		}
		btn.addClass(prefix+'_animating');
		$this._visibilityToggle(mobileNav, true, btn);
	}

	// toggle clicked items
	Plugin.prototype._itemClick = function(el) {
		var $this = this;
		var settings = $this.settings;
		var data = el.data("menu");
		if (!data) {
			data = {};
			data.arrow = el.children('.'+prefix+'_arrow');
			data.ul = el.next('ul');
			data.parent = el.parent();
			el.data("menu", data);
		}
		if (data.parent.hasClass(prefix+'_collapsed')) {
			data.arrow.html(settings.openedSymbol);
			data.parent.removeClass(prefix+'_collapsed');
			data.parent.addClass(prefix+'_open');
			data.parent.addClass(prefix+'_animating');
			$this._visibilityToggle(data.ul, true, el);
		} else {
			data.arrow.html(settings.closedSymbol);
			data.parent.addClass(prefix+'_collapsed');
			data.parent.removeClass(prefix+'_open');
			data.parent.addClass(prefix+'_animating');
			$this._visibilityToggle(data.ul, true, el);
		}
	}

	// toggle actual visibility and accessibility tags
	Plugin.prototype._visibilityToggle = function(el, animate, trigger, init) {
		var $this = this;
		var settings = $this.settings;
		var items = $this._getActionItems(el);
		var duration = 0;
		if (animate)
			duration = settings.duration;

		if (el.hasClass(prefix+'_hidden')) {
			el.removeClass(prefix+'_hidden');
			el.slideDown(duration, settings.easingOpen, function(){

				$(trigger).removeClass(prefix+'_animating');
				$(trigger).parent().removeClass(prefix+'_animating');

				//Fire open callback
				if (!init) {
					settings.open(trigger);
				}
			});
			el.attr('aria-hidden','false');
			items.attr('tabindex', '0');
			$this._setVisAttr(el, false);
		} else {
			el.addClass(prefix+'_hidden');
			el.slideUp(duration, this.settings.easingClose, function() {
				el.attr('aria-hidden','true');
				items.attr('tabindex', '-1');
				$this._setVisAttr(el, true);
				el.hide(); //jQuery 1.7 bug fix

				$(trigger).removeClass(prefix+'_animating');
				$(trigger).parent().removeClass(prefix+'_animating');

				//Fire init or close callback
				if (!init)
					settings.close(trigger);
				else if (trigger == 'init')
					settings.init();
			});
		}
	}

	// set attributes of element and children based on visibility
	Plugin.prototype._setVisAttr = function(el, hidden) {
		var $this = this;

		// select all parents that aren't hidden
		var nonHidden = el.children('li').children('ul').not('.'+prefix+'_hidden');

		// iterate over all items setting appropriate tags
		if (!hidden) {
			nonHidden.each(function(){
				var ul = $(this);
				ul.attr('aria-hidden','false');
				var items = $this._getActionItems(ul);
				items.attr('tabindex', '0');
				$this._setVisAttr(ul, hidden);
			});
		} else {
			nonHidden.each(function(){
				var ul = $(this);
				ul.attr('aria-hidden','true');
				var items = $this._getActionItems(ul);
				items.attr('tabindex', '-1');
				$this._setVisAttr(ul, hidden);
			});
		}
	}

	// get all 1st level items that are clickable
	Plugin.prototype._getActionItems = function(el) {
		var data = el.data("menu");
		if (!data) {
			data = {};
			var items = el.children('li');
			var anchors = items.children('a');
			data.links = anchors.add(items.children('.'+prefix+'_item'));
			el.data("menu", data);
		}
		return data.links;
	}

	Plugin.prototype._outlines = function(state) {
		if (!state) {
			$('.'+prefix+'_item, .'+prefix+'_btn').css('outline','none');
		} else {
			$('.'+prefix+'_item, .'+prefix+'_btn').css('outline','');
		}
	}

	Plugin.prototype.toggle = function(){
		$this._menuToggle();
	}

	Plugin.prototype.open = function(){
		$this = this;
		if ($this.btn.hasClass(prefix+'_collapsed')) {
			$this._menuToggle();
		}
	}

	Plugin.prototype.close = function(){
		$this = this;
		if ($this.btn.hasClass(prefix+'_open')) {
			$this._menuToggle();
		}
	}

	$.fn[mobileMenu] = function ( options ) {
		var args = arguments;

		// Is the first parameter an object (options), or was omitted, instantiate a new instance
		if (options === undefined || typeof options === 'object') {
			return this.each(function () {

				// Only allow the plugin to be instantiated once due to methods
				if (!$.data(this, 'plugin_' + mobileMenu)) {

					// if it has no instance, create a new one, pass options to our plugin constructor,
					// and store the plugin instance in the elements jQuery data object.
					$.data(this, 'plugin_' + mobileMenu, new Plugin( this, options ));
				}
			});

			// If is a string and doesn't start with an underscore or 'init' function, treat this as a call to a public method.
		} else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {

			// Cache the method call to make it possible to return a value
			var returns;

			this.each(function () {
				var instance = $.data(this, 'plugin_' + mobileMenu);

				// Tests that there's already a plugin-instance and checks that the requested public method exists
				if (instance instanceof Plugin && typeof instance[options] === 'function') {

					// Call the method of our plugin instance, and pass it the supplied arguments.
					returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
				}
			});

			// If the earlier cached method gives a value back return the value, otherwise return this to preserve chainability.
			return returns !== undefined ? returns : this;
		}
	};
}(jQuery, document, window));;
(function(a){a.isScrollToFixed=function(b){return !!a(b).data("ScrollToFixed")};a.ScrollToFixed=function(d,i){var l=this;l.$el=a(d);l.el=d;l.$el.data("ScrollToFixed",l);var c=false;var G=l.$el;var H;var E;var e;var y;var D=0;var q=0;var j=-1;var f=-1;var t=null;var z;var g;function u(){G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed");f=-1;D=G.offset().top;q=G.offset().left;if(l.options.offsets){q+=(G.offset().left-G.position().left)}if(j==-1){j=q}H=G.css("position");c=true;if(l.options.bottom!=-1){G.trigger("preFixed.ScrollToFixed");w();G.trigger("fixed.ScrollToFixed")}}function n(){var I=l.options.limit;if(!I){return 0}if(typeof(I)==="function"){return I.apply(G)}return I}function p(){return H==="fixed"}function x(){return H==="absolute"}function h(){return !(p()||x())}function w(){if(!p()){t.css({display:G.css("display"),width:G.outerWidth(true),height:G.outerHeight(true),"float":G.css("float")});cssOptions={"z-index":l.options.zIndex,position:"fixed",top:l.options.bottom==-1?s():"",bottom:l.options.bottom==-1?"":l.options.bottom,"margin-left":"0px"};if(!l.options.dontSetWidth){cssOptions.width=G.width()}G.css(cssOptions);G.addClass(l.options.baseClassName);if(l.options.className){G.addClass(l.options.className)}H="fixed"}}function b(){var J=n();var I=q;if(l.options.removeOffsets){I="";J=J-D}cssOptions={position:"absolute",top:J,left:I,"margin-left":"0px",bottom:""};if(!l.options.dontSetWidth){cssOptions.width=G.width()}G.css(cssOptions);H="absolute"}function k(){if(!h()){f=-1;t.css("display","none");G.css({"z-index":y,width:"",position:E,left:"",top:e,"margin-left":""});G.removeClass("scroll-to-fixed-fixed");if(l.options.className){G.removeClass(l.options.className)}H=null}}function v(I){if(I!=f){G.css("left",q-I);f=I}}function s(){var I=l.options.marginTop;if(!I){return 0}if(typeof(I)==="function"){return I.apply(G)}return I}function A(){if(!a.isScrollToFixed(G)){return}var K=c;if(!c){u()}else{if(h()){D=G.offset().top;q=G.offset().left}}var I=a(window).scrollLeft();var L=a(window).scrollTop();var J=n();if(l.options.minWidth&&a(window).width()<l.options.minWidth){if(!h()||!K){o();G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed")}}else{if(l.options.maxWidth&&a(window).width()>l.options.maxWidth){if(!h()||!K){o();G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed")}}else{if(l.options.bottom==-1){if(J>0&&L>=J-s()){if(!x()||!K){o();G.trigger("preAbsolute.ScrollToFixed");b();G.trigger("unfixed.ScrollToFixed")}}else{if(L>=D-s()){if(!p()||!K){o();G.trigger("preFixed.ScrollToFixed");w();f=-1;G.trigger("fixed.ScrollToFixed")}v(I)}else{if(!h()||!K){o();G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed")}}}}else{if(J>0){if(L+a(window).height()-G.outerHeight(true)>=J-(s()||-m())){if(p()){o();G.trigger("preUnfixed.ScrollToFixed");if(E==="absolute"){b()}else{k()}G.trigger("unfixed.ScrollToFixed")}}else{if(!p()){o();G.trigger("preFixed.ScrollToFixed");w()}v(I);G.trigger("fixed.ScrollToFixed")}}else{v(I)}}}}}function m(){if(!l.options.bottom){return 0}return l.options.bottom}function o(){var I=G.css("position");if(I=="absolute"){G.trigger("postAbsolute.ScrollToFixed")}else{if(I=="fixed"){G.trigger("postFixed.ScrollToFixed")}else{G.trigger("postUnfixed.ScrollToFixed")}}}var C=function(I){if(G.is(":visible")){c=false;A()}};var F=function(I){(!!window.requestAnimationFrame)?requestAnimationFrame(A):A()};var B=function(){var J=document.body;if(document.createElement&&J&&J.appendChild&&J.removeChild){var L=document.createElement("div");if(!L.getBoundingClientRect){return null}L.innerHTML="x";L.style.cssText="position:fixed;top:100px;";J.appendChild(L);var M=J.style.height,N=J.scrollTop;J.style.height="3000px";J.scrollTop=500;var I=L.getBoundingClientRect().top;J.style.height=M;var K=(I===100);J.removeChild(L);J.scrollTop=N;return K}return null};var r=function(I){I=I||window.event;if(I.preventDefault){I.preventDefault()}I.returnValue=false};l.init=function(){l.options=a.extend({},a.ScrollToFixed.defaultOptions,i);y=G.css("z-index");l.$el.css("z-index",l.options.zIndex);t=a("<div />");H=G.css("position");E=G.css("position");e=G.css("top");if(h()){l.$el.after(t)}a(window).bind("resize.ScrollToFixed",C);a(window).bind("scroll.ScrollToFixed",F);if("ontouchmove" in window){a(window).bind("touchmove.ScrollToFixed",A)}if(l.options.preFixed){G.bind("preFixed.ScrollToFixed",l.options.preFixed)}if(l.options.postFixed){G.bind("postFixed.ScrollToFixed",l.options.postFixed)}if(l.options.preUnfixed){G.bind("preUnfixed.ScrollToFixed",l.options.preUnfixed)}if(l.options.postUnfixed){G.bind("postUnfixed.ScrollToFixed",l.options.postUnfixed)}if(l.options.preAbsolute){G.bind("preAbsolute.ScrollToFixed",l.options.preAbsolute)}if(l.options.postAbsolute){G.bind("postAbsolute.ScrollToFixed",l.options.postAbsolute)}if(l.options.fixed){G.bind("fixed.ScrollToFixed",l.options.fixed)}if(l.options.unfixed){G.bind("unfixed.ScrollToFixed",l.options.unfixed)}if(l.options.spacerClass){t.addClass(l.options.spacerClass)}G.bind("resize.ScrollToFixed",function(){t.height(G.height())});G.bind("scroll.ScrollToFixed",function(){G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed");A()});G.bind("detach.ScrollToFixed",function(I){r(I);G.trigger("preUnfixed.ScrollToFixed");k();G.trigger("unfixed.ScrollToFixed");a(window).unbind("resize.ScrollToFixed",C);a(window).unbind("scroll.ScrollToFixed",F);G.unbind(".ScrollToFixed");t.remove();l.$el.removeData("ScrollToFixed")});C()};l.init()};a.ScrollToFixed.defaultOptions={marginTop:0,limit:0,bottom:-1,zIndex:1000,baseClassName:"scroll-to-fixed-fixed"};a.fn.scrollToFixed=function(b){return this.each(function(){(new a.ScrollToFixed(this,b))})}})(jQuery);;
(function($) {
	$(document).ready(function() {
		/*
		 * Sticky Header
		 */
		if ($('#head.wrapper').length && $('.ie6, .ie7, .ie8').length == 0) {
			globalSetupStickyHeader();
		}
		/*
		 * Homepage Slider
		 */
		if ($('.home-slider').length) {
			homePageHomeSliderSetup();
		}
		/*
		 * Homepage Product Spotlight
		 */
		if ($('.home-products').length) {
			homePageHomeProductsSetup();
		}
		/*
		 * Search Form
		 */
		globalSetupSearchForm();
		/*
		 * Mobile Menu
		 */
		//globalSetupMobileMenu();
		setupMobileMenu();
		
		/*
		 * Homepage Services Section
		 */
		if ($('.home-services').length) {
			homePageHomeServicesSetup();
		}
		if ($('.mobile-home-services .content').length) {
			homePageMobileHomeServicesSetup();
		}
		/*
		 * Img/Vid Page Header
		 */
		if ($('.mediapage').length) {
			$('.mediapage .expand').click(mediaPageMobileDescriptionToggle);
			mediaPageSetupDescriptionResize();
		}

		/*
		 * Sidebar Slider
		 */
		if ($('.sideslide.everslider').length) {
			jQuery('.sideslide .view-content').everslider({
				mode: 'carousel',
				effect: 'slide',
				itemKeepRatio: false,
				useCSS: true,
				moveSlides: 'auto',
				fadeEasing: 'linear',
				fadeDuration: 500,
				fadeDelay: 200,
				fadeDirection: 1,
				navigation: false,
				keyboard: true,
				swipeThreshold: 10,
				ticker: true,
				tickerAutoStart: true,
				tickerTimeout: 2000,
				tickerHover: true,
				tickerHoverDelay: 100
			});
		}
	});
	
	/** FUNCTIONS
	------------------------------------------------ */
	function globalSetupStickyHeader() {
		$('#head.wrapper').scrollToFixed();
	}
	function homePageHomeSliderSetup() {
		$('#block-views-home-hero-slider-block .view-content').flexslider({
			animation: "slide",
			direction: "horizontal",
			reverse: false,
			animationLoop: true,
			startAt: 0,
			slideshow: true,
			slideshowSpeed: 7000,
			animationSpeed: 600,
			initDelay: 0,
			randomize: false,
			pauseOnAction: false,
			pauseOnHover: true,
			smoothHeight: false,
			useCSS: true,
			touch: true,
			video: false,
			controlNav: true,
			directionNav: false,
			prevText: "",
			nextText: "",
			keyboard: true,
			multipleKeyboard: false,
			mousewheel: false,
			pausePlay: false,
			pauseText: "",
			playText: "",
			minItems: 1,
			maxItems: 1,
			namespace: "flex-"
		});
	}
	function homePageHomeProductsSetup() {
		$('.home-products').everslider({
			mode: 'carousel',
			effect: 'fade',
			/*itemWidth: '300px',*/
			/*itemHeight: '150px',*/
			itemKeepRatio: false,
			useCSS: true,
			moveSlides: 'auto',
			fadeEasing: 'linear',
			fadeDuration: 500,
			fadeDelay: 200,
			fadeDirection: 1,
			navigation: true,
			keyboard: true,
			swipeThreshold: 10,
			nextNav: '<span class="alt-arrow">Next</span>',
			prevNav: '<span class="alt-arrow">Prev</span>',
			ticker: true,
			tickerAutoStart: true,
			tickerTimeout: 2000,
			tickerHover: true,
			tickerHoverDelay: 100,
		});
	}
	function globalSetupSearchForm() {
		$('#block-search-form').hoverIntent({
			over: function() {
				if (!$(this).hasClass('open')) {
					$(this).addClass('open');
					$(this).find('.form-text').show().animate({
						width: '205px',
					}, 300);
				}
			},
			out: function() {
				if ($(this).hasClass('open')) {
					$(this).find('.form-text').animate({
						width: 0,
					}, {
						duration: 700,
						complete: function() {
							$(this).hide();
							$('#block-search-form').removeClass('open');
						}
					});
				}
			},
			timeout: 700
		});
	}
	function setupMobileMenu() {
		//mobile menu
		var $menu = jQuery('.mobile-menu'),
			$menulink = jQuery('.btn-mobile-menu .content a'),
			$menuTrigger = jQuery('.expanded');

		$menulink.click(function(e) {
			e.preventDefault();
			if (!$menulink.hasClass('active')) {
				jQuery("html, body").animate({ scrollTop: 0 });
			}
			$menulink.toggleClass('active');
			$menu.toggleClass('active');
		});

		$menuTrigger.click(function(e) {
			var $this = jQuery(this);
			$this.toggleClass('active').children('.menu').toggleClass('active');
		});

		var $menul = jQuery('.block-menu-block.mobile-menu .menu-block-wrapper > ul');
		$menul.slicknav({
			label: '',
			prependTo: '.block-menu-block.mobile-menu .content',
			duplicate: false,
			allowParentLinks: true
		});

		var $slickbtn = jQuery('.slicknav_menu .slicknav_btn');
		$slickbtn.click();
	}
	function globalSetupMobileMenu() {
		// Remove mobile menu on window resize
		$(window).resize(function() {
			if ($(window).width() > 767) {
				// At desktop size
				if ($(window).width() > 767) {
					var $menulink = $('.btn-mobile-menu a');
					var $menuTrigger = $('.expanded');
					$menulink.removeClass('active');
					$menu.removeClass('active');
				}
			}
		});
		
		//mobile menu
		$('body').addClass('js');
		var $menu = $('.mobile-menu'),
			$menulink = $('.btn-mobile-menu a'),
			$menuTrigger = $('.expanded');

		$menulink.click(function(e) {
			e.preventDefault();
			$menulink.toggleClass('active');
			$menu.toggleClass('active');
		});

		$menuTrigger.click(function(e) {
			var $this = $(this);
			$this.toggleClass('active').children('.menu').toggleClass('active');
		});
		//end mobile menu
	}
	function homePageHomeServicesSetup() {
		// Remove BR tags (THANKS DRUPAL)
		$('.home-services br').remove();
		// We have JS, remove the click
		$('.home-services h3 a').click(function(e) {
			e.preventDefault();
			return true;
		});
		// Click Handler for service
		$('.home-services li').click(function() {
			if ($(window).width() > 767) {
				$(this).addClass('active'); /* create overlay */
				$('.home-services .content').append('<div class="overlay-services" style="display: none;"></div>');
				//$(this).find('img').clone().appendTo($('.overlay-services'));
				var full = $(this).find('img').data('full');
				$('.overlay-services').append('<img src="' + full + '" />');
				$(this).find('.desc').clone().appendTo($('.overlay-services'));
				$('.overlay-services .desc').append('<div class="close">X</div>');
				var closeFunc = function() {
						$(this).removeClass('active');
						$('.overlay-services').remove();
					}
				$('.overlay-services .close').click(closeFunc);
				jQuery('.overlay-services').hoverIntent({
					over: function() {},
					out: closeFunc,
					timeout: 700
				});
				$('.overlay-services').show();
			} else {
				$(this).find('h3 a').click();
			}
		});
	}
	function homePageMobileHomeServicesSetup() {
		// Remove BR tags
		$('.mobile-home-services br').remove();
		// Setup Slider
		$('.mobile-home-services .content').everslider({
			mode: 'carousel',
			effect: 'slide',
			itemWidth: '200px',
			itemHeight: '177px',
			itemKeepRatio: true,
			useCSS: true,
			moveSlides: 'auto',
			fadeEasing: 'linear',
			fadeDuration: 500,
			fadeDelay: 200,
			fadeDirection: 1,
			navigation: false,
			keyboard: true,
			swipeThreshold: 10,
			nextNav: '<span class="alt-arrow">Next</span>',
			prevNav: '<span class="alt-arrow">Next</span>',
			ticker: true,
			tickerAutoStart: true,
			tickerTimeout: 2000,
			tickerHover: true,
			tickerHoverDelay: 100
		});
	}
	function mediaPageMobileDescriptionToggle() {
		$(this).siblings('.mediadesc').stop().slideToggle();
	}


	function mediaPageSetupDescriptionResize() {
		$(window).resize(function() {
			if ($(window).width() > 767) {
				// At desktop size
				$('.mediadesc').stop().show();
			}
		});
	}
	$(window).load(function() {
		/*
		 * Mobile Services Carousel
		 */
		/*$('.mobile-home-services .content').flexisel({
			autoPlay: false,
			pauseOnHover: false,
			autoPlaySpeed: 1000,
			enableResponsiveBreakpoints: true,
			responsiveBreakpoints: {
				mobile: {
					changePoint: 767,
					visibleItems: 1
				}
			},
			visibleItems: 1,
			clone: true,
		});
		*/
		

	});
})(jQuery);
;
