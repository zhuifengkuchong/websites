(function($) {
	$(document).ready(function() {
		/*
		 * Sticky Header
		 */
		if ($('#head.wrapper').length && $('.ie6, .ie7, .ie8').length == 0) {
			globalSetupStickyHeader();
		}
		/*
		 * Homepage Slider
		 */
		if ($('.home-slider').length) {
			homePageHomeSliderSetup();
		}
		/*
		 * Homepage Product Spotlight
		 */
		if ($('.home-products').length) {
			homePageHomeProductsSetup();
		}
		/*
		 * Search Form
		 */
		globalSetupSearchForm();
		/*
		 * Mobile Menu
		 */
		//globalSetupMobileMenu();
		setupMobileMenu();
		
		/*
		 * Homepage Services Section
		 */
		if ($('.home-services').length) {
			homePageHomeServicesSetup();
		}
		if ($('.mobile-home-services .content').length) {
			homePageMobileHomeServicesSetup();
		}
		/*
		 * Img/Vid Page Header
		 */
		if ($('.mediapage').length) {
			$('.mediapage .expand').click(mediaPageMobileDescriptionToggle);
			mediaPageSetupDescriptionResize();
		}

		/*
		 * Sidebar Slider
		 */
		if ($('.sideslide.everslider').length) {
			jQuery('.sideslide .view-content').everslider({
				mode: 'carousel',
				effect: 'slide',
				itemKeepRatio: false,
				useCSS: true,
				moveSlides: 'auto',
				fadeEasing: 'linear',
				fadeDuration: 500,
				fadeDelay: 200,
				fadeDirection: 1,
				navigation: false,
				keyboard: true,
				swipeThreshold: 10,
				ticker: true,
				tickerAutoStart: true,
				tickerTimeout: 2000,
				tickerHover: true,
				tickerHoverDelay: 100
			});
		}
	});
	
	/** FUNCTIONS
	------------------------------------------------ */
	function globalSetupStickyHeader() {
		$('#head.wrapper').scrollToFixed();
	}
	function homePageHomeSliderSetup() {
		$('#block-views-home-hero-slider-block .view-content').flexslider({
			animation: "slide",
			direction: "horizontal",
			reverse: false,
			animationLoop: true,
			startAt: 0,
			slideshow: true,
			slideshowSpeed: 7000,
			animationSpeed: 600,
			initDelay: 0,
			randomize: false,
			pauseOnAction: false,
			pauseOnHover: true,
			smoothHeight: false,
			useCSS: true,
			touch: true,
			video: false,
			controlNav: true,
			directionNav: false,
			prevText: "",
			nextText: "",
			keyboard: true,
			multipleKeyboard: false,
			mousewheel: false,
			pausePlay: false,
			pauseText: "",
			playText: "",
			minItems: 1,
			maxItems: 1,
			namespace: "flex-"
		});
	}
	function homePageHomeProductsSetup() {
		$('.home-products').everslider({
			mode: 'carousel',
			effect: 'fade',
			/*itemWidth: '300px',*/
			/*itemHeight: '150px',*/
			itemKeepRatio: false,
			useCSS: true,
			moveSlides: 'auto',
			fadeEasing: 'linear',
			fadeDuration: 500,
			fadeDelay: 200,
			fadeDirection: 1,
			navigation: true,
			keyboard: true,
			swipeThreshold: 10,
			nextNav: '<span class="alt-arrow">Next</span>',
			prevNav: '<span class="alt-arrow">Prev</span>',
			ticker: true,
			tickerAutoStart: true,
			tickerTimeout: 2000,
			tickerHover: true,
			tickerHoverDelay: 100,
		});
	}
	function globalSetupSearchForm() {
		$('#block-search-form').hoverIntent({
			over: function() {
				if (!$(this).hasClass('open')) {
					$(this).addClass('open');
					$(this).find('.form-text').show().animate({
						width: '205px',
					}, 300);
				}
			},
			out: function() {
				if ($(this).hasClass('open')) {
					$(this).find('.form-text').animate({
						width: 0,
					}, {
						duration: 700,
						complete: function() {
							$(this).hide();
							$('#block-search-form').removeClass('open');
						}
					});
				}
			},
			timeout: 700
		});
	}
	function setupMobileMenu() {
		//mobile menu
		var $menu = jQuery('.mobile-menu'),
			$menulink = jQuery('.btn-mobile-menu .content a'),
			$menuTrigger = jQuery('.expanded');

		$menulink.click(function(e) {
			e.preventDefault();
			if (!$menulink.hasClass('active')) {
				jQuery("html, body").animate({ scrollTop: 0 });
			}
			$menulink.toggleClass('active');
			$menu.toggleClass('active');
		});

		$menuTrigger.click(function(e) {
			var $this = jQuery(this);
			$this.toggleClass('active').children('.menu').toggleClass('active');
		});

		var $menul = jQuery('.block-menu-block.mobile-menu .menu-block-wrapper > ul');
		$menul.slicknav({
			label: '',
			prependTo: '.block-menu-block.mobile-menu .content',
			duplicate: false,
			allowParentLinks: true
		});

		var $slickbtn = jQuery('.slicknav_menu .slicknav_btn');
		$slickbtn.click();
	}
	function globalSetupMobileMenu() {
		// Remove mobile menu on window resize
		$(window).resize(function() {
			if ($(window).width() > 767) {
				// At desktop size
				if ($(window).width() > 767) {
					var $menulink = $('.btn-mobile-menu a');
					var $menuTrigger = $('.expanded');
					$menulink.removeClass('active');
					$menu.removeClass('active');
				}
			}
		});
		
		//mobile menu
		$('body').addClass('js');
		var $menu = $('.mobile-menu'),
			$menulink = $('.btn-mobile-menu a'),
			$menuTrigger = $('.expanded');

		$menulink.click(function(e) {
			e.preventDefault();
			$menulink.toggleClass('active');
			$menu.toggleClass('active');
		});

		$menuTrigger.click(function(e) {
			var $this = $(this);
			$this.toggleClass('active').children('.menu').toggleClass('active');
		});
		//end mobile menu
	}
	function homePageHomeServicesSetup() {
		// Remove BR tags (THANKS DRUPAL)
		$('.home-services br').remove();
		// We have JS, remove the click
		$('.home-services h3 a').click(function(e) {
			e.preventDefault();
			return true;
		});
		// Click Handler for service
		$('.home-services li').click(function() {
			if ($(window).width() > 767) {
				$(this).addClass('active'); /* create overlay */
				$('.home-services .content').append('<div class="overlay-services" style="display: none;"></div>');
				//$(this).find('img').clone().appendTo($('.overlay-services'));
				var full = $(this).find('img').data('full');
				$('.overlay-services').append('<img src="' + full + '" />');
				$(this).find('.desc').clone().appendTo($('.overlay-services'));
				$('.overlay-services .desc').append('<div class="close">X</div>');
				var closeFunc = function() {
						$(this).removeClass('active');
						$('.overlay-services').remove();
					}
				$('.overlay-services .close').click(closeFunc);
				jQuery('.overlay-services').hoverIntent({
					over: function() {},
					out: closeFunc,
					timeout: 700
				});
				$('.overlay-services').show();
			} else {
				$(this).find('h3 a').click();
			}
		});
	}
	function homePageMobileHomeServicesSetup() {
		// Remove BR tags
		$('.mobile-home-services br').remove();
		// Setup Slider
		$('.mobile-home-services .content').everslider({
			mode: 'carousel',
			effect: 'slide',
			itemWidth: '200px',
			itemHeight: '177px',
			itemKeepRatio: true,
			useCSS: true,
			moveSlides: 'auto',
			fadeEasing: 'linear',
			fadeDuration: 500,
			fadeDelay: 200,
			fadeDirection: 1,
			navigation: false,
			keyboard: true,
			swipeThreshold: 10,
			nextNav: '<span class="alt-arrow">Next</span>',
			prevNav: '<span class="alt-arrow">Next</span>',
			ticker: true,
			tickerAutoStart: true,
			tickerTimeout: 2000,
			tickerHover: true,
			tickerHoverDelay: 100
		});
	}
	function mediaPageMobileDescriptionToggle() {
		$(this).siblings('.mediadesc').stop().slideToggle();
	}


	function mediaPageSetupDescriptionResize() {
		$(window).resize(function() {
			if ($(window).width() > 767) {
				// At desktop size
				$('.mediadesc').stop().show();
			}
		});
	}
	$(window).load(function() {
		/*
		 * Mobile Services Carousel
		 */
		/*$('.mobile-home-services .content').flexisel({
			autoPlay: false,
			pauseOnHover: false,
			autoPlaySpeed: 1000,
			enableResponsiveBreakpoints: true,
			responsiveBreakpoints: {
				mobile: {
					changePoint: 767,
					visibleItems: 1
				}
			},
			visibleItems: 1,
			clone: true,
		});
		*/
		

	});
})(jQuery);
