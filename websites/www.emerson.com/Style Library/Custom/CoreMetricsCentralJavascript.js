var currentUrl = document.URL;
var pathName = window.location.pathname;
var local = pathName.substring(1);
var sIndex = local.indexOf('/');
	if(local.toString().length > 6)
	{
    local = local.substring(0, sIndex);
	}
local = local.replace('-','').replace('/','');

function TagConstruct(social, Placement, Variation){
	ProcessedTag = social + "-" + Placement + "-" + Variation;
	cmCreateElementTag(ProcessedTag, "Social Media");
}

$("a[href*='www.facebook.com/EmersonCorporate']").click(function() {
	var parentNode = this.parentNode.parentNode.className.toString();
	var social = "Facebook", Placement = "";
	var	Variation = local.toUpperCase();
	switch(parentNode){
	case "header-social-items":
		    Placement = "Header";
			break;
	case "footer-social":
			Placement = "Footer";
			break;
	case "hero-social":
			Placement = "Landing";
			break;
	case "links":
			Placement = "RightRail";
			break;
	default:
		alert("Inside Links");
	}
	
	TagConstruct(social, Placement, Variation);
	
	});

$("a[href*='twitter.com/Emerson']").click(function() {	
	var parentNode = this.parentNode.parentNode.className.toString();
	var social = "Twitter", Placement = "";
	var	Variation = local.toUpperCase();
	switch(parentNode){
	case "header-social-items":
		    Placement = "Header";
			break;
	case "footer-social":
			Placement = "Footer";
			break;
	case "hero-social":
			Placement = "Landing";
			break;
	case "links":
			Placement = "RightRail";
			break;
	default:
		alert("Inside Links");
	}
	
	TagConstruct(social, Placement, Variation);
	

	});

$("a[href*='linkedin.com/company/emerson']").click(function() {
    var parentNode = this.parentNode.parentNode.className.toString();
	var social = "LinkedIn", Placement = "";
	var	Variation = local.toUpperCase();
	switch(parentNode){
	case "header-social-items":
		    Placement = "Header";
			break;
	case "footer-social":
			Placement = "Footer";
			break;
	case "hero-social":
			Placement = "Landing";
			break;
	case "links":
			Placement = "RightRail";
			break;
	default:
		alert("Inside Links");
	}
	
	TagConstruct(social, Placement, Variation);
	    

	});

$("a[href*='http://www.youtube.com/emerson']").click(function() {
    var parentNode = this.parentNode.parentNode.className.toString();
	var social = "YouTube", Placement = "";
	var	Variation = local.toUpperCase();
	switch(parentNode){
	case "header-social-items":
		    Placement = "Header";
			break;
	case "footer-social":
			Placement = "Footer";
			break;
	case "hero-social":
			Placement = "Landing";
			break;
	case "links":
			Placement = "RightRail";
			break;
	default:
		alert("Inside Links");
	}
	
	TagConstruct(social, Placement, Variation);
	                   

	});


$("a[href*='http://www.weibo.com/emersoninchina']").click(function() {
    var parentNode = this.parentNode.parentNode.className.toString();
	var social = "Weibo", Placement = "";
	var	Variation = local.toUpperCase();
	switch(parentNode){
	case "header-social-items":
		    Placement = "Header";
			break;
	case "footer-social":
			Placement = "Footer";
			break;
	case "hero-wrapper":
			Placement = "Landing";
			break;
	case "links":
			Placement = "RightRail";
			break;
	default:
		alert("Inside Links");
	}
	
	TagConstruct(social, Placement, Variation);

	});

$("a[href*='http://i.youku.com/emersoninchina']").click(function() {
    var parentNode = this.parentNode.parentNode.className.toString();
	var social = "Youku", Placement = "";
	var	Variation = local.toUpperCase();
	switch(parentNode){
	case "header-social-items":
		    Placement = "Header";
			break;
	case "footer-social":
			Placement = "Footer";
			break;
	case "hero-wrapper":
			Placement = "Landing";
			break;
	case "links":
			Placement = "RightRail";
			break;
	default:
		alert("Inside Links");
	}
	
	TagConstruct(social, Placement, Variation);

	});

$("a[href*='http://www.wechat.com/']").click(function() {
    var parentNode = this.parentNode.parentNode.className.toString();
	var social = "WeChat", Placement = "";
	var	Variation = local.toUpperCase();
	switch(parentNode){
	case "header-social-items":
		    Placement = "Header";
			break;
	case "footer-social":
			Placement = "Footer";
			break;
	case "hero-wrapper":
			Placement = "Landing";
			break;
	case "links":
			Placement = "RightRail";
			break;
	default:
		alert("Inside Links");
	}
	
	TagConstruct(social, Placement, Variation);

	});


getListItem(currentUrl);

function getListItem (currentUrl) {

    var hostListItem = window.location.host;
    var pathNameListItem  = window.location.pathname;
    var localListItem = pathNameListItem.substring(1);
    var sIndexListItem = localListItem.indexOf('/');
    localListItem = localListItem.substring(0, sIndex);
    var url = "http://"+hostListItem+"/"+localListItem+"/_api/web/lists/getbytitle('CoreMetricsWebTagging')/items?$select=Title,URL,TaggingType,ObjectToTag,FileName,Category,ActionType,TargetURL,LimelightPlayerID&$filter=URL eq '" + currentUrl + "'";
    var encodedUrl = encodeURI(url);
                              $.ajax({
                              url: encodedUrl,
                              method: "GET",
                              contentType: "application/json",
                              headers: { "Accept": "application/json; odata=verbose" },
                              async: false,
                                     success: function (data) {
                                                              if(data.d.results.length > 1)
																  {
																  //("Result More Than 1")
																  for (var i = 0; i < data.d.results.length; i++) {
																  		if (data.d.results[i].Title == "ElementTagging"){																  																  
																  			elementTagging(data.d.results[i].TargetURL ,data.d.results[i].FileName, data.d.results[i].Category)
																         }
																  		
																  		
																  		else{
																  		    conversionEventTagging(data.d.results[i].TargetURL ,data.d.results[i].FileName, data.d.results[i].ActionType)
																  		}																  		
																   }
																  }
																  else{
																	  if (data.d.results[0].Title == "ConversionTagging")
																		{																																																																																																																																																		
																		 //call the function for cmCreateConversionEventTag and pass the needed parameter
																		 conversionEventTagging(data.d.results[0].TargetURL ,data.d.results[0].FileName, data.d.results[0].ActionType)																	      
																		} 
																		else
																		{
																		//call the function for cmCreateElementTag and pass the needed parameter
																		elementTagging(data.d.results[0].TargetURL ,data.d.results[0].FileName, data.d.results[0].Category)
																		}																		
														           }
																		
																},
																error: function (data) {
																			console.log(data);
																		}
												});
												
                                                            }
															
															
function conversionEventTagging(targetUrl, fileName, actionType) {
	$("a[href*='" + targetUrl + "']").click(function() {
				cmCreateConversionEventTag(fileName, actionType);
	});	
}


function elementTagging(targetUrl, fileName, category){

$("a[href*='" + targetUrl + "']").click(function() {
                                cmCreateElementTag(fileName, category);

	});
}

     
        
