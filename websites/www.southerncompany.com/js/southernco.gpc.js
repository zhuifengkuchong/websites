var southernco = southernco || {
    gpc: {}
};

southernco.gpc = southernco.gpc || {};

southernco.gpc.webMethod =
    function (method, data, options) {
        var defaults = {
            type: "POST",
            url: "/Services.aspx/" + method,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        };
        var combined = {};
        $.extend(combined, defaults, options);
        $.ajax(combined);
    }