$(document).ready(function(){
	
	// cookie defaults
	$.cookie.path = "/";
	$.cookie.expires = 7;

// Equal height modules
function modHeights() {
	$(".modules").each(function() { // loop over each set of modules
		var modCount = $(this).children(".module").length;
//		console.log("count is " + modCount);
		var mods = new Array();
		var modHeights = new Array();
		var divHeights = new Array();		
		for (var a = 1; a <= modCount; a++) {			
			var mod = $(this).children(".module:nth-child("+a+")");
			mod.height("auto");		
			var offset = mod.offset().top;
			if (mods[offset] === undefined) { mods[offset] = new Array(); }
			mods[offset].push(mod);
	//		console.log(modHeights[offset]);
	//		console.log("offset is " + offset + " and height is " + mod.height() + " outer height is " + mod.outerHeight());
			if (modHeights[offset] === undefined || mod.height() > modHeights[offset]['height']) {
				modHeights[offset] = new Array();
				modHeights[offset]["height"] = mod.height();
				modHeights[offset]["padding"] = mod.outerHeight() - mod.height();
			}
			
			
			// Now the same thing for any "dividers" Only works for first one.
			var divCount = 0;
			mod.children(".divider").each(function() {
				if (!divCount++) {
					if (divHeights[offset] === undefined || $(this).offset().top > divHeights[offset]) {
						divHeights[offset] = $(this).offset().top;
					}
				}
				
			});
		}
	
//		console.log(modHeights);
		for (var x in modHeights) {
	//		console.log(modHeights[x]["padding"]);
			$.each(mods[x], function() {
				var modHeight = modHeights[x]["height"];
				var thisPad = this.outerHeight() - this.height();
	//			if (this.height() != modHeights[x]["height"]) {
					if (!thisPad) {
						modHeight = modHeight + modHeights[x]["padding"];
						//console.log("added " + modHeights[x]["padding"]);
					}
					//console.log("debug = " + modHeight);
					this.height(modHeight);					
					if ($(this).children(".button").length == 1) {			
						$(this).children(".button").wrap("<div class='module-button'></div>");
					}
					
		//		}
				
				var divCount = 0;
				$(this).children(".divider").each(function() {
					if (!divCount++) {
				//		console.log($(this).offset().top + " and " + divHeights[x]);
						if ($(this).offset().top < divHeights[x]) {

							var thisMargin = parseInt($(this).css("margin-top"));
							var diff = divHeights[x] - $(this).offset().top + thisMargin;							
							$(this).css("margin-top",diff+"px");
							
						}
						
					}
					
				});
				
				
			});
		}	
		
		
		
	});
}

//  Responsive Iframes //
function adjustIframes()
{
  $('iframe').each(function(){
    var
    $this       = $(this),
    proportion  = $this.data( 'proportion' ),
    w           = $this.attr('width'),
    actual_w    = $this.width();
    
    if ( ! proportion )
    {
        proportion = $this.attr('height') / w;
        $this.data( 'proportion', proportion );
    }
  
    if ( actual_w != w )
    {
        $this.css( 'height', Math.round( actual_w * proportion ) + 'px' );
    }
  });
}
$(window).on('resize load',adjustIframes);

//NRP//
function moduleButtonPositionAbs (){
	
	$(".module-button").css({
		'position' : 'absolute'
	});
	
}




//NRP//


// first time it runs, set a small delay to allow the fonts to load etc.
setTimeout(modHeights, 750);
		
//modHeights(); 

	// handle window resize. All resize-driven stuff should go in here.
	$(window).resize(function() {
			//NRP//
			$(".module-button").css({
					'position' : 'relative'
			});
			//NRP//
	modHeights();
	//NRP//
	setTimeout(moduleButtonPositionAbs, 200);
	//NRP//	
	});
//NRP//
setTimeout(moduleButtonPositionAbs, 800);	
//NRP//

	
var numLinks = 4;


numLinks--;	
	// quick links slide toggle on landing pages
$("#quick-links li:gt("+numLinks+")").slideUp('fast');
if($("#quick-links li").length <= numLinks+1){
	
	$(".toggle-links").hide();
	
	
}
$(".toggle-links").toggle(function(){
	$(this).toggleClass('active');
	$("#quick-links li:gt("+numLinks+")").slideDown('fast');
	$(".toggle-links a").text('Fewer...')
	
	
}, function(){
	$(this).toggleClass('active');
	$("#quick-links li:gt("+numLinks+")").slideUp('fast');
	$(".toggle-links a").text('More...')
	
	
});

	// Set Location Indicator
/*
	$('ul#breadcrumb li.row-0').each(function() {
		
		var curLink = $(this).text();
		
		// Compare Breadcrumb to Dropdown Navigation
		$('#navigation li > a').each(function() {
			
			var newLink = $(this).text();
		
			if (curLink == newLink) {
							
				$(this).parents('http://www.southerncompany.com/js/li.top').addClass('indicator');
				
			}
		});	
			
	});*/
	
/* Hide Breadcrumb on home page */

$('#home #drawer').hide();
$('nav > ul > li > a').on('click', function(){
	
	$('#home #drawer').show();
	
	
});
foundIt = 0;
$('ul.menu li').each(function(){
		var currentUrl = window.location.href.replace('#', '');
    if(currentUrl.indexOf($(this).find('> a').attr('href'))>-1)
    {
    
    if(foundIt === 0){
    
    		$(this).parents('http://www.southerncompany.com/js/li.expandable.top').addClass('indicator');
    		foundIt = 1;
    
    }
    }
});	
	
	// Main Navigation

	$('nav > ul > li > a').on('click',function () {navDrawer($(this));});
	if (!$(".not-in-nav").length) {
		$('ul#breadcrumb li').on('click',function (e) {
				
				navDrawer($('nav > ul > li.indicator > a'));
				e.preventDefault();
		});

		/* Get rid of href from a tags in breadcrumb*/
		$('ul#breadcrumb li').each(function(){	
			$(this).find('> a').attr('href' , ' ');
			
		});
		
	}
	function navDrawer(elem){
	/* Grab tha length of longest sub nav and set the height for drawer */
	
			navLength = $(elem).parent().find('> ul.menu > li').length;
			drawerHeight = $(elem).parent().find('> ul.menu').height();
						
			//Set the min-height to ul.menu to make height of border-right same for all sub nav
			/*$('http://www.southerncompany.com/js/ul.menu').each(function(){
				
				$(this).css('min-height' , drawerHeight);
				
			});
			*/
						
		$(elem).parent().find(' > ul.menu > li ').each(function(){
				
			var secondLevelNavLen =	$(this).find('> ul.menu > li').length;
						
			if(secondLevelNavLen > navLength){
								
				$("http://www.southerncompany.com/js/nav ul.menu").stop(true, true).css({
					
					'visibility' : 'hidden',
					'display' : 'block'
					
					
				});
				
								
				
				currentDrawerHeight = $(this).find('> ul.menu').height();
				
				if( currentDrawerHeight > drawerHeight ){
					
					drawerHeight = currentDrawerHeight;	
					
				}
				
				$("http://www.southerncompany.com/js/nav ul.menu").stop(true, true).css({
					
					'visibility' : 'visible',
					'display' : 'none'
					
					
				});
				navLength = secondLevelNavLen;
				
				
			}
			else{
				
				$(this).find('> ul.menu').css('height' , drawerHeight);
				
			}
			
		});	
		
		
		$(elem).parent().find(' > ul.menu > li > ul.menu > li').each(function(){
			
			
			var thirdLevelNavLen =	$(this).find('> ul.menu > li').length;
			
			
			if(thirdLevelNavLen > navLength){
			
				$("http://www.southerncompany.com/js/nav ul.menu").stop(true, true).css({
					
					'visibility' : 'hidden',
					'display' : 'block'
					
					
				});
				currentDrawerHeight = $(this).find('> ul.menu').height();
				
				if( currentDrawerHeight > drawerHeight ){
					
					drawerHeight = currentDrawerHeight;	
					
				}
				
				$("http://www.southerncompany.com/js/nav ul.menu").stop(true, true).css({
					
					'visibility' : 'visible',
					'display' : 'none'
					
					
				});
				
			}
			
			else{
				
				$(this).find('> ul.menu').css('height' , drawerHeight);
				
			}
			
		});
		
		$(elem).parent().find(' > ul.menu > li > ul.menu > li > ul.menu > li').each(function(){
				
			var fourthLevelNavLen =	$(this).find('> ul.menu > li').length;
			if(fourthLevelNavLen > navLength){
			
				$("http://www.southerncompany.com/js/nav ul.menu").stop(true, true).css({
					
					'visibility' : 'hidden',
					'display' : 'block'
					
					
				});
				currentDrawerHeight = $(this).find('> ul.menu').height();
				
				if( currentDrawerHeight > drawerHeight ){
					
					drawerHeight = currentDrawerHeight;	
					
				}
				
				$("http://www.southerncompany.com/js/nav ul.menu").stop(true, true).css({
					
					'visibility' : 'visible',
					'display' : 'none'
					
					
				});
				
			}
			else{
				
				$(this).find('> ul.menu').css('height' , drawerHeight);
				
			}
			
		});
		
		//Set Breadcrumb Location in Dropdown
		
		
		$('ul.menu li').each(function(){
			var currentUrl = window.location.href.replace('#', '');
			if(currentUrl.indexOf($(this).find('> a').attr('href'))>-1)
			{
				$(this).addClass('active').siblings().removeClass('active');    
			}
		});	

			
/*$('ul.menu li').hover(function(){
	
	$(this).expandable();
	
},function(){
	
	$(this).find('> a').expandable();
	
});		
*/		
		
		
	/*	$('ul#breadcrumb li').each(function() {
			//alert($(this).index());
			var curLink = $(this).text().toLowerCase();
			
			//Compare Breadcrumb to Dropdown Navigation
			$('#navigation li > a').each(function() {
				//if (foundIt = 0) {				
					var newLink = $(this).text().toLowerCase();				
					//alert("hi");
					if (curLink === newLink) {
						//console.log("new");
						//console.log(curLink);
						$(this).addClass('selected');
						
						$(this).parent('li').addClass('open');
						$(this).next('ul').show();
						$(this).parent('http://www.southerncompany.com/js/li.expandable.open').parent('http://www.southerncompany.com/js/ul.menu').addClass("active-sub-nav");
			
					}
					//foundIt = 1; // temp fix - prevents multiple options triggering it
				//}
				
			});
			
		});
	*/				
		// Calculate Height of largest UL
		//newHeight = $(this).findHeight();
		
	//	console.log(newHeight + " " + drawerHeight);
		
		
	 
		
		
		// Set Height of Drawer
		$('ul#breadcrumb').hide();
		
		$('nav').menuDrawer(drawerHeight);	

		//setTimeout(function(){expand = $(elem).expandable();}, 200)		
		var expand = $(elem).expandable();
		
		$("ul.menu li").each(function(){
		
		
		if($(this).hasClass('active')){
		
		
			///LstFixes
			$(this).siblings('li').removeClass('open');
			$(this).siblings('li').find('> a').removeClass('selected');
			
			
			
			
			
			$(this).siblings('li').addClass('non-active');
			$(this).parent().siblings('http://www.southerncompany.com/js/ul.menu').find('> li').addClass('non-active');
			$(this).find('> a').addClass('selected');
			if($(this).parent('http://www.southerncompany.com/js/ul.menu').hasClass('second-level-subnav')){
				
								
				$(this).parents('ul.menu.first-level-subnav').siblings('a').expandable();
				$(this).parents('li.expandable').first().find('> a').expandable();
				$(this).parents('li.expandable').first().siblings('li').addClass('non-active');
	
				$(this).parents('ul.menu.first-level-subnav').parent().siblings('li').addClass('non-active');
				



								
			}
			else if($(this).parent('http://www.southerncompany.com/js/ul.menu').hasClass('first-level-subnav')){
				
								
				$(this).parents('ul.menu.first-level-subnav').siblings('a').expandable();
				
				$(this).parents('ul.menu.first-level-subnav').parent().siblings('li').addClass('non-active');
				



								
			}
			else if($(this).parent('http://www.southerncompany.com/js/ul.menu').hasClass('third-level-subnav')){
				
								
				$(this).parents('ul.menu.first-level-subnav').siblings('a').expandable();
				$(this).parents('ul.menu.second-level-subnav').siblings('a').expandable();
				$(this).parent('ul.menu.third-level-subnav').siblings('a').expandable();
				
				$(this).parents('li.expandable').first().siblings('li').addClass('non-active');
				$(this).parents('ul.menu.first-level-subnav').parent().siblings('li').addClass('non-active');
				$(this).parents('ul.menu.second-level-subnav').parent().siblings('li').addClass('non-active');
				


								
			}
			else{
				
				
				
			}
			///LstFixes
			$(this).parents('http://www.southerncompany.com/js/ul.menu').siblings('a').addClass('selected');
			//$(this).parents('li.expandable').first().addClass('open').removeClass('non-active');

			
		}
	
	///LstFixes
	
	$("ul.menu  li > a").each(function(){
			
		if($(this).hasClass('selected')){
			
			$(this).parent().addClass('open').removeClass('non-active');
			
		}
		
	});
	
	///LstFixes-end
		
		
	});

		// Collapse Drawer
		
		if ( expand != false ) {
		//console.log("test test");
		var home = document.getElementById('home');
				//hiding breadcrumb on home page
				if(home){
					
					$('http://www.southerncompany.com/js/ul.menu').hide();
					$('#drawer').css('height', '0px');
					$(elem).children().find('a').removeClass('selected');
					
					
				}
				else{
					$('http://www.southerncompany.com/js/ul.menu').hide();
					$('#drawer').css('height', '25px');
					$(elem).children().find('a').removeClass('selected');
					$('ul#breadcrumb').fadeIn('slow');
				}
			
		}
}
	//});
	
	$('nav ul ul li a').click(function () {

		// Show/Hide nested ul's
		$(this).expandable();
		
	});
	
	/*$('a.selected').each(function(){
		//alert('test');
		$(this).parent('li').addClass('open');
		$(this).parent('http://www.southerncompany.com/js/li.open').parent('http://www.southerncompany.com/js/ul.menu').prev('a').addClass('selected').parent('li.expandable').addClass('open');
		
	});*/
	// Twirlers
	
	$(".with-teaser h4, .with-teaser span, .with-teaser h3").click(function(){
		$(this).closest('.with-teaser').toggleClass('active');
		var teaserEl = $(this).closest(".with-teaser");
		var teaser = teaserEl.children('.teaser');
		var pp = teaserEl.children('.detail');

		//$(h4).toggleClass('open');
		$(pp).slideToggle();
		$(teaser).slideToggle();
		//$(this + '.teaser').hide().removeClass('active'); 
		
		if (teaserEl.hasClass('only-open-teaser')) {
			// close others
			teaserEl.siblings(".with-teaser.active").children(".teaser").slideToggle();
			teaserEl.siblings(".with-teaser.active").children(".detail").slideToggle();			
			teaserEl.siblings(".with-teaser.active").removeClass("active");
		}
	});
	
	
	// Overlays
	
	$(".overlay,.simple-overlay,:not('.overlay') .scrollable-overlay").appendTo("body");
	
	$("a.overlay-trigger,area.overlay-trigger").each(function() { 		
		$(this).attr("rel", $(this).attr("href")); 
	});
		
	
	if (navigator.userAgent.match(/iPad/i) != null) {
		
		$("a.overlay-trigger,area.overlay-trigger").overlay({
			//Jquery Tools must include "Expose" script to function properly
			mask: {
				color:'#000',
				opacity:0.6
			},
			top:'25%',
			fixed:false,
			onLoad: function () {
	            $("body").css("overflow", "hidden");
	            $.getScript("../../admin.brightcove.com/js/BrightcoveExperiences.js"/*tpa=http://admin.brightcove.com/js/BrightcoveExperiences.js*/, function(){
		            
		            brightcove.createExperiences();
					callPlayer("ytplayer", "playVideo");
	            });
	            	            	        },
			onClose: function() {
				$("body").css("overflow", "");
				if($(this.getOverlay()).find("form.validated").length > 0) {
					$(this.getOverlay()).find("form.validated").data("validator").reflow();				
					// temp
					$(".error").hide();				
				}
				callPlayer("ytplayer", "stopVideo");
			}
			
		});
		
		var initOverlayHash = location.hash;
		var initOverlay 	= $("a.overlay-trigger,area.overlay-trigger").data("overlay");
		
		if( initOverlayHash == "#initVideo" ){ initOverlay.load(); }
		
		
	}
	
	else {
		
	
		$("a.overlay-trigger,area.overlay-trigger").overlay({
			//Jquery Tools must include "Expose" script to function properly
			mask: {
				color:'#000',
				opacity:0.6
			},
			top:'25%',
			fixed:false,
			onLoad: function () {
	           
	            $.getScript("../../admin.brightcove.com/js/BrightcoveExperiences.js"/*tpa=http://admin.brightcove.com/js/BrightcoveExperiences.js*/, function(){
		            
		            brightcove.createExperiences();
					callPlayer("ytplayer", "playVideo");
	            });
	            	            	        },
			onClose: function() {
				if($(this.getOverlay()).find("form.validated").length > 0) {
					$(this.getOverlay()).find("form.validated").data("validator").reflow();				
					// temp
					$(".error").hide();				
				}
				
					callPlayer("ytplayer", "stopVideo");

			},
			
			
		});
		
		var initOverlayHash = location.hash;
		
		if(initOverlayHash != "" ){
			var initOverlay 	= $("a.overlay-trigger[href='" + initOverlayHash +"']").data("overlay");
		 	initOverlay.load(); 
			
		}
		
		
	}
		
	 $(".ext-overlay-trigger").overlay({
	 	mask:"#000",
	 	left:200,
	 	onBeforeLoad: function() {
				var wrap = this.getOverlay().find(".contentWrap");
				// load the page specified in the trigger
				wrap.load(this.getTrigger().attr("href")); 	
	 	},
		fixed:false		 	
	 
	 });
	 
	 $("a.contact-overlay-trigger").click(function(e) {
	 	e.preventDefault();
		$(this).attr("rel", "#contact-overlay"); 
		var opener = $(this);
	 	if (!$("#contact-overlay").length) {
		 	$("body").append("<div id='contact-overlay' class='simple-overlay'><div id='contact-overlay-detail'></div></div>");
		 	$("#contact-overlay-detail").load($(this).attr("href"), function() {
/*
	            $('#contact-overlay-detail form').ajaxForm({
	            	error: function() { 
	            		$("#contact-overlay-detail form").prepend("<p class='error'>Sorry, there was an error when submitting your inquiry. Please try again, or contact us at <a href='mailto:info@georgiapower.com'>info@georgiapower.com</a></p>");
	            	 },
	            	success: function() { $("#contact-overlay-detail form").html("Thank you for your inquiry."); }
	            
	            }); 			 		
*/
		 		bindContactForms();
			 	opener.overlay({
			 		mask:"#000",
			 		load:true,
			 		api:true,
			 		fixed:false,
					onClose: function() {										
						if($(this.getOverlay()).find("form.validated").length > 0) {						
							$(this.getOverlay()).find("form.validated").data("validator").reflow();
							// temp
							$(".error").hide();
						}
					}
			 		
			 	});
			 });
		 	
	 	} else {
		 	$(this).data("overlay").load();
	 	}
	 
	 });
	 
	 
        
              
	 // Debug Information
	 
	 $('#debug p').urlToLink();
	
	 // Debug animation
	if($.cookie('small')){
		$("#debug a").fadeOut();
		$("#debug").animate({
			
			'width' : '5%'
			
		});
		
		
	}	
	$('#debug .close-button').on('click' , function(){
		
		if(!$.cookie('small')){
				$("#debug a").fadeOut();
				$("#debug").animate({
			
			'width' : '5%'
			
			});
		$.cookie('small' , 1 ,{expires : 1 , path: '/'});
		
	}
		else{
			$("#debug a").fadeIn();
			$("#debug").animate({
			
			'width' : '400px'
			
			});
		$.removeCookie('small' ,{ path: '/' });
		//alert($.cookie('small'));	
			
		}	
		
		
		
	}); 
	
	
			 

/* Add Non-active class to not active links in top nav */

$("#navigation ul.menu li a").each(function(){
	
	$(this).on('click' , function(){
		// remove non-active class when you click onlink
		$(this).parent().removeClass('non-active');
		
		/////
		
		
		$(this).parent('li').siblings().addClass('non-active');
		
	});
	
	
});


	$("a[rel='external']").click(function(){
	  this.target = "_blank";
	});	


	// Promos w/ one link - link entire promo && if rel==external open it in new window
	
	$(".module").not(".no-link").each(function() {

			if($(this).find('a[rel="external"]').length == 1){
				$(this).addClass("linked-promo");			
				var url=$(this).find('a').attr('href');
				
				$(this).click(function(e){			
					window.open(url);
					e.preventDefault();
				});
				
			}
			else{
			
				if($(this).find('a').length == 1){				
					$(this).addClass("linked-promo");							
					$(this).click(function(e) {
						window.location = $(this).find("a").attr('href');
					});
				}
			}

		
		
	});


// Timeline Slider

function positionHandleTitle() {
	if ($(".handle").length) {
		var handlePos = $(".handle").offset().top - 47;
		var handlePosLeft = $(".handle").offset().left - 68;
		$(".handle-title").offset({top: handlePos, left:handlePosLeft});		
	}
}


	if ($("input[type='range']").length) {
		$(":range").rangeinput({
			onSlide : function(event , value){
				$(".handle-title.detail-title").addClass("visible");	
				title = $(".timeline ul.slides > li").eq(value).find("p.title").text();
				if (!title) title = $(".timeline ul.slides > li").eq(value).find("a").text();
				$(".handle-title").html(title);			
				var leftPos = $(".handle").offset().left - 69;
				$(".handle-title").offset({top:$(".handle-title").offset().top, left:leftPos});			
				
			},
			change:function() {
				$(".handle-title.detail-title").removeClass("visible");	
				$(".detail-title").html(title);	
				
			}
	
		});
		//$(".timeline").append("<span class='handle-title detail-title'>March 2004</span>");

	$(window).resize(function(){positionHandleTitle()});
	

	$(".handle").hover(function(){
		
		$(".handle-title.detail-title").addClass("visible");
		positionHandleTitle();
		
	}, function(){		
		
		$(".handle-title.detail-title").removeClass("visible");
		
		
	});
	
	
			$(":range").change(function(event, value) {
				
				$(".flexslider").flexslider(value);
			});
  }

//Timeline slider End

// Opening Up First twirler when page loads


$("div.with-teaser").eq(0).find('h4').click();



$("#search-form-input").focus(function() {
		if ($(this).val() == $(this).attr("data-init-value")) {
			$(this).val("");
		}
	});
$("#search-form-input").blur(function() {	
		if ($(this).val() == "") {
			$(this).val($(this).attr("data-init-value"));
		}
	});

$("#navigation  ul.menu").each(function(){
	
	if($(this).find('li.expandable').length > 0){
		
		$(this).addClass('has-expandable-child');
	}
	
	
});


//set triangle in top nav based on the current page under what directory?
var url = location.pathname;
//var path = url.pathname;
//alert(url);
compUrl = url.split("/");
if(compUrl[1] === 'what-doing' || compUrl[1] === "news"){
	$("#navigation > li:nth-child(2)").addClass("indicator");
	
	
}
else if(compUrl[1] === 'about-us'){
	$("#navigation > li:nth-child(3)").addClass("indicator");
}
else{}



//Add external class to links going outside

$("#content a, #sidebar a, #modules .modules ul li a, #modules.modules .module p a").each(function(){
	
	if(($(this).attr('rel') === 'external')||($(this).attr('target') === '_blank')){
		$(this).parent().addClass("external");
		$(this).addClass("external-link");
	}
	
});


$("#sidebar ul li, #modules .modules ul li").each(function(){
	
	if($(this).hasClass("external")){
		
		$(this).parent().addClass("list");
		
	}
	
});







// jQuery form submission

	
	function bindContactForms() {
		// Validator
		$(".contact-form:not('.validated')").validator({
			position:'center right'	
		}).submit(function(e) {

			if (!$(".contact-form .invalid").length) {
			
				e.preventDefault();
				$(".contact-form *").fadeOut(); // attr("disabled","disabled");
				
				var thisWidth = $(this).width();
				$(this).append("<div class='sending-message' style='width:"+thisWidth+"px;'><img src='../img/loading.gif'/*tpa=http://www.southerncompany.com/img/loading.gif*/ alt='Loading...' /><p>Sending data... please wait.</p></div>");

				var theSubject = $(".contact-form #subject").val();
	
				if (!theSubject) { theSubject = "New form submission from Website"; }
				
				var theBody = "Form Fields: \n\r";
				
				$(".contact-form :input").each(function() {
					var thisID = $(this).attr("id");
					var label = $("label[for='#"+thisID+"']").html()
					if (!label) label = thisID;			
										
					if (($(this).attr("type") != "radio" && $(this).attr("type") != "checkbox") || $(this).is(":checked")) {
						theBody = theBody + label + ": " + $(this).val() + "\n";
					}
				})
	/*
	        	if (!$(".contact-form-results").length) { 
	        		$(".contact-form").after("<div class='contact-form-results'><p>Thank you for being a valued customer. Your request is on its way.</p>");
	        	}
	*/
	
				method = $(".contact-form #formMethod").val();
				if (!method) method = "SendEdiContactUsEmail";
				noFail = 0; // needed while developing.

//				console.log("attempting to send email via " + method);

		        southernco.gpc.webMethod(method,
		        { subject: theSubject, body: theBody },
		        {
		            success: function (msg) {
	
			            if (msg.d == "error" && !noFail) {
	
			            	if (!$(".contact-form-results").length) { 
			            		$(".contact-form").after("<div class='contact-form-results'></div>");
			            	} 
				            
				            $(".contact-form-results").html("<p>Sorry - an error has occurred. Please try again or contact us for assistance.</p>");	            				           			           			            
			            } else {
			            	if (!$(".contact-form-results").length) { 
			            		$(".contact-form").after("<div class='contact-form-results'><p>Thank you for being a valued customer. Your request is on its way.</p>");
			            	}
			            }
			            
		         		$(".contact-form").fadeOut();
		         		$(".contact-form-results").fadeIn();
		         		  
		            },
		            error: function (xhr, status, error) {
	
		            	if (!$(".contact-form-results").length) { 
		            		$(".contact-form").after("<div class='contact-form-results'></div>");
		            	} 
			            
			            $(".contact-form-results").html("<p>Sorry - an error has occurred. Please try again or contact us for assistance.</p>");	            	
	
			     		$(".contact-form").fadeOut();
			     		$(".contact-form-results").fadeIn();
	
	
		            }				
				});
	
	
			}
		}).addClass("validated");
	}
	bindContactForms();
	
	if ($(".lt-ie8").length && !$.cookie('hide-ie-message')) {
		
		$("body").prepend("<div id='ie-warning'>You are running an old version of Internet Explorer; some features of this website may not function correctly. Please consider upgrading to the <a href='http://windows.microsoft.com/en-us/internet-explorer/download-ie'>latest version of Internet Explorer</a>, or try <a href='http://www.google.com/chrome'>Google Chrome</a> or <a href='http://www.mozilla.org/en-US/firefox/new/'>Mozilla Firefox</a>. <span class='close'>Hide this message</span></div>").slideDown();
		$("#ie-warning .close").click(function() {
	//		$("#ie-warning").slideUp();
			$.cookie("hide-ie-message",1,{ expires: 7, path: "/"});
			$("#ie-warning").slideUp();
		});
	}
	
function removeSecMod1 (){
		var winWidth = $(window).width();
		var tOnly = $("#module1").clone();
		
		if(winWidth >= 1040){
		
			
			$("#module1.t-only").remove();
			$("#module3").siblings("#module1").remove();
		
			}
		else if(winWidth < 1040 && winWidth >= 765 ){
			if($("#module3").siblings("#module1").length === 0){
				
				$("#module3").after(tOnly);
				$("#module3").next().addClass('t-only');
				modHeights();
			}
		
		else{}
		}

		
		
	}
	removeSecMod1 ();
	$(window).resize(function(){
		
		removeSecMod1 ();
		// Fixed the responsive issues with firefox -quick resize home page missing smart energy and corporate responsibility site modules
		modHeights();		
		
	});
	
	
	//Mobile Footer Nav Fixes (Click on footer link opens link in top nav)
	$("footer #mobile-menu li a").on('click', function(){
		
		
		var ind = $(this).parent().index();
		ind += 1;
		$("#navigation > li:nth-child(" + ind + ") > a").click();
		
		
	});
	function callPlayer(frame_id, func, args) {
    if (window.jQuery && frame_id instanceof jQuery) frame_id = frame_id.get(0).id;
    var iframe = document.getElementById(frame_id);
    if (iframe && iframe.tagName.toUpperCase() != 'IFRAME') {
        iframe = iframe.getElementsByTagName('iframe')[0];
    }
    if (iframe) {
        // Frame exists, 
        iframe.contentWindow.postMessage(JSON.stringify({
            "event": "command",
            "func": func,
            "args": args || [],
            "id": frame_id
        }), "*");
    }
}	

});


// Brightcove Learning namespace
	var BCL = {};
	//template loaded event handler
	BCL.googleTagMangare = function (experienceID) {dataLayer.push({'event': 'video'});};