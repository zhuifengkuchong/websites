/* Menu Expand/Collapse */

(function($) {

	$.fn.findHeight = function() {
  			
  			var current = $(this);     		
	      	var Height1 = $(current).parent().find('ul').height();
	      	var Height2 = $(current).parent().find('ul li ul').height();
	      	var Height3 = $(current).parent().find('ul li ul ul').height();
	      	
	      	var newHeight = Math.max(Height1, Height2, Height3);
	        return newHeight;
		  		
  	};	
  	
  	$.fn.expandable = function(newHeight) {
	  		
	  		var current = $(this);
	  		var child = current.siblings('.menu');
	  		var par = current.parent();
	  		var adj = current.parent().siblings();
	      		
	      		// Hide Expanded Siblings
	      		if( adj.hasClass('open') ) {
		      		adj.removeClass('open');
		      		adj.find('.menu').hide();
	      		}
	      		
	      		// Add class to selected dropdown
				if( current.siblings('ul').size() > 0 ) {
					
					// Hide Selected Siblings
		      		current.parent('li').siblings().children('a').removeClass('selected');
					
					//current.toggleClass('selected');
					if ( current.hasClass('selected') ) {
						current.removeClass('selected');
					}
					else {
						current.addClass('selected');
					}
				}
				
				// Set height of nested UL's
			    	// $(current).parent().find('ul').css('height', newHeight + 'px');
	      		
	      		// Expand Selected Menu
	        	if( current.is(':visible') ){
	        		
	        		if ( child.is(':visible') ){
	        			
		        		child.slideUp('fast');
	        		}
	        		else {
		        		child.slideDown(300);
	        		}
	        		
	        			        		
	        		if ( par.hasClass('open') ){
		        		par.removeClass('open');
	        		}
	        		else {
		        		par.addClass('open');
	        		}
	        			        		
	        	}
	        	
	        	if ( current.parent().hasClass('open') ) {
		        	return false;
	        	}
	        	

    };
    
    $.fn.menuDrawer = function(newHeight) {
	    return this.each(function() {
	    	
        	// Expand Menu Drawer
        		$('#drawer').css('height', newHeight + 40 + 'px');
        	         	
        	
        	// Collapse Menu Drawer
	        	// if ( $('this').hasClass('selected') ){
	        		// alert('here!');
	        		// $('#drawer').css('height', '0px');
		        	// $('#drawer').hide();
		        	// Add closed tag if needed
	        //	}
        	
	    });
    };
    
    $.fn.urlToLink = function(options) {
        var options = $.extend({}, $.fn.urlToLink.defaults, options); 
        return this.each(function(){
            var element = $(this),
                expression = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            return element.html( element.text().replace(expression, "<a href='$1' target='"+options.target+"'>$1</a>") );
        });
    }
    
    $.fn.urlToLink.defaults = {
        target : '_blank'         // Link target
    }
    
})(jQuery);