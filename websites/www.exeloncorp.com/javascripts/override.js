/* Updated function from init.js */
function IMNGetOOUILocation(obj)
{
	var objRet=new Object;
	var objSpan=obj;
	var objOOUI=obj;
	var oouiX=0, oouiY=0, objDX=0;
	var fRtl=document.dir=="rtl";
	while (objSpan && objSpan.tagName !="SPAN" && objSpan.tagName !="TABLE")
	{
		objSpan=objSpan.parentNode;
	}
	if (objSpan)
	{
		var collNodes=objSpan.tagName=="TABLE" ?
			objSpan.rows(0).cells(0).childNodes :
			objSpan.childNodes;
		var i;
		for (i=0; i < collNodes.length;++i)
		{
			if (collNodes.item(i).tagName=="IMG" && collNodes.item(i).id)
			{
				objOOUI=collNodes.item(i);
				break;
			}
			if (collNodes.item(i).tagName=="A" &&
				collNodes.item(i).childNodes.length > 0 &&
				collNodes.item(i).childNodes.item(0).tagName=="IMG" &&
				collNodes.item(i).childNodes.item(0).id)
			{
				objOOUI=collNodes.item(i).childNodes.item(0);
				break;
			}
		}
	}
	obj=objOOUI;
	while (obj)
	{
		if (fRtl)
		{
			if (obj.scrollWidth >=obj.clientWidth+obj.scrollLeft)
				objDX=obj.scrollWidth - obj.clientWidth - obj.scrollLeft;
			else
				objDX=obj.clientWidth+obj.scrollLeft - obj.scrollWidth;
			oouiX+=obj.offsetLeft+objDX;
		}
		else
			oouiX+=obj.offsetLeft - obj.scrollLeft;
		oouiY+=obj.offsetTop - obj.scrollTop;
		obj=obj.offsetParent;
	}
	try
	{
		obj=window.frameElement;
		while (obj)
		{
			if (fRtl)
			{
				if (obj.scrollWidth >=obj.clientWidth+obj.scrollLeft)
					objDX=obj.scrollWidth - obj.clientWidth - obj.scrollLeft;
				else
					objDX=obj.clientWidth+obj.scrollLeft - obj.scrollWidth;
				oouiX+=obj.offsetLeft+objDX;
			}
			else
				oouiX+=obj.offsetLeft - obj.scrollLeft;
			oouiY+=obj.offsetTop - obj.scrollTop;
			obj=obj.offsetParent;
		}
	} catch(e)
	{
	};
	objRet.objSpan=objSpan;
	objRet.objOOUI=objOOUI;
	objRet.oouiX=oouiX;
	objRet.oouiY=oouiY;
	if (fRtl)
		objRet.oouiX+=objOOUI.offsetWidth;
	return objRet;
}


/* Overrides the OOTB SharePoint function from ie55up.js */
function MSOLayout_GetRealOffset(StartingObject, OffsetType, EndParent)
{
	var realValue=0;
	if (!EndParent) EndParent=document.body;
	for (var currentObject=StartingObject; currentObject && currentObject !=EndParent && currentObject != document.body; currentObject=currentObject.offsetParent)
	{
		var offset = eval('currentObject.offset'+OffsetType);
		if (offset) realValue+=offset;
	}
	return realValue;
}

/* Overrides the OOTB SharePoint function from init.js */
/* Name.dll issue */
	function ProcessDefaultOnLoad(onLoadFunctionNames)
	{
		//** Uncomment this to see when this runs
		//alert('Fixing the Issue');
		
		ProcessPNGImages();
		UpdateAccessibilityUI();
		
		//** We comment out the offending ootb function
		//** and leave the rest of the functions as they were
		//ProcessImn();
		for (var i=0; i < onLoadFunctionNames.length; i++)
		{
			var expr="if(typeof("+onLoadFunctionNames[i]+")=='function'){"+onLoadFunctionNames[i]+"();}";
			eval(expr);
		}
		if (typeof(_spUseDefaultFocus)!="undefined")
			DefaultFocus();
	}	