<script id = "race8a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race8={};
	myVars.races.race8.varName="caption-text__onmouseout";
	myVars.races.race8.varType="@varType@";
	myVars.races.race8.repairType = "@RepairType";
	myVars.races.race8.event1={};
	myVars.races.race8.event2={};
	myVars.races.race8.event1.id = "Lu_DOM";
	myVars.races.race8.event1.type = "onDOMContentLoaded";
	myVars.races.race8.event1.loc = "Lu_DOM_LOC";
	myVars.races.race8.event1.isRead = "False";
	myVars.races.race8.event1.eventType = "@event1EventType@";
	myVars.races.race8.event2.id = "caption-text";
	myVars.races.race8.event2.type = "onmouseout";
	myVars.races.race8.event2.loc = "caption-text_LOC";
	myVars.races.race8.event2.isRead = "True";
	myVars.races.race8.event2.eventType = "@event2EventType@";
	myVars.races.race8.event1.executed= false;// true to disable, false to enable
	myVars.races.race8.event2.executed= false;// true to disable, false to enable
</script>

