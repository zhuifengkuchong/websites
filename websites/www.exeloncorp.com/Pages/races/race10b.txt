<script id = "race10b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race10={};
	myVars.races.race10.varName="main_navigation__onmouseout";
	myVars.races.race10.varType="@varType@";
	myVars.races.race10.repairType = "@RepairType";
	myVars.races.race10.event1={};
	myVars.races.race10.event2={};
	myVars.races.race10.event1.id = "main_navigation";
	myVars.races.race10.event1.type = "onmouseout";
	myVars.races.race10.event1.loc = "main_navigation_LOC";
	myVars.races.race10.event1.isRead = "True";
	myVars.races.race10.event1.eventType = "@event1EventType@";
	myVars.races.race10.event2.id = "Lu_DOM";
	myVars.races.race10.event2.type = "onDOMContentLoaded";
	myVars.races.race10.event2.loc = "Lu_DOM_LOC";
	myVars.races.race10.event2.isRead = "False";
	myVars.races.race10.event2.eventType = "@event2EventType@";
	myVars.races.race10.event1.executed= false;// true to disable, false to enable
	myVars.races.race10.event2.executed= false;// true to disable, false to enable
</script>

