(function ($, window) {

    $.fn.irBar = function (options, callback) {
        "use strict";

        var defaults = {
            url: 'http://investors.devonenergy.com/',
            newsDomain: 'http://devonenergy.com/news',
            usePublic: true
        };

        var o = $.extend({}, defaults, options);

        var container = this,
            module = {

            _init: function () {
                if (o.usePublic){
                    module.getPublicEventFeed(); 
                } else {
                    module.getEventFeed(); 
                }
            },

            convertTime: function (time) {
                var militaryTime = time.split(':'),
                    hours = militaryTime[0],
                    suffix = "AM";
                    
                if (hours >= 12) {
                    suffix = "PM";
                    hours = hours - 12;
                }
                if (hours === 0)
                    hours = 12;

                return parseInt(hours,10) + ':' + militaryTime[1] + ' ' + suffix;
            },

            formatDate: function (date){
                var d = date.split(' ').shift().split('/'),
                    m = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
                return m[parseInt(d[0]-1)]+' '+d[1];
            },

            dataObj: function(tag){
                var serviceData = { 
                    serviceDto:{
                        ViewType: GetViewType(),
                        ViewDate: GetViewDate(),
                        RevisionNumber: GetRevisionNumber(),
                        LanguageId: GetLanguageId(),
                        ItemCount: 5,
                        StartIndex:0,
                        Signature: GetSignature(),
                        TagList:[tag],
                        IncludeTags: true
                    },
                    // eventDateFilter: 3,
                    sortOperator:1,
                    eventSelection: 3,
                    includePressReleases: true,
                    includePresentations: true,
                    includeFinancialReports: true
                }

                return serviceData;
            },

            getEventFeed: function(){
                var inst = this;

                $.ajax({
                    type: "POST",
                    url: "/services/EventService.svc/GetEventList",
                    data: JSON.stringify(inst.dataObj('earnings')),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data){
                        container.find('.earnings').html(inst.eventEarningsTpl(data.GetEventListResult[0]));
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "/services/EventService.svc/GetEventList",
                    data: JSON.stringify(inst.dataObj('featured')),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data){
                        container.find('.featured').html(inst.eventFeaturedTpl(data.GetEventListResult[0]));
                    }
                });
            },

            getPublicEventFeed: function(){
                var inst = this,
                    param = {
                        apiKey: 'CD5CB45625FD420CB67A767DCF836BAE',
                        pageSize: 1,
                        tagList: 'earnings',
                        includeTags:true,
                        eventDateFilter: 3,
                        sortOperator: 1,
                        includePressReleases: true,
                        includePresentations: true,
                        includeFinancialReports: true
                    }

                $.getJSON( "http://investors.devonenergy.com/feed/Event.svc/GetEventList", param,  function( data ) {
                    container.find('.earnings').html(inst.eventEarningsTpl(data.GetEventListResult[0]));
                });

                param.tagList = 'featured'

                $.getJSON( "http://investors.devonenergy.com/feed/Event.svc/GetEventList", param,  function( data ) {
                    container.find('.featured').html(inst.eventFeaturedTpl(data.GetEventListResult[0]));
                });
            },

            eventFeaturedTpl: function(e){
                var presentation = e.EventPresentation.length ? '<li class="slides"><a href="'+ e.EventPresentation[0].DocumentPath +'" target="_blank">Slides</a></li>' : '',
                    webcast = e.WebCastLink.length ? '<li><a href="'+ e.WebCastLink +'" target="_blank">Webcast</a></li>' : '',
                    location = e.Location.length ? e.Location+',&nbsp;' : '',
                    speakers = '',
                    speakerList = '',
                    slides = e.WebCastLink.length ? '' : '<span class="slideDetails">Slides - '+e.EventPresentation[0].DocumentFileType+ ' '+e.EventPresentation[0].DocumentFileSize+'</span>';

                    $.each(e.EventSpeaker, function(i, val){
                        speakers +=  i === 0 ? '<li>'+val.SpeakerName+' '+(val.SpeakerPosition.length ? '- '+val.SpeakerPosition : '')+'</li>' : '<li>,&nbsp;'+val.SpeakerName+' '+(val.SpeakerPosition.length ? '- '+val.SpeakerPosition : '')+'</li>';
                        speakerList = e.EventSpeaker.length ? '<ul class="speakers"><li>Speakers:&nbsp;</li>'+speakers+'</ul>' : '';
                    });


                    var html = 
                    '<a href="http://investors.devonenergy.com/investors/events-presentations/default.aspx"><div class="Title"><i class="icon '+ e.TagsList.join(' ') +'"></i><span class="subTitle">Featured <br/> Event</span></div></a>'+
                    '<ul class="Files '+ e.TagsList.join(' ') +'">' +
                    '<li class="Headline"><span class="HeadlineLink">'+ e.Title +
                            '</span><span class="location">' +
                                location + '</span><span class="date"><span class="eventDate">' +
                                this.formatDate(e.StartDate) + '</span><span class="eventTime '+ e.TagsList.join(' ') +'">,&nbsp;' +
                                this.convertTime(e.StartDate.split(' ').pop()) + ' ' + e.TimeZone.replace('EST', 'ET') +
                            '</span></span>'+slides+speakerList+'</li>' +
                        webcast +
                        presentation +
                        '<li class="last"><a class="arrow" href="http://investors.devonenergy.com/investors/events-presentations/default.aspx"></a></li>' +
                    '</ul>';

                return html;
            },

            eventEarningsTpl: function(e){
                var presentation = e.EventPresentation.length ? '<li><a href="'+ e.EventPresentation[0].DocumentPath +'" target="_blank">Slides</a></li>' : '',
                    webcast = e.WebCastLink.length ? '<li class="webcast"><a href="'+ e.WebCastLink +'" target="_blank">Q&A Webcast</a></li>' : '',
                    pressDate = e.StartDate.split(' ').shift().split('/').pop(),
                    pressRelease = e.EventPressRelease.length ? '<li class="news"><a href="http://devonenergy.com/news/'+pressDate+'/'+ e.EventPressRelease[0].SeoName +'" target="_blank">News Release</a></li>' : '',
                    attachments = '';

                    $.each(e.Attachments, function(i, item){
                        if (item.Title.toLowerCase() == "operations report" || item.Title.toLowerCase() == "management commentary"){
                            attachments +=  '<li class="item'+i+'"><a href="'+ item.Url +'"  target="_blank">'+ item.Title +'</a></li>';
                        }
                    });

                    var html = 
                    '<a href="http://investors.devonenergy.com/investors/quarterly-results/default.aspx"><div class="Title"><i class="icon '+ e.ReportQuarter +'"></i><span class="subTitle">Results</span></div></a>' +
                    '<ul class="Files">' +
                        pressRelease +
                        //presentation +
                        attachments +
                        webcast +
                        '<li class="last"><a class="arrow" href="http://investors.devonenergy.com/investors/quarterly-results/default.aspx" ></a></li>' +
                    '</ul>';

                return html;
            },
        };

        module._init();
    };

})(jQuery, window);