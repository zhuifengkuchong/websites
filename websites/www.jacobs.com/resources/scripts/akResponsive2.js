$(document).ready(function () {

	$('.mobile .subBreadCrumbTrail').on('click tap', function(){
		var backstretch = $(this).closest('.subSection').find('.backstretch:visible'),
			backstretchDiv = backstretch.parent(),
			backstretchImg = backstretch.children('img').attr('src');
			
		var location = '#' + $(this).siblings('.anchor').attr('id');
		window.location.hash = location;



		$(this).parent().parent().siblings().find('.subBreadCrumbTrail').next().slideUp("slow");
		$(this).next().slideToggle("slow",
			function(){ if($(this).is(':visible') && backstretchImg) backstretchDiv.backstretch(backstretchImg); }
			
			);

  $('#sliderGovernment').css("width","100%");
$('.bx-viewport').css("display","inline-block");
$('.bx-wrapper').css("display","inline-block");
  $('#sliderGovernment').css("height","auto");
$('.bx-viewport').css("height","auto");
$('.bx-viewport').css("width","100%");
$('.bx-viewport img').css("width","100%");
	});	
	
	$('iframe#ytplayer').parent().addClass('videoWrapper');


	//JOBS PAGE FUNCTIONALITY

	//Initialize Backstretch
		//Hot Jobs
		$("#subHotJobsBanner").backstretch("../../images/WorkForUs/Jobs/jobsHotJobsBanner.jpg");
		$("#subHotJobsClosingBanner").backstretch("../../images/closingBanner.jpg");
		//Why Jacobs	
		$("#subWhyJacobsBanner").backstretch("../../images/WorkForUs/Jobs/jobsWhyJacobsBanner.jpg"/*tpa=http://www.jacobs.com/images/WorkForUs/Jobs/jobsWhyJacobsBanner.jpg*/);
		$("#subWhyJacobsClosingBanner").backstretch("Unknown_83_filename"/*tpa=http://www.jacobs.com/images/closingBanner.jpg*/);
		//Apply 
		$("#subApplyForAJobBanner").backstretch("../../images/WorkForUs/Jobs/jobsApplyForAJobBanner.jpg");
		$("#subApplyForAJobsClosingBanner").backstretch("../../images/closingBanner.jpg");
					

//	/* show first tabs expanded on page load if at desktop width */
//		if($(window).width() > 567){
//			//Hot Jobs
//			$("#jobsHotJobsTabSelected").css({"left":"0px","width":"140px","height":"6px"});
//			jobsPageTabClick(hotJobs,1);
//			//Why Jacobs
//			$("#jobsWhyJacobsTabSelected").css({"left":"0px","width":"74px","height":"6px"}); 
//			if (!$.support.opacity) { $("#jobsWhyJacobsTabSelected").css("top","-82px") };  <!-- for IE7 and IE8 -->
//			jobsPageTabClick(whyJacobs,5);	
//		}


	//Tab functionality
		//Apply
/*		$("#jobsApplyForAJobTab1, #jobsApplyForAJobTab1b").on('tap click', function () {
			$("#jobsApplyForAJobTabSelected").css({"width":"140px","left":"0px"});
			jobsPageTabClick(applyJacobs,1); 
		});
		$("#jobsApplyForAJobTab2, #jobsApplyForAJobTab2b").on('tap click', function () {
			$("#jobsApplyForAJobTabSelected").css({"width":"90px","left":"155px"});	
			jobsPageTabClick(applyJacobs,2);  
		});
		$("#jobsApplyForAJobTab3, #jobsApplyForAJobTab3b").on('tap click', function () {
			$("#jobsApplyForAJobTabSelected").css({"width":"115px","left":"260px"});	
			jobsPageTabClick(applyJacobs,3); 
		});
		$("#jobsApplyForAJobTab4, #jobsApplyForAJobTab4b").on('tap click', function () {
			$("#jobsApplyForAJobTabSelected").css({"width":"45px","left":"390px"});
			jobsPageTabClick(applyJacobs,4); 
		});
*/
	//Arrow up functionality

		//Apply
		$("#jobsApplyForAJobTabArrowUp").on('tap click', function () {
			jobsPageArrowUpClick(applyJacobs);
		});		

});


var 
	applyJacobs = {
		tabName: 'jobsApplyForAJobTab',
		contentName: 'jobsApplyForAJobTabsContent',
		tabSelectedElem: "#jobsApplyForAJobTabSelected",
		arrowUpElem: "#jobsApplyForAJobTabArrowUp",
		openTab: 0	
	};

	/* hide why jacobs tab content and arrow */
function jobsPageArrowUpClick(sect){
		var tabSyntax = '#' + sect.tabName,
			contentSyntax = '#' + sect.contentName + '_';

		$(contentSyntax + sect.openTab + ',' + sect.tabSelectedElem + ',' + sect.arrowUpElem + ',' + contentSyntax + sect.openTab + "_mobile").slideUp(500);
		$(tabSyntax + sect.openTab).css({"color":"","text-decoration":""});	
		sect.openTab = 0;
}

function jobsPageTabClick(sect,tabNumb,slider){
	var tabSyntax = '#' + sect.tabName,
		tabClass = '.' + sect.tabName,
		contentSyntax = '#' + sect.contentName + '_',
		contentClass = '.' + sect.contentName;

	if(sect.openTab == tabNumb){
		sect.openTab = 0;	
		$(tabSyntax + tabNumb).css({color:"",textDecoration:""});
		if($('#mobileNav').css('display') == 'none') {
		}
		else{
		$(tabSyntax + tabNumb).css("background-image", "url(/images/Mobile_features_tab.png)");
		}
		$(sect.tabSelectedElem + ',' + sect.arrowUpElem + "," + contentSyntax + tabNumb + "," + contentSyntax + tabNumb + "_mobile").slideUp(500);
	}
	

	else{		
		sect.openTab = tabNumb;	
		var mobileElem;
		if( $(contentSyntax + tabNumb + "_mobile").length == 0 ){
			mobileElem = $(contentSyntax + tabNumb).clone();
			mobileElem.attr('id',sect.contentName + '_' + tabNumb + '_mobile');
			mobileElem.removeClass(sect.contentName).addClass(sect.contentName + '_mobile');
			mobileElem.insertAfter("3jobsApplyForAJobTabsMobileContent");	 
		}

		//$(contentClass + ',' + contentClass + '_mobile').slideUp();	
		$(sect.tabSelectedElem + ',' + sect.arrowUpElem + "," + contentSyntax + tabNumb + "," + contentSyntax + "_mobile").siblings(contentClass).slideUp(500);
		
		$(sect.tabSelectedElem + ',' + sect.arrowUpElem + "," + contentSyntax + tabNumb + "," + contentSyntax + "_mobile").slideDown(500);
		
		if(slider != undefined){
			mobileElem.find('#' + slider).attr('id',slider + '_mobile');
			var next = (tabNumb == 6 ? '#subCarouselNextCraft' : '#subCarouselNext' + tabNumb),
				prev = (tabNumb == 6 ? '#subCarouselPrevCraft' : '#subCarouselPrev' + tabNumb),
				width = (tabNumb == 6 ? 320 : 410);

			if($('.bx-viewport #' + slider).length == 0){
				/* photo carousel */
				$('#' + slider + '_mobile').bxSlider({
					minSlides: 1,
					maxSlides: 1,
					slideWidth: width,
					slideMargin: 7,
					infiniteLoop: false
				});	
				$('#' + slider).bxSlider({
					minSlides: 1,
					maxSlides: 1,
					slideWidth: width,
					slideMargin: 7,
					infiniteLoop: false
				});	
			}
		}
	}		
}