MpElDs = {
"https://espanol.southwest.com/mpel/espanol.southwest.com":"es",
"https://espanol.southwest.com/mpel/espanol.swabiz.com":"es",
"https://espanol.southwest.com/mpel/espanoltravel.southwest.com":"es",
"https://espanol.southwest.com/mpel/espanol.global.southwest.com":"es",
"https://espanol.southwest.com/mpel/espanol.southwest-ftst-a.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.southwest-ftst-b.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.southwest-ftst-c.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.southwest-ftst-d.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.southwest-ftst-e.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.ecom-cstg-web.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.ecom-efix-web.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.swabiz-ftst-a.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.swabiz-ftst-b.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.swabiz-ftst-c.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.swabiz-ftst-d.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.swabiz-ftst-e.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.swabiz-cstg-web.swacorp.com":"es",
"https://espanol.southwest.com/mpel/espanol.swabiz-efix-web.swacorp.com":"es",
"https://espanol.southwest.com/mpel/www.southwest.com":"en",
"https://espanol.southwest.com/mpel/southwest.com":"en",
"https://espanol.southwest.com/mpel/www.swabiz.com":"en",
"https://espanol.southwest.com/mpel/swabiz.com":"en",
"https://espanol.southwest.com/mpel/travel.southwest.com":"en",
"https://espanol.southwest.com/mpel/global.southwest.com":"en",
"https://espanol.southwest.com/mpel/southwest-ftst-a.swacorp.com":"en",
"https://espanol.southwest.com/mpel/southwest-ftst-b.swacorp.com":"en",
"https://espanol.southwest.com/mpel/southwest-ftst-c.swacorp.com":"en",
"https://espanol.southwest.com/mpel/southwest-ftst-d.swacorp.com":"en",
"https://espanol.southwest.com/mpel/southwest-ftst-e.swacorp.com":"en",
"https://espanol.southwest.com/mpel/ecom-cstg-web.swacorp.com":"en",
"https://espanol.southwest.com/mpel/ecom-efix-web.swacorp.com":"en",
"https://espanol.southwest.com/mpel/swabiz-ftst-a.swacorp.com":"en",
"https://espanol.southwest.com/mpel/swabiz-ftst-b.swacorp.com":"en",
"https://espanol.southwest.com/mpel/swabiz-ftst-c.swacorp.com":"en",
"https://espanol.southwest.com/mpel/swabiz-ftst-d.swacorp.com":"en",
"https://espanol.southwest.com/mpel/swabiz-ftst-e.swacorp.com":"en",
"https://espanol.southwest.com/mpel/swabiz-cstg-web.swacorp.com":"en",
"https://espanol.southwest.com/mpel/swabiz-efix-web.swacorp.com":"en",
"https://espanol.southwest.com/mpel/swacorp.com":"en",
"https://espanol.southwest.com/mpel/ecom-cstg-web1.swacorp.com":"en"
};
if (!RegExp("MP_LANG=" + MpElDs[location.host]).test(document.cookie)) {
       MpElD = "../../www.southwest.com/index-1.htm"/*tpa=https://www.southwest.com/*/;
	   tdomain = "https://espanol.southwest.com/";

	if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/southwest.com') != -1) {
       MpElD = "../../www.southwest.com/index-1.htm"/*tpa=https://www.southwest.com/*/;
	   tdomain = "https://espanol.southwest.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/swabiz.com') != -1) {
       MpElD = "https://www.swabiz.com/";
	   tdomain = "https://espanol.swabiz.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/travel.southwest.com') != -1) {
       MpElD = "https://travel.southwest.com/";
	   tdomain = "https://espanoltravel.southwest.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/global.southwest.com') != -1) {
       MpElD = "https://global.southwest.com/";
	   tdomain = "https://espanol.global.southwest.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/southwest-ftst-a.swacorp.com') != -1) {
       MpElD = "https://southwest-ftst-a.swacorp.com/";
	   tdomain = "https://espanol.southwest-ftst-a.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/southwest-ftst-b.swacorp.com') != -1) {
       MpElD = "https://southwest-ftst-b.swacorp.com/";
	   tdomain = "https://espanol.southwest-ftst-b.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/southwest-ftst-c.swacorp.com') != -1) {
       MpElD = "https://southwest-ftst-c.swacorp.com/";
	   tdomain = "https://espanol.southwest-ftst-c.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/southwest-ftst-d.swacorp.com') != -1) {
       MpElD = "https://southwest-ftst-d.swacorp.com/";
	   tdomain = "https://espanol.southwest-ftst-d.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/southwest-ftst-e.swacorp.com') != -1) {
       MpElD = "https://southwest-ftst-e.swacorp.com/";
	   tdomain = "https://espanol.southwest-ftst-e.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/ecom-cstg-web.swacorp.com') != -1) {
       MpElD = "https://ecom-cstg-web.swacorp.com/";
	   tdomain = "https://espanol.ecom-cstg-web.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/ecom-efix-web.swacorp.com') != -1) {
       MpElD = "https://ecom-efix-web.swacorp.com/";
	   tdomain = "https://espanol.ecom-efix-web.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/swabiz-ftst-a.swacorp.com') != -1) {
       MpElD = "https://swabiz-ftst-a.swacorp.com/";
	   tdomain = "https://espanol.swabiz-ftst-a.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/swabiz-ftst-b.swacorp.com') != -1) {
       MpElD = "https://swabiz-ftst-b.swacorp.com/";
	   tdomain = "https://espanol.swabiz-ftst-b.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/swabiz-ftst-c.swacorp.com') != -1) {
       MpElD = "https://swabiz-ftst-c.swacorp.com/";
	   tdomain = "https://espanol.swabiz-ftst-c.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/swabiz-ftst-d.swacorp.com') != -1) {
       MpElD = "https://swabiz-ftst-d.swacorp.com/";
	   tdomain = "https://espanol.swabiz-ftst-d.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/swabiz-ftst-e.swacorp.com') != -1) {
       MpElD = "https://swabiz-ftst-e.swacorp.com/";
	   tdomain = "https://espanol.swabiz-ftst-e.swacorp.com/";
    } 
		if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/swabiz-cstg-web.swacorp.com') != -1) {
       MpElD = "https://swabiz-cstg-web.swacorp.com/";
	   tdomain = "https://espanol.swabiz-cstg-web.swacorp.com/";
    } 
	if (location.host.toString().indexOf('https://espanol.southwest.com/mpel/swabiz-efix-web.swacorp.com') != -1) {
       MpElD = "https://swabiz-efix-web.swacorp.com/";
	   tdomain = "https://espanol.swabiz-efix-web.swacorp.com/";
    } 

    MpL = navigator.browserLanguage;
    if (!MpL) MpL = navigator.language;
    document.write(decodeURIComponent("%3Cscript src='") + tdomain + "/mpel.js?href=" + encodeURIComponent(location.href) + "&ref=" + encodeURIComponent(document.referrer) + "&lang=" + MpL + "' type='text/javascript'" + decodeURIComponent("%3E%3C/script%3E"))
};