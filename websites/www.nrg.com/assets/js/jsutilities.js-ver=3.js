
jQuery(document).ready(function(){
//Left Nav exists? If so, show mobile menu counterpart
	if( $('#sec nav').length ) { // implies *not* zero
		//console.log("there's a left nav");
		jQuery('#sec-navmobile').removeClass('skip');
	} else {}
//Top and Left nav active state- grabs the first,second, and last segment in the URL
	var segments = location.pathname.substr(0, location.pathname.toLowerCase().lastIndexOf('/') + 1).replace(/^\/+|\/+$/g, '').split('/');
	//jQuery('#pagewrap').addClass(segments.join(' '));
	var segment0 = segments[0];
	var segment1 = segments[1];
	var segment2 = segments[2];
	var lastSegment = segments.pop();
	var secondLastSegment = segments.slice(-1)[0];
		
	if (segment0 === "") {
		jQuery('#nav-homepage' + '> a').addClass('active on');
		jQuery('#nav-homepage').addClass('current closeOpen');
		jQuery('a.sec-homepage').addClass('on');
		jQuery('aside.sec-homepage').addClass('active');
	}
	else if (segment0 === "es" && typeof segment1 === "undefined") {
		jQuery('#nav-' + segment0 + '> a').addClass('active');
		jQuery('#nav-' + segment0).addClass('current closeOpen');
		jQuery('aside.sec-es').addClass('active');
	}
	else if (segment0 === "es" && typeof segment2 === "undefined") {
		jQuery('#nav-' + segment1 + '> a').addClass('active');
		jQuery('#nav-' + segment1).addClass('current closeOpen');
		jQuery('aside.sec-es' + '-' + segment1).addClass('active');
		jQuery('a.sec-es' + segment1).addClass('on');
	}
	else if (typeof segment1 === "undefined") {
		jQuery('#nav-' + segment0 + '> a').addClass('active on');
		jQuery('#nav-' + segment0).addClass('current closeOpen');
		jQuery('aside.sec-homepage' + '-' + segment0).addClass('active');
		jQuery('aside.sec-' + segment0).addClass('active');
		jQuery('a.sec-' + segment0).addClass('on');
	}
	else if (typeof segment2 === "undefined") {
		jQuery('#nav-' + segment0 + '> a').addClass('active');
		jQuery('#nav-' + segment0).addClass('current closeOpen');
		jQuery('.nav-' + segment0 + '-' + segment1).addClass('on');
		jQuery('aside.sec-' + segment0).addClass('active');
		jQuery('aside.sec-' + segment1).addClass('active');
		jQuery('a.sec-' + secondLastSegment + '-' + lastSegment).addClass('on');
	}
	else if (typeof segment1 != "undefined") {
		jQuery('#nav-' + segment0 + '> a').addClass('active');
		jQuery('#nav-' + segment0).addClass('current closeOpen');
		jQuery('.nav-' + segment0 + '-' + segment1).addClass('on');
		/*for Spanish */jQuery('.nav-' + segment1 + '-' +segment2).addClass('on');
		jQuery('aside.sec-' + segment0).addClass('active');
		jQuery('aside.sec-' + segment1).addClass('active');
		jQuery('aside.sec-' + segment2).addClass('active');
		jQuery('aside.sec-' + lastSegment).addClass('active');
		jQuery('a.sec-' + secondLastSegment + '-' + lastSegment).addClass('on');
	}
//Left nav helper
	jQuery('#sec nav .active h3').addClass('closeOpen');
	jQuery('#sec nav .active ul').addClass('expand');
	$("#sec nav h3 a[href!='javascript:void(0);']").parent().addClass('link');

//Data table 
	$('.data tbody tr:even').addClass('even');
	$('.data tbody tr:odd').addClass('odd');
	
//Back to opt anchor tag	
	var offset = 0;
	var duration = 300;
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset) {
			jQuery('.back-to-top').fadeIn(duration);
		} else {
			jQuery('.back-to-top').fadeOut(duration);
		}
	});
	
	jQuery('.back-to-top').click(function(event) {
		event.preventDefault();
		jQuery('html, body').animate({scrollTop: 0}, duration);
		return false;
	})

});

//Left nav helper
jQuery('#sec h3').click(function(){
	//console.log("hello world");
	var $this = jQuery(this);
	var $thisSibling = $this.siblings()
	$this.toggleClass('closeOpen');
	$this.toggleClass('collapsed');
	//$this.siblings().slideToggle();
	$thisSibling.slideUp('fast', function() {
            $thisSibling.toggleClass('expand')
                .slideDown('fast');
        })
   
});
jQuery('#nav h3').click(function(){
	var $this = jQuery(this);
	jQuery('#nav li').removeClass('closeOpen');
	$this.parent().addClass('closeOpen');
});

//Mobile nav helper
$(document).ready(function () {
    $("#navmobile label").click(function () {
        $("body").toggleClass("menumobile");
    });
});

//Drop-down helper for tabbing through the page
jQuery(function() {
	$('.liTop').each(function() {
		$('*', this).focus(function() {
			$(this).parents('.liTop').addClass('focus');
			$(this).parents('.liTop').siblings().removeClass('focus');
		});
		$('*', this).blur(function() {
			$(this).parents('.liTop').removeClass('focus');
		});
	});
});
//Drop-down helper for tabbing through the page
jQuery(function() {
	$('.nav-menu > ul > li').each(function() {
		$('*', this).focus(function() {
			$(this).parents('.nav-menu > ul > li').addClass('focus');
			$(this).parents('.nav-menu > ul > li').siblings().removeClass('focus');
		});
		$('*', this).blur(function() {
			$(this).parents('.nav-menu > ul > li').removeClass('focus');
		});
	});
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        c = ca[i];
        if (c.indexOf(name) >= 0) return c.substring(name.length);
    }
    return "";
}
//Legal expand collapse
var $button = $('.toggleLegal'),
    $text = $('footer.legal'),
    visible = false;

$button.click(function() {
	$('.toggleLegal span').toggleClass('skip');
    if (visible) {
        
		$text.slideUp('fast', function() {
            $text.addClass('skip')
                .slideDown(0);
        });
    } else {
        $text.slideUp(0, function() {
            $text.removeClass('skip')
                .slideDown('fast');
        });
    }
    visible = !visible;
});
//END Legal expand collapse