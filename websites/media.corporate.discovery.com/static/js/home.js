$(function(){
    var promo = $("#promo"),
        navPromo = $("#nav-promo");

    promo.find(".gutter").cycle({
        fx: "uncover",
        speed: 700,
        timeout: 4000,
        pager: "#nav-promo .nav-gutter",
        pause: true
    });

    // slideshow nav
    var featNavWidth = 0;
    navPromo.find("a").each(function(e){
        featNavWidth += $(this).width();
    });
    navPromo.find(".nav-gutter").width(featNavWidth);
});
