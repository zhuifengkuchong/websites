// ON-LOAD EVENTS
$(window).load(function () {
    adjustNavigation();
    //getNavDesc();
    adjustColumnHeight();
    adjustSecondaryMessagingMargin();
    adjustSubsiteLandingContent();
    $('a.current > img').attr("src", $('a.current > img').attr("activesrc"));

});

$(document).ready(function () {
    addClasses();
    getNewWindowLinks();
});

// CHECK FOR NEW WINDOW LINKS
function getNewWindowLinks() {
    $("a.non-html, a.off-site").click(function (e) {
        var event;
        if (!e) {
            event = window.event;
        }
        else {
            event = e;
        }
        if (event.shiftKey || event.altKey || event.ctrlKey || event.metaKey) {
            return true;
        }
        else {
            var newWindow = window.open(this.getAttribute('href'), '', 'width=780, height=500, scrollbars=yes, resizable=yes, toolbar=yes, location=yes, directories=no, menubar=yes, copyhistory=no');
            if (newWindow) {
                try {
                    if (newWindow.focus()) {
                        newWindow.focus();
                    }
                }
                catch (err) {
                    return false;
                }
                return false;
            }
            return true;
        }
    });
}

// ADD CLASSES v3.0
function addClasses() {
    // Add classes to different types of links to control behavior
    //$("a[href^=http\\:\\/\\/],a[href^=https\\:\\/\\/],a[target=_blank]").addClass("off-site");
    $("a[href$=\\.pdf],a[href$=\\.jpg],a[href$=\\.gif],a[href$=\\.png],a[href$=\\.sflb],a[href$=doc],a[href$=docx],a[href$=xls],a[href$=xlsx],a[href$=txt]").addClass("non-html");
    $("a[href$=\\.jpg],a[href$=\\.gif],a[href$=\\.png]").addClass("image");
    $("a[href$=\\.avi],a[href$=\\.wma],a[href$=\\.mov],a[href$=\\.f4v]").addClass("video");
    //$("a[href$=pdf]").addClass("pdf"); // Removed 10/9/2014
    $("a[href$=doc],a[href$=docx]").addClass("ms-word");
    $("a[href$=xls],a[href$=xlsx]").addClass("ms-excel");
    $("a[href$=txt]").addClass("text");
    // Build selector for current host check
    var currentDomain = window.location.host;
    currentDomain = currentDomain.toLowerCase();
    var currentDomainSelector = "a[href*=" + currentDomain.split('.').join('\\.') + "]";
    // Additional domains per NiSource site
    var additionalDomainSelectors;
    switch (currentDomain) {
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/local.columbiagasohio.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/preview.columbiagasohio.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/test.columbiagasohio.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/www.columbiagasohio.com":
            additionalDomainSelectors = ", a[href*=help\\.columbiagasohio\\.com]";
            break;
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/local.columbiagasmd.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/preview.columbiagasmd.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/test.columbiagasmd.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/www.columbiagasmd.com":
            additionalDomainSelectors = ", a[href*=help\\.columbiagasmd\\.com]";
            break;
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/local.columbiagasky.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/test.columbiagasky.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/preview.columbiagasky.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/www.columbiagasky.com":
            additionalDomainSelectors = ", a[href*=help\\.columbiagasky\\.com]";
            break;
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/local.columbiagasva.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/test.columbiagasva.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/preview.columbiagasva.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/www.columbiagasva.com":
            additionalDomainSelectors = ", a[href*=help\\.columbiagasva\\.com]";
            break;
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/local.columbiagasma.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/test.columbiagasma.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/preview.columbiagasma.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/www.columbiagasma.com":
            additionalDomainSelectors = ", a[href*=help\\.columbiagasma\\.com]";
            break;
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/local.columbiagaspa.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/preview.columbiagaspa.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/test.columbiagaspa.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/www.columbiagaspa.com":
            additionalDomainSelectors = ", a[href*=help\\.columbiagaspa\\.com], a[href*=www\\.directlinkeservices\\.com]";
            break;
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/local.nipsco.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/preview.nipsco.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/test.nipsco.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/www.nipsco.com":
            additionalDomainSelectors = ", a[href*=help\\.nipsco\\.com]";
            break;
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/local.nisource.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/preview.nisource.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/test.nisource.com":
        case "https://www.nisource.com/Sitefinity/WebsiteTemplates/NiSource/App_Themes/Default/JavaScript/www.nisource.com":
            additionalDomainSelectors = ", a[href*=ir\\.nisource\\.com]";
            break;
        default:
            additionalDomainSelectors = "";
    }
    currentDomainSelector = currentDomainSelector + additionalDomainSelectors;
    // Exclude certain links from updated behavior
    $(currentDomainSelector).removeClass("off-site");
    $(".ModalSurvey, .modalSurvey, .modalsurvey, .ModalVideo, .modalVideo, .modalvideo").removeClass("off-site");
}

$(document).ready(function () {
    $('table').not('.tableExcludeAltStyle').each(function() {
        $('tr:odd',  this).addClass('alt');
        $('tr:even',  this).removeClass('alt');
    });
	// phase out function.  jquery does a better job.
    //createAlternateRows();
});

function activate(control) {
    $(control).attr("src", $(control).attr("activesrc"));

}

// This auto adjusts alternate rows as new rows are added
function createAlternateRows() {
    // Check that the browser is DOM compliant
    if (document.getElementById) {
        // Find all table rows		
        
        var row;
        var tablerows = document.getElementsByTagName('tr');
        var counter = 0;
        for (var i = 0; i < tablerows.length; i++) {
            row = tablerows[i];
            if (counter == 1) {
                row.className = 'alt';
                counter = 0;
            }
            else {
                row.className = '';
                counter++;
            }
        }
    }
    $("#tblCustEnrol tr").removeClass("alt");
}

function onClickActivate(control) {
    $(".tabs > li > div > a > img").each(function () {
        $(this).attr("src", $(this).attr("inactivesrc"));
    });
    $(control).attr("src", $(control).attr("activesrc"));
    $(control).parent().parent().addClass("current");
}

function deactivate(control) {
    if (!$(control).parent().hasClass("current")) {
        $(control).attr("src", $(control).attr("inactivesrc"));
    }
}
function adjustColumnHeight() {
    var leftHeight = $("#leftColumn").height();
    var rightHeight = $("#middleColumn").height();
    if (leftHeight > rightHeight) {
        $("#middleColumn").css("height", leftHeight);
    }
    else {
        $("#leftColumn").css("height", rightHeight);
    }

    var leftHeight = $("#navigation-content-container").height();
    var rightHeight = $("#content-container").height();
    if (leftHeight > rightHeight) {
        $("#content-container").css("height", leftHeight);
    }
    else {
        $("#navigation-content-container").css("height", rightHeight);
    }
}

function adjustSecondaryMessagingMargin() {
    var leftHeight = $("#leftSecondaryMessaging").height();
    var rightHeight = $("#rightSecondaryMessaging").height();
    if (leftHeight > rightHeight) {
        var marginHeight = leftHeight - rightHeight;
        $("#rightSecondaryMessaging").css("margin-top", marginHeight);
    }
    else {
        var marginHeight = rightHeight - leftHeight;
        $("#leftSecondaryMessaging").css("margin-top", marginHeight);
    }
}

function adjustSubsiteLandingContent() {
    var leftHeight = $("div#subsitelanding div#landingcontent div#leftlandingcontent").height();
    var rightHeight = $("div#subsitelanding div#landingcontent div#rightlandingcontent").height();
    var ulLeftHeight = $("div#subsitelanding div#landingcontent div#leftlandingcontent .heightmanagement").height();
    var ulRightHeight = $("div#subsitelanding div#landingcontent div#rightlandingcontent .heightmanagement").height();
    if (leftHeight > rightHeight) {
        $("div#subsitelanding div#landingcontent div#rightlandingcontent").css("height", leftHeight);
    }
    else {
        $("div#subsitelanding div#landingcontent div#leftlandingcontent").css("height", rightHeight);
    }

    if (ulLeftHeight > ulRightHeight) {
        $("div#subsitelanding div#landingcontent div#rightlandingcontent .heightmanagement").css("height", ulLeftHeight);
    }
    else {
        $("div#subsitelanding div#landingcontent div#leftlandingcontent .heightmanagement").css("height", ulRightHeight);
    }
}

function makeForm() {
    var form = document.getElementById('Form');
    form.action = "https://www.directlinkeservices.com/nisource/portal/!ut/p/b1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOKNDIINDAx8PDxD_AJMDTwtAnz8g0L9DUI9TfXD9aPAShz9Qp0DPU2MDPz9gswMjCzdLf39_CwM3d1NoAoMcABHA30_j_zcVP2C7Ow0R0dFRQAHPRjW/dl4/d5/L0lDU0lKSmdrS0NsRUpJQSEvb01vZ0FFSVFoakVLSVFBQkdjWndqSlFRQVFnIS80RzNhRDJnanZ5aENreUZNTlFqSVVRUSEvWjdfMjBTMDA4NDlRSDM2NTBJRUNSOTk0VjAwMDYvMC9pYm0uaW52LzE5NjU4NTQyMjY5NQ!!/";
    $(form).append('<input type="hidden"id="userID" value="' + $('#userID').val() + '" />');
    $(form).append('<input type="hidden"id="password" value="' + $('#password').val() + '" />');
    $(form).append('<input type="hidden"id="LDC" value="va" />');
    form.submit();
}

function makeForm(action, ldc) {
    var form = document.getElementById('Form');
    form.action = action;
    $(form).append('<input type="hidden"id="userID" value="' + $('#userID').val() + '" />');
    $(form).append('<input type="hidden"id="password" value="' + $('#password').val() + '" />');
    $(form).append('<input type="hidden"id="LDC" value="' + ldc + '" />');
    form.submit();
}

// ADJUST WIDTH OF SEARCH ELEMENTS BASED ON NAVIGATION WIDTH
function adjustNavigation() {
    var totalWidth = $("div#content").width();
    // Make sure display is set to inline (should be set in CSS already)
    $("ul#nav_primary").css("display", "inline");
    //var navWidth = $("ul#nav_primary").width();
    var navWidth = 0;
    $("ul#nav_primary li").each(function () {
        navWidth += $(this).width();
    });
    var navNumber = $("ul#nav_primary li").length;
    var baseSearchWidth = $("div#search").width();
    var searchPaddingLeft = $("div#search").css("padding-left");
    var searchPaddingRight = $("div#search").css("padding-right");
    var buttonWidth = $("div#search .submit, div#search .sfsearchSubmit").width();
    var buttonWhitespace = 37;
    var searchPadding = parseFloat(searchPaddingLeft, 10) + parseFloat(searchPaddingRight, 10);
    var maxNavWidth = totalWidth - baseSearchWidth;
    var maxNavImageWidths = maxNavWidth - navNumber;
    // Stop function if search box is positioned absolutely (and thus no adjustment needed)
    // Used for NIPSCO, among others
    if ($("div#search").css("position") == "absolute" || $("div#search").css("display") == "none") {
        return false;
    }
    // Stop function if navigation width is too wide
    if (navWidth > maxNavWidth) {
        // Display alert if user is logged into administration area
        var theQS = window.location.search.substring(1);
        if (theQS.indexOf("cmspagemode") != -1) {
            alert("The navigation images used are too large. Please reduce the total width to " + maxNavImageWidths + " pixels or less.");
        }
        return false;
    }
    var searchWidth = totalWidth - navWidth - searchPadding;
    var inputWidth = searchWidth - buttonWidth - buttonWhitespace;
    // Make sure navigation width is set before adjusting search box
    if (navWidth > 0 && navWidth != "" && navWidth != null) {
        $("div#search, .sfsearchBox").width(searchWidth);
        $("div#search .sfsearchTxt").width(inputWidth);
    }
}

function initOverlays() {
    /* Hide by default - custom GML */
    $(".simple_overlay, .apple_overlay").css("display", "none");
    /* Video overlay - custom GML */
    $("span.video[rel]").overlay({
        effect: 'apple',
        mask: 'gray',
        onLoad: function (content) {
            this.getOverlay().find("a.player").flowplayer(0).load();
        },
        onClose: function (content) {
            $f().unload();
        }
    });
    $(".overlay_trigger[rel]").overlay({
        mask: "gray",
        fixed: "true"
    });
    var triggers = $(".modalInput").overlay({
        mask: {
            color: "#ebecff",
            loadSpeed: 200,
            opacity: 0.9
        },
        closeOnClick: false
    });
    var buttons = $("#yesno button").click(function (e) {
        var yes = buttons.index(this) === 0;
        triggers.eq(0).html("You clicked " + (yes ? "yes" : "no"));
    });
    $("#prompt form").submit(function (e) {
        triggers.eq(1).overlay().close();
        var input = $("input", this).val();
        triggers.eq(1).html(input);
        return e.preventDefault();
    });
    // IE 7 bug fix (also for usability) - close overlays when clicked anywhere - custom GML
    $("div#main").addClass("close");

    // if the function argument is given to overlay,
    // it is assumed to be the onBeforeLoad event listener
    $("a.CommunityOverlay[rel]").overlay({
        mask: 'gray',
        effect: 'apple',

        onBeforeLoad: function () {

            // grab wrapper element inside content
            var wrap = this.getOverlay().find(".contentWrap");

            // load the page specified in the trigger
            wrap.load(this.getTrigger().attr("href"));

            // IE 7 bug fix (also for usability) - displays content correctly
            $('#main').css("position", "static");
            $('.aside').css("position", "static");

        },
        // IE 7 bug fix (also for usability) - displays content correctly
        onLoad: function () {
            $('#content .callout').css("position", "static");
        },
        // IE 7 bug fix (also for usability) - displays content correctly
        onClose: function () {
            $('#content .callout').css("position", "relative");
        }
    });
}

function CheckPhoneNumberLength(fieldToMeasure, fieldToJump) {
    var currentLength = document.getElementById(fieldToMeasure).value.length;
    if (currentLength == 3) { document.getElementById(fieldToJump).focus() }
}

// CREATE DESCRIPTION FOR PRIMARY NAVIGATION
//function getNavDesc() {
//    var strCurrentNavDesc;
//    strCurrentNavDesc = $('meta[name=description]').attr("content");
//    $("div#navigationDescription").text(strCurrentNavDesc);
/* REM GML (pending decision regarding usage) */
//    var strTempNavDesc;
//    $("ul#nav_primary a").mouseover(function () {
//        strTempNavDesc = $(this).attr("title");
//        $("div#navigationDescription").text(strTempNavDesc);
//        $(this).attr("title", "");
//    });
//    $("ul#nav_primary a").mouseout(function () {
//        strTempNavDesc = $("div#navigationDescription").text();
//        $(this).attr("title", strTempNavDesc);
//        $("div#navigationDescription").text(strCurrentNavDesc);
//  });
//}


//CREATE DESCRIPTION FOR PRIMARY NAVIGATION
function getNavDesc() {
    var strCurrentNavDesc;
    strCurrentNavDesc = $("ul#nav_primary a.hotstate").attr("title");
    strDescription = $("ul#nav_primary a.hotstate").attr("showdescription");
    //if(strDescription == "true")
    //{
    $("div#navigationDescription").text(strCurrentNavDesc);
    //}
    //else
    //{
    //	$("div#navigationDescription").text("Welcome to NiSource Careers");
    //}
}
