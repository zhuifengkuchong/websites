<script id = "race98b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race98={};
	myVars.races.race98.varName="homepagearrowsleft__onmouseout";
	myVars.races.race98.varType="@varType@";
	myVars.races.race98.repairType = "@RepairType";
	myVars.races.race98.event1={};
	myVars.races.race98.event2={};
	myVars.races.race98.event1.id = "homepagearrowsleft";
	myVars.races.race98.event1.type = "onmouseout";
	myVars.races.race98.event1.loc = "homepagearrowsleft_LOC";
	myVars.races.race98.event1.isRead = "True";
	myVars.races.race98.event1.eventType = "@event1EventType@";
	myVars.races.race98.event2.id = "Lu_DOM";
	myVars.races.race98.event2.type = "onDOMContentLoaded";
	myVars.races.race98.event2.loc = "Lu_DOM_LOC";
	myVars.races.race98.event2.isRead = "False";
	myVars.races.race98.event2.eventType = "@event2EventType@";
	myVars.races.race98.event1.executed= false;// true to disable, false to enable
	myVars.races.race98.event2.executed= false;// true to disable, false to enable
</script>

