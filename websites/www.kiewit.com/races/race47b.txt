<script id = "race47b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race47={};
	myVars.races.race47.varName="Lu_Id_div_39__onmouseover";
	myVars.races.race47.varType="@varType@";
	myVars.races.race47.repairType = "@RepairType";
	myVars.races.race47.event1={};
	myVars.races.race47.event2={};
	myVars.races.race47.event1.id = "Lu_Id_div_39";
	myVars.races.race47.event1.type = "onmouseover";
	myVars.races.race47.event1.loc = "Lu_Id_div_39_LOC";
	myVars.races.race47.event1.isRead = "True";
	myVars.races.race47.event1.eventType = "@event1EventType@";
	myVars.races.race47.event2.id = "Lu_DOM";
	myVars.races.race47.event2.type = "onDOMContentLoaded";
	myVars.races.race47.event2.loc = "Lu_DOM_LOC";
	myVars.races.race47.event2.isRead = "False";
	myVars.races.race47.event2.eventType = "@event2EventType@";
	myVars.races.race47.event1.executed= false;// true to disable, false to enable
	myVars.races.race47.event2.executed= false;// true to disable, false to enable
</script>

