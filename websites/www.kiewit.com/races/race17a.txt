<script id = "race17a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race17={};
	myVars.races.race17.varName="Lu_Id_li_24__onmouseover";
	myVars.races.race17.varType="@varType@";
	myVars.races.race17.repairType = "@RepairType";
	myVars.races.race17.event1={};
	myVars.races.race17.event2={};
	myVars.races.race17.event1.id = "Lu_DOM";
	myVars.races.race17.event1.type = "onDOMContentLoaded";
	myVars.races.race17.event1.loc = "Lu_DOM_LOC";
	myVars.races.race17.event1.isRead = "False";
	myVars.races.race17.event1.eventType = "@event1EventType@";
	myVars.races.race17.event2.id = "Lu_Id_li_30";
	myVars.races.race17.event2.type = "onmouseover";
	myVars.races.race17.event2.loc = "Lu_Id_li_30_LOC";
	myVars.races.race17.event2.isRead = "True";
	myVars.races.race17.event2.eventType = "@event2EventType@";
	myVars.races.race17.event1.executed= false;// true to disable, false to enable
	myVars.races.race17.event2.executed= false;// true to disable, false to enable
</script>

