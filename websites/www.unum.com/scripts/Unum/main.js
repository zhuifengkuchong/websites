/*******************************************************************************

	Title: Unum
	Date: March 2013	

*******************************************************************************/

(function ($) {

    var Unum = {

        init: function () {
            $("body").removeClass("no-js");
            Unum.foooterCheck();
            Unum.audienceDoBoxCheck();
            //Unum.sidebarCheck();
            Unum.subNavLineHeightCorrection();
            Unum.iPadOrientation();
            Unum.colorboxInit();
            Unum.ie8changes();
            Unum.iPadHoverFix();
        },

        /*sidebarCheck: function() {
        if($('.sidebar-right').length > 0 ) {
        $('.content-general').addClass('sidebar-sibling');
        }
        },*/


        iPadOrientation: function () {
            /*if (navigator.platform === 'iPad') {
            window.onorientationchange = function () {
            location.reload();
            }
            }*/
        },

        foooterCheck: function () {
            var winHeight = $(window).height();
            /* for US */
            //var homeDivHeight = $(document.getElementsByClassName("home")).height();
            var homeDivHeight = $(".home").height();
            var heightDif = winHeight - homeDivHeight;
            if (homeDivHeight != null && heightDif > 3) {
                var heightToUse = heightDif + 1;
                //$(document.getElementsByClassName("footer-bottom-full-view")).height(heightToUse);
                $('.footer-bottom-full-view').height(heightToUse);
            }
            /* for Group */
            //var homeGroupDivHeight = $(document.getElementsByClassName("home-group")).height();
            var homeGroupDivHeight = $(".home-group").height();
            var heightGroupDif = winHeight - homeGroupDivHeight;
            if (homeGroupDivHeight != null) {
                if (heightGroupDif > 3) {
                    var heightToUseGroup = heightGroupDif + 1;
                    //$(document.getElementsByClassName("footer-bottom-full-view")).height(heightToUseGroup);
                    $('.footer-bottom-full-view').height(heightToUseGroup);
                }
                else {
                    //$(document.getElementsByClassName("footer-bottom-full-view")).css("display", "none");
                    $('.footer-bottom-full-view').css("display", "none");
                }
            }
        },

        audienceDoBoxCheck: function () {
            /* for US Audience Landing Page*/
            var audienceBoxHeight = $("#boxAudience").outerHeight();
            var doBoxHeight = $("#doBoxWhiteArea").outerHeight();
            if (audienceBoxHeight != null && doBoxHeight != null) {
                $("#doBoxWhiteArea").height(audienceBoxHeight - 88);
            }

        },

        subNavLineHeightCorrection: function () {
            var ulOrange1Height = $("ul.orange").first().height();
            var ulYellow1Height = $("ul.yellow").first().height();
            var ulGreen1Height = $("ul.green").first().height();
            var ulSea1Height = $("http://www.unum.com/scripts/Unum/ul.sea").first().height();
            var ulBlue1Height = $("http://www.unum.com/scripts/Unum/ul.blue").first().height();
            var ulOrange2Height = $("ul.orange").last().height();
            var ulYellow2Height = $("ul.yellow").last().height();
            var ulGreen2Height = $("ul.green").last().height();
            var ulSea2Height = $("http://www.unum.com/scripts/Unum/ul.sea").last().height();
            var ulBlue2Height = $("http://www.unum.com/scripts/Unum/ul.blue").last().height();

            var biggestUL = Math.max(ulOrange1Height, ulYellow1Height, ulGreen1Height, ulSea1Height, ulBlue1Height, ulOrange2Height, ulYellow2Height, ulGreen2Height, ulSea2Height, ulBlue2Height);
            //ie8 renders differently so needs no additional pixels added
            if ($.browser.msie && $.browser.version.substr(0, 1) <= 8) {
                biggestUL = biggestUL;
            }
            else {
                biggestUL = biggestUL + 5;
            }
            biggestUL = biggestUL.toString() + "px";
            $('.row-fluid .subnav ul').css("min-height", biggestUL);
            $('.row-fluid .subnav ul li ul').css("min-height", "1px");
        },



        colorboxInit: function () {
            $("a.colorbox").colorbox();
        },

        ie8changes: function () {
            if ($.browser.msie && $.browser.version.substr(0, 1) <= 8) {
                $('.home .header-nav nav li:last-child').css('background', 'none');
                $('.header .header-nav nav li:last-child').css('background', 'url(/images/Unum/us/separator_item.png) no-repeat left 1px').css('padding-right', '0');
                $('.nav-home div > ul > li.first-item .subnav-home > li:last-child').css('background', '#32B9BE');
                $('.nav-home div > ul > li.second-item .subnav-home > li:last-child').css('background', '#9CCA62');
                $('.nav-home div > ul > li.third-item .subnav-home > li:last-child').css('background', '#F8A346');
                $('.row-fluid [class*="span"].header-top').css('margin-left', '0');
                $('.breadcrumbs ul li:last-child').addClass('after');
                $('.footer-home-group .footer-social .span6:last-child li').css('margin-right', '0');
                $('.nav-home div > ul > li:last-child a').css('border-bottom', 'none');
                $('.content-general .images img').wrap('<div class="ie8img" />');
            }
        },

        iPadHoverFix: function () {
            $(function () {
                Unum.searchIconBackgroundColor = $('div.search > a').css('background-color');
                Unum.searchIconBackgroundPosition = $('div.search a span').css('background-position');
                if (navigator.userAgent.match(/iPad/i) != null) {
                    $('html').addClass('ipad');
                    $('div.search a').click(function () {
                        //alert($('div.search > div:visible').length);
                        if ($('div.search > div:visible').length > 0) {
                            $('div.search > div').hide();
                            $('div.search a').css('background-color', Unum.searchIconBackgroundColor);
                            $('div.search a span').css('background-position', Unum.searchIconBackgroundPosition);
                        } else {
                            $('div.search > div').show();
                            $('div.search a').css('background-color', '#767e84');
                            $('div.search a span').css('background-position', '0 -22px');
                        }
                        return false;
                    });
                }
            });
        }

    }


    $(function () {
        Unum.init();
    });

})(jQuery);


function LoadAjaxContent(itemName, container) {
    $.ajax({
        type: 'GET',
        url: '/metadata/ajax/' + itemName + '.aspx',
        cache: false,
        success: function (data) {
            $('#' + container).html(data);
        }
    });
}