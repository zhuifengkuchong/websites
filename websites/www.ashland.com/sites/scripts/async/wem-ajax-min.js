(function(_1){
if(!(typeof _1.OT==="undefined"||typeof _1.OT.wem==="undefined")){
return;
}
var OT={};
OT.wem={query:((typeof this.vQuery!="undefined")?vQuery:undefined),combine:function(_3,_4){
return OT.wem.query.extend(_3,_4||{});
},extend:function(_5,_6){
this.combine(_5.prototype,_6);
return _5;
}};
OT.wem.utility={replaceTokens:function(_7,_8){
var _9=_7;
var re;
for(var _b in _8){
re=new RegExp("\\%\\{"+_b+"\\}","g");
_9=_9.replace(re,_8[_b]);
}
return _9;
},removeUrlParameters:function(_c,_d){
var _e=_c.split("?");
if(_e.length>=2){
var _f=_e.shift();
var _10=_e.join("?");
var _11=_10.split(/[&;]/g);
for(var x=_d.length;x-->0;){
for(var i=_11.length;i-->0;){
var _14=encodeURIComponent(_d[x])+"=";
if(_11[i].lastIndexOf(_14,0)!==-1){
_11.splice(i,1);
}
}
}
_c=_f+"?"+_11.join("&");
}
return _c;
}};
OT.wem.ajax={addURL:function(id,url){
if(id&&url){
this.internal.urls[id]=url;
return true;
}else{
return false;
}
},findURL:function(id){
if(id&&this.exists(id)){
return this.internal.urls[id];
}else{
return undefined;
}
},execute:function(id,_19,_1a,_1b,_1c){
var url=this.findURL(id);
if(!url){
return;
}
if(_1c){
var _1e=_1c.protocol||"http";
var _1f=_1c.port&&_1c.port!="80"?":"+_1c.port:"";
var _20=_1e+"://"+_1c.host+_1f;
url=_20+url;
}
if(_1b){
if(_19){
var _21=[];
for(var k in _1b){
_21.push(k);
}
url=OT.wem.utility.removeUrlParameters(url,_21);
OT.wem.combine(_19,_1b);
}else{
url=OT.wem.utility.replaceTokens(url,_1b);
}
}
var _23=_1a||{};
var _24={url:url,data:_19,beforeSend:_23.beforeSend,success:_23.success,complete:_23.complete,error:_23.error,type:_19?"POST":"GET"};
OT.wem.query.ajax(_24);
},removeURL:function(id){
if(id&&this.internal.urls[id]){
delete this.internal.urls[id];
return true;
}else{
return false;
}
},exists:function(id){
return this.internal.urls.hasOwnProperty(id);
},internal:{urls:{}}};
_1.OT=OT;
})(window);

