function impressionRedirect(e) {
	var http = new XMLHttpRequest();
	http.open("GET", e.getAttribute("class"), false);
	http.send(null);
}

function set_cookie ( cookie_name, cookie_value, lifespan_in_days, valid_domain )
{
	// http://www.thesitewizard.com/javascripts/cookies.shtml
	var domain_string = valid_domain ? ("; domain=" + valid_domain) : '' ;
	document.cookie = cookie_name + "=" + encodeURIComponent( cookie_value ) +
	"; max-age=" + 60 * 60 * 24 * lifespan_in_days +
	"; path=/" + domain_string ;
}

function get_cookie ( cookie_name )
{
	// http://www.thesitewizard.com/javascripts/cookies.shtml
	var cookie_string = document.cookie ;
	if (cookie_string.length != 0) {
		var re = new RegExp('; ' + cookie_name + '=([^\\s;]*)');
		var cookie_value = cookie_string.match ( re );
		if (cookie_value != null)
		{
			if (isNaN(cookie_value[1]))
			{
				return 0;
			}
			return decodeURIComponent ( cookie_value[1] ) ;
		}
	}
	return '' ;
}