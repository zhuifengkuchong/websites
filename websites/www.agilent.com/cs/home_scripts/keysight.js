var keysightTitle = "";
var keysightDesc1="As of November 1, 2014, Keysight Technologies Inc. (&quot;Keysight&quot;), the division of Agilent Technologies that develops and sells electronic  measurement (EM) products, will de-merge from the Agilent Technologies group into a separate publicly traded company, Keysight Technologies Inc."
var keysightDesc2="In order to ensure business continuity for customers, Agilent Technologies will transfer data for customers of its current Keysight division to Keysight  Technologies Inc., <br/>unless you contact <a href='mailto:privacy_advocate@keysight.com'>privacy_advocate@keysight.com</a>";
//<a href='mailto:privacy_advocate@keysight.com'> </a> 
var keysightLastModified="2013-11-30 00:00:00.0";
var keysightClose="Close";
var myTop;

if($("#eucookie").length == 0) {
  myTop = "0px";
}else{
   myTop = "63px";	
}

function keysightGetCookie(c_name) {
	if (document.cookie.length>0) {
		c_start=document.cookie.indexOf(c_name + "=");
		if (c_start!=-1) {
			c_start=c_start + c_name.length+1;
			c_end=document.cookie.indexOf(";",c_start);
			if (c_end==-1) c_end=document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}
function keysightSetCookie(c_name,value,expiredays) {
	var exdate=new Date();exdate.setDate(exdate.getDate()+expiredays);
	var cookieVal=c_name+ "=" +escape(value)+ ((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
	if(location.hostname.match(/\.agilent\.com$/)) {
		cookieVal += "http://www.agilent.com/cs/home_scripts/;path=/;domain=.agilent.com";
	}
	document.cookie = cookieVal;
}
var keysightTimer;
var keysightCookieName = "Keysight_CookieNotice";
function keysightDisable(hideNotice) {
	clearTimeout(keysightTimer);
	keysightSetCookie(keysightCookieName,keysightLastModified,90);
	if(hideNotice) {
		document.getElementById("keysightcookie").style.display = "none";
		document.body.className = document.body.className.replace(/[ ]*\bkeysightOffset\b/g,'')
	}
}
function keysightInit() {
	var cVal = keysightGetCookie(keysightCookieName);
	if(cVal >= keysightLastModified)
		return;
	var keysightWrapper = document.createElement("DIV");
	keysightWrapper.id = "keysightcookie";
	keysightWrapper.innerHTML = '<div style="text-align:left;font-style:normal;font-family: Arial, Helvetica, sans-serif;color: #444;font-size: 12px;line-height: 1.3;border-bottom: 1px solid #888;width:100%;position:fixed;top:'+myTop+';left:0;z-index:1000000;background: #fbf9e0;-moz-box-shadow: 0 5px 10px #888;-webkit-box-shadow: 0 5px 10px #888;box-shadow: 0 5px 10px #888;"><div style="padding: 10px 40px 15px"><table border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align: top"><h4 style="font-style:normal;font-weight: bold;font-size: 110%;margin: 0 0 5px">' + keysightTitle + '</h4><p style="font-weight: normal;margin: 0;font-size: 100%;font-style:normal">' + keysightDesc1 + ' </p><p style="font-weight: normal;margin: 0;font-size: 100%;font-style:normal">' + keysightDesc2 + ' </p></td><td style="padding: 10px 0 0 50px; vertical-align:bottom"><button onclick="keysightDisable(true);">' + keysightClose + '</button></td></tr></table></div></div>';
	document.body.appendChild(keysightWrapper);
	//document.write('<style type="text/css">body.keysightOffset { padding-top: ' + keysightWrapper.getElementsByTagName('DIV')[0].offsetHeight + 'px !important;}</style>');
	document.body.className += " keysightOffset";
	keysightTimer = setTimeout('keysightDisable()',60000);
}
if(top == self) {
	keysightSetCookie("cookies","cookies");
	if(keysightGetCookie("cookies") == "cookies") {
		keysightSetCookie("cookies","",-1);
		keysightInit();
	}
}