<script id = "race7a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race7={};
	myVars.races.race7.varName="Function[3240].timeout";
	myVars.races.race7.varType="@varType@";
	myVars.races.race7.repairType = "@RepairType";
	myVars.races.race7.event1={};
	myVars.races.race7.event2={};
	myVars.races.race7.event1.id = "cboxCurrent";
	myVars.races.race7.event1.type = "onmouseover";
	myVars.races.race7.event1.loc = "cboxCurrent_LOC";
	myVars.races.race7.event1.isRead = "False";
	myVars.races.race7.event1.eventType = "@event1EventType@";
	myVars.races.race7.event2.id = "cboxNext";
	myVars.races.race7.event2.type = "onmouseover";
	myVars.races.race7.event2.loc = "cboxNext_LOC";
	myVars.races.race7.event2.isRead = "True";
	myVars.races.race7.event2.eventType = "@event2EventType@";
	myVars.races.race7.event1.executed= false;// true to disable, false to enable
	myVars.races.race7.event2.executed= false;// true to disable, false to enable
</script>

