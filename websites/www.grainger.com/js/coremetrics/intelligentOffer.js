
// initialize cookies
window.group_id      = [20];                           // random number between 0 and 99 inclusive - used for ab test group
window.products      = [];                             // products (most recently viewed first)
window.categories    = [];                             // categories (most recently viewed first)
window.brands        = [];                             // brands (most recently viewed first)
window.p_viewed      = [];                             // products viewed
window.p_carted      = [];                             // products carted
window.p_purchased   = [];                             // products purchased
window.c_viewed      = [];                             // categories viewed
window.c_n_views     = [];                             // categories number viewed
window.b_viewed      = [];                             // brands viewed
window.b_n_views     = [];                             // brands number viewed

window.a_arrays = [];
arrays = [p_viewed.join(),p_carted.join(),p_purchased.join(),c_viewed.join(),c_n_views.join(),b_viewed.join(),b_n_views.join()];
window.cookie_value = [group_id.join(), products.join('~'), categories.join('~'), brands.join('~'), arrays.join('|')].join('~|~');


//global image path variables and functions to create image paths for new platform from a current platform image url or an image name
//these values match ones created by ImageMedia.java and dataload project.properties which are unavailable to client side scripting
window.host = location.host;
window.np_image_prefix = location.protocol + "//static.grainger.com/rp/s/is";

//accepts an image url, splits it by "/" and returns the last segment 
window.parseImageUrl = function (image_url) {
  var np_image_url = image_url;
  if (image_url.indexOf("http") != -1){
    np_image_url = image_url.split("/").pop();
  }
    return np_image_url;
};
//accepts a zone and returns string to represent image size based on the zone value
window.lookupImageSizePath = function lookupImageSizePath(zone) {
    var image_size_path;
    switch (zone){
        case "HPBTZ11":        //homepage 120x120
          image_size_path = "?$smthumb$";
          break;
        case "A2CSTKZ":        //homepage 120x120
            image_size_path = "?$smthumb$";
            break;
        case "IDPBTZ12":    //item details bottom rail 120x120
          image_size_path = "?$smthumb$";
          break;
        case "CRTBTZ5":        //cart 120x120
          image_size_path = "?$smthumb$";
          break;
        case "A2CBTZ4":        //add to cart 120x120
          image_size_path = "?$smthumb$";
          break;
        case "STKZ":        //add to cart 120x120
          image_size_path = "?$smthumb$";
          break;
        case "IDPRRZ13":    //item details right rail 80x80
          if(jQuery(window).width() < 1383) {
            image_size_path = "?$smthumb$";
            } else {
              image_size_path = "?$lgswatch$";
            }
          break;
        case "STATIC2":    //Static Page right rail 80x80
          if(jQuery(window).width() < 1383) {
            image_size_path = "?$smthumb$";
            } else {
              image_size_path = "?$lgswatch$";
            }
          break;
        case "STATIC1":    //Static page bottom rail 80x80
            image_size_path = "?$smthumb$";
            break;
        case "IDPRRZ11":    //parent details right rail 80x80
            image_size_path = "?$lgswatch$";
            break;
        case "HTSEARCH":        //Tiered Search
            image_size_path = "?$smthumb$";
            break;
        case "SRTZ3":		//IIB-268
            image_size_path = "?$smthumb$";
            break;
        case "CAT23":        //category carousel 120x120
        	image_size_path = "?$smthumb$";
        	break;
        default: //120x120
          image_size_path = "?$smthumb$";
    }
    return image_size_path;
};
//accepts a zone and image url and returns url of image on the server
window.buildImagePath = function (zone, image_url){
    var display_np_image_url = np_image_prefix;
    display_np_image_url += "/image/Grainger/";
    display_np_image_url += parseImageUrl(image_url);
    display_np_image_url = display_np_image_url.slice(0,-4);
    display_np_image_url += lookupImageSizePath(zone);
    return display_np_image_url;
};

//accepts a zone and returns string to represent image size based on the zone value. Assumes all thumbnails are square. returns int.
window.lookupImageSizes = function (zone) {
    var image_size_wid = 0;
    switch (zone){
        case "HPBTZ11":        //homepage 120x120
          image_size_wid = 120;
          break;
        case "A2CSTKZ":        //homepage 120x120
            image_size_wid = 120;
            break;
        case "IDPBTZ12":    //item details bottom rail 120x120
          image_size_wid = 120;
          break;
        case "CRTBTZ5":        //cart 120x120
          image_size_wid = 120;
          break;
        case "A2CBTZ4":        //add to cart 120x120
          image_size_wid = 120;
          break;
        case "STKZ":        //add to cart 120x120
          image_size_wid = 120;
          break;
        case "IDPRRZ13":    //item details right rail 80x80
          if(jQuery(window).width() < 1383) {
            image_size_wid = 120;
            } else {
              image_size_wid = 80;
            }
          break;
        case "STATIC1":    //static page bottom rail 120x120
           image_size_wid = 120;
          break;
        case "STATIC2":    //Static Page right rail 80x80
          if(jQuery(window).width() < 1383) {
            image_size_wid = 120;
            } else {
              image_size_wid = 80;
            }
          break;
        case "IDPRRZ11":    //Parent details right rail 80x80
            image_size_wid = 80;
            break;
        case "HTSEARCH":        //Tiered Search
            image_size_wid = 120;
            break;
        case "CAT23":        //Category Carousel
            image_size_wid = 120;
            break;
        default: //120x120
          image_size_wid = 120;
    }
    return image_size_wid;
};
//accepts a zone and calls lookupImageSizes to buld height and width. returns string
window.buildImageSize = function (zone) {
    var display_image_size = lookupImageSizes(zone),
        display_image_size_story = "";

    display_image_size_story += " height=\""+display_image_size+"\" width=\""+display_image_size+"\"";

    return display_image_size_story;
};

/******************************** NEWER BUILD FUNCTION *********************************************/

/* - - - - - Carousel Data Functions - - - - - */
var ioCarousel = {
    carouselObject: {},
    categoryObject: {},
    pricingObject: {},
    parseParentData: function(componentTextArray, divID, layout, itemArray, spVar){
        var newObject,
        // set layout Class for display (small, vertical, etc.)
            layoutClass = this.determineLayoutClass(layout);

        newObject = {
            title: componentTextArray[0],
            divID: divID,
            layoutClass: layoutClass, // set class for carousel width
            pagination: (itemArray.length > 4) ? true:false, // set pagination
            totalPages: Math.ceil(itemArray.length / 4), // set number of pages
            spVar: spVar
        };

        return newObject;
    },
    parseProductData: function(componentText, divID, productIDArray, productInfoArray, zone, layout, displayTarget, removeFirst){
        var i = 0, // loop variable
        // for vertical display, only print the first four items
            arrayLength = (layout === 'vertical') ? 4 : productIDArray.length,
        // separate piped component text
            componentTextArray = componentText.split('|'),
            showPrices = (componentTextArray[1] === "sp=Y") ? true : false,
            linkParam = ( isNotEmpty( componentTextArray[3] )) ? componentTextArray[3].replace('?','') : '',
        // set manual link click logic
            manualLink = ( divID === 'addtocart-items-box' || divID === 'addtocart-items-box2' ) ? true : false,
            divOrigin = ( divID === 'addtocart-items-box' || divID === 'addtocart-items-box2' ) ? 'adrt' : '',
        // set link parameter logic (analytics?)
            paramArray = ( isNotEmpty(linkParam) ) ? this.formatClickParam( linkParam ) : '',
            spVar = paramArray[0],
            vcVar = paramArray[1],
       // set image sizes based on returned string (consolidate formatImageSize and buildImageSize?)
            imageSizeArray = this.formatImageSize( buildImageSize(zone) ),
            height = imageSizeArray[0],
            width = imageSizeArray[1],
        // set empty variables for later use   
            newItem, priceLabel, salePriceLabel, pricingObject;
    
        //create individual deffere queue based on div ID
        this['pricingDefer_'+divID] = jQuery.Deferred();
        this.getPricing( removeFirst, productIDArray, divID );
        
        // Build Carousel Object
        this.carouselObject = this.parseParentData( componentTextArray, divID, layout, productIDArray, spVar );
        this.carouselObject.products = [];

        // Set Up for Target
        if ( displayTarget && !removeFirst ) {
            this.setUpTargetCarousel(productIDArray);
        }
        //once pricing object has resolved
        this['pricingDefer_'+divID].done( function(){

        // Build individual Items within Carousel Object
            for (; i<arrayLength; i++) {
                newItem = {
                //define general item data
                    divID: divID,
                    linkParam: linkParam,
                    vcVar: vcVar,
                    zone: zone,
                    manualLink: manualLink,
                    divOrigin: divOrigin,
                    showPrices: showPrices,
                    productID: productIDArray[i],
                // define url/image info
                    imageURL: productInfoArray[i][1],
                    height: height,
                    width: width,
                    imageDescription: productInfoArray[i][0],
                // format display text
                    displayText: addSpaceAfterComma( productInfoArray[i][0] ),
                // determine brand name if Empty Case
                    brand: ( productInfoArray[i][3] === 'NO BRAND NAME ASSIGNED' ) ? 'GRAINGER APPROVED VENDOR' : productInfoArray[i][3],
                // determine if on Sale
                    onSale: (ioCarousel.pricingObject[productIDArray[i]] && ioCarousel.pricingObject[productIDArray[i]].isEmployee == 'Y') ? false : ( productInfoArray[i][5] === 'Y' ) ? true : false,		
                // request and format price and label
                    price: (ioCarousel.pricingObject[productIDArray[i]] && ioCarousel.pricingObject[productIDArray[i]].isEmployee == 'Y') ? (ioCarousel.pricingObject[productIDArray[i]].sellPrice).substring(1,(ioCarousel.pricingObject[productIDArray[i]].sellPrice).length) : parseFloat(productInfoArray[i][2]).toFixed(2),

                    priceLabel: Grainger.helper.MessageHelper.getMessage('product.priceLabel'),
                // request and format price and label
                    salePrice: parseFloat(productInfoArray[i][4]).toFixed(2),
                    salePriceLabel: Grainger.helper.MessageHelper.getMessage('product.salePriceLabel'),
                // set default quanitity info
                    defaultQuant: productInfoArray[i][6]
                };

                //check pricing object for match
                if (ioCarousel.pricingObject[productIDArray[i]]){
                    newItem.salePrice = ioCarousel.pricingObject[productIDArray[i]].sellPrice;
                    newItem.salePriceLabel = ioCarousel.pricingObject[productIDArray[i]].sellPriceLabel;
                } 
                
                ioCarousel.carouselObject.products.push(newItem);
            }
        });
    },
    parseCategoryData: function(titleText, divID, data, zone, layout) {
        var sku, newItem, itemTextArray,
            categoryArray = [],
        // separate piped component text
            componentTextArray = titleText.split('|'),
        // set image sizes based on returned string (consolidate formatImageSize and buildImageSize?)
            imageSizeArray = this.formatImageSize( buildImageSize(zone) ),
            height = imageSizeArray[0],
            width = imageSizeArray[1];
            
        // Build individual Items within Category Object
        for (sku in data.catModelList) {
            itemTextArray = data.catModelList[sku].split('|');
            
            newItem = {
            //define general item data
                displayText: itemTextArray[0],
                height: height,
                sku: sku,
                imageURL: itemTextArray[2],
                url: itemTextArray[1],
                width: width
            };
            //push to array
            categoryArray.push(newItem);
        }

        // Build Category Object
        this.categoryObject = this.parseParentData( componentTextArray, divID, layout, categoryArray, null );
        //push array to new Category object
        this.categoryObject.categories = categoryArray;

        return this.categoryObject;
    },
    determineLayoutClass: function(layout) {
        switch(layout){
            case 'small':
                return 'smallerCarousel';
            case 'vertical':
                return 'verticalList';
            case 'target':
                return 'smallerCarousel targetCarousel';
        }
    },
    formatImageSize: function(sizeString){
        var sizeStringArray, height, width;

        sizeStringArray = sizeString.split('"');
        height = sizeStringArray[1];
        width = sizeStringArray[3];

        return [height, width];
    },
    formatClickParam: function(paramString) {
        var paramStringArray, spVar, vcVar;

        paramStringArray = paramString.split('&');
        spVar = paramStringArray[0].replace('cm_sp=', '');
        vcVar = paramStringArray[1].replace('cm_vc=', '');

        return [spVar, vcVar];
    },
    setUpTargetCarousel: function( itemArray ) {
        var i = 0,
            arrayLength = Math.ceil(itemArray.length / 3);

        ioCarousel.carouselObject.totalPages = arrayLength;
        ioCarousel.carouselObject.target = true;
        ioCarousel.carouselObject.pages = [];

        for (i; i<arrayLength; i++) {
            if ( i === 0 ) {
                ioCarousel.carouselObject.pages.push({isFirst: 'active'});
            } else {
                ioCarousel.carouselObject.pages.push({});
            }
        }
    },
    getPricing: function (ignoreFirst,products,divID){
        if(Grainger.helper.UserHelper.isLoggedIn()) {
            var url = contextPath +"/IntelligentOfferController?ignorefirst="+ignoreFirst+"&productArray="+products;
            
            jQuery.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    ioCarousel.pricingObject = response;
                    ioCarousel['pricingDefer_'+divID].resolve();
                },
                error: function(response) {
                    console.log("Error: No data was recieved from Intelligent Offer pricing call");
                    ioCarousel.pricingObject = {};
                    ioCarousel['pricingDefer_'+divID].resolve();
                }
            });
        } else {
            ioCarousel['pricingDefer_'+divID].resolve();
        }
    }
};

/* - - - - - Carousel Build Functions - - - - - */

//Category Carousels
function buildIOCategoryCarousel(zone,                  // Zone ID specified in the request (as entered on zone edit screen
                                symbolic,              // Target type:
                                                       //      _SP_  Specified Product ID
                                                       //      _SC_  Specified Category ID
                                                       //      _RVP_ Most Recently Viewed Product
                                                       //      _RPP_ Most Recently Purchased Product
                                                       //      _NR_  No Recommendations
                                                       //      _LCP_ Last Carted Product
                                                       //      _MPC_ Visitor's Most Popular Category
                                                       //      _SS_  Specified Search Term
                                target_id,             // Target ID recommendations are based on
                                category,              // Category of Target ID - EPR Category ID if the Target ID is a product
                                rec_attributes,        // 2 dimensional array of attributes for each recommendation
                                target_attributes,     // Array of attribute for the target
                                target_header_txt,     // Header text (as entered on recommendation plan edit screen)
                                ab_test_id,            // String containing AB Test Name, a semicolon, AB test ID for the test element requested - 'no ab test' if no AB Test
                                                       // 
                                                       // The final 2 parameters are not standard parameters passed into zone population function
                                                       // They are added via the function calls at bottom of this file
                                categoryList,          // List of categories
                                div_id,                // div id - must match div id in io_test.html
                                b_display_target,      // indicates whether target will be displayed
                                removefirstElement,
                                layoutcarousel,
                                carouselID) {

	var html = zone + "_zp: No category recommendations returned";

	jQuery('[id^="related-items-box"]').removeClass('loading');
    
    
	if (symbolic !== '_NR_'){        // will not process if 'symbolic' parameter
		var url = contextPath +"/CarouselComponentController/getCategoryInfo?categoryCodeList="+categoryList,
            catData, carouselDiv, item, prodImageArray;
            
		jQuery.ajax({
			url: url,
			dataType: 'JSON',
			success: function(response) {
                if(response !== null){
                    //create carousel
                    catData = ioCarousel.parseCategoryData( target_header_txt, div_id, response, zone, layoutcarousel );
                    carouselDiv = jQuery('#'+div_id);

                    if(carouselDiv.length > 0){
                        //print carousel
                        carouselDiv.html( Grainger.templates.ioCarousels.horizontalCarousel( catData ) );

                        //show images
                        prodImageArray = jQuery( '#'+div_id+' .productImage img' );
                        jQuery(prodImageArray).each(function(){
                            item = jQuery(this);

                            if( item.attr('data-loadimage') ){
                                item.attr('src', item.attr('data-src'));
                                item.attr('data-loadimage',false);
                            }
                        });

                        if (isNotEmpty(modal_variables)){
                            jQuery('document').ready(function() {
                                cmCreateManualImpressionTag(modal_variables.CurrentURL,cm_spVariable,'','','');
                            });
                        }
                    }
                }
			},
			error: function(response) {
				console.log("error");
			}
		});
	}
}

//Product Carousel
function buildIOCarousel(a_product_ids,         // Array of recommended product IDs
                         zone,                  // Zone ID specified in the request (as entered on zone edit screen)
                         symbolic,              // Target type:
                                                //      _SP_  Specified Product ID
                                                //      _SC_  Specified Category ID
                                                //      _RVP_ Most Recently Viewed Product
                                                //      _RPP_ Most Recently Purchased Product
                                                //      _NR_  No Recommendations
                                                //      _LCP_ Last Carted Product
                                                //      _MPC_ Visitor's Most Popular Category
                                                //      _SS_  Specified Search Term
                         target_id,             // Target ID recommendations are based on
                         category,              // Category of Target ID - EPR Category ID if the Target ID is a product
                         rec_attributes,        // 2 dimensional array of attributes for each recommendation
                         target_attributes,     // Array of attribute for the target
                         target_header_txt,     // Header text (as entered on recommendation plan edit screen)
                         ab_test_id,            // String containing AB Test Name, a semicolon, AB test ID for the test element requested - 'no ab test' if no AB Test
                                                // 
                                                // The final 2 parameters are not standard parameters passed into zone population function
                                                // They are added via the function calls at bottom of this file
                         div_id,                //      div id - must match div id in io_test.html
                         b_display_target,        //      indicates whether target will be displayed
                         removefirstElement,
                         layoutcarousel,
                         carouselID) {

    var html, carouselDiv, prodImageArray, item;

    html = zone + "_zp: No recommendations returned";

    jQuery('[id^="related-items-box"]').removeClass('loading');

    if (symbolic !== '_NR_'){  //will not process if 'symbolic' parameter is not a No Recommendations target type
        // separates data logic from template
        ioCarousel.parseProductData(target_header_txt, div_id, a_product_ids, rec_attributes, zone, layoutcarousel, b_display_target, removefirstElement);
        // define parent
        carouselDiv = jQuery('#'+div_id);
        
        ioCarousel['pricingDefer_'+div_id].done( function(){ // deferred wrapper for Pricing Ajax Call
            if(carouselDiv.length > 0 ){
                // Print Carousel
                if ( b_display_target && !removefirstElement ){
                    carouselDiv.html( Grainger.templates.ioCarousels.targetCarousel( ioCarousel.carouselObject ) );
                } else {
                    carouselDiv.html( Grainger.templates.ioCarousels.horizontalCarousel( ioCarousel.carouselObject ) );
                }
                
                // show Images
                prodImageArray = jQuery( '#'+div_id+'_Carousel .productImage img' ).splice(0,4);
                jQuery(prodImageArray).each(function(){
                    item = jQuery(this);

                    if( item.attr('data-loadimage') ){
                        item.attr('src', item.attr('data-src'));
                        item.attr('data-loadimage',false);
                    }
                });

                if(div_id === 'addtocart-items-box' || div_id === 'addtocart-items-box2') {
                    dataCheckInCarouselAddToCart();
                }

                if (isNotEmpty(modal_variables)){
                    jQuery('document').ready(function() {
                      cmCreateManualImpressionTag(modal_variables.CurrentURL,ioCarousel.carouselObject.spVar,'','','');
                    });
                }
            }
        });
    }
}

//Vertical Banners
window.buildVerticalBanner = function (a_product_ids,         // Array of recommended product IDs
                                               zone,                  // Zone ID specified in the request (as entered on zone edit screen)
                                               symbolic,              // Target type:
                                                                      //      _SP_  Specified Product ID
                                                                      //      _SC_  Specified Category ID
                                                                      //      _RVP_ Most Recently Viewed Product
                                                                      //      _RPP_ Most Recently Purchased Product
                                                                      //      _NR_  No Recommendations
                                                                      //      _LCP_ Last Carted Product
                                                                      //      _MPC_ Visitor's Most Popular Category
                                                                      //      _SS_  Specified Search Term
                                               target_id,             // Target ID recommendations are based on
                                               category,              // Category of Target ID - EPR Category ID if the Target ID is a product
                                               rec_attributes,        // 2 dimensional array of attributes for each recommendation
                                               target_attributes,     // Array of attribute for the target
                                               target_header_txt,     // Header text (as entered on recommendation plan edit screen)
                                               ab_test_id,            // String containing AB Test Name, a semicolon, AB test ID for the test element requested - 'no ab test' if no AB Test
                                                                      // 
                                                                      // The final 2 parameters are not standard parameters passed into zone population function
                                                                      // They are added via the function calls at bottom of this file
                                               div_id,                //      div id - must match div id in io_test.html
                                               b_display_target,      //      indicates whether target will be displayed
                                               removefirstElement){
    
    var html, prodImageArray, item;
  
    html = zone + "_zp: No recommendations returned";
  
    // Hide REC zone on IDP if no products there
    if(rec_attributes.length < 1 && div_id == 'related-items-box') {
        jQuery("[id^=related-items-box]").hide().removeClass('loading');
    }

    if (symbolic !== '_NR_'){
        ioCarousel.parseProductData(target_header_txt, div_id, a_product_ids, rec_attributes, zone, 'vertical', b_display_target, removefirstElement);
    
        ioCarousel['pricingDefer_'+div_id].done( function(){ // deferred Wrapper for pricing Ajax Call
            
            jQuery('#'+div_id).html( Grainger.templates.ioCarousels.verticalList(ioCarousel.carouselObject) );
            prodImageArray = jQuery( '#'+div_id+' .productImage img' );

            jQuery(prodImageArray).each(function(){
                item = jQuery(this);

                if( item.attr('data-loadimage') ){
                    item.attr('src', item.attr('data-src'));
                    item.attr('data-loadimage',false);
                }
            });

            jQuery('document').ready(function() {
                cmCreateManualImpressionTag(page_variables.CurrentURL,ioCarousel.carouselObject.spVar,'','','');
            });
        });
    }
};

/******************************** END NEWER BUILD FUCTION **************************************/

//IDP Right rail
window.IDPRRZ13_zp = function (a,b,c,d,e,f,g,h,i){
    var div = getDivFromVerticalZone();
    
    if(showCarouselCartDiv()){
        divprefix = 'addtocart-items-box-IDPRRZ13';
        buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,'small','');
    }else{
        if(jQuery(window).width() < 1383) {
            buildIOCarousel(a,b,c,d,e,f,g,h,i,'alternateRightRail',true,true,'large','');
            jQuery('#alternateRightRail').removeClass('hide');
        } else {
            buildVerticalBanner(a,b,c,d,e,f,g,h,i,div.attr('id'),true,true);
            jQuery('#' + div.attr('id')).show();
        }
    }    
};

//REC-60
window.STATIC1_zp = function (a,b,c,d,e,f,g,h,i)  {
  var divprefix = 'carousel-box-IdpBottom';
  
  if(showCarouselCartDiv()){
      divprefix = 'addtocart-items-box-STATIC1';
  }
  
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,'small',null);
};

//Static page Right rail
window.STATIC2_zp = function (a,b,c,d,e,f,g,h,i){
    var div = getDivFromVerticalZone(),
        size = 'large';

    if(showCarouselCartDiv()){
        divprefix = 'addtocart-items-box-STATIC2';
        size = 'small';
        buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,size,'');
    }else{
        if(jQuery(window).width() < 1383) {
            buildIOCarousel(a,b,c,d,e,f,g,h,i,'alternateRightRail',true,true,'large','');
            jQuery('#alternateRightRail').removeClass('hide');
        } else {
            buildVerticalBanner(a,b,c,d,e,f,g,h,i,div.attr('id'),true,true);
            jQuery('#' + div.attr('id')).show();
        }
    }
};

//PIDP Right rail
window.IDPRRZ11_zp = function (a,b,c,d,e,f,g,h,i){
    var div = getDivFromVerticalZone(),
        size = 'large';
  
    if(showCarouselCartDiv()){
        divprefix = 'addtocart-items-box-IDPRRZ11';
        size = 'small';
        buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,size,'');
    }else{
        if(jQuery(window).width() < 1383) {
            buildIOCarousel(a,b,c,d,e,f,g,h,i,'alternateRightRail',true,true,'large','');
            jQuery('#alternateRightRail').removeClass('hide');
        } else {
            buildVerticalBanner(a,b,c,d,e,f,g,h,i,div.attr('id'),true,true);
            jQuery('#' + div.attr('id')).show();
        }
    }
};

//Category Carousel
window.CAT23_zp = function (a,b,c,d,e,f,g,h,i,j){
    var divprefix = 'carousel-box-';
	buildIOCategoryCarousel(b,c,d,e,f,g,h,i,j,divprefix + 'CAT23',true,true,'small','');
};

//Homepage
window.HPBTZ11_zp = function (a,b,c,d,e,f,g,h,i){
  var divprefix = 'carousel-box-';

  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
  }
    buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'HPBTZ11',true,true,'small','');
};

//Homepage recommendation zone
window.A2CSTKZ_zp = function (a,b,c,d,e,f,g,h,i){
  var divprefix = 'carousel-box-';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
  }
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'A2CSTKZ',true,true,'small','');
};

//Homepage recommendation zone
window.HPOHZ3_zp = function (a,b,c,d,e,f,g,h,i){
  var divprefix = 'carousel-box-';

  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
  }
    buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'HPOHZ3',true,true,'small','');
};

//Add to cart
window.A2CBTZ4_zp = function (a,b,c,d,e,f,g,h,i)  {
  var carouselCart = jQuery('#carouseladdtocart');

  if(carouselCart.length > 0){
    carouselCart.removeClass('hide');
    buildIOCarousel(a,b,c,d,e,f,g,h,i,'addtocart-items-box',true,true,'small','');
  }
};

//Add to cart second carousel
window.STKZ_zp = function (a,b,c,d,e,f,g,h,i)  {
  var carouselCart = jQuery('#carouseladdtocart2');

  if(carouselCart.length > 0){
    carouselCart.removeClass('hide');
    buildIOCarousel(a,b,c,d,e,f,g,h,i,'addtocart-items-box2',true,true,'small','');
  }
};

//Empty Cart
window.EMPTYCRT_zp = function(a,b,c,d,e,f,g,h,i) {
  var divprefix = 'carousel-box-CartBottom',
      size = 'large';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-EMPTYCRT';
    size = 'small';
  }
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,size,'carousel_io_cart');
};

//Cart
window.CRTBTZ5_zp = function (a,b,c,d,e,f,g,h,i)  {
  var divprefix = 'carousel-box-CartBottom',
      size = 'large';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-CRTBTZ5';
    size = 'small';
  }
  
  jQuery('#carouselCart').removeClass('hide');
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,size,'carousel_io_cart');
};

//IDP Bottom rail
window.IDPBTZ12_zp = function (a,b,c,d,e,f,g,h,i)  {
  var divprefix = 'carousel-box-IdpBottom',
      size = 'large';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-IDPBTZ12';
    size = 'small';
  }
  
  if(a !== null && a.length > 0){
    jQuery(".intelligentOfferPlaceholder").each(function(node, index, arr){
      jQuery(node).removeClass('hide');
    });
  } else {
    //No response, hide the component layout
    jQuery('#horizontaltabplaceholder').addClass('hide');
  }
    
    buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,size,'carousel_id_io');
};

//Parent IDP Bottom rail
window.IDPBBZ11_zp = function (a,b,c,d,e,f,g,h,i)  {
  var divprefix = 'carousel-box-IdpBottom',
      size = 'large';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-IDPBBZ11';
    size = 'small';
  }
    buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,size,'carousel_id_io');
};

//Account
window.ACTBTZ14_zp = function (a,b,c,d,e,f,g,h,i)  {
  var carouselCart = jQuery('#carouseladdtocart'),
      divprefix = 'carousel-box-';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
  }
  
  if(jQuery('#carouselAccount') !== null){
    jQuery('#carouselAccount').removeClass('hide');
  }
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'ACTBTZ14',true,true,'small','');
};

//Account
window.ACTOHZ14_zp = function (a,b,c,d,e,f,g,h,i)  {
  var carouselCart = jQuery('#carouseladdtocart'),
      divprefix = 'carousel-box-';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
  }
  
  if(jQuery('#carouselAccount') !== null){
    jQuery('#carouselAccount').removeClass('hide');
  }
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'ACTOHZ14',true,true,'small','');
};

//No Result
window.SRTZ1_zp = function (a,b,c,d,e,f,g,h,i)  {
  var divprefix = 'carousel-box-';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
  }
  
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'SRTZ1',true,true,'small','');
};

//No Result
window.SROH15_zp = function (a,b,c,d,e,f,g,h,i)  {
  var divprefix = 'carousel-box-';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
  }
  
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'SROH15',true,true,'small','');
};

//Invited user Account registration Confirmation
window.ACCACT1_zp = function (a,b,c,d,e,f,g,h,i)  {
  var carouselCart = jQuery('#carouseladdtocart'),
      divprefix = 'carousel-box-',
      size = 'large';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
    size = 'small';
  }
  
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'ACCACT1',true,true,size,'');
};

//Tiered Search
window.HTSEARCH_zp = function (a,b,c,d,e,f,g,h,i)  {
  var carouselCart = jQuery('#carouseladdtocart'),
      divprefix = 'carousel-box-',
      size = 'large';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
    size = 'small';
  }
  
  buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix + 'HTSEARCH',true,true,size,'');
};
//IIB-268
window.SRTZ3_zp = function(a, b, c, d, e, f, g, h, i) {
  var divprefix = 'carousel-box-';
  var type = 'target';
  
  if(showCarouselCartDiv()){
    divprefix = 'addtocart-items-box-';
    type = 'target';
  }

  buildIOCarousel(a, b, c, d, e, f, g, h, i, divprefix + 'SRTZ3', true, false, type, '');
};
//IIB-3290
window.PROH22_zp = function(a, b, c, d, e, f, g, h, i) {
    var divprefix = 'carousel-box-';
    var size = 'large';
    
    buildIOCarousel(a, b, c, d, e, f, g, h, i, divprefix +'PROH22'+d, true, true, size, '');
};
//Static3 page Right rail
window.STATIC3_zp = function (a,b,c,d,e,f,g,h,i){
    var divprefix = 'related-items-box' + d,
        div = jQuery('#' + divprefix),
        size = 'large';

    if( showCarouselCartDiv() ){
        divprefix = 'addtocart-items-box-STATIC3';
        size = 'small';
        buildIOCarousel(a,b,c,d,e,f,g,h,i,divprefix,true,true,size,'');
    } else {
        if ( jQuery(window).width() < 1383 ) {
            buildIOCarousel(a,b,c,d,e,f,g,h,i,'alternateRightRail',true,true,'small','');
            jQuery('#alternateRightRail').removeClass('hide');
        } else {
            buildVerticalBanner(a,b,c,d,e,f,g,h,i,divprefix,true,true);
            jQuery('#' + div.attr('id')).show();
        }
    }
};

function defaultQuantity(quantity){

    re = /^\d*$/;
    
    if (typeof quantity == 'undefined' || quantity === null || quantity.toString().replace(/[\D]/g,"").length === 0 || !quantity.toString().match(re)){
        quantity = 1;
    }
    
    return quantity;
}

function updateReviewContainers(reviewData, div_id) {
  if((div_id != 'addtocart-items-box'||div_id != 'addtocart-items-box2') && div_id != 'carousel-box-CartBottom'){
    for ( var sku in reviewData) {
      var reviewsContainer = jQuery('#' + sku + "_reviews_container");
    
      if(reviewsContainer !== null) {
        reviewsContainer.html(reviewData[sku].HTML);
      }
    }
      
    loadDropTabMenu(div_id);
  }
}

function showCarouselCartDiv(){
  var carouselCart = jQuery('#carouseladdtocart');
  
  if(carouselCart.length > 0){
    carouselCart.removeClass('hide');
    return true;
  }

  return false;
}
function getDivFromVerticalZone(){
  var productId = jQuery('#productId').val();
  //Div id can not be created with |. Replace them with ""
  var prodId = productId.replace(/[|]/g, "");
  var categoryId = jQuery('#catId').val(),
  divprefix = 'related-items-box' + categoryId + prodId;
  var div = jQuery(document.getElementById(divprefix));
  return div;
}

function addSpaceAfterComma(productDesc){
  var result = productDesc.replace(/(\S),(\S)/g, function($0, $1, $2){
    if ( isNaN($1) || isNaN($2) ){
      return $1+", "+$2;
    }

    return $1+","+$2;
  });

	return result;
}