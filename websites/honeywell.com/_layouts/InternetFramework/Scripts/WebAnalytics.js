var sGAAccountId = "UA-12340078-22";
var _hw_baseURLGA = location.href;
var sGARollupAccountId = "UA-25496412-1";
if(_hw_baseURLGA.indexOf("://honeywell.com") != -1)
{
      sGAAccountId = "UA-12340078-1";
}


var sDomainName = ".honeywell.com"; // to track sub domains too
//var _gaq = _gaq || [];

// Click event tracking
//function recordLinkEvent
//param1: The current link reference
//param2: The category of the event 
//param3: The Action of the event
function recordLinkEvent(link, category, action, event) {
    try {

        var _sLinkText;

        //Get the link text that was clicked
        _sLinkText = jQuery(link).text();

        //Get only page names
        if (link.href != '' && !(link.href.match(/^mailto\:/i) || link.href.match(/^javascript\:/i) || link.href.match(/^file\:/i) || link.href.match(/^ftp\:/i))) {

            var _sPage = link.href.substring(link.href.lastIndexOf('/') + 1).toLowerCase();

            if (_sPage.indexOf('#') != -1) {
                _sPage = _sPage.substring(0, _sPage.indexOf('#')).toLowerCase();
            }

            if (_sPage.indexOf('?') != -1) {
                _sPage = _sPage.substring(0, _sPage.indexOf('?')).toLowerCase();
            }

            if (_sPage != '') {
                _sLinkText = _sLinkText + " " + "(" + _sPage + ")";
            }

        }
        //Call the GA tracking function.
        //Pass the Category and Action from the calling function. 
        //Currently passing the Link Text with Page Name as the Label for the Event Tracking function.
		
		//Start Change from GA code to UA code by H118661, E863647
       // _gaq.push(['_trackEvent', category, action, _sLinkText]);
		
		
		//Analytics code
		ga('send', 'event', {
		eventCategory: category,
		eventAction: action,
		eventLabel: _sLinkText  });
		
		//End Change

        // Check for the new window option
        if ((link.target == '') || (link.target == null)) {

            if ((link.href != '') && !((jQuery(link).hasClass('honeywellvideo')) || (jQuery(link).hasClass('hw-footerbox'))) && !(link.href.match(/^mailto\:/i) || link.href.match(/^javascript\:/i) || link.href.match(/^file\:/i) || link.href.match(/^ftp\:/i))) {

                var _sPath = location.href.toLowerCase();
                //Get the current URL path upto #
                if (location.href.indexOf('#') != -1) {
                    _sPath = location.href.substring(0, location.href.indexOf('#')).toLowerCase();
                }

                //Get the current link path upto #
                var _sLink = link.href.toLowerCase();
                if (link.href.indexOf('#') != -1) {
                    _sLink = link.href.substring(0, link.href.indexOf('#')).toLowerCase();
                }

                if (typeof (_sPath) != 'undefined') {
                    if (_sPath != _sLink && _sLink != '') {
                        //Cancel the default behaviour. This is because the default browser action will not wait for setTimeout to complete
                        event.preventDefault();
                        setTimeout(function() {
                            document.location = link.href;
                        }, 100);

                    }
                }

            }
        }

    } catch (err) { }
}

jQuery(document).ready(function() {

    //jQuery('a:not('.hw-consumer-top')').click(function(event) {
       jQuery('a').click(function(event) {
        //call the recordLinkEvent function to track the link click
        //param1: Pass the current link reference
        //param2: Pass the category of the event as Link-Click
        //param3: Pass the current page URL from where the link was clicked.
        //param4: Pass the event

        
      
        
        if(!$(this).hasClass('hw-consumer-top')&&!$(this).hasClass('pressreltitle')&&!$(this).hasClass('hwSocialMedia'))
        {
               
        recordLinkEvent(this, 'Link-Click', document.URL, event);
        
        }

    });

    //The A-Z Index page has links loaded through AJAX. Use the jQuery live function only for these links.
    jQuery('.alphaProductDiv a').live("click", function(event) {
        //call the recordLinkEvent function to track the link click
        //param1: Pass the current link reference
        //param2: Pass the category of the event as Link-Click
        //param3: Pass the current page URL from where the link was clicked.
        //param4: Pass the event
        //recordLinkEvent(this, 'Link-Click', document.URL, event);
    });
    /*Added as Part of Article Page Tracking*/
    jQuery('.hwSocialMedia').click(function(event)
    {
        var title = document.title;
        TrackEvent("Hon-SocialMedia Tracking", "click", $(this).attr("title"), 0);

    
     });


});

function recordClickEvent(category, action, value) {
  try
 {
		//Start Change from GA code to UA code by H118661, E863647
      //_gaq.push(['_trackEvent', category, action, value]);  

		//Analytics code
		ga('send', 'event', {
		eventCategory: category,
		eventAction: action,
		eventValue: value });
    
		//End Change
	
  }catch(err){}
}

// Set the GA Custom Variable at Page Level
function setGACustomVariable(piSlotIndex, psGAVarName, psGAVarValue, piGAVarScope){
  try
 {
 
   // Validate the values
   if(piSlotIndex != "" && psGAVarName != "" && psGAVarValue != "" && piGAVarScope != "")
   {
      // Traditional Code
      //var pageTracker=_gat._getTracker(sGAAccountId);
      //pageTracker._setDomainName(sDomainName);
      //pageTracker._setCustomVar(piSlotIndex, psGAVarName, psGAVarValue, piGAVarScope); 
   
      // New Asynchronous Code
      //var _gaq = _gaq || [];  
      //_gaq.push(['_setAccount', sGAAccountId]);
      //_gaq.push(['_setDomainName', sDomainName]);
      
		//Start Change from GA code to UA code by H118661, E863647
	  //_gaq.push(['_setCustomVar', piSlotIndex, psGAVarName, psGAVarValue, piGAVarScope]);   
		
		//Analytics code
		//ga('set', 'dimension1', psGAVarValue);
		
		//End Change
		
   }
  }catch(err){}
}

// Get metatag value
function getMetaTagContents(psMetaTagName){

   var arrMetaTags = document.getElementsByTagName('meta');

   for(var i in arrMetaTags){
   if(arrMetaTags[i].name == psMetaTagName){
     return arrMetaTags[i].content;
   }
 }
}

// Set Meta Tag Content to GA Variables
function initGATracking(){

     var pageTracker;
     var sGAVarName;
     var iGAVarScope;
     var sMetaTagName;
     var sMetaTagValue;
     var iSlot;
     
	 
	 
	 var arrGAVariables = [
			{'GAVariable':'Segment-Type', 'MetaTagName':'Segment-Type', 'Scope':3, 'Slot':3},
			{'GAVariable':'Target-Audience', 'MetaTagName':'Target-Audience', 'Scope':1, 'Slot':1}, 
			{'GAVariable':'Business-Technology', 'MetaTagName':'Business-Technology', 'Scope':3, 'Slot':4}
     		];
     try
        {

	  // Traditional Code
          //pageTracker=_gat._getTracker(sGAAccountId);
          //pageTracker._setDomainName(sDomainName);
          //pageTracker._trackPageview();
    
    	  // GA Asynchronous Code
   	  //var _gaq = _gaq || [];  
   	 // _gaq.push(['_setAccount', sGAAccountId]);
	//	_gaq.push(['_setDomainName', sDomainName]);
	 

	 //Analytics code
	//ga('create', sGAAccountId { cookieDomain: sDomainName} );
   	 
    
          for(var i=0;i<arrGAVariables.length;i++)
          {
		sMetaTagName = arrGAVariables[i].MetaTagName;
		sGAVarName = arrGAVariables[i].GAVariable;
		iGAVarScope = arrGAVariables[i].Scope;
		iSlot = arrGAVariables[i].Slot;

		sMetaTagValue =  getMetaTagContents(sMetaTagName);

		if(sMetaTagValue != "" && sMetaTagValue != null && sMetaTagValue != 'None')
   		{
		   //pageTracker._setCustomVar(iSlot, sGAVarName, sMetaTagValue, iGAVarScope); 
		   
		   //Start Change from GA code to UA code by H118661, E863647
		   
		   //_gaq.push(['_setCustomVar', iSlot, sGAVarName, sMetaTagValue, iGAVarScope]);
		   
		   //Analytics code
		   
		   //var dimension1Value = "Segment-Type";
		   //var dimension2Value = "Target-Audience";
		   //var dimension3Value = "Business-Technology";
		   //ga('set', 'dimension1', dimension1Value);
		   //ga('set', 'dimension2', dimension2Value);
		   //ga('set', 'dimension3', dimension3Value);
		   
		   //End Change
		   
   		}

          } 
		
			//Start Change from GA code to UA code by H118661, E863647
		
		/* _gaq.push(['_trackPageview']); // Called after custom variable as per Google Recommendation
		_gaq.push(['gT._setAccount', sGARollupAccountId]);	
		_gaq.push(['gT._setDomainName', sDomainName]);
		_gaq.push(['gT._trackPageview']); */
	 
	 
		//Analytics code
		
		//ga('send', 'pageview');
		ga('create', sGARollupAccountId, {'name': 'gT',
		cookieDomain: sDomainName});
		ga('http://honeywell.com/_layouts/InternetFramework/Scripts/gT.send', 'pageview');

			//End Change

        }catch(err){}

} // EOF

// Call the function to set the custom variables
initGATracking();

function TrackEvent(category, verb_actions, label, bln_noninteraction) 
{
	
    try 
    {
        
         var pageTracker = ga('create', sGAAccountId, 'auto'); 
        
		//Start Change from GA code to UA code by H118661, E863647
		
		//_gaq.push(['_trackEvent', category, verb_actions, label, bln_noninteraction]);
		
		//Analytics code
		ga('set', 'nonInteraction', bln_noninteraction);
		ga('send', 'event', {
		eventCategory: category,
		eventAction: verb_actions,
		eventLabel: label });
		
		//End Change
    } 
    catch (e) 
    {
        alert(e);
    }
}
