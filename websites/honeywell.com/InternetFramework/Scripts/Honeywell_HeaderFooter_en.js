/*cookie banner*/
function loadjscssfile(filename, filetype)
{
 if (filetype=="js"){ //if filename is a external JavaScript file
  var fileref=document.createElement('script');
  fileref.setAttribute("type","text/javascript");
  fileref.setAttribute("src", filename);
 }
 else if (filetype=="css"){ //if filename is an external CSS file
  var fileref=document.createElement("link");
  fileref.setAttribute("rel", "stylesheet");
  fileref.setAttribute("type", "text/css");
  fileref.setAttribute("href", filename);
 }
 if (typeof fileref!="undefined")
  document.getElementsByTagName("head")[0].appendChild(fileref);
}

loadjscssfile("../../../eubanner.honeywell.com/javascript/HON.EuCookieCompliance.min.js"/*tpa=http://eubanner.honeywell.com/javascript/HON.EuCookieCompliance.min.js*/, "js"); 
loadjscssfile("../../../eubanner.honeywell.com/css/Banner.css"/*tpa=http://eubanner.honeywell.com/css/Banner.css*/, "css");

document.write('<div id="HoneywellEUCookie-Banner"><div><span id="HoneywellEUCookie-BannerText"></span><a id="HoneywellEUCookie-PrivacyLink">&nbsp;<span id="HoneywellEUCookie-PrivacyText"></span></a></div><div id="HoneywellEUCookie-ConsentButtonDiv"><button type="submit" id="HoneywellEUCookie-CookieDismiss"></button></div></div>');

/*cookie banner*/
/*****************************************************************************/
/*Honeywell Global Variable Declaration */
/*****************************************************************************/

//Define Base URL
var _hw_baseURL = window.location.protocol+"//"+window.location.hostname + ':' + window.location.port;

if(document.getElementsByName('_hw_Base_SiteUrl').length != 0)
{
  _hw_baseURL=document.getElementsByName('_hw_Base_SiteUrl')[0].content;      
}


//Define Global variables

var _hw_sSearch_Search ="SEARCH";

var _hw_sSearch_PleaseEnterSearchText ="Please enter search text";

var _hw_sSiteMap_Div_Sitemap ="SITEMAP";

var _hw_sFooter_Div_YearHoneywellInternationalInc ="2011 Honeywell International Inc.";

var _hw_sSiteMap="SiteMap";

//
//Use the First Bool to enter into new window
//Use the Second Bool to use the image for Worldwide
//

var _hw_header_right={"CONTACT HONEYWELL&nbsp;":[_hw_baseURL +"/Pages/contact-us.aspx",false,false],
"&nbsp;CORPORATE CITIZENSHIP&nbsp;":["http://citizenship.honeywell.com/",false,false],
"&nbsp;WORLDWIDE":[_hw_baseURL +"/worldwide/Pages/worldwide.aspx",false,true]};

//
//Use the First Bool to enter into new window
//Use the Second Bool to use Modal window for Feedback
//

var _hw_uFooter={
"Terms & Conditions":[_hw_baseURL +"/Pages/TermsConditions.aspx",false,false],
"Privacy Statement":[_hw_baseURL +"/privacy/pages/en.aspx",false,false],
"Employee Access":["https://directaccess.ps.honeywell.com/cwa",true,false],
"Contact Honeywell":[_hw_baseURL +"/Pages/contact-us.aspx",false,false],
"Careers":["http://www.careersathoneywell.com/",true,false]
};
	// Define 2nd level Navigation

    	var _hw_aMenuItem_PSD = {				
				"Consumer & Home": [_hw_baseURL +"/Products-Services/Pages/consumer-home.aspx","_hw_MnuCol1"],
				"Aerospace & Defense":[_hw_baseURL +"/Products-Services/Pages/aerospace-defense.aspx","_hw_MnuCol1"],				 
				"Safety & Security":[_hw_baseURL +"/Products-Services/Pages/safety-security.aspx","_hw_MnuCol1"],
				"Buildings, Construction <br/>and Maintenance":[_hw_baseURL +"/Products-Services/Pages/buildings-construction-maintenance.aspx","_hw_MnuCol1"],	
				"Scanning & Mobile Productivity":[_hw_baseURL +"/Products-Services/Pages/scanning-mobile-productivity.aspx","_hw_MnuCol1"],
				"Industrial Process Control":[_hw_baseURL +"/Products-Services/Pages/industrial-process-control.aspx","_hw_MnuCol1"],
				"Efficiency, Energy & Utilities":[_hw_baseURL +"/Products-Services/Pages/efficiency-energy-utilities.aspx","_hw_MnuCol1"],
				"Automotive & Transportation":[_hw_baseURL +"/Products-Services/Pages/automotive-transportation.aspx","_hw_MnuCol1"],								 
				"Oil & Gas, Refining,<br/> Petrochemicals and Biofuels":[_hw_baseURL +"/Products-Services/Pages/oil-gas-refining-petrochemicals.aspx","_hw_MnuCol2"],
				"Healthcare & Medical":[_hw_baseURL +"/Products-Services/Pages/healthcare-medical.aspx","_hw_MnuCol2"],
				"Chemicals, Specialty Materials <br/>& Fertilizers": [_hw_baseURL +"/Products-Services/Pages/chemicals.aspx","_hw_MnuCol2"], 				
                "Manufacturing":[_hw_baseURL +"/Products-Services/Pages/manufacturing.aspx","_hw_MnuCol2"], 
				"Fire Protection & First Responder":[_hw_baseURL +"/Products-Services/Pages/fire-protection-first-responder.aspx","_hw_MnuCol2"],				
				"A-Z Index":[_hw_baseURL +"/Products-Services/Pages/alpha.aspx","_hw_MnuCol2"]  
			   	};
	
	//Array for Image
	   
	 var _hw_aMenuItem_ST_Image = {
				"Energy":["../../_layouts/InternetFramework/Images/dropdown_image1.jpg"/*tpa=http://honeywell.com/_layouts/InternetFramework/Images/dropdown_image1.jpg*/, _hw_baseURL + "/Solutions-Technologies/Pages/energy.aspx"], 
				"Safety":["../../_layouts/InternetFramework/Images/dropdown_image2.jpg"/*tpa=http://honeywell.com/_layouts/InternetFramework/Images/dropdown_image2.jpg*/, _hw_baseURL + "/Solutions-Technologies/Pages/safety.aspx"],
				"Security":["../../_layouts/InternetFramework/Images/dropdown_image3.jpg"/*tpa=http://honeywell.com/_layouts/InternetFramework/Images/dropdown_image3.jpg*/ ,_hw_baseURL + "/Solutions-Technologies/Pages/security.aspx"]  
			   	};

	var _hw_aMenuItem_AboutUs = {
				"Our Company": [_hw_baseURL + "/About/Pages/our-company.aspx",0], 
				"Global Presence": [_hw_baseURL + "/About/Pages/global-presence.aspx",0],
				"Company Recognition": [_hw_baseURL + "/About/CompanyRecognition/Pages/company-recognition.aspx",0],
				"Winners Work Here": [_hw_baseURL + "/About/WinnersWorkHere/Pages/winners-work-here.aspx",0],
				"Our History": [_hw_baseURL +"/About/Pages/our-history.aspx",0], 
				"Honeywell Leadership": [_hw_baseURL +"/About/HoneywellLeadership/Pages/honeywell-leadership.aspx",0],
				"CEO of the Year": [_hw_baseURL +"/About/Pages/ceooftheyear.aspx",0],
				"Code of Business Conduct": [_hw_baseURL +"/About/Pages/code-of-business-conduct.aspx",0],
				"Careers": ["http://www.careersathoneywell.com/",1],
				"Contact Honeywell":[_hw_baseURL +"/Pages/contact-us.aspx",0]
				 };

	var _hw_aMenuItem_Investors = {
					"Investor Relations": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-irhome",0],
				"Corporate Governance": ["http://phx.corporate-ir.net/phoenix.zhtml?c=94774&p=irol-govboard",0], 
				"Event Calendar": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-calendar",0], 
				"Financial Releases": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-news",0],
				"Investor Presentations": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-presentations",0], 
				"SEC Filings & Reports": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-sec",0],
				"Financial Information": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-fundTrading",0],				
				"Email Alerts": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-alerts",0],
				"Information Request": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-investorkit",0],
				"Contact Information": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-contact",0] 
			   	};

	var _hw_aMenuItem_Newsroom = {
       			"Newsroom": ["http://honeywellnow.com/",0], 
				"Press Releases": [_hw_baseURL +"/News/Pages/press-releases.aspx",0], 
				"Media Contacts": [_hw_baseURL +"/News/Pages/media-contacts.aspx",0],
				"CEO Speeches": [_hw_baseURL +"/News/Pages/ceo-speeches.aspx",0], 
				"Logos & Images": [_hw_baseURL +"/News/Pages/graphics-library.aspx",0]
				
			   	};
	
     var _hw_aMenuTree_Complete = { 
				"PRODUCTS & SERVICES": [_hw_aMenuItem_PSD, "twocolumn","Menu Group 1"], 
				"SOLUTIONS & TECHNOLOGIES": [_hw_aMenuItem_ST_Image, "image","Menu Group 2"], 
				"ABOUT US": [_hw_aMenuItem_AboutUs, "text","Menu Group 3"], 
				"INVESTORS": [_hw_aMenuItem_Investors, "text","Menu Group 4"], 
				"NEWS": [_hw_aMenuItem_Newsroom, "text","Menu Group 5"]
			 	};

	//Defining site map items
	
	//Defining site map items
 var _hw_oSiteMapItem_PSD = {
				"Consumer & Home": [_hw_baseURL +"/Products-Services/Pages/consumer-home.aspx",0],
				"Aerospace & Defense":[_hw_baseURL +"/Products-Services/Pages/aerospace-defense.aspx",0],				 
				"Safety & Security":[_hw_baseURL +"/Products-Services/Pages/safety-security.aspx",0],
				"Buildings, Construction <br>and Maintenance":[_hw_baseURL +"/Products-Services/Pages/buildings-construction-maintenance.aspx",0],
				"Scanning & Mobile Productivity":[_hw_baseURL +"/Products-Services/Pages/scanning-mobile-productivity.aspx",0],
				"Industrial Process Control":[_hw_baseURL +"/Products-Services/Pages/industrial-process-control.aspx",0],
				"Efficiency, Energy & Utilities":[_hw_baseURL +"/Products-Services/Pages/efficiency-energy-utilities.aspx",0],
				"Automotive & Transportation":[_hw_baseURL +"/Products-Services/Pages/automotive-transportation.aspx",0],
				"Oil & Gas, Refining,<br/> Petrochemicals and Biofuels":[_hw_baseURL +"/Products-Services/Pages/oil-gas-refining-petrochemicals.aspx",0],
				"Healthcare & Medical":[_hw_baseURL +"/Products-Services/Pages/healthcare-medical.aspx",0],
				"Chemicals, Specialty Materials <br>& Fertilizers": [_hw_baseURL +"/Products-Services/Pages/chemicals.aspx",0],
				"Manufacturing":[_hw_baseURL +"/Products-Services/Pages/manufacturing.aspx",0], 
				"Fire Protection & First Responder":[_hw_baseURL +"/Products-Services/Pages/fire-protection-first-responder.aspx",0],
				"A-Z Index":[_hw_baseURL +"/Products-Services/Pages/alpha.aspx",0]  
			   };
	
	 var _hw_oSiteMapItem_ST = {
				"Energy":[_hw_baseURL +"/Solutions-Technologies/Pages/energy.aspx",0], 
				"Safety":[_hw_baseURL +"/Solutions-Technologies/Pages/safety.aspx",0], 
				"Security":[_hw_baseURL +"/Solutions-Technologies/Pages/security.aspx",0] 
			
			   };
		
	var _hw_oSiteMapItem_AboutUs= {
				"Our Company": [_hw_baseURL + "/About/Pages/our-company.aspx",0], 
				"Global Presence": [_hw_baseURL + "/About/Pages/global-presence.aspx",0],
				"Company Recognition": [_hw_baseURL + "/About/CompanyRecognition/Pages/company-recognition.aspx",0],
				"Winners Work Here": [_hw_baseURL + "/About/WinnersWorkHere/Pages/winners-work-here.aspx",0],
				"Our History": [_hw_baseURL +"/About/Pages/our-history.aspx",0], 
				"Honeywell Leadership": [_hw_baseURL +"/About/HoneywellLeadership/Pages/honeywell-leadership.aspx",0],
				"CEO of the Year": [_hw_baseURL +"/About/Pages/ceooftheyear.aspx",0],
				"Code of Business Conduct": [_hw_baseURL +"/About/Pages/code-of-business-conduct.aspx",0],
				"Careers": ["http://www.careersathoneywell.com/",1],
				"Contact Honeywell": [_hw_baseURL +"/Pages/contact-us.aspx",0]
				 };
				 
				 
		var _hw_oSiteMapItem_CSR= {
				"Hometown Solutions": ["http://citizenship.honeywell.com/hometown-solutions/",0],
		        "Science & Math Education": ["http://citizenship.honeywell.com/hometown-solutions/science-math-education-programs/",0],
		        "Housing & Shelter": ["http://citizenship.honeywell.com/hometown-solutions/housing-shelter-programs/",0],
		        "Family Safety & Security": ["http://citizenship.honeywell.com/hometown-solutions/family-safety-security-programs/",0], 
		        "Humanitarian Relief": ["http://citizenship.honeywell.com/hometown-solutions/humanitarian-relief-programs/",0],
		        "Habitat & Conservation": ["http://citizenship.honeywell.com/hometown-solutions/habitat-conservation-programs/", 0],
		        "Hometown Heroes": ["http://citizenship.honeywell.com/hometown-solutions/hometown-heroes/", 0],
		        "Contact Us": ["http://citizenship.honeywell.com/contact-us/",0],
		        "Guidelines and Policies": ["http://citizenship.honeywell.com/guidelines-and-policies/",0],
				"Sustainable Opportunity": ["http://citizenship.honeywell.com/environmental-commitment/",0],
				"Environmental and Safety Goals":["http://citizenship.honeywell.com/environmental-commitment/environmental-and-safety-goals/",0],	
				"Health, Safety, and Environment Management System":["http://citizenship.honeywell.com/environmental-commitment/health-safety-and-environment-management-system/",0],	
				"Affiliations, Memberships, Partnerships and Reports": ["http://citizenship.honeywell.com/environmental-commitment/affiliations-memberships-partnerships-and-reports/",0],
				"Awards, Certifications, Affiliations": ["http://citizenship.honeywell.com/environmental-commitment/awards-certifications-affiliations/",0],
				"Commitment to Diversity": ["http://citizenship.honeywell.com/our-commitment-to-diversity/",0] 				
				 };			 
	var _hw_oSiteMapItem_Investors= {
					"Investor Relations": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-irhome",0], 
				"Corporate Governance": ["http://phx.corporate-ir.net/phoenix.zhtml?c=94774&p=irol-govboard",0], 
				"Event Calendar": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-calendar",0], 
				"Financial Releases": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-news",0],
				"Investor Presentations": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-presentations",0], 
				"SEC Filings & Reports": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-sec",0],
				"Financial Information": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-fundTrading",0],				
				"Email Alerts": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-alerts",0],
				"Information Request": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-investorkit",0],
				"Contact Information": ["http://investor.honeywell.com/phoenix.zhtml?c=94774&p=irol-contact",0] 
			   };
	var _hw_oSiteMapItem_Newsroom= {
       			"Newsroom": ["http://honeywellnow.com/",0], 
				"Press Releases": [_hw_baseURL +"/News/Pages/press-releases.aspx",0], 
				"Media Contacts": [_hw_baseURL +"/News/Pages/media-contacts.aspx",0],
				"CEO Speeches": [_hw_baseURL +"/News/Pages/ceo-speeches.aspx",0], 
				"Logos & Images": [_hw_baseURL +"/News/Pages/graphics-library.aspx",0]
				
			   };
			   
	var _hw_oSiteMapItem_Careers= {
				"Job Search": ["http://www.careersathoneywell.com/en/JobSearch",0], 
				"Working at Honeywell": ["http://www.careersathoneywell.com/en/WorkingatHoneywell",0], 
				"Career Paths": ["http://www.careersathoneywell.com/en/CareerPaths",0], 
				"Benefits": ["http://www.careersathoneywell.com/en/Benefits",0], 
				"Locations": ["http://www.careersathoneywell.com/en/Locations.aspx",0], 
				"Graduates &amp; Students": ["http://www.careersathoneywell.com/en/GraduatesandStudents",0]
				
			   };
	var _hw_oSiteMapItem_Worldwide= {
				 "Worldwide Directory" :[_hw_baseURL +"/worldwide/Pages/worldwide.aspx",0]
			   };
			   
	var _hw_oSiteMapItem_IP= {
				 "Licensing" :[_hw_baseURL +"/About/Pages/technology-and-intellectual-property.aspx",0]
			   };
			   
	//Array defining all the sitemap structure

   	var _hw_oSiteMapTree_Complete =
							{ "Products &amp; Services": [_hw_oSiteMapItem_PSD, "_hw_SiteMap_Col1"], 
							   "Solutions &amp; Technologies": [_hw_oSiteMapItem_ST, "_hw_SiteMap_Col2"], 
							   "About Us": [_hw_oSiteMapItem_AboutUs, "_hw_SiteMap_Col2"],							   
							   "Intellectual Property": [_hw_oSiteMapItem_IP, "_hw_SiteMap_Col2"],   
							   "Investors": [_hw_oSiteMapItem_Investors, "_hw_SiteMap_Col3"], 
							   "News": [_hw_oSiteMapItem_Newsroom, "_hw_SiteMap_Col4"],
							   "Careers" :[_hw_oSiteMapItem_Careers,"_hw_SiteMap_Col5"],
							   "Corporate Citizenship": [_hw_oSiteMapItem_CSR, "_hw_SiteMap_Col5"],
							   "Worldwide":[_hw_oSiteMapItem_Worldwide,"_hw_SiteMap_Col5"]
				};	





