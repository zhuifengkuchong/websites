/*Generic click tracking*/

jQuery(document).ready(function() 
{
$('a').click(function(event)
{ 
   if( ($(this).parents().hasClass('banner_title')) || ($(this).hasClass('banner_link')) || ($(this).parents().hasClass('hw-webparts-h-in-space')) || ($(this).parents().hasClass('hw-webparts-link')) || ($(this).hasClass('hw-sitemap-txt')) || ($(this).hasClass('hwSocialMedia')) || ($(this).parents().hasClass('blue_embossed_tabs')) || ($(this).parents().hasClass('hw-section-land')) )
   {
       //alert("existing clicked123");
   }	
	else
	{
	   //alert("else clicked456");
       s=s_gi(s_account);
       s.linkTrackVars='eVar1,eVar53';
       s.linkTrackEvents='event4';
       s.events = s.apl(s.events,"event4",",",2); 
       recordlinkscEvent(this);
	}

});
});

/*Generic click tracking ends*/

/*****************************************************************************/
/*HoneywellInternationalInc - Site Catalyst Implementation For Honeywell.com*/
/*Description: This variable is assigned with dynamically change the  */
/*Developed by Web Content Support Team Oct 17th 2012 */
/*SiteCatalyst code version: H.25.1.*/
/*Copyright 1996-2012 Adobe, Inc. All Rights Reserved*/
/*More info available at http://www.omniture.com */
/*****************************************************************************/
var _hw_date = new Date();
var _hw_CurrentYear = _hw_date.getFullYear(); /*Find Date*/
var _hwHonURL = location.href;/*Find URL*/
var _hwHonURLLC =_hwHonURL.toLowerCase(); /*Convert to LowerCase*/
var _hwBCHome = "/home";/*Var Home Page Find*/
var _hwBCContact = "/contact";/*Var Contact Page Find*/
var _hwBCPNFound = "/pagenotfound";/*Var Page not Find*/
var _hwBCemployee = "/employee";/*Var employee access Page Find*/
var _hwBCterms = "/termsconditions";/*Var terms&conditions Page Find*/
var _hwBCvideocenter = "/videocenter";/*Var VideoCenter Page Find*/
var _hwBCsearch = "/search";/*Var search Page Find*/
var _hwBCHonnow = "http://honeywell.com/InternetFramework/Scripts/honeywellnow.com";/*Var HoneywellNow Page Find*/
var _hwNoBCFindProtocol = location.protocol;
var _hwNoBCFindHost = location.host;
var _hwBCRead = ($(".hw-nav-bar").text()).trim();
/*Replace Breadcrumb*/
var _hwBCReplace = _hwBCRead.split('>').join(':');
var _hwBCTrimStart = _hwBCReplace.split(' ').join('');
var _hwBCTrimStart1 = _hwBCTrimStart.split(':');
var _hwBCName = _hwBCTrimStart1.length; 
var _hwBcHonInt = "Honeywell International";
var _hwNoBCReadAllSite = _hwNoBCFindProtocol + '//' + _hwNoBCFindHost + '/sites/';
/*Replace Breadcrumb*/

/*Condition to Check Page Name Start */
 if(_hwHonURLLC.indexOf(_hwBCHome)!=-1) 
 {
   	HonHomeSCPageName();/*Check Home Page*/
 }
 else if(_hwHonURLLC.indexOf(_hwBCContact)!=-1) 
 {
 	HonContactSCPageName();/*Check Contact Us Page*/
 }
 else if(_hwHonURLLC.indexOf(_hwBCPNFound)!=-1) 
 {
 	HonNotFoundSCPageName();/*Check Page Not Found Page*/
 }
 
 else if(_hwHonURLLC.indexOf(_hwBCemployee)!=-1) 
 {
 	HonEmployeeAccessSCPageName();/*Check employee access Page*/
 }
 
else if(_hwHonURLLC.indexOf(_hwBCterms)!=-1) 
 {
 	HonTermsSCPageName();/*Check terms&conditions Page*/
 }

else if(_hwHonURLLC.indexOf(_hwBCvideocenter)!=-1) 
 {
 	HonVideoCenterSCPageName();/*Check videocenter Page*/
 }
 
else if(_hwHonURLLC.indexOf(_hwBCsearch)!=-1)  
 {
 	HonSearchSCPageName();/*Check search Page*/
 }
 
else if(_hwHonURLLC.indexOf(_hwBCHonnow)!=-1)  
 {
 	HonHonNowSCPageName();/*Track HoneywellNow Page*/
 }
 
 /*else if(_hwBCRead=_hwBcHonInt)
 {
  	HonNoBCSCPageName(); /*Check No BreadCrump's
  	alert("last Else IF");
 }*/
 else
 {   
    HonBreadCrumb();
 } 

/*Fuction to Check Page Name End */

function HonHomeSCPageName()   { s.pageName="Hon:Home"; s.prop7="Home"; }      /*Updating s.Name Home Page Name*/ 
function HonContactSCPageName(){ s.pageName="Hon:Contact"; s.prop7="Contact"; s.eVar32 = "Honeywell Contact Us Lead"; s.events="event32";} /*Updating s.Name Contact Page Name*/
function HonNotFoundSCPageName(){ s.pageName="404 error page"; s.prop30="http://www.honeywell.com/this_page_isnt_here"; s.pageType="errorPage";} /* 404 Error Page*/
function HonEmployeeAccessSCPageName(){ s.pageName="Hon:EmployeeAccess"; s.prop7="EmployeeAccess";} /* employee access Page*/
function HonTermsSCPageName(){ s.pageName="Hon:Terms&Conditions"; s.prop7="Terms&Conditions";} /* Terms&conditions Page*/
function HonVideoCenterSCPageName(){ s.pageName="Hon:VideoCenter"; s.prop7="VideoCenter";} /* VideoCenter Page*/
function HonSearchSCPageName(){ s.pageName="Hon:Search"; s.prop7="Search";} /* Search Page*/
function HonHonNowSCPageName() {   
   var arr = $.grep(_hwBCTrimStart1, function(value) {
                           return value != "Macro-Trends";
            }); /* to remove the text Macro Trends from the BreadCrumb */
   
   var alength=arr.length;
         if(alength <=3) /*Checking BreadCrumb limit for HomePage and Article Page tracking */
	{
		s.pageName=(alength==3)?"Hon:HonNow:"+arr[alength-1]:"Hon:HonNow:HomePage"; //assigning page name as per the standards
		s.prop7=(alength==3)?arr[alength-1]:"HonNow:HomePage"; //assigning the Prop7 value as per the standards
	}
        else
	{
                var pagename_val="Hon:HonNow";
		for(pnval=2;pnval<=alength-1;pnval++)
		{
		 pagename_val =  pagename_val+":"+arr[pnval];
		}
		//alert(pagename_val);
		s.pageName=pagename_val;  //assigning the PageName as Hon:HonNow:Category:ArticleTitle
		
		var prop7_val = "HonNow";
		for(p7val=2;p7val<=alength-2;p7val++)
		{
		  prop7_val = prop7_val+":"+arr[p7val];	      
		}
		s.prop7=prop7_val;//assigning the Prop7 value as HonNow:Category
		s.prop46=arr[alength-1]; //assigning the article title in the prop46 variable
		
	}
   
   } /* HoneywellNow Page - Arun Prasad */

function HonBreadCrumb()
{
	if(_hwBCName<=2) /*Checking Only Two Array*/
	{
		s.pageName="Hon:"+_hwBCTrimStart1[1];
		s.prop7=_hwBCTrimStart1[1];
	}
	/*else
	{
		s.pageName="Hon:"+_hwBCTrimStart1[1]+":"+_hwBCTrimStart1[2];
		s.prop7=_hwBCTrimStart1[1];
		s.prop46=_hwBCTrimStart1[2];
	}*/
	
	/* Done the code change to track all the levels of navigation */
	else
	{
	    //alert(_hwBCName);
		var pagename_val="Hon";
		for(pnval=1;pnval<=_hwBCName-1;pnval++)
		{
		 pagename_val =  pagename_val+":"+_hwBCTrimStart1[pnval];
		}
		//alert(pagename_val);
		s.pageName=pagename_val;
		
		var prop7_val = "";
		for(p7val=1;p7val<=_hwBCName-2;p7val++)
		{
		  if(p7val==1)
		  {
		   prop7_val = _hwBCTrimStart1[p7val];
		  }
		  else
		  {
			prop7_val = prop7_val+":"+_hwBCTrimStart1[p7val];
	      }
		}
		//alert(prop7_val);
		s.prop7=prop7_val;
		
		//alert(_hwBCTrimStart1[_hwBCName-1]);
		s.prop46=_hwBCTrimStart1[_hwBCName-1];
		
	}
	
}


function HonNoBCSCPageName()
{
/*Checking No BreadCrumbs across the Board*/
 var _hwBCFindPN = _hwNoBCReadAllSite.split('.aspx');
 var _hwBCFindPN0= _hwBCFindPN[0];
 var _hwBCFindPNReplace = _hwBCFindPN0, replacement = '!';
 _hwBCFindPNReplace = _hwBCFindPNReplace.replace(/\/([^\/]*)$/,replacement+'$1');
 var _hwBCFindPNReplaceSplit = _hwBCFindPNReplace.split("!");
 var _hwBCFindPNReplaceSA = _hwBCFindPNReplaceSplit[1];
 var _hwBCFinalPN = _hwBCFindPNReplaceSA.replace(/\b[a-z]/g, function($1){ return $1.toUpperCase(); });
 /*_hwBCFinalPN is used for no BreadCrumbs */ 
 //alert(_hwBCFinalPN+"No Breadcrump so Appending the Page Name ");
}


function HonCheckSiteCollection(HonSCVar)
{
   /*Checking Site Collection across the Board */
	if(_hwHonURLLC.indexOf(_hwNoBCReadAllSite)!=-1)
	{
		var _hwSiteCollFind = _hwNoBCReadAllSite.split('/');
    	var _hwSiteCollName = _hwSiteCollFind[4];
		//alert( _hwSiteCollName + HonSCVar +"Yes, this site belongs to site Collection");
	}
	else   
	{ 
	 	//alert(HonSCVar +"No, this site not belongs to site Collection");
	 	s.pageName="Hon:"+HonSCVar;
   	}

}


//s.pageName="Hon:Home"
s.currentYear=_hw_CurrentYear; 
s.server=""
s.channel="Hon"
s.pageType=""
s.prop1=""
s.prop2=""
s.prop3=""
s.prop4=""
s.prop5=""
/* Conversion Variables */
s.campaign=""
s.state=""
s.zip=""
s.events=""
s.products=""
s.purchaseID=""
s.eVar1=""
s.eVar2=""
s.eVar3=""
s.eVar4=""
s.eVar5=""
s.eVar18=s.prop18=window.location.href;
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code=s.t();if(s_code)document.write(s_code)

if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
document.write("<noscript><img src='http://honeywell.112.2o7.net/b/ss/hwhwhon.comprod/1/H.25.1--NS/0' height='1' width='1' border='0p' alt=''/></noscript>");
if(navigator.appVersion.indexOf('MSIE')>=0)document.write('--'+unescape('%3E'))

jQuery(document).ready(function() 
{
/*Social Media Icon Tracking*/
$('.hwSocialMedia').click(function(event) 
{
  s=s_gi(s_account);
  s.linkTrackVars='events,eVar45,prop45,eVar44,prop44';
  s.linkTrackEvents='event23';
  s.events = s.apl(s.events,"event23",",",2); 
  s.prop44 = s.eVar44 = s.pageName; 
  s.prop45 = s.eVar45 = $(this).attr("title");
  s.tl(this,'o', $(this).attr("title"));
});
/*Contact Us Page Tracking*/
$('.hw-contact-submit input').click(function(event) 
{

var _hwCustomer  = $("select[id$='_ddlCustomer'] option:selected").text();
var _hwCountry = $("select[id$='_ddlCountryName'] option:selected").text();
var _hwProductType = $("select[id$='_ddlProductType'] option:selected").text();
var _hwInquiryType = $("select[id$='_ddlSubject'] option:selected").text();
var _hwRsupplier = $("input[value='rbSupplier']:checked").val();
var _hwRmedia = $("input[value='rbMedia']:checked").val();
var _hwRinvestor = $("input[value='rbInvestor']:checked").val();
var _hwRcustomer = $("input[value='rbCustomer']:checked").val();
var _hwRother = $("input[value='rbOther']:checked").val();
var _hwRLsupplier = $("label[for$='_rbSupplier']").text();
var _hwRLmedia = $("label[for$='_rbMedia']").text();
var _hwRLinvestor = $("label[for$='_rbInvestor']").text();
var _hwRLcustomer = $("label[for$='_rbCustomer']").text();
var _hwRLother = $("label[for$='_txtOther']").text();
s=s_gi(s_account);
s.linkTrackVars= 'events,prop32,eVar32,prop33,eVar33,prop34,eVar34,prop35,eVar35';
s.events=s.apl(s.events,"event32",",",2);
s.linkTrackEvents= 'event32';
s.tl(this,'o','Contact us Submit Button');

if (_hwCountry =="Select Country") 
{
s.prop32 = s.eVar32 = "Not Selected: Country";
}
else
{
s.prop32 = s.eVar32 = _hwCountry;
}

if (_hwProductType == "Select Inquiry Type")
{
s.prop33 = s.eVar33 = "Not Selected: Product Type"
}
else
{
	s.prop33 = s.eVar33 = _hwProductType;
}
	s.prop34 = s.eVar34 = _hwInquiryType;
if (_hwRsupplier == "rbSupplier")
{
	s.prop35 = s.eVar35 =_hwRLsupplier;
}
else if (_hwRmedia == "rbMedia")
{
	s.prop35 = s.eVar35 =_hwRLmedia;
}
else if (_hwRinvestor == "rbInvestor")
{
	s.prop35 = s.eVar35 =_hwRLinvestor;
}

else if (_hwRcustomer == "rbCustomer")
{
s.prop35 = s.eVar35 =_hwRLcustomer+":"+_hwCustomer;
}
else if (_hwRother  == "rbOther")
{
	s.prop35 = s.eVar35 =_hwRLother;
}
s.tl(this,'o','Contact us Submit Button');
 
});

/*Tracking RSS Buttons*/
//$('#linkitem').click(function(event) 
$('.hw-section-land #linkitem a').click(function(event) 
{
  var _hWrsssubscribe = "Hon:Newsroom:Subscribe";
  var _hWPressRelease = "Hon:Newsroom";
  if(s.pageName==_hWPressRelease)
  {
  	s=s_gi(s_account);
  	s.linkTrackVars='events,prop42,eVar42';
  	s.linkTrackEvents='event43';
  	s.events = s.apl(s.events,"event43",",",2); 
  	s.prop42 = s.eVar42 = s.pageName;
  	s.tl(this,'o', 'RSS Subscribe Button clicked on Press Release page');
  } 
  else if(s.pageName==_hWrsssubscribe)
 {
	s=s_gi(s_account);
  	s.linkTrackVars='events,prop42,eVar42';
  	s.linkTrackEvents='event43';
  	s.events = s.apl(s.events,"event43",",",2); 
  	s.prop42 = s.eVar42 = s.pageName;
  	s.tl(this,'o', 'RSS Subscribe Button clicked on rss-subscribe page');

 }
   
});

/*Tracking exits to Careers Website*/


/*Sitemap Navigation*/
$('.hw-sitemap_linktxt').live("click", function(event)
{
	var _hwCarTxt = $(this).text();
	if (_hwCarTxt=="Careers")
	{
		s=s_gi(s_account);
  		s.linkTrackVars='events,prop31,eVar31';
  		s.linkTrackEvents='event31';
  		s.events = s.apl(s.events,"event31",",",2); 
  		s.prop31 = s.eVar31 = "Sitemap"; 
  		s.tl(this,'o', 'Top Menu Careers link');

	}
	
	else
	{
	   //alert("else clicked456");
	   s=s_gi(s_account);
       s.linkTrackVars='eVar1,eVar53';
       s.linkTrackEvents='event4';
       s.events = s.apl(s.events,"event4",",",2); 
       s.eVar1 =  $(this).text()+' - Sitemap';//generic links
       s.eVar53= 'Generic Clicks';
       s.tl(this,'o', 'Generic Click Tracking' );
	}

	

});
	


/*Top Navigation*/
$('.hw-parent-mnu-item a ').click(function(event) 
{
	var _hwCarTxt = $(this).text();
	if (_hwCarTxt=="Careers")
	{
		s=s_gi(s_account);
  		s.linkTrackVars='events,prop31,eVar31';
  		s.linkTrackEvents='event31';
  		s.events = s.apl(s.events,"event31",",",2); 
  		s.prop31 = s.eVar31 = "Top Menu"; 
  		s.tl(this,'o', 'Top Menu Careers link');

	}
});

/*Footer Navigation*/
$('.hw-footer-link a').click(function(event) 
{
	var _hwCarTxt = $(this).text();
	if (_hwCarTxt=="Careers")
	{
		s=s_gi(s_account);
  		s.linkTrackVars='events,prop31,eVar31';
  		s.linkTrackEvents='event31';
  		s.events = s.apl(s.events,"event31",",",2); 
  		s.prop31 = s.eVar31 = "Footer Menu"; 
  		s.tl(this,'o', 'Footer Careers link');

	}
});
	

/*Left Navigation*/
$('.hw-lft-nav-js-active a').click(function(event) 
{
	var _hwCarTxt = $(this).text();
	if (_hwCarTxt=="Careers")
	{
		s=s_gi(s_account);
  		s.linkTrackVars='events,prop31,eVar31';
  		s.linkTrackEvents='event31';
  		s.events = s.apl(s.events,"event31",",",2); 
  		s.prop31 = s.eVar31 = "Left Menu"; 
  		s.tl(this,'o', 'Left Navigation Careers link');

	}
});

/*Internal Campaigns Analysis*/
$('.home_banner_content .banner_text_content .banner_title a, .home_banner_content .banner_text_content .banner_link').click(function(event) 
{
  s=s_gi(s_account);
  s.linkTrackVars='eVar1';
  
  if(($(this).hasClass('banner_link'))){
	  
	  if($(this).attr("title")==""){
		s.eVar1 = $(this).text().trim() + " - Button"; //if no tool tip text available; takes button text
	  }else{
		s.eVar1 = $(this).attr("title") + " - " + $(this).text().trim() + " - Button"; //else takes both tooltip text and button text 
	  }
  }else{
	  s.eVar1 = $(this).attr("title");  //Clicks from Banner Link Text
  }
  
  s.eVar53= 'Promo Clicks';  
  s.tl(this,'o', s.eVar1 );
});


/* SC tracking for three small banners in hon.com  homepage*/
$('.hw-webparts-home .hw-webparts-h-in-space p a img').click(function(event) 
{
  s=s_gi(s_account);
  s.linkTrackVars='eVar1';
  s.eVar1 = $(this).parents('div.hw-webparts-h-in-space').find('span.hw-webparts-head').text() +' : IMAGE';
  s.tl(this,'o', s.eVar1 );
});

$('.hw-webparts-home .hw-webparts-h-in-space .hw-webparts-link a').click(function(event) 
{
  s=s_gi(s_account);
  s.linkTrackVars='eVar1';
  s.eVar1 = $(this).parents('div.hw-webparts-h-in-space').find('span.hw-webparts-head').text() +' : '+ $(this).text();  
  s.tl(this,'o', s.eVar1 );
});

/*Enhanced Download Tracking*/
 
$("select.hw_codeofcondact").change(function()
 {
 	
    var hwCodeofContact=$(this).find('option:selected').text(); 
    var hwCodeofVal=$(this).find('option:selected').val(); 
    window.open(this.options[this.selectedIndex].value,'_blank');
	showPDF(hwCodeofVal, hwCodeofContact);
	
	
 }); 


});

/*Enhanced Download Tracking Function*/
function showPDF(PhwCodeofVal, PhwCodeofContact)
{
      // stuff to show the PDF here
s.linkTrackVars+=',eVar8,eVar19,prop8,prop9,prop43,events';
s.eVar8=PhwCodeofVal;
s.prop9='D=v8'; 
s.eVar19=s.prop8='D=pageName'; 
s.events=s.apl(s.events,'event2',',',0); 
s.prop43=PhwCodeofContact;
s.linkTrackEvents='event2';
s.tl(true,'D','Code of Conduct');
return false;
}




function recordlinkscEvent(linksc) {
try {
var _slinkscText;

//Get the linksc text that was clicked
_slinkscText = jQuery(linksc).text();

//Get only page names
if (linksc.href != '' && !(linksc.href.match(/^mailto\:/i) || linksc.href.match(/^javascript\:/i) || linksc.href.match(/^file\:/i) || linksc.href.match(/^ftp\:/i))) 
{
 var _sPagesc = linksc.href.substring(linksc.href.lastIndexOf('/') + 1).toLowerCase();

  if (_sPagesc.indexOf('#') != -1) 
  {
    _sPagesc = _sPagesc.substring(0, _sPagesc.indexOf('#')).toLowerCase();
  }

  if (_sPagesc.indexOf('?') != -1) 
  {
     _sPagesc = _sPagesc.substring(0, _sPagesc.indexOf('?')).toLowerCase();
  } 

  if (_sPagesc != '') 
  {
      _slinkscText = _slinkscText + " " + "(" + _sPagesc + ")";
  }

}


               

        //_gaq.push(['_trackEvent', category, action, _slinkscText]);
		 s.eVar1 = _slinkscText;//all promo,logo,generic links
        s.eVar53= 'Generic Clicks';
       s.tl(this,'o', 'Generic Click Tracking' );



        // Check for the new window option

        if ((linksc.target == '') || (linksc.target == null)) {



            if ((linksc.href != '') && !((jQuery(linksc).hasClass('honeywellvideo')) || (jQuery(linksc).hasClass('hw-footerbox'))) && !(linksc.href.match(/^mailto\:/i) || linksc.href.match(/^javascript\:/i) || linksc.href.match(/^file\:/i) || linksc.href.match(/^ftp\:/i))) {



                var _sPath = location.href.toLowerCase();

                //Get the current URL path upto #

                if (location.href.indexOf('#') != -1) {

                    _sPath = location.href.substring(0, location.href.indexOf('#')).toLowerCase();

                }



                //Get the current linksc path upto #

                var _slinksc = linksc.href.toLowerCase();

                if (linksc.href.indexOf('#') != -1) {

                    _slinksc = linksc.href.substring(0, linksc.href.indexOf('#')).toLowerCase();

                }



                if (typeof (_sPath) != 'undefined') {

                    if (_sPath != _slinksc && _slinksc != '') {

                    //Cancel the default behaviour. This is because the default browser action will not wait for setTimeout to complete

                        event.preventDefault();

                        setTimeout(function() {

                            document.location = linksc.href;

                        }, 100);



                    }

                }



            }

         }



    } catch (err) { }

}

