/* extra stuff needed for every page */
_omniture.campaign=_omniture.getQueryParam('cid');

_omniture.prop10=_omniture.campaign;
_omniture.prop16=_omniture.getPreviousValue(_omniture.pageName,"s_pv");
if (_omniture.prop16){
    _omniture.prop15=_omniture.getPercentPageViewed();
}

_omniture.eVar10=_omniture.prop10;
_omniture.eVar11=_omniture.prop11;
_omniture.eVar12=_omniture.prop12;
_omniture.eVar13=_omniture.prop13;
_omniture.eVar14=_omniture.prop14;
_omniture.eVar17=_omniture.prop17;
_omniture.eVar18=_omniture.prop18;

// to be set by the page before omniture code is reached.
/*   example:
 _omniprops = {
  Event: 1,
  prop17: "site:promotion",
  prop18: "site:register",
  prop10: "prop10-override" 
  };
  
*/  
// set any page specific properties not controlled by the omniture engine.
if (typeof(_omniprops) != 'undefined') {
  for (prop in _omniprops) {
    _omniture[prop]=_omniprops[prop];
  }
}


  


