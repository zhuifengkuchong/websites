/****************************************************************************
***   3M WebTrends SmartSource Data Loader (WW)                           ***
***   Copyright (c) 3M.  All rights reserved.                             ***
***   Created: 3/19/2012 3:37:54 PM                                       ***
***   3M.com Asset ID: 1273678560856	||  LMD: 09/26/2015               ***
****************************************************************************/

//global vars
var site_locale = 'en_WW', WT = {}, WT_pg = {}, DCS = {};

//load core tag with parameters
window.webtrendsAsyncInit = function () {
	$db('*** START WEBTRENDS LOGGING ***','webtrends'); 
	$db('     Loading webtrends core plug-in','webtrends');
	var site_locale = getSiteLocale();
	var siteFPCDomain = getCurrDomain();
	var onSiteDomain = getCurrDomain().substring(1);
	
	var dcs = new Webtrends.dcs().init({
		dcsid: getDCSId(site_locale),
		domain: "http://solutions.3m.com/3MContentRetrievalAPI/statse.webtrendslive.com",
		timezone: -6,
		i18n: true,
		offsite: true,
		download: true,
		downloadtypes: "xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
		anchor: true,
		javascript: true,
		onsitedoms: onSiteDomain,
		fpcdom: siteFPCDomain,
		fpc: "WT_FPC1",
		plugins: {
			hm:{src:"../../s.webtrends.com/js/webtrends.hm.js"/*tpa=http://s.webtrends.com/js/webtrends.hm.js*/,hmurlparams:"p,loc,plmlblid,pc,N,rt,sku,PC_7_RJH9U52300N9F0IU7S8MC91KP3000000_nid,PC_7_RJH9U5230GGA50II31R7LV2AG7000000_nid,PC_7_RJH9U523080QE0IIHEICN90GC1000000_nid"},
			facebook: {src: "../../s.webtrends.com/js/webtrends.fb.js"/*tpa=http://s.webtrends.com/js/webtrends.fb.js*/},
			yt: {src: "../../s.webtrends.com/js/webtrends.yt.js"/*tpa=http://s.webtrends.com/js/webtrends.yt.js*/},
			mmm_downloads:{src:"//solutions.3m.com/3MContentRetrievalAPI/BlobServlet?assetId=1361806296197&assetType=MMM_Image&blobAttribute=ImageFile&x=a.js"},
			brightcove:{src:"//solutions.3m.com/3MContentRetrievalAPI/BlobServlet?assetId=1361815863944&assetType=MMM_Image&blobAttribute=ImageFile&x=a.js"},
			scroll_depth:{src:"//solutions.3m.com/3MContentRetrievalAPI/BlobServlet?assetId=1361814098901&assetType=MMM_Image&blobAttribute=ImageFile&x=a.js"},
		 	replicate:{src:"../../s.webtrends.com/js/webtrends.replicate.js"/*tpa=http://s.webtrends.com/js/webtrends.replicate.js*/, servers:[{ domain: "http://solutions.3m.com/3MContentRetrievalAPI/scs.webtrends.com"}]}
		}
	});
	
	var keyCount = 0;
	for (key in WT_pg) {       		
		dcs.WT[key] = WT_pg[key]; 
		keyCount++;
	}
	
	$db('     FPCDomain: ' + siteFPCDomain,'webtrends');	
	$db('     OnSite Domain: ' + onSiteDomain,'webtrends');	
	$db('     Added ' + keyCount + ' deprecated properties to core tag','webtrends'); 
	$db('     Firing dcs.track();','webtrends');
	dcs.track({
		finish: function(tag,options) {
			$db('     dcs.track Fire Complete','webtrends');
		}
	});
	runTripleTag(site_locale);	//call triple tag logic
	
};

//Start of Triple Tag Logic
function runTripleTag(site_locale) {
	var tripleTagCountryCodes = "('EU','CZ','AT','CH','DE','GR','GB','ZA','ES','FI','BE','AE','FR','HU','IL','IT','KZ','BE','NL','NO','PL','PT','RO','RU','SK','SE','TR','UA','DK','LT','EG')",
	currentCountry = site_locale.substring(3,5);
	$db('*** START TRIPLE TAG ***', 'webtrends');
	//only run plug-in if triple tag country code matches current locale... 
	if (tripleTagCountryCodes.indexOf(currentCountry) !== -1) {
		$db('     Triple Tag Country: ' + currentCountry, 'webtrends');
		var urlCodes = {
			"3m-espe" : "ESPE",
			"3m-espe-ee" : "ESPE",
			"3m_espe" : "ESPE",
			"3mgraphics" : "CG_GraphicSolutions",
			"abrasive-systems" : "AbrasiveSystems",
			"abrasivesystems" : "AbrasiveSystems", 
			"architectural-markets-eu" : "ArchitecturalMarkets",
			"artscrafts" : "ArtsCrafts",
			"asd" : "AbrasiveSystems", 
			"autofilms" : "AutomotiveWindowFilms", 
			"brandingsolutions" : "CG_BrandingSolutions",
			"cavilon" : "Cavilon",
			"coban" : "Coban", 
			"corrosion_protection" : "Corrosion%20Protection", 
			"corrosioncoatings" : "Corrosion%20Protection", 
			"digital-signage" : "DigitalSignage",
			"dyneon_eu" : "Dyneon", 
			"electricalmkts" : "Electrical",
			"electronicmonitoring" : "Electronic%20Monitoring",
			"electronics" : "Electronics",
			"euconsumerprojectors" : "Consumer%20Projectors",
			"eu_electricalmarkets" : "Electrical", 
			"eu_post-it" : "Post-it",
			"eu_post-its" : "Post-it", 
			"eu_scotch" : "Scotch",
			"eu-aad" : "BodyshopSolutions",
			"eu-aero" : "Aerospace",
			"eu-asd" : "AbrasiveSystems", 
			"eu-auto" : "AOEM2",
			"eu-auto" : "AOEM2",
			"eu-auto" : "Automotive%20Manufacturing", 
			"eu-command" : "Command",
			"eucommand" : "Command",
			"eu-cuno" : "Cuno",
			"eu-cuno" : "Filtration%20Solutions", 
			"eu-eamd" : "Energy%20%26%20Advance%20Materials", 
			"eu-filtrete" : "Filtrete",
			"eufuturo" : "Futuro",
			"eu-nexcare" : "Nexcare",
			"eu-pmd" : "PMD",
			"eu-post-its" : "Post-it", 
			"eu-scotch" : "Scotch",
			"eu-scotchbrite" : "Scotch-brite",
			"eu-scotchgard" : "Scotchgard",
			"euscotchbrand" : "Scotchbrand",
			"eye-occlusion" : "Opticlude",
			"facilities" : "Facilities",
			"facilitiesmanagement" : "Facilities",
			"fiscotch-bluebrand" : "Scotch%20BLUE",
			"food-safety" : "Microbiology",
			"gasdetection" : "GasDetection",
			"globalscotchbrand" : "Scotchbrand",
			"graphicsolutions" : "CG_GraphicSolutions",
			"healthcare" : "Healthcare%20portal",
			"healthcare-europe" : "Healthcare%20portal",
			"his" : "HIS",
			"identification_and_authentication_solutions" : "Security%20Systems", 
			"industrial-adhesives-and-tapes" : "TapesAndAdhesives",
			"library" : "LibrarySystems",
			"library_systems" : "LibrarySystems", 
			"librarysys" : "LibrarySystems",
			"littmann" : "Littmann",
			"littmann-gallery" : "Littmann",
			"meetingpresentationsolutions" : "Projection", 
			"microbiology_europe" : "Microbiology", 
			"mobileinteractive" : "Mobile_Interactive", 
			"mro-home" : "MRO",
			"occsafety" : "OH%26ES",
			"occupationalsafety" : "OH%26ES",
			"oem" : "OEM", 
			"pocketprojectors" : "Pocket%20Projectors", 
			"post-its" : "Post-it", 
			"renewableenergy" : "RenewableEnergy Wind", 
			"sandblaster" : "Sandblaster",
			"scotch-lite" : "Scotchlite",
			"skin-care" : "WoundResourceCenter",
			"solar" : "RenewableEnergy Solar", 
			"speedglas" : "Speedglas",
			"speedglashome" : "Speedglas",
			"ssd_eu" : "Security%20Systems",
			"tapes-and-adhesives" : "TapesAndAdhesives", 
			"tegaderm" : "Tegaderm", 
			"telecom_eu" : "Telecomms",
			"telecomms" : "Telecomms",
			"thinsulate_insulation" : "Thinsulate",
			"trafficsafety" : "TSS", 
			"traffic-safety-systems" : "TSS",
			"unitek" : "Unitek",
			"vehiclegraphics" : "VehicleGraphics",
			"vikuiti-uk" : "Optical", 
			"welding" : "Speedglas",
			"window-films" : "WindowFilms"
		};
		
		var siteFPCDomain = getCurrDomain();
		var onSiteDomain = getCurrDomain().substring(1);			
		
		var dcsTriple = new Webtrends.dcs().init({
			dcsid: 'dcsshk2e69w8wf9xwv5itrxpt_8w9c',
			domain: "http://solutions.3m.com/3MContentRetrievalAPI/statse.webtrendslive.com",
			timezone: -6,
			i18n: true,
			offsite: true,
			download: true,
			downloadtypes: "xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip",
			anchor: true,
			javascript: true,
			onsitedoms: onSiteDomain,
			fpcdom: siteFPCDomain,
			fpc: "WT_FPC2"
		});
		
		var url = window.location.href;
		var indexLocale = url.indexOf("3M/") + 3;
		var urlpath = url.substring(indexLocale, url.length - 1);
		var locale = url.substring(indexLocale, (indexLocale + 5));
		var placename = urlpath.substring(6, urlpath.indexOf("/", 6)).toLowerCase();
		
		if (urlCodes[placename] !== null) {
			dcsTriple.WT.z_cgdrill = urlpath;
			dcsTriple.WT.cg_n = urlCodes[placename]; 
			if (dcsTriple.WT.cg_n == undefined) {
				dcsTriple.WT.cg_n = readMetatagValue('mmmsite') + "-dbg"
			}
			dcsTriple.WT.pn_sku = locale + "|" + dcsTriple.WT.c/* product sku made up of local and lookup data */
			dcsTriple.WT.tx_e = "v"/* constant to ensure that  values are passed properly */
		}
		
		$db('     Triple Tag FPCDomain: ' + siteFPCDomain,'webtrends');	
		$db('     Triple Tag OnSite Domain: ' + onSiteDomain,'webtrends');	
		$db('     Triple Tag dcsID: dcsshk2e69w8wf9xwv5itrxpt_8w9c','webtrends');
		$db('     Firing triple.track();','webtrends');
		dcsTriple.track({
			finish: function(tag,options) {
				$db('     Triple Tag Fire Complete, disabling tag','webtrends');
				tag.enabled = false;
			}
		});
		
	}
	else {
		$db('     Not a Triple Tag Country: ' + currentCountry, 'webtrends');
	}
}
//end Triple Tag Function

//get FPC Domain
function getCurrDomain() {
	var siteDomain, siteFPCdomain
	var TLDs=["com","net","org","edu","mil","co"];
	
	siteDomain=document.domain.split(".");
	if(siteDomain[0]=="www")	{
		siteDomain.splice(0,1);
	}
	
	if(siteDomain.length>=2&&siteDomain[siteDomain.length-1].length>2){
		siteFPCdomain="."+siteDomain[siteDomain.length-2]+"."+siteDomain[siteDomain.length-1];
	}
	else{
		if(siteDomain.length==2)	{
			siteFPCdomain="."+siteDomain[siteDomain.length-2]+"."+siteDomain[siteDomain.length-1];
		}
		else	{
			if(siteDomain.length>=3&&siteDomain[siteDomain.length-1].length==2){
				var found = false, dmn = siteDomain[siteDomain.length-2];
				for (var i in TLDs) {
					if (dmn == TLDs[i]) {
						siteFPCdomain="."+siteDomain[siteDomain.length-3]+"."+siteDomain[siteDomain.length-2]+"."+siteDomain[siteDomain.length-1];
						found = true;
						break;
					}
				}
				if (found == false) {
					if((siteDomain[siteDomain.length-2].length>3)||(siteDomain[siteDomain.length-2].length==2))	{
						siteFPCdomain="."+siteDomain[siteDomain.length-2]+"."+siteDomain[siteDomain.length-1];
					}
				}
			}
			else{
				siteFPCdomain=""
			}
		}
	}
	return siteFPCdomain;
}

//store and retrieve correct DCS ID by passing in current locale
function getDCSId(site_Locale) {
	if(!site_Locale) {
		site_Locale = 'en_WW';		//site_Locale not passed in properly
	}
	var dcsId, onDemandLocales;		//declare function variables
	onDemandLocales = {
		"bg_BG" : "dcstfdi5o10000ch94z1tu534_9x2l", // Bulgarian - Bulgaria
		"cs_CZ" : "dcsbc2h7d00000oy6v3er9534_1t1h", // Czech - European Dashboard
		"da_DK" : "dcsc01xp700000oqovd59a534_4c9w", // Danish-Denmark - European Dashboard
		"de_AT" : "dcsdo53w1000000cpj599m534_3d2q", // German-Austria - European Dashboard
		"de_CH" : "dcs33hntd00000wwmgos6n534_5p6s", // German-Switzerland - European Dashboard
		"de_DE" : "dcsmiqttd00000439ogzsm534_8x8l", // German-Germany - European Dashboard
		"el_GR" : "dcst209gv000000cta5mdd534_6x2b", // Greek-Greece - European Dashboard
		"en_AE" : "dcsrhyg9s0000008u81leb534_8c5s", // English-Gulf
		"en_AU" : "dcss7g5v210000cpbvn3xl434_7t5f", // English-Australia
		"en_BE" : "dcs9ubw2m10000go98ppuu434_6u7f", // English-Belgium
		"en_BW" : "dcs1q3h23000004jpnvwam434_9j3z", // English-Botswana
		"en_CA" : "dcsa91d2f100000gc8rtwa534_9j8h", // English-Canada
		"en_CH" : "dcso99dtf000008eoysh9t434_9p1r", // English-Switzerland
		"en_CN" : "dcs69lgqb00000sd1c2rmm434_4i2n", // English-China
		"en_CR" : "dcsju1lr110000o6h8bj2n434_9q2i", // English-Costa Rica
		"en_DE" : "dcs2b5y7v00000wokhxz5o434_8q1y", // English-Germany
		"en_DK" : "dcs6zs9bz00000chp4yhav434_8w4q", // English-Denmark
		"en_EG" : "dcskmbznq10000c1twhden434_7e2i", // English-Egypt
		"en_ES" : "dcsfym53x00000gs8atqtw434_9q6z", // English-Spain
		"en_EU" : "dcstspuy1000004v6pp6sn434_8u4j", // English-European
		"en_FI" : "dcsberorf000000c1t4cmv434_1y4l", // English-Finland
		"en_GB" : "dcshefjze00000kbi1xb2k534_2g8u", // English-UK - European Dashboard
		"en_GU" : "dcsieo5jd000004bnw6v6v534_5s7q", // English-Guam
		"en_HK" : "dcs3ne9aw00000oiy95tjo434_2g1b", // English-Hong Kong
		"en_ID" : "dcsx4vlu00000086qukfbp434_4w3t", // English-Indonesia
		"en_IE" : "dcsgglbxq10000k34t6gob534_4c4c", // English-Ireland
		"en_IN" : "dcsoa27xh10000cdaybnvo434_5g5y", // English-India
		"en_IS" : "dcs4gro8900000o6dhb6yv434_3e9n", // English-Israel
		"en_JM" : "dcsy0cydb10000w02jr9np434_5g3m", // English-Jamaica
		"en_KE" : "dcsvtv3bn10000kvd7y3zp434_4b8s", // English-Kenya
		"en_KR" : "dcscqzkr000000cprz5xcq434_3p7g", // English-Korea
		"en_LK" : "dcsn17zo000000cl82kpts434_9p4i", // English-Sri Lanka
		"en_MY" : "dcslkwfjd100000k3ocroq434_1b9t", // English-Malaysia
		"en_NA" : "dcsmor7fc10000sdhgkk2r434_1r6z", // English-Namibia
		"en_NL" : "dcs1lkedq1000082n1h18w434_5z9t", // English-Netherlands
		"en_NO" : "dcsqvssou10000sxwlmwhw434_5n1n", // English-Norway
		"en_NZ" : "dcsbrl8fc10000g8t4reer434_8f8g", // English-New Zealand
		"en_PH" : "dcs4xk00u00000c19107ur434_7w2l", // English-Phillippines
		"en_PK" : "dcsspdtbz00000wsznjuku434_1u1p", // English-Pakistan
		"en_PR" : "dcs5x8wy3000000wkp616s434_3l3n", // English-Puerto Rico
		"en_SE" : "dcs8kakhc000004nkyzk5x434_1h8e", // English-Sweden
		"en_SG" : "dcsqcq1se00000oqwddvhs434_1j8v", // English-Singapore
		"en_TT" : "dcsp1jxlp00000w80nzblt434_3x4h", // English-Trinidad
		"en_TW" : "dcsx4expg3uls0h9az217kfxp_4o4o", // English-Taiwan
		"en_US" : "dcs4t6zu210000wg6dqhqj534_1s9q", // English-US
		"en_VN" : "dcsmuiw0v00000o2ef75zt434_1w8p", // English-Vietnam
		"en_WW" : "dcs3nzjw300000cxp3ezau434_4s1n", // English-WW
		"en_ZA" : "dcs9af6ok000008yfhda0c534_3v6k", // English-South Africa - European Dashboard
		"es_AR" : "dcs87gyls00000cl4bkcp1534_7j6i", // Spanish-Argentina
		"es_BO" : "dcst61i6q100000ggzq612534_9g1g", // Spanish-Bolivia
		"es_CL" : "dcsi1dk1900000oasnx0d2534_2q7w", // Spanish-Chile
		"es_CO" : "dcsztx8501000086283wm2534_9r2e", // Spanish-Colombia
		"es_CR" : "dcs4pkib21000000g0bp03534_5q9j", // Spanish-Costa Rica
		"es_DO" : "dcshizu9p000008q19nem3534_7f7c", // Spanish-Dominican Republic
		"es_EC" : "dcs2ygfo4000000kf1v704534_8c8w", // Spanish-Ecuador
		"es_ES" : "dcs0hm4od000000omphc6s534_3c3o", // Spanish-Spain - European Dashboard
		"es_GT" : "dcshf2lct10000w4dydrx4534_5s2h", // Spanish-Guatemala
		"es_GU" : "dcsw3wisg00000oyehmhyv534_7n3o", // Spanish-Guam
		"es_HN" : "dcsyftf6e10000oyqqlkb5534_1e2t", // Spanish-Honduras
		"es_MX" : "dcsrqbu6w100008adnpdxh534_8x8z", // Spanish-Latino
		"es_NI" : "dcsf0oahi100008u0brfl5534_8b6b", // Spanish-Nicaragua
		"es_PA" : "dcso2319s10000spavwav5534_9r7c", // Spanish-Panama
		"es_PE" : "dcsu9zfq600000cd2gcxm6534_4b7q", // Spanish-Peru
		"es_PR" : "dcsbyosvn10000w8c0isw6534_2o7b", // Spanish-Puerto Rico
		"es_PY" : "dcs5fbh4b00000gkmj3576534_2s8u", // Spanish-Paraguay
		"es_SV" : "dcsza4j8c00000oerp12c4534_5l5u", // Spanish-El Salvador
		"es_UY" : "dcs8fnhkh00000k3ooom87534_4q3v", // Spanish-Uruguay
		"es_VE" : "dcs1t4z8g100004zx8uhi7534_2y4k", // Spanish-Venezuela
		"et_EE" : "dcs1uc5q200000wgyq7ejx434_4t6w", // Estonian - Estonia
		"fi_FI" : "dcsq021qb0000086up36ek534_6v3r", // Finnish-Finland - European Dashboard
		"fr_AE" : "dcs40ijze10000chhmyr1d534_7r7f", // French-Gulf
		"fr_BE" : "dcs6fo3cw10000stp1j5ac534_6j4g", // French-Belgium - European Dashboard
		"fr_CA" : "dcs7naksz00000go1qpzlc534_6u9r", // French-Canada
		"fr_CH" : "dcscg3tx410000om17skll534_4i2l", // French-Switzerland - European Dashboard
		"fr_FR" : "dcsff9ilp10000s14a91ok534_6s1q", // French-France - European Dashboard
		"fr_MA" : "dcsakye2k00000s9eng6zx434_3j5b", // French-Morocco
		"hr_BA" : "dcseud6se10000w0me9g7l434_9s9c", // Croatian-Bosnia
		"hr_HR" : "dcsr31qdj00000ouz6h9ll434_5c2c", // Croatian - Croatia
		"hu_HU" : "dcsm8jx4u10000k73vahnd534_4j3m", // Hungarian-Hungary - European Dashboard
		"id_ID" : "dcsgzssof000004z10uumy434_1z3n", // Indonesian-Indonesia
		"it_CH" : "dcs5albmh10000oubkzpwy434_6n1o", // Italian-Switzerland
		"it_IT" : "dcs8al7mp00000ggv3i7fq534_2k1d", // Italian-Italy - European Dashboard
		"iw_IL" : "dcsbck59n00000g4qbn0by434_3n4y", // Hebrew - Israel
		"ja_JP" : "dcsrbsjql0000082fjhbzd534_9z4x", // Japanese
		"kk_KZ" : "dcs4tdyc710000sxo3n69e534_8d7c", // Kazakh-Kazakhstan
		"ko_KR" : "dcstjm10d00000oq40wyoe534_6d2l", // Korean - Korea
		"lt_LT" : "dcsb1heyw000000chxm520534_6h2r", // Lithuanian - Lithuania
		"lv_LV" : "dcs2zp6c1100008i35fcoz434_8o4f", // Latvian  - Latvia
		"mk_MK" : "dcsg0j32b10000s5vpuyf0534_6d9w", // Macedonian-Macedonia
		"nl_BE" : "dcstcxtl600000cl0kkzka534_6p6h", // Dutch-Belgium - European Dashboard
		"nl_NL" : "dcsbn7pr1100000oqghpaj534_2d9h", // Dutch-Netherlands - European Dashboard
		"no_NO" : "dcs9yzrf010000k7f8txyq534_7d5k", // Norwegian - European Dashboard
		"pl_PL" : "dcsm91wu0100008mek1uye534_9m9e", // Polish - European Dashboard
		"pt_BR" : "dcs3imjsc10000sho47p8f534_3i4w", // Portuguese-Brazil
		"pt_PT" : "dcsbibl8l000004v6t8kqr534_8w3m", // Portuguese-Portugal - European Dashboard
		"ro_RO" : "dcsw3a87i10000gc0tdjkf534_4c5z", // Romanian - Romania - European Dashboard
		"ru_BY" : "dcs1jcy7t0000086ellcyf534_8y2u", // Russian-Belarus
		"ru_KZ" : "dcse0yxcv00000w0q9s6ag534_4m2c", // Russian-Kazakhstan
		"ru_RU" : "dcs3vhoka10000kv1yy0mg534_8t4z", // Russian-Russia - European Dashboard
		"ru_UA" : "dcs14b0sh00000g07e1tr0534_9c8p", // Russian-Ukraine
		"sk_SK" : "dcslznhqn000000kredobh534_7e9p", // Slovak-Slovakia - European Dashboard
		"sl_SI" : "dcsmvklin10000kf1zijlh534_4h7e", // Slovenian-Slovenia
		"sq_AL" : "dcszoxk8g000008atnotei434_4r2k", // Albanian-Albania
		"sr_ME" : "dcs208eju100000wgy6o11534_2n2h", // Serbian-Montenegro
		"sr_RS" : "dcsfdez2c10000oqsmdid1534_7g1n", // Serbian-Serbia
		"sr_YU" : "dcs4jkyin10000cpfq6uzg534_3r8q", // Serbian-Yugoslavia
		"sv_SE" : "dcsglr84610000w4pbw79i534_2d2k", // Swedish-Sweden - European Dashboard
		"th_TH" : "dcsm0k5hw10000st9x0cu7534_8b2w", // Thai-Thailand
		"tr_TR" : "dcsdqgpmc10000kz0032li534_8l3o", // Turkish - European Dashboard
		"uk_RU" : "dcsrswnh000000w41peokv534_2p8l", // Ukranian-Russia
		"uk_UA" : "dcsyxrf4w10000ctesavyi534_7y3o", // Ukrainian - Ukraine - European Dashboard
		"us_SharePoint" : "dcsj6i8dwuz5bdvaosv4v4nd6_2o5l", // SharePoint
		"vi_VN" : "dcs0a3fes00000sd9ykut8534_5y3m", // Vietnamese-Vietnam
		"ww_WW" : "dcs9k4t8e00000g8lmro59534_7j7p", // Worldwide
		"zh_CN" : "dcsda68nc10000086i0ork434_5v3g", // Chinese-Simplified
		"zh_HK" : "dcs48ft4t000004n8lh4uj434_8w9r", // Chinese-Hong Kong
		"zh_TW" : "dcsii48uy0000004v6xjf9534_5q3c" // Chinese-Traditional
	};
	
	dcsId = onDemandLocales[site_Locale];
	if (dcsId == undefined || dcsId == 'undefined') {dcsId = onDemandLocales['en_US'];}
	if (typeof gDcsId !== 'undefined') {
		$db('     gDcsID detected... testing to see if legal value...','webtrends');  
		var gDcsLegal = false,
			gDcsLocale = '';
		for (var key in onDemandLocales) {
			if (onDemandLocales[key] == gDcsId) {
				//found legal match
				gDcsLegal = true;
				gDcsLocale = key;
				dcsId = gDcsId;
				break;
			}
		}
		if (gDcsLegal == true) {
			$db('        gDcsID exists in onDemandLocales object - overriding original dcsID with ' + gDcsLocale + ': ' + dcsId,'webtrends');
		}
		else {
			$db('         gDcsID value of ' + gDcsId + ' doesn\'t exist in onDemandLocales object - falling back to en_US dcsID of ' + dcsId,'webtrends');
		}
	}
	else {
		$db('     gDcsID not detected.','webtrends');  
	}
	$db('     dcsId = ' + dcsId,'webtrends');
	return dcsId;
}/* Utility Functions that Support Webtrends */

//deprecated functions from v8
function dcsMultiTrack() {
	$msg('Calls to dcsMultiTrack should only be done upon events, not page load. Contact eMetrics');
}

function dcsMultiTrackHREF(){
	$msg('dcsMultiTrackHREF() is a deprecated function and should be updated. Contact eMetrics');
}	

function dcsMultiTrackImage() {
	$msg('dcsMultiTrackImage() is a deprecated function and should be updated. Contact eMetrics');
}

function dcsMultiTrackForm(){
	$msg('dcsMultiTrackImage() is a deprecated function and should be updated. Contact eMetrics');
}

function dcsMultiTrackArray() {
	$msg('dcsMultiTrackArray() is a deprecated function and should be updated. Contact eMetrics');
}

function dcsOffsite() {
	$msg('dcsOffsite() is a deprecated function and should be updated. Contact eMetrics');
}

function sendPageInfoToSDC() {
	//kill old function call until removed from theme
}

//set DCS parameters for sites that reference them. 
DCS.dcsuri = window.location.pathname + window.location.search + window.location.hash;

//end deprecated functions

//Utility Logging Functions
function $msg(msg){
	//always outputs the message passed into the console. 
	if (window.console && window.console.log){
		window.console.log(msg);
	}
}

function $db(msg, param){
	//debug function (message to write to log, parameter to enable (default is debug=webtrends)
	if (!param) {
		var param = 'webtrends';
	}
	if ($u.debug) {
		if ($u.debug.indexOf(param) != -1) {
			if (window.console && window.console.log) 
			window.console.log(msg);
		}
	}
};

function readMetatagValue(a){
	//case insensitive meta tag reader
	var b,c,d,e,f,g;
	e="",f=0,document.all?b=document.all.tags("meta"):document.documentElement&&(b=document.getElementsByTagName("meta"));
	var h=b.length;
	for(g=0;
	g<h;
	g++){
		c=b[g].name;
		if(c.toUpperCase()==a.toUpperCase())return f=1,""+b[g].content
	}
	if(f===0)return "No Meta Tag Found"
}

function getURLStringWT(){
	//get URL Params and store within $u
	var vars = [], urlHash;
	var urlHashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for (var i = 0; i < urlHashes.length; i++) {
		urlHash = urlHashes[i].split('=');
		vars.push(urlHash[0]);
		vars[urlHash[0]] = urlHash[1];
	}
	return vars;
}
var $u = getURLStringWT();

function getSiteLocale(){
	//declare function variables
	var site_locale = 'en_WW', 
	site_locale = readMetatagValue('DCSExt.Locale');
	$db('     Site Locale: ' + site_locale, 'webtrends');
	return site_locale;
}

(function(){
	$db('     Loading Webtrends Files...','webtrends');
	WT_pg = WT;		//convert old WT params on page into WT_pg for passing into tag. 
	var s, s2;
	s = document.createElement("script");
	s.async = true;
	s.src = "BlobServlet-lmd=1395888402000&assetId=1319226005010&assetType=MMM_Image&blobAttribute=ImageFile&x=y.js"/*tpa=http://solutions.3m.com/3MContentRetrievalAPI/BlobServlet?assetId=1319226005010&assetType=MMM_Image&blobAttribute=ImageFile&x=y*/;
	s2 = document.getElementsByTagName("script")[0];
	s2.parentNode.insertBefore(s, s2);
}());


