
/**
* User Agent Object
* detects user agent.
* usage: 
*  var ua = new UserAgent();
*  if (ua.ie == true) ...;
*/
function UserAgent() {
	var b = navigator.appName;
	var v = this.version = navigator.appVersion;
	this.v = parseInt(v);
	this.ns = (b=="Netscape");
	this.ie = (b=="Microsoft Internet Explorer");
	this.opera = navigator.userAgent.indexOf("Opera")>0;
  this.safari = navigator.userAgent.indexOf("Safari")>0;
	if (this.ns) {
		this.v = parseInt(v);
		this.ns4 = (this.v==4);
		this.ns6 = (this.v>=5);
	}
	else if (this.ie) {
		this.ie4 = this.ie5 = this.ie55 = this.ie6 = false;
		if (v.indexOf('MSIE 4')>0) {this.ie4 = true; this.v = 4;}
		else if (v.indexOf('MSIE 5')>0) {this.ie5 = true; this.v = 5;}
		else if (v.indexOf('MSIE 5.5')>0) {this.ie55 = true; this.v = 5.5;}
		else if (v.indexOf('MSIE 6')>0) {this.ie6 = true; this.v = 6;}
	}
	else if (this.opera) {
		this.v = parseInt(v);
	} 
	this.dom = (document.getElementById)? true : false;
	var ua = navigator.userAgent.toLowerCase();

	this.win32 = ua.indexOf("win")>-1;
	this.mac = ua.indexOf("mac")>-1;
	this.other = (!this.win32 && !this.mac);
}
