<script id = "race1a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race1={};
	myVars.races.race1.varName="searchBox__onfocus";
	myVars.races.race1.varType="@varType@";
	myVars.races.race1.repairType = "@RepairType";
	myVars.races.race1.event1={};
	myVars.races.race1.event2={};
	myVars.races.race1.event1.id = "Lu_DOM";
	myVars.races.race1.event1.type = "onDOMContentLoaded";
	myVars.races.race1.event1.loc = "Lu_DOM_LOC";
	myVars.races.race1.event1.isRead = "False";
	myVars.races.race1.event1.eventType = "@event1EventType@";
	myVars.races.race1.event2.id = "searchBox";
	myVars.races.race1.event2.type = "onfocus";
	myVars.races.race1.event2.loc = "searchBox_LOC";
	myVars.races.race1.event2.isRead = "True";
	myVars.races.race1.event2.eventType = "@event2EventType@";
	myVars.races.race1.event1.executed= false;// true to disable, false to enable
	myVars.races.race1.event2.executed= false;// true to disable, false to enable
</script>

