
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
<link href="/css/OmniStyles.css" rel="stylesheet" type="text/css" />

<meta name="description" content="" />
<meta name="keywords" content="" />

<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<script type=IN/JYMBII data-format="inline" data-companyid="7117"></script>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery(".content").hide();
  jQuery(".hideall").hide();
  //toggle the componenet with class msg_body

  jQuery(".heading").click(function()
  {
    jQuery(this).next(".content").slideToggle(500);
    jQuery(this).find("span").text(jQuery(this).find("span").text() == '+' ? '-' : '+');
  });

  jQuery(".showall").click(function()
  {
    jQuery(".expand").text("-");
    jQuery(".showall").hide();	
    jQuery(".hideall").show();	
    jQuery(".content").show();

  });

    jQuery(".hideall").click(function()
  {
   jQuery(".expand").text("+");
    jQuery(".hideall").hide();	
    jQuery(".showall").show();	
    jQuery(".content").hide();
    });
	
});
</script>



<title> Omnicare : File Not Found</title>
<script type="text/javascript">
	var addthis_config = {"data_track_clickback":true};
</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4d77a7937134e621"></script>
<meta name="google-site-verification" content="6skl_IhTr4rhXn_gxqT6_yw2EQQW41Tna2QWptvoDWQ" />
</head>

<body>
<div class="MainContainer">

<div class="TopNav"><p><a href="/long-term-care/omnicare-pharmacy-services.aspx">Omnicare Pharmacy</a>  |  <a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-irhome">Investors</a>  |  <a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-news&nyo=0">News</a>  |  <a href="/careers.aspx">Careers</a>  |  <a href="/about-us/contact-us.aspx">Contact Us</a></p></div>
<div class="MainNav"><a href="/"><img src="/images/omniLogo.gif" alt="Omnicare" width="227" height="73" align="left" border="0" /></a>
<span><a href="/about-us.aspx">About Us</a><a href="/our-companies.aspx">Our Companies</a><a href="/about-us/our-mission.aspx">Our Mission</a></span></div>




<div class="SubHeader"><img src="/images/headerWave.gif" width="962" height="30" /></div>
<div class="Crumbs"><div id="breadcrumb"><ul class="Crumbs"><li class="level1" id="id-1098"><a href="/" class="level1">Home</a></li><li>&gt;</li><li class="level2 active on" id="id-1804"><a href="/file-not-found.aspx" class="level2 active on">File Not Found</a></li></ul></div>
<span class="tools">
<!-- AddThis Button BEGIN -->
<span class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_compact"></a>
</span>
<!-- AddThis Button END -->
</span>
</div>
<div class="LeftNav">
<ul class="LeftNav lv1">
  <li class="hasChildren"><a href="/long-term-care.aspx">Long Term Care</a></li>
  <li><a href="/specialty-care.aspx">Specialty Care</a></li>
  <li class="hasChildren"><a href="/technology-solutions.aspx">Technology Solutions</a></li>
  <li class="hasChildren"><a href="/about-us.aspx">About Us</a></li>
  <li class="hasChildren"><a href="/our-companies.aspx">Our Companies</a></li>
  <li class="hasChildren"><a href="/careers.aspx">Careers</a></li>
  <li><a href="/omniplanfinder.aspx">OmniPlanFinder</a></li>
  <li><a href="/amda.aspx">AMDA</a></li>
</ul>
<ul class="LeftNav lv1">
	<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-irhome">Investors</a></li>
	<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-news&nyo=0">News and Events</a></li>
</ul>
</div>
<div class="SubContent">
	<div class="PageTitle">Oops - Content Not Found</div>
	<div class="MainContent"><p style="text-align: left;">&nbsp;</p>

We're sorry, but we can't find the page you're looking for. Please
use the links at the left or at the bottom of the page to find your
way around the site.<br />
<br />
<p style="text-align: left;">If you think something is wrong then
please let us know at <a
href="mailto:webadministrator@omnicare.com">webadministrator@omnicare.com</a>.</p>

<p style="text-align: left;">&nbsp;</p>

<p style="text-align: left;">&nbsp;</p>

<p style="text-align: left;">&nbsp;</p>

<p style="text-align: left;">&nbsp;</p></div>
</div>


</div>
<div class="siteMap">
    <div class="siteMapContent">
	<div><h4><a href="/long-term-care/omnicare-pharmacy-services.aspx">Omnicare Pharmacy</a></h4><ul class=""><li class="level4" id="id-1248"><a href="/long-term-care/omnicare-pharmacy-services/technology-solutions.aspx" class="level4">Technology Solutions</a></li><li class="level4" id="id-1221"><a href="/long-term-care/omnicare-pharmacy-services/pharmacy-consulting.aspx" class="level4">Pharmacy Consulting</a></li><li class="level4" id="id-1262"><a href="/long-term-care/omnicare-pharmacy-services/infusion-therapy.aspx" class="level4">Infusion Therapy</a></li><li class="level4" id="id-1264"><a href="/long-term-care/omnicare-pharmacy-services/medicare-part-d.aspx" class="level4">Medicare Part D</a></li><li class="level4" id="id-1276"><a href="/long-term-care/omnicare-pharmacy-services/assisted-living.aspx" class="level4">Assisted Living</a></li><li class="level4" id="id-1279"><a href="/long-term-care/omnicare-pharmacy-services/alternate-care.aspx" class="level4">Alternate Care</a></li><li class="level4" id="id-1929"><a href="/long-term-care/omnicare-pharmacy-services/ltc-education.aspx" class="level4">LTC Education</a></li><li class="level4" id="id-1934"><a href="/long-term-care/omnicare-pharmacy-services/omniplanfinder.aspx" class="level4">OmniPlanFinder</a></li></ul></div>
	<div><h4><a href="/technology-solutions.aspx">Technology Solutions</a></h4><ul class=""><li class="level3" id="id-1538"><a href="/technology-solutions/omniview.aspx" class="level3">Omniview</a></li><li class="level3" id="id-1321"><a href="/technology-solutions/omniview-for-doctorsprescribers.aspx" class="level3">Omniview for Doctors/Prescribers</a></li><li class="level3" id="id-1322"><a href="/technology-solutions/omniview-for-residentsfamilies.aspx" class="level3">Omniview for Residents/Families</a></li></ul></div>
	<div><h4><a href="/about-us.aspx">About Us</a></h4><ul class=""><li class="level3" id="id-1434"><a href="/about-us/contact-us.aspx" class="level3">Contact Us</a></li><li class="level3" id="id-1308"><a href="/about-us/our-mission.aspx" class="level3">Our Mission</a></li><li class="level3" id="id-1702"><a href="/about-us/core-values.aspx" class="level3">Core Values</a></li><li class="level3" id="id-1564"><a href="/about-us/website-privacy-policy.aspx" class="level3">Website Privacy Policy</a></li><li class="level3" id="id-1565"><a href="/about-us/terms-of-use.aspx" class="level3">Terms of Use</a></li><li class="level3" id="id-1829"><a href="/about-us/hipaa-privacy-statement.aspx" class="level3">HIPAA Privacy Statement</a></li><li class="level3" id="id-1931"><a href="/about-us/dra-false-claims-act-notice.aspx" class="level3">DRA False Claims Act Notice</a></li></ul></div>
	<div><h4><a href="/our-companies.aspx">Our Companies</a></h4><ul class=""><li class="level3" id="id-1567"><a href="/our-companies/long-term-care.aspx" class="level3">Long Term Care</a></li><li class="level3" id="id-1436"><a href="/our-companies/specialty-care.aspx" class="level3">Specialty Care</a></li><li class="level3" id="id-1306"><a href="/our-companies/hospital.aspx" class="level3">Hospital</a></li></ul></div>
	<div><h4><a href="/careers.aspx">Careers</a></h4><ul class=""><li class="level3" id="id-1785"><a href="/careers/culture.aspx" class="level3">Culture</a></li><li class="level3" id="id-1767"><a href="/careers/benefits.aspx" class="level3">Benefits</a></li><li class="level3" id="id-1496"><a href="/careers/interview-process.aspx" class="level3">Interview Process</a></li><li class="level3" id="id-1506"><a href="/careers/university-relations.aspx" class="level3">University Relations</a></li><li class="level3" id="id-1482"><a href="/careers/career-faqs.aspx" class="level3">Career FAQs</a></li></ul></div>
	<div><h4><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-irhome">Investors</a></h4>
	    <ul>
		<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-govhighlights">Corporate Governance</a></li>
		<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-stockquote">Stock Information</a></li>
		<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-sec">Financial Information</a></li>
		<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-fundsnapshot">Fundamentals</a></li>
		<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-analysts">Analysts and Estimates</a></li>
		<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-contact">Contact Information</a></li>
		<li><a href="http://ir.omnicare.com/phoenix.zhtml?c=65516&p=irol-news&nyo=0">News and Events</a></li>

	     </ul>
	</div>
    </div>
</div>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-943002-2";
urchinTracker();
</script>

<script type="text/javascript">
    (function () {
        var didInit = false;
        function initMunchkin() {
            if (didInit === false) {
                didInit = true;
                Munchkin.init('095-VIX-581');
            }
        }
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = document.location.protocol + '//munchkin.marketo.net/munchkin.js';
        s.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                initMunchkin();
            }
        };
        s.onload = initMunchkin;
        document.getElementsByTagName('head')[0].appendChild(s);
    })();
</script>

</body>
</html> 


