<script id = "race14a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race14={};
	myVars.races.race14.varName="[0x7ffad93a4000].focus";
	myVars.races.race14.varType="@varType@";
	myVars.races.race14.repairType = "@RepairType";
	myVars.races.race14.event1={};
	myVars.races.race14.event2={};
	myVars.races.race14.event1.id = "Lu_DOM";
	myVars.races.race14.event1.type = "onDOMContentLoaded";
	myVars.races.race14.event1.loc = "Lu_DOM_LOC";
	myVars.races.race14.event1.isRead = "False";
	myVars.races.race14.event1.eventType = "@event1EventType@";
	myVars.races.race14.event2.id = "ctl06_ctl04_ctl00_TXTQuery";
	myVars.races.race14.event2.type = "onfocus";
	myVars.races.race14.event2.loc = "ctl06_ctl04_ctl00_TXTQuery_LOC";
	myVars.races.race14.event2.isRead = "True";
	myVars.races.race14.event2.eventType = "@event2EventType@";
	myVars.races.race14.event1.executed= false;// true to disable, false to enable
	myVars.races.race14.event2.executed= false;// true to disable, false to enable
</script>

