<script id = "race30b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race30={};
	myVars.races.race30.varName="Object[389129]._f5";
	myVars.races.race30.varType="@varType@";
	myVars.races.race30.repairType = "@RepairType";
	myVars.races.race30.event1={};
	myVars.races.race30.event2={};
	myVars.races.race30.event1.id = "heroCarousel";
	myVars.races.race30.event1.type = "onmouseout";
	myVars.races.race30.event1.loc = "heroCarousel_LOC";
	myVars.races.race30.event1.isRead = "False";
	myVars.races.race30.event1.eventType = "@event1EventType@";
	myVars.races.race30.event2.id = "heroCarousel";
	myVars.races.race30.event2.type = "onmouseover";
	myVars.races.race30.event2.loc = "heroCarousel_LOC";
	myVars.races.race30.event2.isRead = "False";
	myVars.races.race30.event2.eventType = "@event2EventType@";
	myVars.races.race30.event1.executed= false;// true to disable, false to enable
	myVars.races.race30.event2.executed= false;// true to disable, false to enable
</script>

