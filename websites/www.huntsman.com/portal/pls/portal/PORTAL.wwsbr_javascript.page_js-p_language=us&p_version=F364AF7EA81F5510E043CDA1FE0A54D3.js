function getParameter(queryString, parameter) {
  var paramRegExp = new RegExp("[\\?&]" + parameter + "=(\\w*)");
  var result = queryString.match(paramRegExp);
  return result == null ? null : result[1];
}
function refreshPortlet(portletReference, pageURL) {
  if (document.getElementById && document.getElementById("portalIFrame")) {
    animateRefreshIcon(portletReference)
    var debugLevel = getParameter(location.search, "_debug");
    document.getElementById("portalIFrame").src = 
       pageURL + (pageURL.indexOf("?") == -1 ? "?" : "&") + "_portletRefresh=" + escape(portletReference)
       + (debugLevel != null ? "&_debug=" + debugLevel : "");
  }
  else
    location.href = pageURL;
}
function refreshPortletIFrame(portletReference, pageURL) {
  document.getElementById("portalIFrame_" + portletReference).src = 
     pageURL + (pageURL.indexOf("?") == -1 ? "?" : "&") + "_portletRefresh=" + escape(portletReference);
}
function animateRefreshIcon(portletReference) {
  var rimg = document.getElementById("refresh_" + portletReference);
  if (!rimg) rimg = document.images["refresh_" + portletReference]
  if (rimg) rimg.src = "../../../images/prefresha.gif"/*tpa=http://www.huntsman.com/images/prefresha.gif*/
}
var _tmpImg = new Image();
function high(imgname) {
  var img = eval("document.images.i" + imgname);
  if (img) {
    _tmpImg.src = img.src;
    img.src = eval("high" + imgname + ".src");
  }
}
function low(imgname) {;
  var img = eval("document.images.i" + imgname);
  if (img) {
    img.src = _tmpImg.src;
  }
}
function folderpropertysheet(cornerid,siteid) {window.open("http://www.huntsman.com/portal/pls/portal/PORTAL.wwpob_property_ui.page_property_sheet?p_cornerid="+cornerid+"&p_siteid="+siteid,"propertysheet","scrollbars=1,resizable=1,width=500,height=500").focus();}
function propertysheet(thingid,masterthingid,cornerid,siteid) {window.open("http://www.huntsman.com/portal/pls/portal/PORTAL.wwpob_property_ui.item_property_sheet?p_thingid="+thingid+"&p_masterthingid="+masterthingid+"&p_cornerid="+cornerid+"&p_siteid="+siteid,"propertysheet","scrollbars=1,resizable=1,width=500,height=500").focus();}
function copyBody(portletReference) {
  if (document.styleSheets) {
    var rules, ruleslen;
    for (var s=0; s < document.styleSheets.length; s++) {
      if (document.styleSheets[s].cssRules)
        rules = document.styleSheets[s].cssRules;
      else
        rules = document.styleSheets[s].rules;
      ruleslen = (rules ? rules.length : 0);
      for (var r=0; r<ruleslen; r++) {
        if (parent.document.styleSheets[0].insertRule) {
          try {
            parent.document.styleSheets[0].insertRule(
              rules[r].selectorText + "{" + rules[r].style.cssText + "}",
              parent.document.styleSheets[0].cssRules.length);
          }
          catch (e) { // this is likely to happen on Apple Safari
            null;
            /* alert("exception - insertRule("
               + rules[r].selectorText + "{" + rules[r].style.cssText + "},"
               + parent.document.styleSheets[0].cssRules.length + ")"); */
          }
        }
        else {
          parent.document.styleSheets[0].addRule(
            rules[r].selectorText,
            rules[r].style.cssText ? rules[r].style.cssText : " ");
        }
      }
    }
  }
  var srcScripts = document.getElementsByTagName("script");
  var destHead   = parent.document.getElementsByTagName("head").item(0);
  for (var i=0; i<srcScripts.length; i++)
  {
      var newScript = parent.document.createElement("script");
      newScript.type = srcScripts.item(i).type;
      if (srcScripts.item(i).src)
        newScript.src  = srcScripts.item(i).src;
      newScript.text = srcScripts.item(i).text;
      destHead.appendChild(newScript);
  }
  parent.document.getElementById("p" + portletReference).innerHTML = document.body.innerHTML;
}


function removePortlet(pRef)
{
  if (confirm("Are you sure you want to remove the portlet from this page? You can reinstate this portlet by clicking on the Personalize Link on the Page."))
  {
    document.getElementById("p" + pRef).style.display = "none";
    var u = "http://www.huntsman.com/portal/pls/portal/PORTAL.wwpob_page_dialogs.remove_portlet_dlg" + "?p_portlet_ref=" + pRef;

    // Call the remove URL synchronously
    if (window.XMLHttpRequest) {    // W3C
      var req = new XMLHttpRequest();
      req.open("GET", u, false);
      req.send(null);
    }
    else if (window.ActiveXObject) {    // IE
      var req = new ActiveXObject("Microsoft.XMLHTTP");
      if (req) {
        req.open("GET", u, false);
        req.send();
      }
    }
  }
}

var restoreImg = null;
function collapsePortlet(pRef, pageUrl)
{
  if (document.getElementById)
  {
    // get the portlet contents (includes jpdk prefixing)
    var e = document.getElementById("pcnt" + pRef);
    // If null, try wsrp prefixing which is just a single p
    if(e == null)
    {
        e = document.getElementById("p" + pRef);
    }

    // determine the intended state by looking at the collapse/expand state
    var st = (e.style.display == "none" ? 0 : 1);

    // if collapsing, show the restore image, and dynamically hide content
    if (st == 1)
    {
      // the restore image is cached
      if (!restoreImg)
      {
        restoreImg = new Image();
        restoreImg.src = "../../../images/restore.gif"/*tpa=http://www.huntsman.com/images/restore.gif*/;
        restoreImg.alt = "Restore";
      }

      // hide the portlet content and show restore image
      e.style.display = "none";
      var cimg = document.getElementById("collapse_" + pRef);
      if (!cimg) cimg = document.images["collapse_" + pRef];
      if (cimg) { cimg.src = restoreImg.src; cimg.alt = restoreImg.alt; }
    }

    // This URL returns nothing, but updates the collapse/restore state
    var u = "http://www.huntsman.com/portal/pls/portal/PORTAL.wwpob_page_dialogs.collapse_portlet?p_portlet_ref=" + pRef + "&p_state=" + st;

    // Call the collapse/restore URL.  If restoring,
    // then do this synchronously (before refreshing).
    if (window.XMLHttpRequest) {    // W3C
      var req = new XMLHttpRequest();
      req.open("GET", u, st != 0);
      req.send(null);
    }
    else if (window.ActiveXObject) {    // IE
      var req = new ActiveXObject("Microsoft.XMLHTTP");
      if (req) {
        req.open("GET", u, st != 0);
        req.send();
      }
    }

    // Refresh contents if restoring from collapsed state
    if (st == 0)
      refreshPortlet(pRef, pageUrl);
  }
}
    

function iframePortletHead(page, iframe)
{
  copyStyleSheets(page, iframe);

  function copyStyleSheets(source, target)
  {
    var styleSheets = source.styleSheets;
    if (styleSheets)
    {
      for (var i = 0; i < styleSheets.length; i++)
      {
        if (styleSheets[i].ownerNode)      // W3C
        {
          // Copy the STYLE element straight into the HEAD
          var styleSheet = styleSheets[i].ownerNode.cloneNode(true);
          target.getElementsByTagName("HEAD").item(0).appendChild(styleSheet);
        } 
        else if (target.createStyleSheet)  // IE
        {
          if (styleSheets[i].href != "")
          {
            // Copy stylesheet reference
            target.createStyleSheet(styleSheets[i].href);
          }
          else
          {
            // Copy inline stylesheet manually
            var styleSheet = target.createStyleSheet();

            var ruleArray = styleSheets[i].rules;
            for (var r = 0; r < ruleArray.length; r++)
            {
              var text;
              if (ruleArray[r].style.cssText)
              {
                text = ruleArray[r].style.cssText;
              }
              styleSheet.addRule(ruleArray[r].selectorText, text);
            } 
          }
        } 
      }
    }
  }
}


function iframePortletAfterContent(title, portletReference, isSuccess)
{
  changePortletTitle(parent.document, portletReference, title);

  if (isSuccess) 
  {
    setTitleElementsDisplay(portletReference, "none", new Array("portletTitle_temp_"));
    setTitleElementsDisplay(portletReference, "inline" /*, default elements */);
  }

  var pla = document.createAttribute("portlet-loaded");
  pla.nodeValue = isSuccess ? "success" : "error";
  document.body.setAttributeNode(pla);
}

function changePortletTitle(doc, portletReference, title)
{
  var t = doc.getElementById("portletTitle_" + portletReference);
  if (t) t.innerHTML = title;
     
  // Update temporary title since in case of an error the actual title span
  // will not be revealed
  var tt = doc.getElementById("portletTitle_temp_" + portletReference);
  if (tt) tt.innerHTML = title;
}

function setTitleElementsDisplay(portletReference, state, prefixes)
{
  if (typeof prefixes == "undefined")
  {
    // Default set of title elements
    prefixes = new Array("title_",
                         "actions_", 
                         "collapse_span_",
                         "collapse_spacer_",
                         "remove_span_",
                         "remove_spacer_");
  }

  for (p in prefixes)
  {
    var e = parent.document.getElementById(prefixes[p] + portletReference);
    if (e) e.style.display = state;
  }
}

function iframePortletOnload(iframeElement, portletReference)
{
  var status = iframeElement.contentWindow.document.body.getAttribute("portlet-loaded");
  
  if (status === "success")
  {
    // iframe contains good portlet markup from the PPE 
  }
  else if (status === "error")
  {
    // iframe contains a portlet error from the PPE
    if (retryPortlet(iframeElement))
    {
      return;
    }
  }
  else 
  {
    // iframe contains non-portlet markup!
    if (retryPortlet(iframeElement))
    {
      return;
    }
    // if retry limit is reached then display an error message
    // in place of whatever ended up in the frame
    renderIframeError(portletReference, iframeElement);
  }

  // Reset retry counter in case the portlet is refreshed
  iframeElement.removeAttribute("retry-count"); 

  try
  {
    modifyStyleSheets(iframeElement.contentWindow.document);
    fixIframeTargets(iframeElement);
    resizePortlet(iframeElement);
  }
  finally
  {
    // Whatever happens, stop the refresh icon from spinning
    stopRefreshIcon(portletReference);
  }
 
  function retryPortlet(iframeElement)
  { 
    var RETRY_LIMIT = 1;

    var retries = iframeElement.getAttribute("retry-count");
    if (retries == null) retries = 0;
    if (retries < RETRY_LIMIT)
    {
      changePortletTitle(document, portletReference, "Retrying ...");
  
      var retriesAttr = document.createAttribute("retry-count");
      retriesAttr.nodeValue = parseInt(retries) + 1;
      iframeElement.setAttributeNode(retriesAttr);

      iframeElement.src = iframeElement.src;  // reload frame

      return true;
    }
    else
    {
      return false;
    }
  } 

  function renderIframeError(portletReference, iframeElement)
  {
    changePortletTitle(document, portletReference, "Error Message");

    iframeElement.contentWindow.document.body.innerHTML = 
      "<div class='PortletText1'>Error: Cannot get portlet markup. Try reloading the portlet or the page.</div>";
  }

  function modifyStyleSheets(doc)
  {
    var styleSheets = doc.styleSheets;				
    if (styleSheets)
    {
      // Get number of pixels in one em in the given document
      var emSize = getEmSize(doc.body);
      for (var i = 0; i < styleSheets.length; i++)
      {
        try
        {
          var ruleArray = styleSheets[i].rules || styleSheets[i].cssRules;
          for (var r = 0; r < ruleArray.length; r++)
          {   
            // Filter out non-WSRP CSS classes
            if (ruleArray[r].selectorText.indexOf(".portlet-") == 0)
            {
              var s = ruleArray[r].style;
              if (s.fontSize)
              {
                var ems = s.fontSize;
                if (ems.indexOf("em") != -1)
                {
                  ems = ems.substring(0, ems.length - 2);
                  s.fontSize = Math.round(emSize * ems) + "px";
                }
              }
            }
          }
        }
        catch (e)
        {
          // Move onto the next stylesheet
        }
      }
    }
  }

  function getEmSize(el)
  {
    // Calculate the size of one em in pixels in the context of the given element
    var tempDiv = el.ownerDocument.createElement("div");
    tempDiv.style.height = "1em";
    el.appendChild(tempDiv);
    var px = tempDiv.offsetHeight;
    el.removeChild(tempDiv);
    return px;
  }

  function fixIframeTargets(iframeElement)
  {
    var frameDocument = iframeElement.contentWindow.document;  

    var links = frameDocument.getElementsByTagName("a");
    for (var l = 0; l < links.length; l++) 
    { 
      var link = links[l];
      if (!link.target && link.href && 
          link.protocol && (link.protocol == "http:" || link.protocol == "https:"))
      {
        link.target = "_top";
      }
    }

    var forms = frameDocument.getElementsByTagName("form");
    for (var f = 0; f < forms.length; f++) 
    { 
      if (!forms[f].target)
      {
        forms[f].target = "_top";
      }
    }
  }

  function resizePortlet(iframeElement)
  {
    resizeIframeHeight(iframeElement);
  }

  function resizeIframeHeight(iframeElement)
  {
    var iframeWindow = iframeElement.contentWindow;  

    // Special case for ADF portlets running in full screen modes
    if (getParameter(location.search, "_type") == "portlet" &&
        elementUsesClass(iframeWindow.document.body, "p_AFMaximized"))
    {
      iframeElement.style.height = "0px";
        
      // Add a temporary empty div to the bottom of the body so we
      // can work out how much space is used without the portlet
      var x = document.createElement("div");
      document.body.appendChild(x);

      var availableHeight = document.body.clientHeight;
      // Calculate height required for decoration based on where the temporary
      // div is located and add bottom margin and padding
      var decorationHeight = x.offsetTop + 
                             getPixels(getStyle(document.body, "padding-bottom")) +
                             getPixels(getStyle(document.body, "margin-bottom"));
      var portletHeight = Math.max(0, availableHeight - decorationHeight);

      document.body.removeChild(x);

      iframeElement.style.height = portletHeight + "px"; 
    }
    else
    {
      iframeElement.style.height = getScrollHeight(iframeWindow.document) + "px";
    }
  }

  function getScrollHeight(doc)
  {
    // Cross browser method to get scroll height of a document
    var ff = doc.documentElement.scrollHeight;
    return ff != 0 ? ff : doc.body.scrollHeight;
  }

  function getStyle(el, styleProp)
  {
    // Cross browser method to get style of an element
    if (el.currentStyle)
    {
      // IE - convert to camel case style name first
      var re = /-([a-z])/;
      while(re.test(styleProp)) 
      {
        styleProp = styleProp.replace(re, RegExp.$1.toUpperCase());  
      }
      var y = el.currentStyle[styleProp];
    }
    else if (window.getComputedStyle)
    {
      var y = document.defaultView.getComputedStyle(el, null).getPropertyValue(styleProp);
    }
    return y;
  }

  function getPixels(s)
  {
    // Return 0 if this is not a pixel measurement
    return (s && s.indexOf("px") != -1 ) ? Number(s.substring(0, s.indexOf("px"))) : 0;
  }

  function stopRefreshIcon(portletReference)
  {
    var rimg = parent.document.getElementById("refresh_" + portletReference);
    if (rimg) rimg.src="../../../images/prefresh.gif"/*tpa=http://www.huntsman.com/images/prefresh.gif*/;
  }

  function elementUsesClass(element, cn)
  {
    var classNames = element.className;
    return classNames && classNames.length > 0 && 
           (classNames == cn ||
            new RegExp("(^|\\s)" + cn + "(\\s|$)").test(classNames));
  }

}


function iframePortletRefresh(portletReference)
{
  animateRefreshIcon(portletReference);
  setTitleElementsDisplay(portletReference, "none");
  setTitleElementsDisplay(portletReference, "inline", new Array("portletTitle_temp_"));
  var iframeElement = document.getElementById("portletIFrame_" + portletReference);
  if (iframeElement)
  {
    iframeElement.style.height = "0px"; 
    iframeElement.src = iframeElement.src;
  }
  else
  {  
    location.href = location.href;
  }
}
function show_context_help(h) {window.open(h,"Help","menubar=1,toolbar=1,scrollbars=1,resizable=1,width=700,height=500").focus();}
