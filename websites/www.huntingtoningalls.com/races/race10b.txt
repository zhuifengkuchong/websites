<script id = "race10b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race10={};
	myVars.races.race10.varName="Window[26].qm_la";
	myVars.races.race10.varType="@varType@";
	myVars.races.race10.repairType = "@RepairType";
	myVars.races.race10.event1={};
	myVars.races.race10.event2={};
	myVars.races.race10.event1.id = "Lu_Id_a_21";
	myVars.races.race10.event1.type = "onmouseover";
	myVars.races.race10.event1.loc = "Lu_Id_a_21_LOC";
	myVars.races.race10.event1.isRead = "True";
	myVars.races.race10.event1.eventType = "@event1EventType@";
	myVars.races.race10.event2.id = "Lu_Id_a_20";
	myVars.races.race10.event2.type = "onmouseover";
	myVars.races.race10.event2.loc = "Lu_Id_a_20_LOC";
	myVars.races.race10.event2.isRead = "False";
	myVars.races.race10.event2.eventType = "@event2EventType@";
	myVars.races.race10.event1.executed= false;// true to disable, false to enable
	myVars.races.race10.event2.executed= false;// true to disable, false to enable
</script>

