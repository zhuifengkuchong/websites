(function ($, window) {

    $.fn.irBar = function (options, callback) {
        "use strict";

        var defaults = {
            url: 'http://franklin.q4web.com/',
            key: '675D693233B6485F8B80CC9AF69C0163',
            items: 3
        }

        var o = $.extend({}, defaults, options),
        element = this,
        module = {

            _init: function () {
                module.getEventFeed(); 
            },

            formatDate: function (date){
                var d = date.split(' ').shift().split('/'),
                    m = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                return m[parseInt(d[0]-1)]+' '+d[1]+', '+d[2];
            },

            getEventFeed: function(){
                var _ = this;

                $.getJSON( o.url + '/feed/PressRelease.svc/GetPressReleaseList?apiKey='+o.key+'&pressReleaseDateFilter=1&bodyType=0&pageSize='+ o.items + '&callback=?',  function( data ) {
                    element.html(_.parseNews(data.GetPressReleaseListResult));
                });
            },

            parseNews: function(news){
                var _ = this,
                    newsItems = '';

                $.each(news, function(i, release){
                    newsItems += '<li><a href="'+ o.url + '/' + release.LinkToDetailPage +'">'+ release.Headline +'</a> <span class="date">('+ _.formatDate( release.PressReleaseDate ) +')</span></li>'
                });

                return newsItems;
            }
        };

        module._init();
    };

})(jQuery, window);