<script id = "race10a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race10={};
	myVars.races.race10.varName="Tree[0x7f89880ec458]:Lu_Id_meta_4";
	myVars.races.race10.varType="@varType@";
	myVars.races.race10.repairType = "@RepairType";
	myVars.races.race10.event1={};
	myVars.races.race10.event2={};
	myVars.races.race10.event1.id = "Lu_Id_meta_4";
	myVars.races.race10.event1.type = "Lu_Id_meta_4__parsed";
	myVars.races.race10.event1.loc = "Lu_Id_meta_4_LOC";
	myVars.races.race10.event1.isRead = "False";
	myVars.races.race10.event1.eventType = "@event1EventType@";
	myVars.races.race10.event2.id = "Lu_Id_meta_4";
	myVars.races.race10.event2.type = "Lu_Id_meta_4__parsed";
	myVars.races.race10.event2.loc = "Lu_Id_meta_4_LOC";
	myVars.races.race10.event2.isRead = "False";
	myVars.races.race10.event2.eventType = "@event2EventType@";
	myVars.races.race10.event1.executed= false;// true to disable, false to enable
	myVars.races.race10.event2.executed= false;// true to disable, false to enable
</script>

