//	Date:		08/3/2008
// Description: 	These functions are used to add to the web site navigation, validation, breadcrumbs, pagetitles, windows, etc.


	function openBrowser(path, wide, high)
	{
		var x = screen.availWidth-wide-20;
		var topRightWin = window.open(path,'infoWin', 'left='+x+',screenX='+x+',top=10, screenY=10,height='+high+',width='+wide+',scrollbars=yes,resizable=yes,menubar=no,toolbar=no,location=0,status=1');
		topRightWin.focus();
	}


/* Pre load images */

if (document.images) {


    top1 = new Image(793,84);
    top1.src = "../../images/searsholdingscorp.jpg"/*tpa=http://www.searsholdings.com/images/searsholdingscorp.jpg*/;

    cartopright = new Image(452,115);
    cartopright.src = "../../images/careers_top_right2.jpg"/*tpa=http://www.searsholdings.com/images/careers_top_right2.jpg*/;

    store1 = new Image(171,115);
    store1.src = "../../images/careers/store.jpg"/*tpa=http://www.searsholdings.com/images/careers/store.jpg*/;

    store2 = new Image(171,115);
    store2.src = "../../images/careers/store-cust.jpg"/*tpa=http://www.searsholdings.com/images/careers/store-cust.jpg*/;
}


/*	Read the page title from the document title and display it.  This is an a method with an intent to increase flexibility to
	displaying the title considering that the */

function title()
{
	if (document.title.length < 5) {
		document.title = "Sears Holdings Talent Acquisition";
		return document.title;
	}
	return document.title;
}

function arrow()
{
	var ie=document.all

	if (ie) {
		return "<span style='FONT-SIZE: 11pt; FONT-FAMILY: Webdings; color: #DDE8F4;'>6</span>";
	}
	return "";
}


/* Expand / Collapse Text */
function expander(obj) {
	var el = document.getElementById(obj);
	if ( el.style.display != 'none' ) {
		el.style.display = 'none';
	} else {
		el.style.display = '';  }
}


		/* Breadcrumbs */

function relpath( linklength ){
	var cd = "../";
	var dir =[ cd ];

	for ( j = 1; j < linklength; j++ ) {
		dir [j] = cd + dir[ j - 1 ];
	}
	return true? dir.reverse() : dir();
}


function breadcrumbs(){

	var link = location.toString();
	var crumb = "";

	//link=link.substring( link.indexOf("/") + 1 );
	link=link.substring( link.indexOf("//") + 1 );

	link=link.split("/");

	var href = relpath( link.length );

	href [link.length] = "";

        var text = "";
        var url = "";

			// form the crumbs

	for ( i = 1; i < link.length-1; i++ ) {
		url = href[ i + 2];
		text = unescape(link[i]);
		if ( i > 1) {
        	text = text.substring(0, 1).toUpperCase() + text.substring( 1, text.length );
        }
		crumb += ("&nbsp;<a class=rnav-text-link href=" + url + '>' + text + "</a>  > ")
	}
	crumb += "&nbsp;" + document.title;
	crumb = crumb.replace(eval("/"+location.host+"/"), "Home")

return crumb
}

function swapImage(name,image) {
    document.images[name].src = image;
}



var myFont = '<link href="../css/sh.css"/*tpa=http://www.searsholdings.com/include/css/sh.css*/ rel="stylesheet" type="text/css">';
var myLargerFont = '<link href="../css/sh_large.css"/*tpa=http://www.searsholdings.com/include/css/sh_large.css*/ rel="stylesheet" type="text/css">';

if ( getStyle("fontSize") == "large" ) {
	myFont = myLargerFont;
}

document.write(myFont);

function getStyle(name) {

    var start = document.cookie.indexOf(name+"=");
    var len = start+name.length + 1;
    if ((!start) && (name != document.cookie.substring(0,name.length)))
    	return null;
    if (start == -1)
    	return null;
    var end = document.cookie.indexOf(";",len);
    if (end == -1)
    	end = document.cookie.length;

    return unescape(document.cookie.substring(len,end));
}

function setStyle(value) {
	var name = "fontSize";
	var path = "/";
	var domain = location.host;
	var secure;
	var today = new Date();
	var expires = new Date(today.getTime() + (56 * 86400000));

    document.cookie = name + "=" +escape(value) +
        ( (expires) ? ";expires=" + expires.toGMTString() : "") +
        ( (path) ? ";path=" + path : "") +
        ( (domain) ? ";domain=" + domain : "") +
        ( (secure) ? ";secure" : "");
    window.location.reload();
}


function helper () {
	var small = '<a  class="right-nav" href="javascript:void(\'\')" onClick="setStyle(\'small\')" class="toplink_my">Smaller Text</a> ';

	var larger = '<a  class="right-nav" href="javascript:void(\'\')" onClick="setStyle(\'large\')" class="toplink_my">Larger Text</a> ';

	if ( getStyle("fontSize") == "small" || getStyle("fontSize") == null ) { small = larger; }

		else if ( getStyle("fontSize") == "large" ) { larger = small; }

	document.write(small);

	//if (document.all) //only for IE
	//document.write('<span class="gray1">&nbsp;|&nbsp;</span> <a href="javascript:printFriendly();" class="small">printer friendly</a>')
}


//		Opens external site windows without location bar and
//		eliminates other features to keep visitor focus.
//		It's a 'Common Window' eliminating multiple pop ups
//		Window opens in the upper right corner of the browser.

	function comWin(site) {

		var xWidth = document.body.clientWidth;
		var yHeight = 600;
		var x = window.screenLeft + document.body.clientWidth - xWidth + 40;
		var y = window.screenTop;

		if (site == null ) {  // gives this method the ability to call it with or wihout a url param
			site = "../../index.htm"/*tpa=http://www.searsholdings.com/*/
		}

		var xWin = window.open(site, "x" , "width="+xWidth+  ",height="+yHeight+ ",top="+y+  ",left="+x + ", toolbar=yes,location=yes,status=no,menubar=yes,scrollbars=yes,resizable=yes");
		xWin.focus();
	}

	function confirmation(url) {
	  if (confirm("Are you sure you want to delete?")) {
		 location.href = url;
	  } else {return false; }
	}

	//The print friendly window opens up within the browser
	//window aligned with the browsers Right most and Top most edge.
		function printFriendly() {

		var xWidth = 500;
		var yHeight = 500;
		var x = window.screenLeft + document.body.clientWidth - xWidth + 10;
		var y = window.screenTop;

		var printWin = window.open("", "printSpecial" , "width="+xWidth+  ",height="+yHeight+ ",top="+y+  ",left="+x + ", toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes");
		printWin.document.open();
		printWin.document.write("<head><link href='../css/sh.css'/*tpa=http://www.searsholdings.com/include/css/sh.css*/ rel='stylesheet' type='text/css'><style>.no-print{display:none;}</style></head>");
		printWin.document.write('<p align=right><a href="javascript:window.print()">Print</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:window.close()">Close Window</a>&nbsp;&nbsp;&nbsp;');


		printWin.document.write(friendly.innerHTML);
		printWin.document.close();
		printWin.focus();

		//if (window.print){
		//	printWin.print();
		//}
	}

			//JOBS
			var jobs=new Array()
			jobs[0]='<a href="http://www.searsholdings.com/careers/jobs/entrylevel_hourly.htm" class="careers_nav_ddm">Entry-level & Hourly Jobs</a>'
			jobs[1]='<a href="http://www.searsholdings.com/careers/jobs/professional.htm" class="careers_nav_ddm">Professional & Salaried Jobs</a>'

			//AREAS of OPPORTUNITY
			var opportunity=new Array()
			opportunity[0]='<a href="http://www.searsholdings.com/careers/opportunities/stores/" class="careers_nav_ddm">Stores</a>'
			opportunity[1]='<a href="http://www.searsholdings.com/careers/opportunities/pharmacy/" class="careers_nav_ddm">Pharmacy</a>'
			opportunity[2]='<a href="http://www.searsholdings.com/careers/opportunities/supplychain_logistics.htm" class="careers_nav_ddm">Supply Chain & Logistics</a>'
			opportunity[3]='<a href="http://www.searsholdings.com/careers/opportunities/home%20services/" class="careers_nav_ddm">Home Services</a>'
			opportunity[4]='<a href="http://www.searsholdings.com/careers/opportunities/ecommerce.htm" class="careers_nav_ddm">eCommerce</a>'
			opportunity[5]='<a href="http://www.searsholdings.com/careers/opportunities/corporate.htm" class="careers_nav_ddm">Corporate</a>'
			opportunity[6]='<a href="http://www.searsholdings.com/careers/college/" class="careers_nav_ddm">College Programs</a>'
			opportunity[7]='<a href="http://www.searsholdings.com/careers/opportunities/military/" class="careers_nav_ddm">Military Programs</a>'
			opportunity[8]='<a href="javascript:comWin(\'http://www.searsholdings.com/business/\');" class="careers_nav_ddm">Business Opportunities</a>'
			opportunity[9]='<a href="http://www.searsholdings.com/careers/opportunities/current-associates.htm" class="careers_nav_ddm">Current Associates</a>'

			//COLLEGE PROGRAMS
			var college=new Array()
			college[0]='<a href="http://www.searsholdings.com/careers/jobs/MBA/" class="careers_nav_ddm">MBA Graduate</a>'
			college[1]='<a href="http://www.searsholdings.com/careers/college/undergraduate.htm" class="careers_nav_ddm">Undergraduate</a>'
			college[2]='<a href="http://www.searsholdings.com/careers/opportunities/pharmacy/" class="careers_nav_ddm">Pharmacy</a>'
			college[3]='<a href="http://www.searsholdings.com/careers/college/events/index.jsp" class="careers_nav_ddm">Events Calendar</a>'
			college[4]='<a href="http://www.searsholdings.com/careers/college/college-staffing.htm" class="careers_nav_ddm">Staffing Process</a>'

			//ABOUT US
			var about=new Array()
			about[0]='<a href="http://www.searsholdings.com/careers/about/faqs.htm" class="careers_nav_ddm">FAQs</a>'
			about[1]='<a href="http://www.searsholdings.com/careers/events/schedule/index.jsp" class="careers_nav_ddm">Events Calendar</a>'
			about[2]='<a href="http://www.searsholdings.com/careers/about/staffing.htm" class="careers_nav_ddm">Staffing Process</a>'
			about[3]='<a href="http://www.searsholdings.com/careers/about/locations.htm" class="careers_nav_ddm">Locations</a>'
			//about[4]='<a href="http://www.searsholdings.com/careers/opportunities/" class="careers_nav_ddm">Business Formats</a>'
			about[4]='<a href="http://www.searsholdings.com/careers/about/brands/" class="careers_nav_ddm">Brands</a>'

			//WHY US
			var whyus=new Array()
			whyus[0]='<a href="http://www.searsholdings.com/careers/why us/benefits.htm" class="careers_nav_ddm">Our Benefits</a>'
			whyus[1]='<a href="http://www.searsholdings.com/careers/why us/diversity/" class="careers_nav_ddm">Our Commitment to Diversity</a>'
			whyus[2]='<a href="javascript:comWin(\'http://www.searsholdings.com/communityrelations/\');" class="careers_nav_ddm">Our Community Involvement</a>'
			whyus[3]='<a href="http://www.searsholdings.com/careers/opportunities/military/" class="careers_nav_ddm">Our Commitment to the Military</a>'
			whyus[4]='<a href="http://www.searsholdings.com/careers/why us/culture.htm" class="careers_nav_ddm">Our Culture</a>'
			whyus[5]='<a href="http://www.searsholdings.com/careers/why us/recognition.htm" class="careers_nav_ddm">Our Recognition</a>'
			whyus[6]='<a href="http://www.searsholdings.com/careers/why us/history.htm" class="careers_nav_ddm">Our History</a>'

			//EVENTS
			var events=new Array()
			events[0]='<a href="http://www.searsholdings.com/careers/events/schedule/index.jsp" class="careers_nav_ddm">General Events</a>'
			events[1]='<a href="http://www.searsholdings.com/careers/college/events/index.jsp" class="careers_nav_ddm">College Events</a>'

			//SHOP ONLINE
			var shop=new Array()
			shop[0]='<a href="http://www.sears.com/" class="grn-nv-top2-shop">Sears.com</a>'
			shop[1]='<a href="http://www.kmart.com/" class="grn-nv-top2-shop">Kmart.com</a>'
			shop[2]='<a href="http://www.landsend.com/" class="grn-nv-top2-shop">LandsEnd.com</a>'
			shop[3]='<a href="http://www.thegreatindoors.com/" class="grn-nv-top2-shop">TheGreatIndoors.com</a>'
			shop[4]='<a href="http://www.osh.com/" class="grn-nv-top2-shop">OSH.com</a>'
			shop[5]='<a href="http://www.mygofer.com/" class="grn-nv-top2-shop">mygofer.com</a>'
			shop[6]='<a href="http://www.searshomeservices.com/" class="grn-nv-top2-shop">SearsHomeServices.com</a>'
			shop[7]='<a href="http://www.servicelive.com/" class="grn-nv-top2-shop">ServiceLive.com</a>'
			shop[8]='<a href="http://www.searsoutlet.com/" class="grn-nv-top2-shop">SearsOutlet.com</a>'
			shop[9]='<a href="http://www.searspartsdirect.com/" class="grn-nv-top2-shop">SearsPartsDirect.com</a>'

			var menuwidth="138px" // default menu width
			var submenuwidth="138px" // default menu width

			var ie=document.all
			var ns=document.getElementById&&!document.all
			if (ie||ns)
				document.write('<div id="ddprops" style="visibility:hidden;width:'+menuwidth+';background-color:'+"#4a4a4a"+'" onMouseover="clearMenu()" onMouseout="autoHide(event)"></div>')

			function dropDown(obj, e, menucontents, menuwidth){
				if (window.event)
					window.event.cancelBubble=true
				else if (e.stopPropagation) e.stopPropagation()
					clearMenu()
				objDropMenu=document.getElementById? document.getElementById("ddprops") : ddprops
				createContent(menucontents)

				if (ie||ns){
					showHide(objDropMenu.style, e, menuwidth, "hidden")
					objDropMenu.x=getOffset(obj, "left")
					objDropMenu.y=getOffset(obj, "top")
					objDropMenu.style.left=objDropMenu.x-clearBroserEdges(obj, "rightedge")+"px"
					objDropMenu.style.top=objDropMenu.y-clearBroserEdges(obj, "bottomedge")+obj.offsetHeight+"px"
				}

			}

			function showHide(obj, e, menuwidth, hidden){
				if (ie||ns)
					objDropMenu.style.left=objDropMenu.style.top="-499px"
				if (menuwidth!=""){
					objDropMenu.widthobj=objDropMenu.style
					objDropMenu.widthobj.width=menuwidth
				}
				if (e.type=="click" && obj.visibility==hidden || e.type=="mouseover")
					obj.visibility="visible"
				else if (e.type=="click")
					obj.visibility=hidden
			}

			function getOffset(what, offsettype){ // get position offset
				var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
				var parentEl=what.offsetParent;
				while (parentEl!=null){
					totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
					parentEl=parentEl.offsetParent;
				}
				return totaloffset;
			}

			function checkIECompat(){
				return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
			}

			function clearBroserEdges(obj, edge){ // clear browser edge
				var edgeoffset=0
				if (edge=="rightedge"){
					var windowedge=ie && !window.opera? checkIECompat().scrollLeft+checkIECompat().clientWidth-15 : window.pageXOffset+window.innerWidth-15
					objDropMenu.contentmeasure=objDropMenu.offsetWidth
					if (windowedge-objDropMenu.x < objDropMenu.contentmeasure)
						edgeoffset=objDropMenu.contentmeasure-obj.offsetWidth
				} else {
				var topedge=ie && !window.opera? checkIECompat().scrollTop : window.pageYOffset
				var windowedge=ie && !window.opera? checkIECompat().scrollTop+checkIECompat().clientHeight-15 : window.pageYOffset+window.innerHeight-18
				objDropMenu.contentmeasure=objDropMenu.offsetHeight
				if (windowedge-objDropMenu.y < objDropMenu.contentmeasure){
					edgeoffset=objDropMenu.contentmeasure+obj.offsetHeight
					if ((objDropMenu.y-topedge)<objDropMenu.contentmeasure)
						edgeoffset=objDropMenu.y+obj.offsetHeight-topedge
				}
			}
			return edgeoffset
			}

			function createContent(menucontent){
				if (ie||ns)
				objDropMenu.innerHTML=menucontent.join("")
			}

			function autoHide(e){
				if (ie&&!objDropMenu.contains(e.toElement))
					hideMenu()
				else if (ns&&e.currentTarget!= e.relatedTarget)
					hideMenu()
			}

			function hide(e){
				if (typeof objDropMenu!="undefined"){
					if (ie||ns)
					objDropMenu.style.visibility="hidden"
				}
			}

			function hideMenu(){
				if (ie||ns)
				delayhide=setTimeout("hide()",100) //hide in 100 miliseconds
			}

			function clearMenu(){
				if (typeof delayhide!="undefined")
					clearTimeout(delayhide)
			}

				document.onclick=hide