<script id = "race149b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race149={};
	myVars.races.race149.varName="Object[2078].uuid";
	myVars.races.race149.varType="@varType@";
	myVars.races.race149.repairType = "@RepairType";
	myVars.races.race149.event1={};
	myVars.races.race149.event2={};
	myVars.races.race149.event1.id = "analytics-separator";
	myVars.races.race149.event1.type = "onkeypress";
	myVars.races.race149.event1.loc = "analytics-separator_LOC";
	myVars.races.race149.event1.isRead = "True";
	myVars.races.race149.event1.eventType = "@event1EventType@";
	myVars.races.race149.event2.id = "ctc-booking-pace";
	myVars.races.race149.event2.type = "onkeypress";
	myVars.races.race149.event2.loc = "ctc-booking-pace_LOC";
	myVars.races.race149.event2.isRead = "False";
	myVars.races.race149.event2.eventType = "@event2EventType@";
	myVars.races.race149.event1.executed= false;// true to disable, false to enable
	myVars.races.race149.event2.executed= false;// true to disable, false to enable
</script>

