<script id = "race220b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race220={};
	myVars.races.race220.varName="Object[2078].uuid";
	myVars.races.race220.varType="@varType@";
	myVars.races.race220.repairType = "@RepairType";
	myVars.races.race220.event1={};
	myVars.races.race220.event2={};
	myVars.races.race220.event1.id = "corpCodeBlankMessage";
	myVars.races.race220.event1.type = "onkeypress";
	myVars.races.race220.event1.loc = "corpCodeBlankMessage_LOC";
	myVars.races.race220.event1.isRead = "True";
	myVars.races.race220.event1.eventType = "@event1EventType@";
	myVars.races.race220.event2.id = "awardTypeBlankMessage";
	myVars.races.race220.event2.type = "onkeypress";
	myVars.races.race220.event2.loc = "awardTypeBlankMessage_LOC";
	myVars.races.race220.event2.isRead = "False";
	myVars.races.race220.event2.eventType = "@event2EventType@";
	myVars.races.race220.event1.executed= false;// true to disable, false to enable
	myVars.races.race220.event2.executed= false;// true to disable, false to enable
</script>

