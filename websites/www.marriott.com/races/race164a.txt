<script id = "race164a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race164={};
	myVars.races.race164.varName="Object[2078].uuid";
	myVars.races.race164.varType="@varType@";
	myVars.races.race164.repairType = "@RepairType";
	myVars.races.race164.event1={};
	myVars.races.race164.event2={};
	myVars.races.race164.event1.id = "brand-ri";
	myVars.races.race164.event1.type = "onkeypress";
	myVars.races.race164.event1.loc = "brand-ri_LOC";
	myVars.races.race164.event1.isRead = "False";
	myVars.races.race164.event1.eventType = "@event1EventType@";
	myVars.races.race164.event2.id = "brand-sh";
	myVars.races.race164.event2.type = "onkeypress";
	myVars.races.race164.event2.loc = "brand-sh_LOC";
	myVars.races.race164.event2.isRead = "True";
	myVars.races.race164.event2.eventType = "@event2EventType@";
	myVars.races.race164.event1.executed= false;// true to disable, false to enable
	myVars.races.race164.event2.executed= false;// true to disable, false to enable
</script>

