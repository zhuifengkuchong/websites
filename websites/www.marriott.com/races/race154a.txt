<script id = "race154a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race154={};
	myVars.races.race154.varName="Object[2078].uuid";
	myVars.races.race154.varType="@varType@";
	myVars.races.race154.repairType = "@RepairType";
	myVars.races.race154.event1={};
	myVars.races.race154.event2={};
	myVars.races.race154.event1.id = "analytic-configuration";
	myVars.races.race154.event1.type = "onkeypress";
	myVars.races.race154.event1.loc = "analytic-configuration_LOC";
	myVars.races.race154.event1.isRead = "False";
	myVars.races.race154.event1.eventType = "@event1EventType@";
	myVars.races.race154.event2.id = "my-account-password";
	myVars.races.race154.event2.type = "onkeypress";
	myVars.races.race154.event2.loc = "my-account-password_LOC";
	myVars.races.race154.event2.isRead = "True";
	myVars.races.race154.event2.eventType = "@event2EventType@";
	myVars.races.race154.event1.executed= false;// true to disable, false to enable
	myVars.races.race154.event2.executed= false;// true to disable, false to enable
</script>

