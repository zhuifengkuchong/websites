<script id = "race104b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race104={};
	myVars.races.race104.varName="checkInDate__onkeypress";
	myVars.races.race104.varType="@varType@";
	myVars.races.race104.repairType = "@RepairType";
	myVars.races.race104.event1={};
	myVars.races.race104.event2={};
	myVars.races.race104.event1.id = "checkInDate";
	myVars.races.race104.event1.type = "onkeypress";
	myVars.races.race104.event1.loc = "checkInDate_LOC";
	myVars.races.race104.event1.isRead = "True";
	myVars.races.race104.event1.eventType = "@event1EventType@";
	myVars.races.race104.event2.id = "Lu_DOM";
	myVars.races.race104.event2.type = "onDOMContentLoaded";
	myVars.races.race104.event2.loc = "Lu_DOM_LOC";
	myVars.races.race104.event2.isRead = "False";
	myVars.races.race104.event2.eventType = "@event2EventType@";
	myVars.races.race104.event1.executed= false;// true to disable, false to enable
	myVars.races.race104.event2.executed= false;// true to disable, false to enable
</script>

