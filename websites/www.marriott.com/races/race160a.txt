<script id = "race160a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race160={};
	myVars.races.race160.varName="Object[2078].uuid";
	myVars.races.race160.varType="@varType@";
	myVars.races.race160.repairType = "@RepairType";
	myVars.races.race160.event1={};
	myVars.races.race160.event2={};
	myVars.races.race160.event1.id = "brand-et";
	myVars.races.race160.event1.type = "onkeypress";
	myVars.races.race160.event1.loc = "brand-et_LOC";
	myVars.races.race160.event1.isRead = "False";
	myVars.races.race160.event1.eventType = "@event1EventType@";
	myVars.races.race160.event2.id = "brand-mvci";
	myVars.races.race160.event2.type = "onkeypress";
	myVars.races.race160.event2.loc = "brand-mvci_LOC";
	myVars.races.race160.event2.isRead = "True";
	myVars.races.race160.event2.eventType = "@event2EventType@";
	myVars.races.race160.event1.executed= false;// true to disable, false to enable
	myVars.races.race160.event2.executed= false;// true to disable, false to enable
</script>

