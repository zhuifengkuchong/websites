<script id = "race118b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race118={};
	myVars.races.race118.varName="header-my-account-sign-in__onchange";
	myVars.races.race118.varType="@varType@";
	myVars.races.race118.repairType = "@RepairType";
	myVars.races.race118.event1={};
	myVars.races.race118.event2={};
	myVars.races.race118.event1.id = "header-my-account-sign-in";
	myVars.races.race118.event1.type = "onchange";
	myVars.races.race118.event1.loc = "header-my-account-sign-in_LOC";
	myVars.races.race118.event1.isRead = "True";
	myVars.races.race118.event1.eventType = "@event1EventType@";
	myVars.races.race118.event2.id = "Lu_DOM";
	myVars.races.race118.event2.type = "onDOMContentLoaded";
	myVars.races.race118.event2.loc = "Lu_DOM_LOC";
	myVars.races.race118.event2.isRead = "False";
	myVars.races.race118.event2.eventType = "@event2EventType@";
	myVars.races.race118.event1.executed= false;// true to disable, false to enable
	myVars.races.race118.event2.executed= false;// true to disable, false to enable
</script>

