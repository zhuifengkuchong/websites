<script id = "race147a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race147={};
	myVars.races.race147.varName="Object[2078].uuid";
	myVars.races.race147.varType="@varType@";
	myVars.races.race147.repairType = "@RepairType";
	myVars.races.race147.event1={};
	myVars.races.race147.event2={};
	myVars.races.race147.event1.id = "Lu_Id_input_33";
	myVars.races.race147.event1.type = "onkeypress";
	myVars.races.race147.event1.loc = "Lu_Id_input_33_LOC";
	myVars.races.race147.event1.isRead = "False";
	myVars.races.race147.event1.eventType = "@event1EventType@";
	myVars.races.race147.event2.id = "ctc-stay-total";
	myVars.races.race147.event2.type = "onkeypress";
	myVars.races.race147.event2.loc = "ctc-stay-total_LOC";
	myVars.races.race147.event2.isRead = "True";
	myVars.races.race147.event2.eventType = "@event2EventType@";
	myVars.races.race147.event1.executed= false;// true to disable, false to enable
	myVars.races.race147.event2.executed= false;// true to disable, false to enable
</script>

