<script id = "race156a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race156={};
	myVars.races.race156.varName="Object[2078].uuid";
	myVars.races.race156.varType="@varType@";
	myVars.races.race156.repairType = "@RepairType";
	myVars.races.race156.event1={};
	myVars.races.race156.event2={};
	myVars.races.race156.event1.id = "my-account-rememberme";
	myVars.races.race156.event1.type = "onkeypress";
	myVars.races.race156.event1.loc = "my-account-rememberme_LOC";
	myVars.races.race156.event1.isRead = "False";
	myVars.races.race156.event1.eventType = "@event1EventType@";
	myVars.races.race156.event2.id = "my-account-userid";
	myVars.races.race156.event2.type = "onkeypress";
	myVars.races.race156.event2.loc = "my-account-userid_LOC";
	myVars.races.race156.event2.isRead = "True";
	myVars.races.race156.event2.eventType = "@event2EventType@";
	myVars.races.race156.event1.executed= false;// true to disable, false to enable
	myVars.races.race156.event2.executed= false;// true to disable, false to enable
</script>

