<script id = "race198a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race198={};
	myVars.races.race198.varName="Object[2078].uuid";
	myVars.races.race198.varType="@varType@";
	myVars.races.race198.repairType = "@RepairType";
	myVars.races.race198.event1={};
	myVars.races.race198.event2={};
	myVars.races.race198.event1.id = "Lu_Id_input_25";
	myVars.races.race198.event1.type = "onkeypress";
	myVars.races.race198.event1.loc = "Lu_Id_input_25_LOC";
	myVars.races.race198.event1.isRead = "False";
	myVars.races.race198.event1.eventType = "@event1EventType@";
	myVars.races.race198.event2.id = "Lu_Id_input_24";
	myVars.races.race198.event2.type = "onkeypress";
	myVars.races.race198.event2.loc = "Lu_Id_input_24_LOC";
	myVars.races.race198.event2.isRead = "True";
	myVars.races.race198.event2.eventType = "@event2EventType@";
	myVars.races.race198.event1.executed= false;// true to disable, false to enable
	myVars.races.race198.event2.executed= false;// true to disable, false to enable
</script>

