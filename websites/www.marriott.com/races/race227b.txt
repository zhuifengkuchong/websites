<script id = "race227b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race227={};
	myVars.races.race227.varName="Object[2078].uuid";
	myVars.races.race227.varType="@varType@";
	myVars.races.race227.repairType = "@RepairType";
	myVars.races.race227.event1={};
	myVars.races.race227.event2={};
	myVars.races.race227.event1.id = "header-my-account-userid";
	myVars.races.race227.event1.type = "onkeypress";
	myVars.races.race227.event1.loc = "header-my-account-userid_LOC";
	myVars.races.race227.event1.isRead = "True";
	myVars.races.race227.event1.eventType = "@event1EventType@";
	myVars.races.race227.event2.id = "header-my-account-password";
	myVars.races.race227.event2.type = "onkeypress";
	myVars.races.race227.event2.loc = "header-my-account-password_LOC";
	myVars.races.race227.event2.isRead = "False";
	myVars.races.race227.event2.eventType = "@event2EventType@";
	myVars.races.race227.event1.executed= false;// true to disable, false to enable
	myVars.races.race227.event2.executed= false;// true to disable, false to enable
</script>

