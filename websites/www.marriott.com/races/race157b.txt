<script id = "race157b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race157={};
	myVars.races.race157.varName="Object[2078].uuid";
	myVars.races.race157.varType="@varType@";
	myVars.races.race157.repairType = "@RepairType";
	myVars.races.race157.event1={};
	myVars.races.race157.event2={};
	myVars.races.race157.event1.id = "Lu_Id_input_32";
	myVars.races.race157.event1.type = "onkeypress";
	myVars.races.race157.event1.loc = "Lu_Id_input_32_LOC";
	myVars.races.race157.event1.isRead = "True";
	myVars.races.race157.event1.eventType = "@event1EventType@";
	myVars.races.race157.event2.id = "my-account-userid";
	myVars.races.race157.event2.type = "onkeypress";
	myVars.races.race157.event2.loc = "my-account-userid_LOC";
	myVars.races.race157.event2.isRead = "False";
	myVars.races.race157.event2.eventType = "@event2EventType@";
	myVars.races.race157.event1.executed= false;// true to disable, false to enable
	myVars.races.race157.event2.executed= false;// true to disable, false to enable
</script>

