<script id = "race151b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race151={};
	myVars.races.race151.varName="Object[2078].uuid";
	myVars.races.race151.varType="@varType@";
	myVars.races.race151.repairType = "@RepairType";
	myVars.races.race151.event1={};
	myVars.races.race151.event2={};
	myVars.races.race151.event1.id = "analytics-personalization";
	myVars.races.race151.event1.type = "onkeypress";
	myVars.races.race151.event1.loc = "analytics-personalization_LOC";
	myVars.races.race151.event1.isRead = "True";
	myVars.races.race151.event1.eventType = "@event1EventType@";
	myVars.races.race151.event2.id = "analyze-unspecified-links";
	myVars.races.race151.event2.type = "onkeypress";
	myVars.races.race151.event2.loc = "analyze-unspecified-links_LOC";
	myVars.races.race151.event2.isRead = "False";
	myVars.races.race151.event2.eventType = "@event2EventType@";
	myVars.races.race151.event1.executed= false;// true to disable, false to enable
	myVars.races.race151.event2.executed= false;// true to disable, false to enable
</script>

