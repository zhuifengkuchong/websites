<script id = "race139a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race139={};
	myVars.races.race139.varName="Object[2078].uuid";
	myVars.races.race139.varType="@varType@";
	myVars.races.race139.repairType = "@RepairType";
	myVars.races.race139.event1={};
	myVars.races.race139.event2={};
	myVars.races.race139.event1.id = "Lu_Id_button_1";
	myVars.races.race139.event1.type = "onkeypress";
	myVars.races.race139.event1.loc = "Lu_Id_button_1_LOC";
	myVars.races.race139.event1.isRead = "False";
	myVars.races.race139.event1.eventType = "@event1EventType@";
	myVars.races.race139.event2.id = "header-my-account-sign-in";
	myVars.races.race139.event2.type = "onkeypress";
	myVars.races.race139.event2.loc = "header-my-account-sign-in_LOC";
	myVars.races.race139.event2.isRead = "True";
	myVars.races.race139.event2.eventType = "@event2EventType@";
	myVars.races.race139.event1.executed= false;// true to disable, false to enable
	myVars.races.race139.event2.executed= false;// true to disable, false to enable
</script>

