<script id = "race237a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race237={};
	myVars.races.race237.varName="Object[2078].uuid";
	myVars.races.race237.varType="@varType@";
	myVars.races.race237.repairType = "@RepairType";
	myVars.races.race237.event1={};
	myVars.races.race237.event2={};
	myVars.races.race237.event1.id = "Lu_Id_input_6";
	myVars.races.race237.event1.type = "onkeypress";
	myVars.races.race237.event1.loc = "Lu_Id_input_6_LOC";
	myVars.races.race237.event1.isRead = "False";
	myVars.races.race237.event1.eventType = "@event1EventType@";
	myVars.races.race237.event2.id = "Lu_Id_input_5";
	myVars.races.race237.event2.type = "onkeypress";
	myVars.races.race237.event2.loc = "Lu_Id_input_5_LOC";
	myVars.races.race237.event2.isRead = "True";
	myVars.races.race237.event2.eventType = "@event2EventType@";
	myVars.races.race237.event1.executed= false;// true to disable, false to enable
	myVars.races.race237.event2.executed= false;// true to disable, false to enable
</script>

