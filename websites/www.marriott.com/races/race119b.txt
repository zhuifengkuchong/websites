<script id = "race119b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race119={};
	myVars.races.race119.varName="my-account-signin-button__onchange";
	myVars.races.race119.varType="@varType@";
	myVars.races.race119.repairType = "@RepairType";
	myVars.races.race119.event1={};
	myVars.races.race119.event2={};
	myVars.races.race119.event1.id = "my-account-signin-button";
	myVars.races.race119.event1.type = "onchange";
	myVars.races.race119.event1.loc = "my-account-signin-button_LOC";
	myVars.races.race119.event1.isRead = "True";
	myVars.races.race119.event1.eventType = "@event1EventType@";
	myVars.races.race119.event2.id = "Lu_DOM";
	myVars.races.race119.event2.type = "onDOMContentLoaded";
	myVars.races.race119.event2.loc = "Lu_DOM_LOC";
	myVars.races.race119.event2.isRead = "False";
	myVars.races.race119.event2.eventType = "@event2EventType@";
	myVars.races.race119.event1.executed= false;// true to disable, false to enable
	myVars.races.race119.event2.executed= false;// true to disable, false to enable
</script>

