<script id = "race226a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race226={};
	myVars.races.race226.varName="Object[2078].uuid";
	myVars.races.race226.varType="@varType@";
	myVars.races.race226.repairType = "@RepairType";
	myVars.races.race226.event1={};
	myVars.races.race226.event2={};
	myVars.races.race226.event1.id = "my-account-rememberme";
	myVars.races.race226.event1.type = "onkeypress";
	myVars.races.race226.event1.loc = "my-account-rememberme_LOC";
	myVars.races.race226.event1.isRead = "False";
	myVars.races.race226.event1.eventType = "@event1EventType@";
	myVars.races.race226.event2.id = "header-my-account-password";
	myVars.races.race226.event2.type = "onkeypress";
	myVars.races.race226.event2.loc = "header-my-account-password_LOC";
	myVars.races.race226.event2.isRead = "True";
	myVars.races.race226.event2.eventType = "@event2EventType@";
	myVars.races.race226.event1.executed= false;// true to disable, false to enable
	myVars.races.race226.event2.executed= false;// true to disable, false to enable
</script>

