<script id = "race224b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race224={};
	myVars.races.race224.varName="Object[2078].uuid";
	myVars.races.race224.varType="@varType@";
	myVars.races.race224.repairType = "@RepairType";
	myVars.races.race224.event1={};
	myVars.races.race224.event2={};
	myVars.races.race224.event1.id = "header-sign-in-link-data-analytics";
	myVars.races.race224.event1.type = "onkeypress";
	myVars.races.race224.event1.loc = "header-sign-in-link-data-analytics_LOC";
	myVars.races.race224.event1.isRead = "True";
	myVars.races.race224.event1.eventType = "@event1EventType@";
	myVars.races.race224.event2.id = "header-join-rewards-link-data-analytics";
	myVars.races.race224.event2.type = "onkeypress";
	myVars.races.race224.event2.loc = "header-join-rewards-link-data-analytics_LOC";
	myVars.races.race224.event2.isRead = "False";
	myVars.races.race224.event2.eventType = "@event2EventType@";
	myVars.races.race224.event1.executed= false;// true to disable, false to enable
	myVars.races.race224.event2.executed= false;// true to disable, false to enable
</script>

