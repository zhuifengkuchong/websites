<script id = "race210a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race210={};
	myVars.races.race210.varName="Object[2078].uuid";
	myVars.races.race210.varType="@varType@";
	myVars.races.race210.repairType = "@RepairType";
	myVars.races.race210.event1={};
	myVars.races.race210.event2={};
	myVars.races.race210.event1.id = "search-location";
	myVars.races.race210.event1.type = "onkeypress";
	myVars.races.race210.event1.loc = "search-location_LOC";
	myVars.races.race210.event1.isRead = "False";
	myVars.races.race210.event1.eventType = "@event1EventType@";
	myVars.races.race210.event2.id = "autosuggest-cat-headers";
	myVars.races.race210.event2.type = "onkeypress";
	myVars.races.race210.event2.loc = "autosuggest-cat-headers_LOC";
	myVars.races.race210.event2.isRead = "True";
	myVars.races.race210.event2.eventType = "@event2EventType@";
	myVars.races.race210.event1.executed= false;// true to disable, false to enable
	myVars.races.race210.event2.executed= false;// true to disable, false to enable
</script>

