<script id = "race27a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race27={};
	myVars.races.race27.varName="brand-tp__onkeypress";
	myVars.races.race27.varType="@varType@";
	myVars.races.race27.repairType = "@RepairType";
	myVars.races.race27.event1={};
	myVars.races.race27.event2={};
	myVars.races.race27.event1.id = "Lu_DOM";
	myVars.races.race27.event1.type = "onDOMContentLoaded";
	myVars.races.race27.event1.loc = "Lu_DOM_LOC";
	myVars.races.race27.event1.isRead = "False";
	myVars.races.race27.event1.eventType = "@event1EventType@";
	myVars.races.race27.event2.id = "brand-tp";
	myVars.races.race27.event2.type = "onkeypress";
	myVars.races.race27.event2.loc = "brand-tp_LOC";
	myVars.races.race27.event2.isRead = "True";
	myVars.races.race27.event2.eventType = "@event2EventType@";
	myVars.races.race27.event1.executed= false;// true to disable, false to enable
	myVars.races.race27.event2.executed= false;// true to disable, false to enable
</script>

