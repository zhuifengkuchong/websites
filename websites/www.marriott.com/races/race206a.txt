<script id = "race206a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race206={};
	myVars.races.race206.varName="Object[2078].uuid";
	myVars.races.race206.varType="@varType@";
	myVars.races.race206.repairType = "@RepairType";
	myVars.races.race206.event1={};
	myVars.races.race206.event2={};
	myVars.races.race206.event1.id = "single-search-room-count-label";
	myVars.races.race206.event1.type = "onkeypress";
	myVars.races.race206.event1.loc = "single-search-room-count-label_LOC";
	myVars.races.race206.event1.isRead = "False";
	myVars.races.race206.event1.eventType = "@event1EventType@";
	myVars.races.race206.event2.id = "single-search-date-format";
	myVars.races.race206.event2.type = "onkeypress";
	myVars.races.race206.event2.loc = "single-search-date-format_LOC";
	myVars.races.race206.event2.isRead = "True";
	myVars.races.race206.event2.eventType = "@event2EventType@";
	myVars.races.race206.event1.executed= false;// true to disable, false to enable
	myVars.races.race206.event2.executed= false;// true to disable, false to enable
</script>

