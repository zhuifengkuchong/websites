<script id = "race185a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race185={};
	myVars.races.race185.varName="Object[2078].uuid";
	myVars.races.race185.varType="@varType@";
	myVars.races.race185.repairType = "@RepairType";
	myVars.races.race185.event1={};
	myVars.races.race185.event2={};
	myVars.races.race185.event1.id = "mr-number";
	myVars.races.race185.event1.type = "onkeypress";
	myVars.races.race185.event1.loc = "mr-number_LOC";
	myVars.races.race185.event1.isRead = "False";
	myVars.races.race185.event1.eventType = "@event1EventType@";
	myVars.races.race185.event2.id = "flexible-date-search";
	myVars.races.race185.event2.type = "onkeypress";
	myVars.races.race185.event2.loc = "flexible-date-search_LOC";
	myVars.races.race185.event2.isRead = "True";
	myVars.races.race185.event2.eventType = "@event2EventType@";
	myVars.races.race185.event1.executed= false;// true to disable, false to enable
	myVars.races.race185.event2.executed= false;// true to disable, false to enable
</script>

