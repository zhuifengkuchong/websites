<script id = "race161b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race161={};
	myVars.races.race161.varName="Object[2078].uuid";
	myVars.races.race161.varType="@varType@";
	myVars.races.race161.repairType = "@RepairType";
	myVars.races.race161.event1={};
	myVars.races.race161.event2={};
	myVars.races.race161.event1.id = "brand-er";
	myVars.races.race161.event1.type = "onkeypress";
	myVars.races.race161.event1.loc = "brand-er_LOC";
	myVars.races.race161.event1.isRead = "True";
	myVars.races.race161.event1.eventType = "@event1EventType@";
	myVars.races.race161.event2.id = "brand-mvci";
	myVars.races.race161.event2.type = "onkeypress";
	myVars.races.race161.event2.loc = "brand-mvci_LOC";
	myVars.races.race161.event2.isRead = "False";
	myVars.races.race161.event2.eventType = "@event2EventType@";
	myVars.races.race161.event1.executed= false;// true to disable, false to enable
	myVars.races.race161.event2.executed= false;// true to disable, false to enable
</script>

