<script id = "race236a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race236={};
	myVars.races.race236.varName="Object[2078].uuid";
	myVars.races.race236.varType="@varType@";
	myVars.races.race236.repairType = "@RepairType";
	myVars.races.race236.event1={};
	myVars.races.race236.event2={};
	myVars.races.race236.event1.id = "Lu_Id_input_7";
	myVars.races.race236.event1.type = "onkeypress";
	myVars.races.race236.event1.loc = "Lu_Id_input_7_LOC";
	myVars.races.race236.event1.isRead = "False";
	myVars.races.race236.event1.eventType = "@event1EventType@";
	myVars.races.race236.event2.id = "Lu_Id_input_6";
	myVars.races.race236.event2.type = "onkeypress";
	myVars.races.race236.event2.loc = "Lu_Id_input_6_LOC";
	myVars.races.race236.event2.isRead = "True";
	myVars.races.race236.event2.eventType = "@event2EventType@";
	myVars.races.race236.event1.executed= false;// true to disable, false to enable
	myVars.races.race236.event2.executed= false;// true to disable, false to enable
</script>

