<script id = "race72a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race72={};
	myVars.races.race72.varName="single-search-type__onkeypress";
	myVars.races.race72.varType="@varType@";
	myVars.races.race72.repairType = "@RepairType";
	myVars.races.race72.event1={};
	myVars.races.race72.event2={};
	myVars.races.race72.event1.id = "Lu_DOM";
	myVars.races.race72.event1.type = "onDOMContentLoaded";
	myVars.races.race72.event1.loc = "Lu_DOM_LOC";
	myVars.races.race72.event1.isRead = "False";
	myVars.races.race72.event1.eventType = "@event1EventType@";
	myVars.races.race72.event2.id = "single-search-type";
	myVars.races.race72.event2.type = "onkeypress";
	myVars.races.race72.event2.loc = "single-search-type_LOC";
	myVars.races.race72.event2.isRead = "True";
	myVars.races.race72.event2.eventType = "@event2EventType@";
	myVars.races.race72.event1.executed= false;// true to disable, false to enable
	myVars.races.race72.event2.executed= false;// true to disable, false to enable
</script>

