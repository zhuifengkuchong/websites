<script id = "race222a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race222={};
	myVars.races.race222.varName="Object[2078].uuid";
	myVars.races.race222.varType="@varType@";
	myVars.races.race222.repairType = "@RepairType";
	myVars.races.race222.event1={};
	myVars.races.race222.event2={};
	myVars.races.race222.event1.id = "groupMessage";
	myVars.races.race222.event1.type = "onkeypress";
	myVars.races.race222.event1.loc = "groupMessage_LOC";
	myVars.races.race222.event1.isRead = "False";
	myVars.races.race222.event1.eventType = "@event1EventType@";
	myVars.races.race222.event2.id = "useRewardsPtsMessage";
	myVars.races.race222.event2.type = "onkeypress";
	myVars.races.race222.event2.loc = "useRewardsPtsMessage_LOC";
	myVars.races.race222.event2.isRead = "True";
	myVars.races.race222.event2.eventType = "@event2EventType@";
	myVars.races.race222.event1.executed= false;// true to disable, false to enable
	myVars.races.race222.event2.executed= false;// true to disable, false to enable
</script>

