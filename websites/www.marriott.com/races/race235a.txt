<script id = "race235a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race235={};
	myVars.races.race235.varName="Object[2078].uuid";
	myVars.races.race235.varType="@varType@";
	myVars.races.race235.repairType = "@RepairType";
	myVars.races.race235.event1={};
	myVars.races.race235.event2={};
	myVars.races.race235.event1.id = "Lu_Id_input_8";
	myVars.races.race235.event1.type = "onkeypress";
	myVars.races.race235.event1.loc = "Lu_Id_input_8_LOC";
	myVars.races.race235.event1.isRead = "False";
	myVars.races.race235.event1.eventType = "@event1EventType@";
	myVars.races.race235.event2.id = "Lu_Id_input_7";
	myVars.races.race235.event2.type = "onkeypress";
	myVars.races.race235.event2.loc = "Lu_Id_input_7_LOC";
	myVars.races.race235.event2.isRead = "True";
	myVars.races.race235.event2.eventType = "@event2EventType@";
	myVars.races.race235.event1.executed= false;// true to disable, false to enable
	myVars.races.race235.event2.executed= false;// true to disable, false to enable
</script>

