<script id = "race253a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race253={};
	myVars.races.race253.varName="Object[2078].uuid";
	myVars.races.race253.varType="@varType@";
	myVars.races.race253.repairType = "@RepairType";
	myVars.races.race253.event1={};
	myVars.races.race253.event2={};
	myVars.races.race253.event1.id = "find-reservaton";
	myVars.races.race253.event1.type = "onchange";
	myVars.races.race253.event1.loc = "find-reservaton_LOC";
	myVars.races.race253.event1.isRead = "False";
	myVars.races.race253.event1.eventType = "@event1EventType@";
	myVars.races.race253.event2.id = "guestCount";
	myVars.races.race253.event2.type = "onchange";
	myVars.races.race253.event2.loc = "guestCount_LOC";
	myVars.races.race253.event2.isRead = "True";
	myVars.races.race253.event2.eventType = "@event2EventType@";
	myVars.races.race253.event1.executed= false;// true to disable, false to enable
	myVars.races.race253.event2.executed= false;// true to disable, false to enable
</script>

