<script id = "race187b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race187={};
	myVars.races.race187.varName="Object[2078].uuid";
	myVars.races.race187.varType="@varType@";
	myVars.races.race187.repairType = "@RepairType";
	myVars.races.race187.event1={};
	myVars.races.race187.event2={};
	myVars.races.race187.event1.id = "hotel-fromDate";
	myVars.races.race187.event1.type = "onkeypress";
	myVars.races.race187.event1.loc = "hotel-fromDate_LOC";
	myVars.races.race187.event1.isRead = "True";
	myVars.races.race187.event1.eventType = "@event1EventType@";
	myVars.races.race187.event2.id = "hotel-toDate";
	myVars.races.race187.event2.type = "onkeypress";
	myVars.races.race187.event2.loc = "hotel-toDate_LOC";
	myVars.races.race187.event2.isRead = "False";
	myVars.races.race187.event2.eventType = "@event2EventType@";
	myVars.races.race187.event1.executed= false;// true to disable, false to enable
	myVars.races.race187.event2.executed= false;// true to disable, false to enable
</script>

