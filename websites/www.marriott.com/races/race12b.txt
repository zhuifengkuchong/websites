<script id = "race12b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race12={};
	myVars.races.race12.varName="ctc-stay-total__onkeypress";
	myVars.races.race12.varType="@varType@";
	myVars.races.race12.repairType = "@RepairType";
	myVars.races.race12.event1={};
	myVars.races.race12.event2={};
	myVars.races.race12.event1.id = "ctc-stay-total";
	myVars.races.race12.event1.type = "onkeypress";
	myVars.races.race12.event1.loc = "ctc-stay-total_LOC";
	myVars.races.race12.event1.isRead = "True";
	myVars.races.race12.event1.eventType = "@event1EventType@";
	myVars.races.race12.event2.id = "Lu_DOM";
	myVars.races.race12.event2.type = "onDOMContentLoaded";
	myVars.races.race12.event2.loc = "Lu_DOM_LOC";
	myVars.races.race12.event2.isRead = "False";
	myVars.races.race12.event2.eventType = "@event2EventType@";
	myVars.races.race12.event1.executed= false;// true to disable, false to enable
	myVars.races.race12.event2.executed= false;// true to disable, false to enable
</script>

