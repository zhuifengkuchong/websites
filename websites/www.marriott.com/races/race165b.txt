<script id = "race165b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race165={};
	myVars.races.race165.varName="Object[2078].uuid";
	myVars.races.race165.varType="@varType@";
	myVars.races.race165.repairType = "@RepairType";
	myVars.races.race165.event1={};
	myVars.races.race165.event2={};
	myVars.races.race165.event1.id = "brand-fn";
	myVars.races.race165.event1.type = "onkeypress";
	myVars.races.race165.event1.loc = "brand-fn_LOC";
	myVars.races.race165.event1.isRead = "True";
	myVars.races.race165.event1.eventType = "@event1EventType@";
	myVars.races.race165.event2.id = "brand-sh";
	myVars.races.race165.event2.type = "onkeypress";
	myVars.races.race165.event2.loc = "brand-sh_LOC";
	myVars.races.race165.event2.isRead = "False";
	myVars.races.race165.event2.eventType = "@event2EventType@";
	myVars.races.race165.event1.executed= false;// true to disable, false to enable
	myVars.races.race165.event2.executed= false;// true to disable, false to enable
</script>

