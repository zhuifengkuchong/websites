<script id = "race221b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race221={};
	myVars.races.race221.varName="Object[2078].uuid";
	myVars.races.race221.varType="@varType@";
	myVars.races.race221.repairType = "@RepairType";
	myVars.races.race221.event1={};
	myVars.races.race221.event2={};
	myVars.races.race221.event1.id = "groupMessage";
	myVars.races.race221.event1.type = "onkeypress";
	myVars.races.race221.event1.loc = "groupMessage_LOC";
	myVars.races.race221.event1.isRead = "True";
	myVars.races.race221.event1.eventType = "@event1EventType@";
	myVars.races.race221.event2.id = "corpCodeBlankMessage";
	myVars.races.race221.event2.type = "onkeypress";
	myVars.races.race221.event2.loc = "corpCodeBlankMessage_LOC";
	myVars.races.race221.event2.isRead = "False";
	myVars.races.race221.event2.eventType = "@event2EventType@";
	myVars.races.race221.event1.executed= false;// true to disable, false to enable
	myVars.races.race221.event2.executed= false;// true to disable, false to enable
</script>

