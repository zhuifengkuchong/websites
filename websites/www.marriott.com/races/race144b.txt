<script id = "race144b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race144={};
	myVars.races.race144.varName="Object[2078].uuid";
	myVars.races.race144.varType="@varType@";
	myVars.races.race144.repairType = "@RepairType";
	myVars.races.race144.event1={};
	myVars.races.race144.event2={};
	myVars.races.race144.event1.id = "length-of-stay1";
	myVars.races.race144.event1.type = "onkeypress";
	myVars.races.race144.event1.loc = "length-of-stay1_LOC";
	myVars.races.race144.event1.isRead = "True";
	myVars.races.race144.event1.eventType = "@event1EventType@";
	myVars.races.race144.event2.id = "roomCount";
	myVars.races.race144.event2.type = "onkeypress";
	myVars.races.race144.event2.loc = "roomCount_LOC";
	myVars.races.race144.event2.isRead = "False";
	myVars.races.race144.event2.eventType = "@event2EventType@";
	myVars.races.race144.event1.executed= false;// true to disable, false to enable
	myVars.races.race144.event2.executed= false;// true to disable, false to enable
</script>

