<script id = "race110b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race110={};
	myVars.races.race110.varName="header-reservation-panel-trigger-data-analytics__onkeypress";
	myVars.races.race110.varType="@varType@";
	myVars.races.race110.repairType = "@RepairType";
	myVars.races.race110.event1={};
	myVars.races.race110.event2={};
	myVars.races.race110.event1.id = "header-reservation-panel-trigger-data-analytics";
	myVars.races.race110.event1.type = "onkeypress";
	myVars.races.race110.event1.loc = "header-reservation-panel-trigger-data-analytics_LOC";
	myVars.races.race110.event1.isRead = "True";
	myVars.races.race110.event1.eventType = "@event1EventType@";
	myVars.races.race110.event2.id = "Lu_DOM";
	myVars.races.race110.event2.type = "onDOMContentLoaded";
	myVars.races.race110.event2.loc = "Lu_DOM_LOC";
	myVars.races.race110.event2.isRead = "False";
	myVars.races.race110.event2.eventType = "@event2EventType@";
	myVars.races.race110.event1.executed= false;// true to disable, false to enable
	myVars.races.race110.event2.executed= false;// true to disable, false to enable
</script>

