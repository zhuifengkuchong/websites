<script id = "race176b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race176={};
	myVars.races.race176.varName="Object[2078].uuid";
	myVars.races.race176.varType="@varType@";
	myVars.races.race176.repairType = "@RepairType";
	myVars.races.race176.event1={};
	myVars.races.race176.event2={};
	myVars.races.race176.event1.id = "brand-ge";
	myVars.races.race176.event1.type = "onkeypress";
	myVars.races.race176.event1.loc = "brand-ge_LOC";
	myVars.races.race176.event1.isRead = "True";
	myVars.races.race176.event1.eventType = "@event1EventType@";
	myVars.races.race176.event2.id = "brand-rz";
	myVars.races.race176.event2.type = "onkeypress";
	myVars.races.race176.event2.loc = "brand-rz_LOC";
	myVars.races.race176.event2.isRead = "False";
	myVars.races.race176.event2.eventType = "@event2EventType@";
	myVars.races.race176.event1.executed= false;// true to disable, false to enable
	myVars.races.race176.event2.executed= false;// true to disable, false to enable
</script>

