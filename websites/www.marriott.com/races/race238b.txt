<script id = "race238b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race238={};
	myVars.races.race238.varName="Object[2078].uuid";
	myVars.races.race238.varType="@varType@";
	myVars.races.race238.repairType = "@RepairType";
	myVars.races.race238.event1={};
	myVars.races.race238.event2={};
	myVars.races.race238.event1.id = "Lu_Id_input_4";
	myVars.races.race238.event1.type = "onkeypress";
	myVars.races.race238.event1.loc = "Lu_Id_input_4_LOC";
	myVars.races.race238.event1.isRead = "True";
	myVars.races.race238.event1.eventType = "@event1EventType@";
	myVars.races.race238.event2.id = "Lu_Id_input_5";
	myVars.races.race238.event2.type = "onkeypress";
	myVars.races.race238.event2.loc = "Lu_Id_input_5_LOC";
	myVars.races.race238.event2.isRead = "False";
	myVars.races.race238.event2.eventType = "@event2EventType@";
	myVars.races.race238.event1.executed= false;// true to disable, false to enable
	myVars.races.race238.event2.executed= false;// true to disable, false to enable
</script>

