<script id = "race32a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race32={};
	myVars.races.race32.varName="brand-cy__onkeypress";
	myVars.races.race32.varType="@varType@";
	myVars.races.race32.repairType = "@RepairType";
	myVars.races.race32.event1={};
	myVars.races.race32.event2={};
	myVars.races.race32.event1.id = "Lu_DOM";
	myVars.races.race32.event1.type = "onDOMContentLoaded";
	myVars.races.race32.event1.loc = "Lu_DOM_LOC";
	myVars.races.race32.event1.isRead = "False";
	myVars.races.race32.event1.eventType = "@event1EventType@";
	myVars.races.race32.event2.id = "brand-cy";
	myVars.races.race32.event2.type = "onkeypress";
	myVars.races.race32.event2.loc = "brand-cy_LOC";
	myVars.races.race32.event2.isRead = "True";
	myVars.races.race32.event2.eventType = "@event2EventType@";
	myVars.races.race32.event1.executed= false;// true to disable, false to enable
	myVars.races.race32.event2.executed= false;// true to disable, false to enable
</script>

