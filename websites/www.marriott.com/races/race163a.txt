<script id = "race163a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race163={};
	myVars.races.race163.varName="Object[2078].uuid";
	myVars.races.race163.varType="@varType@";
	myVars.races.race163.repairType = "@RepairType";
	myVars.races.race163.event1={};
	myVars.races.race163.event2={};
	myVars.races.race163.event1.id = "brand-tp";
	myVars.races.race163.event1.type = "onkeypress";
	myVars.races.race163.event1.loc = "brand-tp_LOC";
	myVars.races.race163.event1.isRead = "False";
	myVars.races.race163.event1.eventType = "@event1EventType@";
	myVars.races.race163.event2.id = "brand-ri";
	myVars.races.race163.event2.type = "onkeypress";
	myVars.races.race163.event2.loc = "brand-ri_LOC";
	myVars.races.race163.event2.isRead = "True";
	myVars.races.race163.event2.eventType = "@event2EventType@";
	myVars.races.race163.event1.executed= false;// true to disable, false to enable
	myVars.races.race163.event2.executed= false;// true to disable, false to enable
</script>

