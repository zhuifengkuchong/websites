<script id = "race94b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race94={};
	myVars.races.race94.varName="my-account-rememberme__onkeypress";
	myVars.races.race94.varType="@varType@";
	myVars.races.race94.repairType = "@RepairType";
	myVars.races.race94.event1={};
	myVars.races.race94.event2={};
	myVars.races.race94.event1.id = "my-account-rememberme";
	myVars.races.race94.event1.type = "onkeypress";
	myVars.races.race94.event1.loc = "my-account-rememberme_LOC";
	myVars.races.race94.event1.isRead = "True";
	myVars.races.race94.event1.eventType = "@event1EventType@";
	myVars.races.race94.event2.id = "Lu_DOM";
	myVars.races.race94.event2.type = "onDOMContentLoaded";
	myVars.races.race94.event2.loc = "Lu_DOM_LOC";
	myVars.races.race94.event2.isRead = "False";
	myVars.races.race94.event2.eventType = "@event2EventType@";
	myVars.races.race94.event1.executed= false;// true to disable, false to enable
	myVars.races.race94.event2.executed= false;// true to disable, false to enable
</script>

