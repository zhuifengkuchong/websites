<script id = "race133b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race133={};
	myVars.races.race133.varName="analytics-personalization__onchange";
	myVars.races.race133.varType="@varType@";
	myVars.races.race133.repairType = "@RepairType";
	myVars.races.race133.event1={};
	myVars.races.race133.event2={};
	myVars.races.race133.event1.id = "analytics-personalization";
	myVars.races.race133.event1.type = "onchange";
	myVars.races.race133.event1.loc = "analytics-personalization_LOC";
	myVars.races.race133.event1.isRead = "True";
	myVars.races.race133.event1.eventType = "@event1EventType@";
	myVars.races.race133.event2.id = "Lu_DOM";
	myVars.races.race133.event2.type = "onDOMContentLoaded";
	myVars.races.race133.event2.loc = "Lu_DOM_LOC";
	myVars.races.race133.event2.isRead = "False";
	myVars.races.race133.event2.eventType = "@event2EventType@";
	myVars.races.race133.event1.executed= false;// true to disable, false to enable
	myVars.races.race133.event2.executed= false;// true to disable, false to enable
</script>

