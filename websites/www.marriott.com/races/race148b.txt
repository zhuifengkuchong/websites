<script id = "race148b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race148={};
	myVars.races.race148.varName="Object[2078].uuid";
	myVars.races.race148.varType="@varType@";
	myVars.races.race148.repairType = "@RepairType";
	myVars.races.race148.event1={};
	myVars.races.race148.event2={};
	myVars.races.race148.event1.id = "ctc-booking-pace";
	myVars.races.race148.event1.type = "onkeypress";
	myVars.races.race148.event1.loc = "ctc-booking-pace_LOC";
	myVars.races.race148.event1.isRead = "True";
	myVars.races.race148.event1.eventType = "@event1EventType@";
	myVars.races.race148.event2.id = "ctc-stay-total";
	myVars.races.race148.event2.type = "onkeypress";
	myVars.races.race148.event2.loc = "ctc-stay-total_LOC";
	myVars.races.race148.event2.isRead = "False";
	myVars.races.race148.event2.eventType = "@event2EventType@";
	myVars.races.race148.event1.executed= false;// true to disable, false to enable
	myVars.races.race148.event2.executed= false;// true to disable, false to enable
</script>

