<script id = "race241b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race241={};
	myVars.races.race241.varName="Object[2078].uuid";
	myVars.races.race241.varType="@varType@";
	myVars.races.race241.repairType = "@RepairType";
	myVars.races.race241.event1={};
	myVars.races.race241.event2={};
	myVars.races.race241.event1.id = "lastName";
	myVars.races.race241.event1.type = "onkeypress";
	myVars.races.race241.event1.loc = "lastName_LOC";
	myVars.races.race241.event1.isRead = "True";
	myVars.races.race241.event1.eventType = "@event1EventType@";
	myVars.races.race241.event2.id = "confirmationNumber";
	myVars.races.race241.event2.type = "onkeypress";
	myVars.races.race241.event2.loc = "confirmationNumber_LOC";
	myVars.races.race241.event2.isRead = "False";
	myVars.races.race241.event2.eventType = "@event2EventType@";
	myVars.races.race241.event1.executed= false;// true to disable, false to enable
	myVars.races.race241.event2.executed= false;// true to disable, false to enable
</script>

