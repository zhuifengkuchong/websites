<script id = "race138b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race138={};
	myVars.races.race138.varName="Object[2078].uuid";
	myVars.races.race138.varType="@varType@";
	myVars.races.race138.repairType = "@RepairType";
	myVars.races.race138.event1={};
	myVars.races.race138.event2={};
	myVars.races.race138.event1.id = "Lu_Id_button_1";
	myVars.races.race138.event1.type = "onkeypress";
	myVars.races.race138.event1.loc = "Lu_Id_button_1_LOC";
	myVars.races.race138.event1.isRead = "True";
	myVars.races.race138.event1.eventType = "@event1EventType@";
	myVars.races.race138.event2.id = "Lu_Id_button_2";
	myVars.races.race138.event2.type = "onkeypress";
	myVars.races.race138.event2.loc = "Lu_Id_button_2_LOC";
	myVars.races.race138.event2.isRead = "False";
	myVars.races.race138.event2.eventType = "@event2EventType@";
	myVars.races.race138.event1.executed= false;// true to disable, false to enable
	myVars.races.race138.event2.executed= false;// true to disable, false to enable
</script>

