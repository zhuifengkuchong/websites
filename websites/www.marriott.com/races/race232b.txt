<script id = "race232b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race232={};
	myVars.races.race232.varName="Object[2078].uuid";
	myVars.races.race232.varType="@varType@";
	myVars.races.race232.repairType = "@RepairType";
	myVars.races.race232.event1={};
	myVars.races.race232.event2={};
	myVars.races.race232.event1.id = "Lu_Id_input_10";
	myVars.races.race232.event1.type = "onkeypress";
	myVars.races.race232.event1.loc = "Lu_Id_input_10_LOC";
	myVars.races.race232.event1.isRead = "True";
	myVars.races.race232.event1.eventType = "@event1EventType@";
	myVars.races.race232.event2.id = "my-account-userid";
	myVars.races.race232.event2.type = "onkeypress";
	myVars.races.race232.event2.loc = "my-account-userid_LOC";
	myVars.races.race232.event2.isRead = "False";
	myVars.races.race232.event2.eventType = "@event2EventType@";
	myVars.races.race232.event1.executed= false;// true to disable, false to enable
	myVars.races.race232.event2.executed= false;// true to disable, false to enable
</script>

