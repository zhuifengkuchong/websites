<script id = "race167a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race167={};
	myVars.races.race167.varName="Object[2078].uuid";
	myVars.races.race167.varType="@varType@";
	myVars.races.race167.repairType = "@RepairType";
	myVars.races.race167.event1={};
	myVars.races.race167.event2={};
	myVars.races.race167.event1.id = "brand-pr";
	myVars.races.race167.event1.type = "onkeypress";
	myVars.races.race167.event1.loc = "brand-pr_LOC";
	myVars.races.race167.event1.isRead = "False";
	myVars.races.race167.event1.eventType = "@event1EventType@";
	myVars.races.race167.event2.id = "brand-cy";
	myVars.races.race167.event2.type = "onkeypress";
	myVars.races.race167.event2.loc = "brand-cy_LOC";
	myVars.races.race167.event2.isRead = "True";
	myVars.races.race167.event2.eventType = "@event2EventType@";
	myVars.races.race167.event1.executed= false;// true to disable, false to enable
	myVars.races.race167.event2.executed= false;// true to disable, false to enable
</script>

