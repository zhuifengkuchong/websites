<script id = "race152b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race152={};
	myVars.races.race152.varName="Object[2078].uuid";
	myVars.races.race152.varType="@varType@";
	myVars.races.race152.repairType = "@RepairType";
	myVars.races.race152.event1={};
	myVars.races.race152.event2={};
	myVars.races.race152.event1.id = "analytics-page";
	myVars.races.race152.event1.type = "onkeypress";
	myVars.races.race152.event1.loc = "analytics-page_LOC";
	myVars.races.race152.event1.isRead = "True";
	myVars.races.race152.event1.eventType = "@event1EventType@";
	myVars.races.race152.event2.id = "analytics-personalization";
	myVars.races.race152.event2.type = "onkeypress";
	myVars.races.race152.event2.loc = "analytics-personalization_LOC";
	myVars.races.race152.event2.isRead = "False";
	myVars.races.race152.event2.eventType = "@event2EventType@";
	myVars.races.race152.event1.executed= false;// true to disable, false to enable
	myVars.races.race152.event2.executed= false;// true to disable, false to enable
</script>

