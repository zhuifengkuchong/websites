<script id = "race181a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race181={};
	myVars.races.race181.varName="Object[2078].uuid";
	myVars.races.race181.varType="@varType@";
	myVars.races.race181.repairType = "@RepairType";
	myVars.races.race181.event1={};
	myVars.races.race181.event2={};
	myVars.races.race181.event1.id = "S9R";
	myVars.races.race181.event1.type = "onkeypress";
	myVars.races.race181.event1.loc = "S9R_LOC";
	myVars.races.race181.event1.isRead = "False";
	myVars.races.race181.event1.eventType = "@event1EventType@";
	myVars.races.race181.event2.id = "AAA";
	myVars.races.race181.event2.type = "onkeypress";
	myVars.races.race181.event2.loc = "AAA_LOC";
	myVars.races.race181.event2.isRead = "True";
	myVars.races.race181.event2.eventType = "@event2EventType@";
	myVars.races.race181.event1.executed= false;// true to disable, false to enable
	myVars.races.race181.event2.executed= false;// true to disable, false to enable
</script>

