<script id = "race100b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race100={};
	myVars.races.race100.varName="Lu_Id_input_7__onkeypress";
	myVars.races.race100.varType="@varType@";
	myVars.races.race100.repairType = "@RepairType";
	myVars.races.race100.event1={};
	myVars.races.race100.event2={};
	myVars.races.race100.event1.id = "Lu_Id_input_7";
	myVars.races.race100.event1.type = "onkeypress";
	myVars.races.race100.event1.loc = "Lu_Id_input_7_LOC";
	myVars.races.race100.event1.isRead = "True";
	myVars.races.race100.event1.eventType = "@event1EventType@";
	myVars.races.race100.event2.id = "Lu_DOM";
	myVars.races.race100.event2.type = "onDOMContentLoaded";
	myVars.races.race100.event2.loc = "Lu_DOM_LOC";
	myVars.races.race100.event2.isRead = "False";
	myVars.races.race100.event2.eventType = "@event2EventType@";
	myVars.races.race100.event1.executed= false;// true to disable, false to enable
	myVars.races.race100.event2.executed= false;// true to disable, false to enable
</script>

