<script id = "race240a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race240={};
	myVars.races.race240.varName="Object[2078].uuid";
	myVars.races.race240.varType="@varType@";
	myVars.races.race240.repairType = "@RepairType";
	myVars.races.race240.event1={};
	myVars.races.race240.event2={};
	myVars.races.race240.event1.id = "checkInDate";
	myVars.races.race240.event1.type = "onkeypress";
	myVars.races.race240.event1.loc = "checkInDate_LOC";
	myVars.races.race240.event1.isRead = "False";
	myVars.races.race240.event1.eventType = "@event1EventType@";
	myVars.races.race240.event2.id = "confirmationNumber";
	myVars.races.race240.event2.type = "onkeypress";
	myVars.races.race240.event2.loc = "confirmationNumber_LOC";
	myVars.races.race240.event2.isRead = "True";
	myVars.races.race240.event2.eventType = "@event2EventType@";
	myVars.races.race240.event1.executed= false;// true to disable, false to enable
	myVars.races.race240.event2.executed= false;// true to disable, false to enable
</script>

