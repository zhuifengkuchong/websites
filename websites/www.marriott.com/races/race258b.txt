<script id = "race258b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race258={};
	myVars.races.race258.varName="Object[2078].uuid";
	myVars.races.race258.varType="@varType@";
	myVars.races.race258.repairType = "@RepairType";
	myVars.races.race258.event1={};
	myVars.races.race258.event2={};
	myVars.races.race258.event1.id = "ctc-stay-total";
	myVars.races.race258.event1.type = "onchange";
	myVars.races.race258.event1.loc = "ctc-stay-total_LOC";
	myVars.races.race258.event1.isRead = "True";
	myVars.races.race258.event1.eventType = "@event1EventType@";
	myVars.races.race258.event2.id = "Lu_Id_input_33";
	myVars.races.race258.event2.type = "onchange";
	myVars.races.race258.event2.loc = "Lu_Id_input_33_LOC";
	myVars.races.race258.event2.isRead = "False";
	myVars.races.race258.event2.eventType = "@event2EventType@";
	myVars.races.race258.event1.executed= false;// true to disable, false to enable
	myVars.races.race258.event2.executed= false;// true to disable, false to enable
</script>

