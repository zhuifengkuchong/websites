<script id = "race87a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race87={};
	myVars.races.race87.varName="useRewardsPtsMessage__onkeypress";
	myVars.races.race87.varType="@varType@";
	myVars.races.race87.repairType = "@RepairType";
	myVars.races.race87.event1={};
	myVars.races.race87.event2={};
	myVars.races.race87.event1.id = "Lu_DOM";
	myVars.races.race87.event1.type = "onDOMContentLoaded";
	myVars.races.race87.event1.loc = "Lu_DOM_LOC";
	myVars.races.race87.event1.isRead = "False";
	myVars.races.race87.event1.eventType = "@event1EventType@";
	myVars.races.race87.event2.id = "useRewardsPtsMessage";
	myVars.races.race87.event2.type = "onkeypress";
	myVars.races.race87.event2.loc = "useRewardsPtsMessage_LOC";
	myVars.races.race87.event2.isRead = "True";
	myVars.races.race87.event2.eventType = "@event2EventType@";
	myVars.races.race87.event1.executed= false;// true to disable, false to enable
	myVars.races.race87.event2.executed= false;// true to disable, false to enable
</script>

