<script id = "race141b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race141={};
	myVars.races.race141.varName="Object[2078].uuid";
	myVars.races.race141.varType="@varType@";
	myVars.races.race141.repairType = "@RepairType";
	myVars.races.race141.event1={};
	myVars.races.race141.event2={};
	myVars.races.race141.event1.id = "find-reservaton";
	myVars.races.race141.event1.type = "onkeypress";
	myVars.races.race141.event1.loc = "find-reservaton_LOC";
	myVars.races.race141.event1.isRead = "True";
	myVars.races.race141.event1.eventType = "@event1EventType@";
	myVars.races.race141.event2.id = "my-account-signin-button";
	myVars.races.race141.event2.type = "onkeypress";
	myVars.races.race141.event2.loc = "my-account-signin-button_LOC";
	myVars.races.race141.event2.isRead = "False";
	myVars.races.race141.event2.eventType = "@event2EventType@";
	myVars.races.race141.event1.executed= false;// true to disable, false to enable
	myVars.races.race141.event2.executed= false;// true to disable, false to enable
</script>

