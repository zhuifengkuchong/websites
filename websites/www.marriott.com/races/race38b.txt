<script id = "race38b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race38={};
	myVars.races.race38.varName="brand-eb__onkeypress";
	myVars.races.race38.varType="@varType@";
	myVars.races.race38.repairType = "@RepairType";
	myVars.races.race38.event1={};
	myVars.races.race38.event2={};
	myVars.races.race38.event1.id = "brand-eb";
	myVars.races.race38.event1.type = "onkeypress";
	myVars.races.race38.event1.loc = "brand-eb_LOC";
	myVars.races.race38.event1.isRead = "True";
	myVars.races.race38.event1.eventType = "@event1EventType@";
	myVars.races.race38.event2.id = "Lu_DOM";
	myVars.races.race38.event2.type = "onDOMContentLoaded";
	myVars.races.race38.event2.loc = "Lu_DOM_LOC";
	myVars.races.race38.event2.isRead = "False";
	myVars.races.race38.event2.eventType = "@event2EventType@";
	myVars.races.race38.event1.executed= false;// true to disable, false to enable
	myVars.races.race38.event2.executed= false;// true to disable, false to enable
</script>

