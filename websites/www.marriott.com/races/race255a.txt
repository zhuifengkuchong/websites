<script id = "race255a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race255={};
	myVars.races.race255.varName="Object[2078].uuid";
	myVars.races.race255.varType="@varType@";
	myVars.races.race255.repairType = "@RepairType";
	myVars.races.race255.event1={};
	myVars.races.race255.event2={};
	myVars.races.race255.event1.id = "roomCount";
	myVars.races.race255.event1.type = "onchange";
	myVars.races.race255.event1.loc = "roomCount_LOC";
	myVars.races.race255.event1.isRead = "False";
	myVars.races.race255.event1.eventType = "@event1EventType@";
	myVars.races.race255.event2.id = "length-of-stay1";
	myVars.races.race255.event2.type = "onchange";
	myVars.races.race255.event2.loc = "length-of-stay1_LOC";
	myVars.races.race255.event2.isRead = "True";
	myVars.races.race255.event2.eventType = "@event2EventType@";
	myVars.races.race255.event1.executed= false;// true to disable, false to enable
	myVars.races.race255.event2.executed= false;// true to disable, false to enable
</script>

