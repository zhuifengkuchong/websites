<script id = "race169a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race169={};
	myVars.races.race169.varName="Object[2078].uuid";
	myVars.races.race169.varType="@varType@";
	myVars.races.race169.repairType = "@RepairType";
	myVars.races.race169.event1={};
	myVars.races.race169.event2={};
	myVars.races.race169.event1.id = "brand-mc";
	myVars.races.race169.event1.type = "onkeypress";
	myVars.races.race169.event1.loc = "brand-mc_LOC";
	myVars.races.race169.event1.isRead = "False";
	myVars.races.race169.event1.eventType = "@event1EventType@";
	myVars.races.race169.event2.id = "brand-ox";
	myVars.races.race169.event2.type = "onkeypress";
	myVars.races.race169.event2.loc = "brand-ox_LOC";
	myVars.races.race169.event2.isRead = "True";
	myVars.races.race169.event2.eventType = "@event2EventType@";
	myVars.races.race169.event1.executed= false;// true to disable, false to enable
	myVars.races.race169.event2.executed= false;// true to disable, false to enable
</script>

