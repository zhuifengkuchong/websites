<script id = "race158b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race158={};
	myVars.races.race158.varName="Object[2078].uuid";
	myVars.races.race158.varType="@varType@";
	myVars.races.race158.repairType = "@RepairType";
	myVars.races.race158.event1={};
	myVars.races.race158.event2={};
	myVars.races.race158.event1.id = "brand-all";
	myVars.races.race158.event1.type = "onkeypress";
	myVars.races.race158.event1.loc = "brand-all_LOC";
	myVars.races.race158.event1.isRead = "True";
	myVars.races.race158.event1.eventType = "@event1EventType@";
	myVars.races.race158.event2.id = "Lu_Id_input_32";
	myVars.races.race158.event2.type = "onkeypress";
	myVars.races.race158.event2.loc = "Lu_Id_input_32_LOC";
	myVars.races.race158.event2.isRead = "False";
	myVars.races.race158.event2.eventType = "@event2EventType@";
	myVars.races.race158.event1.executed= false;// true to disable, false to enable
	myVars.races.race158.event2.executed= false;// true to disable, false to enable
</script>

