/*
 * The following functions are to be used ONLY in conjunction with
 * /rewardSearch/hotelSearchForm.jsp. DO NOT USE THESE FUNCTIONS TO
 * VALIDATE OTHER FORMS.
 * 
 * Author: Shamik Desai and Brian Eriksen
 */
 

function validateAndSubmitHotelSearchActionForm()
{
	var formObj = $('form#in-a-city-form');
	if($(formObj).find(":input[name='destinationAddress.stateProvince']").val() === "" && (($(formObj).find(":input[name='destinationAddress.country']").val() === "") || ($(formObj).find(":input[name='destinationAddress.country']").val() === " ")))
	{	
		alert($(formObj).find("#invalid-state-country-error-msg").val());
		return;
	}
	 
	if($(formObj).find(":input[name='destinationAddress.country']").val() === "US" && $(formObj).find(":input[name='destinationAddress.stateProvince']").val() === "")
	{
		alert($(formObj).find("#invalid-state-error-msg").val());
		return;
	}
	//submit the form
	sendHotelsSearchCommand($(formObj).get(0));
}


function sendHotelsSearchCommand(objForm) {
    	var useRewardsPoints = objForm.useRewardsPoints;
    	var corporateCode = objForm.corporateCode.value;
    	var isReservationSite = document.getElementById("isReservationSite").value;

		if (corporateCode != "") {
    		objForm.clusterCode.value = "other";
    	}
		
		//use points checkbox is selected
		if(useRewardsPoints != null && useRewardsPoints.checked) {
			if (isReservationSite == 'true') {
				objForm.submit();
			}
			else {
				// Pop to marriott.com (new browser window)
				sendUSHotelsSearchCommand(objForm, 'http://www.marriott.com/search/submitSearch.mi');
			}
		}
		//use points checkbox is not selected
		else {
			if (isReservationSite == 'true') {
				objForm.submit();
			}
			else {
				// Pop to marriott.com (new browser window)
				sendUSHotelsSearchCommand(objForm, 'http://www.marriott.com/search/submitSearch.mi');
			} 
		}
    }
    
function sendUSHotelsSearchCommand(objForm, actionPath) {
		var dateFormatPattern = $(':hidden[name=dateFormatPattern]').val().toLowerCase();
		var fromDate = objForm.fromDate.value;
		if(fromDate != null && fromDate != ''){
			fromDate = dateInMMDDYYFormat(fromDate,dateFormatPattern) ;
		}
		var toDate = objForm.toDate.value;
		if(toDate != null && toDate != ''){
			toDate = dateInMMDDYYFormat(toDate,dateFormatPattern) ;
		}

		var guestCount = objForm.guestCount.value;
		var roomCount = objForm.roomCount.value;
		var objUseRewardsPoints = objForm.useRewardsPoints;
		var cityName = objForm["http://www.marriott.com/common/js/destinationAddress.city"].value;
		var countryCode = objForm["destinationAddress.country"].value
		var stateCode = objForm["destinationAddress.stateProvince"].value 
		var corporateCode = objForm.corporateCode.value;
    	var marriottRewardsNumber = objForm.marriottRewardsNumber.value;
    	var recordsPerPage = objForm.recordsPerPage.value;
    	
		var params = "&fromDate="+fromDate+"&toDate="+toDate+"&guestCount="+guestCount+"&roomCount="+roomCount;
		
		if(objUseRewardsPoints != null)
		{
			 useRewardsPoints = objUseRewardsPoints.value;
		}
		if(objUseRewardsPoints != null &&
			objUseRewardsPoints.checked &&
				useRewardsPoints.length > 0)
		{
			params = params + "&useRewardsPoints="+useRewardsPoints;
		}
		
		if (corporateCode != "" && corporateCode.length > 0) {
    		clusterCode = "other";
    		params = params + "&clusterCode="+clusterCode + "&corporateCode="+corporateCode;
    	}
    	
		params = params + "&marriottRewardsNumber="+marriottRewardsNumber + "&destinationAddress.city="+cityName +
				 "&destinationAddress.stateProvince="+stateCode + "&destinationAddress.country="+countryCode + 
				 "&recordsPerPage="+recordsPerPage +"&marriottBrands=all&groupCode=&resultsMap=&searchRadius=&vsMarriottBrands=&";
				 
	
		var url = actionPath +"?searchType=InCity";
		javascript:sendto('US','default_domain',url+params,'3');
	}
	$(function() {
		var categoryTierContainer = $('.js-tabs');
		if(categoryTierContainer.length > 0){
			var tierValue = categoryTierContainer.data('options').selectedTab;
			var tabs = categoryTierContainer.tabs();
			if(typeof tierValue != 'undefined'){
				tabs.tabs('select',parseInt(tierValue));
			}
		}
		
		if($(".ritz-hws-url").length > 0){
			$(".ritz-hws-url").click(function() {
				window.opener.location.href = $(this).attr('href');
				return false;
			});
		}
	});