FSR.surveydefs = [{
    name: 'tablet',
    platform: 'tablet',
    invite: {
        when: 'onentry',
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Can we email or text you later a brief customer satisfaction survey so we can improve your mobile experience?",
            attribution: "Conducted by ForeSee.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll help",
            error: "Error"
        }], [{
            reverseButtons: false,
            headline: "Thank you for helping!",
            blurb: "Please provide your email address or mobile number (US and CA only). After your visit we'll send you a link to the survey. Text Messaging rates apply.",
            attribution: "ForeSee's <a class='fsrPrivacy' href='http://www.foresee.com/privacy-policy.shtml' target='_blank'>Privacy Policy</a>",
            declineButton: "Cancel",
            acceptButton: "email/text me",
            error: "Error",
            mobileExitDialog: {
                support: "b", //e for email only, s for sms only, b for both
                inputMessage: "email or mobile number",
                emailMeButtonText: "email me",
                textMeButtonText: "text me",
                fieldRequiredErrorText: "Enter a mobile number or email address",
                invalidFormatErrorText: "Format should be: name@domain.com or 123-456-7890"
            }
        }]]
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 5,
        lf: 3
    },
    include: {
        urls: [/\.com\//]
    }
}, {
    name: 'phone',
    platform: 'phone',
    invite: {
        when: 'onentry',
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Can we email or text you later a brief customer satisfaction survey so we can improve your mobile experience?",
            attribution: "Conducted by ForeSee.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll help",
            error: "Error"
        }], [{
            reverseButtons: false,
            headline: "Thank you for helping!",
            blurb: "Please provide your email address or mobile number (US and CA only). After your visit we'll send you a link to the survey. Text Messaging rates apply.",
            attribution: "ForeSee's <a class='fsrPrivacy' href='http://www.foresee.com/privacy-policy.shtml' target='_blank'>Privacy Policy</a>",
            declineButton: "Cancel",
            acceptButton: "email/text me",
            error: "Error",
            mobileExitDialog: {
                support: "b", //e for email only, s for sms only, b for both
                inputMessage: "email or mobile number",
                emailMeButtonText: "email me",
                textMeButtonText: "text me",
                fieldRequiredErrorText: "Enter a mobile number or email address",
                invalidFormatErrorText: "Format should be: name@domain.com or 123-456-7890"
            }
        }]]
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 5,
        lf: 3
    },
    include: {
        urls: [/\.com\//]
    }
}, {
    name: 'browse',
    section: 'adhoc_7',
    platform: 'desktop',
    pin: 1,
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 0,
        lf: 1
    },
    include: {
        urls: ['http://www.marriott.com/common/js/survey/foresee/www.marriott.com/marriott/mvt-super-page.mi2']
    }
}, {
    name: 'browse',
    section: 'adhoc_6',
    platform: 'desktop',
    pin: 1,
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 0,
        lf: 1
    },
    include: {
        urls: ['http://www.marriott.com/common/js/survey/foresee/www.marriott.com/marriott/mvt-super-page.mi2']
    }
}, {
    name: 'browse',
    section: 'adhoc_5',
    platform: 'desktop',
    pin: 1,
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 0,
        lf: 1
    },
    include: {
        urls: ['http://www.marriott.com/common/js/survey/foresee/www.marriott.com/marriott/mvt-super-page.mi2']
    }
}, {
    name: 'browse',
    section: 'adhoc_4',
    platform: 'desktop',
    pin: 1,
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 0,
        lf: 1
    },
    include: {
        urls: ['http://www.marriott.com/common/js/survey/foresee/www.marriott.com/marriott/mvt-super-page.mi2']
    }
}, {
    name: 'browse',
    section: 'adhoc_3',
    platform: 'desktop',
    pin: 1,
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 0,
        lf: 1
    },
    include: {
        urls: ['http://www.marriott.com/common/js/survey/foresee/www.marriott.com/marriott/mvt-super-page.mi2']
    }
}, {
    name: 'browse',
    section: 'adhoc_2',
    platform: 'desktop',
    pin: 1,
    invite: {
        when: 'onentry',
        dialogs: [{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thanks for logging on! You've been randomly chosen to take part in a very brief 5 question survey to let us know about your experience accessing the hotel Internet today.",
            noticeAboutSurvey: "Please take a moment to share your opinions, which are essential in helping us provide the best experience possible.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback"
        }]
    },
    pop: {
        when: 'now'
    },
    criteria: {
        sp: 7,
        lf: 1
    },
    include: {
        urls: [/\.com\/hotels\/einterface\//i]
    }
}, {
    name: 'browse',
    section: 'adhoc_1',
    platform: 'desktop',
    pin: 1,
    invite: {
        when: 'onentry',
        dialogs: [{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            noticeAboutSurvey: "The survey is focused on the 'Groups, Events & Meetings' section within individual hotel property pages. please look for it at the <u>conclusion</u> of your visit.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback"
        }]
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 45,
        lf: 1
    },
    include: {
        urls: [/\.com\/hotels\/event-planning\//i]
    }
}, {
    name: 'browse',
    section: 'jp',
    platform: 'desktop',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 22,
        lf: 2
    },
    include: {
        urls: [/\.jp/]
    }
}, {
    name: 'browse',
    section: 'uk',
    platform: 'desktop',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 3,
        lf: 2
    },
    include: {
        urls: [/\.co\.uk/]
    }
}, {
    name: 'browse',
    section: 'es',
    platform: 'desktop',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 7,
        lf: 2
    },
    include: {
        urls: [/espanol\./]
    }
}, {
    name: 'browse',
    section: 'cn',
    platform: 'desktop',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 15,
        lf: 2
    },
    include: {
        urls: [/\.cn\//]
    }
}, {
    name: 'browse',
    section: 'fr',
    platform: 'desktop',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 21,
        lf: 2
    },
    include: {
        urls: [/\.fr\//]
    }
}, {
    name: 'browse',
    section: 'de',
    platform: 'desktop',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 8,
        lf: 2
    },
    include: {
        urls: [/\.de\//]
    }
}, {
    name: 'lift',
    platform: 'desktop',
    invite: {
        when: 'onentry',
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback"
        }], [{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback"
        }]]
    },
    pop: {
        when: ['now', 'later'],
        now: 'entry',
        later: 'exit'
    },
    criteria: {
        sp: [.8, 1],
        lf: [{
            v: 1,
            o: '='
        }, {
            v: 2,
            o: '>='
        }]
    },
    include: {
        urls: [/\.com\//]
    }
}];
FSR.properties = {
    repeatdays: 90,
    
    repeatoverride: false,
    
    altcookie: {},
    
    language: {
        locale: 'en',
        src: 'location',
        locales: [{
            match: /\.jp/,
            locale: 'jp'
        }, {
            match: /\.co\.uk/,
            locale: 'uk'
        }, {
            match: /\.de\//,
            locale: 'de'
        }, {
            match: /\.fr\//,
            locale: 'fr'
        }, {
            match: /\.cn\//,
            locale: 'cn'
        }, {
            match: /espanol\./,
            locale: 'es'
        }]
    },
    
    exclude: {},
    
    zIndexPopup: 10000,
    
    ignoreWindowTopCheck: false,
    
    ipexclude: 'fsr$ip',
    
    mobileHeartbeat: {
        delay: 60, /*mobile on exit heartbeat delay seconds*/
        max: 3600 /*mobile on exit heartbeat max run time seconds*/
    },
    
    invite: {
    
        // For no site logo, comment this line:
        siteLogo: "Unknown_83_filename"/*tpa=http://www.marriott.com/common/js/survey/foresee/sitelogo.gif*/,
        
        //alt text fore site logo img
        siteLogoAlt: "",
        
        /* Desktop */
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback",
            error: "Error",
            warnLaunch: "this will launch a new window",
            locales: {
                "jp": {
                    headline: "ご意見をお聞かせください。",
                    blurb: "弊社のウェブサイトをご利用くださり、ありがとうございます。このたび、お客様の満足度を改善するプロジェクトの一環として、お客様の満足度に関するアンケートへのご案内を送信させていただきました。",
                    noticeAboutSurvey: "このアンケート調査はお客様の全体的な満足度を把握することを目的としています。本サイトの閲覧<u>終了時</u>にご協力ください。",
                    attribution: "このアンケート調査は、第三者企業であるForeSeeが、ご覧いただいているサイトに代わって実施いたします。",
                    closeInviteButtonText: "ウィンドウを閉じるにはクリックしてください。",
                    declineButton: "いいえ、参加しません",
                    acceptButton: "はい、参加します"
                },
                "uk": {
                    headline: "We'd welcome your feedback!",
                    blurb: "Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
                    noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
                    attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
                    closeInviteButtonText: "Click to close.",
                    declineButton: "No, thanks",
                    acceptButton: "Yes, I'll give feedback"
                },
                "es": {
                    headline: "¡Agradeceremos sus comentarios!",
                    blurb: "Gracias por visitar nuestro sitio web. Ha sido seleccionado para participar en una breve encuesta de satisfacción del cliente para permitirnos conocer cómo mejorar su experiencia.",
                    noticeAboutSurvey: "La encuesta está diseñada para medir su experiencia total, échele un vistazo cuando <u>finalice</u> su visita.",
                    attribution: "Esta encuesta es realizada por una empresa independiente llamada ForeSee, en representación del sitio que está visitando.",
                    closeInviteButtonText: "Haga clic aquí para cerrar esta ventana",
                    declineButton: "No, gracias",
                    acceptButton: "Sí, proporcionaré mis comentarios"
                },
                "cn": {
                    headline: "歡迎您提供意見回饋！",
                    blurb: "感謝您造訪我們的網站。您已獲選參加客戶滿意度意見調查，以協助我們瞭解如何改善您的使用體驗。",
                    noticeAboutSurvey: "本意見調查旨在評估您的整體使用體驗，將會在<u>您要離開本網站</u>時出現。",
                    attribution: "本意見調查是由獨立的公司 ForeSee 代表本網站進行。",
                    closeInviteButtonText: "請按一下這裡以關閉本視窗",
                    declineButton: "不，謝謝",
                    acceptButton: "是的，我願意提供意見回饋"
                },
                "fr": {
                    headline: "Nous aimerions connaître votre opinion",
                    blurb: "Merci de votre visite de notre site. Vous avez été choisi au hasard pour participer à une étude de satisfaction de clientèle qui nous permettra de savoir comment améliorer votre expérience sur notre site web.",
                    noticeAboutSurvey: "L'étude est conçue pour mesurer la totalité de votre opinion sur notre site et apparaîtra à la <u>fin de votre visite.</u>",
                    attribution: "Cette étude est réalisée par ForeSee, une société indépendante, pour le compte du site que vous visitez actuellement.",
                    closeInviteButtonText: "Cliquez ici pour fermer cette fenêtre",
                    declineButton: "Non merci",
                    acceptButton: "Oui, je souhaite donner mon opinion"
                },
                "de": {
                    headline: "Wir würden uns über Ihre Meinung freuen!",
                    blurb: "Vielen Dank für Ihren Besuch auf unserer Website. Sie wurden zufällig ausgewählt, an einer Umfrage zur Kundenzufriedenheit teilzunehmen, anhand der wir nachvollziehen können, wie wir unsere Website noch verbessern können.",
                    noticeAboutSurvey: "Mithilfe der Umfrage soll Ihre gesamte Erfahrung mit der Seite beurteilt werden. Bitte sehen Sie sich die Umfrage am <u>Ende</u> Ihres Besuchs an.",
                    attribution: "Diese Umfrage wird vom unabhängigen Unternehmen ForeSee im Auftrag der Betreiber dieser Seite durchgeführt.",
                    closeInviteButtonText: "Klicken Sie hier, um das Fenster zu schließen",
                    declineButton: "Nein, Danke.",
                    acceptButton: "Ja, ich gebe ein Feedback ab."
                }
            }
        
        }]],

        exclude: {
            urls: ['/search/findHotels', '/search/refineSearch', '/reservation/', '/gst/', 'http://www.marriott.com/common/js/survey/foresee/signIn.mi', 'http://www.marriott.com/common/js/survey/foresee/arabic.marriott.com'],
            referrers: ['http://www.marriott.com/common/js/survey/foresee/google.com'],
            userAgents: [],
            browsers: [],
            cookies: [],
            variables: []
        },
        include: {
            local: ['.']
        },
        
        delay: 0,
        timeout: 0,
        
        hideOnClick: false,
        
        hideCloseButton: false,
        
        css: 'Unknown_83_filename'/*tpa=http://www.marriott.com/common/js/survey/foresee/foresee-dhtml.css*/,
        
        hide: [],
        
        hideFlash: false,
        
        type: 'dhtml',
        /* desktop */
        // url: 'http://www.marriott.com/common/js/survey/foresee/invite.html'
        /* mobile */
        url: 'http://www.marriott.com/common/js/survey/foresee/invite-mobile.html',
        back: 'url'
    
        //SurveyMutex: 'SurveyMutex'
    },
    
    tracker: {
        width: '690',
        height: '415',
        timeout: 3,
        adjust: true,
        alert: {
            enabled: true,
            message: 'The survey is now available.',
            locales: [{
                locale: 'jp',
                message: 'アンケート調査をお受けいただけます。'
            }, {
                locale: 'uk',
                message: 'The survey is now available.'
            }, {
                locale: 'es',
                message: 'Ahora está disponible su encuesta.'
            }, {
                locale: 'cn',
                message: '您現在可以開始填寫意見調查問卷。'
            }, {
                locale: 'fr',
                message: 'Votre étude est maintenant disponible.'
            }, {
                locale: 'de',
                message: 'Ihre Umfrage ist jetzt verfügbar.'
            }]
        },
        url: 'http://www.marriott.com/common/js/survey/foresee/tracker.html',
        locales: [{
            locale: 'jp',
            url: 'http://www.marriott.com/common/js/survey/foresee/tracker_jp.html'
        }, {
            locale: 'uk',
            url: 'http://www.marriott.com/common/js/survey/foresee/tracker_uk.html'
        }, {
            locale: 'es',
            url: 'http://www.marriott.com/common/js/survey/foresee/tracker_es.html'
        }, {
            locale: 'cn',
            url: 'http://www.marriott.com/common/js/survey/foresee/tracker_cn.html'
        }, {
            locale: 'fr',
            url: 'http://www.marriott.com/common/js/survey/foresee/tracker_fr.html'
        }, {
            locale: 'de',
            url: 'http://www.marriott.com/common/js/survey/foresee/tracker_de.html'
        }]
    },
    
    survey: {
        width: 690,
        height: 600
    },
    
    qualifier: {
        footer: '<div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="http://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
        width: '690',
        height: '500',
        bgcolor: '#333',
        opacity: 0.7,
        x: 'center',
        y: 'center',
        delay: 0,
        buttons: {
            accept: 'Continue'
        },
        hideOnClick: false,
        css: 'Unknown_83_filename'/*tpa=http://www.marriott.com/common/js/survey/foresee/foresee-dhtml.css*/,
        url: 'http://www.marriott.com/common/js/survey/foresee/qualifying.html'
    },
    
    cancel: {
        url: 'http://www.marriott.com/common/js/survey/foresee/cancel.html',
        width: '690',
        height: '400'
    },
    
    pop: {
        what: 'survey',
        after: 'leaving-site',
        pu: false,
        tracker: true
    },
    
    meta: {
        referrer: true,
        terms: true,
        ref_url: true,
        url: true,
        url_params: false,
        user_agent: false,
        entry: false,
        entry_params: false
    },
    
    events: {
        enabled: true,
        id: true,
        codes: {
            purchase: 800,
            items: 801,
            dollars: 802,
            followup: 803,
            information: 804,
            content: 805
        },
        pd: 7,
        custom: {
            purchase: {
                enabled: true,
                repeat: false,
                source: 'url',
                patterns: ['http://www.marriott.com/reservation/confirmation.mi']
            },
            dollars: {
                enabled: true,
                repeat: false,
                source: 'variable',
                name: 'FSR_revenue'
            }
        }
    },
    
    previous: false,
    
    analytics: {
        google_local: false,
        google_remote: false
    },
    
    cpps: {},
    
    mode: 'hybrid'
};
