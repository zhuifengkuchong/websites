
 
//Height Equalizer for my stay components
function equalHeight(group){ 
    	var tallest = 0;
    	group.each(function() { 
    		var thisHeight = $(this).height();
    		if(thisHeight > tallest) {
    			tallest = thisHeight;
    		}
    	});
    	group.height(tallest);
    	
    }
    $(function(){ 
	if($('body').hasClass('lt-ie9') && $('.reservation-info-section').length > 0){ 

	equalHeight($('.reservation-info-section'));
	
	}
});
    
var smsPopover = function(obj) {
    var respopover = {},
        thisUrl= obj.attr('href'),
        closeText = msgResources.closeText || 'Close',
        loadingText = msgResources.loadingText || 'Loading...';
    if (obj.length) {
        $.magnificPopup.open({
            mainClass: 'm-modal t-modal-med',
            items: {
                src: thisUrl ,
                type: 'ajax'
            },
            ajax: {
                settings: {
                    dataType: 'html'
                }
            },
            closeMarkup: '<div class="m-modal-utilities">' +
                            '<button class="m-modal-close mfp-close is-hover-fix" type="button">%title% <i class="icon mfp-close icon-close"></i></button>' 
                        + '</div>'
            ,tClose: closeText
            ,tLoading: loadingText
            ,callbacks: {
                beforeOpen: function() {
                    this.container.attr('aria-live', 'assertive');
                }
                ,parseAjax: function(mfpResponse) {
                    var frag = $(mfpResponse.data).find('.l-body-container');
                    mfpResponse.data = frag;
                }
                ,ajaxContentAdded: function() {
                	var self = this;
                	var formConatiner = self.container.find('#sms-contry-code');
                	var smsTermCondition = self.container.find('.js-sms-term');
                	var enTermCondition = self.container.find('.js-en-term');
                	var gbMessaging = self.container.find('.js-gb-msg');
                	var cnTermCondition = self.container.find('.js-cn-term');
                	var actionSelector = self.container.find('form[id=smsForm]'); 
                	var $mfpContainer =self.container.find('.mfp-content');
                	//the sms country code for Great Britain is 44, which has no term and Condition
                	//the sms country code for China and Hong Kong are 86 and 852 which have same term and condition
                	formConatiner.on('change.smsform', function(){
                		enTermCondition.removeClass('is-hidden');
                		smsTermCondition.removeClass('is-hidden');
                		var countryCode = $(this).val();
                		if (countryCode == "44") {
                			gbMessaging.removeClass('is-hidden');
                		    smsTermCondition.addClass('is-transition-hidden');
                		} else if (countryCode == "86" ||countryCode =="852") {
                			smsTermCondition.removeClass('is-transition-hidden');
                			cnTermCondition.removeClass('is-hidden');
                			enTermCondition.addClass('is-transition-hidden');
                 		    gbMessaging.addClass('is-hidden');
                			
                		   
                		} else {
                			smsTermCondition.removeClass('is-transition-hidden');
                		    enTermCondition.removeClass('is-transition-hidden');
                		    gbMessaging.addClass('is-hidden');
                		    cnTermCondition.addClass('is-hidden');
                		}
                	});
                	actionSelector.on('submit.smsform', function(e){
                		 e.preventDefault();
                		 var $form = $(this);
                		 var $formValues = $form.serialize();
                		 var url = $form.prop('action');
                		 $.post(url, $formValues, function(data) {
                			 var content = $(data).find('#sms-panel');
                			 var errorMsg =content.find('#error-messages');
                			 if(errorMsg.length){
                				 $('#error-messages').remove();
                				 gbMessaging.before(errorMsg);
                			 }else{
                				$mfpContainer.find('#sms-panel > :not(.m-modal-utilities)').remove();
                    			$("#sms-panel", $mfpContainer).append(content);
                			 }
                			 
	 		            });
                	});
                }
            }
            
        });
    }
};
    
$(function(){
	$('.js-order-cert-panel').removeClass('hidden');
	if($('#edit-dates-confirmation').length > 0)
	{
		var cancelText = '';
		if(document.getElementById('cancelText')){
			var cancelText = $('#cancelText').val();
		}
		var cancelLink = ' <a href="" class="button-submit cancel" id="cancel" title="'+cancelText+'"><em>'+cancelText+'</em></a>';
		$('#edit-dates-confirmation a#continue').after(cancelLink);
		$('a#cancel').bind('click',function(){
			window.close();
			return false;
		});
		
		$('#edit-dates-confirmation #continue').click(function(){
			var url = $(this).attr('href');
			window.opener.location = url;
			window.close();
			return false;
		});
	}
	
	if($('.print-reservation-container').length > 0)
	{
		$('.reservation-info-section-container').each(function () {
			$(this).children('.reservation-info-section').height($(this).height());
		});	
	}
	
	if($('.reservation-body-container').length > 0)
	{
		$('#cancelBtn').click(function(){
			$('#preferences-fade-section,#preferences-fade-section-hidden').toggle();
			$('#change-preferences-section,#change-preferences-section-active').slideToggle();
			return false;
		});
	}
	/*fix for overflow in IE7 document mode for all IE browser mode*/
	if (($.browser.msie && $.browser.version == '7.0'&& document.documentMode && document.documentMode == '7')||($.browser.msie && $.browser.version == '8.0'&& document.documentMode && document.documentMode == '7')||($.browser.msie && $.browser.version == '9.0'&& document.documentMode && document.documentMode == '7')){
		$('.res-offer-container').css('clear','both');
}
	
	//Review and Confirm: edit preferences
    if($('#edit-preferences').length > 0){
	    $('#edit-preferences').click(function(){
			$('#preferences-fade-section,#preferences-fade-section-hidden').fadeOut();
			$('#change-preferences-section,#change-preferences-section-active').slideToggle();
			return false;
		});
	}
	if($('#reservation-summary-container').length > 0 && $('.print-reservation-container').length == 0){
		//to hide continue button when only have purchase points check box as payment selection
		var purchasePointsCheckBox = $('.selection-container #purchase-points-check')
		if ( $(purchasePointsCheckBox).length > 0 && !$(purchasePointsCheckBox).is(':checked'))
		{
			$('#submit-changes-button').hide();
		}
		
		//When purchase point checkbox clicked
		$(purchasePointsCheckBox).click (function() {
			$('#submit-changes-button').toggle();
		});
	}
	
	if($('#confirm-details').length > 0){
		$('form[id="confirm-details"]').submit(function(){
			$(this).find('button').css('backgroundColor','#999').attr('disabled','disabled');
		});
	}
	
	/* Position Visa placement on Confirmation page */
	$('.offers-container .article-container .column .article-content.layout-6 img').each(function () { 
		//Get image (with a element wrapper)
		var floatedElement = ($(this).parent().is('a')) ? $(this).parent() : $(this);
		// only create divs to float elements if there are no existing divs	
		if (floatedElement.closest('div').hasClass('article-content')){	
			//Wrap text with div
			floatedElement.siblings().wrapAll('<div class="left-column"></div>');
			//Wrap image with div
			floatedElement.wrap('<div class="right-column"></div>'); 
		}
	});
	
	
	var $redemptionToggleContainer = $('.js-rdmptn-toggle-container');
	var $paymentOptionsControl = $('input[name="redemptionSelection"]');
	var _togglePaymentPanel = function(obj) {
		var id = obj.attr("id");
		var content = ".js-" + id + "-panel";
		if(obj.prop("checked") == true) {
			$redemptionToggleContainer.addClass('is-hidden');
			$(content).removeClass('is-hidden');
			//this is for purchase point checkbox
			if ((obj.attr('type') == 'checkbox') && (id == 'purchase-points')) {
				$('.js-purchase-points-checked').show();  
				$('.js-purchase-points-unchecked').hide();
			}
		}
		else {
			$(content).addClass('is-hidden');
			//this is for purchase point checkbox
			if ((obj.attr('type') == 'checkbox') && (id == 'purchase-points')) {
				$('.js-purchase-points-checked').hide();
				$('.js-purchase-points-unchecked').show();
				
			}
		}
	}
	$paymentOptionsControl.each(function(index, row){
		_togglePaymentPanel($(this));
		
	})
	
	$paymentOptionsControl.on('click', function() {
		_togglePaymentPanel($(this));
		
	});
	
	$('.js-purchase-points-unchecked a').on('click', function(){
		$('.js-purchase-points-unchecked').hide();
		$('.js-purchase-points-checked').show();
		$('#purchase-points').prop('checked', true);
	});
	if($('#facebook-dialog').length > 0){
		$('#facebook-dialog').show();
		$("#fb-share-link").click(function(){
			var winTop = window.outerHeight / 2 - 125;
			var winLeft = window.outerWidth / 2 - 275;
			window.open($(this).attr('href'), '_blank', 'top=' + winTop + ',left=' + winLeft + ',width=550,height=250,location=no,menubar=no,scrollbars=no,status=no,toolbar=no');
			return false;
		});
		
	}

	
	var $redemptionToggleContainer = $('.js-rdmptn-toggle-container');
	var $paymentOptionsControl = $('input[name="redemptionSelection"]');

	
	var _togglePaymentPanel = function(obj) {
		var id = obj.attr("id");
		var content = ".js-" + id + "-panel";
		if(obj.prop("checked") == true) {
			$redemptionToggleContainer.addClass('is-hidden');
			$(content).removeClass('is-hidden');
			//this is for purchase point checkbox
			if ((obj.attr('type') == 'checkbox') && (id == 'purchase-points')) {
				$('.js-purchase-points-checked').show();
				$('.js-purchase-points-unchecked').hide();
			}
		}
		else {
			$(content).addClass('is-hidden');
			//this is for purchase point checkbox
			if ((obj.attr('type') == 'checkbox') && (id == 'purchase-points')) {
				$('.js-purchase-points-checked').hide();
				$('.js-purchase-points-unchecked').show();
				
			}
		}
	}
	var $smsContryCode = $('#sms-contry-code');
	var _toggleSmsTerms = function(obj){
		var countryCode = obj.val()
		if (countryCode == "1"){
			$("#sms-term-container").addClass('hidden')
		}
	}
	
	
	$paymentOptionsControl.each(function(index, row){
		_togglePaymentPanel($(this));
		
	})
	
	$paymentOptionsControl.on('click', function() {
		_togglePaymentPanel($(this));
		
	});
	
	
	$('.js-res-popover').on('click', function(e){
		e.preventDefault();
		smsPopover($(this));
	});
	
});



























