// namespace objects
var SSContributorMode = new Object();
SSContributorMode.SSCookie = new Object();
SSContributorMode.SSQueryString = new Object();
SSContributorMode.SSPopup = null;

// environment variables
SSContributorMode.IS_MAC = (navigator.userAgent.indexOf("Mac") != -1);
SSContributorMode.IS_OPERA = (navigator.userAgent.toLowerCase().indexOf("opera") > -1);
SSContributorMode.IS_FIREFOX = (navigator.userAgent.toLowerCase().indexOf("gecko") > -1) && (navigator.userAgent.toLowerCase().indexOf("firefox") > -1);
SSContributorMode.IS_NETSCAPE = (navigator.userAgent.toLowerCase().indexOf("gecko") > -1) && (navigator.userAgent.toLowerCase().indexOf("netscape") > -1);
SSContributorMode.IS_SAFARI = ((navigator.userAgent.toLowerCase().indexOf("applewebkit") > -1) || (navigator.userAgent.toLowerCase().indexOf("apple") > -1));
SSContributorMode.IS_IE = (navigator.userAgent.toLowerCase().indexOf("msie") > -1) && !SSContributorMode.IS_OPERA;
SSContributorMode.IS_MOZILLA = (navigator.userAgent.toLowerCase().indexOf("mozilla") > -1) && !SSContributorMode.IS_IE && !SSContributorMode.IS_OPERA && !SSContributorMode.IS_FIREFOX && !SSContributorMode.IS_NETSCAPE;
SSContributorMode.IS_COMPATIBLE_IE_ENVIRONMENT = (SSContributorMode.IS_IE && !SSContributorMode.IS_MAC);
SSContributorMode.IS_COMPATIBLE_FF_ENVIRONMENT = ((SSContributorMode.IS_MOZILLA || SSContributorMode.IS_FIREFOX) && !SSContributorMode.IS_MAC);
SSContributorMode.IS_CONTRIBUTOR_MODE = (SSContributor && !SSHideContributorUI && !SSForceContributor && (SSContributorMode.IS_COMPATIBLE_IE_ENVIRONMENT || SSContributorMode.IS_COMPATIBLE_FF_ENVIRONMENT));

// utility methods
SSContributorMode.IsString = function(obj) { return (typeof obj == 'string'); }
SSContributorMode.IsNumber = function(obj) { return (typeof obj == 'number'); }
SSContributorMode.IsBoolean = function(obj) { return (typeof obj == 'boolean'); }
SSContributorMode.IsArray = function(obj) { return (obj instanceof Array); }
SSContributorMode.IsFunction = function(obj) { return (typeof obj == 'function'); }
SSContributorMode.IsUndefined = function(obj) { return (typeof obj == 'undefined'); }
SSContributorMode.IsNull = function(obj) { return (obj == null); }
SSContributorMode.IsNotValid = function(obj) { return (SSContributorMode.IsNull(obj) || SSContributorMode.IsUndefined(obj)); }
SSContributorMode.IsValid = function(obj) { return (!SSContributorMode.IsNull(obj) && !SSContributorMode.IsUndefined(obj)); }
SSContributorMode.Trim = function(s) { return s.replace(/^\s*/,'').replace(/\s*$/,''); }
SSContributorMode.Url = window.location.href.toString();
SSContributorMode.UrlBase = window.location.href.split("?")[0].split("#")[0];
SSContributorMode.QueryString = window.location.search;
SSContributorMode.Bookmark = (SSContributorMode.IsString(window.location.href.split("#")[1])) ? "#"+window.location.href.split("#")[1] : "";

// site studio specific properties
SSContributorMode.HideContributorUI = "SSHideContributorUI";
SSContributorMode.ContributorClientId = "ssContributor77";
SSContributorMode.ContributorVarName = "SSContributor";
SSContributorMode.ContributorPreviewId = "previewId";
SSContributorMode.StellentRenderingEngine = "sre";
SSContributorMode.Firefox = "ff";
SSContributorMode.InternetExplorer= "ie";

//***************************************************************************
//***************************************************************************
//***************************************************************************
//                           SSContributorMode
//***************************************************************************
//***************************************************************************
//***************************************************************************

SSContributorMode.Toggle = function()
{
	var hash = SSContributorMode.Bookmark;
	var query = SSContributorMode.QueryString;
	var contributor = SSContributorMode.SSCookie.GetValue(SSContributorMode.ContributorVarName);

	if (SSContributorMode.IS_CONTRIBUTOR_MODE) // disable
	{
		// clean up known query string values - some of which are legacy values
		if (SSContributorMode.IsValid(query) && query.length > 0)
		{
			query = SSContributorMode.SSQueryString.RemoveValue(query, SSContributorMode.ContributorPreviewId);
			query = SSContributorMode.SSQueryString.RemoveValue(query, SSContributorMode.ContributorVarName);
		}

		if (SSContributorMode.SSCookie.GetValue(SSContributorMode.StellentRenderingEngine) == SSContributorMode.Firefox)
		{
			query = SSContributorMode.SSQueryString.SetValue(query, SSContributorMode.StellentRenderingEngine, SSContributorMode.Firefox);
		}

		SSContributorMode.SSCookie.SetValue(SSContributorMode.ContributorVarName, "false");
		SSContributorMode.SSCookie.SetValue(SSContributorMode.ContributorPreviewId, "");

		SSContributorMode.ReleodURL(SSContributorMode.UrlBase + query + hash);
	}
	else // enable
	{
		if (SSContributorMode.IS_COMPATIBLE_IE_ENVIRONMENT)
		{
			SSContributorMode.SSCookie.SetValue(SSContributorMode.ContributorVarName, "true");
		}
		else if (SSContributorMode.IS_COMPATIBLE_FF_ENVIRONMENT)
		{
			query = SSContributorMode.SSQueryString.SetValue(query, SSContributorMode.StellentRenderingEngine, SSContributorMode.InternetExplorer);
			query = SSContributorMode.SSQueryString.SetValue(query, SSContributorMode.ContributorVarName, "true");
		}

		SSContributorMode.ReleodURL(SSContributorMode.UrlBase + query + hash);
	}
}

//***************************************************************************

SSContributorMode.InstallStellentTab = function()
{
	window.location = "http://www.stryker.com/stellent/idcplg?IdcService=SS_SHOW_BROWSER_INSTALL_PAGE&showUrl=" + escape(SSContributorMode.Url);
}

//***************************************************************************

SSContributorMode.ReleodURL = function(url)
{
	if (SSContributorMode.IsValid(url))
	{
		if (SSContributorMode.Url != url)
			window.location = url;
		else
			window.location.reload(true);
	}
}

//***************************************************************************

SSContributorMode.ContributorIconContextMenu = function(dataId)
{
	if (SSContributorMode.IS_CONTRIBUTOR_MODE)
	{
		var oNodeListActions = document.getElementById(dataId).XMLDocument.selectNodes("actions/action");
		var numNodes = oNodeListActions.length;
		var divTagStyle = "STYLE='white-space:nowrap; background:#cccccc; border:1px solid black; border-top:1px solid white; border-left:1px solid white; height:20px; color:black; font-family:verdana; font-weight:bold; padding:2px; padding-left:10px; font-size:8pt; cursor:hand' ";
		var divTagActions = "onmouseover='this.style.background=\"#ffffff\"' onmouseout='this.style.background=\"#cccccc\"'";
		var menu = "";

		if (SSContributorMode.SSPopup == null)
		{
			SSContributorMode.SSPopup = window.createPopup();
		}

		for (n = 0; n < numNodes; n++)
		{
			var oNodeAction = oNodeListActions.item(n);

			menu = menu + "<div ";
			menu = menu + divTagStyle;
			menu = menu + divTagActions;
			menu = menu + ">";
			menu = menu + oNodeAction.xml;
			menu = menu + "</div>";
		}
		SSContributorMode.SSPopup.document.body.innerHTML = menu;

		var popupBody = SSContributorMode.SSPopup.document.body;

		// The following popup object is used only to detect what height the
		// displayed popup object should be using the scrollHeight property.
		// This is important because the size of the popup object varies
		// depending on the length of the definition text. This first
		// popup object is not seen by the user.
		SSContributorMode.SSPopup.show(0, 0, 100, 0);
		var realHeight = popupBody.scrollHeight;
		var realWidth = popupBody.scrollWidth + 10;

		// Hides the dimension detector popup object.
		SSContributorMode.SSPopup.hide();

		// Shows the actual popup object with correct height.
		SSContributorMode.SSPopup.show(0, 15, realWidth, realHeight, event.srcElement);
		return false;
	}
	else
		return false;
}

//***************************************************************************

SSContributorMode.ContributorIconToggle = function()
{

	if (SSContributorMode.IS_CONTRIBUTOR_MODE && SSContributorMode.IsValid(event) && SSContributorMode.IsValid(event.srcElement) && SSContributorMode.IsValid(event.srcElement.src))
	{
		var o = event.srcElement;
		if (o.src.search(/StellentDot_edit.gif/gi) >= 0) o.src = o.src.replace(/StellentDot_edit.gif/gi, "../../../en-us/index.htm"/*tpa=http://www.stryker.com/stellent/websites/en-us/StellentDot_edit_over.gif*/);
		else if (o.src.search(/StellentDot_edit_over.gif/gi) >= 0) o.src = o.src.replace(/StellentDot_edit_over.gif/gi, "../../../en-us/index.htm"/*tpa=http://www.stryker.com/stellent/websites/en-us/StellentDot_edit.gif*/);
		else if (o.src.search(/StellentDot_locked.gif/gi) >= 0) o.src = o.src.replace(/StellentDot_locked.gif/gi, "../../../en-us/index.htm"/*tpa=http://www.stryker.com/stellent/websites/en-us/StellentDot_locked_over.gif*/);
		else if (o.src.search(/StellentDot_locked_over.gif/gi) >= 0) o.src = o.src.replace(/StellentDot_locked_over.gif/gi, "../../../en-us/index.htm"/*tpa=http://www.stryker.com/stellent/websites/en-us/StellentDot_locked.gif*/);
		else if (o.src.search(/StellentDot_workflow.gif/gi) >= 0) o.src = o.src.replace(/StellentDot_workflow.gif/gi, "../../../en-us/index.htm"/*tpa=http://www.stryker.com/stellent/websites/en-us/StellentDot_workflow_over.gif*/);
		else if (o.src.search(/StellentDot_workflow_over.gif/gi) >= 0) o.src = o.src.replace(/StellentDot_workflow_over.gif/gi, "../../../en-us/index.htm"/*tpa=http://www.stryker.com/stellent/websites/en-us/StellentDot_workflow.gif*/);
	}
}

//***************************************************************************

SSContributorMode.LaunchContributorClient = function()
{
	var app = document.getElementById(SSContributorMode.ContributorClientId);
	if (SSContributorMode.IS_CONTRIBUTOR_MODE && SSContributorMode.IsValid(app))
	{
		app.EditContributorFile(document);
	}
}

//***************************************************************************

SSContributorMode.SwitchContributionRegionFile = function(siteId, nodeId, regionId, useSecondary, docName)
{
	var app = document.getElementById(SSContributorMode.ContributorClientId);
	if (SSContributorMode.IS_CONTRIBUTOR_MODE && SSContributorMode.IsValid(app))
	{
		app.SwitchContributorFile(document, siteId, nodeId, regionId, useSecondary, docName);
	}
}

//***************************************************************************

SSContributorMode.ShowContributorModeDiff = function(currentUrl)
{
	var app = document.getElementById(SSContributorMode.ContributorClientId);
	if (SSContributorMode.IS_CONTRIBUTOR_MODE && SSContributorMode.IsValid(app))
	{
		var hash = SSContributorMode.Bookmark;
		var query = SSContributorMode.QueryString;

		// clean up known query string values - some of which are legacy values
		if (SSContributorMode.IsValid(query) && query.length > 0)
		{
			query = SSContributorMode.SSQueryString.RemoveValue(query, SSContributorMode.ContributorPreviewId);
			query = SSContributorMode.SSQueryString.RemoveValue(query, SSContributorMode.ContributorVarName);
		}

		var query1 = SSContributorMode.SSQueryString.SetValue(query, SSContributorMode.ContributorVarName, "false");
		var query2 = SSContributorMode.SSQueryString.SetValue(query, SSContributorMode.ContributorVarName, "true");
		query2 = SSContributorMode.SSQueryString.SetValue(query2, SSContributorMode.HideContributorUI, "1");

		var url1 = SSContributorMode.UrlBase + query1 + hash;
		var url2 = SSContributorMode.UrlBase + query2 + hash;

		SSContributorMode.SSCookie.SetValue(SSContributorMode.ContributorVarName, "false");
		SSContributorMode.SSCookie.SetValue(SSContributorMode.ContributorPreviewId, "");

		app.CompareFiles(url1, url2, true, false);
	}
}

//***************************************************************************

SSContributorMode.DragEventHandler = function()
{
	if (SSContributorMode.IS_CONTRIBUTOR_MODE)
	{
		this.style.left = event.clientX-this.X + document.body.scrollLeft;
		this.style.top = event.clientY-this.Y + document.body.scrollTop;
	}
}

//***************************************************************************

SSContributorMode.MouseDownEventHandler = function()
{
	if (SSContributorMode.IS_CONTRIBUTOR_MODE)
	{
		this.X = event.offsetX;
		this.Y = event.offsetY;
	}
}

//***************************************************************************

SSContributorMode.InsertContributorModeGraphic = function()
{
	SSContributorMode.LanguageId = (SSContributorMode.IsString(g_strLanguageId)) ? g_strLanguageId : "en";
	SSContributorMode.RelativeWebRoot = g_HttpRelativeWebRoot;
	SSContributorMode.ContributorModeImageName = "../../../en-us/index.htm"/*tpa=http://www.stryker.com/stellent/websites/en-us/contributorMode.jpg*/;
	SSContributorMode.ContributorModeImageSrc = SSContributorMode.RelativeWebRoot + "images/sitestudio/" + SSContributorMode.LanguageId + "/" + SSContributorMode.ContributorModeImageName;

	if (document.readyState == "complete")
	{
		var img = document.createElement("IMG");
		img.style.border = "solid black 1px";
		img.style.padding = 20;
		img.style.position = "absolute";
		img.style.cursor = "move";
		img.style.top = 15;
		img.style.left = document.body.clientWidth - 225;
		img.ondrag = SSContributorMode.DragEventHandler;
		img.onmousedown = SSContributorMode.MouseDownEventHandler;
		img.src = SSContributorMode.ContributorModeImageSrc;
		document.body.insertBefore(img);
		document.body.focus();
	}
}

//***************************************************************************

SSContributorMode.EnableContributionMode = function()
{
	var query = SSContributorMode.QueryString;
	if (SSContributorMode.SSQueryString.GetValue(query, SSContributorMode.StellentRenderingEngine) == SSContributorMode.InternetExplorer)
	{
		SSContributorMode.SSCookie.SetValue(SSContributorMode.StellentRenderingEngine, SSContributorMode.Firefox);
	}
	document.attachEvent('onreadystatechange', SSContributorMode.InsertContributorModeGraphic);
}

//***************************************************************************

SSContributorMode.Initialize = function()
{
	if (SSContributorMode.IS_COMPATIBLE_IE_ENVIRONMENT)
	{
		document.attachEvent('onkeydown', SSContributorMode.KeyCommandHandler);
		if (SSContributorMode.IS_CONTRIBUTOR_MODE)
			SSContributorMode.EnableContributionMode();
	}
	else if (SSContributorMode.IS_COMPATIBLE_FF_ENVIRONMENT)
	{
		document.addEventListener('keypress', SSContributorMode.KeyCommandHandler, false);
		if (SSContributorMode.IS_CONTRIBUTOR_MODE)
			SSContributorMode.InstallStellentTab();
	}
}

// ***************************************************************************
//
// The following function uses virtual key codes to determine
// what key or key combination has been pressed by the user.
// The default value is CTRL + SHIFT + F5. The F5 key has a
// Virtual-Key Code of 116 (or 0x74 in hexadecimal). The codes
// for the other typical Function keys, F1 through F12 are
// 112 (0x70) through 123 (0x7B) respectively. For a complete
// list of virtual-key codes please refer to the following URL:
// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/WinUI/WindowsUserInterface/UserInput/VirtualKeyCodes.asp
// Alternatively, do a Google search on the term "Virtual-Key Codes"
// and you should find some useful information.
//
// You may also use the HTML code below to create a test page that
// you can use to capture the values of the keys being pressed.
//
// ***************************************************************************
//  Alter the KeyCommandHandler method to change the key command that
//  enables Contributor Mode. Utilize the following HTML source code to
//  identify the key codes you wish to use for your key commands.
//
//  <html><head><script>
//  function load() { document.body.attachEvent('onkeydown', logKeyDown); }
//  function logKeyDown()
//  {
//   var IsShiftKeyDown = (event.shiftKey) ? "yes<br>" : "no<br>";
//   var IsCtrlKeyDown = (event.ctrlKey) ? "yes<br>" : "no<br>";
//   output.innerHTML += "Key Code: "
//    + event.keyCode + "<br>"
//    + "Is Shift Key Down?: "
//    + IsShiftKeyDown
//    + "Is Ctrl Key Down?: "
//    + IsCtrlKeyDown
//    + "<br>";
//  }
//  </script></head>
//  <body onload="load()"><center>
//  <b>Press a key to reveal its Key Code:</b><br><br>
//  <div id="output"></div>
//  </center>
//  </body>
//  <html>
// ***************************************************************************

SSContributorMode.KeyCommandHandler = function(e)
{
	var eventObject =  (SSContributorMode.IsValid(e)) ? e : event;
	if (eventObject.ctrlKey && eventObject.shiftKey && SSContributorMode.IsValid(eventObject.keyCode) && eventObject.keyCode == 116)
	{
		SSContributorMode.Toggle();
		return false;
	}
}

//***************************************************************
//***************************************************************
//***************************************************************
//                         SSCookie
//***************************************************************
//***************************************************************
//***************************************************************

SSContributorMode.SSCookie.GetValue = function(name)
{

	var arr = document.cookie.split(";");
	for (var i = 0; i < arr.length; i++)
	{
		var crumbs = arr[i].split("=");
		if (name == SSContributorMode.Trim(crumbs[0]))
			return unescape(SSContributorMode.Trim(crumbs[1]));
	}
	return null;
}

//***************************************************************************

SSContributorMode.SSCookie.SetValue = function(name, value)
{
	if (SSContributorMode.IsValid(name) && SSContributorMode.IsValid(value))
	{
		document.cookie = name + "=" + escape(value) + "; path=/";
	}
}

//***************************************************************
//***************************************************************
//***************************************************************
//                        SSQueryString
//***************************************************************
//***************************************************************
//***************************************************************

SSContributorMode.SSQueryString.GetValue = function(query, name)
{
	if (query.indexOf(name) >= 0)
	{
		var q = query.replace(/\?/, '');

		if (SSContributorMode.IsValid(q) && q.length > 0)
		{
			var pairs = q.split("&");
			for (var i = 0; i < pairs.length; i++)
			{
				var p = pairs[0].split("=");
				if (name == p[0])
					return p[1];
			}
		}
	}
	return null;
}

//***************************************************************************

SSContributorMode.SSQueryString.SetValue = function(query, name, value)
{
	var q = SSContributorMode.SSQueryString.RemoveValue(query, name);
	var con = (q.length == 0) ? "?" : (q[q.length-1] == "&") ? "" : "&";
	return q + con + name + "=" + value;
}

//***************************************************************************

SSContributorMode.SSQueryString.RemoveValue = function(query, name)
{
	if (query.indexOf(name) >= 0)
	{
		var q = query.replace(/\?/,'');

		if (SSContributorMode.IsValid(q) && q.length > 0)
		{
			var tmp = "";
			var pairs = q.split("&");
			for (var i = 0; i < pairs.length; i++)
			{
				var p = pairs[i].split("=");
				if (name != p[0])
					tmp += "&" + p[0] + "=" + p[1];
			}
			return tmp.replace(/\&/,'?');
		}
	}
	return query;
}

//***************************************************************
//***************************************************************
//***************************************************************

SSContributorMode.Initialize();

//***************************************************************
//***************************************************************
//***************************************************************

