<script id = "race61a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race61={};
	myVars.races.race61.varName="Object[617].uuid";
	myVars.races.race61.varType="@varType@";
	myVars.races.race61.repairType = "@RepairType";
	myVars.races.race61.event1={};
	myVars.races.race61.event2={};
	myVars.races.race61.event1.id = "search";
	myVars.races.race61.event1.type = "onkeypress";
	myVars.races.race61.event1.loc = "search_LOC";
	myVars.races.race61.event1.isRead = "False";
	myVars.races.race61.event1.eventType = "@event1EventType@";
	myVars.races.race61.event2.id = "Lu_Id_input_11";
	myVars.races.race61.event2.type = "onchange";
	myVars.races.race61.event2.loc = "Lu_Id_input_11_LOC";
	myVars.races.race61.event2.isRead = "True";
	myVars.races.race61.event2.eventType = "@event2EventType@";
	myVars.races.race61.event1.executed= false;// true to disable, false to enable
	myVars.races.race61.event2.executed= false;// true to disable, false to enable
</script>

