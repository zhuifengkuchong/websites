<script id = "race93b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race93={};
	myVars.races.race93.varName="Object[617].uuid";
	myVars.races.race93.varType="@varType@";
	myVars.races.race93.repairType = "@RepairType";
	myVars.races.race93.event1={};
	myVars.races.race93.event2={};
	myVars.races.race93.event1.id = "Lu_Id_input_5";
	myVars.races.race93.event1.type = "onblur";
	myVars.races.race93.event1.loc = "Lu_Id_input_5_LOC";
	myVars.races.race93.event1.isRead = "True";
	myVars.races.race93.event1.eventType = "@event1EventType@";
	myVars.races.race93.event2.id = "Lu_Id_input_6";
	myVars.races.race93.event2.type = "onblur";
	myVars.races.race93.event2.loc = "Lu_Id_input_6_LOC";
	myVars.races.race93.event2.isRead = "False";
	myVars.races.race93.event2.eventType = "@event2EventType@";
	myVars.races.race93.event1.executed= false;// true to disable, false to enable
	myVars.races.race93.event2.executed= false;// true to disable, false to enable
</script>

