<script id = "race79a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race79={};
	myVars.races.race79.varName="Object[617].uuid";
	myVars.races.race79.varType="@varType@";
	myVars.races.race79.repairType = "@RepairType";
	myVars.races.race79.event1={};
	myVars.races.race79.event2={};
	myVars.races.race79.event1.id = "Lu_Id_input_7";
	myVars.races.race79.event1.type = "onfocus";
	myVars.races.race79.event1.loc = "Lu_Id_input_7_LOC";
	myVars.races.race79.event1.isRead = "False";
	myVars.races.race79.event1.eventType = "@event1EventType@";
	myVars.races.race79.event2.id = "Lu_Id_input_6";
	myVars.races.race79.event2.type = "onfocus";
	myVars.races.race79.event2.loc = "Lu_Id_input_6_LOC";
	myVars.races.race79.event2.isRead = "True";
	myVars.races.race79.event2.eventType = "@event2EventType@";
	myVars.races.race79.event1.executed= false;// true to disable, false to enable
	myVars.races.race79.event2.executed= false;// true to disable, false to enable
</script>

