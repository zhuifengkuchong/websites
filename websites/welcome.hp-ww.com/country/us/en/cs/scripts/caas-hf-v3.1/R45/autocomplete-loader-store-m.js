
(function(){if(!Object['keys']){Object.keys=function(obj){var keys=[];for(var i in obj){if(obj.hasOwnProperty(i)){keys.push(i);}}
return keys;};}
DefaultAutocompleteSSLoader=new Class({Implements:Options,options:{language:'',country:'',url:'',count:5,callbackParam:'cbf'},templates:{list:'<ul>{items}</ul>',item:'<li><a id="i{i}" href="javascript:void(0);" class="js_suggestion_text" data-suggestion="{suggestion}">'+'{suggestion}'+'</a></li>'},initialize:function(options){this.setOptions(options||{});this.type='hho-store';},getSuggestions:function(query,callback){var params={term:query,lang:this.options.language},result={'query':query,'html':'','itemsCount':0},self=this;var seggestionsRequest=new Request.JSONP({url:this.options.url,data:params,callbackKey:this.options.callbackParam,onFailure:function(){callback(null);},onException:function(){callback(null);},onSuccess:function(data){if(data){var items=data.terms,itemsHtml='';if(items.length>0){Array.some(items,function(item,index){if(index==self.options.count)return true;itemsHtml+=self.templates.item.substitute({i:index,suggestion:Object.keys(item)[0]});});result.html=self.templates.list.substitute({items:itemsHtml});result.itemsCount=items.length>self.options.count?self.options.count:items.length;}}
callback(result);}});seggestionsRequest.send();},getParams:function(){return this.options;},setParams:function(params){this.setOptions(params);}});})();window.autocompleteLoader.addLoader(new DefaultAutocompleteSSLoader());

/*
Date: 3/15/2015 2:30:38 PM
All images published
*/