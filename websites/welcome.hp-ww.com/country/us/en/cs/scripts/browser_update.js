//browser-update.org notification script, <browser-update.org>
//Copyright (c) 2007-2009, MIT Style License <browser-update.org/LICENSE.txt>
var $buo = function(op,test) {
var jsv=5;
var n = window.navigator,b;
this.op=op||{};
//options
this.op.l = op.l||n["language"]||n["userLanguage"]||document.documentElement.getAttribute("lang")||"en";
this.op.vsakt = {i:9,f:10,o:12,s:5.1,n:20};
this.op.vsdefault = {i:6,f:13,o:10.1,s:3.2,n:10};
this.op.vs =op.vs||this.op.vsdefault;
for (b in this.op.vsakt)
    if (this.op.vs[b]>=this.op.vsakt[b])
        this.op.vs[b]=this.op.vsakt[b]-0.05;

if (!op.reminder || op.reminder<0.1 )
    this.op.reminder=0;
else
    this.op.reminder=op.reminder||24;

this.op.onshow = op.onshow||function(o){};
this.op.url= op.url||"http://browser-update.org/update.html";
this.op.pageurl = op.pageurl || window.location.hostname || "unknown";
this.op.newwindow=op.newwindow||false;

this.op.test=test||op.test||false;
if (window.location.hash=="#test-bu")
    this.op.test=true;

function getBrowser() {
    var n,v,t,ua = navigator.userAgent;
    var names={i:'Internet Explorer',f:'Firefox',o:'Opera',s:'Apple Safari',n:'Netscape Navigator', c:"Chrome", x:"Other"};
    if (/like firefox|chromeframe|seamonkey|opera mini|min|meego|netfront|moblin|maemo|arora|camino|flot|k-meleon|fennec|kazehakase|galeon|android|mobile|iphone|ipod|ipad|epiphany|rekonq|symbian|webos/i.test(ua)) n="x";
    else if (/Trident.(\d+\.\d+)/i.test(ua)) n="io";
    else if (/MSIE.(\d+\.\d+)/i.test(ua)) n="i";
    else if (/Chrome.(\d+\.\d+)/i.test(ua)) n="c";
    else if (/Firefox.(\d+\.\d+)/i.test(ua)) n="f";
    else if (/Version.(\d+.\d+).{0,10}Safari/i.test(ua))	n="s";
    else if (/Safari.(\d+)/i.test(ua)) n="so";
    else if (/Opera.*Version.(\d+\.?\d+)/i.test(ua)) n="o";
    else if (/Opera.(\d+\.?\d+)/i.test(ua)) n="o";
    else if (/Netscape.(\d+)/i.test(ua)) n="n";
    else return {n:"x",v:0,t:names[n]};
    if (n=="x") return {n:"x",v:0,t:names[n]};
    
    v=new Number(RegExp.$1);
    if (n=="so") {
        v=((v<100) && 1.0) || ((v<130) && 1.2) || ((v<320) && 1.3) || ((v<520) && 2.0) || ((v<524) && 3.0) || ((v<526) && 3.2) ||4.0;
        n="s";
    }
    if (n=="i" && v==7 && window.XDomainRequest) {
        v=8;
    }
    if (n=="io") {
        n="i";
        if (v>5) v=10;
        else if (v>4) v=9;
        else if (v>3.1) v=8;
        else if (v>3) v=7;
        else v=9;
    }	
    return {n:n,v:v,t:names[n]+" "+v}
}

this.op.browser=getBrowser();
if (!this.op.test && (!this.op.browser || !this.op.browser.n || this.op.browser.n=="x" || this.op.browser.n=="c" || document.cookie.indexOf("browserupdateorg=pause")>-1 || this.op.browser.v>this.op.vs[this.op.browser.n]))
    return;


if (!this.op.test) {
    var i = new Image();
}
if (this.op.reminder>0) {
    var d = new Date(new Date().getTime() +1000*3600*this.op.reminder);
    document.cookie = 'browserupdateorg=pause; expires='+d.toGMTString()+'; path=/';
}
var ll=this.op.l.substr(0,2);
var languages = "ja";
if (languages.indexOf(ll)!==false)
    this.op.url="http://ie.microsoft.com/";
var tar="";
if (this.op.newwindow)
    tar=' target="_blank"';

function busprintf() {
    var args=arguments;
    var data = args[ 0 ];
    for( var k=1; k<args.length; ++k ) {
        data = data.replace( /%s/, args[ k ] );
    }
    return data;
}

var t = 'Your browser (%s) is <b>out of date</b>. It has known <b>security flaws</b> and may <b>not display all features</b> of this and other websites. \
         <a%s>Learn how to update your browser</a>';
if (ll=="de")
    t = 'Ihr Browser (%s) ist <b>veraltet</b>. Er weist bekannte <b>Sicherheitslücken</b> auf und zeigt möglicherweise <b>nicht alle Features</b> dieser und anderer Websites an. <a%s>Hier erfahren Sie mehr darüber, wie Sie Ihren Browser aktualisieren</a>';
else if (ll=="it")
    t = 'Il vostro browser (%s) è <b>obsoleto</b>. Presenta <b>minacce di sicurezza</b> conosciute e potrebbe <b>non visualizzare tutte le funzioni</b> su questo o su un altro sito Web <a%s>Scoprite come aggiornare il vostro browser</a>';
else if (ll=="pl")
    t = 'Przeglądarka (%s) jest <b>nieaktualna</b>. Występuje w niej znany <b>błąd dotyczący zabezpieczeń</b> i może <b>nie wyświetlać wszystkich elementów</b> tej witryny lub innych witryn. <a%s>Dowiedz się, jak zaktualizować przeglądarkę</a>';
else if (ll=="es")
    t = 'Su navegador (%s) está <b>obsoleto</b>. Ha tenido <b>fallos de seguridad</b> y puede <b>que no muestre todas las funciones</b> de este y otros sitios web. <a%s>Obtenga más información sobre cómo actualizar su navegador</a>';
else if (ll=="nl")
    t = 'Uw browser (%s) is <b>verouderd</b>. Hij heeft <b>beveiligingsrisico\'s</b> en kan mogelijk <b>niet alle kenmerken weergeven</b> van deze en andere websites. <a%s>Leer hoe u uw browser kunt updaten</a>';
else if (ll=="pt")
    t = 'Seu navegador (%s) está <b>desatualizado</b>. Ele possui <b>falhas de segurança</b> conhecidas e pode <b>não exibir todos os recursos</b> deste e de outros sites. <a%s>Saiba como atualizar seu navegador</a>';
else if (ll=="sl")
    t = 'Vaš brskalnik (%s) je <b>zastarel</b>. Ima znane <b>težave z varnostjo</b>, zato to spletno mesto in druga spletna mesta morda <b>ne bodo v celoti prikazana</b>. <a%s>Več informacij o tem, kako posodobiti brkalnik</a>';
else if (ll=="ru")
    t = 'Вы используете <b>устаревшую версию</b> браузера (%s). Браузер имеет <b>уязвимости</b> в системе защиты и может <b>не отображать все элементы</b> этого или других веб-сайтов. <a%s>Подробнее о том, как обновить браузер</a>';
else if (ll=="id")
    t = 'Browser Anda (% s) sudah <b>kedaluarsa</b>. Browser yang Anda pakai memiliki <b>kelemahan keamanan</b> dan mungkin <b>tidak dapat menampilkan semua fitur</b> dari situs Web ini dan lainnya. <a%s> Pelajari cara memperbarui browser Anda</a>';
else if (ll=="uk")
    t = 'Your browser (%s) is <b>out of date</b>. It has known <b>security flaws</b> and may <b>not display all features</b> of this and other websites. \
         <a%s>Learn how to update your browser</a>';
else if (ll=="ko")
    t = '지금 사용하고 계신 브라우저(%s)는 <b>오래되었습니다.</b> 알려진 <b>보안 취약점</b>이 존재하며, 새로운 웹 사이트가 <b>깨져 보일 수도</b> 있습니다. <a%s>브라우저를 어떻게 업데이트하나요?</a>';
else if (ll=="rm")
    t = 'Tes navigatur (%s) è <b>antiquà</b>. El cuntegna <b>problems da segirezza</b> enconuschents e mussa eventualmain <b>betg tut las funcziuns</b> da questa ed autras websites. <a%s>Emprenda sco actualisar tes navigatur</a>.';
else if (ll=="ja")	
	t = 'お使いのブラウザー(%s)は<b>古いバージョン</b>です。 このブラウザーには<b>脆弱性</b>があり、このWebサイト、またはその他のWebサイトの<b>すべての情報が表示されない</b>可能性があります。 <a%s>ブラウザーのアップデート方法の詳細はこちら</a>';
else if (ll=="fr")
	t = 'Votre navigateur (%s) n\'est <b>pas à jour</b>. Il comporte des <b>failles de sécurité</b> connues et peut <b>ne pas afficher toutes les fonctions</b> de ce site Web ou d\'autres sites. <a%s>Découvrez comment mettre à jour votre navigateur</a>';
else if (ll=="da")
        t = 'Din browser (%s) er <b>ikke opdateret</b>. Den har kendte <b>sikkerhedshuller</b> og kan muligvis <b>ikke vise alle funktionerne</b> på dette og andre websteder. <a%s>Få mere at vide om, hvordan du kan opdatere din browser</a>';
else if (ll=="al")
        t = 'Shfletuesi juaj (%s) është <b>ca i vjetër</b>. Ai ka <b>të meta sigurie</b> të njohura dhe mundet të <b>mos i shfaqë të gjitha karakteristikat</b> e kësaj dhe shumë faqeve web të tjera. <a%s>Mësoni se si të përditësoni shfletuesin tuaj</a>';
else if (ll=="ca")
        t = 'El teu navegador (%s) està <b>desactualitzat</b>. Té <b>vulnerabilitats</b> conegudes i pot <b>no mostrar totes les característiques</b> d\'aquest i altres llocs web. <a%s>Aprèn a actualitzar el navegador</a>';
else if (ll=="tr")
    t = 'Tarayıcınız (%s), <b>güncel değil</b>. Bilinen <b>güvenlik hataları</b> içermekte olup, bu ve diğer web sitelerine ait <b>tüm özellikleri görüntüleyemeyebilir</b>. <a%s>Tarayıcınızı nasıl güncelleyeceğinizi öğrenin</a>';
else if (ll=="zh")
    t = '您的瀏覽器 (%s) 已<b>過時</b>。 其含有<b>安全性問題</b>，而且可能<b>不會顯示</b>此網站及其他網站上的所有功能。 <a%s>瞭解如何更新您的瀏覽器</a>';
else if (ll=="sv")
    t = 'Din webbläsare (%s) är <b>för gammal</b>. Den har kända <b>säkerhetsbrister</b> och kanske <b>inte visar alla funktioner</b> på den här och andra webbplatser. <a%s>Läs om hur du uppdaterar din webbläsare</a>';
else if (ll=="sr")
    t = 'Vaš pregledač (%s) je <b>zastareo</b>. On ima poznate <b>bezbednosne nedostatke</b> i možda <b>neće prikazati sve funkcije</b> ove i drugih Veb lokacija. <a%s>Saznajte kako da ažurirate pregledač</a>';
else if (ll=="ro")
    t = 'Browserul (%s) este <b>învechit</b>. Are <b>riscuri de securitate</b> cunoscute şi este posibil <b>să nu afişeze toate caracteristicile</b> acestui site web şi ale altora. <a%s>Aflaţi cum să vă actualizaţi browserul</a>';
else if (ll=="no")
    t = 'Nettleseren din (%s) er <b>utdatert</b>. Den har kjente <b>sikkerhetssvakheter</b> og vil kanskje <b>ikke vise alle funksjonene</b> på dette og andre nettsteder. <a%s>Lær hvordan du oppdaterer nettleseren</a>';
else if (ll=="lt")
    t = 'Jūsų naršyklė (%s) <b>pasenusi</b>. Jai būdingi žinomi, <b>su sauga susiję trikdžiai</b>; ja naudojantis gali būti <b>neparodytos visos</b> šios ir kitų svetainių funkcijos. <a%s>Sužinokite, kaip atnaujinti naršyklę</a>';
else if (ll=="lv")
    t = 'Jūsu pārlūkprogramma (%s) ir <b>novecojusi</b>. Tai ir zināmi <b>drošības trūkumi</b> un tā var <b>neparādīt visus līdzekļus</b> šajā un citās tīmekļa vietnēs. <a%s>Uzziniet, kā atjaunināt savu pārlūkprogrammu</a>';
else if (ll=="hu")
    t = 'Az Ön böngészője (%s) <b>elavult</b>. Ismert <b>biztonsági hibái</b> vannak, és előfordulhat, hogy <b>nem jelenít meg minden funkciót</b> e honlapon és másokon. <a%s>Tudja meg, miként frissítheti a böngészőjét</a>';
else if (ll=="gr")
    t = 'Το πρόγραμμα περιήγησης (%s) <b>δεν είναι ενημερωμένο</b>. Παρουσιάζει γνωστά <b>προβλήματα ασφάλειας</b> και ενδέχεται <b>να μην εμφανίζει όλες τις λειτουργίες</b> αυτής και άλλων τοποθεσιών web. <a%s>Μάθετε πώς μπορείτε να ενημερώσετε το πρόγραμμα περιήγησης</a>';
else if (ll=="fi")
    t = 'Selaimesi (%s) on <b>vanhentunut</b>. Siinä on tunnettuja <b>tietoturva-aukkoja</b>, eikä se välttämättä <b>pysty näyttämään kaikkia</b> tämän ja muiden verkkosivujen ominaisuuksia. <a%s>Lue, kuinka selain päivitetään</a>';
else if (ll=="et")
    t = 'Teie brauser (%s) on <b>aegunud</b>. Sellel on teadaolevaid <b>turbevigu</b> ja see <b>ei pruugi kuvada kõiki</b> selle ja muude veebisaitide funktsioone. <a%s>Lisateave brauseri uuendamise kohta</a>';
else if (ll=="cs")
    t = 'Prohlížeč (%s) je <b>zastaralý</b>. Je známý svými <b>bezpečnostními trhlinami</b> a nemusí <b>zobrazit všechny funkce</b> této nebo jiných webových stránek. <a%s>Informace o aktualizaci prohlížeče</a>';
else if (ll=="hr")
    t = 'Vaš je preglednik (%s) <b>zastario</b>. Ima poznate <b>sigurnosne propuste</b> i možda <b>neće prikazati sve značajke</b> ovog ili drugih web-mjesta. <a%s>Saznajte kako nadograditi preglednik</a>';
else if (ll=="bg")
    t = 'Вашият браузър (%s) е <b>остарял</b>. Той има <b>пробив в защитата</b> и може <b>да не покаже всички функции</b> на този и други уеб сайтове. <a%s>Научете как да актуализирате браузъра</a>';
else if (ll=="ar")
    t = 'مستعرضك (%s) هو <b> منتهي الصلاحية</b>. قد تعرّف على <b>أخطاء الأمان</b> وقد لا يتمكن <b>من عرض جميع ميزات</b> هذا الموقع وباقي موقع الويب الأخرى. <a%s>تعرّف على كيفية تحديث مستعرضك</a>';

		 
if (op.text)
    t = op.text;

this.op.text=busprintf(t,this.op.browser.t,' href="'+this.op.url+'"'+tar);

var div = document.createElement("div");
this.op.div = div;
div.id="buorg";
div.className="buorg";
div.innerHTML= '<div>' + this.op.text + '<div id="buorgclose">X</div></div>';

var sheet = document.createElement("style");
if(!document.getElementById("cookie_privacy_holder")) {
	var style = ".buorg {position:absolute;z-index:111111;\
		width:100%; top:0px; left:0px; \
		border-bottom:1px solid #A29330; \
		background:#FDF2AB no-repeat 10px center url(http://browser-update.org/img/dialog-warning.gif);\
		text-align:left; cursor:pointer; \
		font-family: Arial,Helvetica,sans-serif; color:#000; font-size: 12px;}\
		.buorg div { padding:5px 36px 5px 40px; } \
		.buorg a,.buorg a:visited  {color:#E25600; text-decoration: underline;}\
		#buorgclose { position: absolute; right: .5em; top:.2em; height: 20px; width: 12px; font-weight: bold;font-size:14px; padding:0; }";
}
else {
	var style = ".buorg {position:absolute;z-index:111111;\
		width:100%; top:0px; left:0px; \
		border-bottom:1px solid #A29330; \
		background:#FDF2AB no-repeat 10px center url(http://browser-update.org/img/dialog-warning.gif);\
		text-align:left; cursor:pointer; \
		font-family: Arial,Helvetica,sans-serif; color:#000; font-size: 12px;}\
		.buorg div { padding:5px 36px 5px 40px; } \
		.buorg a,.buorg a:visited  {color:#E25600; text-decoration: underline;}\
		#buorgclose { position: absolute; right: .5em; top:.2em; height: 20px; width: 12px; font-weight: bold;font-size:14px; padding:0; }\
		#cookie_privacy_popup {margin-top: 25px}";
}
document.body.insertBefore(div,document.body.firstChild);
document.getElementsByTagName("head")[0].appendChild(sheet);
try {
    sheet.innerText=style;
    sheet.innerHTML=style;
}
catch(e) {
    try {
        sheet.styleSheet.cssText=style;
    }
    catch(e) {
        return;
    }
}

var hpeucv_cph = document.getElementById('cookie_privacy_holder');
var hpeucv_cpp = document.getElementById('cookie_privacy_popup');

if(hpeucv_cph) { //privacy cookie banner found on page
	hpeucv_cph.style.paddingTop = "142px";
	hpeucv_cpp.style.marginTop = "27px";
}

var me=this;
div.onclick=function(){
    if (me.op.newwindow)
        window.open(me.op.url,"_blank");
    else
        window.location.href=me.op.url;
    return false;
};
div.getElementsByTagName("a")[0].onclick = function(e) {
    var e = e || window.event;
    if (e.stopPropagation) e.stopPropagation();
    else e.cancelBubble = true;
    return true;
}

this.op.bodymt = document.body.style.marginTop;
document.body.style.marginTop = (div.clientHeight)+"px";
document.getElementById("buorgclose").onclick = function(e) {
    var e = e || window.event;
    if (e.stopPropagation) e.stopPropagation();
    else e.cancelBubble = true;
    me.op.div.style.display="none";
    document.body.style.marginTop = me.op.bodymt;
    return true;
}
op.onshow(this.op);

}
var $buoop = $buoop||{};
$bu=$buo($buoop);


/* UnCompressed - Reason: UNKNOWN REASON */

/*
Date: 8/16/2012 12:05:41 PM
Non-published images:
/webdav/17%20United%20States-English%20Web/Building%20Blocks/System/00%20Shared/Content/CSS/i/http://browser-update.org/img/dialog-warning.gif
*/