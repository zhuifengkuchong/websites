// JavaScript Document
$(document).ready(function() {
        
    if($('#contactus li.menutitle').length > 0) {
        $('#contactus li.menutitle').click(function() {
            if ($(this).hasClass('open menutitle')) {
                $(this).removeClass('open menutitle');
                $(this).addClass('close menutitle');
                $(this).next().slideDown(150);
                return false;
            } else if ($(this).hasClass('close menutitle')) {
                $(this).removeClass('close menutitle');
                $(this).addClass('open menutitle');
                $(this).next().slideUp(150);
                return false;
            }            
        });
    }
    
});

$(document).ready(function() {
        
    if($('#collapsible li.menutitle').length > 0) {
        $('#collapsible li.menutitle').click(function() {
            if ($(this).hasClass('open menutitle')) {
                $(this).removeClass('open menutitle');
                $(this).addClass('close menutitle');
                $(this).next().slideDown(150);
                return false;
            } else if ($(this).hasClass('close menutitle')) {
                $(this).removeClass('close menutitle');
                $(this).addClass('open menutitle');
                $(this).next().slideUp(150);
                return false;
            }            
        });
    }
    
});