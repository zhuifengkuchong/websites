/**
*    @name                            Accordion
*    @descripton                        This Jquery plugin makes creating accordions pain free
*    @version                        1.3
*    @requires                        Jquery 1.2.6+
*
*    @author                            Jan Jarfalk
*    @author-email                    jan.jarfalk@unwrongest.com
*    @author-website                    http://www.unwrongest.com
*
*    @licens                            MIT License - http://www.opensource.org/licenses/mit-license.php
*/
(function(jQuery){
     jQuery.fn.extend({  
        accordion: function()
        {       
            return this.each(function() {
                var $ul = $(this);
                
                if($ul.data('accordiated'))
                    return false;
                                                    
                $.each($ul.find('ul, li>div'), function(){
                    $(this).data('accordiated', true);
                    $(this).hide();
                });
                
                $.each($ul.find('a'), function(){
                    $(this).click(function(e){
                        activate(this);
                        return void(0);
                    });
                });
            });
        }
    }); 
    
    jQuery.fn.extend({
        accordionExpand: function(CurrentNavItemTitles)
        {
            return this.each(function() {
                var CurrentNavItemTitle;
                var actives = new Array();

                while((actives.length == 0) && (CurrentNavItemTitles.length > 0))
                {
                    CurrentNavItemTitle = CurrentNavItemTitles.shift();
                    actives = $(this).find('a:contains(' + CurrentNavItemTitle + ')');
                }

                var active;
                $(actives).each(function(index) {
                    if($(this).text() == CurrentNavItemTitle)
                        active = $(this);
                });

                if(active){
                    activate(active, 'toggle');
                    $(active).parents().show();
                }
            });
        }
    });
    
    function activate(el,effect){
        $(el).parents('li').toggleClass('active').siblings().removeClass('active').children('ul, div').slideUp('fast');
        $(el).siblings('ul, div')[(effect || 'slideToggle')]((!effect)?'fast':null);
    }
})(jQuery);