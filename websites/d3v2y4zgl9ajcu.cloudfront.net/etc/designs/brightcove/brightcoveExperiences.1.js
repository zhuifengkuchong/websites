var BCLplayer;
var BCLexperienceModule;
var BCLvideoPlayer;
var BCLcurrentVideo;

//listener for player error
function onPlayerError(event) {
    /* */
}

//listener for when player is loaded
function onPlayerLoaded(id) {
  // newLog();
//  log("EVENT: onPlayerLoaded");
    try {
  		BCLplayer = brightcove.getExperience(id);
  		if (BCLplayer== null || typeof BCLplayer ==="undefined") BCLplayer = brightcove.api.getExperience(id);
    } catch(e) {
    }
  BCLexperienceModule = BCLplayer.getModule(APIModules.EXPERIENCE);
}

//listener for when player is ready
function onPlayerReady(event) {
 // log("EVENT: onPlayerReady");

  // get a reference to the video player module
  BCLvideoPlayer = BCLplayer.getModule(APIModules.VIDEO_PLAYER);
  // add a listener for media change events
  BCLvideoPlayer.addEventListener(BCMediaEvent.BEGIN, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.COMPLETE, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.CHANGE, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.ERROR, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.PLAY, onMediaBegin);
  BCLvideoPlayer.addEventListener(BCMediaEvent.STOP, onMediaBegin);

    if (typeof customBrightcoveListener !== "undefined"){
        //added 2nd (optional) event listener for setting up custom callbacks on a per-project basis.
        BCLvideoPlayer.addEventListener(BCMediaEvent.BEGIN, customBrightcoveListener);
        BCLvideoPlayer.addEventListener(BCMediaEvent.COMPLETE, customBrightcoveListener);
        BCLvideoPlayer.addEventListener(BCMediaEvent.CHANGE, customBrightcoveListener);
        BCLvideoPlayer.addEventListener(BCMediaEvent.ERROR, customBrightcoveListener);
        BCLvideoPlayer.addEventListener(BCMediaEvent.PLAY, customBrightcoveListener);
        BCLvideoPlayer.addEventListener(BCMediaEvent.STOP, customBrightcoveListener);
        BCLvideoPlayer.addEventListener(BCMediaEvent.PROGRESS, customBrightcoveListener);
        BCLvideoPlayer.addEventListener(BCMediaEvent.SEEK_NOTIFY, customBrightcoveListener);
    }
}  

if (customBC == undefined) {
    var customBC = {};
    customBC.createElement = function (el) {
        if (document.createElementNS) {
            return document.createElementNS('http://www.w3.org/1999/xhtml', el);
        } else {
            return document.createElement(el);
        }
    };

    customBC.createVideo = function (width,height,playerID,playerKey,videoPlayer,VideoRandomID) {
    	var innerhtml = '<object id="myExperience_'+VideoRandomID+'" class="BrightcoveExperience">';
		innerhtml += '<param name="bgcolor" value="#FFFFFF" />';
        if(width !== '0'){
            innerhtml += '<param name="width" value="'+width+'" />';
        }
        if(height !== '0'){
            innerhtml += '<param name="height" value="'+height+'" />';
        }
		innerhtml += '<param name="playerID" value="'+playerID+'" />';
		innerhtml += '<param name="playerKey" value="'+playerKey+'" />';
		innerhtml += '<param name="isVid" value="true" />';
		innerhtml += '<param name="isUI" value="true" />';
		innerhtml += '<param name="dynamicStreaming" value="true" />';
		innerhtml += '<param name="@videoPlayer" value="'+videoPlayer+'" />';
		if ( window.location.protocol == 'https:') innerhtml += '<param name="secureConnections" value="true" /> <param name="secureHTMLConnections" value="true" />';
		innerhtml += '<param name="templateLoadHandler" value="onPlayerLoaded" />';
		innerhtml += '<param name="templateReadyHandler" value="onPlayerReady" />';
		innerhtml += '<param name="templateErrorHandler" value="onPlayerError" />';
		innerhtml += '<param name="includeAPI" value="true" /> ';
		innerhtml += '<param name="wmode" value="transparent" />';
		innerhtml += '</object>';
		var objID = document.getElementById(VideoRandomID);
		objID.innerHTML = innerhtml;
		var apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "../../../../sadmin.brightcove.com/js/BrightcoveExperiences-1.js"/*tpa=https://sadmin.brightcove.com/js/BrightcoveExperiences.js*/;
		
		//ensure that BrightcoveExperiences is fully loaded before calling createExperiences
		apiInclude.onload = apiInclude.onreadystatechange = function(){
			try{
				brightcove.createExperiences();
			}catch(e){
				
			}	
        }
		objID.parentNode.appendChild(apiInclude);
		
		apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "../../../../sadmin.brightcove.com/js/APIModules_all-1.js"/*tpa=https://sadmin.brightcove.com/js/APIModules_all.js*/;
		objID.parentNode.appendChild(apiInclude);
		
		apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "../../../../files.brightcove.com/bc-mapi.js"/*tpa=https://files.brightcove.com/bc-mapi.js*/;
		objID.parentNode.appendChild(apiInclude);
		
		/*
		 * apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.text  = "window.onload = function() {brightcove.createExperiences();};";
		//objID.parentNode.appendChild(apiInclude);
		*/
    };
    customBC.createVideoPlaylist = function (width,height,playerID,playerKey,videoPlayer,VideoRandomID) {
    	var innerhtml = '<object id="myExperience_'+VideoRandomID+'" class="BrightcoveExperience">';
		innerhtml += '<param name="bgcolor" value="#FFFFFF" />';
        if(width !== '0'){
            innerhtml += '<param name="width" value="'+width+'" />';
        }
        if(height !== '0'){
            innerhtml += '<param name="height" value="'+height+'" />';
        }
		innerhtml += '<param name="playerID" value="'+playerID+'" />';
		innerhtml += '<param name="playerKey" value="'+playerKey+'" />';
		innerhtml += '<param name="isVid" value="true" />';
		innerhtml += '<param name="isUI" value="true" />';
		innerhtml += '<param name="dynamicStreaming" value="true" />';
		innerhtml += '<param name="@playlistTabs"  value="'+videoPlayer+'" />';
		if ( window.location.protocol == 'https:') innerhtml += '<param name="secureConnections" value="true" /> <param name="secureHTMLConnections" value="true" />';
		innerhtml += '<param name="templateLoadHandler" value="onPlayerLoaded" />';
		innerhtml += '<param name="templateReadyHandler" value="onPlayerReady" />';
		innerhtml += '<param name="templateErrorHandler" value="onPlayerError" />';
		innerhtml += '<param name="includeAPI" value="true" /> ';
		innerhtml += '<param name="wmode" value="transparent" />';
		innerhtml += '</object>';
		var objID = document.getElementById(VideoRandomID);
		objID.innerHTML = innerhtml;
		var apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "../../../../sadmin.brightcove.com/js/BrightcoveExperiences-1.js"/*tpa=https://sadmin.brightcove.com/js/BrightcoveExperiences.js*/;
		
		//ensure that BrightcoveExperiences is fully loaded before calling createExperiences
		apiInclude.onload = apiInclude.onreadystatechange = function(){
			try{
				brightcove.createExperiences();
			}catch(e){
				
			}	
        }
		objID.parentNode.appendChild(apiInclude);
		
		apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "../../../../sadmin.brightcove.com/js/APIModules_all-1.js"/*tpa=https://sadmin.brightcove.com/js/APIModules_all.js*/;
		objID.parentNode.appendChild(apiInclude);
		
		apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.src = "../../../../files.brightcove.com/bc-mapi.js"/*tpa=https://files.brightcove.com/bc-mapi.js*/;
		objID.parentNode.appendChild(apiInclude);
		
		/*
		 * apiInclude = customBC.createElement('script');
		apiInclude.type = "text/javascript";
		apiInclude.text  = "window.onload = function() {brightcove.createExperiences();};";
		//objID.parentNode.appendChild(apiInclude);
		*/
    };
}
