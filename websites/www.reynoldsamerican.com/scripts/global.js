function isMobileSafari() {
	if( navigator.userAgent.match( /(iPod|iPhone|iPad)/ ) ) {
		return true
	} else {
		return false
	}
}

$(document).ready(function() {
	if(!isMobileSafari()) {
		var params = {wmode: 'transparent'};
		swfobject.embedSWF("Unknown_83_filename"/*tpa=http://www.reynoldsamerican.com/scripts/flash/hero.swf*/, "hero_flash", "789", "221", "9.0.0", "Unknown_83_filename"/*tpa=http://www.reynoldsamerican.com/scripts/flash/expressInstall.swf*/, null, params);
	}
	else {
		$('.hero_tablet_row').hide();

		$('#hero_tablet_row_1').delay(500).fadeIn('slow', function() {
			$('#hero_tablet_row_2').delay(500).fadeIn('slow', function() {
				$('#hero_tablet_row_3').delay(500).fadeIn('slow');
			});
		});
	}
});
