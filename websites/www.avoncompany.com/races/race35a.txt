<script id = "race35a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race35={};
	myVars.races.race35.varName="Array[2901]$LEN";
	myVars.races.race35.varType="@varType@";
	myVars.races.race35.repairType = "@RepairType";
	myVars.races.race35.event1={};
	myVars.races.race35.event2={};
	myVars.races.race35.event1.id = "Lu_Id_div_4";
	myVars.races.race35.event1.type = "onmouseover";
	myVars.races.race35.event1.loc = "Lu_Id_div_4_LOC";
	myVars.races.race35.event1.isRead = "False";
	myVars.races.race35.event1.eventType = "@event1EventType@";
	myVars.races.race35.event2.id = "Lu_Id_div_5";
	myVars.races.race35.event2.type = "onmouseover";
	myVars.races.race35.event2.loc = "Lu_Id_div_5_LOC";
	myVars.races.race35.event2.isRead = "True";
	myVars.races.race35.event2.eventType = "@event2EventType@";
	myVars.races.race35.event1.executed= false;// true to disable, false to enable
	myVars.races.race35.event2.executed= false;// true to disable, false to enable
</script>

