<script id = "race99a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race99={};
	myVars.races.race99.varName="Function[18841].elem";
	myVars.races.race99.varType="@varType@";
	myVars.races.race99.repairType = "@RepairType";
	myVars.races.race99.event1={};
	myVars.races.race99.event2={};
	myVars.races.race99.event1.id = "Lu_Id_div_6";
	myVars.races.race99.event1.type = "onmouseout";
	myVars.races.race99.event1.loc = "Lu_Id_div_6_LOC";
	myVars.races.race99.event1.isRead = "False";
	myVars.races.race99.event1.eventType = "@event1EventType@";
	myVars.races.race99.event2.id = "Lu_Id_div_8";
	myVars.races.race99.event2.type = "onmouseout";
	myVars.races.race99.event2.loc = "Lu_Id_div_8_LOC";
	myVars.races.race99.event2.isRead = "True";
	myVars.races.race99.event2.eventType = "@event2EventType@";
	myVars.races.race99.event1.executed= false;// true to disable, false to enable
	myVars.races.race99.event2.executed= false;// true to disable, false to enable
</script>

