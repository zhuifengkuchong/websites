<script id = "race112b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race112={};
	myVars.races.race112.varName="Object[14839].o";
	myVars.races.race112.varType="@varType@";
	myVars.races.race112.repairType = "@RepairType";
	myVars.races.race112.event1={};
	myVars.races.race112.event2={};
	myVars.races.race112.event1.id = "Lu_Id_a_53";
	myVars.races.race112.event1.type = "onclick";
	myVars.races.race112.event1.loc = "Lu_Id_a_53_LOC";
	myVars.races.race112.event1.isRead = "True";
	myVars.races.race112.event1.eventType = "@event1EventType@";
	myVars.races.race112.event2.id = "Lu_Id_a_28";
	myVars.races.race112.event2.type = "onclick";
	myVars.races.race112.event2.loc = "Lu_Id_a_28_LOC";
	myVars.races.race112.event2.isRead = "False";
	myVars.races.race112.event2.eventType = "@event2EventType@";
	myVars.races.race112.event1.executed= false;// true to disable, false to enable
	myVars.races.race112.event2.executed= false;// true to disable, false to enable
</script>

