<script id = "race111a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race111={};
	myVars.races.race111.varName="Object[14671].o";
	myVars.races.race111.varType="@varType@";
	myVars.races.race111.repairType = "@RepairType";
	myVars.races.race111.event1={};
	myVars.races.race111.event2={};
	myVars.races.race111.event1.id = "Lu_Id_a_4";
	myVars.races.race111.event1.type = "onclick";
	myVars.races.race111.event1.loc = "Lu_Id_a_4_LOC";
	myVars.races.race111.event1.isRead = "False";
	myVars.races.race111.event1.eventType = "@event1EventType@";
	myVars.races.race111.event2.id = "Lu_Id_a_53";
	myVars.races.race111.event2.type = "onclick";
	myVars.races.race111.event2.loc = "Lu_Id_a_53_LOC";
	myVars.races.race111.event2.isRead = "True";
	myVars.races.race111.event2.eventType = "@event2EventType@";
	myVars.races.race111.event1.executed= false;// true to disable, false to enable
	myVars.races.race111.event2.executed= false;// true to disable, false to enable
</script>

