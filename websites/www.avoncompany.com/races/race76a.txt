<script id = "race76a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race76={};
	myVars.races.race76.varName="Function[17074].elem";
	myVars.races.race76.varType="@varType@";
	myVars.races.race76.repairType = "@RepairType";
	myVars.races.race76.event1={};
	myVars.races.race76.event2={};
	myVars.races.race76.event1.id = "Lu_Id_div_7";
	myVars.races.race76.event1.type = "onmouseover";
	myVars.races.race76.event1.loc = "Lu_Id_div_7_LOC";
	myVars.races.race76.event1.isRead = "False";
	myVars.races.race76.event1.eventType = "@event1EventType@";
	myVars.races.race76.event2.id = "Lu_Id_div_4";
	myVars.races.race76.event2.type = "onmouseout";
	myVars.races.race76.event2.loc = "Lu_Id_div_4_LOC";
	myVars.races.race76.event2.isRead = "True";
	myVars.races.race76.event2.eventType = "@event2EventType@";
	myVars.races.race76.event1.executed= false;// true to disable, false to enable
	myVars.races.race76.event2.executed= false;// true to disable, false to enable
</script>

