<script id = "race96b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race96={};
	myVars.races.race96.varName="Function[17393].elem";
	myVars.races.race96.varType="@varType@";
	myVars.races.race96.repairType = "@RepairType";
	myVars.races.race96.event1={};
	myVars.races.race96.event2={};
	myVars.races.race96.event1.id = "Lu_Id_div_7";
	myVars.races.race96.event1.type = "onmouseout";
	myVars.races.race96.event1.loc = "Lu_Id_div_7_LOC";
	myVars.races.race96.event1.isRead = "True";
	myVars.races.race96.event1.eventType = "@event1EventType@";
	myVars.races.race96.event2.id = "Lu_Id_div_8";
	myVars.races.race96.event2.type = "onmouseover";
	myVars.races.race96.event2.loc = "Lu_Id_div_8_LOC";
	myVars.races.race96.event2.isRead = "False";
	myVars.races.race96.event2.eventType = "@event2EventType@";
	myVars.races.race96.event1.executed= false;// true to disable, false to enable
	myVars.races.race96.event2.executed= false;// true to disable, false to enable
</script>

