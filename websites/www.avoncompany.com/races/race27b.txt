<script id = "race27b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race27={};
	myVars.races.race27.varName="Tree[0x7fa8c57e6458]:m2";
	myVars.races.race27.varType="@varType@";
	myVars.races.race27.repairType = "@RepairType";
	myVars.races.race27.event1={};
	myVars.races.race27.event2={};
	myVars.races.race27.event1.id = "Lu_Id_a_17";
	myVars.races.race27.event1.type = "onmouseover";
	myVars.races.race27.event1.loc = "Lu_Id_a_17_LOC";
	myVars.races.race27.event1.isRead = "True";
	myVars.races.race27.event1.eventType = "@event1EventType@";
	myVars.races.race27.event2.id = "m2";
	myVars.races.race27.event2.type = "m2__parsed";
	myVars.races.race27.event2.loc = "m2_LOC";
	myVars.races.race27.event2.isRead = "False";
	myVars.races.race27.event2.eventType = "@event2EventType@";
	myVars.races.race27.event1.executed= false;// true to disable, false to enable
	myVars.races.race27.event2.executed= false;// true to disable, false to enable
</script>

