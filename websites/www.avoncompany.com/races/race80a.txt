<script id = "race80a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race80={};
	myVars.races.race80.varName="Array[2901]$LEN";
	myVars.races.race80.varType="@varType@";
	myVars.races.race80.repairType = "@RepairType";
	myVars.races.race80.event1={};
	myVars.races.race80.event2={};
	myVars.races.race80.event1.id = "Lu_Id_div_4";
	myVars.races.race80.event1.type = "onmouseout";
	myVars.races.race80.event1.loc = "Lu_Id_div_4_LOC";
	myVars.races.race80.event1.isRead = "False";
	myVars.races.race80.event1.eventType = "@event1EventType@";
	myVars.races.race80.event2.id = "Lu_Id_div_5";
	myVars.races.race80.event2.type = "onmouseout";
	myVars.races.race80.event2.loc = "Lu_Id_div_5_LOC";
	myVars.races.race80.event2.isRead = "True";
	myVars.races.race80.event2.eventType = "@event2EventType@";
	myVars.races.race80.event1.executed= false;// true to disable, false to enable
	myVars.races.race80.event2.executed= false;// true to disable, false to enable
</script>

