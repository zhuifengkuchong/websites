<script id = "race44b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race44={};
	myVars.races.race44.varName="Array[2901]$LEN";
	myVars.races.race44.varType="@varType@";
	myVars.races.race44.repairType = "@RepairType";
	myVars.races.race44.event1={};
	myVars.races.race44.event2={};
	myVars.races.race44.event1.id = "Lu_Id_div_8";
	myVars.races.race44.event1.type = "onmouseover";
	myVars.races.race44.event1.loc = "Lu_Id_div_8_LOC";
	myVars.races.race44.event1.isRead = "True";
	myVars.races.race44.event1.eventType = "@event1EventType@";
	myVars.races.race44.event2.id = "Lu_Id_div_7";
	myVars.races.race44.event2.type = "onmouseover";
	myVars.races.race44.event2.loc = "Lu_Id_div_7_LOC";
	myVars.races.race44.event2.isRead = "False";
	myVars.races.race44.event2.eventType = "@event2EventType@";
	myVars.races.race44.event1.executed= false;// true to disable, false to enable
	myVars.races.race44.event2.executed= false;// true to disable, false to enable
</script>

