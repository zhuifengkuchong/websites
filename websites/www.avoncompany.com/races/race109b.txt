<script id = "race109b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race109={};
	myVars.races.race109.varName="Object[14671].o";
	myVars.races.race109.varType="@varType@";
	myVars.races.race109.repairType = "@RepairType";
	myVars.races.race109.event1={};
	myVars.races.race109.event2={};
	myVars.races.race109.event1.id = "Lu_Id_a_44";
	myVars.races.race109.event1.type = "onclick";
	myVars.races.race109.event1.loc = "Lu_Id_a_44_LOC";
	myVars.races.race109.event1.isRead = "True";
	myVars.races.race109.event1.eventType = "@event1EventType@";
	myVars.races.race109.event2.id = "Lu_Id_a_4";
	myVars.races.race109.event2.type = "onclick";
	myVars.races.race109.event2.loc = "Lu_Id_a_4_LOC";
	myVars.races.race109.event2.isRead = "False";
	myVars.races.race109.event2.eventType = "@event2EventType@";
	myVars.races.race109.event1.executed= false;// true to disable, false to enable
	myVars.races.race109.event2.executed= false;// true to disable, false to enable
</script>

