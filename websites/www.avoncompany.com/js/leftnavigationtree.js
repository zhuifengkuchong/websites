// JavaScript Document// ===================================================================
// Author: Matt Kruse <matt@mattkruse.com>
// WWW: http://www.mattkruse.com/
//
// NOTICE: You may use this code for any purpose, commercial or
// private, without any further permission from the author. You may
// remove this notice from your final code if you wish, however it is
// appreciated by the author if at least my web site address is kept.
//
// You may *NOT* re-distribute this code in any way except through its
// use. That means, you can include it in your product, or your web
// site, or any other form where the code is actually being used. You
// may not put the plain javascript up on your site for download or
// include it in your javascript libraries for download. 
// If you wish to share this code with others, please just point them
// to the URL instead.
// Please DO NOT link directly to my .js files from your site. Copy
// the files to your server and use them there. Thank you.
// ===================================================================

//LeftNavaddEvent(window,"load",LeftNavconvertTrees);
function LeftNavaddEvent(o,e,f){
	if(o.LeftNavaddEventListener){
		o.LeftNavaddEventListener(e,f,true);
		return true;
	}else if(o.attachEvent){
		return o.attachEvent("on"+e,f);
	}else{
		return false;
	}
}

function LeftNavsetDefault(name,val){
	if(typeof(window[name])=="undefined" || window[name]==null){
		window[name]=val;
	}
}

function LeftNavexpandTree(treeId){
	var ul = document.getElementById(treeId);
	if(ul == null){
		return false;
	}
	LeftNavexpandCollapseList(ul,nodeOpenClass);
}

function LeftNavcollapseTree(treeId){
	var ul = document.getElementById(treeId);
	if(ul == null){
		return false;
	}
	LeftNavexpandCollapseList(ul,nodeClosedClass);
}

function LeftNavexpandToItem(treeId,itemId){
	var ul = document.getElementById(treeId);
	if(ul == null){
		return false;
	}
	var ret = LeftNavexpandCollapseList(ul,nodeOpenClass,itemId);
	if(ret){
		var o = document.getElementById(itemId);
		/*if(o.scrollIntoView){
			o.scrollIntoView(false);
		}*/
	}
}

function LeftNavexpandCollapseList(ul,cName,itemId){
	if(!ul.childNodes || ul.childNodes.length==0){
		return false;
	}
	for(var itemi=0;itemi<ul.childNodes.length;itemi++){
		var item = ul.childNodes[itemi];
		if(itemId!=null && item.id==itemId){
			return true;
		}if(item.nodeName == "LI"){
			var subLists = false;
			for(var sitemi=0;sitemi<item.childNodes.length;sitemi++){
				var sitem = item.childNodes[sitemi];
				if(sitem.nodeName=="UL"){
					subLists = true;
					var ret = LeftNavexpandCollapseList(sitem,cName,itemId);
					if(itemId!=null && ret){
						item.className=cName;
						return true;
					}
				}
			}
			if(subLists && itemId==null){
				item.className = cName;
			}
		}
	}
}



function LeftNavconvertTrees(){
	LeftNavsetDefault("treeClass","leftnavigationtree");
	LeftNavsetDefault("nodeClosedClass","liClosed");
	LeftNavsetDefault("nodeOpenClass","liOpen");
	LeftNavsetDefault("nodeBulletClass","liBullet");
	LeftNavsetDefault("nodeLinkClass","bullet");
	LeftNavsetDefault("preProcessTrees",true);
	if(preProcessTrees){
		if(!document.createElement){
			return;
		}
	uls = document.getElementsByTagName("ul");
	
		for(var uli=0;uli<uls.length;uli++){
			var ul=uls[uli];
			if(ul.nodeName=="UL" && ul.className==treeClass){
				LeftNavprocessList(ul);
			}
		}
	}
}


function LeftNavprocessList(ul){
	if(!ul.childNodes || ul.childNodes.length==0){
		return;
	}
	for(var itemi=0;itemi<ul.childNodes.length;itemi++){
		var item = ul.childNodes[itemi];
		if(item.nodeName == "LI"){
			var subLists = false;
			for(var sitemi=0;sitemi<item.childNodes.length;sitemi++){
				var sitem = item.childNodes[sitemi];
				if(sitem.nodeName=="UL"){
					subLists = true;LeftNavprocessList(sitem);
				}
			}
			var s= document.createElement("SPAN");
			var t= '';s.className = nodeLinkClass;
			if(subLists){
				if(item.className==null || item.className==""){
				item.className = nodeClosedClass;
				}
				
				if(item.firstChild.nodeName=="#text"){
					t = t+item.firstChild.nodeValue;
					item.removeChild(item.firstChild);
				}
				
				//s.onclick = function(){
				//this.parentNode.className =(this.parentNode.className==nodeOpenClass) ? nodeClosedClass : nodeOpenClass;
				//return false;
				//}
			}else{
				item.className = nodeBulletClass;
				//s.onclick = function(){
				//return false;
				//}
			}
		
		s.appendChild(document.createTextNode(t));
		item.insertBefore(s,item.firstChild);
		}
	}
}

