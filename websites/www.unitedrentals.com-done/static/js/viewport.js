// adjust viewport for mobile devices
// we want to run this as soon as possible, not waiting for the document ready event

( function() {	
 		"use strict";
		
		var getMobileDeviceType,
		    setMobileViewport,
		    setDesktopViewport;
		
		//		
		 getMobileDeviceType = function () {
			var usragnt, device;
			
		 	usragnt = navigator.userAgent.toLowerCase();
		 	device = "desktop";
		 
		 // test for ios	
		 	if (/iphone|ipad|ipod/i.test(usragnt)) {
		           if (/ipad/i.test(usragnt)) {
		           		device = 'tablet';
		           } 
		           else {
		                device = 'handheld';
		           }
		      return device;
		   }
		 	
		  // test for android
		   if (/android/i.test(usragnt)) {
		           if (/mobile/i.test(usragnt)) {
		                device = 'handheld';
		           } 
		           else {
		                device = 'tablet';
		           }
		      return device;
		   }
		   
		   // test for blackberry and playbook
		    if (/blackberry/i.test(usragnt)) {
		        		device = 'handheld';
		    }
		    else {
		    	if (/playbook/i.test(usragnt)) {
		                device = 'tablet';
		    	}
		    }
		    return device;
		 };
 
		 //	 Mobile viewport: set the site width to fit a device and also disable zooming
		 setMobileViewport = function() {
		 	var el = document.getElementById("viewport");
		 	if (el !== null ) {
		 			el.setAttribute('content', 'width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0'); 
		 		}
		 };		 

		 // Desktop viewport: Normally only width is required. However, if this script runs AFTER mobile viewport has been set
		 // other attributes need to be restored. Note that min-scale and max-scale are set here to some orbitrary values - require further research
		 // We are currently NOT using this function because desktop viewport is set in the markup
		 setDesktopViewport = function() {
		 	var el = document.getElementById("viewport");
		 	if (el !== null ) {
		 		el.setAttribute('content', 'width=1200, initial-scale=0.8, user-scalable=yes, minimum-scale=0.1, maximum-scale=10'); 
		 	}
		 };
 
		 // Run viewport setter. This script assumes that by default viewport is set to the desktop
		 if (getMobileDeviceType() === 'handheld') {
		 	setMobileViewport();
		 }
}() );