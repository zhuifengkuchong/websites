var netapp = {};
// Constants - define standard tag values.
/* The tag for all ui */
netapp.DATA_TAG = "data-ntap-ui";
netapp.LISTENERS = "section.tabs[" + netapp.DATA_TAG + "]";
netapp.SOCIAL_LISTENERS = "section.socialTabs[" + netapp.DATA_TAG + "]";
netapp.TABS_LINKS = "section.tabs li";
netapp.MEGA_MENU = ".no-touch #tabs li.tab section";
netapp.TOP_SHELVES = "aside.shelves article";
netapp.MENU_SHOW_HOVER_DELAY = 100; //milleseconds
netapp.MENU_HIDE_HOVER_DELAY = 1000; //milliseconds
netapp.UI_ACTIVE_CLASS = "active";
netapp.UI_DIRECTION = $('http://www.netapp.com/us/static/js/html.rtl').length ? "rtl" : "ltr";
netapp.LIVE_CHAT_URL = 'http://livechat.boldchat.com/aid/3438906697823849762/';
netapp.VIDEO_TAG = "data-ntap-video";
netapp.VIDEO_LISTENERS = "a[" + netapp.VIDEO_TAG + "]";
netapp.BASE_URL = location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
var NetAppPlayer = function() {};
NetAppPlayer.init = function(player, playerContainer) {
	this.player = player;
	this.expandedHeight = 585;
	this.shrunkHeight = 520;
	this.playerContainer = playerContainer;
};
NetAppPlayer.expand = function(offset) {
	document.getElementById(this.player).style.height = this.expandedHeight - offset + "px";
};
NetAppPlayer.shrink = function(offset) {
	document.getElementById(this.player).style.height = this.shrunkHeight - offset + "px";
};


// GEFN: a brute force idea for stopping click events while something is animating - barkan
if (Modernizr.touch) {
	$(document).on('click', function(e) {
		if ($(':animated').size() > 0) {
			e.stopPropagation();
			e.preventDefault();
		}
	});
}

netapp.clickEvent = Modernizr.touch && jQuery.jGestures ? 'tapone' : 'click';

// Removes href, making global navs expand reliably cross device for touch devices.
// inserts the href before clicking thelink as a link. 
// Dozens of "simple fix for click problems on touch device' articles didn't help us.
// Eric gets the points for the fix.
if (Modernizr.touch && ntapDevice === 'desktop') {
	$('#tabs .columns').prepend('<li class="touchCloser" style="background: url(http://media.netapp.com/images/v-form-close.png)no-repeat;background-position: 94% 50%; height: 20px;width: 100%;padding: 5px 0;"></li>');
	$('.touchCloser').each(function() {
		$(this).bind(netapp.clickEvent, function() {
			setTimeout(function() {
				$('.touch .tab').blur();
			}, 400);
		});
	});
	$('.tabText').each(function() {
		$(this).data('realLink', this.href);
		this.href = "#";
		$(this).bind('touchstart', function() {
			var realLink = $(this).data('realLink');
			var $this = $(this);
			setTimeout(function() {
				$this.attr('href', realLink);
			}, 500);
		});
	});
};
netapp.util = {};

netapp.util.getBaseURL = function() {
	var baseURL = window.location.href.split('/').splice(0, 4).join('/');
	return baseURL;
};
netapp.util.getCC = function() {
	var path = document.location.pathname;
	var pathArray = path.split('/');
	var len = pathArray.length;
	var cc = pathArray[1];
	return cc;
};
/**
      Parse string, change all the single quotes to double. Returns a javascript object.
      @param text the text json that needs to be converted to object.
      @return the object which can be now traversed.
*/
netapp.util.getJSON = function(text) {
	var updStr = text.replace(/\'/g, "\"");
	return jQuery.parseJSON(updStr);
};

/**
      Utility function to get the QueryString out of the URL.
      @param key for the query string.
      @return return the value of the key.
*/

netapp.util.getQueryString = function(key) {
	var vars = [],
		hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for (var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars[key];

};

/**
 * Utility to handle decoding URIs.
 * Depending on the result, we will want to decode 0, 1, or 2 times.
 */

netapp.util.uriDecoder = function(decodeThis) {
	//console.log(decodeThis);
	var decodeSave = decodeThis;
	if (decodeThis.indexOf("%") !== -1) {
		try {
			decodeSave = netapp.util.uriDecoder(decodeURI(decodeThis));
		} catch (e) {
			decodeSave = decodeThis;
		}
	}
	return decodeSave;
};

netapp.util.removeParam = function(key, sourceURL) {
	var rtn = sourceURL.split("?")[0],
		param,
		params_arr = [],
		queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
	if (queryString !== "") {
		params_arr = queryString.split("&");
		for (var i = params_arr.length - 1; i >= 0; i -= 1) {
			param = params_arr[i].split("=")[0];
			if (param === key) {
				params_arr.splice(i, 1);
			}
		}
		rtn = rtn + "?" + params_arr.join("&");
	}
	return rtn;
};

netapp.util.getOmniturePageName = function(url) {
	var index = url.indexOf("http://www.netapp.com/us/static/js/www.netapp.com");
	if (index !== -1) {
		var path = url.substring(index + 14);
		var pathArray = path.split('/');
		var last = pathArray.pop();
		if (last === '') {
			pathArray.push('index');
		} else {
			var base = last.split('.')[0];
			pathArray.push(base);
		}
		var out = pathArray.join(':');
		return out.substring(1);
	} else {
		return url;
	}
};
netapp.ui = {};

/**
      Initializes the ui architecture.
*/
netapp.ui.init = function() {
	var $megaMenu = $(netapp.MEGA_MENU);
	netapp.ui.megaMenu($megaMenu);

	$(document).keydown(function(e) {
		//console.log(e.keyCode);
		if (e.keyCode === 27) {
			netapp.ui.hideContent();
		}
	});

	//Get all the links with data-ntap-ui attribute
	$("a[" + netapp.DATA_TAG + "^='{']").bind(netapp.clickEvent, function(e) {
		e.preventDefault();
		var uiDataJSON = netapp.util.getJSON($(this).attr(netapp.DATA_TAG));
		//console.log(uiDataJSON.type);
		//invoke the corresponding widget/ui behavior
		if (uiDataJSON.type !== 'growElement') { 
			netapp.ui[uiDataJSON.type](this, uiDataJSON.options);
			return false;
		}
	});
	//gefn refactor
	$(netapp.LISTENERS).each(function() {
		var uiDataJSON = netapp.util.getJSON($(this).attr(netapp.DATA_TAG));
		$(this).find("nav a").bind(netapp.clickEvent, function() {
			netapp.ui[uiDataJSON.type](this, $.extend(uiDataJSON.options, {
				show: $(this).attr("href")
			}));
			return false;
		});
	});
	//social listener
	$(netapp.SOCIAL_LISTENERS).each(function() {
		var uiDataJSON = netapp.util.getJSON($(this).attr(netapp.DATA_TAG));
		$(this).find("nav a").bind(netapp.clickEvent, function() {
			netapp.ui[uiDataJSON.type](this, $.extend(uiDataJSON.options, {
				show: $(this).attr("href")
			}));
			return false;
		});
	});

	$("section[" + netapp.DATA_TAG + "^='{']").each(function() {

		var uiDataJSON = netapp.util.getJSON($(this).attr(netapp.DATA_TAG));
		//console.log(uiDataJSON);
		netapp.ui[uiDataJSON.type](this, uiDataJSON.options);
		//console.log(uiDataJSON.options);

	});


	// For putting content in a temporary place and moving it somewhere outside of the template, e.g. putting product demo before .container1 - barkan
	$('[data-ntap-insertBefore]').each(function() {
		if($("#iframe3d")) {
			$(this).insertBefore($('.productImage'));
		} else {
			$($(this).attr('data-ntap-insertBefore')).before(this);
		}
	});


	$('[data-ntap-ui *= growElement]').each(netapp.ui.growElement.init);
	$('[data-ntap-ui *= dropdown]').each(netapp.ui.dropdown.init);
	$('[data-ntap-ui *= technoMercial]').each(netapp.ui.technoMercial.init);
	$('[data-ntap-ui *= socialIcons] a').each(netapp.ui.socialIcons.init);
	$('[data-ntap-ui *= stickyTabs]').each(netapp.ui.stickyTabs.init);
	$('#countrySelector .columns a').each(netapp.ui.rememberCountry.init);
	$('[data-ntap-ui *= videoPlayer]').each(netapp.ui.videoPlayer.init);
	//Rewrite tech specs dropdown on all product pages
	$('#managementListData').each(netapp.ui.dropdown.techspecs.init);
	$('#C_Number_of_Employees1').each(netapp.ui.dropdown.alpahNumSort.init);
	$('#C_OPTIN_GEN1').on('change', function(){
	   this.value = this.checked ? 1 : null;
	}).change();
};
netapp.sitesearch = {};
var ssKeyword;
var href;

netapp.sitesearch.init = function() {
	
	//testing to see if there is a url param
	if (netapp.util.getQueryString("url")) {
		var newUrl = netapp.util.removeParam("url", location.search);
		window.location = location.protocol + "//" + location.host + location.pathname + newUrl;
	}

	//check to see if the search box is empty, if so, populate with q if that exist
	if ($('article#main #q1').val() === "") {
		if (netapp.util.getQueryString("q")) {
			var queryString = netapp.util.uriDecoder(netapp.util.getQueryString("q"));
			$('article#main #q1').val(decodeURIComponent(queryString).replace(/\+/g, " ").replace("~2F", "/"));
			$('article#main #q2').val(decodeURIComponent(queryString).replace(/\+/g, " ").replace("~2F", "/"));
		}
	}

	netapp.sitesearch.registerListeners();

	//update the select drop down
	if (netapp.util.getQueryString("searchtype")) {
		var searchType = "searchtype=" + netapp.util.getQueryString("searchtype");
		$('#q1SelectDomain').val(searchType);
		$('#q2SelectDomain').val(searchType);
		$('#q1SelectDomain').change();
		$('#q2SelectDomain').change();
	}
	else {
		var searchType = "searchtype=general";
		$('#q1SelectDomain').val("searchtype=general");
		$('#q2SelectDomain').val("searchtype=general");
		$('#q1SelectDomain').change();
		$('#q2SelectDomain').change();
	}

	ssKeyword = netapp.util.getQueryString("q");
	netapp.sessionStore.init(ssKeyword, netapp.sitesearch.history, 5);
	netapp.sessionStore.print(netapp.sitesearch.history, "#siteSearchHistory ul", "http://www.netapp.com/us/static/js/search.aspx?q=");
	$(document).pjax('#siteSearchHistory a', '#pjaxContainer');

};

netapp.sitesearch.registerListeners = function() {
	var $articleMain = $("article#main");
	var $searchButton = $("button.siteSearchButton");
	var $searchDomain = $("select.siteSearchDropDown");

	$(document).on('pjax:beforeSend', function(event) {
		$('#pjaxContainer').css("opacity", ".2");
	});
	$(document).on('pjax:complete', function(event) {
		$('#pjaxContainer').css("opacity", "1");
		//update search box
		if (netapp.util.getQueryString("q")) {
			var queryString = netapp.util.uriDecoder(netapp.util.getQueryString("q"));
			$('article#main #q1').val(decodeURIComponent(queryString).replace(/\+/g, " ").replace("~2F", "/"));
			$('article#main #q2').val(decodeURIComponent(queryString).replace(/\+/g, " ").replace("~2F", "/"));
			var searchTypeDomain = netapp.util.getQueryString("searchtype");
			var searchType;
			if (searchTypeDomain === "" || searchTypeDomain === undefined) {
				searchType = "searchtype=general";
			}
			else {
				searchType = "searchtype=" + searchTypeDomain;
			}

			$('#q1SelectDomain').val(searchType);
	    	$('#q2SelectDomain').val(searchType);
	    	$('#q1SelectDomain').change();
	    	$('#q2SelectDomain').change();
		}
		//console.log("complete");
	});
	$(document).on('pjax:timeout', function(event) {
		// Prevent default timeout redirection behavior
		event.preventDefault();
	});

	$(document).on('pjax:success', function(event) {
		//var temp = location.search.match(new RegExp("q" + "=(.*?)($|\;)", "i"));
		//$('article#main #q1').val(temp[1]);
		netapp.sessionStore.print(netapp.sitesearch.history, "#siteSearchHistory ul", "http://www.netapp.com/us/static/js/search.aspx?q=");
		$(document).pjax('#siteSearchHistory a', '#pjaxContainer');
		ga('send', 'pageview');
		//sitecatalyst tracking 
		var $searchType = netapp.util.getQueryString("searchtype");
		var keyword = $('article#main #q1').val();
		var searchTerm;
		if($searchType === "communities"){
			searchTerm = keyword.concat("/community");			
		}
		else if($searchType === "kb"){
			searchTerm = keyword.concat("/knowledgebase");			
		}
		else{
			searchTerm = keyword;
		}		
		var page = netapp.util.getQueryString("page");
		var refinement = netapp.util.getQueryString("q1");

		if (typeof Omniture !== 'undefined') {
			if ($('article.noResults').length > 0) {
				searchTerm = "null:" + searchTerm;
			}
			var customParams = {};
			if (keyword && (!page && !refinement)) {
				customParams = {
					events: 'event8',
					prop5: searchTerm,
					eVar1: searchTerm
				};
			}
			if (refinement) {
				customParams = {
					events: 'event17',
					prop5: searchTerm,
					eVar1: searchTerm,
					eVar7: refinement,
					prop7: refinement
				};
			}
			if (page) {
				customParams = {
					events: 'event18',
					prop5: searchTerm,
					eVar1: searchTerm
				};
			}
			Omniture.Call.customLink({
				href: '/'
			}, customParams);
		}
	});

	$("article#main").find("button#q1SiteSearchButton").on("click", function() {
		//check to see if there is a selectDomain
		var href;
		if ($('#q1SelectDomain').length > 0) {
			href = '?q=' + encodeURIComponent($('#q1').val().replace(/\s+/g, '+').replace(/\//g, '~2F')) + '&' + $('select#q1SelectDomain').find("option:selected").attr("value");
		} else {
			href = '?q=' + encodeURIComponent($('#q1').val().replace(/\s+/g, '+').replace(/\//g, '~2F'));
		}
		netapp.sitesearch.submitSearch(href);
		// automatically close the keyboard on iOS
		document.activeElement.blur();

		ssKeyword = netapp.util.getQueryString("q");
		netapp.sessionStore.init(ssKeyword, netapp.sitesearch.history, 5);
		return false;
	});

	$("article#main").find("button#q2SiteSearchButton").on("click", function() {
		//check to see if there is a selectDomain
		
		if ($('#q2SelectDomain').length > 0) {
			href = '?q=' + encodeURIComponent($('#q2').val().replace(/\s+/g, '+').replace(/\//g, '~2F')) + '&' + $('select#q2SelectDomain').find("option:selected").attr("value");
		} else {
			href = '?q=' + encodeURIComponent($('#q2').val().replace(/\s+/g, '+').replace(/\//g, '~2F'));
		}
		netapp.sitesearch.submitSearch(href);
		// automatically close the keyboard on iOS
		document.activeElement.blur();

		ssKeyword = netapp.util.getQueryString("q");
		netapp.sessionStore.init(ssKeyword, netapp.sitesearch.history, 5);
		return false;
	});

	$('#q1SiteSearchForm').on("submit", function() {
		//check to see if there is a selectDomain
		if ($('#q1SelectDomain').length > 0) {
			href = '?q=' + encodeURIComponent($('#q1').val().replace(/\s+/g, '+').replace(/\//g, '~2F')) + '&' + $('select#q1SelectDomain').find("option:selected").attr("value");
		} else {
			href = '?q=' + encodeURIComponent($('#q1').val().replace(/\s+/g, '+').replace(/\//g, '~2F'));
		}
		netapp.sitesearch.submitSearch(href);
		// automatically close the keyboard on iOS
		document.activeElement.blur();

		ssKeyword = netapp.util.getQueryString("q");
		netapp.sessionStore.init(ssKeyword, netapp.sitesearch.history, 5);
		return false;
	});

	$('#q2SiteSearchForm').on("submit", function() {
		//check to see if there is a selectDomain
		if ($('#q2SelectDomain').length > 0) {
			href = '?q=' + encodeURIComponent($('#q2').val().replace(/\s+/g, '+').replace(/\//g, '~2F')) + '&' + $('select#q2SelectDomain').find("option:selected").attr("value");
		} else {
			href = '?q=' + encodeURIComponent($('#q2').val().replace(/\s+/g, '+').replace(/\//g, '~2F'));
		}
		netapp.sitesearch.submitSearch(href);
		// automatically close the keyboard on iOS
		document.activeElement.blur();

		ssKeyword = netapp.util.getQueryString("q");
		netapp.sessionStore.init(ssKeyword, netapp.sitesearch.history, 5);
		return false;
	});

	$('div#pjaxContainer').on("click", "section.siteSearchFilter input[type=checkbox], .resultFilters .resultFilter", function() {
		href = $(this).data("ntapCategory");
		netapp.sitesearch.submitSearch(href);
		return false;
	});

	//smooth scroll to top on page
	$('div#pjaxContainer').on("click", 'div.resultPager ul li a[href]', function() {
		href = $(this).attr("href");
		netapp.sitesearch.submitSearch(href);
		$('html,body,#main').animate({
			scrollTop: $('html').offset().top
		}, 1000);
		return false;
	});
	//banner tracking
	$('div#pjaxContainer').on("click", 'section#adBlock ul li a', function() {
		href = $(this).attr("href");
		netapp.sitesearch.trackBannerClick(href);
	});

	//Handling the fake style drop down
	$('.siteSearchDomain').html($('#q1SelectDomain option:selected').text());
	$('#q1SelectDomain').change(function() {
		$('#q1SiteSearchDomain').html($('#q1SelectDomain option:selected').text());
	});
	$('#q2SelectDomain').change(function() {
		$('#q2SiteSearchDomain').html($('#q2SelectDomain option:selected').text());
	});

	//Show and hide the facets for mobile. Happens to work on desktop as well
	$('div#pjaxContainer').on("click", '#filterCategory', function() {
		$('#siteSearchFacets').toggle('slow');
		return false;
	});
};

// All the magic of PJAX
netapp.sitesearch.submitSearch = function(href) {
	if (!window.history || !window.history.pushState) {
		window.location.href = "http://www.netapp.com/us/static/js/index.aspx" + encodeURI(href);
	} else {
		$.pjax({
			url: "search.aspx" + href,
			container: "#pjaxContainer"
		});
	}
};

netapp.sitesearch.trackBannerClick = function(href) {
	if (typeof Omniture !== 'undefined') {
		var bannerInfo = 'search banner/' + netapp.util.getOmniturePageName(href);
		var params = {
			eVar45: bannerInfo,
			events: 'event7'
		};

		Omniture.Call.customLink({
			href: '/'
		}, params);
	}
};
netapp.events = {};
netapp.events.init = {};
netapp.events.searchParams = {
	q: 'fas', //keyword
	page: 1, //page
	sp_c: 10, //pageSize
	sp_s: 'publish_date' //sortby

};

netapp.events.init = function() {
	netapp.IS_EVENTS = true;
	$('div.container3').on("click", 'a#showAllSummaries', function() {
		netapp.library.showSummaries(this);
	});

	$('div.container3').on("click", 'a#hideAllSummaries', function() {
		netapp.library.hideSummaries(this);
	});

	$('div.container3').on("click", 'a.viewSum', function() {
		netapp.library.showHideSummary(this);
	});
};
netapp.library = {};
netapp.library.init = {};
netapp.library.searchParams = {
	params: {
		q: '', //keyword
		ft: 'library_results_html',
		resultsType: 'search_results',
		x1: 't1',
		q1: 'Library',
		m_count_lib: '10',
		m_sort_lib: 'publish_date',
		page: 1 //page
	},
	set: function(name, value) {
		netapp.library.searchParams.params[name] = value;
	},
	get: function(name) {
		return netapp.library.searchParams.params[name];
	}
};

netapp.library.video = {
	isPlayerAdded: false
};
netapp.library.video.searchParams = {
	params: {
		keyword: '', //keyword
		page: 1, //page
		pageSize: 12,
		resultsType: 'search_results' //search_results or search_categories
	},
	set: function(name, value) {
		netapp.library.video.searchParams.params[name] = value;
	},
	get: function(name) {
		return netapp.library.video.searchParams.params[name];
	}

};
netapp.library.video.categories = {
	products: '',
	solutions: ''
};
netapp.library.video.params = {
	autoStart: true,
	bgcolor: '#FFFFFF',
	width: ntapDevice === "phone" ? '300' : 640,
	height: ntapDevice === "phone" ? '420' : 520,
	isVid: true,
	wmode: 'transparent',
	dynamicStreaming: true,
	isUI: true
};
netapp.library.video.players = {
	'us': '1893375411001',
	'uk': '1940201509001',
	'ru': '1937987951001',
	'nl': '1943274765001',
	'mx': '1947505404001',
	'kr': '1943168011001',
	'jp': '1943255079001',
	'it': '1943255080001',
	'in': '1943274767001',
	'il': '1943274768001',
	'fr': '1947505405001',
	'es': '1943374059001',
	'de': '1943374060001',
	'cn': '1943374061001',
	'br': '1943168012001',
	'au': '1943274769001',
	'as': '1943374062001',
	'ch': '1943274771001',
	'se': '1943168014001',
	'tw': '1947505406001',
	'ca': '1893375411001'
};
netapp.library.showcase = {};
netapp.library.showcase.init = {};
var bcExp, modVP, modExp;

netapp.library.video.init = function() {
	netapp.library.video.registerListeners();
	if (location.href.match("videoId")) {
		var videoId = netapp.util.getQueryString("videoId");
		$('section.carousel').addClass('hidden'); //hide carousel 
		netapp.library.video.addVideoPlayer(videoId, "");
		NetAppPlayer.init('libraryVideoPlayer', document.getElementById('BCLvideoWrapper'));
	}
};

netapp.library.video.preparePageContainers = function() {

	function checkForContainer() {
		//add video container is not present
		if ($("#BCLvideoWrapper").length === 0) {
			if ($(".tabsBar").length > 0) {
				if ($("#SocialShare").length > 0) {
					$('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/>').insertAfter(".container1 #SocialShare");
				} else {
					$('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/>').insertAfter(".container1 .tabsBar");
				}
			} else {
				if ($("#BCLvideoWrapper").length === 0) {
					$(".container1 section").prepend('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/>');
				}
			}
		}
		if ($('div#BCLvideoWrapper[data-ntap-video-load]').length !== 0) {
			netapp.library.video.params.autoStart = false;
			netapp.library.video.addVideoPlayer($('div#BCLvideoWrapper[data-ntap-video-load]').attr("data-ntap-video-load"), "");
			NetAppPlayer.init('libraryVideoPlayer', document.getElementById('BCLvideoWrapper'));
		}
	}

	//make sure brightcove is loaded and the check containers
	if (typeof brightcove === 'undefined') //load bricghtcove if not loaded
	{
		$.getScript("../../../../admin.brightcove.com/js/BrightcoveExperiences.js"/*tpa=http://admin.brightcove.com/js/BrightcoveExperiences.js*/).done(function() {
			$.getScript("../../../../admin.brightcove.com/js/APIModules_all.js"/*tpa=http://admin.brightcove.com/js/APIModules_all.js*/).done(function() {
				checkForContainer();
			});
		});
	} else {
		checkForContainer();
	}
};

netapp.library.video.attachVideoClicks = function() {


	if (($('.playVideo').length) || ($("a[rel*=modal-video]").length) || $("a[data-ntap-video]").length || $("img[data-ntap-video]").length) {
		netapp.library.video.preparePageContainers(); // setup page for video if not already
		var cc = "";
		//catch and hook up legacy rel="modal-video..."
		if (($("a[rel*=modal-video]").length) && ($("a[rel*=modal-video]").attr("rel").indexOf("modal-video") !== -1)) {
			$("a[rel*=modal-video]").each(
				function() {
					$(this).addClass("playVideo");
					//Strip out video ID
					var endIndex = $(this).attr("rel").lastIndexOf("-");
					var videoId = $(this).attr("rel").substring(12, endIndex);
					cc = $(this).attr("rel").substring(endIndex + 1);
					$(this).attr("data-ntap-video", videoId);
					//console.log("parsed:"+cc);
				});
		}

		//bind video play events
		$('[data-ntap-video]').bind(netapp.clickEvent, function() {

			if ((window.location.href.indexOf("/library/") !== -1) || (window.location.href.indexOf("/careers/") !== -1)) {
				$('section.carousel').addClass('hidden'); //hide carousel for library or careers
			}
			netapp.library.video.addVideoPlayer($(this).attr("data-ntap-video"), cc);
			NetAppPlayer.init('libraryVideoPlayer', document.getElementById('BCLvideoWrapper'));
			$('.productImage').hide();

			if (typeof _elqQ !== undefined) {
				var _elq_url = location.href;
				var _vid = $(this).attr('data-ntap-video');

				if (location.href.match(".aspx")) {
					_elq_url += "-video-";
				} else {
					_elq_url += "video-";
				}
				_elq_url += _vid;
				_elqQ.push(['elqTrackPageView', _elq_url]);
			}
		});
	}
	$('.video .close a').on(netapp.clickEvent, function() {
		$('figure.video').addClass('hidden');
		$('section.carousel').removeClass('hidden');
		if (typeof modExp.unload === 'function') {
			modExp.unload();
		} else if (typeof modVP.pause === 'function') {
			modVP.pause(true);
		}
		$("#libraryVideoPlayer").remove(); //remove player object
		netapp.library.video.isPlayerAdded = false; //set player added to false
		return false;
	});
	if ($('.sliderModule').length) {
		netapp.ui.sliderModule();
		$('section.sliderModule').css('border', '1px solid red');
	}
};

netapp.library.video.registerListeners = function() {
	$("article#main").find("input.searchBtn").on("click", function() {
		netapp.library.video.submitForm();
		return false;
	});

	$('#videoLibrarySearchForm').on("submit", function() {
		netapp.library.video.submitForm();
		return false;
	});

	$("div.container2").on("click", "ul.searchFilter li a", function() {
		netapp.library.video.refineSearch(this);
	});
	$("div.container2").on("click", "ul.filters a", function() {
		netapp.library.video.resetCategory(this);
	});
	$("div.container3").on("click", 'a[data-ntap-video-categories-reset]', function() {
		netapp.library.video.resetCategory(this);
	});
	$('div.container3').on("click", 'a[data-ntap-pagination]', function() {
		var page = $(this).attr("data-ntap-pagination");
		netapp.library.video.loadPage(page);
		return false;
	});
	$('div.container3').on("change", 'select.resultsPerPage', function() {
		var selectedValue = $(this).find("option:selected").attr("value");
		netapp.library.video.setResultsPerPage(selectedValue);
		return false;
	});
	$('div.container3').on("change", 'select.sortBy', function() {
		var selectedValue = netapp.util.getJSON($(this).find("option:selected").attr("data-ntap-results-sort"));
		netapp.library.video.sortResults(selectedValue);
		return false;
	});
	$('div.container3').on("click", 'a#mostRecent', function() {
		netapp.library.video.loadPopularOrRecent('recent');
		return false;
	});

	$('div.container3').on("click", 'a#mostPopular', function() {
		netapp.library.video.loadPopularOrRecent('popular');
		return false;
	});

};
netapp.library.video.loadCategories = function() {
	netapp.library.video.searchParams.set("resultsType", "search_categories");
	$('section#resultsFilter').addClass('loading');
	$.ajax({
		url: netapp.VIDEO_LIBRARY_SERVICE,
		data: netapp.library.video.searchParams.params,
		success: function(data) {
			$('section#resultsFilter').removeClass('loading');
			$("article#main").find("div.container2").html(data.html);
		}
	});
};
netapp.library.video.search = function() {
	netapp.library.video.searchParams.set("resultsType", "search_results");
	$('section.results').addClass('loading');
	$.ajax({
		url: netapp.VIDEO_LIBRARY_SERVICE,
		data: netapp.library.video.searchParams.params,
		success: function(data) {
			$("article#main").find("div.container3").html(data.html);
			$('section.results').removeClass('loading');
			netapp.library.video.attachVideoClicks();
		}
	});
};
netapp.library.video.submitForm = function() {
	var keyword = $('#libraryKeyword').prop("value");
	if (keyword !== "") {
		netapp.library.video.searchParams.set("keyword", keyword);
	} 
	netapp.library.video.searchParams.set("keyword", keyword);
	delete netapp.library.video.searchParams.params.products;
	delete netapp.library.video.searchParams.params.solutions;
	delete netapp.library.video.searchParams.params.resultsDataType;
	delete netapp.library.video.searchParams.params.sortOrder;
	netapp.library.video.search();
	netapp.library.video.loadCategories();
};

netapp.library.video.sortResults = function(data) {
	$.extend(netapp.library.video.searchParams.params, data);
	netapp.library.video.search();
};


function onContentLoad(evt) {
	var currentVideo = modVP.getCurrentVideo();
	var currentLink = modSocial.getLink();
	if (currentVideo !== null) {
		var videoName = currentVideo.displayName;
		if (currentLink.indexOf("?") !== -1) {
			currentLink = currentLink.substring(0, currentLink.indexOf("?"));
		}
		var updLink = currentLink + "?videoId=" + currentVideo.id;
		modSocial.setLink(updLink);
	}
}

function onMediaChange(evt) {
	var currentVideo = modVP.getCurrentVideo();
	var currentLink = modSocial.getLink();
	if (currentVideo !== null) {
		var videoName = currentVideo.displayName;
		if (currentLink.indexOf("?") !== -1) {
			currentLink = currentLink.substring(0, currentLink.indexOf("?"));
		}
		var updLink = currentLink + "?videoId=" + currentVideo.id;
		modSocial.setLink(updLink);
	}
}

function emailButtonClicked(evt) {
	var currentVideo = modVP.getCurrentVideo();
	if (currentVideo !== null && typeof Omniture !== 'undefined') {
		var videoName = currentVideo.displayName;
		var params = {
			prop43: 'video-send-email-' + videoName,
			eVar43: 'video-send-email-' + videoName,
			events: 'event5'
		};
		Omniture.Call.customLink({
			href: '/'
		}, params);

	}
}

function copyLinkButtonClicked(evt) {
	var currentVideo = modVP.getCurrentVideo();
	if (currentVideo !== null && typeof Omniture !== 'undefined') {
		var videoName = currentVideo.displayName;
		var params = {
			prop43: 'video-copy-link-' + videoName,
			eVar43: 'video-copy-link-' + videoName,
			events: 'event5'
		};
		Omniture.Call.customLink({
			href: '/'
		}, params);

	}
}

function copyCodeButtonClicked(evt) {
	var currentVideo = modVP.getCurrentVideo();
	if (currentVideo !== null && typeof Omniture !== 'undefined') {
		var videoName = currentVideo.displayName;
		var params = {
			prop43: 'video-copy-code-' + videoName,
			eVar43: 'video-copy-code-' + videoName,
			events: 'event5'
		};
		Omniture.Call.customLink({
			href: '/'
		}, params);

	}
}

bcTemplateLoaded = function(experienceId) {
	bcExp = brightcove.getExperience(experienceId);
	if (typeof bcExp !== 'undefined') {
		modVP = bcExp.getModule(APIModules.VIDEO_PLAYER);
		modExp = bcExp.getModule(APIModules.EXPERIENCE);
		modSocial = bcExp.getModule(APIModules.SOCIAL);
		modMenu = bcExp.getModule(APIModules.MENU);
	} else {
		bcExp = brightcove.api.getExperience(experienceId);
		modVP = bcExp.getModule(brightcove.api.modules.APIModules.VIDEO_PLAYER);
		modExp = bcExp.getModule(brightcove.api.modules.APIModules.EXPERIENCE);
		modSocial = bcExp.getModule(brightcove.api.modules.APIModules.SOCIAL);
		modMenu = bcExp.getModule(brightcove.api.modules.APIModules.MENU);
	}
};
bcTemplateReady = function(evt) {
	if (typeof BCExperienceEvent.CONTENT_LOAD !== 'undefined') {
		modExp.addEventListener(BCExperienceEvent.CONTENT_LOAD, onContentLoad);
	}
	if (typeof BCMediaEvent.CHANGE !== 'undefined') {
		modVP.addEventListener(BCMediaEvent.CHANGE, onMediaChange);
	}
	if (typeof modMenu !== 'undefined') {
		modMenu.addEventListener(BCMenuEvent.SEND_EMAIL_CLICK, emailButtonClicked);
	}
	if (typeof modMenu !== 'undefined') {
		modMenu.addEventListener(BCMenuEvent.COPY_LINK, copyLinkButtonClicked);
	}
	if (typeof modMenu !== 'undefined') {
		modMenu.addEventListener(BCMenuEvent.COPY_CODE, copyCodeButtonClicked);
	}
};


netapp.library.video.loadPopularOrRecent = function(resultsType) {
	$.extend(netapp.library.video.searchParams.params, {
		'resultsDataType': resultsType
	});
	netapp.library.video.submitForm();
};

netapp.library.video.loadPage = function(page) {
	netapp.library.video.searchParams.set("page", page);
	netapp.library.video.search();
};
netapp.library.video.setResultsPerPage = function(value) {
	netapp.library.video.searchParams.set("pageSize", value);
	netapp.library.video.searchParams.set("page", 1);
	netapp.library.video.search();
};
netapp.library.video.resetCategory = function(node) {
	var nodeData = $(node).attr("data-ntap-video-categories-reset");
	delete netapp.library.video.searchParams.params[nodeData];
	netapp.library.video.loadCategories();
	netapp.library.video.search();
};
netapp.library.video.refineSearch = function(node) {
	var nodeData = netapp.util.getJSON($(node).attr("data-ntap-video-categories"));
	$.extend(netapp.library.video.searchParams.params, nodeData);
	netapp.library.video.loadCategories();
	netapp.library.video.search();
};


netapp.library.video.addVideoPlayer = function(videoId, cc) {
	var path = document.location.pathname;
	var pathArray = path.split('/');
	var len = pathArray.length;
	var geo = pathArray[1];
	if (cc !== "") {
		geo = cc;
	}
	var playerId = netapp.library.video.players[geo];
	netapp.library.video.initVideoPlayer(videoId, playerId, geo);
};
netapp.library.video.initVideoPlayer = function(videoId, playerId, geo) {
	if (typeof videoId !== "undefined") {
		if (!netapp.library.video.isPlayerAdded) {
			var videoPlayer = brightcove.createElement("object");
			$('figure.video').removeClass("hidden");
			videoPlayer.id = 'libraryVideoPlayer';
			var parameter;
			for (var i in netapp.library.video.params) {
				parameter = brightcove.createElement("param");
				parameter.name = i;
				parameter.value = netapp.library.video.params[i];
				videoPlayer.appendChild(parameter);
			}
			//videoId
			parameter = brightcove.createElement("param");
			parameter.name = "@videoPlayer";
			parameter.value = videoId;
			videoPlayer.appendChild(parameter);
			//playerID
			parameter = brightcove.createElement("param");
			parameter.name = "playerID";
			parameter.value = playerId;
			videoPlayer.appendChild(parameter);
			//cc for reporting
			parameter = brightcove.createElement("param");
			parameter.name = "cc";
			parameter.value = geo;
			videoPlayer.appendChild(parameter);
			//templateloaded handler
			parameter = brightcove.createElement("param");
			parameter.name = "templateLoadHandler";
			parameter.value = 'bcTemplateLoaded';
			videoPlayer.appendChild(parameter);
			//template readyhandler
			parameter = brightcove.createElement("param");
			parameter.name = "templateReadyHandler";
			parameter.value = 'bcTemplateReady';
			videoPlayer.appendChild(parameter);

			if (ntapDevice === 'phone') {
				parameter = brightcove.createElement("param");
				parameter.name = "includeAPI";
				parameter.value = "true";
				videoPlayer.appendChild(parameter);
			}

			var container = document.getElementById('BCLvideoWrapper');
			brightcove.createExperience(videoPlayer, container, true);
			netapp.library.video.isPlayerAdded = true;
		} else {
			if (typeof modVP.loadVideo === 'function') {
				modVP.loadVideo(videoId);
			} else if (typeof modVP.loadVideoByID === 'function') {
				modVP.loadVideoByID(videoId);
			}
		}
	}
};

netapp.library.showcase.init = function() {
	var cc = netapp.util.getCC();
	var showcaseSearchURL = netapp.CUSTOMER_SHOWCASE_SEARCH_SERVICE[cc];
	$("article#main").find("input.searchBtn").on("click", function() {
		netapp.library.showcase.loadResults(showcaseSearchURL);
		return false;
	});

	$('#librarySearchForm').on("submit", function() {
		netapp.library.showcase.loadResults(showcaseSearchURL);
		return false;
	});

	$('div.container2').on("click", 'a#showAllSummaries', function() {
		netapp.library.showSummaries(this);
	});

	$('div.container2').on("click", 'a#hideAllSummaries', function() {
		netapp.library.hideSummaries(this);
	});

	$('div.container2').on("click", 'a.viewSum', function() {
		netapp.library.showHideSummary(this);
	});

	$('div.container2').on("click", 'div.paginator ul li a', function() {
		var href = $(this).attr("href");
		netapp.library.showcase.loadResults(href);
		return false;
	});
	$('div.container2').on("change", 'select.resultsPerPage', function() {
		var href = $(this).find("option:selected").attr("value");
		netapp.library.showcase.loadResults(href);
		return false;
	});
	$('div.container2').on("change", 'select.sortBy', function() {
		var href = $(this).find("option:selected").attr("value");
		netapp.library.showcase.loadResults(href);
		return false;
	});
};
netapp.library.showcase.loadResults = function(href) {
	var hrefStr = href.substring(1);
	var hrefParams = netapp.library.getParamsFromHref(hrefStr);
	hrefParams.ft = "library_results_html";
	var keyword = $('#libraryKeyword').prop("value");
	$.extend(hrefParams, {
		'resultsType': 'search_results'
	});
	if (keyword !== "") {
		$.extend(hrefParams, {
			'q': keyword
		});
	} 
	$('section.results').addClass('loading');
	$.ajax({
		url: netapp.LIBRARY_SERVICE,
		data: hrefParams,
		success: function(data) {
			$('section.results').removeClass('loading');
			$("article#main").find("div.container2").html(data.html);
		},
		error: function() {

		}
	});
};
netapp.library.showcase.submitForm = function() {
	var keyword = $('#libraryKeyword').prop("value");
	if (keyword !== "") {
		netapp.library.searchParams.set("q", keyword);
	} 
	netapp.library.showcase.search();
};
netapp.library.showcase.search = function() {
	netapp.library.searchParams.set("resultsType", "search_results");
	netapp.library.searchParams.set("ft", "library_results_html");
	var keyword = $('#libraryKeyword').prop("value");
	if (keyword !== "") {
		netapp.library.searchParams.params.q = keyword;
	}
	$.extend(netapp.library.searchParams.params, {
		'x2': 'lib_doc_type',
		'q2': 'Customer Story'
	});
	$.ajax({
		url: netapp.LIBRARY_SERVICE,
		data: netapp.library.searchParams.params,
		success: function(data) {
			$("article#main").find("div.container2").html(data.html);
		},
		error: function() {

		}
	});
};
netapp.library.showcase.sortResults = function(value) {
	netapp.library.searchParams.set("m_sort_lib", value);
	netapp.library.searchParams.set("page", 1);
	netapp.library.showcase.search();

};
netapp.library.showcase.setResultsPerPage = function(value) {
	netapp.library.searchParams.set("m_count_lib", value);
	netapp.library.searchParams.set("page", 1);
	netapp.library.showcase.search();

};
netapp.library.showcase.loadPage = function(page) {
	netapp.library.searchParams.set("page", page);
	netapp.library.showcase.search();
};
netapp.library.init = function() {
	netapp.library.registerListeners();
};
netapp.library.registerListeners = function() {
	
	//Handling the fake style drop down
	$('.siteSearchDomain').html($('#q1SelectDomain option:selected').text());
	$('#q1SelectDomain').change(function() {
		$('#q1SiteSearchDomain').html($('#q1SelectDomain option:selected').text());
	});
	
	$("article#main").find("input.searchBtn").on("click", function() {
		var href = $('select#searchType').find("option:selected").attr("value");
		netapp.library.loadResultsAndCategories(href);
		return false;
	});

	$('#librarySearchForm').on("submit", function() {
		var href = $('select#searchType').find("option:selected").attr("value");
		netapp.library.loadResultsAndCategories(href);
		return false;
	});
	$('div.container3').on("click", 'a#showAllSummaries', function() {
		netapp.library.showSummaries(this);
	});

	$('div.container3').on("click", 'a#hideAllSummaries', function() {
		netapp.library.hideSummaries(this);
	});

	$('div.container3').on("click", 'a.viewSum', function() {
		netapp.library.showHideSummary(this);
	});

	$('div.container3').on("click", 'div.paginator ul li a', function() {
		var href = $(this).attr("href");
		netapp.library.loadResults(href);
		return false;
	});
	$('div.container3').on("change", 'select.resultsPerPage', function() {
		var href = $(this).find("option:selected").attr("value");
		netapp.library.loadResults(href);
		return false;
	});
	$('div.container3').on("change", 'select.sortBy', function() {
		var href = $(this).find("option:selected").attr("value");
		netapp.library.loadResults(href);
		return false;
	});

	$("div.container2").on("click", "section#resultsFilter ul.searchFilter li a[title]", function() {
		var href = $(this).attr("href");
		netapp.library.loadResultsAndCategories(href);
		return false;
	});
	$("div.container2").on("click", "section#resultsFilter ul.searchFilter li.selected a", function() {
		var href = $(this).attr("href");
		netapp.library.loadResultsAndCategories(href);
		return false;
	});
};
netapp.library.loadResultsAndCategories = function(href) {
	netapp.library.loadResults(href);
	netapp.library.loadCategories(href);
};
netapp.library.getParamsFromHref = function(hrefStr) {
	var vars = {},
		hash;
	var hashes = hrefStr.split('&');
	var len = hashes.length;
	for (var i = 0; i < len; i++) {
		hash = hashes[i].split('=');
		vars[hash[0]] = hash[1];
	}
	return vars;
};

netapp.library.loadCategories = function(href) {
	var hrefStr = href.substring(1);
	var hrefParams = netapp.library.getParamsFromHref(hrefStr);
	hrefParams.ft = "library_facets_html";
	var keyword = $('#libraryKeyword').prop("value");
	$.extend(hrefParams, {
		'resultsType': 'search_categories'
	});
	if (keyword !== "") {
		$.extend(hrefParams, {
			'q': keyword
		});
	} 
	if (!hrefParams.hasOwnProperty('m_sort_lib')) {
		$.extend(hrefParams, {
			'm_sort_lib': 'relevance',
			'act': 'sort'
		});
	}
	$('section#resultsFilter').addClass('loading');
	$.ajax({
		url: netapp.LIBRARY_SERVICE,
		data: hrefParams,
		success: function(data) {
			$('section#resultsFilter').removeClass('loading');
			$("article#main").find("div.container2").html(data.html);
		}
	});
};
netapp.library.loadResults = function(href) {
	var hrefStr = href.substring(1);
	var hrefParams = netapp.library.getParamsFromHref(hrefStr);
	hrefParams.ft = "library_results_html";
	var keyword = $('#libraryKeyword').prop("value");
	$.extend(hrefParams, {
		'resultsType': 'search_results'
	});
	if (keyword !== "") {
		$.extend(hrefParams, {
			'q': keyword
		});
	} 
	if (!hrefParams.hasOwnProperty('m_sort_lib')) {
		$.extend(hrefParams, {
			'm_sort_lib': 'relevance',
			'act': 'sort'
		});
	}
	$('section.results').addClass('loading');
	$.ajax({
		url: netapp.LIBRARY_SERVICE,
		data: hrefParams,
		success: function(data) {
			$('section.results').removeClass('loading');
			$("article#main").find("div.container3").html(data.html);
		},
		error: function() {

		}
	});
};

netapp.library.showSummaries = function(node) {
	var summaryNodes = $(node).parents().eq(2).find("td.summary");
	var summaryLinks = $(node).parents().eq(2).find("a.viewSum");
	summaryNodes.each(function() {
		netapp.library.show(this);
	});
	summaryLinks.each(function() {
		netapp.library.show(this);
	});

	return false;
};
netapp.library.hideSummaries = function(node) {
	var summaryNodes = $(node).parents().eq(2).find("td.summary");
	var summaryLinks = $(node).parents().eq(2).find("a.viewSum");
	summaryNodes.each(function() {
		netapp.library.hide(this);
	});
	summaryLinks.each(function() {
		netapp.library.hide(this);
	});
	return false;
};
netapp.library.show = function(node) {
	if (!$(node).hasClass('active')) {
		$(node).addClass("active");
	}
	return false;
};
netapp.library.hide = function(node) {
	if ($(node).hasClass('active')) {
		$(node).removeClass("active");
	}
	return false;
};

netapp.library.showHideSummary = function(node) {
	if (!$(node).hasClass('active')) {
		$(node).addClass("active");
	}
	else {
		$(node).removeClass("active");
	}
	var $summary = $(node).parents().eq(3).next().children('.summary');
	if (!$summary.hasClass('active')) {
		$summary.addClass('active');
	} else {
		$summary.removeClass("active");
	}
};
netapp.sessionStore = {};
var localArray;
//start of sessionStorage test
netapp.sessionStore.init = function(theItem, theArray, maxItems) {
	if(theItem === '' || theItem === undefined) {
		return;
	}
	//For testing only
	//console.log(sessionStorage);

	// Check if the browser support Local Storage
	if (sessionStorage) {
		try {
			//logic
			if (sessionStorage[theArray]) {
				localArray = JSON.parse(sessionStorage[theArray]);
			} else {
				localArray = [];
			}
			// Check if the value exists in the array,
			// returns -1 if the array does not contain the value
			if ($.inArray(theItem, localArray) === -1) {
				// If not, put it first and...
				localArray.unshift(theItem);
				//  ...pop one at the end if array is too big
				if (localArray.length > maxItems) {
					localArray.pop();
				}

				// Adding localArray to the storage
				sessionStorage[theArray] = JSON.stringify(localArray);
			}
			//end of logic
		} catch (domException) {
			if (domException.name === 'QuotaExceededError' ||
				domException.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
				// Fallback code comes here.
			}
		}
	}
};

netapp.sessionStore.print = function(theArray, theContainer, optQueryString) {
	if (sessionStorage[theArray]) {
		try {
			var tempArray = [];
			tempArray = JSON.parse(sessionStorage[theArray]);

			if (tempArray.length) {
				$(theContainer).empty();
				for (var i = 0; i < tempArray.length; i++) {
					$("<li><a href='" + optQueryString + tempArray[i] + "'>" + decodeURIComponent(netapp.util.uriDecoder(tempArray[i])).replace(/\+/g, " ").replace("~2F","/") + "</a></li>").appendTo(theContainer);
				}
				$(theContainer + ':has(li)').closest('.module').css('display', 'block');
			}
		} catch (domException) {
			if (domException.name === 'QuotaExceededError' ||
				domException.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
				// Fallback code comes here.
			}
		}
	}
};
	
netapp.ui.accordion = function(obj, options) {

	var allPanels = $('.personDetails').hide();
	var allPersons = $('.accordion > .person > h1 > a');

	if (window.location.hash.indexOf('#') === 0) {
		$(window.location.hash).addClass('current');
		$('.current').parent().next().show();
		var scrollPos = $(window.location.hash).parent().offset().top;
		$(window).scrollTop(scrollPos);
	}

	if (window.location.hash.indexOf('#') === -1) {
		$('.accordion .person :first .name').addClass('current');
		$('.accordion .person :first').children().css('display', 'block');
	}

	$('.accordion > article > h1 > a').bind(netapp.clickEvent, function() {
		allPanels.hide();
		if ($(this).hasClass("current")) {
			allPersons.removeClass("current");
		} else {
			allPersons.removeClass("current");
			$(this).parent().next().show();
			$(this).addClass("current");
			location = '#' + $(this).attr('id');
		}
		return false;
	});
};
/**
      The carousel using Anything SLider.
*/

netapp.ui.carousel = function(obj, options) {
	if ($('.carousel.landing').length && $('.carousel .slides > li').length > 1) {
		$('.carousel').addClass('flexslider').append('<div class="controls"><div class="liner"></div></div>');
		$('.carousel').flexslider({
			/* Putting all args here for speed of fine-tuning as we go. {value} indicates that's the default flexSlider value.
			   All comments from flexSlider - barkan */
			namespace: "flex-", //{flex-} String: Prefix string attached to the class of every element generated by the plugin
			selector: ".slides > li", //{.slides > li} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
			animation: "fade", //{fade} String: Select your animation type, "fade" or "slide"
			easing: "swing", //{swing} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
			direction: "horizontal", //{horizontal} String: Select the sliding direction, "horizontal" or "vertical"
			reverse: false, //{false} Boolean: Reverse the animation direction
			animationLoop: typeof options.animationLoop !== 'undefined' ? options.animationLoop : false, //{true} Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
			smoothHeight: false, //{false} Boolean: Allow height of the slider to animate smoothly in horizontal mode
			startAt: 0, //{0} Integer: The slide that the slider should start on. Array notation (0 = first slide)
			slideshow: options.slideshow || false, //{true} Boolean: Animate slider automatically
			slideshowSpeed: 7000, //{7000} Integer: Set the speed of the slideshow cycling, in milliseconds
			animationSpeed: 600, //{600} Integer: Set the speed of animations, in milliseconds
			initDelay: 0, //{0} Integer: Set an initialization delay, in milliseconds
			randomize: false, //{false} Boolean: Randomize slide order

			// Usability features
			pauseOnAction: true, //{true} Boolean: Pause the slideshow when interacting with control elements, highly recommended.
			pauseOnHover: true, //{false} Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
			useCSS: true, //{true} Boolean: Slider will use CSS3 transitions if available
			touch: false, //{true} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
			video: false, //{false} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

			// Primary Controls
			controlNav: true, //{true} Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
			directionNav: false, //{true} Boolean: Create navigation for previous/next navigation? (true/false)
			prevText: "", //{Previous} String: Set the text for the "previous" directionNav item
			nextText: "", //{Next} String: Set the text for the "next" directionNav item

			// Secondary Navigation
			keyboard: true, //{true} Boolean: Allow slider navigating via keyboard left/right keys
			multipleKeyboard: false, //{false} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
			mousewheel: false, //{false} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
			pausePlay: false, //{false} Boolean: Create pause/play dynamic element
			pauseText: 'Pause', //{Pause} String: Set the text for the "pause" pausePlay item
			playText: 'Play', //{Play} String: Set the text for the "play" pausePlay item

			// Special properties
			controlsContainer: ".flexslider .controls .liner", //{""} Selector: USE CLASS SELECTOR. Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be ".flexslider-container". Property is ignored if given element is not found.
			manualControls: "", //{""} Selector: Declare custom control navigation. Examples would be ".flex-control-nav li" or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
			sync: "", //{""} Selector: Mirror the actions performed on this slider with another slider. Use with care.
			asNavFor: "", //{""} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

			// Carousel Options
			itemWidth: 0, //{0} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
			itemMargin: 0, //{0} Integer: Margin between carousel items.
			minItems: 0, //{0} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
			maxItems: 0, //{0} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
			move: 0, //{0} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.

			// Callback API
			start: function() {}, //{} Callback: function(slider) - Fires when the slider loads the first slide
			before: function() {}, //{} Callback: function(slider) - Fires asynchronously with each slider animation
			after: function() {}, //{} Callback: function(slider) - Fires after each slider animation completes
			end: function() {}, //{} Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
			added: function() {}, //{} Callback: function(slider) - Fires after a slide is added
			removed: function() {} //{} Callback: function(slider) - Fires after a slide is removed
		});
	}
}

/**
      The carousel using Anything SLider.
*/
;
netapp.ui.carouselOptions = {
	animation: "slide", //{fade} String: Select your animation type, "fade" or "slide"
	slideshow: false,
	animationLoop: false,
	// Usability features
	pauseOnHover: true, //{false} Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering

	// Primary Controls
	controlNav: false, //{true} Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
	directionNav: false
};
netapp.ui.dropdown = {
	init: function() {
		// Create the dropdown base
		if ($(".tabsBar ul").children().length > 0) {
			$("<select class='dropdownMenu'/>").appendTo(".tabsBar");

			// Populate dropdown with menu items
			$(".tabsBar a").each(function() {
				var el = $(this);
				if ($.trim(el.text()) !== $.trim($('.tabsBar ul > .active').text())) {
					$("<option />", {
						"value": el.attr("href"),
						"text": el.text()
					}).appendTo(".tabsBar select");
				} else {
					$("<option />", {
						"selected": "selected",
						"value": el.attr("href"),
						"text": el.text()
					}).appendTo(".tabsBar select");
				}
			});
			$(".tabsBar select").change(function() {
				window.location = $(this).find("option:selected").val();
			});
		}
		if ($(".footerTabs ul").children().length > 0) {
			$("<select class='dropdownMenu'/>").appendTo(".footerTabs");

			// Populate dropdown with menu items
			$(".footerTabs a").each(function() {
				var el = $(this);
				if ($.trim(el.text()) !== $.trim($('.footerTabs ul > .active').text())) {
					$("<option />", {
						"value": el.attr("href"),
						"text": el.text()
					}).appendTo(".footerTabs select");
				} else {
					$("<option />", {
						"selected": "selected",
						"value": el.attr("href"),
						"text": el.text()
					}).appendTo(".footerTabs select");
				}
			});
			$(".footerTabs select").change(function() {
				window.location = $(this).find("option:selected").val();
			});
		}

	}
};

//Rewrite tech spec dropdown
netapp.ui.dropdown.techspecs = {
	init: function() {
		$(this).attr("onchange", "netapp.ui.dropdown.techspecs.jumpMenu('parent',this,0)");
	},
	jumpMenu: function(targ, selObj, restore) {
		eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
		if (restore) {
			selObj.selectedIndex = 0;
		} 
	}
};

//Alpha numeric sorting
netapp.ui.dropdown.alpahNumSort = {
	init: function() {
		var $dd = $(this);
		if ($dd.length > 0) { // make sure we found the select we were looking for
		
		    // save the selected value
		    var selectedVal = $dd.val();
		
		    // get the options and loop through them
		    var $options = $('option', $dd);
		    var arrVals = [];
		    $options.each(function(){
		        // push each option value and text into an array
		        arrVals.push({
					val: parseFloat(jQuery(this).val()) ? parseFloat(jQuery(this).val()) : jQuery(this).val(),
					realval: jQuery(this).val(),
					text: jQuery(this).text()
				});
				//console.log(arrVals);
		    });
		
		    // sort the array by the value (change val to text to sort by text instead)
		    arrVals.sort(function(a, b){
		        return a.val - b.val;
		    });
		
		    // loop through the sorted array and set the text/values to the options
		    for (var i = 0, l = arrVals.length; i < l; i++) {
		        $($options[i]).val(arrVals[i].realval).text(arrVals[i].text);
		        //console.log(arrVals[i]);
		    }
		
		    // set the selected value back
		    $dd.val(selectedVal);
		}

	}
};
netapp.ui.equalHeight = function(obj, options) {
	var maxHeight = 0,
		items = $(options.selector);
	items.each(function(i, item) {
		maxHeight = Math.max($(item).height(), maxHeight);
	});
	items.each(function(i, item) {
		var thisDelta = maxHeight - $(item).height(),
			thisLast = $(item).children().last();
		if (thisDelta > 0) {
			thisLast.css('min-height', thisLast.height() + thisDelta);
		}
	});
};
/**
      EU Cookie Law Notice Modal JS.
      gefn - needs animation which needs researching in colorbox 
    IE is jerky - Shyam.
*/

netapp.ui.euModal = function(obj, options) {
	// Get the Query string in the URL
	var query = netapp.util.getQueryString("euCookie");
	if (query) {
		//Below option is extended to add & remove the background yellow of the modal.
		options = $.extend(options, {
			onLoad: function() {
				$("#cboxContent").addClass("euCookieModalBG");
			},
			onClosed: function() {
				$("#cboxContent").removeClass("euCookieModalBG");
			}
		});
		$.colorbox(options);
		$("#cookieNoticeShort a").bind(netapp.clickEvent, function() {
			$("#cookieNoticeShort").addClass("hidden");
			$("#cookieNoticeExpanded").removeClass("hidden");
			$.colorbox.resize();
		});

		//taking screenWidth  to resize the euModal with 50px padding

		var screenWidth = screen.width;

		if (screenWidth < 490) {
			var newWidth = screen.width - 100;
			$.colorbox.resize({
				width: newWidth
			});
		}
	}
};
netapp.ui.growElement = {
	lastGrown: {}, // for closing from more than one place
	lastHidden: {}, // for closing from more than one place
	init: function() {
		var uiDataJSON = netapp.util.getJSON($(this).attr(netapp.DATA_TAG)),
			options = uiDataJSON.options;
		if (ntapDevice === "phone" && $(options.grows).attr('src')) {
			$(this).data('src', $(options.grows).attr('src'));
			$(this).bind(netapp.clickEvent, function() {
				window.open($(this).data('src'));
			});
		} else {
			$(this).data('grows', $(options.grows));
			$(this).data('hides', $(options.hides));

			$(options.grows).data('grows', $(options.grows));
			$(options.grows).data('hides', $(options.hides));

			$(this).bind(netapp.clickEvent, netapp.ui.growElement.growFunc);
		}

	},
	growFunc: function() {
		if (typeof Omniture !== 'undefined') {
			var params = {
				eVar45: '3d-demo',
				events: 'event7'
			};
			Omniture.Call.customLink({
				href: '/'
			}, params);
		}

		var g = $(this).data('grows').first(); // doesn't make sense to grow multiple elements - barkan

		netapp.ui.growElement.lastGrown = g;
		netapp.ui.growElement.lastHidden = $(this).data('hides');

		$(this).data('hides').each(function(i, h) {
			$(h).animate({
				height: 'hide',
				opacity: 'hide'
			});
		});

		if ($(g).attr('data-full-width') && $(g).attr('data-full-height')) {
			$(g).animate({
				width: $(g).attr('data-full-width'),
				height: $(g).attr('data-full-height'),
				opacity: 'show'
			});
		} else {
			$(g).animate({
				width: 'show',
				height: 'show',
				opactiy: 'show'
			});
		}
		return false;
	},
	hideFunc: function() {
		$(this.lastHidden).each(function(i, h) {
			$(h).animate({
				height: 'show',
				opacity: 'show'
			});
		});

		$(this.lastGrown).animate({
			width: 0,
			height: 0,
			opacity: 'hide'
		});
	}
};
netapp.ui.hideContent = function() {
	$("#tabs li.tab").removeClass(netapp.ui.menuSettings.hoverClass);
	$("#tabs li.tab").children("ul").hide();

	if ($("div.details").is(":visible")) {
		$("#banner").find("div.details").addClass("hidden");
	} 

};
netapp.ui.liveChatSettings = {
	'us': {
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=3011443994008877806&amp;wdid=3682071963888102016&amp;url=' + escape(document.location.href),
		chatID: 'Chat2062696572973033378'
	},
	'cn': {
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=4715985455342252930&amp;wdid=4128635980169529276&amp;url=' + escape(document.location.href),
		chatID: 'Chat1653577421068585630'
	},
	'kr': {
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=7158486773845042735&amp;wdid=4461421699358108630&amp;url=' + escape(document.location.href),
		chatID: 'Chat1320491535744444148'
	},
	'it': {
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=3207688872652260696&amp;wdid=1379692910022363355&amp;url=' + escape(document.location.href),
		chatID: 'Chat4367598902258599417'
	},
	'uk': {
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=6866554161482778948&amp;wdid=124822476997916420&amp;url=' + escape(document.location.href),
		chatID: 'Chat3315218916840667686'
	},
	'de': {
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=5647474061355152344&amp;wdid=8666768590634319856&amp;url=' + escape(document.location.href),
		chatID: 'Chat6341067685597934290'
	},
	'in': {
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=763324604623093912&amp;wdid=2426445596525794305&amp;url=' + escape(document.location.href),
		chatID: 'Chat1014721697346722083'
	},
	'au': {
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=4146812614014004074&amp;wdid=3499522200958717238&amp;url=' + escape(document.location.href),
		chatID: 'Chat2245536503348084756'
	},
	'as': { 
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=3268985311739115372&amp;wdid=1355755926095416900&amp;url=' + escape(document.location.href), 
		chatID: 'Chat4425331712528794470' 
	},
	'jp': { 
		url: netapp.LIVE_CHAT_URL + 'bc.chat?cbdid=299731303167067454&amp;wdid=2304121612912153553&amp;url=' + escape(document.location.href), 
		chatID: 'Chat3476938401032003315' 
	} 

};
netapp.ui.livechat = function(obj, options) {
	//gefn refactor once all country accounts are populated
	var cc = (options.country !== "") ? options.country : netapp.util.getCC();
	this.newWindow = window.open(netapp.ui.liveChatSettings[cc].url,
		netapp.ui.liveChatSettings[cc].chatID,
		'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=640,height=480');
	this.newWindow.focus();
	this.newWindow.opener = window;
	return false;

};
netapp.ui.map = function(obj, options) {

	var directionsDisplay = new google.maps.DirectionsRenderer(),
		directionsService = new google.maps.DirectionsService(),
		infowindow = new google.maps.InfoWindow(),
		mapOptions = {
			center: new google.maps.LatLng(options.lat, options.lon),
			zoom: parseInt(options.zoom),
			panControl: true,
			zoomControl: true,
			scaleControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		},
		map = new google.maps.Map(document.getElementById("map_canvas"),
			mapOptions);
	
	function plotPoints() {
		var uiDataJSON = netapp.util.getJSON($(this).attr('data-ntap-mapPoint')),
			lat = uiDataJSON.lat,
			lon = uiDataJSON.lon,
			title = uiDataJSON.title;
		var image = new google.maps.MarkerImage('../../../../media.netapp.com/images/netapp-icon.png'/*tpa=http://media.netapp.com/images/netapp-icon.png*/,
			// This marker is 32 pixels wide by 32 pixels tall.
			new google.maps.Size(32, 32),
			// The origin for this image is 0,0.
			new google.maps.Point(0, 0),
			// The anchor for this image is the base of the flagpole at 0,32.
			new google.maps.Point(0, 32));

		var shadow = new google.maps.MarkerImage('../../../../media.netapp.com/images/netapp-icon-shaddow.png'/*tpa=http://media.netapp.com/images/netapp-icon-shaddow.png*/,
			// The shadow image is larger in the horizontal dimension
			// while the position and offset are the same as for the main image.
			new google.maps.Size(50, 32),
			new google.maps.Point(0, 0),
			new google.maps.Point(0, 32));

		var myLatLng = new google.maps.LatLng(lat, lon);
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			shadow: shadow,
			icon: image,
			title: title,
			flat: false
		});

		var content = ($(this).html());

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.close();
			infowindow.setContent(content);
			infowindow.open(map, marker);
		});

	}

	directionsDisplay.setMap(map);
	directionsDisplay.setPanel(document.getElementById("directionsPanel"));

	$('[data-ntap-mapPoint *= lat]').each(plotPoints);

	$(document).on(netapp.clickEvent, '#map_canvas [data-ntap-ui *= setOffice]', function() {
		var uiDataJSON = netapp.util.getJSON($(this).attr('data-ntap-ui')),
			address = uiDataJSON.options.address,
			text = uiDataJSON.options.text;
		document.getElementById("endCoords").value = address;
		document.getElementById("endLocation").value = text;
		document.getElementById('directionsPanel').style.display = 'block';
		// sprint 11: added focus element
		document.getElementById("start").focus();
	});


	$('#getDir').bind(netapp.clickEvent, function() {
		var start = document.getElementById("start").value;
		var end = document.getElementById("endCoords").value;
		var request = {
			origin: start,
			destination: end,
			travelMode: google.maps.TravelMode.DRIVING
		};
		directionsService.route(request, function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			}
		});
	});

	
};
netapp.ui.megaMenu = function(megaMenu) {
	$('header#banner').removeClass("no-js");
	megaMenu.hover(function(event) {
		var timeout = $(this).data("timeout");
		if (timeout) {
			clearTimeout(timeout);
		} 
		if (event.type === netapp.ui.menuSettings.showMenuEvent) {
			$(this).parent().siblings("http://www.netapp.com/us/static/js/li.tab").removeClass(netapp.ui.menuSettings.hoverClass);
			$(this).data("timeout", setTimeout($.proxy(function() {
				$(this).parent().addClass(netapp.ui.menuSettings.hoverClass);
				//console.log(this);
				if($(window).width() >= 800) {
					netapp.ui.equalHeight(this, {selector: ".pColumn h2"});
					netapp.ui.equalHeight(this, {selector: ".pColumn"});
				}
				$(this).children("ul").show();
			}, $(this)), netapp.MENU_SHOW_HOVER_DELAY));
		} else if (event.type === netapp.ui.menuSettings.hideMenuEvent) {
			$(this).data("timeout", setTimeout($.proxy(function() {
				$(this).parent().removeClass(netapp.ui.menuSettings.hoverClass);
				$(this).children("ul").hide();
			}, $(this)), netapp.MENU_HIDE_HOVER_DELAY));
		}
	});
};
netapp.ui.menuSettings = {
	showMenuEvent: 'mouseenter',
	hideMenuEvent: 'mouseleave',
	hoverClass: "hover"
};
/**
      The dialog ui behavior using jquery colorbox plugin.
*/

netapp.ui.modal = function(obj, options) {
	//$('#' + obj.id).colorbox(options);
	return false;
};
netapp.ui.moduleCarousel = function() {
	$('.moduleCarousel').flexslider({
		animation: "slide", //{fade} String: Select your animation type, "fade" or "slide"
		slideshow: true, //{true} Boolean: Animate slider automatically
		slideshowSpeed: 12000, //{7000} Integer: Set the speed of the slideshow cycling, in milliseconds
		animationLoop: true,
		animationSpeed: 600, //{600} Integer: Set the speed of animations, in milliseconds
		initDelay: 0, //{0} Integer: Set an initialization delay, in milliseconds

		// Usability features
		pauseOnHover: true, //{false} Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
		pauseOnAction: false,
		// Primary Controls
		controlNav: true, //{true} Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
		directionNav: false, //{true} Boolean: Create navigation for previous/next navigation? (true/false)

		// Carousel Options
		itemWidth: 253,
		itemMargin: 0,
		minItems: 1, //{0} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
		maxItems: 1, //{0} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
		move: 1 //{0} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
	});

};
netapp.ui.rememberCountry = {
	init: function() {
		$(this).bind(netapp.clickEvent, function() {
			if ($('#countryChoice').attr('checked') === "checked") {
				var _href = $(this).attr("href");
				if (_href.indexOf('?') > -1) {
					$(this).attr("href", _href + '&setDefault=true');
				} else {
					$(this).attr("href", _href + '?setDefault=true');
				}
			}
		});
	}
};
netapp.ui.setOffice = function(obj, options) {
	document.getElementById("endCoords").value = options.address;
	document.getElementById("endLocation").value = options.text;
	document.getElementById('directionsPanel').style.display = 'block';
	// sprint 11: added focus element
	document.getElementById("start").focus();
};
/**
      The Customer Showcase carousels.
*/

netapp.ui.showcaseCarousel = function(obj, options) {

	$('section.carousel').flexslider($.extend(netapp.ui.carouselOptions, options));
	$('nav.carousel').flexslider({
		/* Putting all args here for speed of fine-tuning as we go. {value} indicates that's the default flexSlider value.
		     All comments from flexSlider - barkan */
		/* Putting all args here for speed of fine-tuning as we go. {value} indicates that's the default flexSlider value.
		   All comments from flexSlider - barkan */
		animation: "slide", //{fade} String: Select your animation type, "fade" or "slide"
		slideshow: false,
		animationLoop: false,
		animationSpeed: 1200, //{600} Integer: Set the speed of animations, in milliseconds
		initDelay: 0, //{0} Integer: Set an initialization delay, in milliseconds

		// Usability features
		pauseOnHover: true, //{false} Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
		pauseOnAction: false,
		// Primary Controls
		controlNav: false, //{true} Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
		directionNav: true, //{true} Boolean: Create navigation for previous/next navigation? (true/false)
		prevText: "Previous", //{Previous} String: Set the text for the "previous" directionNav item
		nextText: "Next", //{Next} String: Set the text for the "next" directionNav item

		// Special Properties
		controlsContainer: "nav.control", //{""} Selector: USE CLASS SELECTOR. Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be ".flexslider-container". Property is ignored if given element is not found.
		asNavFor: "section.carousel",
		start: function() {
			//remove the active class if the other slider is playing, to reduce confusion between control slide and animating slide
			if ($('section.carousel').data('flexslider').playing) {
				$('nav.carousel li.panel.active').removeClass('active');
			}
		},

		// Carousel Options
		itemWidth: 444,
		itemMargin: 0,
		minItems: ntapDevice === "phone" ? 2 : 3, //{0} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
		maxItems: ntapDevice === "phone" ? 2 : 3, //{0} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
		move: ntapDevice === "phone" ? 2 : 3 //{0} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.

	});

	$('nav.carousel li.panel').bind(netapp.clickEvent, function() {
		$('nav.carousel li.panel').removeClass('active');
		$('nav.carousel li.panel').removeClass('flex-active-slide');
		$(this).addClass('flex-active-slide active');
	});
};
netapp.ui.siteSearch = function(obj, options) {
	$('.utility ul form').css('width',$('.utility ul').outerWidth());
	
	if ($('.utility #headerSearchBox').width() === 0 ) {
		var utilityWidth = $('.utility ul').outerWidth();
		$('.utility #headerSearchBox').animate({width: '+='+utilityWidth}, 300, function() {
			$('.utility #headerSearchBox').css('overflow','visible');
			$('.utility').css('overflow','visible');
		});
		$( "#textBox" ).focus();
		$(document).keyup(function(e) {
			if (e.keyCode == 13) { 
				$( "#search" ).submit();
			}     // enter
			if (e.keyCode == 27) { 
				$('.utility #headerSearchBox').animate({width: '0'}, 300, function() {
					$('.utility #headerSearchBox').css('overflow','hidden');
					$('.utility').css('overflow','hidden');
				});
	  		}   // esc
		});
	}
	else {
		$('.utility #headerSearchBox').animate({width: '0'}, 300, function() {
			$('.utility #headerSearchBox').css('overflow','hidden');
			$('.utility').css('overflow','hidden');
		});
	}
	return false;
};
/**
      sidebar module slider
*/

netapp.ui.sliderModule = function() {
	$('.sliderModule').flexslider({
		animation: "slide", //{fade} String: Select your animation type, "fade" or "slide"
		slideshow: true, //{true} Boolean: Animate slider automatically
		slideshowSpeed: 12000, //{7000} Integer: Set the speed of the slideshow cycling, in milliseconds
		animationLoop: true,
		animationSpeed: 600, //{600} Integer: Set the speed of animations, in milliseconds
		initDelay: 0, //{0} Integer: Set an initialization delay, in milliseconds

		// Usability features
		pauseOnHover: true, //{false} Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
		pauseOnAction: false,
		// Primary Controls
		controlNav: true, //{true} Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
		directionNav: false, //{true} Boolean: Create navigation for previous/next navigation? (true/false)


		// Carousel Options
		itemWidth: 1000,
		itemMargin: 1000,
		minItems: 1, //{0} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
		maxItems: 1, //{0} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
		move: 1 //{0} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.

	});
};
netapp.ui.socialIcons = {
	init: function() {
		var theTitle = encodeURIComponent(document.title),
			thePage = window.location.href,
			theTagURL,
			tryParent = false,
			theClasses = '.' + $(this).attr('class').split(' ')[0];
		//console.log(theClasses);
		
		//hotfix to remove email icon from CN pages
		$('[lang="zh-CN"] a.small-email.addthis_button_email.at300b').parent().remove();
		
		switch (theClasses) {
			case ".large-facebook":
			case ".medium-facebook":
			case ".small-facebook":
				theTagURL = "http://www.facebook.com/sharer.php?u=" + thePage + "&title=" + theTitle;
				break;
			case ".large-twitter":
			case ".medium-twitter":
			case ".small-twitter":
				theTagURL = "http://twitter.com/share?url=" + thePage + "&text=" + theTitle + " - ";
				break;
			case ".large-linkedin":
			case ".medium-linkedin":
			case ".small-linkedin":
				theTagURL = "http://www.linkedin.com/shareArticle?mini=true&url=" + thePage + "&title=" + theTitle;
				//console.log(theTagURL);
				break;
			case ".large-qzone":
			case ".medium-qzone":
			case ".small-qzone":
				theTagURL = "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=" + thePage + "&title=" + theTitle;
				break;
			case ".large-google-plus":
			case ".medium-google-plus":
			case ".small-google-plus":
				theTagURL = "https://plus.google.com/share?url=" + thePage + "&h1=" + window.navigator.userLanguage || window.navigator.language;
				break;
			case ".large-xing":
			case ".medium-xing":
			case ".small-xing":
				theTagURL = "https://www.xing.com/app/user?op=share;url=" + thePage;
				break;
			case ".large-weibo":
			case ".medium-weibo":
			case ".small-weibo":
				theTagURL = "http://service.weibo.com/share/share.php?url=" + thePage + "&title=" + theTitle;
				break;
		}
		if (theTagURL !== '' && theClasses !== ".small-colleague" && theClasses !== ".small-email") {

			if ($(this).is("span")) {
				//console.log("bind int share widget: " + theTagURL);
				$(this).parent().bind(netapp.clickEvent, function() {
					window.open(theTagURL, '', 'scrollbars=yes,menubar=no,width=620,height=400,resizable=yes,toolbar=no,location=no,status=no');
					return false;
				});
			} else {
				//console.log("bind non int share widget: " + theTagURL);
				$(this).bind(netapp.clickEvent, function() {
					window.open(theTagURL, '', 'scrollbars=yes,menubar=no,width=620,height=400,resizable=yes,toolbar=no,location=no,status=no');
					return false;
				});
			}
		}
	}
};
netapp.ui.socialTabs = function(obj, options) {
	$('.headingOne').show();
	$('.headingTwo').hide();
	$('.headingThree').hide();

	if (!obj.className.match('socialTabs')) { // bit of error-checking to prevent unsetting .active on pageload.
		$(options.hide).hide();
		$('.socialTabs li').removeClass("active");
		$(obj).closest('li').addClass("active");
		$(options.show).show();
	}
	$('#communityTab > a').bind(netapp.clickEvent, function() {
		$('.headingOne').show();
		$('.headingTwo').hide();
		$('.headingThree').hide();
		$(".socialTabContent#0001").show();
		$(".socialTabContent#0002").hide();
		$(".socialTabContent#0003").hide();
	});
	$('#blogTab > a').bind(netapp.clickEvent, function() {
		$('.headingOne').hide();
		$('.headingTwo').show();
		$('.headingThree').hide();
		$(".socialTabContent#0001").hide();
		$(".socialTabContent#0002").show();
		$(".socialTabContent#0003").hide();
	});
	$('#twitterTab > a').bind(netapp.clickEvent, function() {
		$('.headingOne').hide();
		$('.headingTwo').hide();
		$('.headingThree').show();
		$(".socialTabContent#0001").hide();
		$(".socialTabContent#0002").hide();
		$(".socialTabContent#0003").show();
	});
	return false;
};
//why are we declaring a variable way out here? - ev
var showItems = 3;


netapp.ui.spotlightTabs = function(obj, options) {
	$(".spotlightTabs .tabsBar li a").bind(netapp.clickEvent, function() {
		$(".tabsBar li.active").removeClass("active");
		$(this).parent().addClass("active");
		var i = $(this).parent().index();
		var showTab = i;
		$(".spotlight.active").removeClass("active");
		$(".spotlight").eq(i).addClass("active");
		return false;
	});

	$('.spotlight').each(function() {
		var listItems = $(".items li", this).size();
		var pages = Math.ceil(listItems / showItems);
		//if(listItems > showItems) {
		//  $(".controls", this).addClass("active");
		//  for ( var i = 1; i <= pages ; i++ ) {
		//    $(".flex-control-nav", this).append( '<li><a href="#">' + i + '</a></li>' );
		//  }
		//$(".flex-control-nav", this).children("li").eq(0).children("a").addClass("flex-active");
		//}
		$(".items li", this).slice(0, 3).css("display", "block");
		var linerWidth = pages * 2.4;
		//$(".controls .liner", this).width(linerWidth+"em");
	});
};

function spotlightPaginate() {
	var num = parseInt($(".spotlight.active a.flex-active").text()) - 1;
	var start = showItems * num;
	var end = showItems + start;
	$(".spotlight.active .items li").hide();
	$(".spotlight.active .items li").slice(start, end).css("display", "block");
	if ($(".spotlight.active .flex-control-nav li").eq(0).children("a").hasClass("flex-active")) {
		$(".spotlight.active .flex-direction-nav li a.flex-prev").addClass("flex-disabled");
	} else {
		$(".spotlight.active .flex-direction-nav li a.flex-prev").removeClass("flex-disabled");
	}
	if ($(".spotlight.active .flex-control-nav li").eq(-1).children("a").hasClass("flex-active")) {
		$(".spotlight.active .flex-direction-nav li a.flex-next").addClass("flex-disabled");
	} else {
		$(".spotlight.active .flex-direction-nav li a.flex-next").removeClass("flex-disabled");
	}
	//console.log(num);
};
netapp.ui.stickyTabs = {
	init: function() {
		$(window).on('load', function(e){
			e.preventDefault();
			if (window.location.hash.indexOf('#') !== -1) {
				var currentTab = window.location.hash;
				$('#main .tab.active').removeClass('active');
				$('#main .tab a[href="'+currentTab+'"]').parents('li').addClass('active');
				$('select.dropdownMenu option:selected').removeAttr('selected');
				$('.dropdownMenu option[value="'+currentTab+'"]').prop('selected','selected');
				$('.tabbedSection').hide();
				$(currentTab).show();
			} else {
				$('.tabbedSection').hide();
				$('#main .tab1').addClass('active');
				$('#t1').show();
			}
		}).on('hashchange', function(e){
			e.preventDefault();
			if (window.location.hash.indexOf('#') !== -1) {
				var currentTab = window.location.hash;
				$('#main .tab.active').removeClass('active');
				$('#main .tab a[href="'+currentTab+'"]').parents('li').addClass('active');
				$('select.dropdownMenu option:selected').removeAttr('selected');
				$('.dropdownMenu option[value="'+currentTab+'"]').prop('selected','selected');
				$('.eventDetailSection').fadeOut('slow');
				$(currentTab).fadeIn('slow', function(){
					$('html,body').animate({
			          scrollTop: $(currentTab).offset().top
			        }, 1000);					
				});
				
			} else {
				$('#main .tab.active').removeClass('active');
				$('#main .tab:first-of-type').addClass('active');
				$('.eventDetailSection').fadeOut('slow');
				$('.eventDetailSection:first-of-type').fadeIn('slow');
			}
		});
		var initialDist = $('.tabsBarParent').offset().top;
		$(document).on("scroll", function() { 
			var top = $(window).scrollTop();
			var topDist = $('.tabsBarParent').offset().top;
			//console.log(top);
			if($('.tabsBarParent').hasClass("sticky")) {
				if (top < initialDist) { 
					$('.tabsBarParent').removeClass("sticky"); 
				} 
			}
			else {
				if (top > initialDist) { 
					$('.tabsBarParent').addClass("sticky"); 
				} 
			}
		});

	}
};
netapp.ui.tabs = {};

/**
 * tabs ui behavior
 * @param {Object} options
 */
netapp.ui.tabs = function(obj, options) {
	if (obj.className !== 'tabs') { // bit of error-checking to prevent unsetting .active on pageload.
		$(options.hide).removeClass("active").hide(); // putting .hide() in there to make it backwards compatible - zk
		$(netapp.TABS_LINKS).removeClass("active");
		$(obj).closest('li').addClass("active");
		var cId = $(obj).attr("href");
		$(cId).addClass("active");
		if ($(cId).is(":hidden")) { // making it backwards compatible - zk
			$(options.show).show();
		}
		return false;
	}
	//  if (obj.className != 'tabs') {// bit of error-checking to prevent unsetting .active on pageload.
	//  $(options.hide).hide();
	//  $(netapp.TABS_LINKS).removeClass("active");
	//  $(obj).closest('li').addClass("active");
	//  $(options.show).show();
	//  return false;
	//  }
};
netapp.ui.technoMercial = {
	init: function() {
		if ($(".technomercial260 div").length) {
			$(".technomercial260").hide();
		}
		if ($(".technomercial360 div").length) {
			$(".technomercial360").hide();
		}
	}
};
netapp.ui.ticker = function(obj, options) {
	var startDate = new Date(2012, 11, 1); // This is 2012 Dec 1
	var today = new Date();
	var incr = 75; // GB per second
	var startValue = 7267000000;

	var n = (today - startDate) / 1000;
	var renderNum = Math.round((n * incr) + startValue);

	$('.ticker').jOdometer({
		increment: 1,
		counterStart: renderNum < 10000000000 ? "0" + renderNum : renderNum,
		numbersImage: '../images/ticker/jodometer-numbers-netapp.png'/*tpa=http://www.netapp.com/us/static/images/ticker/jodometer-numbers-netapp.png*/,
		spaceNumbers: 1,
		offsetRight: -1,
		widthNumber: 18,
		widthDot: 6
	});
};
netapp.ui.utility = function(obj, options) {
	$('#countrySelectorLink').colorbox({'inline':'true','opacity':'0.4', 'width':'80%', 'maxWidth':'800px'});
};
netapp.ui.videoCarousel = function() {
	$('.videoCarousel').flexslider({
		animation: "slide", //{fade} String: Select your animation type, "fade" or "slide"
		slideshow: true, //{true} Boolean: Animate slider automatically
		slideshowSpeed: 12000, //{7000} Integer: Set the speed of the slideshow cycling, in milliseconds
		animationLoop: false,
		animationSpeed: 600, //{600} Integer: Set the speed of animations, in milliseconds
		initDelay: 0, //{0} Integer: Set an initialization delay, in milliseconds

		// Usability features
		pauseOnHover: true, //{false} Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
		pauseOnAction: false,
		// Primary Controls
		controlNav: false, //{true} Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
		directionNav: true, //{true} Boolean: Create navigation for previous/next navigation? (true/false)
		prevText: "Previous", //{Previous} String: Set the text for the "previous" directionNav item
		nextText: "Next", //{Next} String: Set the text for the "next" directionNav item

		// Carousel Options
		itemWidth: 253,
		itemMargin: 13,
		minItems: 3, //{0} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
		maxItems: 3, //{0} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
		move: 0 //{0} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
	});
	$('section.carousel li.panel').bind(netapp.clickEvent, function() {
		$('section.carousel li.panel.active').removeClass('active');
		$(this).addClass('active');
	});
};
netapp.ui.videoPlayer = {

	init: function() {

		var player, APIModules, mediaEvent, captionsEvent;

		netapp.ui.videoPlayer.isPlayerAdded = false;

		netapp.ui.videoPlayer.defaults = {
			bgcolor: '#FFFFFF',
			width: '560',
			height: '315',
			videoID: '',
			playerKey: 'AQ~~,AAAAPLMHdIE~,XTYhbdaYQW4CXsESMZ87DAzjDVu5EbVC',
			playerID: '',
			videoContainer: $('.videoContainer')
		}

		$('.js-loadVideo').on(netapp.clickEvent, function(e) {
			var options = netapp.ui.videoPlayer.defaults.videoContainer.data("ntapUi").options;
			options = $.extend(netapp.ui.videoPlayer.defaults, options);
			// console.log(options);
			e.preventDefault();

			netapp.ui.videoPlayer.addPlayer(options);
		});
	},

	addPlayer: function(options) {
		netapp.ui.videoPlayer.isPlayerAdded = true;
		var template = Handlebars.compile($('#playerTemplate').html());
		options.videoContainer.append(template(options));
		brightcove.createExperiences();
		netapp.ui.videoPlayer.toggleVideo(options.videoContainer);
		$('html, body').animate({
			scrollTop: options.videoContainer.position().top
		}, 1000);
	},

	toggleVideo: function(el) {
		if(el.hasClass('hidden')) {
			el.removeClass('hidden');
			el.prev().addClass('hidden');
			netapp.ui.videoPlayer.closeVideo();
		} else {
			el.prev().removeClass('hidden');
			el.addClass('hidden');
		}
	},

	closeVideo: function() {
		$('<div class="close"><a href="#">x</a></div>').prependTo(netapp.ui.videoPlayer.defaults.videoContainer);
		$('.close').on(netapp.clickEvent, function() {
			$(this).remove();
			netapp.ui.videoPlayer.removeVideo();
		});
	},

	removeVideo: function() {
		netapp.ui.videoPlayer.toggleVideo(netapp.ui.videoPlayer.defaults.videoContainer);
		netapp.ui.videoPlayer.defaults.videoContainer.removeAttr('style');
		experienceModule.unload;
		$('#playerTemplate').nextAll().remove();
		netapp.ui.videoPlayer.isPlayerAdded = false;
	},

	onTemplateLoad: function(experienceID) {
		console.log("onTemplateLoad");
		player = brightcove.api.getExperience(experienceID);
		APIModules = brightcove.api.modules.APIModules;
		mediaEvent = brightcove.api.events.MediaEvent;
		captionsEvent = brightcove.api.events.CaptionsEvent;
	},

	onTemplateReady: function(evt) {
		console.log("onTemplateReady");
		videoPlayer = player.getModule(APIModules.VIDEO_PLAYER);
		experienceModule = player.getModule(APIModules.EXPERIENCE);
		captionsModule = player.getModule(APIModules.CAPTIONS);
		contentModule = player.getModule(APIModules.CONTENT);

		captionsModule.setCaptionsEnabled(true);
		videoPlayer.play();

		videoPlayer.getCurrentRendition(function (renditionDTO) {
			var newPercentage = (renditionDTO.frameHeight / renditionDTO.frameWidth) * 100;
			newPercentage = newPercentage + "%";
			$('.videoContainer').css({paddingBottom: newPercentage});
		});

	}

};
/*jshint unused:false*/
/*jshint evil: true */
/*global location:true, Modernizr:true, ntapDevice:true, _gaq:true, Omniture:true, escape: true, google:true, _elqQ:true */
/*global APIModules:true */
/*global brightcove:true */
/*global modMenu:true */
/*global modSocial:true */
/*global BCMenuEvent:true */
/*global BCMediaEvent:true */
/*global BCExperienceEvent:true */
/*global bcTemplateReady:true */
/*global bcTemplateLoaded:true */








































netapp.VIDEO_LIBRARY_SERVICE = netapp.util.getBaseURL() + "/system/getVideoSearchResults.aspx";
netapp.LIBRARY_SERVICE = netapp.util.getBaseURL() + "/system/getSearchResults.aspx";
netapp.IS_EVENTS = false;
netapp.CUSTOMER_SHOWCASE_SEARCH_SERVICE = {
	'us': '?ft=library_results_html&q2=Customer+Story&x2=lib_doc_type',
	'de': '?ft=library_results_html&q2=Kundenreferenz&x2=lib_doc_type',
	'uk': '?ft=library_results_html&q2=Customer+Story&x2=lib_doc_type'
};





setTimeout(function() {
	$(".iframe3d").contents().find(".close3d").bind(netapp.clickEvent, function() {
		netapp.ui.growElement.hideFunc();
	});
}, 5000);


$(document).ready(function() {
	
	$(document).bind(netapp.clickEvent, function(event) {
		//console.log(event.target);
		if (!$(event.target).is("#textBox")) {
			if ($('.utility #headerSearchBox').width() > '0' ) {
				$('.utility #headerSearchBox').animate({width: '0'}, 300, function() {
					$('.utility #headerSearchBox').css('overflow','hidden');
					$('.utility').css('overflow','hidden');
				});
			}
		}
	});
	
	//REDESIGN-90 overwriting defaults for search box functionality
	$('#submitSearchHeader').submit(function(event) {
		window.location.href = $('#submitSearchHeader').attr('action') + '/?q=' + $('#textBox').val().replace(/\s+/g, '+').replace(/\//g, '~2F') + '&searchtype=general';
		event.preventDefault();
	});

	//hotfix for linkType, remove once we determine and implement long term solution
	$('.container1 .linkType').each(function() {
		$(this).text(' (PDF)');
	});
	
	// moves linkType to before the link in the right-rail
	if($('.linkList li .linkType')) {
		$('.linkList li .linkType').each (
			function(){
				$(this).prependTo($(this).parent());
			}
		);
	}
	
	//hotfix to remove email icon from CN pages
	if(window.location.href.indexOf("/cn/") > -1) {
		$('a.small-email.addthis_button_email').parent().remove();
	}
	
	// Set up the country select for mobile
	if ($('#changeCountry').length) {
		$('#changeCountry').on(netapp.clickEvent, function(e) {
			var newCountry = $('#footerCountryList').val();
	
			if (newCountry !== "") {
				location = $('#footerCountryList').val();
			}
			return false;
		});
	}
	if (netapp.clickEvent === 'tapone') {
		$('#changeCountry').on('click', function(e) {
			return false;
		});
	}

	$('figure.video.hidden #BCLvideoWrapper').prepend('<div class="close"><a href="#">Close <img src="../images/v-form-close.png"/*tpa=http://www.netapp.com/us/static/images/v-form-close.png*/ alt="Close" /></a></div>');
	$('.W01 .container1 table, .W03 .container1 table').each(function() {
		if ($(this).parents('.tableContainer, .tableContainor, table').size() === 0) {
			$(this).wrap('<div class="tableContainer" />');
		}
	});
	
	//moving all videos and technomercials to the bottom
	$('.W03 .container1 figure.video, .W03 .container1 figure.technomercial360, .W03 .container1 figure.technomercial260').each(function() {
		var figureWithVideo;
		figureWithVideo = $(this).detach();
		figureWithVideo.appendTo( ".container1" );
	});

	$('h1.tabHeader').bind(netapp.clickEvent, function() {
		if ($(this).parent().hasClass('active')) {
			return false;
		} else {
			$(this).parent().parent().children('section.tabContent.active').children('.liner').slideToggle(400).parent().removeClass('active');
			$(this).siblings('.liner').slideToggle(400).parent('section.tabContent').addClass('active');
			return false;
		}
	});

	if($('.ntapForm').parent().parent('.module')) {
		$('.ntapForm').parent().parent('.module').addClass('fcModule');
		$('.module .ntapForm select').parent().parent().addClass('selectBox');
	}
	netapp.ui.init();
	$('.searchFilter li:has(ul)').addClass('category');
	if (location.href.match("/library/") && !(location.href.match("videos"))) {
		netapp.library.init();
	}
	if (location.href.match('customer-showcase')) {
		netapp.library.showcase.init();
	}
	if (location.href.indexOf("http://www.netapp.com/us/static/js/videos.aspx") > 0) {
		netapp.library.video.init();
	}
	if (location.href.indexOf("/events/") > 0) {
		netapp.events.init();
	}
	if (location.href.indexOf("/search/") > 0) {
		netapp.sitesearch.init();
	}
	netapp.library.video.attachVideoClicks();
	if ($('div#BCLvideoWrapper[data-ntap-video-load]').length) {
		netapp.library.video.preparePageContainers();
	}
	

});

// midsize containers equalish heights
$(window).ready(function(){
	$('.W10.no-touch .container1').height($('.W10.no-touch .container2').height() - 70);
});

// home page form module interaction
var homeSubmit = 0;
$('.no-touch').on('click', 'div.close', function(){
	if($('.ntapForm').is(':visible')) {
		$('.ntapForm').slideUp(function(){
			$(this).parents('.hmodule').removeClass('active');
			$('.close').remove();
		});
	} else if($('#embeddedThanks').is(':visible'))  {
		homeSubmit = 1;
		$('#embeddedThanks').slideUp(function(){
			$(this).parent('.hmodule').removeClass('active');
			$(this).siblings('.close').remove();
		});
	}
});

$('.no-touch').on('click', 'div.hmodule:last-child', function(){
	if($(window).width() > 970) {
		if(!$(this).hasClass('active') && homeSubmit == 0) {
			$(this).addClass('active');
			$(this).find('.ntapForm').slideDown(function(){
				$(this).parents('.hmodule').prepend('<div class="close">&nbsp;</div>');
				$('.ntapForm :input:enabled:visible:first').focus();
			});
		} else if(!$(this).hasClass('active') && homeSubmit == 1) {
			$(this).addClass('active');
			$(this).find('#embeddedThanks').slideDown(function(){
				$(this).parent().prepend('<div class="close">&nbsp;</div>');
			});
		}
	}
});
function homeModHeight(){
	if ($(window).width() < 971 && $(window).width() > 600) {
		$('.homeModules').height($('.hmodule:last-child .fcModule').innerHeight() + $('.hmodule:last-child p:first-of-type').innerHeight() + 40);
	} else {
		$('.homeModules').removeAttr('style');
	}	
}
if ($('.homeModules').length) {
	$(window).on('load', homeModHeight);
	$(window).on('resize', homeModHeight);
}
//Legacy functions for inline video anchors tags using light window modal

var myLightWindow = {};
myLightWindow._playModalVideo = function(videoId, cc, var1, var2) {
	cc = "";
	function playVideo() {
		if ($("#BCLvideoWrapper").length === 0) {
			if ($(".tabsBar").length > 0) {
				if ($("#SocialShare").length > 0) {
					$('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/><br/>').insertAfter(".container1 #SocialShare");
				} else {
					$('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/><br/>').insertAfter(".container1 .tabsBar");
				}
			} else {
				if ($("#BCLvideoWrapper").length === 0) {
					$(".container1 section").prepend('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/><br/>');
				}
			}
		}
		NetAppPlayer.init('libraryVideoPlayer', document.getElementById('BCLvideoWrapper'));
		netapp.library.video.addVideoPlayer(videoId, cc);
		$("html, body").animate({
			scrollTop: $("#BCLvideoWrapper").position().top
		}, 1000);
	}
	
	if (typeof brightcove === 'undefined') //load bricghtcove if not loaded
	{
		$.getScript("../../../../admin.brightcove.com/js/BrightcoveExperiences.js"/*tpa=http://admin.brightcove.com/js/BrightcoveExperiences.js*/).done(function() {
			$.getScript("../../../../admin.brightcove.com/js/APIModules_all.js"/*tpa=http://admin.brightcove.com/js/APIModules_all.js*/).done(function() {
				playVideo();
			});
		});
	} else {
		playVideo();
	}

};

var BCVideoPlayer = {};
BCVideoPlayer.playVideo = function(videoId) {
	function playVideo() {
		if ($("#BCLvideoWrapper").length === 0) {
			if ($(".tabsBar").length > 0) {
				if ($("#SocialShare").length > 0) {
					$('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/><br/>').insertAfter(".container1 #SocialShare");
				} else {
					$('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/><br/>').insertAfter(".container1 .tabsBar");
				}
			} else {
				if ($("#BCLvideoWrapper").length === 0) {
					$(".container1 section").prepend('<figure class="video"><div id="BCLvideoWrapper"></div></figure><br/><br/>');
				}
			}
		}
		NetAppPlayer.init('libraryVideoPlayer', document.getElementById('BCLvideoWrapper'));
		netapp.library.video.addVideoPlayer(videoId, "");
		$("html, body").animate({
			scrollTop: $("#BCLvideoWrapper").position().top
		}, 1000);
	  $('#BCLvideoWrapper').mousedown(function() {
	  	ga('send', 'event', 'Video', 'Interaction', videoId);
	  });
	}

	if (typeof brightcove === 'undefined') //load bricghtcove if not loaded
	{
		$.getScript("../../../../admin.brightcove.com/js/BrightcoveExperiences.js"/*tpa=http://admin.brightcove.com/js/BrightcoveExperiences.js*/).done(function() {
			$.getScript("../../../../admin.brightcove.com/js/APIModules_all.js"/*tpa=http://admin.brightcove.com/js/APIModules_all.js*/).done(function() {
				playVideo();
			});
		});
	} else {
		playVideo();
	}

};
