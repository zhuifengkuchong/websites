var __elq_form_pagename = window.location.pathname.split('/').pop().split('.')[0];

var formClickEvent = Modernizr.touch ? 'tapone' : 'click';

var key = "BAD4@.56CEGFHIJKLVWdfTUhijXYZbacemngMNOPQRSopqrstuvz018923klwxy7";
var base = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@.0123456789";

function encode(str)
{
  var encodedStr = "";
  for (i = 0; i < str.length; i++) {
    current = str.charAt(i);
    idx = base.indexOf(current);
    nextVal = (idx == -1) ? current : key.charAt(idx);
    encodedStr += nextVal;
  }
  return encodedStr;
}

// Need to trigger the webshims polyfill so that the ready state later can work with it - barkan
if(jQuery.webshims !== undefined) {
  $.webshims.setOptions({
    waitReady:false
  });
  $.webshims.polyfill('forms forms-ext');
}  

//this is the callback for the company field and name/title auto populate once user enters value into company or email field.
var db_auto_complete_callback = function(data){
// A selection was made from the drop down
  if(pick = data.pick) {
  // Intentionally uses the assignment operator (=) instead of the equality operator (==) to
  // check if data.pick is not null or undefined and assign it to a convenient local variable, company.

  // Check if full firmographic data was returned by checking company_name.
  // In this example we ignore if just registry info.
    if(pick.company_name) {
      var pk_companyname = pick.company_name;
      var pk_industry = pick.industry;
      var pk_subindustry = pick.sub_industry;
      var pk_revenue_range = pick.revenue_range;
      // More company info available if desired...
    }
  }
  // An email was entered
  if(person = data.person) {
    // Set Person fields. firstname, lastname, and title may be undefined
    // if a contact wasn't found.
    if(jQuery('#first_name').length >0) {
      jQuery('#first_name').attr('value', ('first_name' in person) ? person.first_name : ''); 
      jQuery('#last_name').attr('value', ('last_name' in person) ? person.last_name : '');
      jQuery('#title').attr('value', ('title' in person) ? person.title : '');
    }
    var pn_firstname = person.first_name;
    var pn_lastname = person.last_name;
    var pn_title = person.title;
    var pn_companyname = person.company_name;
    // More company info available if desired...
  }
  // Match made based on typing in company field. Use when no pick object is available.
  if(input_match = data.input_match) {
    var im_companyname = input_match.company_name;
    // More company info available if desired...
  }
};
	
// Toggle portions of the form on/off, depending on whether the user is known or not - barkan
var toggleIfKnown = function() {
  hideIfKnown();
  showIfKnown();
};

function revealForm () {
    $('.ntapForm').animate({opacity:1},200);
}

function hideIfKnown () {
   // No hidden fields should be required
    $('[data-elq-hideIfKnown]').each(function(){
      $(this).find('[required]').attr('required',false);
      $(this).hide();
    });
}

function showIfKnown () {
  $('[data-elq-showIfKnown]').each(function(){
    $(this).show();
  });
}

var prePopulate = function() {
  if(GetElqContentPersonalizationValue) {
    var elqForm = jQuery('form.ntapForm[data-elq-prepop="true"]');
    if(elqForm.length > 0) {
      var inputs = jQuery('form.ntapForm input, form.ntapForm select, form.ntapForm textarea').not(':button,:hidden:not(#C_EmailAddress,[data-elq-serialized]),:image');

      //inputs.add($('.ntapForm [data-elq-serialized]'));
      
      inputs.each(function() {
        var thisPvalue = GetElqContentPersonalizationValue(jQuery(this).attr('id'));
        if( thisPvalue != "" &&  thisPvalue != "0" && thisPvalue != "N" && thisPvalue != "n") {
          if ((this.type == "radio" || this.type == "checkbox") ) {
            this.checked = true;
          }else {
            jQuery(this).val(thisPvalue);
          }
        }
        if ($(this).attr('data-elq-serialized')) {
          var serializedValues = ($(this).val()).split("::"); 
          $(this).closest('[data-elq-serialize]').find('[type=checkbox]').each(function(){
            for (var n=0,l=serializedValues.length;n<l;n++) {
              if (this.value == serializedValues[n]) {
                this.checked = true;
              }    
            }
          });
        }	
      });
      toggleIfKnown();
    }
  }
};

function PPPField() {}

function PPPField (fldName, liIsHidden, fldValue)
{
	if (fldValue == undefined || fldValue == "null" || fldValue == null)
		fldValue = "";
	this.fldName = fldName;
	this.fldValue = fldValue;
	this.dependentOnFldName = "";
	this.liIsHidden = liIsHidden;
	this.liName = this.fldName + "_LI";
	this.isDependent = false; 
	PPPField.prototype.setFldVal(this);
	PPPField.prototype.setCMFldVal(this);
	PPPField.prototype.setValFromDependent(this);
	PPPField.prototype.setCMValFromDependent(this);
}

PPPField.prototype.setCMFldVal = function (obj){
	if (obj.fldValue != undefined && obj.fldValue != ""){
		$("#"+obj.fldName+"_CM").val(obj.fldValue).change();
		$("#"+obj.fldName).val(obj.fldValue).change();
	}
};

PPPField.prototype.setCMValFromDependent = function(obj){
	for (var i=0;i<FLD_DEPENDENCIES.length;i++)
	{
		if (obj.fldName == FLD_DEPENDENCIES[i][1])
		{
			obj.dependentOnFldName = FLD_DEPENDENCIES[i][0];
			if (typeof(GetElqContentPersonalizationValue) !== 'undefined' && typeof(GetElqContentPersonalizationValue) === 'function') { 
				if(GetElqContentPersonalizationValue(FLD_DEPENDENCIES[i][0]) != ""){
					obj.fldValue = GetElqContentPersonalizationValue(FLD_DEPENDENCIES[i][0]);
					$("#"+obj.fldName+"_CM").val(GetElqContentPersonalizationValue(FLD_DEPENDENCIES[i][0])).change();
				}
			}
			this.isDependent = true;
			break;
		}
	}
};

PPPField.prototype.setFldVal = function (obj){
	if (obj.fldValue != undefined && obj.fldValue != "")
		$("#"+obj.fldName).val(obj.fldValue).change();
};

PPPField.prototype.setValFromDependent = function(obj){
	for (var i=0;i<FLD_DEPENDENCIES.length;i++)
	{
		if (obj.fldName == FLD_DEPENDENCIES[i][1])
		{
			obj.dependentOnFldName = FLD_DEPENDENCIES[i][0];
			if (typeof(GetElqContentPersonalizationValue) !== 'undefined' && typeof(GetElqContentPersonalizationValue) === 'function') { 
				if(GetElqContentPersonalizationValue(FLD_DEPENDENCIES[i][0]) != ""){
					obj.fldValue = GetElqContentPersonalizationValue(FLD_DEPENDENCIES[i][0]);
					$("#"+obj.fldName).val(GetElqContentPersonalizationValue(FLD_DEPENDENCIES[i][0])).change();
				}
			}
			this.isDependent = true;
			break;
		}
	}
};

PPPField.prototype.processHideLI = function(obj){
	this.liIsHidden = true;
	if ($("#"+obj.liName) != undefined)
	{
		$("#"+obj.liName).attr("class","hidden");
		$("#"+obj.fldName).removeAttr("required");
		
		if(!dontChangetwoFldsToDisplay){
			twoFldsToDisplay = twoFldsToDisplay.filter(function (el) {
								return el.name !== obj.fldName;
								});
		}
	}
};

PPPField.prototype.processShowLI = function(obj, requiredAttr){
	if (requiredAttr == undefined || requiredAttr == "null" || requiredAttr == null)
		requiredAttr = true;
	this.liIsHidden = false;
	if ($("#"+obj.liName) != undefined)
	{
		$("#"+obj.liName).removeAttr("class");
		if (obj.fldName.indexOf("_CM") == -1 )
		{
			if(obj.liName != "OPTIN_GEN_LI")
				$("#"+obj.liName).attr("class","col-xs-12 col-sm-8 col-md-4");
		}
		if(requiredAttr)
			$("#"+obj.fldName).attr("required","true");
		else
			$("#"+obj.fldName).removeAttr("required");
		if(!dontChangetwoFldsToDisplay)
			twoFldsToDisplay.push(obj);
	}
};

PPPField.prototype.isOtherStateFldHidden = function(obj){
	//if ($("#"+obj.fldName).attr("id") == "C_State_Other1" && GetElqContentPersonalizationValue("C_State_Other1") == "" && (GetElqContentPersonalizationValue("C_State_Prov") != "" || $("C_State_Prov_LI").attr("class") == undefined)) // Only hidden class can be applied to its LI
	if ($("#"+obj.fldName).attr("id") == "C_State_Other1" && (jQuery.inArray( ($( "#C_Country" ).val()).trim(), COUNTRIES_HAVING_STATE)) == -1) // Country for which states arent known
		return false;
	return true;
};

// Show Contact Me - No Matter What
function processContactMe(){
	if (typeof(GetElqContentPersonalizationValue) !== 'undefined' && typeof(GetElqContentPersonalizationValue) === 'function') { // We can talk to ELQ
		var thePPField = new PPPField("C_Contact_Me1", true, GetElqContentPersonalizationValue("C_Contact_Me1"));
		if(thePPField.fldValue == "1") {
			$("#"+thePPField.fldName).prop("checked",false).change();
			thePPField.processShowLI(thePPField, false);
		}
	}
}

// Opt In Gen once selected should not be shown!
function processOptInGen()
{
	if (typeof(GetElqContentPersonalizationValue) !== 'undefined' && typeof(GetElqContentPersonalizationValue) === 'function') { // We can talk to ELQ
		var thePPField = new PPPField("OPTIN_GEN", true, GetElqContentPersonalizationValue("OPTIN_GEN"));
		if(thePPField.fldValue == "1") {
			$("#"+thePPField.fldName).prop("checked",true).change();
			//thePPField.processHideLI(thePPField);
			$("#optinDiv").hide();
		}
		else{
			$("#"+thePPField.fldName).prop("checked",false).change();
			//thePPField.processShowLI(thePPField, false);
			$("#optinDiv").show();
		}
	}
}

function processDateCreated()
{
	if (typeof(GetElqContentPersonalizationValue) !== 'undefined' && typeof(GetElqContentPersonalizationValue) === 'function') { // We can talk to ELQ{
		var thePPField = new PPPField("C_DateCreated", true, GetElqContentPersonalizationValue("C_DateCreated"));
		if(thePPField.fldValue == undefined || thePPField.fldValue == "") {
			$("#"+obj.fldName).val(Date.now());
		}
	}
}

var progressiveProfile = function(ranProfile) { // Added for progressive profiling POC
    // hide ol and show loader
    
	if(ranProfile) // We have data, so re initialize and capture 2 fields which r displayed
		twoFldsToDisplay = [];
	var elqForm = jQuery('form.ntapForm');
	if(elqForm.length > 0) {
		var profiledFields = 0,
				maxEmpty = 2,
				haveData = ranProfile && GetElqContentPersonalizationValue !== 'undefined',
				PPWorthy = [
					"C_EmailAddress", //Business Email* 
					"C_Country", // Country
					"C_Job_Role1", //Job Role
					"C_Solution_Interest12", // Solution Interest
					"C_Company", // Company Name
					"C_Zip_Postal", // Zip/Postal Code
					"C_FirstName", // First name
					"C_LastName",  // Last name
					"C_Relationship_to_NetApp1", // Relationship w/NetApp
					"C_PhoneNumber", // Phone number -- field used to to send value
					"C_Industry1", // Industry
					"C_Number_of_Employees1", // Company Size
					"C_State_Prov", // State/Province  - ALWAYS BEFORE C_STATE_OTHER1
					"C_State_Other1", // State/Province Other
					//"C_OPTIN_GEN1", // Hidden field Opt In to get value from ELQ - ALWAYS BEFORE OPTIN_GEN
					//"OPTIN_GEN", // Visible field Opt In to send value to ELQ
					//"C_Contact_Me1" // Contact Me
				];
		var boolFldValExist;
		var tempPPFld;
		for (var n=0,PPL=PPWorthy.length;n<PPL;n++) {
			boolFldValExist = false;
			var thisPPID = PPWorthy[n],
					thisPPField = $("#" + thisPPID),
					foundField = thisPPField.size() > 0;

			if (foundField) {
				var thePPField;
				if (typeof(GetElqContentPersonalizationValue) !== 'undefined' && typeof(GetElqContentPersonalizationValue) === 'function') { 
					thePPField = new PPPField(thisPPID, true, GetElqContentPersonalizationValue(thisPPID));
					
					if (thisPPID == "C_State_Prov"){
						var otherSF = new PPPField("C_State_Other1", true, GetElqContentPersonalizationValue("C_State_Other1"));
						if (!otherSF.isOtherStateFldHidden(otherSF)){ // Other State field isnt hidden than 
							thePPField.processHideLI(thePPField);
							continue;
						}
					}
					
					if (thisPPID == "C_State_Other1"){
						if (thePPField.isOtherStateFldHidden(thePPField)){
							thePPField.processHideLI(thePPField);
							continue;
						}
					}
					
					if (profiledFields < 2 && thisPPID == "C_LastName" && !shouldHideLastName){ // If 2 fields aren't reached, last name is NOT to be hidden and first name is displayed
						profiledFields++; // Increase the count
						thePPField.processShowLI(thePPField);
					}
					
					if (profiledFields < 2 && (thePPField.fldValue == undefined ||  thePPField.fldValue == '') ) {
						if (thisPPID == "C_FirstName" && profiledFields == 1){ // First Name is the second field, then DO NOT show First Name, show EMAIL instead
								thePPField.processHideLI(thePPField); // Hide first name
								profiledFields++; // Increase the count
								// Show email, to consistently show 2 fields
								tempPPFld = new PPPField("C_EmailAddress", true, GetElqContentPersonalizationValue("C_EmailAddress"));
								tempPPFld.processShowLI(tempPPFld);
						}
						else if(thisPPID == "C_LastName" && profiledFields == 0){ // Last Name is the first field, then HAVE TO show First Name
							profiledFields++; // Increase the count
							thePPField.processShowLI(thePPField); // Show last name for sure
							profiledFields++; // Increase the count
							tempPPFld = new PPPField("C_FirstName", true, GetElqContentPersonalizationValue("C_FirstName"));
							tempPPFld.processShowLI(tempPPFld); // Also show first name right here
						}
						else if(thisPPID == "C_LastName" && profiledFields == 1){ // Last Name is the second field and this is set to false: shouldHideLastName
							if(!shouldHideLastName){
								profiledFields++; // Increase the count
								thePPField.processShowLI(thePPField);
							}
							else{
								thePPField.processHideLI(thePPField);
								continue;
							}
						}
						else if (thisPPID == "C_FirstName" && profiledFields == 0){ // First Name is the first field, then make sure last name is the next field even if its value exist
							shouldHideLastName = false;
							profiledFields++; // Increase the count
							thePPField.processShowLI(thePPField);
						}
						else{
							// Haven't shown two fields yet, and we don't have data for this field
							profiledFields++; // Increase the count
							thePPField.processShowLI(thePPField);
						}
					} else {
						if(ranProfile){
							if (!(thisPPID == "C_LastName" && !shouldHideLastName)){ // If its last name field and shouldHideLastName is marked as false show last name
								thePPField.processHideLI(thePPField);
							}
						}
					}
				}
				else {
					thePPField = new PPPField(thisPPID, true);
					if (profiledFields < 2) {
						profiledFields++; // Increase the count
						thePPField.processShowLI(thePPField);
					}
					else {
						thePPField.processHideLI(thePPField);
					}
				}
			}
		}
		
		if (profiledFields == 1) {
			var emailFld = new PPPField("C_EmailAddress", true, GetElqContentPersonalizationValue("C_EmailAddress"));
			emailFld.processShowLI(emailFld);
		}
		
		// Dont show any fields if all fields are filled up!
		// if (profiledFields == 0) {
			// var emailFld = new PPPField("C_EmailAddress", true, GetElqContentPersonalizationValue("C_EmailAddress"));
			// emailFld.processShowLI(emailFld);
			// var ctryField = new PPPField("C_Country", true, GetElqContentPersonalizationValue("C_Country"));
			// ctryField.processShowLI(ctryField);
		// }
		
		if (ranProfile){
			processOptInGen();
			//handleChkBoxes();
			processContactMe();
			//sprocessPhoneFld();
			processDateCreated();
			dontChangetwoFldsToDisplay = true;
		}
		toggleIfKnown();
	}
	ranProfile = true; // Seems like second time is the charm. Need to figure out what's wrong the first time it runs...
	
	$("#loaderDiv").delay(2500).hide(0);
	
};
        
//this is the part of callback function called from global footer hook
var __elq_dl_cb = function() {
	prePopulate(); 
	progressiveProfile(true); // Using alternate function for POC, with flag for successfulLookup
};

var getFormQueryParam = function(param) {
    var qs = window.location.search.substring(1); //remove the leading '?'
    var pairs = qs.split('&');
    var params = {};
    for (var i = 0; i < pairs.length; i++) {
      var nvArray = pairs[i].split('=');
      var name = nvArray[0].toLowerCase();
      var value = nvArray[1];
      params[name] = value; 
    }
    return params[param];
};

var getCookie = function(key) {
    if (document.cookie.length == 0) return;
    var start = document.cookie.indexOf(key + "=");
    if (start != -1) {
      var valStart = start + key.length + 1;
      var end = document.cookie.indexOf(";", valStart);
      if (end == -1) end = document.cookie.length;
      return unescape(document.cookie.substring(valStart, end));
    }
};	
///////// window load //////////
jQuery(window).load(function () {

  var ref_src = getFormQueryParam('ref_source');
  if(ref_src) {
    $('#REF_SOURCE').val(ref_src);
    if (this.GetElqCustomerGUID) {
      $('#elqCustomerGUID').val(GetElqCustomerGUID());
    }
  }
    
});

///////// demandbase stuff inside of doc ready /////////
jQuery(document).ready(function($) { 
  // Turn off validation so we can catch the invalid submit - barkan
  $('.ntapForm').attr('novalidate',"");
  if ($('.ntapForm [dir=ltr]').size() > 0) {
    $('.ntapForm').attr('dir','ltr'); // Quick hack to let Ben get LTR on the forms - barkan
  }

  if(window.location.href.indexOf('s=y') >= 0) {
      $('#ntapFormContainer').hide();
      $('.thankYou').show();
    
  }
  
  // Prep serialized checkboxes and radio buttons - barkan
  if ($('[data-elq-serialize]').size() > 0) {
    $('[data-elq-serialize]').each(function(){
      var serializedInput = $('<input type="hidden" data-elq-serialized="true" />'),
          serializedName = $(this).attr('data-elq-serialize'),
          serializeFrom = $(this).find('[type=checkbox]');

      serializedInput.attr('id',serializedName).attr('name',serializedName);
      $(this).append(serializedInput);

      // Instead of doing this on submit, do this when it is changed. Seems safer - barkan
      $(serializeFrom).change(function(){
        var serialTarget = $(this).closest('[data-elq-serialize]').find('[data-elq-serialized]'),
            serialSource = $(this).closest('[data-elq-serialize]').find('[type=checkbox]'),
            serialArray = [];
        for (var n=0,sl=serialSource.length;n<sl;n++) {
          if (serialSource[n].checked && serialSource[n].value != 1) {
            serialArray[serialArray.length] = serialSource[n].value; 
          }
        }
        serialTarget.val(serialArray.join("::"));
      });
    });
  }
  
		if(typeof(__ntap_dmdbase) !== "undefined"){
			if(__ntap_dmdbase !== undefined && __ntap_dmdbase.registry_country !== undefined ) {
				var user_country = __ntap_dmdbase.registry_country;
				$('select#C_Country').val(user_country).change();
				$('select#C_Country_CM').val(user_country).change();
			}
		}
  

  (function() {  
    if(jQuery('.ntapForm #registry_longitude').length == 0) {
      var frm = $('.ntapForm').first();
	  
	  if(getCookie('__ntap_global_id')) {
	      var __cookie = getCookie('__ntap_global_id');
	      var hidden1 = document.createElement("input");
          hidden1.setAttribute('type', 'hidden');
          hidden1.setAttribute('id', 'ntap-cid');
          hidden1.setAttribute('name', 'ntap-cid');
          hidden1.setAttribute('value', __cookie);
          frm.append(hidden1); 
	  }
      
      if(typeof __ntap_dmdbase !== 'undefined') {
        var db = __ntap_dmdbase;
        var db_wl = ('watch_list' in db ) ? db.watch_list : undefined;

        var _db_fields = new Array("forbes_2000","fortune_1000","country_name","city","employee_range","latitude","zip","country","demandbase_sid","company_name","registry_longitude","annual_sales","primary_sic","registry_country_code","registry_state","registry_city","marketing_alias","total_contacts","registry_latitude","revenue_range","street_address","isp","registry_zip_code","registry_company_name","ip","web_site","phone","registry_area_code","longitude","location_contacts","sub_industry","registry_country","stock_ticker","state","employee_count","industry","audience","audience_segment");
        var _num = _db_fields.length;
        var _wl_fields = new Array("nagp-id","s5000-penetration","vertical","duns","nagp-name","s5000-source","id","aoo-classification","s5000-flag","aw-country","aw-field-source","cdot");
        var _num_wl = _wl_fields.length;	  
        
        for ( var i=0;i<_num;i++) {
          var hidden1 = document.createElement("input");
          hidden1.setAttribute('type', 'hidden');
          hidden1.setAttribute('id', _db_fields[i]);
          hidden1.setAttribute('name', _db_fields[i]);
          hidden1.setAttribute('value', (_db_fields[i] in db) ? db[_db_fields[i]] : '' );

          frm.append(hidden1); 
        }    
        
        if(db_wl !== undefined) {
          for ( var i=0;i<_num_wl;i++)  {
            var hidden1 = document.createElement("input");
            hidden1.setAttribute('type', 'hidden');
            hidden1.setAttribute('id', _wl_fields[i]);
            hidden1.setAttribute('name', _wl_fields[i]);
            hidden1.setAttribute('value', (_wl_fields[i] in db_wl) ? db_wl[_wl_fields[i]] : '');

            frm.append(hidden1); 
          }
        }
        else {
          for ( var i=0;i<_num_wl;i++)  {
            var hidden1 = document.createElement("input");
            hidden1.setAttribute('type', 'hidden');
            hidden1.setAttribute('id', _wl_fields[i]);
            hidden1.setAttribute('name', _wl_fields[i]);
            hidden1.setAttribute('value', '');

            frm.append(hidden1); 
          }
        }
        
      }
      
      //add empty value hidden fields here, they will be filled w/ value when valid email address is entered by the user.
      var ps = ['first_name','last_name','title'];
      for(var i=0;i<ps.length;i++) {
          var hidden1 = document.createElement("input");
          hidden1.setAttribute('type', 'hidden');
          hidden1.setAttribute('id', ps[i]);
          hidden1.setAttribute('name', ps[i]);
          hidden1.setAttribute('value', '');
          frm.append(hidden1); 
      }		
    }	 // if the hidden fields are not on the form/page yet
	
    // attach demandbase auto complete widget for company field and email field. those fields ID need to match w/ the form.
    if(typeof Demandbase !== "undefined") { // error-checking as the absence of Demandbase breaks everything else in here - barkan
      Demandbase.CompanyAutocomplete.widget({
        company: "C_Company", 
        key: "849cadea23b0d8d467582591102279252f88f102",
        callback: db_auto_complete_callback 
      });
    }
  })();
  
///////// handling showing/hiding fields for Other select lists //////////
  $('.otherField').fadeOut(0);

  $('.ntapForm select[data-elq-otherField]').change(function(){
      var theOtherField = $($(this).attr('data-elq-otherField')).closest('label');
      if ($(this).val() == "Other") {
        $(theOtherField).slideDown().focus();
      } else {
        $(theOtherField).slideUp();
      }
    });
    
///////// validation //////////
    // Set up required field messages on document ready
    $('.form-group :input, select').not("[type='hidden']").not("button").each(function(i,r){
      var errorMsg = standardFormMsgs.fallback;

      if ($(r).is('[data-elq-customError]') && $(r).attr('data-elq-customError').indexOf('Example of custom message') == -1) { // Prevent the dummy example fvrom being displayed
        errorMsg = $(r).attr('data-elq-customError');
      } else if (standardFormMsgs.byId[r.id]){
        errorMsg = standardFormMsgs.byId[r.id];
      } else {
        for (var i=0,gLength=standardFormMsgs.bySelector.length; i < gLength; i++) {
          if ($(r).is(standardFormMsgs.bySelector[i][0])) {
            errorMsg = standardFormMsgs.bySelector[i][1];
          }
        }
      }

      $(r).attr('data-errormessage',errorMsg);
      $(r).closest('li').attr('id',r.id + "_LI"); // used to link back later
    });
	
	$('.ntapForm').ready(function(){
		progressiveProfile(false);
		//$('input, textarea').placeholder();
		$("form#progressive-profile").css("overflow", "");	
	}
	);
	
    function singleValidation (){
      // leaving removeclass here in case I suddenly remember why it was here due to some bug reappearing - barkan
      //$(this).removeClass('user-error');
      if ($(this).closest('.clickGroup :invalid').size()==0) {
        $('.msg',$(this).closest('.clickGroup')).fadeOut();
      }

      if ($(this).checkValidity()) {
        $(this).nextAll('.msg').fadeOut();
        $(this).removeClass('user-error');
      }
    }
    if(!$('.ntapForm').parent('.hmodule')) {
	    $('.ntapForm [required]').bind('focusout',singleValidation);
		$('.ntapForm [required]').bind('change',singleValidation);
	 }
	$('#cmDiv').find('select, input').each(
	function( intIndex ){
		$(this).bind('focusout',singleValidation);
	});

    $('.clickGroup:has([required]) input, .clickGroup:has(.group-required) [type=checkbox]').bind(formClickEvent,singleValidation);
    
	$(".ntapForm [required], #cmDiv input, #cmDiv select, .group-required").bind('invalid', function(e){
	
    //$('.ntapForm [required], .group-required').bind('invalid', function(e){
      // TODO: move a lot of this to a one-time function so we don't have to test to avoid adding junk a second time - barkan
      $(e.target).addClass('user-error');

      var clickGroup = $(this).closest('.clickGroup'),
          isClickGroup = clickGroup.size();

      if ((isClickGroup && $('.msg',clickGroup).size() == 0) || $(this).nextAll('.msg').size() == 0) {
        var msgWrapper = $('<em class="msg"></em>').css('opacity',0),
            msgText = $('<b></b>');

        $(msgWrapper).append(msgText);

        $(msgText).text($(this).attr('data-errormessage'));

        if (isClickGroup && $('legend',clickGroup).size() > 0 && $('http://www.netapp.com/us/static/js/legend .msg',clickGroup).size() == 0) {
          $('legend',clickGroup).append(msgWrapper);
        } else if ($(this).next('.input-date').size()) {
          $(this).next('.input-date').after(msgWrapper); // Because there's a fake element here that webshims put in for date functionality - barkan
        } else if (!isClickGroup) {
          $(this).after(msgWrapper);
        }
        $(msgWrapper).animate({opacity:1},500); // fadeIn changes the display property in some weird way here - barkan
      } else {
         $(this).nextAll('.msg').fadeIn();
         $('.msg',clickGroup).fadeIn();
      }
      e.preventDefault(); // Stop the form from getting validated by default UI
    });

    $('#embeddedResubmit').bind(formClickEvent,function(){
	  if($('#ntapFormContainer').length) {
		  $('#embeddedThanks').hide();
		  $('.ntapForm').show();
		  $('fieldset#nameCapture').show();
		  $('#ntapFormContainer').show();
		  if(document.getElementById('C_Contact_Me1').checked){
			  $("#C_Contact_Me1").prop("checked", false);
			  hideCMFields();
			  $("#topButton").attr("class","btn btn-primary col-xs-12");
			  $("#topButton").attr("onclick",""); // Make button functional
		  }
	  }
	  else{
		  $('.ntapForm [data-elq-showIfKnown]').hide();
		  $('#C_EmailAddress').attr('required',true);
		  $('#C_Country').attr('required',true);

		  $('#embeddedThanks').hide();
		  $('#embeddedKnown').hide();
		  $('.ntapForm [data-elq-hideIfKnown]').show();
		  
		  $('#C_EmailAddress').focus();
	  }
    });
	

///// webcast only ///////
$(".ntapForm #email").change(function(){ 
            var emailValue = $(this).val();
            var uniqueCodeValue = "602577";
            var uniqueIDEmailValue = uniqueCodeValue + " " + "(" + emailValue + ")";
            $("#uniqueIDEmail").val(uniqueIDEmailValue);
            $("#C_EmailAddress").val(emailValue);    
});
$(".ntapForm #company").change(function(){        
            var companyValue = $(this).val();
            $("#C_Company").val(companyValue);
});

///// submit //////
    $('.ntapForm').submit(function(){
      if ($(this).checkValidity()) {
        if ($('#embeddedThanks').size() > 0) {
          hideIfKnown();
		  $('#embeddedKnown').hide();
		  if($('#ntapFormContainer').length) {$('#ntapFormContainer').hide();}
		  $("#loaderDivTY").show();
		  setTimeout(function(){$("#loaderDivTY").hide();},4000);
          $('#embeddedThanks').show();
          $('.ntapForm').hide();
          ga('send', 'event', 'Form', 'Submit', location.href, 101);
          //addition of ds3 doubleclick form tracking
          if (typeof FLOOD1 == 'function') { 
			  FLOOD1('netapp', '1stne0'); 
		  }
		 var $topButton = $('#topButton');
		  var params = {};
          if (typeof Omniture !== 'undefined') {
		      var asset_name = ($('.ntapForm #PremiumContent').val() == undefined)?"":$('.ntapForm #PremiumContent').val();
			 if(!$topButton.hasClass('cmDownload')){
				  params = { 
					eVar47: "FC Registrations",
					events: "event79,event1",
					eVar5: asset_name
				  };
			  }
			 else{
				 params = { 
					eVar47: "Sales Inquiry",
					events: "event79,event80"
				  };
			 }
		      Omniture.Call.customLink({ href: '/' }, params, 'o');	
          }			  
        }
		if ($("input#SuccessPageURL").size() > 0){
			var successURL = $("input#SuccessPageURL").val();
			var email = $("input#C_EmailAddress").val();
			var updSuccessURL = successURL + "?ea=" + encode(email);
			$("input#SuccessPageURL").val(updSuccessURL);
		}
        return true;
      } else {
        // If we're inside a module, then don't show the error warning - barkan
        if ($(this).closest('.module').size() == 0 && $('.ntapForm #paidSearchForm').size() == 0) {
          var invalidFields = $('.ntapForm .user-error'),
              errorList = $('<ul></ul>');
          if (invalidFields.length) {
            $('.errorWarning').addClass('hasErrors');
            $('.errorWarning').html(
              invalidFields.length == 1 ? standardFormMsgs.errorWarningOne:standardFormMsgs.errorWarningMulti                 
            );
            $('.errorWarning').append(errorList);
  
            for (var i=0,errLength=invalidFields.length;i < errLength;i++) {
              var $badField = $(invalidFields[i]),
                  bId = $badField.attr('id'),
                  bLi = bId + "_LI",
                  bLabel = $('span',$badField.closest('li')).contents().eq(0).text().trim(),
                  bLegend =  $('legend',$badField.closest('fieldset')).contents().eq(0).text().trim(),
                  bLabel = $badField.is(':checkbox,:radio') ? bLegend : bLabel,
                  bMsg = $badField.attr('data-errormessage');
                  
              $(errorList).append($('<li data-error-el="#'+(bId)+'"><a href="#'+(bLi)+'"><b>'+ bLabel +"</b>: "+ bMsg +'</a></li>').bind(formClickEvent,
                function(){$($(this).attr('data-error-el')).delay(10).focus();} // gefn: this is temporary, will do better soon - barkan
              ));
            }
          }
        }
        ga('send', 'event', 'Form', 'Error', location.href + ':' + $('.ntapForm .user-error').data('errormessage'), 0);
        return false;
      }
    });
    // If this isn't being processed by Eloqua, show it now
    if (jQuery('form.ntapForm[data-elq-prepop="true"]').size() == 0) { // Will need to consider a different hook after POC
      revealForm();
    } else {
      // Just in case
      setTimeout(revealForm,1000);
    }

});
