if (document.getElementById('textBox')) {
	var acDSgeos = {
			'as':'sp10/04/f3/6d',
			'au':'sp10/04/f3/73',
			'br':'sp10/04/f3/74',
			'ch':'sp10/04/f3/6b',
                        'ca':'sp10/04/f6/01', 
			'cn':'sp10/05/09/69',
			'de':'sp10/04/f3/65',
			'es':'sp10/04/f3/69',
			'fr':'sp10/05/09/70',
			'il':'sp10/04/f3/67',
			'in':'sp10/05/09/72',
			'it':'sp10/04/f3/66',
			'jp':'sp10/04/f4/b2',
			'kr':'sp10/04/f3/77',
			'mx':'sp10/04/f3/78',
			'nl':'sp10/04/f3/79',
			'ru':'sp10/04/f3/7a',
			'se':'sp10/04/f3/6a',
			'uk':'sp10/04/f3/c3',
			'us':'sp10/04/f2/22'
		},
		thisAcDSgeo = (typeof netapp === 'object' && typeof netapp.util === 'object' && typeof netapp.util.getCC === 'function') ? acDSgeos[netapp.util.getCC()] : acDSgeos['us'],
		g_staged = (document.getElementById("sp_staged") ? document.getElementById("sp_staged").value : 0),
		protocol = (document.location.protocol == "https:" ? "https:" : "http:"),
		postfix = (g_staged ? "-stage/" : "/"),
		acDS = new YAHOO.util.ScriptNodeDataSource(
			(protocol + "//content.atomz.com/autocomplete/" +thisAcDSgeo + postfix),
			{'asyncMode':'ignoreStaleResponses','maxCacheEntries':1000,'responseSchema':{resultsList: "ResultSet.Result",fields: ["phrase"]}}
		),
		acObjsProps = {
			'queryDelay':	0.2,
			'useShadow':	false,
			'autoHighlight':	false,
			'minQueryLength':	1,
			'maxResultsDisplayed':	10,
			'animVert':	false,
			'queryQuestionMark':	true,
			'resultTypeList':	false,
			'formatResult':	function(oResultData, sQuery, sResultMatch) {return (sResultMatch) ? sResultMatch : "";},
			'generateRequest':	function(q) {return "?query=" + q + "&max_results=" + this.maxResultsDisplayed;}
		},
		acObjQ0 = new YAHOO.widget.AutoComplete("textBox", "autocomplete", acDS, acObjsProps),
		acObjQ1 = document.getElementById('q1') ? new YAHOO.widget.AutoComplete("q1", "q1complete", acDS, acObjsProps):false,
		acObjQ2 = document.getElementById('q2') ? new YAHOO.widget.AutoComplete("q2", "q2complete", acDS, acObjsProps):false,
		acSH0 = function(){document.submitSearchHeader.submit();},
		acSH1 = acObjQ1 ? function(){$('button#siteSearchButton').click();}:false,
		acSH2 = acObjQ2 ? function(){$('button#siteSearchButtonTwo').click();}:false;

	acObjQ0.itemSelectEvent.subscribe(acSH0);

	if (acObjQ1) {acObjQ1.itemSelectEvent.subscribe(acSH1)};

	if (acObjQ2) {acObjQ2.itemSelectEvent.subscribe(acSH2)};
}
;
