function setCookie(cname,cvalue,exdays){
	var d = new Date();
	d.setTime(d.getTime()+(exdays*24*60*60*1000));
	var expires = "expires="+d.toGMTString();
	document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}
 
function getCookie(cname){
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++)
	  {
	  //var c = ca[i].trim();
	  var c = $.trim(ca[i]);
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	}
	return "";
}

var cookieLaw = {};

// eu cookie law country to assigned to demandbase
if (typeof __ntap_dmdbase === 'undefined') {
  var result = $.ajax({url: "http://api.hostip.info/country.php", async: false}).responseText;
  if(result && result != '') {
  	cookieLaw.euCountry = $.trim(result);
  } else {
  	cookieLaw.euCountry = "US";
  }
} else {
  cookieLaw.euCountry  = __ntap_dmdbase.country;
}


cookieLaw._countries = ["BE","BG","CZ","DK","DE","EE","IE","EL","ES","FR","IT","CY","LV","LT","LU",
							"HU","MT","NL","GB","AT","PL","PT","RO","SI","SK","FI","SE","UK","GR","EU","AU","IN","AS","KR","CN","MX"];
cookieLaw.nonEUCountries = ["jp","br","il","ru"];
cookieLaw.euCheck = $.inArray(cookieLaw.euCountry,cookieLaw._countries);

cookieLaw.urlSplit = window.location.pathname.split("/");
cookieLaw.nonEU = $.inArray(cookieLaw.urlSplit[1], cookieLaw.nonEUCountries);

if(cookieLaw.nonEU > -1) {
  cookieLaw.euCheck = -1;
}


cookieLaw.isAccept = function(){
  var ret = null;
  var temp = cookieLaw.getCookie("eu-cookie-user-cookie");
  if(temp != null){
    var tempsplit = temp.split('|');
    if(tempsplit.length > 1){
      if(tempsplit[0] == 'true'){
        
        ret = true;
        
      }else{
        ret = false;
      }
      
    }
  }
  
  return ret;
}
  
cookieLaw.modal = {
  show:function() {
    //$('#cookie_notice_overlay').show();
    //$('#cookie_notice').show();
	var options = {'inline':'true','opacity':'0.4', 'href':'#cookieNotice', 'width':'42%', 'maxWidth':'420px', 'transition':'none'};
	options = $.extend(options, {onLoad:function(){$("#cboxContent").addClass("euCookieModalBG")},
		              onClosed:function(){$("#cboxContent").removeClass("euCookieModalBG");}});
		$.colorbox(options);
		$("#cookieNoticeShort a").bind('click', function() {
			$("#cookieNoticeShort").addClass("hidden");
			$("#cookieNoticeExpanded").removeClass("hidden");
			$.colorbox.resize();
			return false;

		});
    if($('#ss-linkExpand').is(":visible")) optOutCheck = true;
    // No more required
    //$('#form_cookie').attr('action', window.location.href);

    this._close();  
    this._continue(); 
    this._overlayClick();
  },
  _close:function() {
    $('#cboxClose').click(function(event){
                               
      var evID = event.target.id;
      
      if(evID == "cookie_notice" || evID == "cboxClose" ) {
        
        cookieLaw.modal._setYearCookie(true);
 

        //Need to refresh if the close button is clicked.
        window.location.reload();
        
          
      }
      
    });
    
  },
  _continue:function(){
    
    $('#storeCookie').click(function(){
      var isSession = false;
      var isContinue = true;
      if($('#enableCookie').is(':checked')){
        isContinue = false; 
        isSession = true;
      }
      cookieLaw.modal._setYearCookie(isContinue,isSession);
      // Need to refresh the page
      //$('#form_cookie').submit();
      window.location.reload();
      
      return false; 
      
    });

  },
  _overlayClick:function(){
    $('#cboxOverlay').click(function(event){
      var evID = event.target.id;
      if(evID == "cboxOverlay"){
        cookieLaw.modal._setYearCookie(true); 
        //Need to refresh the page on click of the overlay.
        window.location.reload();
      } 

    });

  },  
  _setYearCookie:function(isCookie,isSession){
    
    var session = false;
    if(isSession != undefined || isSession != null){
      session = isSession;
      
    }
    var monthNames = [ "January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December" ];
    var today = new Date();
    var dd = today.getDate();
    var mm = monthNames[today.getMonth()];
    var yyyy = today.getFullYear();
    var currentDate = mm+"-"+dd+"-"+yyyy;
        
    var name = 'eu-cookie-user-cookie';  
    var value =  isCookie+'|'+currentDate;
    var expires = '';
    if(!session){
      var date = new Date();
      date.setTime(date.getTime()+(3650*24*60*60*1000));
      
      expires = "; expires="+date.toGMTString();
    }
    document.cookie = name+"="+value+expires+"; path=/";
    

    
  }


}

// helper 
cookieLaw.shouldInject = function(){
  var temp = true;

  if(cookieLaw.euCheck > -1){
    if(cookieLaw.isAccept() == false || cookieLaw.isAccept() == 'false' ||cookieLaw.isAccept() == null ){
      temp = false;
    }
  }
  
  return temp;
}

//get the cookie
cookieLaw.getCookie = function (name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1)
    {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1)
    {
        end = dc.length;
    }
    return unescape(dc.substring(begin + prefix.length, end));
}



// original scripti

scripty = {
      _scripts:[]
    }

    scripty.add = function(script) {
      
     /* var starts = script.match("^http");
      if(starts != "http") {
        script = "http://www.netapp.com/includes/"+script;
      }*/
	  //script = "/us/static/js" + script;
      
      this._scripts.push(script);
    }

    scripty.injectAll = function() {
      
      var shouldDo = cookieLaw.shouldInject();
      
      if(shouldDo){
        for(i = 0;i<this._scripts.length;i++) {
        
          var tags = document.createElement('script');
          tags.type='text/javascript';
          tags.src=this._scripts[i];
          
          $('body').append(tags);
                    
        }
      } 
    }


$(document).ready(function(){
	if($("#cookieInfo").length > 0) {

	  var tempCookie = $.trim(cookieLaw.getCookie("eu-cookie-user-cookie"));
	  
	  if(tempCookie != null){
		  var eucookie = tempCookie.split('|');
		  if(eucookie.length > 1){
		      if(eucookie[0] == 'true'){
			  $('#cookieInfo').show();
			  var cookieInfo = eucookie[1].split("-");
			  var month = cookieInfo[0];
			  var day = cookieInfo[1];
			  var year = cookieInfo[2];														
			  $('#cookieInfoDay').html(day);
			  $('#cookieInfoMonth').html(month);
			  $('#cookieInfoYear').html(year);														
  
		      }
  
		  }
	    }
      }

});
