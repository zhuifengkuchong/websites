var getQueryParam = function(param) {
    var qs = window.location.search.substring(1); //remove the leading '?'
    var pairs = qs.split('&');
    var params = {};
    for (var i = 0; i < pairs.length; i++) {
      var nvArray = pairs[i].split('=');
      var name = nvArray[0];
      var value = nvArray[1];
      params[name] = value; 
    }
    return params[param];
}

var FirstLookup = true;
var __elq_userEmail = "";
function SetElqContent() {
  if (FirstLookup) {
    //LOOKUP B:  Eloqua Contact/DataCard/Prospect/Company Data Lookup from Email Address - the key is the contact lookup key inside of Elq
    __elq_userEmail = GetElqContentPersonalizationValue('V_Email_Address');
    if(__elq_userEmail != "") {
      _elqQ.push(['elqDataLookup', escape('a99cf9d85af3413b94153eff2599f829'), '<C_EmailAddress>' + GetElqContentPersonalizationValue('V_Email_Address') + '</C_EmailAddress>']);
      FirstLookup = false;
    }
  } else {
    //This is triggered after Lookup B fires. starts the intended action for the current page.
    if(GetElqContentPersonalizationValue('C_EmailAddress')) {
      if(__elq_userEmail == "") {
        __elq_userEmail = GetElqContentPersonalizationValue('C_EmailAddress');
      }       	
	  //we found the callback defined on the page
	  if(typeof __elq_dl_cb == 'function') {
        __elq_dl_cb();	    
	  }
    }  			
    if(typeof revealForm == 'function') {
        revealForm();     
    }
  }
}

if (typeof _elqQ !== 'undefined') {
  if(window.location.search.indexOf("ek=") >= 0) { //query string lookup
    _elqQ.push(['elqDataLookup', escape('3234859427884e16af8b990ae6fadba8'), '<ContactIDExt>' + getQueryParam("ecid") + '</ContactIDExt><C_EmailAddress>' + getQueryParam("ek") + '</C_EmailAddress>']);
    FirstLookup = false;
  }
  else {
    //LOOKUP A:  Eloqua Visitor Data Lookup from Cookie  -  the key is the vistor data lookup key inside of Elq
    if(typeof __elq_form_ty_pagename !== 'undefined') {
    setTimeout(function(){_elqQ.push(['elqDataLookup', escape('e99d7f6878b94a45ab4610defdc10609'), '']);},1800);
    }
    else {
    _elqQ.push(['elqDataLookup', escape('e99d7f6878b94a45ab4610defdc10609'), '']);
    //unsuccessful lookup, call progressiveProfile with false (just for POC)
    //progressiveProfile(false); 
    }
  }
}

function popDropDown(selectObj, valArray) {
    var i;
    for (i = 0; i < valArray.length; i++) {
        var opt = valArray[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        selectObj.appendChild(el);
    }
}

function popDropDowns(){
	popDropDown(document.getElementById('C_Solution_Interest12'), SOL_INT_ARRAY);
	popDropDown(document.getElementById('C_Job_Role1'), JOB_ROLE_ARRAY);
	popDropDown(document.getElementById('C_Industry1'), INDUSTRY_ARRAY);
	popDropDown(document.getElementById('C_Number_of_Employees1'), COMPANY_SIZE_ARRAY);
	popDropDown(document.getElementById('C_Relationship_to_NetApp1'), REL_TO_NETAPP_ARRAY);
}

function blankDropdownValue(obj){
	obj.selectedIndex = -1;
}

function hideFldAndLI(fldName){
	if (document.getElementById(fldName+'_LI') != undefined)
	{
		applyClassHidden(document.getElementById(fldName+'_LI'));
		markElementNotRequired(document.getElementById(fldName));
		//blankDropdownValue(document.getElementById(fldName));
	}
}

function showFldAndLI(fldName){
	if (document.getElementById(fldName+'_LI') != undefined)
	{
		removeClassHidden(document.getElementById(fldName+'_LI'));
		$(document.getElementById(fldName+'_LI')).attr( 'class', 'col-xs-12 col-sm-8 col-md-6' );
		markElementRequired(document.getElementById(fldName));
		//blankDropdownValue(document.getElementById(fldName));
	}
}

function applyClassHidden(obj){
	$(obj).attr( 'class', 'hidden' );
}

function removeClassHidden(obj){
	$(obj).removeAttr( 'class' ); // Only hidden class is applied so remove it
}

function markElementNotRequired(obj){
	$(obj).removeAttr("required")
}

function markElementRequired(obj){
	$(obj).prop("required","true")
}

function processStateDD(ctrySelectObj)
{
	if ((jQuery.inArray( ($( "#C_Country" ).val()).trim(), COUNTRIES_HAVING_STATE)) == -1) // Country for which states arent known
	{
		hideFldAndLI('C_State_Prov');
	}
	else
	{
		popDropDown(document.getElementById('C_State_Prov'), eval(($( "#C_Country" ).val()).toUpperCase().replace(/ /g,"_")+"_STATE_ARRAY"));
		hideFldAndLI('C_State_Other1');
	}
}

$( "#C_Country" ).change(
function() {
	processStateDD(this);
}
);

function handleChkBoxes()
{
	if(document.getElementById("C_OPTIN_GEN1").value == "1")
		document.getElementById("OPTIN_GEN").checked = true;
	if(document.getElementById("C_Contact_Me1").value == "1")
		document.getElementById("C_Contact_Me1").checked = true;
}

function onLoadToDo(){
	popDropDowns();
	hideCMFields();
}

function processPhoneFld()
{
	var busPhoneObj = document.getElementById("C_BusPhone");
	if (busPhoneObj != undefined)
	{
		if(busPhoneObj.value != "")
		{
			document.getElementById("C_PhoneNumber").value = busPhoneObj.value;
		}
	}
}
function SetChkBxSelection(obj){
		if (obj.checked){
			obj.value = "1";
			if (obj.id == "C_Contact_Me1") {
				showCMFields();
				if($('fieldset#nameCapture').length) {
					$('fieldset#nameCapture input[required], fieldset#nameCapture select[required]').removeAttr('required').removeClass('user-error form-ui-invalid');
					$('.ntapForm .msg').hide();
					$("#topButton").attr("class", $("#topButton").attr("class") + " cmDownload ");
				} else {
					$("#topButton").attr("class", "btn cmDownload col-xs-12"); // Make button non functional
					$("#topButton").attr("onclick","return(false);"); // Make button non functional
				}
				if (typeof Omniture !== 'undefined') {
					var params = { 
						events: "event6",
						eVar44: "Progressive"
				   };
					Omniture.Call.customLink({ href: '/' }, params, 'o');	
				}
				if ($('.hmodule').length && $(window).width() < 971) {
					$('.hmodule:last-child').addClass('cmActive');
				}
			}
		} else {
			obj.value = "0";
			$(obj).removeAttr('checked');
			if (obj.id == "C_Contact_Me1") {
				hideCMFields();
				if($('fieldset#nameCapture').length) {
					$('fieldset#nameCapture li:not(".hidden") input , fieldset#nameCapture li:not(".hidden") select').attr('required','true');
					$("#topButton").attr("class", $("#topButton").attr("class").replace("cmDownload",""));
				} else {
					$("#topButton").attr("class","btn btn-primary col-xs-12");
					$("#topButton").attr("onclick",""); // Make button functional
				}
			}
		}
}

function showCMFields()
{
	if($('fieldset#cmForm').length) {
		$('fieldset#cmForm').removeClass('hidden');
		$('fieldset#nameCapture').addClass('hidden');
		$('#topButton').hide();
		if ($('.hmodule').length && $(window).width() < 971) {
			$('.hmodule:last-child').addClass('cmActive');
		}
	} else {
		$("#cmDiv").removeAttr("class");	
	}
	$("#cmDiv").attr("required");
	$.each($('#cmDiv input, #cmDiv select, #cmDiv textarea').serializeArray(), 
			function(i, obj) { 
				//$('#'+obj.name).attr("class","col-xs-12 col-sm-8 col-md-6");
				if (obj.name != "Comment_CM")
					$('#'+obj.name).attr("required","required");
				var actFld = obj.name.replace("_CM","");
				$('#'+obj.name).val($("#"+actFld).val());
				
				$("#"+actFld).on('change', function() {
					var fldName = actFld + "_CM";
					$("#"+fldName).val(this.value);
				});
				
				$('#'+obj.name).on('change', function() {
					var fldN = obj.name.replace("_CM","");
					$("#"+fldN).val(this.value);
				});
				ContactMeFormData[obj.name] = obj.value; 
				}
			);
}

function hideCMFields()
{
	$.each($('#cmDiv input, #cmDiv select, #cmDiv textarea').serializeArray(), 
	function(i, obj) { 
		$('#'+obj.name).removeAttr("required");
		ContactMeFormData[obj.name] = obj.value; 
		}
	);
	if($('fieldset#cmForm').length) {
		$('fieldset#cmForm').addClass('hidden');
		$('fieldset#nameCapture').removeClass('hidden');
		$('#topButton').show();
		if ($('.hmodule').length && $(window).width() < 971) {
			$('.hmodule:last-child').removeClass('cmActive');
		}
	} else {
		$("#cmDiv").attr("class","hidden");	
	}
}
	
window.addEventListener ? window.addEventListener("load",onLoadToDo(),false) : window.attachEvent("onload",onLoadToDo);
