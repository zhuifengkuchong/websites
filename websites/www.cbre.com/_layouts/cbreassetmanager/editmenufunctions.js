/// <reference path="../Cbre/js/jquery-1.6.2-vsdoc.js"/*tpa=http://www.cbre.com/_layouts/Cbre/js/jquery-1.6.2-vsdoc.js*//>

EditMenuFunctions = {

    showAssetManagerDialog: function (mode) {
        clientContext = new SP.ClientContext.get_current();
        web = clientContext.get_site().get_rootWeb();
        var siteurl = _spPageContextInfo.webServerRelativeUrl;
        if (siteurl.charAt(siteurl.length - 1) != '/') {
            siteurl = siteurl + '/';
        }
        pageurl = siteurl + '_layouts/cbreassetmanager/Assetpicker.aspx';

        var windowWidth = 1024;
        var windowHeight = 728;
        if (FV4UI()) {
            var options;
            if (mode == "link")
                options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: EditMenuFunctions.dialogClosedLinkCallback };
            else
                options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: EditMenuFunctions.dialogClosedImageCallback };

            var dialog = SP.UI.ModalDialog.showModalDialog(options);
        }
        else {
            var features;
            if (window.showModalDialog) {
                features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
            }
            else {
                features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
            }
            var result;
            if (mode == "link")
                result = commonShowModalDialog(pageurl, features, EditMenuFunctions.dialogClosedLinkCallback);
            else
                result = commonShowModalDialog(pageurl, features, EditMenuFunctions.dialogClosedImageCallback);
        }
    },

    dialogClosedLinkCallback: function (dialogResult, results) {
        if (results == null || results == undefined)
            return;

        if (dialogResult == SP.UI.DialogResult.OK) {
            EditMenuFunctions.insertRTELink(results);
        }
    },

    insertRTELink: function (link) {

        //Delete any existing content the range contains
        RTE.Cursor.get_range().deleteContent();

        //Get a handle on the current range of the cursor inside the RTE
        var rng = RTE.Cursor.get_range().$3_0;

        //Create a DOM object representing the image and insert
        var d = rng.ownerDocument;
        var a = d.createElement("a");
        a.innerHTML = link.Title;
        a.setAttribute("href", link.Url);

        SP.UI.UIUtility.insertAfter(a, rng);
    },

    dialogClosedImageCallback: function (dialogResult, results) {
        if (results == null || results == undefined)
            return;

        if (dialogResult == SP.UI.DialogResult.OK) {
            EditMenuFunctions.insertRTEImage(results.Url);
        }
    },

    insertRTEImage: function (imageUrl) {

        //Delete any existing content the range contains
        RTE.Cursor.get_range().deleteContent();

        //Get a handle on the current range of the cursor inside the RTE
        var rng = RTE.Cursor.get_range().$3_0;

        //Create a DOM object representing the image and insert
        var d = rng.ownerDocument;
        var a = d.createElement("img");
        a.setAttribute("src", imageUrl);

        SP.UI.UIUtility.insertAfter(a, rng);
    }
}