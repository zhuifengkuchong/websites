/// <reference path="../Cbre/js/jquery-1.6.2-vsdoc.js"/*tpa=http://www.cbre.com/_layouts/Cbre/js/jquery-1.6.2-vsdoc.js*//>
/// <reference path="C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\14\TEMPLATE\LAYOUTS\MicrosoftAjax.js" />
/// <reference path="C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\14\TEMPLATE\LAYOUTS\SP.debug.js" />
/// <reference path="C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\14\TEMPLATE\LAYOUTS\SP.Core.debug.js" />

var urlsplit = /\?|\#/;

String.prototype.format = function () {
	var formatted = this;
	for (var i = 0; i < arguments.length; i++) {
		var regexp = new RegExp('\\{' + i + '\\}', 'gi');
		formatted = formatted.replace(regexp, arguments[i]);
	}
	return formatted;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////   Page Object
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PageWorkflow = {

    IsCheckedIn: null,
    IsInWorkflow: null,
    IsUpdated: false,
    Localizations: [],

    start: function () {
        $(function () {
            PageWorkflow.localize(function () {
                // StartPublishQuery => Do you want to publish this Page?
                if (confirm(PageWorkflow.Localizations['StartPagePublishQuery'])) {
                    ExecuteOrDelayUntilScriptLoaded(PageWorkflow.executeUpdatePagePublishing, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbreworkflow/sp.js*/);
                }
            });
        });
    },

    startUrgent: function () {
        $(function () {
            PageWorkflow.localize(function () {
                ExecuteOrDelayUntilScriptLoaded(PageWorkflow.showUrgentApprovalDialogModal, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbreworkflow/sp.js*/);
            });
        });
    },

    enablePublishPage: function () {
        PageWorkflow.isPageCheckedIn();
        PageWorkflow.isPageInWorkflow();

        return PageWorkflow.IsCheckedIn && (!PageWorkflow.IsInWorkflow);
    },

    isPageCheckedIn: function () {
        if (PageWorkflow.IsCheckedIn == null) {
            PageWorkflow.IsCheckedIn = false;
            $.ajax({
                url: "/_vti_bin/CbreWorkflowService.svc/ispagecheckedin",
                cache: false,
                data: { url: document.location.toString().split(urlsplit)[0] },
                dataType: "json",
                contentType: "application/json",
                success: function (result) {
                    PageWorkflow.IsCheckedIn = result.IsPageCheckedInResult;
                    RefreshCommandUI();
                },
                error: function (err) {
                    alert(err.errorMessage);
                    return false;
                }
            });
        }
        return PageWorkflow.IsCheckedIn;
    },

    isPageInWorkflow: function () {
        if (PageWorkflow.IsInWorkflow == null) {
            PageWorkflow.IsInWorkflow = false;
            $.ajax({
                url: "/_vti_bin/CbreWorkflowService.svc/ispageinworkflow",
                cache: false,
                data: { url: document.location.toString().split(urlsplit)[0] },
                dataType: "json",
                contentType: "application/json",
                success: function (result) {
                    PageWorkflow.IsInWorkflow = result.IsPageInWorkflowResult;
                    PageWorkflow.IsUpdated = true;
                    RefreshCommandUI();
                },
                error: function (err) {
                    alert(err.errorMessage);
                    return false;
                }
            });
        }
        return PageWorkflow.IsInWorkflow;
    },

    localize: function (successfunc) {
        var proxy = new Cbre.Util.ServiceProxy("/_vti_bin/LocalizationService.svc/");
        proxy.invoke(
			"localizations?resources=StartPagePublishQuery,StartWorkflowNotification,PageItemRequestFailed,NoApprovalsAvailable&resourcefile=Cbre.GlobalWeb.Workflow",
			"GET",
			{},
			function (result) {
			    for (var i = 0; i < result.length; i++) {
			        PageWorkflow.Localizations[result[i].Key] = result[i].Value;
			    }
			    successfunc();
			},
			function (result) { logger.debug(result); },
			false
		);
    },

    executeUpdatePagePublishing: function () {
        PageWorkflow.updatePagePublishing(false, '', '');
    },

    updatePagePublishing: function (isUrgent, urgentUser, urgentComment) {
        var pwdialog = WorkflowCommon.showPleaseWaitDialogModal();

        var clientContext = new SP.ClientContext(_spPageContextInfo.siteServerRelativeUrl);
        var web = clientContext.get_web();
        var pubList = web.get_lists().getByTitle('Pageset Approval Workflow Host List');

        var itemCreationInfo = new SP.ListItemCreationInformation();
        var pubListItem = pubList.addItem(itemCreationInfo);

        var url = document.location.toString().split(urlsplit);
        var lastSlashPosition = url[0].lastIndexOf("/");
        var fileUrl = url[0].substring(lastSlashPosition + 1);

        var guid = WorkflowCommon.generateGuid();

        pubListItem.set_item("Title", fileUrl.replace(/\.aspx/i, ""));
        pubListItem.set_item("PageUrl", url[0]);
        pubListItem.set_item("PagesetID", "");
        pubListItem.set_item("WorkflowControlID", guid);
        pubListItem.set_item("Urgent", isUrgent);
        pubListItem.set_item("UrgentApprover", urgentUser);
        pubListItem.set_item("Comment", urgentComment);

        pubListItem.update();
        clientContext.executeQueryAsync(
			function (sender, args) {
			    // StartWorkflowNotification => Starting Publishing Workflow.
			    SP.UI.Notify.addNotification(PageWorkflow.Localizations['StartWorkflowNotification']);
			    PageWorkflow.IsInWorkflow = true;
			    PageWorkflow.IsUpdated = true;

			    setTimeout(
                function () {
                    //RefreshCommandUI();
                    WorkflowCommon.closePleaseWaitDialogModal(pwdialog);
                    window.location.reload();
                },
                15000);
			},
			function (sender, args) {
			    // PagesetItemRequestFailed => Get Page List item request failed {0}\n{1}
			    WorkflowCommon.closePleaseWaitDialogModal(pwdialog);
			    alert(PageWorkflow.Localizations['PageItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
			}
		);
    },

    showUrgentApprovalDialogModal: function () {
        var siteurl = _spPageContextInfo.webServerRelativeUrl;
        if (siteurl.charAt(siteurl.length - 1) != '/') {
            siteurl = siteurl + '/';
        }
        pageurl = siteurl + '_layouts/CbreWorkflow/UrgentApproval.aspx?url=' + window.location;

        var windowWidth = 420;
        var windowHeight = 340;

        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: PageWorkflow.dialogClosedCallback };
        SP.UI.ModalDialog.showModalDialog(options);
    },

    dialogClosedCallback: function (result, arguments) {
        if (result) {
            PageWorkflow.updatePagePublishing(true, arguments.domain + "\\" + arguments.user, arguments.comment);
        }
    },

    approveRejectPageFromPage: function (IsApproval) {
        $(function () {
            PageWorkflow.localize(function () {

                var clientContext = new SP.ClientContext(_spPageContextInfo.siteServerRelativeUrl);
                var web = clientContext.get_web();
                var taskList = web.get_lists().getByTitle('Pageset Approval Workflow Task List');
                clientContext.load(taskList);

                var camlQuery = new SP.CamlQuery();
                camlQuery.set_viewXml("<View><Query><Where><And><Eq><FieldRef Name='PageUrl'/><Value Type='Text'>" +
							  document.location.toString().split(urlsplit)[0] +
							  "</Value></Eq><Neq><FieldRef Name='Status'/><Value Type='Choice'>Completed</Value></Neq></And></Where></Query></View>");

                var taskListItems = taskList.getItems(camlQuery);
                clientContext.load(taskListItems);

                clientContext.executeQueryAsync(
			        function (sender, args) {
			            if (taskListItems.get_count() > 0) {

			                var listEnumerator = taskListItems.getEnumerator();

			                listEnumerator.moveNext();
			                var item = listEnumerator.get_current();

			                siteurl = _spPageContextInfo.siteServerRelativeUrl;
			                if (siteurl.charAt(siteurl.length - 1) != '/') {
			                    siteurl = siteurl + '/';
			                }

			                if (IsApproval)
			                    pageurl = siteurl + 'PagesetApprovalWorkflowForms/PagesetApprovalTask1Form.aspx?List=' + taskList.get_id() + '&ID=' + item.get_id() + '&Process=Approve';
			                else
			                    pageurl = siteurl + 'PagesetApprovalWorkflowForms/PagesetApprovalTask1Form.aspx?List=' + taskList.get_id() + '&ID=' + item.get_id() + '&Process=Reject';

			                var windowWidth = 600;
			                var windowHeight = 310;
			                if (FV4UI()) {
			                    var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: PagesetWorkflow.dialogClosedApprovalCallback };
			                    var dialog = SP.UI.ModalDialog.showModalDialog(options);
			                }
			                else {
			                    var features;
			                    if (window.showModalDialog) {
			                        features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
			                    }
			                    else {
			                        features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
			                    }
			                    var result = commonShowModalDialog(pageurl, features, PageWorkflow.dialogClosedApprovalCallback);
			                }
			            }
			            else {
			                // NoApprovalsAvailable => No approvals to be done
			                alert(PageWorkflow.Localizations['NoApprovalsAvailable']);
			            }
			        },
			        function (sender, args) {
			            // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
			            alert(PageWorkflow.Localizations['PageItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
			        }
		        );
            });
        });
    },

    dialogClosedApprovalCallback: function (dialogResult, results) {
        if (dialogResult == SP.UI.DialogResult.OK) {
            window.location.reload();
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////   Pageset Object
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PagesetWorkflow = {

    InEditPubMode: null,
    InWorkflowMode: null,
    IsDirtyFlag: null,
    IsUpdated: false,
    Localizations: [],

    start: function () {
        $(function () {
            PagesetWorkflow.localize(function () {
                // StartPublishQuery => Do you want to publish this Pageset?
                if (confirm(PagesetWorkflow.Localizations['StartPublishQuery'])) {
                    ExecuteOrDelayUntilScriptLoaded(PagesetWorkflow.executeUpdatePagesetPublishing, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbreworkflow/sp.js*/);
                }
            });
        });
    },

    startUrgent: function () {
        $(function () {
            PagesetWorkflow.localize(function () {
                ExecuteOrDelayUntilScriptLoaded(PagesetWorkflow.showUrgentApprovalDialogModal, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbreworkflow/sp.js*/);
            });
        });
    },

    isPagesetSaved: function () {

        if (PagesetWorkflow.InEditPubMode == null) {
            PagesetWorkflow.InEditPubMode = true;
            PagesetWorkflow.localize(function () {
                ExecuteOrDelayUntilScriptLoaded(PagesetWorkflow.findPagesetCookie, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbreworkflow/sp.js*/);
            });
        }
        if (PagesetWorkflow.IsDirtyFlag == null) {
            PagesetWorkflow.IsDirtyFlag = false;
            PagesetWorkflow.localize(function () {
                ExecuteOrDelayUntilScriptLoaded(PagesetWorkflow.isDirty, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbreworkflow/sp.js*/);
            });
        }

        return !PagesetWorkflow.InEditPubMode && PagesetWorkflow.IsDirtyFlag;
    },

    isDirty: function () {
        var url = document.location.toString().split(urlsplit);
        var lastSlashPosition = url[0].toLowerCase().indexOf("/pages/");
        var fileUrl = _spPageContextInfo.webServerRelativeUrl + url[0].substring(lastSlashPosition);
        fileUrl = fileUrl.replace("//", "/");

        var clientContext = new SP.ClientContext.get_current();
        var web = clientContext.get_web();
        var file = web.getFileByServerRelativeUrl(fileUrl);

        clientContext.load(file);

        clientContext.executeQueryAsync(
			function (sender, args) {
			    if (file.get_minorVersion() > 0)
			        PagesetWorkflow.IsDirtyFlag = true;
			    else
			        PagesetWorkflow.IsDirtyFlag = false;

			    RefreshCommandUI();
			},
			function (sender, args) {
			}
		);
    },

    isInWorkflow: function () {

        if (PagesetWorkflow.InWorkflowMode == null) {
            PagesetWorkflow.InWorkflowMode = false;
            PagesetWorkflow.localize(function () {
                ExecuteOrDelayUntilScriptLoaded(PagesetWorkflow.findWorkflow, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbreworkflow/sp.js*/);
            });
        }
        return PagesetWorkflow.InWorkflowMode;
    },

    localize: function (successfunc) {
        var proxy = new Cbre.Util.ServiceProxy("/_vti_bin/LocalizationService.svc/");
        proxy.invoke(
			"localizations?resources=StartPublishQuery,StartWorkflowNotification,PagesetItemRequestFailed,ConfirmSelectedItemProcess,NoApprovalsAvailable&resourcefile=Cbre.GlobalWeb.Workflow",
			"GET",
			{},
			function (result) {
			    for (var i = 0; i < result.length; i++) {
			        PagesetWorkflow.Localizations[result[i].Key] = result[i].Value;
			    }
			    successfunc();
			},
			function (result) { logger.debug(result); },
			false
		);
    },


    executeUpdatePagesetPublishing: function () {
        PagesetWorkflow.updatePagesetPublishing(false, '', '');
    },

    updatePagesetPublishing: function (isUrgent, urgentUser, urgentComment) {
        var pwdialog = WorkflowCommon.showPleaseWaitDialogModal();

        var clientContext = new SP.ClientContext.get_current();
        var web = clientContext.get_web();
        var pagesList = web.get_lists().getByTitle("Pages");

        var url = document.location.toString().split(urlsplit);
        var lastSlashPosition = url[0].lastIndexOf("/");
        var fileUrl = url[0].substring(lastSlashPosition + 1);

        var proxy = new Cbre.Util.ServiceProxy("/_vti_bin/CbreWorkflowService.svc/");
        proxy.invoke(
			"isalreadyinapproval?url=" + url,
			"GET",
			{},
			function (data) {
			    if (data.length == 0) {
			        var clientContext = new SP.ClientContext.get_current();
			        var web = clientContext.get_web();
			        var pagesList = web.get_lists().getByTitle("Pages");

			        var url = document.location.toString().split(urlsplit);
			        var lastSlashPosition = url[0].lastIndexOf("/");
			        var fileUrl = url[0].substring(lastSlashPosition + 1);

			        var camlQuery = new SP.CamlQuery();
			        camlQuery.set_viewXml("<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>");

			        var listItems = pagesList.getItems(camlQuery);
			        clientContext.load(listItems, 'Include(PagesetID,PagesetName)');

			        clientContext.executeQueryAsync(
			            function (sender, args) {
			                var listEnumerator = listItems.getEnumerator();

			                listEnumerator.moveNext();
			                var item = listEnumerator.get_current();

			                var clientContext2 = new SP.ClientContext(_spPageContextInfo.siteServerRelativeUrl);
			                var web2 = clientContext2.get_web();
			                var pubList = web2.get_lists().getByTitle('Pageset Approval Workflow Host List');

			                var itemCreationInfo = new SP.ListItemCreationInformation();
			                var pubListItem = pubList.addItem(itemCreationInfo);

			                var guid = WorkflowCommon.generateGuid();

			                pubListItem.set_item("Title", item.get_item("PagesetName").toString());
			                pubListItem.set_item("PagesetID", item.get_item("PagesetID").toString());
			                pubListItem.set_item("WorkflowControlID", guid);
			                pubListItem.set_item("Urgent", isUrgent);
			                pubListItem.set_item("UrgentApprover", urgentUser);
			                pubListItem.set_item("Comment", urgentComment);

			                pubListItem.update();
			                clientContext2.executeQueryAsync(
					            function (sender, args) {
					                // StartWorkflowNotification => Starting Publishing Workflow.
					                SP.UI.Notify.addNotification(PagesetWorkflow.Localizations['StartWorkflowNotification']);
					                PagesetWorkflow.InEditPubMode = true;
					                PagesetWorkflow.InWorkflowMode = true;
					                PagesetWorkflow.IsUpdated = true;

					                setTimeout(
                                    function () {
                                        //RefreshCommandUI();
                                        WorkflowCommon.closePleaseWaitDialogModal(pwdialog);
                                        window.location.reload();
                                    },
                                    15000);
					            },
					            function (sender, args) {
					                // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
					                WorkflowCommon.closePleaseWaitDialogModal(pwdialog);
					                alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
					            }
				            );
			            },
			            function (sender, args) {
			                // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
			                WorkflowCommon.closePleaseWaitDialogModal(pwdialog);
			                alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
			            }
		            );
			    }
			    else {
			        WorkflowCommon.closePleaseWaitDialogModal(pwdialog);
			        var text = "The following pages are currently pending approval:\n\n";
			        for (var i = 0; i < data.length; i++) {
			            text += data[i] + "\n";
			        }
			        text += "\nThe workflow cannot proceed. Please contact administrator to cancel above workflow(s).\n";
			        alert(text);
			    }
			},
            function (sender, args) {
                // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
                WorkflowCommon.closePleaseWaitDialogModal(pwdialog);
                alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
            }
        );
    },

    showUrgentApprovalDialogModal: function () {
        var siteurl = _spPageContextInfo.webServerRelativeUrl;
        if (siteurl.charAt(siteurl.length - 1) != '/') {
            siteurl = siteurl + '/';
        }
        pageurl = siteurl + '_layouts/CbreWorkflow/UrgentApproval.aspx?url=' + window.location;

        var windowWidth = 420;
        var windowHeight = 340;
        if (FV4UI()) {
            var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: PagesetWorkflow.dialogClosedCallback };
            var dialog = SP.UI.ModalDialog.showModalDialog(options);
        }
        else {
            var features;
            if (window.showModalDialog) {
                features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
            }
            else {
                features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
            }
            var result = commonShowModalDialog(pageurl, features, PagesetWorkflow.dialogClosedCallback);
        }
    },

    dialogClosedCallback: function (result, arguments) {
        if (result) {
            PagesetWorkflow.updatePagesetPublishing(true, arguments.domain + "\\" + arguments.user, arguments.comment);
        }
    },

    findPagesetCookie: function () {
        var clientContext = new SP.ClientContext.get_current();
        var web = clientContext.get_web();
        var list = web.get_lists().getByTitle("Pages");

        var url = document.location.toString().split(urlsplit);
        var lastSlashPosition = url[0].lastIndexOf("/");
        var fileUrl = url[0].substring(lastSlashPosition + 1);

        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>");

        var pageListItems = list.getItems(camlQuery);
        clientContext.load(pageListItems, 'Include(PagesetID)');

        clientContext.executeQueryAsync(
			function (sender, args) {
			    var listEnumerator = pageListItems.getEnumerator();

			    listEnumerator.moveNext();
			    var item = listEnumerator.get_current();
			    var id = item.get_item("PagesetID").toString();

			    if (PagesetWorkflow.readCookie('PagesetInEditMode_' + id) == 'true') {
			        PagesetWorkflow.InEditPubMode = true;
			        RefreshCommandUI();
			    }
			    else {
			        var clientContext2 = new SP.ClientContext(_spPageContextInfo.siteServerRelativeUrl);
			        var web2 = clientContext2.get_web();
			        var pubList = web2.get_lists().getByTitle('Pageset Approval Workflow Task List');

			        var camlQuery = new SP.CamlQuery();
			        camlQuery.set_viewXml("<View><Query><Where><Eq><FieldRef Name='PagesetID'/><Value Type='Text'>" + id + "</Value></Eq></Where></Query></View>");

			        var pubListItems = pubList.getItems(camlQuery);
			        clientContext2.load(pubListItems, 'Include(PagesetID,PercentComplete)');

			        clientContext2.executeQueryAsync(
						function (sender, args) {
						    var pubListEnumerator = pubListItems.getEnumerator();
						    PagesetWorkflow.InEditPubMode = false;

						    while (pubListEnumerator.moveNext()) {
						        var item = pubListEnumerator.get_current();
						        if (item.get_item("PercentComplete") < 1.0) {    // Error, Canceled or Complete
						            PagesetWorkflow.InEditPubMode = true;
						        }
						    }
						    RefreshCommandUI();
						},
						function (sender, args) {
						    // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
						    alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
						}
					);
			    }
			},
			function (sender, args) {
			    // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
			    alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
			}
		);
    },

    readCookie: function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    },

    approveRejectPagesetFromPage: function (IsApproval) {
        $(function () {
            PagesetWorkflow.localize(function () {
                var clientContext = new SP.ClientContext.get_current();
                var web = clientContext.get_web();
                var pagesList = web.get_lists().getByTitle("Pages");

                var url = document.location.toString().split(urlsplit);
                var lastSlashPosition = url[0].lastIndexOf("/");
                var fileUrl = url[0].substring(lastSlashPosition + 1);

                var camlQuery = new SP.CamlQuery();
                camlQuery.set_viewXml("<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>");

                var pagesListItems = pagesList.getItems(camlQuery);
                clientContext.load(pagesListItems, 'Include(PagesetID)');

                clientContext.executeQueryAsync(
			        function (sender, args) {
			            var listEnumerator = pagesListItems.getEnumerator();

			            listEnumerator.moveNext();
			            var item = listEnumerator.get_current();
			            var id = item.get_item("PagesetID").toString();

			            var clientContext2 = new SP.ClientContext(_spPageContextInfo.siteServerRelativeUrl);
			            var web2 = clientContext2.get_web();
			            var taskList = web2.get_lists().getByTitle('Pageset Approval Workflow Task List');
			            clientContext2.load(taskList);

			            var camlQuery2 = new SP.CamlQuery();
			            camlQuery2.set_viewXml("<View><Query><Where><And><Eq><FieldRef Name='PagesetID'/><Value Type='Text'>" + id + "</Value></Eq><Neq><FieldRef Name='Status'/><Value Type='Choice'>Completed</Value></Neq></And></Where></Query></View>");

			            var taskListItems = taskList.getItems(camlQuery2);
			            clientContext2.load(taskListItems);

			            clientContext2.executeQueryAsync(
					        function (sender, args) {
					            if (taskListItems.get_count() > 0) {

					                var listEnumerator = taskListItems.getEnumerator();

					                listEnumerator.moveNext();
					                var item = listEnumerator.get_current();

					                siteurl = _spPageContextInfo.siteServerRelativeUrl;
					                if (siteurl.charAt(siteurl.length - 1) != '/') {
					                    siteurl = siteurl + '/';
					                }

					                if (IsApproval)
					                    pageurl = siteurl + 'PagesetApprovalWorkflowForms/PagesetApprovalTask1Form.aspx?List=' + taskList.get_id() + '&ID=' + item.get_id() + '&Process=Approve';
					                else
					                    pageurl = siteurl + 'PagesetApprovalWorkflowForms/PagesetApprovalTask1Form.aspx?List=' + taskList.get_id() + '&ID=' + item.get_id() + '&Process=Reject';

					                var windowWidth = 600;
					                var windowHeight = 310;
					                if (FV4UI()) {
					                    var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: PagesetWorkflow.dialogClosedApprovalCallback };
					                    var dialog = SP.UI.ModalDialog.showModalDialog(options);
					                }
					                else {
					                    var features;
					                    if (window.showModalDialog) {
					                        features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
					                    }
					                    else {
					                        features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
					                    }
					                    var result = commonShowModalDialog(pageurl, features, PagesetWorkflow.dialogClosedApprovalCallback);
					                }
					            }
					            else {
					                // NoApprovalsAvailable => No approvals to be done
					                alert(PagesetWorkflow.Localizations['NoApprovalsAvailable']);
					            }
					        },
					        function (sender, args) {
					            // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
					            alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
					        }
				        );
			        },
			        function (sender, args) {
			            // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
			            alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
			        }
		        );
            });
        });
    },

    approveRejectPagesetFromList: function (ListID, ItemID, Process) {
        siteurl = _spPageContextInfo.siteServerRelativeUrl;
        if (siteurl.charAt(siteurl.length - 1) != '/') {
            siteurl = siteurl + '/';
        }

        pageurl = siteurl + 'PagesetApprovalWorkflowForms/PagesetApprovalTask1Form.aspx?List=' + ListID + '&ID=' + ItemID + '&Process=' + Process;

        var windowWidth = 600;
        var windowHeight = 310;
        if (FV4UI()) {
            var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: PagesetWorkflow.dialogClosedApprovalCallback };
            var dialog = SP.UI.ModalDialog.showModalDialog(options);
        }
        else {
            var features;
            if (window.showModalDialog) {
                features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
            }
            else {
                features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
            }
            var result = commonShowModalDialog(pageurl, features, PagesetWorkflow.dialogClosedApprovalCallback);
        }
    },

    dialogClosedApprovalCallback: function (dialogResult, results) {
        if (dialogResult == SP.UI.DialogResult.OK) {
            window.location.reload();
        }
    },

    findWorkflow: function () {
        var clientContext = new SP.ClientContext.get_current();
        var web = clientContext.get_web();
        var pagesList = web.get_lists().getByTitle("Pages");

        var url = document.location.toString().split(urlsplit);
        var lastSlashPosition = url[0].lastIndexOf("/");
        var fileUrl = url[0].substring(lastSlashPosition + 1);

        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>");

        var pagesListItems = pagesList.getItems(camlQuery);
        clientContext.load(pagesListItems, 'Include(PagesetID)');

        clientContext.executeQueryAsync(
			function (sender, args) {
			    var listEnumerator = pagesListItems.getEnumerator();

			    listEnumerator.moveNext();
			    var item = listEnumerator.get_current();
			    var id = item.get_item("PagesetID").toString();

			    var clientContext2 = new SP.ClientContext(_spPageContextInfo.siteServerRelativeUrl);
			    var web2 = clientContext2.get_web();
			    var taskList = web2.get_lists().getByTitle('Pageset Approval Workflow Task List');
			    clientContext2.load(taskList);

			    var camlQuery2 = new SP.CamlQuery();
			    camlQuery2.set_viewXml("<View><Query><Where><And><Eq><FieldRef Name='PagesetID'/><Value Type='Text'>" + id + "</Value></Eq><And><Neq><FieldRef Name='Status'/><Value Type='Choice'>Completed</Value></Neq><Neq><FieldRef Name='Status'/><Value Type='Choice'>Tasks_Completed</Value></Neq></And></And></Where></Query></View>");

			    var taskListItems = taskList.getItems(camlQuery2);
			    clientContext2.load(taskListItems);

			    clientContext2.executeQueryAsync(
					function (sender, args) {
					    if (taskListItems.get_count() > 0) {
					        PagesetWorkflow.InWorkflowMode = true;
					    }
					    else {
					        PagesetWorkflow.InWorkflowMode = false;
					    }
					    PagesetWorkflow.IsUpdated = true;
					    RefreshCommandUI();  // Cause Publish Button to disable                        }
					},
					function (sender, args) {
					    // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
					    alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
					}
				);
			},
			function (sender, args) {
			    // PagesetItemRequestFailed => Get Pageset List item request failed {0}\n{1}
			    alert(PagesetWorkflow.Localizations['PagesetItemRequestFailed'].format(args.get_message(), args.get_stackTrace()));
			}
		);
    },

    cancelWorkflow: function (func) {
        var clientContext = new SP.ClientContext.get_current();
        var web = clientContext.get_web();
        var pagesList = web.get_lists().getByTitle("Pages");

        var url = document.location.toString().split(urlsplit);
        var lastSlashPosition = url[0].lastIndexOf("/");
        var fileUrl = url[0].substring(lastSlashPosition + 1);

        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml("<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>");

        var pagesListItems = pagesList.getItems(camlQuery);
        clientContext.load(pagesListItems, 'Include(PagesetID)');

        clientContext.executeQueryAsync(
			function (sender, args) {
			    var listEnumerator = pagesListItems.getEnumerator();

			    listEnumerator.moveNext();
			    var item = listEnumerator.get_current();
			    var id = item.get_item("PagesetID").toString();

			    var clientContext2 = new SP.ClientContext(_spPageContextInfo.siteServerRelativeUrl);
			    var web2 = clientContext2.get_web();
			    var taskList = web2.get_lists().getByTitle('Pageset Approval Workflow Task List');
			    clientContext2.load(taskList);

			    var camlQuery2 = new SP.CamlQuery();
			    camlQuery2.set_viewXml("<View><Query><Where><And><Eq><FieldRef Name='PagesetID'/><Value Type='Text'>" + id + "</Value></Eq><Neq><FieldRef Name='Status'/><Value Type='Choice'>Completed</Value></Neq></And></Where></Query></View>");

			    var taskListItems = taskList.getItems(camlQuery2);
			    clientContext2.load(taskListItems);

			    clientContext2.executeQueryAsync(
					function (sender, args) {
					    var listEnumerator = taskListItems.getEnumerator();

					    listEnumerator.moveNext();
					    var taskItem = listEnumerator.get_current();

					    taskItem.set_item("Completed", true);
					    taskItem.set_item("Status", "Completed");
					    taskItem.set_item("ApprovalStatus", "Cancel Workflow");
					    taskItem.set_item("EditorComments", "Workflow canceled by Author");
					    taskItem.set_item("PercentComplete", 1);

					    taskItem.update();
					    clientContext2.executeQueryAsync(
							function (sender, args) {
							    SP.UI.Notify.addNotification("Workflow was cancelled");

							    if (func != undefined)
							        func();
							},
							function (sender, args) {
							    alert("Update List item request failed " + args.get_message() + " --- " + args.get_stackTrace());
							}
						);
					},
					function (sender, args) {
					    alert("Get Task List item request failed " + args.get_message() + " --- " + args.get_stackTrace());
					}
				);
			},
			function (sender, args) {
			    alert("Get Pageset List item request failed " + args.get_message() + " --- " + args.get_stackTrace());
			}
		);
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////   ApprovalAssistant Object
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ApprovalAssistant = {
    approveRejectPageset: function (ListID, ItemID, Process) {
        ApprovalAssistant.updateApprovalStatus(ListID, ItemID, Process);
        var proxy = new Cbre.Util.ServiceProxy("/_vti_bin/CbreWorkflowService.svc/");
        proxy.invoke(
			"buildpagesetapprovaltask1formcall?listid=" + ListID + "&itemid=" + ItemID + "&process=" + Process,
			"GET",
			{},
			function (pageurl) {
			    var windowWidth = 600;
			    var windowHeight = 310;
			    if (FV4UI()) {
			        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: PagesetWorkflow.dialogClosedApprovalCallback };
			        var dialog = SP.UI.ModalDialog.showModalDialog(options);
			    }
			    else {
			        var features;
			        if (window.showModalDialog) {
			            features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
			        }
			        else {
			            features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
			        }
			        var result = commonShowModalDialog(pageurl, features, PagesetWorkflow.dialogClosedApprovalCallback);
			    }
			},
			function (result) { logger.debug(result); },
			false
		);
    },

    approveRejectPagesetList: function (Process) {
        $(function () {
            PagesetWorkflow.localize(function () {
                // ConfirmSelectedItemProcess => Do you want to {0} the selected list item(s)?
                if (confirm(PagesetWorkflow.Localizations['ConfirmSelectedItemProcess'].format(Process))) {
                    var listId = SP.ListOperation.Selection.getSelectedList();
                    var items = SP.ListOperation.Selection.getSelectedItems();

                    var itemIds = '';
                    var itemIndex;
                    var first = true;
                    for (itemIndex in items) {
                        if (first) {
                            itemIds = items[itemIndex].id;
                            first = false;
                        }
                        else {
                            itemIds += ',' + items[itemIndex].id;
                        }
                        ApprovalAssistant.updateApprovalStatus(listId, items[itemIndex].id, Process);
                    }

                    var proxy = new Cbre.Util.ServiceProxy("/_vti_bin/CbreWorkflowService.svc/");
                    proxy.invoke(
						"approverejectpagesetlist?listid=" + listId + "&itemids=" + itemIds + "&process=" + Process,
						"GET",
						{},
						function (result) {
						    window.location.reload();
						},
						function (result) { logger.debug(result); },
						false
					);
                }
            });
        });
    },

    updateApprovalStatus: function (ListID, ItemID, Process) {
        var clientContext = new SP.ClientContext.get_current();
        var web = clientContext.get_web();
        var aaList = web.get_lists().getById(ListID);
        var listItem = aaList.getItemById(ItemID);
        clientContext.load(listItem);
        clientContext.executeQueryAsync(
			function (sender, args) {
			    listItem.set_item("PercentComplete", 1);
			    listItem.update();
			    clientContext.executeQueryAsync(
					function (sender, args) {
					},
					function (sender, args) {
					    alert("Update List item request failed " + args.get_message() + " --- " + args.get_stackTrace());
					}
				);
			},
			function (sender, args) {
			    alert("Get List item request failed " + args.get_message() + " --- " + args.get_stackTrace());
			}
        );
    },

    checkIsEnabled: function () {
        var selectedItems = SP.ListOperation.Selection.getSelectedItems();
        var count = CountDictionary(selectedItems);
        return (count > 0);
    }
}

WorkflowCommon = {

    generateGuid: function () {
        var result, i, j;
        result = '';
        for (j = 0; j < 32; j++) {
            if (j == 8 || j == 12 || j == 16 || j == 20)
                result = result + '-';
            i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
            result = result + i;
        }
        return result;
    },

    showPleaseWaitDialogModal: function () {
        var siteurl = _spPageContextInfo.webServerRelativeUrl;
        if (siteurl.charAt(siteurl.length - 1) != '/') {
            siteurl = siteurl + '/';
        }
        pageurl = siteurl + '_layouts/CbreWorkflow/PleaseWaitWorkflowProcessing.aspx';

        var windowWidth = 320;
        var windowHeight = 80;

        var options = { width: windowWidth, height: windowHeight, resizable: false, status: false, menubar: false, help: false, showClose: false, url: pageurl };
        return SP.UI.ModalDialog.showModalDialog(options);
    },

    closePleaseWaitDialogModal: function (dialog) {
        dialog.close();
    },

    getWorkflowTaskID: function (cntlid, successfunc, failfunc) {
        var proxy = new Cbre.Util.ServiceProxy("/_vti_bin/CbreWorkflowService.svc/");
        proxy.invoke(
            "getworkflowtaskid?cntlid=" + cntlid,
			"GET",
			{},
			function (result) {
			    if (result >= 0)
			        successfunc(result);
			    else
			        failfunc(result);
			},
			function (result) { failfunc(result); },
			false
		);
    }
}