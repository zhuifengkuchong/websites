/// <reference name="Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbrepageset/MicrosoftAjax.js*/ />
/// <reference path="file://C:/Program Files/Common Files/Microsoft Shared/Web Server Extensions/14/TEMPLATE/LAYOUTS/SP.core.debug.js" /> 
/// <reference path="file://C:/Program Files/Common Files/Microsoft Shared/Web Server Extensions/14/TEMPLATE/LAYOUTS/SP.debug.js" />

ExecuteOrDelayUntilScriptLoaded(startPageStateSetup, 'Unknown_83_filename'/*tpa=http://www.cbre.com/_layouts/cbrepageset/sp.js*/);

var urlsplit = /\?|\#/;

function startPageStateSetup() {

    var PagesetState = getPagesetState(); removePagesetState();

    if (PagesetState != null) {

        $(document).ready(function () {

            var process = true;
            $('span:[role]').each(function () {
                if ($(this).attr('role') == 'alert') {
                    var statusId = SP.UI.Status.addStatus('<b>Please resolve error(s) before continuing.</b>');
                    SP.UI.Status.setStatusPriColor(statusId, 'red');
                    process = false;
                }
            });

            if (process) {
                switch (PagesetState) {
                    case 'PageStateManagePageset':
                        executeManagePagesetDialogModal();
                        break;
                    case 'PageStateSavePageset':
                        executePagesetSaveCommand();
                        break;
                    case 'PageStateAddPageset':
                        executeAddPagesetPageDialogModal();
                        break;
                    case 'PageStateEditPagesetProperties':
                        executeEditPagesetPropertiesDialogModal();
                        break;
                    default:
                        break;
                }
            }
        });
    }
    else {

        // Edit Web Part button disabled when pageset isn't in edit mode.
        if (window.jQuery) {
            $(document).ready(function () {
                var item = "#MSOMenu_Edit";
                if ($(item).length > 0) {
                    $(item).attr("Enabled", "false");
                    $(item).attr("Title", "The pageset must be in edit mode.");
                    if (!isPageNotInEditMode()) {
                        $(item).attr("Enabled", "true");
                        $(item).attr("Title", "Change properties of this shared Web Part. These changes will apply to all users.");
                    }
                }
            });
        }
    }
}


function dialogClosedCallback(result, arguments) {
    if (FV4UI()) {
        if (result) {
            if (arguments) {
                SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
                window.location = arguments;
            }
            else {
                SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
                window.location.reload();
            }
        }
    }
    else {
        if (result) {
            SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
            window.location = result;
        }
        else {
            SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
            window.location.reload();
        }
    }
}

function dialogClosedRefreshCallback(result, arguments) {
    if (FV4UI()) {
        if (result) {
            if (arguments != 'cancel') {
                SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
                window.location.reload();
            }
        }
    }
    else {
        if (result != 'cancel') {
            SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
            window.location.reload();
        }
    }    
}

var _IsPagesetPage = null;   // Set Asynchronously and when successfully completed this var is reset and the EnableScript is recalled via RefreshCommandUI()
var _IsAnAuthor = null;

function isPagesetPage() {
    if (_IsPagesetPage == null) {
        _IsPagesetPage = false;
        ExecuteOrDelayUntilScriptLoaded(InitializePagesetListItem, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbrepageset/sp.js*/);
    }

    if (PagesetWorkflow.InWorkflowMode == null)
        PagesetWorkflow.isInWorkflow();

    return (_IsPagesetPage && !PagesetWorkflow.InWorkflowMode);
}

function isPagesetPageAndNotAuthor() {
    if (_IsAnAuthor == null) {
        var proxy = new Cbre.Util.ServiceProxy("/_vti_bin/CbrePageset/PagesetRest.svc/");
        proxy.invoke(
            "isanauthor?url=" + document.location.toString().split(urlsplit)[0],
            "GET",
            {},
            function (result) {
                _IsAnAuthor = result;
            },
            function (result) {
                logger.debug(result);
                _IsAnAuthor = false;
            },
            false
        );
    }
    isPagesetPage();

    return _IsPagesetPage && !_IsAnAuthor;
}

function InitializePagesetListItem() {
    var clientContext = new SP.ClientContext.get_current();
    var web = clientContext.get_web();
    var list = web.get_lists().getByTitle("Pages");

    var url = document.location.toString().split(urlsplit);
    var lastSlashPosition = url[0].lastIndexOf("/");
    var fileUrl = url[0].substring(lastSlashPosition + 1);

    var camlQuery = new SP.CamlQuery();
    var q = "<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>";
    camlQuery.set_viewXml(q);

    this.listItemsPS = list.getItems(camlQuery);
    clientContext.load(listItemsPS, 'Include(ContentTypeId)');
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onListItemsLoadSuccess), Function.createDelegate(this, this.onQueryFailed));

}

function onListItemsLoadSuccess(sender, args) {
    var listEnumerator = this.listItemsPS.getEnumerator();
    //should only be one item
    listEnumerator.moveNext();
    var item = listEnumerator.get_current();
    var id = item.get_item("ContentTypeId").toString();

    if (id.toUpperCase().startsWith("0X010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF390046653FC1B25A45DE842CCA3D9C09CAC300AA357E31E3C949888513987A0BCA1650")) {
        _IsPagesetPage = true;
    }
    else {
        _IsPagesetPage = false;
    }
    RefreshCommandUI();
}




function showAddPagesetPageDialogModal() {
    if (_PagesetInEditMode == true) {
        setPagesetState('PageStateAddPageset');
        ExecuteCommand('PageStateGroupSave');
    }
    else {
        executeAddPagesetPageDialogModal();
    }
}
function executeAddPagesetPageDialogModal() {
    siteurl = _spPageContextInfo.webServerRelativeUrl;
    if (siteurl.charAt(siteurl.length - 1) != '/') {
        siteurl = siteurl + '/';
    }
    pageurl = siteurl + '_layouts/CbrePageset/AddPagesetPage.aspx?url=' + window.location;

    var windowWidth = 590;
    var windowHeight = 680;
    if (FV4UI()) {
        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: dialogClosedCallback };
        var dialog = SP.UI.ModalDialog.showModalDialog(options);
    }
    else {
        var features;
        if (window.showModalDialog) {
            features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
        }
        else {
            features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
        }
        var result = commonShowModalDialog(pageurl, features, dialogClosedCallback);
    }
}

function showManagePagesetDialogModal() {
    if (_PagesetInEditMode == true) {
        setPagesetState('PageStateManagePageset');
        ExecuteCommand('PageStateGroupSave');
    }
    else {
        executeManagePagesetDialogModal();
    }
}
function executeManagePagesetDialogModal() {
    siteurl = _spPageContextInfo.webServerRelativeUrl;
    if (siteurl.charAt(siteurl.length - 1) != '/') {
        siteurl = siteurl + '/';
    }
    pageurl = siteurl + '_layouts/CbrePageset/ManagePageset.aspx?url=' + window.location;

    var windowWidth = 540;
    var windowHeight = 400;
    if (FV4UI()) {
        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: dialogClosedRefreshCallback };
        var dialog = SP.UI.ModalDialog.showModalDialog(options);
    }
    else {
        var features;
        if (window.showModalDialog) {
            features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
        }
        else {
            features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
        }
        var result = commonShowModalDialog(pageurl, features, dialogClosedRefreshCallback);
    }
}
function showDeletePagesetDialogModal() {
    siteurl = _spPageContextInfo.webServerRelativeUrl;
    if (siteurl.charAt(siteurl.length - 1) != '/') {
        siteurl = siteurl + '/';
    }
    pageurl = siteurl + '_layouts/CbrePageset/DeletePageset.aspx?url=' + window.location;

    var windowWidth = 440;
    var windowHeight = 205;
    if (FV4UI()) {
        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: dialogClosedCallback };
        var dialog = SP.UI.ModalDialog.showModalDialog(options);
    }
    else {
        var features;
        if (window.showModalDialog) {
            features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
        }
        else {
            features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
        }
        var result = commonShowModalDialog(pageurl, features, dialogClosedCallback);
    }
}

function editPagesetCommand() {
    ExecuteOrDelayUntilScriptLoaded(setEditModePageset, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbrepageset/sp.js*/);
}

function setEditModePageset() {
    if (_PagesetInfoRefreshed) {
        if (typeof (PagesetWorkflow) != 'undefined') {    
            if (!PagesetWorkflow.IsUpdated) return;
            if (PagesetWorkflow.InWorkflowMode) {
                if (confirm("An Approval process has been started for this Pageset.\nEditing of this Pageset required the stopping of this process.\n\nDo you want to stop the approval process?")) {
                    PagesetWorkflow.cancelWorkflow(CreatePagesetCookie);
                }
            } else {
                CreatePagesetCookie();
            }
        }
    }
}

function CreatePagesetCookie() {
    var clientContext = new SP.ClientContext.get_current();
    var web = clientContext.get_web();
    var list = web.get_lists().getByTitle("Pages");

    var url = document.location.toString().split(urlsplit);
    var lastSlashPosition = url[0].lastIndexOf("/");
    var fileUrl = url[0].substring(lastSlashPosition + 1);

    var camlQuery = new SP.CamlQuery();
    var q = "<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>";
    camlQuery.set_viewXml(q);
    this.listItemsCreateCookie = list.getItems(camlQuery);
    clientContext.load(listItemsCreateCookie, 'Include(PagesetID)');
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onEditPagesetCommandSuccess), Function.createDelegate(this, this.onQueryFailed));
}
function onEditPagesetCommandSuccess(sender, args) {
    var listEnumerator = this.listItemsCreateCookie.getEnumerator();
    //should only be one item
    listEnumerator.moveNext();
    var item = listEnumerator.get_current();
    var id = item.get_item("PagesetID").toString();

    CheckoutPageset(id);

    createCookie('PagesetInEditMode_' + id, 'true', 1);
}

function CheckoutPageset(pagesetId) {
    clientContext = new SP.ClientContext.get_current();
    web = clientContext.get_web();
    var list = web.get_lists().getByTitle("Pages");
    var camlQuery = new SP.CamlQuery();
    var q = "<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='PagesetID'/><Value Type='Text'>" + pagesetId + "</Value></Eq></Where></Query></View>";
    camlQuery.set_viewXml(q);
    this.listItemsCheckout = list.getItems(camlQuery);
    clientContext.load(listItemsCheckout, 'Include(CheckoutUser)');
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onListItemsCheckOutSuccess), Function.createDelegate(this, this.onQueryFailed));
}
function onListItemsCheckOutSuccess(sender, args) {
    var listEnumerator = this.listItemsCheckout.getEnumerator();
    //iterate though all of the items
    while (listEnumerator.moveNext()) {
        var item = listEnumerator.get_current();
        if (item.get_item("CheckoutUser") == null) {
            var file = item.get_file();
            file.checkOut();
        }
    }
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onUpdateListItemCheckOutSuccess), Function.createDelegate(this, this.onQueryFailed));
}
function onUpdateListItemCheckOutSuccess(sender, args) {
    SP.UI.Notify.addNotification('Pageset Checked Out.');
    window.location.reload();   
}

var _PagesetInEditMode = null;   // Set Asynchronously and when successfully completed this var is reset and the EnableScript is recalled via RefreshCommandUI()
var _PagesetEditPermission = null;
var _PagesetInfoRefreshed = false;

function editPagesetNoCookieSet() {
    if (_PagesetEditPermission == null) {
        var proxy = new Cbre.Util.ServiceProxy("/_vti_bin/CbrePageset/PagesetRest.svc/");
        proxy.invoke(
            "haseditpermission?url=" + document.location.toString().split(urlsplit)[0],
            "GET",
            {},
            function (result) {
                _PagesetEditPermission = result;
            },
            function (result) {
                logger.debug(result);
                _PagesetEditPermission = false;
            },
            false
        );
    }
    if (_PagesetInEditMode == null) {
        _PagesetInEditMode = false;
        ExecuteOrDelayUntilScriptLoaded(FindPagesetCookie, "Unknown_83_filename"/*tpa=http://www.cbre.com/_layouts/cbrepageset/sp.js*/);
    }
    return !_PagesetInEditMode && _PagesetEditPermission && _PagesetInfoRefreshed;
}
function savePagesetCookieSet() {
    if (_PagesetInEditMode == null) {
        _PagesetInEditMode = false;
        FindPagesetCookie();
    }
    return _PagesetInEditMode;
}
function FindPagesetCookie() {
    var clientContext = new SP.ClientContext.get_current();
    var web = clientContext.get_web();
    var list = web.get_lists().getByTitle("Pages");

    var url = document.location.toString().split(urlsplit);
    var lastSlashPosition = url[0].lastIndexOf("/");
    var fileUrl = url[0].substring(lastSlashPosition + 1);

    var camlQuery = new SP.CamlQuery();
    var q = "<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>";
    camlQuery.set_viewXml(q);
    this.listItemsFindCookie = list.getItems(camlQuery);
    clientContext.load(listItemsFindCookie, 'Include(PagesetID)');
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onFindPagesetCookieSuccess), Function.createDelegate(this, this.onQueryFailed));
}
function onFindPagesetCookieSuccess(sender, args) {
    var listEnumerator = this.listItemsFindCookie.getEnumerator();
    //should only be one item
    listEnumerator.moveNext();
    var item = listEnumerator.get_current();
    var id = item.get_item("PagesetID").toString();

    if (readCookie('PagesetInEditMode_' + id) == 'true')
        _PagesetInEditMode = true;
    else
        _PagesetInEditMode = false;

    _PagesetInfoRefreshed = true;

    RefreshCommandUI();
}

var FinishSaveurl;

function savePagesetCommand() {
    _PagesetInEditMode = false;
    RefreshCommandUI();

    var clientContext = new SP.ClientContext.get_current();
    var web = clientContext.get_web();
    var list = web.get_lists().getByTitle("Pages");

    var url = document.location.toString().split(urlsplit);
    var lastSlashPosition = url[0].lastIndexOf("/");
    var fileUrl = url[0].substring(lastSlashPosition + 1);

    var camlQuery = new SP.CamlQuery();
    var q = "<View Scope='RecursiveAll'><Query><Where><Eq><FieldRef Name='FileLeafRef'/><Value Type='Text'>" + fileUrl + "</Value></Eq></Where></Query></View>";
    camlQuery.set_viewXml(q);
    this.listItemsByUrl = list.getItems(camlQuery);
    clientContext.load(listItemsByUrl, 'Include(PagesetID)');
    clientContext.executeQueryAsync(Function.createDelegate(this, this.onSavePagesetCommandSuccess), Function.createDelegate(this, this.onQueryFailed));
}
function onSavePagesetCommandSuccess(sender, args) {
    var listEnumerator = this.listItemsByUrl.getEnumerator();
    //should only be one item
    listEnumerator.moveNext();
    var item = listEnumerator.get_current();
    var id = item.get_item("PagesetID").toString();

    _PagesetInEditMode = true;   // Postback from ExecuteCommand will set this correctly
    //eraseCookie('PagesetInEditMode_' + id); //done in following ExecuteCommand in PagesetLayoutPage.cs
    ExecuteCommand('PageStateGroupSaveAndStop');
}

function editPagesetPropertiesCommand() {
    if (_PagesetInEditMode == true) {
        setPagesetState('PageStateEditPagesetProperties');
        ExecuteCommand('PageStateGroupSave');
    }
    else {
        viewPagesetPropertiesCommand();
    }
}
function executeEditPagesetPropertiesDialogModal() {
    siteurl = _spPageContextInfo.webServerRelativeUrl;
    if (siteurl.charAt(siteurl.length - 1) != '/') {
        siteurl = siteurl + '/';
    }
    pageurl = siteurl + '_layouts/CbrePageset/PagesetPropertyEdit.aspx?url=' + window.location;

    var windowWidth = 750;
    var windowHeight = 640;
    if (FV4UI()) {
        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: dialogClosedRefreshCallback };
        var dialog = SP.UI.ModalDialog.showModalDialog(options);
    }
    else {
        var features;
        if (window.showModalDialog) {
            features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
        }
        else {
            features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
        }
        var result = commonShowModalDialog(pageurl, features, dialogClosedRefreshCallback);
    }
}

function viewPagesetPropertiesCommand() {
    siteurl = _spPageContextInfo.webServerRelativeUrl;
    if (siteurl.charAt(siteurl.length - 1) != '/') {
        siteurl = siteurl + '/';
    }
    pageurl = siteurl + '_layouts/CbrePageset/PagesetPropertyEdit.aspx?url=' + window.location + '&Mode=display';

    var windowWidth = 750;
    var windowHeight = 640;
    if (FV4UI()) {
        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl };
        var dialog = SP.UI.ModalDialog.showModalDialog(options);
    }
    else {
        var features;
        if (window.showModalDialog) {
            features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
        }
        else {
            features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
        }
        var result = commonShowModalDialog(pageurl, features, null);
    }
}

// Page Functions

function editPageCommand() {
    ExecuteCommand('PageStateGroupEdit');
}

function savePageCommand() {
    ExecuteCommand('PageStateGroupCheckin');
}

function isPageNotInEditMode() {
    return document.forms[0].elements["MSOLayout_InDesignMode"].value != "1";
}

var _IsPageInEditMode = null;   // Set Asynchronously and when successfully completed this var is reset and the EnableScript is recalled via RefreshCommandUI()
function isPageCheckedOut() {

    if (_IsPageInEditMode == null) {
        _IsPageInEditMode = false;
        $.ajax({
            url: "/_vti_bin/CbrePageset/PagesetRest.svc/ispagecheckedin",
            cache: false,
            data: { url: document.location.toString().split(urlsplit)[0] },
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                _IsPageInEditMode = !result.IsPageCheckedInResult;
                RefreshCommandUI();
            },
            error: function (err) {
                alert(err.errorMessage);
                return false;
            }
        });
    }
    return _IsPageInEditMode;
}

// Utility Functions

function ExecuteCommand(cmd) {
    var $v_0 = SP.Ribbon.PageManager.get_instance();
    if ($v_0) {
        $v_0.get_commandDispatcher().executeCommand(cmd, null);
    }
}

function onQueryFailed(sender, args) {
    alert('request failed ' + args.get_message() + '\n' + args.get_stackTrace());
}

function setPagesetState(value) {
    var date = new Date();
    date.setTime(date.getTime() + 30000);
    var expires = "; expires=" + date.toGMTString();
    document.cookie = "PagesetPageState=" + value + expires + "; path=/";
}
function getPagesetState() {
    return readCookie('PagesetPageState');
}
function removePagesetState() {
    return eraseCookie('PagesetPageState');
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

// Action Menu functions

function dialogClosedAddPagesetCallback(result, arguments) {
    if (FV4UI()) {
        if (result) {
            if (arguments) {
                executeNewPagesetPropertiesDialogModal(arguments);
            }
            else {
                window.location.reload();
            }
        }
    }
    else {
        if (result) {
            executeNewPagesetPropertiesDialogModal(arguments);
        }
        else {
            window.location.reload();
        }
    }
}
function dialogClosedPagesetPropertiesCallback(result, arguments) {
    if (FV4UI()) {
        if (result) {
            if (arguments) {
                SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
                window.location = arguments;
            }
            else {
                SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
                window.location.reload();
            }
        }
    }
    else {
        if (result) {
            SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
            window.location = result;
        }
        else {
            SP.Ribbon.PageState.PageStateHandler.ignoreNextUnload = true;
            window.location.reload();
        }
    }
}
function executeNewPagesetPropertiesDialogModal(location) {
    pageurl = location.substring(0, location.search("Pages/")) + '_layouts/CbrePageset/PagesetPropertyEdit.aspx?newurl=' + location;
    var windowWidth = 750;
    var windowHeight = 640;
    if (FV4UI()) {
        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: dialogClosedPagesetPropertiesCallback };
        var dialog = SP.UI.ModalDialog.showModalDialog(options);
    }
    else {
        var features;
        if (window.showModalDialog) {
            features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
        }
        else {
            features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
        }
        var result = commonShowModalDialog(pageurl, features, dialogClosedPagesetPropertiesCallback);
    }
}
function showPagesetDialogModal() {
    siteurl = _spPageContextInfo.webServerRelativeUrl;
    if (siteurl.charAt(siteurl.length - 1) != '/') {
        siteurl = siteurl + '/';
    }
    pageurl = siteurl + '_layouts/CbrePageset/AddPageset.aspx';
    var windowWidth = 810;
    var windowHeight = 680;
    if (FV4UI()) {
        var options = { width: windowWidth, height: windowHeight, resizable: true, status: false, menubar: false, help: false, url: pageurl, dialogReturnValueCallback: dialogClosedAddPagesetCallback };
        var dialog = SP.UI.ModalDialog.showModalDialog(options);
    }
    else {
        var features;
        if (window.showModalDialog) {
            features = 'dialogWidth:' + windowWidth + 'px;dialogHeight:' + windowHeight + 'px;resizable:yes;status:no;menubar:no;help:no';
        }
        else {
            features = 'width=' + windowWidth + ',height=' + windowHeight + ',resizable=yes,status=no,menubar=no,help=no';
        }
        var result = commonShowModalDialog(pageurl, features, dialogClosedAddPagesetCallback);
    }
}
