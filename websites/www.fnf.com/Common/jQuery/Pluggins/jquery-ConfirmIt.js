(function($) {
    $.extend({
        ConfirmLinkButton: function(btn, message) {

            if ($('#divConfirm').length != 0)
                $('#divConfirm').remove();
            //if ($('#divConfirm').length == 0) {
                $('body').append('<div id="divConfirm" style="text-align: center; padding: 10px;"></div>');
                var elements = '<div id=divConfirmMessage style="padding: 10px;"></div>'
                elements += '<input type="button" id="btnConfirmOK" value="OK" />';
                elements += '<input type="button" id="btnConfirmCancel" value="Cancel" />';
                $('#divConfirm').append(elements);
            //}

            $('#divConfirm').dialog({ autoOpen: false, modal: true });
            $('#divConfirm').dialog("open");
            $('#divConfirmMessage').text(message);
            $('#btnConfirmCancel').click(function($e0) {
                $('#divConfirm').dialog("close");
            });
            $('#btnConfirmOK').click(function($e1) {
                $e1.preventDefault();
                eval(btn.href);
            });
            return false;
        }
    })
})(jQuery);

(function($) {
    $.extend({
        ConfirmButton: function(fnOk, fnCancel, message, arg) {

            if ($('#divConfirm').length != 0)
                $('#divConfirm').remove();
            //if ($('#divConfirm').length == 0) {
            $('body').append('<div id=divConfirm style="text-align: center; padding: 10px;"></div>');
            var elements = '<div id=divConfirmMessage style="padding: 10px;"></div>'
            elements += '<input type="button" id="btnConfirmOK" value="OK" />';
            elements += '<input type="button" id="btnConfirmCancel" value="Cancel" />';
            $('#divConfirm').append(elements);
            //}

            $('#divConfirm').dialog({ autoOpen: false, modal: true });
            $('#divConfirm').dialog("open");
            $('#divConfirmMessage').text(message);
            $('#btnConfirmCancel').click(function($e0) {
                $('#divConfirm').dialog("close");
                if (fnCancel)
                    fnCancel(arg);
            });
            $('#btnConfirmOK').click(function($e1) {
                $e1.preventDefault();
                $('#divConfirm').dialog("close");
                if (fnOk)
                    fnOk(arg);
            });
            return false;
        }
    })
})(jQuery);

(function($) {
    $.extend({
        ConfirmButtonSubmit: function(button, message) {

            if ($('#divConfirm').length != 0) {
                if ($('#divConfirm').attr("submit") == "true")
                    return true;
                $('#divConfirm').remove();
            }
            $('body').append('<div id=divConfirm style="text-align: center; padding: 10px;"></div>');
            var elements = '<div id=divConfirmMessage style="padding: 10px;"></div>'
            $('#divConfirm').append(elements);

            $('#divConfirm').dialog({
                autoOpen: false,
                modal: true,
                buttons: {
                    "OK": function() {
                        $(this).attr("submit", "true");
                        $(this).dialog("close");
                        $(button).click();
                    },
                    "Cancel": function() {
                        $(this).dialog("close");
                    }
                }
            });
            $('#divConfirm').dialog("open");
            $('#divConfirmMessage').text(message);
            return false;
        }
    })
})(jQuery);

(function($) {
    $.extend({
        ConfirmItAlertMessage: function(message, fnOk, arg, title) {
            if (title === undefined || title === null)
                title = "";

            if ($('#divConfirmItMessage').length != 0)
                $('#divConfirmItMessage').remove();
            $('body').append('<div id=divConfirmItMessage class="confirmItMessage" style="text-align: center; padding: 10px;"></div>');
            var elements = '<div id=divConfirmItMessageText style="padding: 10px;"></div>'
            $('#divConfirmItMessage').append(elements);

            $('#divConfirmItMessage').dialog({
                autoOpen: false,
                modal: true,
                title: title,
                buttons: {
                    "OK": function() {
                        $(this).dialog("close");
                        if (fnOk)
                            fnOk(arg);
                    }
                }
            });
            $('#divConfirmItMessage').dialog("open");
            $('#divConfirmItMessageText').html(message);
            return false;
        }
    })
})(jQuery);


(function($) {
    $.extend({
        ConfirmItConfirm: function(fnOk, fnCancel, message, arg, title) {
            if (title === undefined || title === null)
                title = "";
            if ($('#divConfirm').length != 0)
                $('#divConfirm').remove();
            $('body').append('<div id=divConfirm style="text-align: center; padding: 10px;"></div>');
            var elements = '<div id=divConfirmMessage style="padding: 10px;"></div>'
            $('#divConfirm').append(elements);

            $('#divConfirm').dialog({
                autoOpen: false,
                modal: true,
                title: title,
                buttons: {
                    "OK": function() {
                        $(this).dialog("close");
                        if (fnOk)
                            fnOk(arg);
                    },
                    "Cancel": function() {
                        $(this).dialog("close");
                        if (fnCancel)
                            fnCancel(arg);
                    }
                }
            });
            $('#divConfirm').dialog("open");
            $('#divConfirmMessage').text(message);
            return false;
        }
    })
})(jQuery);