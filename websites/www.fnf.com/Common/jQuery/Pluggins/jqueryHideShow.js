$(document).ready(function() {
    $('#divAttorney').hide();
    $('#divLender').hide();
    $('#divCommercial').hide();
    $('#divBuilder').hide();
    $('#divRealtor').hide();
    $('#divAgent').hide();
    $('#divHomeOwner').hide();

    $('#mAttorney').hover(
        function() {
            $('#divAttorney').show();
            $('#divLender').hide();
            $('#divCommercial').hide();
            $('#divBuilder').hide();
            $('#divRealtor').hide();
            $('#divAgent').hide();
            $('#divHomeOwner').hide();
        },
        function() {
            $('#divAttorney').hide();
        }
    );
            $('#divAttorney').hover(
        function() { $('#divAttorney').show(); },
        function() { $('#divAttorney').hide(); }
    );
    
    $('#mLender').hover(
        function() {
            $('#divAttorney').hide();
            $('#divLender').show();
            $('#divCommercial').hide();
            $('#divBuilder').hide();
            $('#divRealtor').hide();
            $('#divAgent').hide();
            $('#divHomeOwner').hide();
        },
        function() {
            $('#divLender').hide();
        }
    );
            $('#divLender').hover(
        function() { $('#divLender').show(); },
        function() { $('#divLender').hide(); }
    );
    
    $('#mCommercial').hover(
        function() {
            $('#divAttorney').hide();
            $('#divLender').hide();
            $('#divCommercial').show();
            $('#divBuilder').hide();
            $('#divRealtor').hide();
            $('#divAgent').hide();
            $('#divHomeOwner').hide();
        },
        function() {
            $('#divCommercial').hide();
        }
    );
            $('#divCommercial').hover(
        function() { $('#divCommercial').show(); },
        function() { $('#divCommercial').hide(); }
    );
    
    $('#mBuilder').hover(
        function() {
            $('#divAttorney').hide();
            $('#divLender').hide();
            $('#divCommercial').hide();
            $('#divBuilder').show();
            $('#divRealtor').hide();
            $('#divAgent').hide();
            $('#divHomeOwner').hide();
        },
        function() {
            $('#divBuilder').hide();
        }
    );
            $('#divBuilder').hover(
        function() { $('#divBuilder').show(); },
        function() { $('#divBuilder').hide(); }
    );
    
    $('#mRealtor').hover(
        function() {
            $('#divAttorney').hide();
            $('#divLender').hide();
            $('#divCommercial').hide();
            $('#divBuilder').hide();
            $('#divRealtor').show();
            $('#divAgent').hide();
            $('#divHomeOwner').hide();
        },
        function() {
            $('#divRealtor').hide();
        }
    );
            $('#divRealtor').hover(
        function() { $('#divRealtor').show(); },
        function() { $('#divRealtor').hide(); }
    );
    
    $('#mAgent').hover(
        function() {
            $('#divAttorney').hide();
            $('#divLender').hide();
            $('#divCommercial').hide();
            $('#divBuilder').hide();
            $('#divRealtor').hide();
            $('#divAgent').show();
            $('#divHomeOwner').hide();
        },
        function() {
            $('#divAgent').hide();
        }
    );
            $('#divAgent').hover(
        function() { $('#divAgent').show(); },
        function() { $('#divAgent').hide(); }
    );
    
    $('#mHomeOwner').hover(
        function() {
            $('#divAttorney').hide();
            $('#divLender').hide();
            $('#divCommercial').hide();
            $('#divBuilder').hide();
            $('#divRealtor').hide();
            $('#divAgent').hide();
            $('#divHomeOwner').show();
        },
        function() {
            $('#divHomeOwner').hide();
        }
    );
            $('#divHomeOwner').hover(
        function() { $('#divHomeOwner').show(); },
        function() { $('#divHomeOwner').hide(); }
    );
});