
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    
	<title>JetBlue</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/css/global.css?v=1.5">
    <link rel="stylesheet" href="/css/Background.css">
    <link rel="stylesheet" href="/css/resolve.css">
        
    <script type="text/javascript">
            var jsonData = null;
            var jsonTData = null;

            var scheduleExtDate =  {
    "year": 2016,
    "month": 2,
    "day": 10
}

    </script>
    <script type="text/javascript" src="/js/util/jbConsoleProtect.js"></script>
    <script src="/js/vendor/modernizr-2.0.6.min.js"></script>
    <script type="text/javascript" src="//fonts.jetblue.com/sfh4pzq.js"></script>
    <script type="text/javascript">        try { Typekit.load(); } catch (e) { }</script>

    
    <script src="/js/vendor/angularjs/1.2.6/angular.min.js"></script>    
    <script src="/js/vendor/angularjs/1.2.6/angular-route.min.js"></script>
    <script src="/js/vendor/angularjs/1.2.6/angular-resource.min.js"></script>
    
   

    <script src="/js/vendor/jquery/1.6.2/jquery.min.js"></script>
    <script src="/js/vendor/jquery-ui-1.8.16.custom.min.js"></script>
    <script src="/js/vendor/jquery.placeholder.js" type="text/javascript"></script>
    <script src="/js/util/touchDetection.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        if (typeof (oJFK) == 'undefined') document.write('<script src="/ajax-data/booker-flights.js"><\/script>');
    </script>

    <script>        window.jQuery || document.write('<script src="/js/vendor/jquery-1.6.2.min.js"><\/script>')</script>
    <script type="text/javascript">
        function clearSearchText() {
            document.getElementById('search-jb').reset();
        }

        window.onload = clearSearchText;
    </script>
    <script src="/js/vendor/MP_JB_enes.js" type="text/javascript"></script>
    <script src="/js/jbAnalytics.min.js?v=1.0"></script>
    <script type="text/javascript">
        try {
            var jbAnalyticsController = new AnalyticsController();
            $(jbAnalyticsController).trigger(jbAnalyticsController.EVENTS.SET_SERVER_DATA_IN_PAGEMETRICS, [{server: "JetBlue",channel: "ERROR",pageName: "404",TB: false}]);
        }
        catch (err) {
            console.log(err);
        }
    </script>
    <script src="//s.btstatic.com/tag.js">
        { site: "UXE8DVB", mode: "sync" }
    </script>


    
    <link rel="stylesheet" href="/css/error/404.css">

</head>
<body class="">
    <script type="text/javascript">
        // longitude and latitude from akamai header:
        
        var akamaiLat = 37.2556;
        var akamaiLong = -80.429;
        

        // this script logs out a logged in user on the event of a login timeout
        var isLoggedIn = "False";
        if(isLoggedIn.toLowerCase() == "true")
        {
            var sessionTimeout = 15 * 60000;
            window.setTimeout("OnLoginTimeout()", sessionTimeout);
        }

        function OnLoginTimeout() {
            window.location.reload(); // reload page to show logged out view
        }
    </script>

    <script type="text/javascript">
        var cookies = document.cookie.split(";");
        var baseAirportCookie = "";
        var travelModeValue = "";

        for (var i = 0; i < cookies.length; i++) {
            if (jQuery.trim(cookies[i].substr(0, cookies[i].indexOf("="))) == "base_airport")
                baseAirportCookie = cookies[i].substr(cookies[i].indexOf("=") + 1);
        }
        if (typeof (sUserState) != "undefined") {
            switch (sUserState) {
                case "Can't determine":
                    travelModeValue = "def";
                    break;
                case "Not Logged in":
                    travelModeValue = "def";
                    break;
                case "Logged in|No Travel":
                    travelModeValue = "li";
                    break;
                case "Logged in|lt48 hours":
                    travelModeValue = "lt48";
                    break;
                case "Logged in|lt24 hours":
                    travelModeValue = "lt24";
                    break;
                case "Logged in|Checked in":
                    travelModeValue = "ci";
                    break;
            }
        }
        if (baseAirportCookie == "") travelModeValue = "";
    </script>
    <div id="bgHolder">
        <div class="background">
        </div>
    </div>
    
    <div class="page">

    <div id="fb-root">
    </div>
    <div id="jb-header" style="display: none">
        <div class="wrapper">
              <div id="skipToMainContentLink">
                 <a href="#container">Skip to main content</a>
              </div>
                                  
              
              
                    <a href="/?intcmp=hd_home" class="jbLogo logo"><img src="/img/jetblue-white-reg.png" alt="JetBlue"></img></a>
              
                        
            
            <div class="global-nav">
                <div align="right">
                    <iframe role="presentation" src="/JetblueAlerts/MainTitle.aspx" width="500" height="38" marginwidth="0"
                        marginheight="0" align="top" scrolling="no" frameborder="0" id="hdalert" name="hdalert" title="Flight Alerts">
                    </iframe>
                </div>                
                <ul class="login-utility">
                    <li><a lang="es" class="espanol jb-primary-link" mporgnav href="http://hola.jetblue.com/"
                        onclick="return switchLanguage('es');
                        function switchLanguage(lang) {
                            MP.SrcUrl=unescape('mp_js_orgin_url');MP.UrlLang='mp_js_current_lang';MP.init();
	                        MP.switchLanguage(MP.UrlLang==lang?'en':lang);
	                        return false;}">Espa&ntilde;ol</a>
                    </li>
                    <li>
                        <a class="help jb-primary-link" href="/help/">
                        Help
                        </a>
                    </li>
                    <li>
                        <a class="contact-us jb-primary-link" href="/contact-us/">
                        Contact us
                        </a>
                    </li>
                    <li class="soflyHeaderMaster">
                        <a class="jb-primary-link" href="/SoFly/">
                        SoFly<!--<span class="jbSocialBlueTMMasterHeader">™</span>-->
                        </a><!--<div class="soflyHeaderNew">NEW</div>-->
                    </li>
                    
                            <li class="true-blue-sign-in">
                            <span class="trueblue-sprite">
                                <img src="/img/bg-true-blue-label.jpg" alt="TrueBlue Login" class="trueblue-sprite-login"></img>
                            </span>
                            <a class="sign-in jb-primary-link" href="https://book.jetblue.com/B6.auth/login?intcmp=hd_signin&service=https://www.jetblue.com/error/404/default.aspx?404;https://www.jetblue.com:80/jbTags/js/jbtags.js">
                                <span>Sign In</span>
                            </a>
                            </li>
                            <li class="true-blue-join"><a class="join jb-primary-link" href="https://trueblue.jetblue.com/web/trueblue/register/?intcmp=hd_join">
                                <span>Join</span></a></li>
                        
                </ul>
              
            </div>
            <!-- END: .global-nav -->
            <div class="main-nav">
                <ul id="jb-primary-links">
                    <!-- @v: 1.0.1 - 2015-0420 for prod (updated from prod src) -->
<li class="first submenu">
<a class="plan-a-trip jb-primary-link" href="/plan-a-trip/">
    <span class="foreground-sprite">
        <img src="/img/bg-main-nav-primary.png" alt=""></img>
    </span>
    <span class="name">Plan a trip</span>
</a>
    <div class="jb-secondary-links">
        <div class="plan-a-trip-submenu">
            <ul>
                <li class="flights">
                <a class="jb-secondary-link" href="/flights/">
                <span class="foreground-sprite">
                    <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                </span>
                <span class="secondary-link-name">Flights</span>
                
                </a>
                </li>
                <li class="vacations">
                <a class="jb-secondary-link" href="/vacations/">
                <span class="foreground-sprite">
                    <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                </span>
                <span class="secondary-link-name">Getaways</span>
                </a>
                </li>
                <li class="hotels">
                <a class="jb-secondary-link" href="/hotels/">
                    <span class="foreground-sprite">
                        <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                    </span>
                    <span class="secondary-link-name">Hotels</span>
                
                </a>
                </li>
                <li class="cars">
                <a class="jb-secondary-link" href="/cars/">
                    <span class="foreground-sprite">
                        <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                    </span>
                    <span class="secondary-link-name">Cars</span>
                </a>
                </li>
                <li class="cruises">
                <a class="jb-secondary-link" href="/cruises/" target="_blank">
                    <span class="foreground-sprite">
                        <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                    </span>
                    <span class="secondary-link-name">Cruises<img src="/img/new-window-icon.png" class="new-window" alt="opens a new window"/></span>
                </a>
                </li>
                <li class="deals-sprite">
                <a class="jb-secondary-link" href="/deals/">
                    <span class="foreground-sprite">
                        <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                    </span>
                    <span class="secondary-link-name">Deals</span>
                </a>
                </li>

                <li class="additional-jb-link">
                <ul>
                    <li class="additional-plan-a-trip-links">
                        <a class="jb-secondary-link" href="/timetable/">Timetables</a> 
                        <a href="/email/" class="jb-secondary-link">Fare alerts</a>
                        <a class="jb-secondary-link" id="FClink" href="/bestfarefinder/">Best Fare Finder</a>
                    </li>
                </ul>
                </li>                                    
            </ul>
        </div>
    </div>
</li>
<li>
    <a class="manage-flight jb-primary-link" href="https://book.jetblue.com/B6.myb/landing.html">
        <span class="foreground-sprite">
            <img src="/img/bg-main-nav-primary.png" alt=""></img>
        </span>
        <span class="name">Manage flights</span>
    </a>
</li>
<li>
    <a class="where-we-jet jb-primary-link" href="/wherewejet/">
        <span class="foreground-sprite">
            <img src="/img/bg-main-nav-primary.png" alt=""></img>
        </span>
        <span class="name">Where we jet</span>
    </a>    
</li>
<li class="submenu">
    <a class="flying-on-jetblue jb-primary-link" href="/flying-on-jetblue/">
        <span class="foreground-sprite">
            <img src="/img/bg-main-nav-primary.png" alt=""></img>
        </span>
        <span class="name">Flying on JetBlue</span>
    </a>
    <div class="jb-secondary-links">
        <div class="flying-on-jetblue-submenu">
            <ul>
                <li class="directv">
                    <a class="jb-secondary-link" href="/flying-on-jetblue/directv/">
                        <span class="foreground-sprite">
                            <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                        </span>
                        <span class="secondary-link-name">36 channels of<br />
                        live TV at every seat</span>
                    </a>
                </li>
              
                <li class="first-bag">
                    <a class="jb-secondary-link" href="/flying-on-jetblue/snacks-and-drinks/">
                        <span class="foreground-sprite">
                            <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                        </span>
                        <span class="secondary-link-name">Snacks &amp; drinks</span>
                    </a>
                </li>
                <li class="even-more">
                    <a class="jb-secondary-link" href="/flying-on-jetblue/even-more/">
                        <span class="foreground-sprite">
                            <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                        </span>
                        <span class="secondary-link-name">Even More&trade;</span>
                    
                    </a>
                </li>
                <li class="mint-tab">
                    <a class="jb-secondary-link" href="/flying-on-jetblue/mint/">
                        <span class="foreground-sprite">
                            <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                        </span>
                        <span class="secondary-link-name">Mint</span>
                    </a>
                </li>
                <li class="additional-jb-link">
                <ul>
                    <li class="additional-flying-on-jetblue-links-1">
                        <a class="jb-secondary-link" href="/flying-on-jetblue/mint/">Mint</a>
                        <a class="jb-secondary-link" href="/flying-on-jetblue/directv/">DIRECTV&reg;</a> 
                        <a class="jb-secondary-link" href="/flying-on-jetblue/snacks-and-drinks/">Snacks &amp; drinks</a> 
                        <a class="jb-secondary-link" href="/flying-on-jetblue/radio/">SiriusXM Radio&reg;</a> 
                        <a class="jb-secondary-link" href="/flying-on-jetblue/movies/">Movies &amp; more</a> 
                    </li>
                    <li class="additional-flying-on-jetblue-links-2">
                        <a class="jb-secondary-link" href="/flying-on-jetblue/even-more/" >Even More&trade;</a> 
                        <a class="jb-secondary-link" href="/flying-on-jetblue/wifi/">Wi-Fi</a>
                        <a href="/flying-on-jetblue/shut-eye/" class="jb-secondary-link" >Shut Eye service</a>
                        <a href="/flying-on-jetblue/customer-protection/" class="jb-secondary-link">Customer protection</a>
                    </li>
                </ul>
                </li>
                
            </ul>
        </div>
    </div>
</li>
<li class="submenu">
    <a class="travel-information jb-primary-link" href="/travel/">
        <span class="foreground-sprite">
            <img src="/img/bg-main-nav-primary.png" alt=""></img>
        </span>
        <span class="name">Travel information</span>    
    </a>
    <div class="jb-secondary-links">
        <div class="travel-information-submenu">
            <ul>
                <li class="t-5">
                    <a class="jb-secondary-link" href="/travel/jfk/">
                        <span class="foreground-sprite">
                            <img src="/img/bg_main_nav_secondary_icons.jpg" alt="T5"></img>
                        </span>
                        <span class="secondary-link-name">JFK</span>                
                    </a>
                </li>
                <li class="baggage-info">
                    <a class="jb-secondary-link" href="/travel/baggage/">
                        <span class="foreground-sprite">
                            <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                        </span>
                        <span class="secondary-link-name">Baggage info</span>
                    </a>
                </li>
                <li class="special-needs">
                    <a class="jb-secondary-link" href="/travel/special-needs/">
                        <span class="foreground-sprite">
                            <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                        </span>
                        <span class="secondary-link-name">Special assistance</span>
                    </a>
                </li>
                <li class="jet-paws">
                    <a class="jb-secondary-link" href="/travel/pets/">
                        <span class="foreground-sprite">
                            <img src="/img/bg_main_nav_secondary_icons.jpg" alt=""></img>
                        </span>
                        <span class="secondary-link-name">JetPaws&trade;</span>                
                    </a>
                </li>
                <li class="additional-jb-link">
                <ul>
                    <li class="additional-travel-information-links-1">
                        <a class="jb-secondary-link" href="/travel/jfk/" >T5 at JFK</a> 
                        <a class="jb-secondary-link" href="/travel/baggage/" >Baggage info</a>
                        <a class="jb-secondary-link" href="/travel/airports/" >Airport information</a>
                        <a class="jb-secondary-link" href="/travel/cancellations-delays/" >Cancellations &amp; delays</a>
                        <a class="jb-secondary-link" href="/travel/planes/" >Our planes</a> 
                    </li>
                    <li class="additional-travel-information-links-2">
                        <a class="jb-secondary-link" href="/travel/special-needs/" >Special assistance</a>
                        <a class="jb-secondary-link" href="/travel/international-travel/" >International travel</a> 
                        <a class="jb-secondary-link" href="/travel/groups/" >Groups (8+travelers)</a> 
                        <a class="jb-secondary-link" href="/travel/pets/" >Pets</a>
                        <a class="jb-secondary-link" href="/travel/kids/" >Traveling with kids</a>
                    </li>
                </ul>
                </li>
            </ul>
        </div>
    </div>
</li>
                    <li>
                        
                                <a id="tbmvLoggedOutMode" class="true-blue jb-primary-link" href="http://trueblue.jetblue.com/?intcmp=hd_trueblue">
                                    <span class="foreground-sprite">
                                        <img src="/img/bg-main-nav-primary.png" alt=""></img>
                                    </span>
                                    <span class="member">TrueBlue</span>
                                </a>
                            
                    </li>
                </ul>
                <div id="jb-backdrop">
                    &nbsp;</div>
            </div>
            <!-- END: .main-nav -->
        </div>
    </div>
    <!-- Check if page is in logged in view but the TrueBlue cookie is expired. If so, reload page to show logged out view instead of the cached page. -->
    <script type="text/javascript">
        $('.manage-flight')[0].href = 'https://book.jetblue.com/B6.myb';
        // Read JetBlue TrueBlue cookie
        var cookies = document.cookie.split(";");
        var jbtbCookie;
        if (cookies != null) {
            var cookieName = "jbTrueBlueCookie";
            for (var i = 0; i < cookies.length; i++) {
                if (jQuery.trim(cookies[i].substr(0, cookies[i].indexOf("="))) == cookieName)
                    jbtbCookie = unescape(cookies[i].substr(cookies[i].indexOf("=") + 1));
            }
        }

        var loggedInElem = document.getElementById("tbmvLoggedInMode");
        var travelModePreview = "";
        if (!jbtbCookie && loggedInElem && !travelModePreview)
            window.location.reload(); // reload page to show logged out view
        else
            document.getElementById("jb-header").style.display = "block";  // show the header since it is hidden by default to not show logged out users their name & points on cached pages.
    </script>
    

<div class="bg-banner-wrap">

    <div id="carry-on-bag" class="row-one-wrap clearfix six">
    
        <div class="col6">

            <h2 class="alpha heading">Sad face</h2>
            
            <div style="width:550px;"><p class="intro">Even though the page you requested couldn't be found, there are plenty of places on jetblue.com that will turn that frown upside down.</p>
            
            <h3>Find your way</h3>
<p class="alpha"><a href="https://book.jetblue.com/B6/webqtrip.html">Book a flight</a><br />
<a href="https://checkin.jetblue.com/B6WebCheckIn/WebCheckIn.html#" target="_blank">Get boarding pass</a><br />
<a href="/flightstatus/">Track a flight</a><br />
<a href="https://book.jetblue.com/B6.myb/landing.html">Manage your flights</a><br />
<a href="/vacations/">Book a vacation</a><br />
<a href="/">Go home</a></p>

</div>

        </div> <!-- /.col6 -->
    
    </div> <!-- /.row-one-wrap -->

</div> <!-- /.bg-banner-wrap -->
    

    <div id="jb-footer">
        <div class="header-wrap clearfix">
            <div class="header-inner clearfix">
                <div class="app-wrap">
                    <div class="media">
                        <div class="app-wrap">
    <div class="media">
        <div class="img"><a href="/mobile/iphone/appdownload/" target="_blank"><img src="/img/jbapp-icon.png?v=1" title="JetBlue app icon" border="0"/></a></div>
        <div class="inner-media">
            <p class="hd"><strong>Same smart app. More smartphones.</strong><span class="fine-print"><a href="/mobile/">Download the JetBlue mobile app for iPhone and Android now!</a></span></p>
        </div>
    </div>
</div>
                    </div>
                </div>
                <!-- /.app-wrap -->
                <div class="search-wrap">
                    <form name="search-jb" id="search-jb" method="post" action="//help.jetblue.com/SRVS/CGI-BIN/webisapi.dll/,/">
                    <input type="hidden" name="kb" value="Askblue"></input>
                    <input type="hidden" name="company" value="{FC8CB04E-D869-4864-A23E-066A382DAD8E}"></input>
                    <input type="hidden" name="command" value="new"></input>
                    <!-- <input name="NATURAL" size="32" type="text" />-->
                    <div class="search-inner">
                        <label for="search-box" class="offscreen">Search jetblue.com</label><input id="search-box" type="text" name="NATURAL" placeholder="Search jetblue.com" />
                        <input id="loupe-btn" type="submit" name="search_btn" value="Search" />
                    </div>
                    <!-- /.search-inner -->
                    </form>
                </div>
                <!-- /.search-wrap -->
            </div>
            <!-- /.header-inner -->
        </div>
        <!-- /.header-wrap -->
        <div class="body-wrap clearfix">
            <div class="social-wrap clearfix">
                <ul>
                    <li class="soflyFooter">
                        <div class="inner-media">
							<a href="/Sofly/">
                                <img src="/img/soflybubbleicon.png" /><span>SoFly <span class="jbSocialBlueTMMasterFooter">&trade;</span></span>
                            </a>
							<span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View and share JetBlue pics and travel tricks&nbsp;								
                            </span>
                            <!--<div class="soflyFooterNew">NEW</div>-->
                        </div>
                    </li>
                   
                    <li class="facebook">
                        <div class="inner-media">
                            <a href="http://www.facebook.com/JetBlue" target="_blank">
                                <img src="/img/facebookicon.png" />Like us on Facebook
                            </a>
                            <span>&nbsp;1,025,719&nbsp;like this</span>
                        </div>
                    </li>
                    <li class="twitter">
                        <div class="inner-media">
                            <a href="http://twitter.com/jetblue" target="_blank">
                                <img src="/img/twittericon.png" />Follow us at @JetBlue
                            </a>
                            <span>&nbsp;1,939,033&nbsp;followers</span>
                        </div>
                    </li>
					<li class="instagram">
						<div class="inner-media">
							<a href="http://www.instagram.com/jetblue" target ="_blank">
								<img src="/img/instagramicon.png" />Follow us on Instagram
							</a>
                            <span>&nbsp;63,525&nbsp;followers</span>
						</div>
					</li>
                    <li class="youtube">
                        <div class="inner-media">
                            <a href="http://www.youtube.com/jetblue" target="_blank">
                                <img src="/img/youtubeicon.png" />Visit us on YouTube
                            </a>
                            <span>&nbsp;5,610,887&nbsp;total views</span>
                        </div>
                    </li>
                    <li class="btblog">
                        <div class="inner-media">
                            <a href="http://blog.jetblue.com" target="_blank">
                                <img src="/img/bluetalesicon.png" />BlueTales Blog
                            </a>
                            <span>&nbsp;Your destination for all things JetBlue and beyond <p>&nbsp;</p></span>
                        </div>
                    </li>
                </ul>
            </div><div class="sitemap-wrap">
                <ul>
                    <li class="list-head">Get to know us</li>
                    <li><a href="/about/">About us</a></li>
					<li><a href="http://mediaroom.jetblue.com/media-room.aspx">Press room</a></li>
					<li><a href="/corporate-social-responsibility/">CSR</a></li>
					<li><a href="/green/">Sustainability</a></li>
					<li><a href="/about/promotions/">Promotions</a></li>
					<li><a href="/mobile/">Mobile</a></li>
                    <li><a href="https://shop.jetblue.com/" target="_blank">ShopBlue</a></li>
					<li><a href="http://investor.jetblue.com/investor-relations.aspx">Investor Relations</a></li>
                </ul>
                <!-- /Get to know us -->
                <ul>
                    <li class="list-head">Services</li>
                    <li><a href="/airline-partners/">Partner airlines</a></li>
                    <li><a href="/vacations/">Getaways</a></li>
                    <li><a href="/corporate/">Corporate</a></li>
                    <li><a href="/cargo/">Cargo</a></li>
                    <li><a href="/travel-agents/">Travel agents</a></li>
                    <li><a href="/travel/special-needs/">Special assistance</a></li>
					<li><a href="/travel-insurance/">Travel Insurance</a></li>
					<li><a href="/traveldeals/veterans-discount/">Veterans Discount</a></li>
                </ul>
                <!-- /Services -->
                <ul class="last">
                    <li class="list-head">Talk to us</li>
                    <li><a href="/contact-us/">Contact us</a></li>
					<li><a href="/contact-us/receipt/">Request a receipt</a></li>
                    <li><a href="/work-here/">Work here</a></li>
                    <li><a href="/legal/">Legal</a></li>
                    <li><a href="/legal/privacy/">Privacy</a></li>
                    <li><a href="/sitemap/">Site map</a></li>
                    <li><a href="/contact-us/email/suggestion/?topic=jetblue.com">Website feedback</a></li>
				</ul>
                <!-- /Talk to us -->
            </div>
            <!-- /.sitemap-wrap -->
            <div id="getaways-cities-list" class="hide-cities clearfix">
                <div class="getaways-cities-wrap">
                    <p><strong>Top JetBlue Getaways vacation packages:</strong> <a href="/vacations/aruba-vacations/?intcmp=jb_footer" data-city="AUA">Aruba</a> | <a href="/vacations/barbados-vacations/?intcmp=jb_footer" data-city="BGI">Barbados</a> | <a href="/vacations/bermuda-vacations/?intcmp=jb_footer" data-city="BDA">Bermuda</a> | <a href="/vacations/cancun-vacations/?intcmp=jb_footer" data-city="CUN">Cancun, Mexico</a> | <a href="/vacations/curacao-vacations/?intcmp=jb_footer" data-city="CUR">Curacao</a> | <a href="/vacations/dominican-republic-vacations/?intcmp=jb_footer">Dominican Republic</a> | <a href="/vacations/fort-lauderdale-vacations/?intcmp=jb_footer" data-city="FLL XFL">Fort Lauderdale</a> | <a href="/vacations/grand-cayman-vacations/?intcmp=jb_footer" data-city="GCM">Grand Cayman</a> | <a href="/vacations/jamaica-vacations/?intcmp=jb_footer" data-city="MBJ">Jamaica</a> | <a href="/vacations/liberia-costa-rica-vacations/?intcmp=jb_footer" data-city="LIR">Liberia, Costa Rica</a> | <a href="/vacations/miami-vacations/" data-city="FLL">Miami</a> | <a href="/vacations/bahamas-vacations/?intcmp=jb_footer" data-city="NAS">Nassau, Bahamas</a> | <a href="/vacations/new-york-city-vacations/?intcmp=jb_footer" data-city="JFK LGA EWR HPN SWF NYC">New York City</a> | <a href="/vacations/puerto-rico-vacations/?intcmp=jb_footer" data-city="SJU PSE BQN">Puerto Rico</a> | <a href="/vacations/st-lucia-vacations/?intcmp=jb_footer" data-city="UVF">St. Lucia</a> | <a href="/vacations/turks-caicos-vacations/?intcmp=jb_footer" data-city="PLS">Turks &amp; Caicos</a> | <a href="/vacations/walt-disney-world-vacations/?intcmp=jb_footer" data-city="MCO"><em>Walt Disney World&reg;</em> Resort</a><span class="show-more"><a href="/vacations/aguadilla-vacations/?intcmp=jb_footer" data-city="BQN">Aguadilla, Puerto Rico</a> | <a href="/vacations/albuquerque-vacations/?intcmp=jb_footer" data-city="ABQ">Albuquerque</a> | <a href="/vacations/disneyland-vacations/?intcmp=jb_footer" data-city="LGB ZLA">Anaheim / Disneyland&reg; Resort</a> | <a href="/vacations/bogota-colombia-vacations/?intcmp=jb_footer" data-city="BOG">Bogot&aacute;, Colombia</a> | <a href="/vacations/boston-vacations/?intcmp=jb_footer" data-city="BOS XBO">Boston</a> | <a href="/vacations/riviera-maya-vacations/?intcmp=jb_footer" data-city="CUN">Cancun Riviera Maya</a> | <a href="/vacations/cartagena-vacations/?intcmp=jb_footer" data-city="CTG">Cartagena, Colombia</a> | <a href="/vacations/charleston-vacations/?intcmp=jb_footer" data-city="CHS">Charleston</a> | <a href="/vacations/fort-myers-vacations/?intcmp=jb_footer" data-city="RSW">Fort Myers</a> | <a href="/vacations/grenada-vacations/?intcmp=jb_footer" data-city="GND">Grenada</a> | <a href="/vacations/hilton-head-vacations/?intcmp=jb_footer" data-city="SAV">Hilton Head, SC</a> |<a href="/vacations/los-angeles-vacations/?intcmp=jb_footer" data-city="LAX ZLA">LA / Hollywood</a> | <a href="/vacations/la-romana-vacations/?intcmp=jb_footer" data-city="LRM XDR">La Romana, Dominican Republic</a> | <a href="/vacations/las-vegas-vacations/?intcmp=jb_footer" data-city="LAS">Las Vegas</a> | <a href="/vacations/new-orleans-vacations/?intcmp=jb_footer" data-city="MSY">New Orleans</a> | <a href="/vacations/orlando-vacations/?intcmp=jb_footer" data-city="MCO">Orlando</a> | <a href="/vacations/phoenix-vacations/?intcmp=jb_footer" data-city="PHX">Phoenix</a> | <a href="/vacations/ponce-vacations/?intcmp=jb_footer" data-city="PSE">Ponc&eacute;, Puerto Rico</a> | <a href="/vacations/puerto-plata-vacations/?intcmp=jb_footer" data-city="POP">Puerto Plata, Dominican Republic</a> | <a href="/vacations/punta-cana-vacations/?intcmp=jb_footer" data-city="PUJ">Punta Cana, Dominican Republic</a> | <a href="/vacations/reno-tahoe-vacations/?intcmp=jb_footer" data-city="RNO">Reno / Lake Tahoe</a> | <a href="/vacations/st-croix-vacations/?intcmp=jb_footer" data-city="STX">Saint Croix</a> | <a href="/vacations/st-maarten-vacations/?intcmp=jb_footer" data-city="SXM">Saint Martin</a> | <a href="/vacations/st-thomas-vacations/?intcmp=jb_footer" data-city="STT">St. Thomas / St. John</a> | <a href="/vacations/salt-lake-park-city-vacations/?intcmp=jb_footer" data-city="SLC">Salt Lake/Park City</a> | <a href="/vacations/samana-vacations/?intcmp=jb_footer" data-city="AZS">Samana, Dominican Republic</a> | <a href="/vacations/san-diego-vacations/?intcmp=jb_footer" data-city="SAN">San Diego</a> | <a href="/vacations/san-francisco-vacations/?intcmp=jb_footer" data-city="SFO XSF">San Francisco</a> | <a href="/vacations/costa-rica-vacations/" data-city="SJO">San Jos&eacute;, Costa Rica</a> | <a href="/vacations/san-juan-vacations/?intcmp=jb_footer" data-city="SJU">San Juan, Puerto Rico</a> | <a href="/vacations/santo-domingo-vacations/?intcmp=jb_footer" data-city="SDQ XDR">Santo Domingo, Dominican Republic</a> | <a href="/vacations/sarasota-vacations/?intcmp=jb_footer" data-city="SRQ">Sarasota</a> | <a href="/vacations/savannah-georgia-vacations/?intcmp=jb_footer" data-city="SAV">Savannah</a> | <a href="/vacations/st-pete-clearwater-vacations/?intcmp=jb_footer" data-city="TPA">St. Pete / Clearwater</a> | <a href="/vacations/west-palm-beach-vacations/?intcmp=jb_footer" data-city="PBI XFL">West Palm Beach</a><br><strong>Travel ideas:</strong> <a href="/vacations/adventure/?id=1&intcmp=jb_footer">Adventure</a> | <a href="/vacations/all-inclusive/?id=2&intcmp=jb_footer">All Inclusive</a> | <a href="/vacations/budget-travel/?id=3&intcmp=jb_footer">Budget</a> | <a href="/vacations/family/?id=4&intcmp=jb_footer">Family</a> | <a href="/vacations/girlfriend-getaways/?id=5&intcmp=jb_footer">Girlfriend Getaways</a> | <a href="/vacations/golf/?id=6&intcmp=jb_footer">Golf</a> | <a href="/vacations/Honeymoons-and-babymoons/?id=7&intcmp=jb_footer">Honeymoon &amp; Babymoons</a> | <a href="/vacations/luxury/?id=8&intcmp=jb_footer">Luxury</a> | <a href="/vacations/Romance/?id=9&intcmp=jb_footer">Romance</a> | <a href="/vacations/Spa/?id=10&intcmp=jb_footer">Spa</a></span></p>
                    <a id="city-expander" href="javascript:void(0);"><p class="expander">
                        <span>Show more cities</span> &nbsp;<img class="down-icon" src="/img/getaway-cities-list-arrow.jpg"
                            alt="" width="7" height="8" /><img class="up-icon" src="/img/getaway-cities-list-arrow-up.jpg"
                                alt="" width="7" height="8" /></p></a>
                </div>
            </div>
        </div>
        <!-- /.body-wrap -->
        <div class="footer-wrap">
            <span class="fine-print">
                <script type="text/javascript">
				<!--
                    var copyrightcurrentTime = new Date()
                    var copyrightyear = copyrightcurrentTime.getFullYear()
                    document.write("&copy;" + copyrightyear + " JetBlue Airways")
				//-->
                </script>
            </span>
        </div>
        <!-- /.footer-wrap -->
    </div>
    <!-- /#jb-footer -->
    <!-- JavaScript at the bottom for fast page loading -->
    <script src="/js/src/JB.GlobalBookerErrors.js?v=1.1"></script>
    <script src="/js/SetSabreAnalyticsCookie.js"></script>
    <script src="/js/vendor/simple-inheritance.js"></script>
    <script src="/js/vendor/jquery.scrollTo-1.4.2-min.js"></script>
    <script src="/js/vendor/json2.min.js"></script>
    <script src="/js/vendor/amplify.min.js"></script>
    <script src="/js/cookie.js?v=1.3"></script>
    <script src="/js/util/new-window-attribute.js" type="text/javascript"></script>

    <!-- scripts concatenated and minified via ant build script-->
    <script src="/js/src/JB.js?v=1.1"></script>
    <script src="/js/src/JB.Fn.js"></script>
    <script src="/js/src/JB.Helper.js?v=1.4.6"></script>
    <script src="/js/src/JB.Model.js?v=1.0.0"></script>
    <script src="/js/vendor/jquery.cookie.js?v=1.0"></script>
	
    <script src="/js/geolocation/geoLocation_main.js" defer></script>

    <script src="/js/src/JB.Class.TrueBlue.js?v=1.0"></script>

    <!-- end scripts-->
    <!--
	<script>
		$(function() {
			$('#calendar-wrap').datepicker({
				numberOfMonths: 2,
				dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
				minDate: new Date(),
				maxDate: new Date(2012, 4 - 1, 30)
			});
		});
	</script>
-->
    <!--[if lt IE 7 ]>
	<script src="/js/libs/dd_belatedpng.js"></script>
	<script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
	<![endif]-->
    <!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
	<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->
    <!-- <script> // Change UA-XXXXX-X to be your site's ID
		window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
		Modernizr.load({
		  load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
		});
	</script> -->
    <!-- OMNITURE TRAILER CODE STARTS HERE | OMNITURE TRAILER CODE STARTS HERE | OMNITURE TRAILER CODE STARTS HERE -->
    <script language="JavaScript">
        if (navigator.appVersion.indexOf('MSIE') >= 0) document.write(unescape('%3C') + '\!-' + '-');
    </script>
    <noscript>
        <img src="//jetbluecom2.112.2O7.net/b/ss/jetbluecom2/1/H.6--NS/0" height="1" width="1"
            border="0" alt="" />
    </noscript>
    <!--/DO NOT REMOVE/-->
    <script language="JavaScript">
        // fix missing MMSwapImage and MMswapImgStore files
        function MM_swapImage() { }
        function MM_swapImgRestore() { } 
    </script>
    <!-- End SiteCatalyst code version: H.6. -->
    <!-- OMNITURE TRAILER CODE ENDS HERE | OMNITURE TRAILER CODE ENDS HERE | OMNITURE TRAILER CODE ENDS HERE -->
    
    <!--Add content to the end of the footer-->
    
    <!--omniture tag--->
    <script language="JavaScript">
    <!--
    s.pageName="JetBlue:Error:404"
    var s_code=s.t();
    if(s_code)document.write(s_code)//--></script>
    <!--/omniture tag---></div>

    <script src="/js/src/JB.Global.js?v=1.5"></script>
    <script src="/js/src/JB.Subpages.js"></script>
    <script type="text/javascript" id="mpelid" src="//hola.jetblue.com/mpel/mpel.js"></script>

    <script type="text/javascript">
        ; (function () {
            function gup(name) {
                name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                var regexS = "[\\?&]" + name + "=([^&#]*)",
                    regex = new RegExp(regexS),
                    results = regex.exec(window.location.href);

                if (results == null) {
                    return "";
                }
                else {
                    return results[1];
                }
            }

            var regExp = /www-stg.jetblue.com/g,
                jbTagsUrl = window.location.href,
                app_param = gup('jbTags');

            if (regExp.test(jbTagsUrl) && app_param === "true") {
                document.write('<link rel="stylesheet" href="/jbTags/css/jbTags.css"> ');
                document.write("<scr" + "ipt src='/jbTags/js/jbtags.js'></sc" + "ript>");
                document.write("<div id='gContainer' class='ui-notify'><div class='ui-notify-message ui-notify-message-style'><a href='#' class='ui-notify-close ui-notify-cross'>x</a><h1 id='gHeader'>Sticky Notification</h1><p id='gBlurb'>JetBlue Tags 'sticky' notification. Click on the X above to close me.</p></div></div>");
            }
        })();
    </script>
</body>
</html>
