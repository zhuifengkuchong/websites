BookerWrapper.service('CheckinConfig', function(){
	this.messages={
		"errors":{
			"header": "Oops!",
			"INVALID_PNR": "Please enter your confirmation number. It should be six characters (letters and numbers).",
			"INVALID_DEPARTURE":"Please choose a valid departure city.",
			"INVALID_FIRST_NAME":"Please enter the first name of a customer on your itinerary.",
			"INVALID_LAST_NAME":"Please enter the last name of a customer on your itinerary."
		},
		"formcompletion":"You will be taken to a new page to complete the process."
	};
	this.labels={
		"description":"You can check in online up to 24 hours in advance, but no less than 90 minutes prior to your flight.",
		"legend":"Enter your flight information to check in.",
		"confimnumber":"Confirmation Number",
		"departurecity":"Departure City",
		"firstname":"First Name",
		"lastname":"Last Name"
	};
	this.trueblue = {
		"link": {
			"href": "https://book.jetblue.com/B6.myb",
			"text":"Get your boarding pass through your TrueBlue account"
		},
		"img":{
			"src":"../../../img/lrg-lft-arrow.gif"/*tpa=http://www.jetblue.com/img/lrg-lft-arrow.gif*/,
			"alt":""
		},
		"title":"Don't have your confirmation number?"
	};
	this.form = {
		"action":"https://checkin.jetblue.com/WCI/wci",
		"method":"POST",
		"submitButtonText":"Check In"
	};
	this.PNRExample = {
			"image" : "../../../img/pnr-example.jpg"/*tpa=http://www.jetblue.com/img/pnr-example.jpg*/,
			"headerText" : "PNR Example"
	}
});