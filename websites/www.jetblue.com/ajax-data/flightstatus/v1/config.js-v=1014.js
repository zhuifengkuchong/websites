BookerWrapper.service('FlightStatusConfig', function(){
	this.messages={
		searchTypeLegend: "Select flight status search criteria",
		errors:{
			INVALID_DEPARTURE:"Please choose a valid departure city.",
			INVALID_ARRIVAL:"Please choose a valid arrival city.",
			INVALID_FLIGHT_NUMBER: "Please enter a valid flight number."
		},
		citySelector:{
			FOOTER_TEXT_PARTNER_AIRLINES: "Flying on one of our partner airlines out of a city not shown here? <a href=\"http://www.jetblue.com/flying-on-jetblue/airline-partners/\">Click here</a>."
		}
	};
	this.scopeDefaults = {
			"all" : {
			},
			"flight-status" : {
				"form" :{
					"action" : ""
				}
			}			
	};
	this.searchTypes={
		"cities":{
			"name":"Cities",
			"value":"byCities",
			"label":"By cities",
			"legend":"Search by cities"
		},
		"flights":{
			"name":"Flight",
			"value":"byFlightNumber",
			"label":"By flight number",
			"legend":"Search by flight number"
		}
	};
	this.form = {
		"action":"http://www.jetblue.com/flightstatus/default.aspx",
		"method":"POST"
	};
});