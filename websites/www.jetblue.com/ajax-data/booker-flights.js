
// Loading dynamic city data from BookerData 1.0
// Force reloading city data
// Cache created at 11/6/2014 1:22:13 AM
// Start City Data Block

// Airport Data
var oABQ = {"code":"ABQ","name":"Albuquerque, NM (ABQ)","cc":"US","jb":true};
var oABZ = {"code":"ABZ","name":"Aberdeen, United Kingdom (ABZ)","cc":"GB","jb":false};
var oACC = {"code":"ACC","name":"Accra, Ghana (ACC)","cc":"GH","jb":false};
var oACE = {"code":"ACE","name":"Lanzarote, Spain (ACE)","cc":"ES","jb":false};
var oACK = {"code":"ACK","name":"Nantucket, MA (ACK)","cc":"US","jb":true};
var oADB = {"code":"ADB","name":"Izmir, Turkey (ADB)","cc":"TR","jb":false};
var oADD = {"code":"ADD","name":"Addis Ababa, Ethiopia (ADD)","cc":"ET","jb":false};
var oADL = {"code":"ADL","name":"Adelaide, Australia (ADL)","cc":"AU","jb":false};
var oAGP = {"code":"AGP","name":"Malaga (AGP)","cc":"ES","jb":false};
var oAKL = {"code":"AKL","name":"Auckland, New Zealand (AKL)","cc":"NZ","jb":false};
var oALB = {"code":"ALB","name":"Albany, NY (ALB)","cc":"US","jb":false};
var oAMD = {"code":"AMD","name":"Ahmedabad, India (AMD)","cc":"IN","jb":false};
var oAMM = {"code":"AMM","name":"Amman, Jordan (AMM)","cc":"JO","jb":false};
var oAMS = {"code":"AMS","name":"Amsterdam - Schiphol (AMS)","cc":"NL","jb":false};
var oANC = {"code":"ANC","name":"Anchorage, AK (ANC)","cc":"US","jb":true};
var oANU = {"code":"ANU","name":"Antigua, Antigua and Barbuda (ANU)","cc":"AG","jb":false};
var oAPL = {"code":"APL","name":"Nampula, Mozambique (APL)","cc":"MZ","jb":false};
var oARN = {"code":"ARN","name":"Stockholm (ARN)","cc":"SE","jb":false};
var oATH = {"code":"ATH","name":"Athens, Greece (ATH)","cc":"GR","jb":false};
var oAUA = {"code":"AUA","name":"Aruba (AUA)","cc":"AW","jb":true};
var oAUG = {"code":"AUG","name":"Augusta, ME (AUG)","cc":"US","jb":false};
var oAUS = {"code":"AUS","name":"Austin, TX (AUS)","cc":"US","jb":true};
var oAXA = {"code":"AXA","name":"Anguilla - Wall Blake (AXA)","cc":"AI","jb":false};
var oAZS = {"code":"AZS","name":"Samana, Dominican Republic (AZS)","cc":"DO","jb":true};
var oBAH = {"code":"BAH","name":"Bahrain (BAH)","cc":"BH","jb":false};
var oBBK = {"code":"BBK","name":"Kasane, Botswana (BBK)","cc":"BW","jb":false};
var oBCN = {"code":"BCN","name":"Barcelona (BCN)","cc":"ES","jb":false};
var oBDA = {"code":"BDA","name":"Bermuda (BDA)","cc":"BM","jb":true};
var oBDL = {"code":"BDL","name":"Hartford Springfield, CT (BDL)","cc":"US","jb":true};
var oBEW = {"code":"BEW","name":"Beira, Mozambique (BEW)","cc":"MZ","jb":false};
var oBFN = {"code":"BFN","name":"Bloemfontein, South Africa (BFN)","cc":"ZA","jb":false};
var oBGI = {"code":"BGI","name":"Bridgetown, Barbados (BGI)","cc":"BB","jb":true};
var oBGO = {"code":"BGO","name":"Bergen, Norway (BGO)","cc":"NO","jb":false};
var oBGW = {"code":"BGW","name":"Baghdad, Iraq (BGW)","cc":"IQ","jb":false};
var oBHB = {"code":"BHB","name":"Bar Harbor, ME (BHB)","cc":"US","jb":false};
var oBHD = {"code":"BHD","name":"Belfast, UK (BHD)","cc":"GB","jb":false};
var oBHX = {"code":"BHX","name":"Birmingham, United Kingdom (BHX)","cc":"GB","jb":false};
var oBID = {"code":"BID","name":"Block Island State Airport (BID)","cc":"US","jb":false};
var oBIM = {"code":"BIM","name":"Bimini, Bahamas (BIM)","cc":"BS","jb":false};
var oBIO = {"code":"BIO","name":"Bilbao (BIO)","cc":"ES","jb":false};
var oBKK = {"code":"BKK","name":"Bangkok (BKK)","cc":"TH","jb":false};
var oBLK = {"code":"BLK","name":"Blackpool, United Kingdom (BLK)","cc":"GB","jb":false};
var oBLL = {"code":"BLL","name":"Billund, Denmark (BLL)","cc":"DK","jb":false};
var oBLQ = {"code":"BLQ","name":"Bologna, Italy (BLQ)","cc":"IT","jb":false};
var oBLR = {"code":"BLR","name":"Bangalore, India (BLR)","cc":"IN","jb":false};
var oBLZ = {"code":"BLZ","name":"Blantyre, Malawi (BLZ)","cc":"MW","jb":false};
var oBNE = {"code":"BNE","name":"Brisbane, Australia (BNE)","cc":"AU","jb":false};
var oBOD = {"code":"BOD","name":"Bordeaux, France (BOD)","cc":"FR","jb":false};
var oBOG = {"code":"BOG","name":"Bogota, Colombia (BOG)","cc":"CO","jb":true};
var oBOH = {"code":"BOH","name":"Bournemouth, United Kingdom (BOH)","cc":"GB","jb":false};
var oBOJ = {"code":"BOJ","name":"Burgas, Bulgaria (BOJ)","cc":"BG","jb":false};
var oBOM = {"code":"BOM","name":"Mumbai, India (BOM)","cc":"IN","jb":false};
var oBOS = {"code":"BOS","name":"Boston, MA (BOS)","cc":"US","jb":true};
var oBQN = {"code":"BQN","name":"Aguadilla, PR (BQN)","cc":"PR","jb":true};
var oBRS = {"code":"BRS","name":"Bristol, United Kingdom (BRS)","cc":"GB","jb":false};
var oBRU = {"code":"BRU","name":"Brussels (BRU)","cc":"BE","jb":false};
var oBSR = {"code":"BSR","name":"Basra, Iraq (BSR)","cc":"IQ","jb":false};
var oBTV = {"code":"BTV","name":"Burlington, VT (BTV)","cc":"US","jb":true};
var oBUD = {"code":"BUD","name":"Budapest (BUD)","cc":"HU","jb":false};
var oBUF = {"code":"BUF","name":"Buffalo, NY (BUF)","cc":"US","jb":true};
var oBUQ = {"code":"BUQ","name":"Bulawayo, Zimbabwe (BUQ)","cc":"ZW","jb":false};
var oBUR = {"code":"BUR","name":"Burbank, CA (BUR)","cc":"US","jb":true};
var oBWI = {"code":"BWI","name":"Baltimore, MD (BWI)","cc":"US","jb":true};
var oCAI = {"code":"CAI","name":"Cairo, Egypt (CAI)","cc":"EG","jb":false};
var oCAN = {"code":"CAN","name":"Guangzhou, China (CAN)","cc":"CN","jb":false};
var oCCJ = {"code":"CCJ","name":"Kozhikode, India (CCJ)","cc":"IN","jb":false};
var oCCU = {"code":"CCU","name":"Kolkata, India (CCU)","cc":"IN","jb":false};
var oCDG = {"code":"CDG","name":"Paris - Charles de Gaulle (CDG)","cc":"FR","jb":false};
var oCGK = {"code":"CGK","name":"Jakarta, Indonesia (CGK)","cc":"ID","jb":false};
var oCHS = {"code":"CHS","name":"Charleston, SC (CHS)","cc":"US","jb":true};
var oCLT = {"code":"CLT","name":"Charlotte, NC (CLT)","cc":"US","jb":true};
var oCMB = {"code":"CMB","name":"Colombo, Sri Lanka (CMB)","cc":"LK","jb":false};
var oCOK = {"code":"COK","name":"Kochi, India (COK)","cc":"IN","jb":false};
var oCOO = {"code":"COO","name":"Cotonou, Benin (COO)","cc":"BJ","jb":false};
var oCPH = {"code":"CPH","name":"Copenhagen (CPH)","cc":"DK","jb":false};
var oCPT = {"code":"CPT","name":"Cape Town (CPT)","cc":"ZA","jb":false};
var oCPX = {"code":"CPX","name":"Culebra, PR (CPX)","cc":"PR","jb":false};
var oCRK = {"code":"CRK","name":"Angeles/Mabalacat, Philippines (CRK)","cc":"PH","jb":false};
var oCTA = {"code":"CTA","name":"Catania, Italy (CTA)","cc":"IT","jb":false};
var oCTG = {"code":"CTG","name":"Cartagena, Colombia (CTG)","cc":"CO","jb":true};
var oCUN = {"code":"CUN","name":"Cancun, Mexico (CUN)","cc":"MX","jb":true};
var oCUR = {"code":"CUR","name":"Curacao, Curacao (CUR)","cc":"CW","jb":true};
var oCWL = {"code":"CWL","name":"Cardiff, UK (CWL)","cc":"GB","jb":false};
var oDAC = {"code":"DAC","name":"Dhaka, Bangladesh (DAC)","cc":"BD","jb":false};
var oDAR = {"code":"DAR","name":"Dar Es Salaam, Tanzania (DAR)","cc":"TZ","jb":false};
var oDBV = {"code":"DBV","name":"Dubrovnik, Croatia (DBV)","cc":"HR","jb":false};
var oDCA = {"code":"DCA","name":"Washington-National, DC (DCA)","cc":"US","jb":true};
var oDEL = {"code":"DEL","name":"Delhi, India (DEL)","cc":"IN","jb":false};
var oDEN = {"code":"DEN","name":"Denver, CO (DEN)","cc":"US","jb":true};
var oDFW = {"code":"DFW","name":"Dallas/Fort Worth, TX (DFW)","cc":"US","jb":true};
var oDKR = {"code":"DKR","name":"Dakar, Senegal (DKR)","cc":"SN","jb":false};
var oDMM = {"code":"DMM","name":"Dammam, Saudi Arabia (DMM)","cc":"SA","jb":false};
var oDOH = {"code":"DOH","name":"Doha, Qatar (DOH)","cc":"QA","jb":false};
var oDOM = {"code":"DOM","name":"Dominica, Dominica (DOM)","cc":"DM","jb":false};
var oDTW = {"code":"DTW","name":"Detroit, MI (DTW)","cc":"US","jb":true};
var oDUB = {"code":"DUB","name":"Dublin (DUB)","cc":"IE","jb":false};
var oDUR = {"code":"DUR","name":"Durban (DUR)","cc":"ZA","jb":false};
var oDUS = {"code":"DUS","name":"Dusseldorf (DUS)","cc":"DE","jb":false};
var oDXB = {"code":"DXB","name":"Dubai, United Arab Emirates (DXB)","cc":"AE","jb":false};
var oEBB = {"code":"EBB","name":"Entebbe, Uganda (EBB)","cc":"UG","jb":false};
var oEDI = {"code":"EDI","name":"Edinburgh, UK (EDI)","cc":"GB","jb":false};
var oEIS = {"code":"EIS","name":"Tortola, Beef Island (EIS)","cc":"VG","jb":false};
var oELH = {"code":"ELH","name":"North Eleuthera, Bahamas (ELH)","cc":"BS","jb":false};
var oELS = {"code":"ELS","name":"East London, South Africa (ELS)","cc":"ZA","jb":false};
var oEWB = {"code":"EWB","name":"New Bedford, MA (EWB)","cc":"US","jb":false};
var oEWR = {"code":"EWR","name":"Newark, NJ (EWR)","cc":"US","jb":true};
var oEYW = {"code":"EYW","name":"Key West, FL (EYW)","cc":"US","jb":false};
var oEZE = {"code":"EZE","name":"Buenos Aires (EZE)","cc":"AR","jb":false};
var oFAO = {"code":"FAO","name":"Faro, Portugal (FAO)","cc":"PT","jb":false};
var oFBM = {"code":"FBM","name":"Lubumbashi, Democratic Republic of the Congo (FBM)","cc":"CD","jb":false};
var oFCO = {"code":"FCO","name":"Rome (Fiumicino) (FCO)","cc":"IT","jb":false};
var oFDF = {"code":"FDF","name":"Fort De France, Martinique (FDF)","cc":"MQ","jb":false};
var oFLL = {"code":"FLL","name":"Fort Lauderdale, FL (FLL)","cc":"US","jb":true};
var oFPO = {"code":"FPO","name":"Freeport, Bahamas (FPO)","cc":"BS","jb":false};
var oFRA = {"code":"FRA","name":"Frankfurt (FRA)","cc":"DE","jb":false};
var oGBE = {"code":"GBE","name":"Gaborone, Botswana (GBE)","cc":"BW","jb":false};
var oGCM = {"code":"GCM","name":"Grand Cayman (GCM)","cc":"KY","jb":true};
var oGGT = {"code":"GGT","name":"George Town, Bahamas (GGT)","cc":"BS","jb":false};
var oGHB = {"code":"GHB","name":"Governors Harbour, Bahamas (GHB)","cc":"BS","jb":false};
var oGIG = {"code":"GIG","name":"Rio De Janeiro (GIG)","cc":"BR","jb":false};
var oGLA = {"code":"GLA","name":"Glasgow, UK (GLA)","cc":"GB","jb":false};
var oGND = {"code":"GND","name":"Grenada, Grenada (GND)","cc":"GD","jb":false};
var oGNV = {"code":"GNV","name":"Gainesville, FL (GNV)","cc":"US","jb":false};
var oGOT = {"code":"GOT","name":"Gothenberg Landvetter, Sweden (GOT)","cc":"SE","jb":false};
var oGRJ = {"code":"GRJ","name":"George, South Africa (GRJ)","cc":"ZA","jb":false};
var oGRU = {"code":"GRU","name":"Sao Paulo - Guarulhos (GRU)","cc":"BR","jb":false};
var oGVA = {"code":"GVA","name":"Geneva (GVA)","cc":"CH","jb":false};
var oHAJ = {"code":"HAJ","name":"Hanover, Germany (HAJ)","cc":"DE","jb":false};
var oHAM = {"code":"HAM","name":"Hamburg, Germany (HAM)","cc":"DE","jb":false};
var oHDS = {"code":"HDS","name":"Hoedspruit, South Africa (HDS)","cc":"ZA","jb":false};
var oHEL = {"code":"HEL","name":"Helsinki (HEL)","cc":"FI","jb":false};
var oHKG = {"code":"HKG","name":"Hong Kong, Hong Kong (HKG)","cc":"HK","jb":false};
var oHKT = {"code":"HKT","name":"Phuket, Thailand (HKT)","cc":"TH","jb":false};
var oHND = {"code":"HND","name":"Tokyo (Haneda), Japan (HND)","cc":"JP","jb":false};
var oHNL = {"code":"HNL","name":"Oahu-Honolulu, HI (HNL)","cc":"US","jb":false};
var oHOU = {"code":"HOU","name":"Houston, TX (HOU)","cc":"US","jb":true};
var oHPN = {"code":"HPN","name":"Westchester County, NY (HPN)","cc":"US","jb":true};
var oHRE = {"code":"HRE","name":"Harare, Zimbabwe (HRE)","cc":"ZW","jb":false};
var oHYA = {"code":"HYA","name":"Hyannis/Cape Cod, MA (HYA)","cc":"US","jb":true};
var oHYD = {"code":"HYD","name":"Hyderabad, India (HYD)","cc":"IN","jb":false};
var oIAD = {"code":"IAD","name":"Washington, DC/Dulles (IAD)","cc":"US","jb":true};
var oIOM = {"code":"IOM","name":"Isle of Man, UK (IOM)","cc":"GB","jb":false};
var oISB = {"code":"ISB","name":"Islamabad, Pakistan (ISB)","cc":"PK","jb":false};
var oITO = {"code":"ITO","name":"Big Island-Hilo, HI (ITO)","cc":"US","jb":false};
var oJAX = {"code":"JAX","name":"Jacksonville, FL (JAX)","cc":"US","jb":true};
var oJED = {"code":"JED","name":"Jeddah, Saudi Arabia (JED)","cc":"SA","jb":false};
var oJER = {"code":"JER","name":"Jersey, United Kingdom (JER)","cc":"GB","jb":false};
var oJFK = {"code":"JFK","name":"New York City, NY (JFK)","cc":"US","jb":true};
var oJNB = {"code":"JNB","name":"Johannesburg (JNB)","cc":"ZA","jb":false};
var oKBL = {"code":"KBL","name":"Kabul, Afghanistan (KBL)","cc":"AF","jb":false};
var oKEF = {"code":"KEF","name":"Reykjavik (KEF)","cc":"IS","jb":false};
var oKHI = {"code":"KHI","name":"Karachi, Pakistan (KHI)","cc":"PK","jb":false};
var oKIM = {"code":"KIM","name":"Kimberley, South Africa (KIM)","cc":"ZA","jb":false};
var oKIN = {"code":"KIN","name":"Kingston, Jamaica (KIN)","cc":"JM","jb":true};
var oKIR = {"code":"KIR","name":"Kerry (KIR)","cc":"IE","jb":false};
var oKOA = {"code":"KOA","name":"Big Island-Kona, HI (KOA)","cc":"US","jb":false};
var oKRK = {"code":"KRK","name":"Krakow, Poland (KRK)","cc":"PL","jb":false};
var oKUL = {"code":"KUL","name":"Kuala Lumpur, Malaysia (KUL)","cc":"MY","jb":false};
var oKWI = {"code":"KWI","name":"Kuwait, Kuwait (KWI)","cc":"KW","jb":false};
var oLAD = {"code":"LAD","name":"Luanda, Angola (LAD)","cc":"AO","jb":false};
var oLAS = {"code":"LAS","name":"Las Vegas, NV (LAS)","cc":"US","jb":true};
var oLAX = {"code":"LAX","name":"Los Angeles, CA (LAX)","cc":"US","jb":true};
var oLEB = {"code":"LEB","name":"Lebanon, NH (LEB)","cc":"US","jb":false};
var oLED = {"code":"LED","name":"St. Petersburg, Russia (LED)","cc":"RU","jb":false};
var oLGA = {"code":"LGA","name":"New York-LaGuardia, NY (LGA)","cc":"US","jb":true};
var oLGB = {"code":"LGB","name":"Long Beach, CA (LGB)","cc":"US","jb":true};
var oLGW = {"code":"LGW","name":"London-Gatwick, United Kingdom (LGW)","cc":"GB","jb":false};
var oLHE = {"code":"LHE","name":"Lahore, Pakistan (LHE)","cc":"PK","jb":false};
var oLHR = {"code":"LHR","name":"London Heathrow (LHR)","cc":"GB","jb":false};
var oLIH = {"code":"LIH","name":"Kauai-Lihue, HI (LIH)","cc":"US","jb":false};
var oLIM = {"code":"LIM","name":"Lima, Peru (LIM)","cc":"PE","jb":true};
var oLIN = {"code":"LIN","name":"Milan - Linate (LIN)","cc":"IT","jb":false};
var oLIR = {"code":"LIR","name":"Liberia, Costa Rica (LIR)","cc":"CR","jb":true};
var oLIS = {"code":"LIS","name":"Lisbon, Portugal (LIS)","cc":"PT","jb":false};
var oLLW = {"code":"LLW","name":"Lilongwe, Malawi (LLW)","cc":"MW","jb":false};
var oLNY = {"code":"LNY","name":"Lanai, HI (LNY)","cc":"US","jb":false};
var oLOS = {"code":"LOS","name":"Lagos, Nigeria (LOS)","cc":"NG","jb":false};
var oLPA = {"code":"LPA","name":"Las Palmas, Spain (LPA)","cc":"ES","jb":false};
var oLRM = {"code":"LRM","name":"La Romana, DR (LRM)","cc":"DO","jb":true};
var oLUN = {"code":"LUN","name":"Lusaka (LUN)","cc":"ZM","jb":false};
var oLVI = {"code":"LVI","name":"Livingstone, Zambia (LVI)","cc":"ZM","jb":false};
var oLYS = {"code":"LYS","name":"Lyon, France (LYS)","cc":"FR","jb":false};
var oMAA = {"code":"MAA","name":"Chennai, India (MAA)","cc":"IN","jb":false};
var oMAD = {"code":"MAD","name":"Madrid (MAD)","cc":"ES","jb":false};
var oMAN = {"code":"MAN","name":"Manchester (MAN)","cc":"GB","jb":false};
var oMAZ = {"code":"MAZ","name":"Mayaguez, PR (MAZ)","cc":"PR","jb":false};
var oMBJ = {"code":"MBJ","name":"Montego Bay, Jamaica (MBJ)","cc":"JM","jb":true};
var oMCN = {"code":"MCN","name":"Macon, GA (MCN)","cc":"US","jb":false};
var oMCO = {"code":"MCO","name":"Orlando, FL (MCO)","cc":"US","jb":true};
var oMCT = {"code":"MCT","name":"Muscat, Oman (MCT)","cc":"OM","jb":false};
var oMDE = {"code":"MDE","name":"Medellin, Colombia (MDE)","cc":"CO","jb":true};
var oMED = {"code":"MED","name":"Madinah Mohammad bin Abdulaziz, Saudi Arabia (MED)","cc":"SA","jb":false};
var oMHH = {"code":"MHH","name":"Marsh Harbour, Bahamas (MHH)","cc":"BS","jb":false};
var oMKK = {"code":"MKK","name":"Hoolehua/Molokai, HI (MKK)","cc":"US","jb":false};
var oMLE = {"code":"MLE","name":"Male, Maldives (MLE)","cc":"MV","jb":false};
var oMNL = {"code":"MNL","name":"Manila, Philippines (MNL)","cc":"PH","jb":false};
var oMPM = {"code":"MPM","name":"Maputo, Mozambique (MPM)","cc":"MZ","jb":false};
var oMQP = {"code":"MQP","name":"Nelspruit/Kruger National Park, South Africa (MQP)","cc":"ZA","jb":false};
var oMRU = {"code":"MRU","name":"Mauritius (MRU)","cc":"MU","jb":false};
var oMSS = {"code":"MSS","name":"Massena, NY (MSS)","cc":"US","jb":false};
var oMSU = {"code":"MSU","name":"Maseru, Lesotho (MSU)","cc":"LS","jb":false};
var oMSY = {"code":"MSY","name":"New Orleans, LA (MSY)","cc":"US","jb":true};
var oMTS = {"code":"MTS","name":"Manzini, Swaziland (MTS)","cc":"SZ","jb":false};
var oMUB = {"code":"MUB","name":"Maun, Botswana (MUB)","cc":"BW","jb":false};
var oMUC = {"code":"MUC","name":"Munich (MUC)","cc":"DE","jb":false};
var oMVY = {"code":"MVY","name":"Martha's Vineyard, MA (MVY)","cc":"US","jb":true};
var oMXP = {"code":"MXP","name":"Milan - Malpensa (MXP)","cc":"IT","jb":false};
var oNAP = {"code":"NAP","name":"Naples, Italy (NAP)","cc":"IT","jb":false};
var oNAS = {"code":"NAS","name":"Nassau, Bahamas (NAS)","cc":"BS","jb":true};
var oNBO = {"code":"NBO","name":"Nairobi, Kenya (NBO)","cc":"KE","jb":false};
var oNCE = {"code":"NCE","name":"Nice, France (NCE)","cc":"FR","jb":false};
var oNCL = {"code":"NCL","name":"Newcastle, United Kingdom (NCL)","cc":"GB","jb":false};
var oNEV = {"code":"NEV","name":"Nevis (NEV)","cc":"KN","jb":false};
var oNLA = {"code":"NLA","name":"Ndola, Zambia (NLA)","cc":"ZM","jb":false};
var oNYC = {"code":"NYC","name":"New York City area (NYC)","cc":"US","jb":true};
var oOAK = {"code":"OAK","name":"Oakland, CA (OAK)","cc":"US","jb":true};
var oOGG = {"code":"OGG","name":"Maui-Kahului, HI (OGG)","cc":"US","jb":false};
var oOGS = {"code":"OGS","name":"Ogdensburg, NY (OGS)","cc":"US","jb":false};
var oORD = {"code":"ORD","name":"Chicago-O'Hare, IL (ORD)","cc":"US","jb":true};
var oORH = {"code":"ORH","name":"Worcester, MA (ORH)","cc":"US","jb":true};
var oORK = {"code":"ORK","name":"Cork, Ireland (ORK)","cc":"IE","jb":false};
var oOSL = {"code":"OSL","name":"Oslo, Norway (OSL)","cc":"NO","jb":false};
var oPAP = {"code":"PAP","name":"Port-au-Prince, Haiti (PAP)","cc":"HT","jb":true};
var oPBI = {"code":"PBI","name":"West Palm Beach, FL (PBI)","cc":"US","jb":true};
var oPDX = {"code":"PDX","name":"Portland, OR (PDX)","cc":"US","jb":true};
var oPER = {"code":"PER","name":"Perth, Australia (PER)","cc":"AU","jb":false};
var oPEW = {"code":"PEW","name":"Peshawar, Pakistan (PEW)","cc":"PK","jb":false};
var oPGF = {"code":"PGF","name":"Perpignan, France (PGF)","cc":"FR","jb":false};
var oPHL = {"code":"PHL","name":"Philadelphia, PA (PHL)","cc":"US","jb":true};
var oPHW = {"code":"PHW","name":"Phalaborwa, South Africa (PHW)","cc":"ZA","jb":false};
var oPHX = {"code":"PHX","name":"Phoenix, AZ (PHX)","cc":"US","jb":true};
var oPIT = {"code":"PIT","name":"Pittsburgh, PA (PIT)","cc":"US","jb":true};
var oPLS = {"code":"PLS","name":"Providenciales (PLS)","cc":"TC","jb":true};
var oPLZ = {"code":"PLZ","name":"Port Elizabeth (PLZ)","cc":"ZA","jb":false};
var oPMI = {"code":"PMI","name":"Palma de Mallorca/Majorca, Spain (PMI)","cc":"ES","jb":false};
var oPNS = {"code":"PNS","name":"Pensacola, FL (PNS)","cc":"US","jb":false};
var oPOL = {"code":"POL","name":"Pemba, Mozambique (POL)","cc":"MZ","jb":false};
var oPOP = {"code":"POP","name":"Puerto Plata, DR (POP)","cc":"DO","jb":true};
var oPOS = {"code":"POS","name":"Port of Spain, Trinidad and Tobago (POS)","cc":"TT","jb":true};
var oPRG = {"code":"PRG","name":"Prague, Czech Republic (PRG)","cc":"CZ","jb":false};
var oPSE = {"code":"PSE","name":"Ponce, PR (PSE)","cc":"PR","jb":true};
var oPTG = {"code":"PTG","name":"Polokwane, South Africa (PTG)","cc":"ZA","jb":false};
var oPTP = {"code":"PTP","name":"Pointe A Pitre, Guadeloupe (PTP)","cc":"GP","jb":false};
var oPUJ = {"code":"PUJ","name":"Punta Cana, DR (PUJ)","cc":"DO","jb":true};
var oPVC = {"code":"PVC","name":"Provincetown, MA (PVC)","cc":"US","jb":false};
var oPVD = {"code":"PVD","name":"Providence, RI (PVD)","cc":"US","jb":true};
var oPVG = {"code":"PVG","name":"Shanghai, China (PVG)","cc":"CN","jb":false};
var oPWM = {"code":"PWM","name":"Portland, ME (PWM)","cc":"US","jb":true};
var oPZB = {"code":"PZB","name":"Pietermaritzburg, South Africa (PZB)","cc":"ZA","jb":false};
var oRCB = {"code":"RCB","name":"Richards Bay, South Africa (RCB)","cc":"ZA","jb":false};
var oRDU = {"code":"RDU","name":"Raleigh/Durham, NC (RDU)","cc":"US","jb":true};
var oRIC = {"code":"RIC","name":"Richmond, VA (RIC)","cc":"US","jb":true};
var oRKD = {"code":"RKD","name":"Rockland, ME (RKD)","cc":"US","jb":false};
var oRNS = {"code":"RNS","name":"Rennes, France (RNS)","cc":"FR","jb":false};
var oROC = {"code":"ROC","name":"Rochester, NY (ROC)","cc":"US","jb":true};
var oRSW = {"code":"RSW","name":"Fort Myers, FL (RSW)","cc":"US","jb":true};
var oRUH = {"code":"RUH","name":"Riyadh, Saudi Arabia (RUH)","cc":"SA","jb":false};
var oRUT = {"code":"RUT","name":"Rutland, VT (RUT)","cc":"US","jb":false};
var oSAN = {"code":"SAN","name":"San Diego, CA (SAN)","cc":"US","jb":true};
var oSAV = {"code":"SAV","name":"Savannah / Hilton Head (SAV)","cc":"US","jb":true};
var oSCQ = {"code":"SCQ","name":"Santiago de Compostela, Spain (SCQ)","cc":"ES","jb":false};
var oSDQ = {"code":"SDQ","name":"Santo Domingo, DR (SDQ)","cc":"DO","jb":true};
var oSEA = {"code":"SEA","name":"Seattle, WA (SEA)","cc":"US","jb":true};
var oSEN = {"code":"SEN","name":"London, United Kingdom (SEN)","cc":"GB","jb":false};
var oSEZ = {"code":"SEZ","name":"Seychelles, Seychelles (SEZ)","cc":"SC","jb":false};
var oSFO = {"code":"SFO","name":"San Francisco, CA (SFO)","cc":"US","jb":true};
var oSGN = {"code":"SGN","name":"Ho Chi Minh City, Vietnam (SGN)","cc":"VN","jb":false};
var oSIN = {"code":"SIN","name":"Singapore, Singapore (SIN)","cc":"SG","jb":false};
var oSJC = {"code":"SJC","name":"San Jose, CA (SJC)","cc":"US","jb":true};
var oSJO = {"code":"SJO","name":"San Jose, Costa Rica (SJO)","cc":"CR","jb":true};
var oSJU = {"code":"SJU","name":"San Juan, PR (SJU)","cc":"PR","jb":true};
var oSKB = {"code":"SKB","name":"St. Kitts, Saint Kitts and Nevis (SKB)","cc":"KN","jb":false};
var oSKT = {"code":"SKT","name":"Sialkot, Pakistan (SKT)","cc":"PK","jb":false};
var oSLC = {"code":"SLC","name":"Salt Lake City, UT (SLC)","cc":"US","jb":true};
var oSLK = {"code":"SLK","name":"Saranac Lake, NY (SLK)","cc":"US","jb":false};
var oSLU = {"code":"SLU","name":"St. Lucia-Castries, Saint Lucia (SLU)","cc":"LC","jb":false};
var oSMF = {"code":"SMF","name":"Sacramento, CA (SMF)","cc":"US","jb":true};
var oSNN = {"code":"SNN","name":"Limerick - Shannon (SNN)","cc":"IE","jb":false};
var oSRQ = {"code":"SRQ","name":"Sarasota, FL (SRQ)","cc":"US","jb":true};
var oSTI = {"code":"STI","name":"Santiago, DR (STI)","cc":"DO","jb":true};
var oSTR = {"code":"STR","name":"Stuttgart, Germany (STR)","cc":"DE","jb":false};
var oSTT = {"code":"STT","name":"St. Thomas (STT)","cc":"VI","jb":true};
var oSTX = {"code":"STX","name":"St. Croix (STX)","cc":"VI","jb":true};
var oSVD = {"code":"SVD","name":"St. Vincent, Saint Vincent and Grenadines (SVD)","cc":"VC","jb":false};
var oSVG = {"code":"SVG","name":"Stavanger, Norway (SVG)","cc":"NO","jb":false};
var oSWF = {"code":"SWF","name":"Newburgh, NY (SWF)","cc":"US","jb":true};
var oSXF = {"code":"SXF","name":"Berlin-Schoenefeld, Germany (SXF)","cc":"DE","jb":false};
var oSXM = {"code":"SXM","name":"St. Maarten (SXM)","cc":"SX","jb":true};
var oSYD = {"code":"SYD","name":"Sydney, Australia (SYD)","cc":"AU","jb":false};
var oSYR = {"code":"SYR","name":"Syracuse, NY (SYR)","cc":"US","jb":true};
var oSZK = {"code":"SZK","name":"Skukuza, South Africa (SZK)","cc":"ZA","jb":false};
var oTCB = {"code":"TCB","name":"Treasure Cay, Bahamas (TCB)","cc":"BS","jb":false};
var oTET = {"code":"TET","name":"Tete, Mozambique (TET)","cc":"MZ","jb":false};
var oTLH = {"code":"TLH","name":"Tallahassee, FL (TLH)","cc":"US","jb":false};
var oTLS = {"code":"TLS","name":"Toulouse, France (TLS)","cc":"FR","jb":false};
var oTNR = {"code":"TNR","name":"Antananarivo, Madagascar (TNR)","cc":"MG","jb":false};
var oTPA = {"code":"TPA","name":"Tampa, FL (TPA)","cc":"US","jb":true};
var oTRD = {"code":"TRD","name":"Trondheim, Norway (TRD)","cc":"NO","jb":false};
var oTRV = {"code":"TRV","name":"Thiruvananthapuram, India (TRV)","cc":"IN","jb":false};
var oUTN = {"code":"UTN","name":"Upington, South Africa (UTN)","cc":"ZA","jb":false};
var oUTT = {"code":"UTT","name":"Umtata, South Africa (UTT)","cc":"ZA","jb":false};
var oUVF = {"code":"UVF","name":"St. Lucia (UVF)","cc":"LC","jb":true};
var oVCE = {"code":"VCE","name":"Venice, Italy (VCE)","cc":"IT","jb":false};
var oVFA = {"code":"VFA","name":"Victoria Falls, Zimbabwe (VFA)","cc":"ZW","jb":false};
var oVIE = {"code":"VIE","name":"Vienna, Austria (VIE)","cc":"AT","jb":false};
var oVIJ = {"code":"VIJ","name":"Virgin Gorda (VIJ)","cc":"VG","jb":false};
var oVNX = {"code":"VNX","name":"Vilankulos, Mozambique (VNX)","cc":"MZ","jb":false};
var oVQS = {"code":"VQS","name":"Vieques, PR (VQS)","cc":"PR","jb":false};
var oVRN = {"code":"VRN","name":"Verona, Italy (VRN)","cc":"IT","jb":false};
var oWAS = {"code":"WAS","name":"Washington D.C. area (WAS)","cc":"US","jb":true};
var oWAW = {"code":"WAW","name":"Warsaw, Poland (WAW)","cc":"PL","jb":false};
var oWDH = {"code":"WDH","name":"Windhoek (WDH)","cc":"NA","jb":false};
var oWVB = {"code":"WVB","name":"Walvis Bay, Namibia (WVB)","cc":"NA","jb":false};
var oXBO = {"code":"XBO","name":"Boston area","cc":"US","jb":true};
var oXDR = {"code":"XDR","name":"Dominican Republic southern coast area","cc":"DO","jb":true};
var oXFL = {"code":"XFL","name":"South Florida area","cc":"US","jb":true};
var oXSF = {"code":"XSF","name":"San Francisco area","cc":"US","jb":true};
var oZLA = {"code":"ZLA","name":"Los Angeles area","cc":"US","jb":true};
var oZRH = {"code":"ZRH","name":"Zurich (ZRH)","cc":"CH","jb":false};

// Country Data
var cAO = {
"code":"AO",
"name":"ANGOLA",
"airports":["LAD",0]
};
var cBJ = {
"code":"BJ",
"name":"BENIN",
"airports":["COO",0]
};
var cBW = {
"code":"BW",
"name":"BOTSWANA",
"airports":["BBK","GBE","MUB",0]
};
var cCD = {
"code":"CD",
"name":"CONGO, DEMOCRATIC REPUBLIC",
"airports":["FBM",0]
};
var cCG = {
"code":"CG",
"name":"CONGO",
"airports":[0]
};
var cDZ = {
"code":"DZ",
"name":"ALGERIA",
"airports":[0]
};
var cEG = {
"code":"EG",
"name":"EGYPT",
"airports":["CAI",0]
};
var cER = {
"code":"ER",
"name":"ERITREA",
"airports":[0]
};
var cET = {
"code":"ET",
"name":"ETHIOPIA",
"airports":["ADD",0]
};
var cGA = {
"code":"GA",
"name":"GABON",
"airports":[0]
};
var cGH = {
"code":"GH",
"name":"GHANA",
"airports":["ACC",0]
};
var cGQ = {
"code":"GQ",
"name":"EQUATORIAL GUINEA",
"airports":[0]
};
var cKE = {
"code":"KE",
"name":"KENYA",
"airports":["NBO",0]
};
var cLY = {
"code":"LY",
"name":"LIBYAN ARAB JAMAHIRIYA",
"airports":[0]
};
var cLS = {
"code":"LS",
"name":"LESOTHO",
"airports":["MSU",0]
};
var cMA = {
"code":"MA",
"name":"MOROCCO",
"airports":[0]
};
var cMG = {
"code":"MG",
"name":"MADAGASCAR",
"airports":["TNR",0]
};
var cMZ = {
"code":"MZ",
"name":"MOZAMBIQUE",
"airports":["APL","BEW","MPM","POL","TET","VNX",0]
};
var cMU = {
"code":"MU",
"name":"MAURITIUS",
"airports":["MRU",0]
};
var cMW = {
"code":"MW",
"name":"MALAWI",
"airports":["BLZ","LLW",0]
};
var cNA = {
"code":"NA",
"name":"NAMIBIA",
"airports":["WDH","WVB",0]
};
var cNG = {
"code":"NG",
"name":"NIGERIA",
"airports":["LOS",0]
};
var cOM = {
"code":"OM",
"name":"OMAN",
"airports":["MCT",0]
};
var cSD = {
"code":"SD",
"name":"SUDAN",
"airports":[0]
};
var cSN = {
"code":"SN",
"name":"SENEGAL",
"airports":["DKR",0]
};
var cSZ = {
"code":"SZ",
"name":"SWAZILAND",
"airports":["MTS",0]
};
var cSC = {
"code":"SC",
"name":"SEYCHELLES",
"airports":["SEZ",0]
};
var cTN = {
"code":"TN",
"name":"TUNISIA",
"airports":[0]
};
var cTZ = {
"code":"TZ",
"name":"TANZANIA",
"airports":["DAR",0]
};
var cUG = {
"code":"UG",
"name":"UGANDA",
"airports":["EBB",0]
};
var cYE = {
"code":"YE",
"name":"YEMEN",
"airports":[0]
};
var cZA = {
"code":"ZA",
"name":"SOUTH AFRICA",
"airports":["BFN","CPT","DUR","ELS","GRJ","HDS","JNB","KIM","MQP","PHW","PLZ","PTG","PZB","RCB","SZK","UTN","UTT",0]
};
var cZM = {
"code":"ZM",
"name":"ZAMBIA",
"airports":["LUN","LVI","NLA",0]
};
var cZW = {
"code":"ZW",
"name":"ZIMBABWE",
"airports":["BUQ","HRE","VFA",0]
};
var cAF = {
"code":"AF",
"name":"AFGHANISTAN",
"airports":["KBL",0]
};
var cBD = {
"code":"BD",
"name":"BANGLADESH",
"airports":["DAC",0]
};
var cCN = {
"code":"CN",
"name":"CHINA",
"airports":["CAN","PVG",0]
};
var cHK = {
"code":"HK",
"name":"HONG KONG",
"airports":["HKG",0]
};
var cID = {
"code":"ID",
"name":"INDONESIA",
"airports":["CGK",0]
};
var cIN = {
"code":"IN",
"name":"INDIA",
"airports":["AMD","BLR","BOM","CCJ","CCU","COK","DEL","HYD","MAA","TRV",0]
};
var cJP = {
"code":"JP",
"name":"JAPAN",
"airports":["HND",0]
};
var cKZ = {
"code":"KZ",
"name":"KAZAKSTAN",
"airports":[0]
};
var cKH = {
"code":"KH",
"name":"CAMBODIA",
"airports":[0]
};
var cKR = {
"code":"KR",
"name":"SOUTH KOREA",
"airports":[0]
};
var cLK = {
"code":"LK",
"name":"SRI LANKA",
"airports":["CMB",0]
};
var cMV = {
"code":"MV",
"name":"MALDIVES",
"airports":["MLE",0]
};
var cMY = {
"code":"MY",
"name":"MALAYSIA",
"airports":["KUL",0]
};
var cPK = {
"code":"PK",
"name":"PAKISTAN",
"airports":["ISB","KHI","LHE","PEW","SKT",0]
};
var cPH = {
"code":"PH",
"name":"PHILIPPINES",
"airports":["CRK","MNL",0]
};
var cSG = {
"code":"SG",
"name":"SINGAPORE",
"airports":["SIN",0]
};
var cTH = {
"code":"TH",
"name":"THAILAND",
"airports":["BKK","HKT",0]
};
var cTM = {
"code":"TM",
"name":"TURKMENISTAN",
"airports":[0]
};
var cUZ = {
"code":"UZ",
"name":"UZBEKISTAN",
"airports":[0]
};
var cVN = {
"code":"VN",
"name":"VIET NAM",
"airports":["SGN",0]
};
var cAW = {
"code":"AW",
"name":"ARUBA",
"airports":["AUA",0]
};
var cAI = {
"code":"AI",
"name":"ANGUILLA",
"airports":["AXA",0]
};
var cAG = {
"code":"AG",
"name":"ANTIGUA AND BARBUDA",
"airports":["ANU",0]
};
var cBS = {
"code":"BS",
"name":"BAHAMAS",
"airports":["BIM","ELH","FPO","GGT","GHB","MHH","NAS","TCB",0]
};
var cBZ = {
"code":"BZ",
"name":"BELIZE",
"airports":[0]
};
var cBM = {
"code":"BM",
"name":"BERMUDA",
"airports":["BDA",0]
};
var cBO = {
"code":"BO",
"name":"BOLIVIA",
"airports":[0]
};
var cBB = {
"code":"BB",
"name":"BARBADOS",
"airports":["BGI",0]
};
var cCR = {
"code":"CR",
"name":"COSTA RICA",
"airports":["LIR","SJO",0]
};
var cCW = {
"code":"CW",
"name":"CURACAO",
"airports":["CUR",0]
};
var cKY = {
"code":"KY",
"name":"CAYMAN ISLANDS",
"airports":["GCM",0]
};
var cDM = {
"code":"DM",
"name":"DOMINICA",
"airports":["DOM",0]
};
var cDO = {
"code":"DO",
"name":"DOMINICAN REPUBLIC",
"airports":["AZS","LRM","POP","PUJ","SDQ","STI","XDR",0]
};
var cGP = {
"code":"GP",
"name":"GUADELOUPE",
"airports":["PTP",0]
};
var cGD = {
"code":"GD",
"name":"GRENADA",
"airports":["GND",0]
};
var cGT = {
"code":"GT",
"name":"GUATEMALA",
"airports":[0]
};
var cHN = {
"code":"HN",
"name":"HONDURAS",
"airports":[0]
};
var cHT = {
"code":"HT",
"name":"HAITI",
"airports":["PAP",0]
};
var cJM = {
"code":"JM",
"name":"JAMAICA",
"airports":["KIN","MBJ",0]
};
var cKN = {
"code":"KN",
"name":"SAINT KITTS AND NEVIS",
"airports":["NEV","SKB",0]
};
var cLC = {
"code":"LC",
"name":"SAINT LUCIA",
"airports":["SLU","UVF",0]
};
var cMX = {
"code":"MX",
"name":"MEXICO",
"airports":["CUN",0]
};
var cMQ = {
"code":"MQ",
"name":"MARTINIQUE",
"airports":["FDF",0]
};
var cNI = {
"code":"NI",
"name":"NICARAGUA",
"airports":[0]
};
var cPA = {
"code":"PA",
"name":"PANAMA",
"airports":[0]
};
var cPR = {
"code":"PR",
"name":"PUERTO RICO",
"airports":["BQN","CPX","MAZ","PSE","SJU","VQS",0]
};
var cSV = {
"code":"SV",
"name":"EL SALVADOR",
"airports":[0]
};
var cSX = {
"code":"SX",
"name":"SAINT MAARTEN",
"airports":["SXM",0]
};
var cTC = {
"code":"TC",
"name":"TURKS AND CAICOS ISLANDS",
"airports":["PLS",0]
};
var cTT = {
"code":"TT",
"name":"TRINIDAD AND TOBAGO",
"airports":["POS",0]
};
var cVC = {
"code":"VC",
"name":"SAINT VINCENT AND THE GRENADINES",
"airports":["SVD",0]
};
var cVG = {
"code":"VG",
"name":"BRITISH VIRGIN ISLANDS",
"airports":["EIS","VIJ",0]
};
var cVI = {
"code":"VI",
"name":"U.S. VIRGIN ISLANDS",
"airports":["STT","STX",0]
};
var cAL = {
"code":"AL",
"name":"ALBANIA",
"airports":[0]
};
var cAT = {
"code":"AT",
"name":"AUSTRIA",
"airports":["VIE",0]
};
var cAZ = {
"code":"AZ",
"name":"AZERBAIJAN",
"airports":[0]
};
var cBE = {
"code":"BE",
"name":"BELGIUM",
"airports":["BRU",0]
};
var cBG = {
"code":"BG",
"name":"BULGARIA",
"airports":["BOJ",0]
};
var cBA = {
"code":"BA",
"name":"BOSNIA AND HERZEGOVINA",
"airports":[0]
};
var cBY = {
"code":"BY",
"name":"BELARUS",
"airports":[0]
};
var cCH = {
"code":"CH",
"name":"SWITZERLAND",
"airports":["GVA","ZRH",0]
};
var cCY = {
"code":"CY",
"name":"CYPRUS",
"airports":[0]
};
var cCZ = {
"code":"CZ",
"name":"CZECH REPUBLIC",
"airports":["PRG",0]
};
var cDE = {
"code":"DE",
"name":"GERMANY",
"airports":["DUS","FRA","HAJ","HAM","MUC","STR","SXF",0]
};
var cDK = {
"code":"DK",
"name":"DENMARK",
"airports":["BLL","CPH",0]
};
var cES = {
"code":"ES",
"name":"SPAIN",
"airports":["ACE","AGP","BCN","BIO","LPA","MAD","PMI","SCQ",0]
};
var cEE = {
"code":"EE",
"name":"ESTONIA",
"airports":[0]
};
var cFI = {
"code":"FI",
"name":"FINLAND",
"airports":["HEL",0]
};
var cFR = {
"code":"FR",
"name":"FRANCE",
"airports":["BOD","CDG","LYS","NCE","PGF","RNS","TLS",0]
};
var cGB = {
"code":"GB",
"name":"UNITED KINGDOM",
"airports":["ABZ","BHD","BHX","BLK","BOH","BRS","CWL","EDI","GLA","IOM","JER","LGW","LHR","MAN","NCL","SEN",0]
};
var cGE = {
"code":"GE",
"name":"GEORGIA",
"airports":[0]
};
var cGR = {
"code":"GR",
"name":"GREECE",
"airports":["ATH",0]
};
var cHR = {
"code":"HR",
"name":"CROATIA",
"airports":["DBV",0]
};
var cHU = {
"code":"HU",
"name":"HUNGARY",
"airports":["BUD",0]
};
var cIE = {
"code":"IE",
"name":"IRELAND",
"airports":["DUB","KIR","ORK","SNN",0]
};
var cIS = {
"code":"IS",
"name":"ICELAND",
"airports":["KEF",0]
};
var cIT = {
"code":"IT",
"name":"ITALY",
"airports":["BLQ","CTA","FCO","LIN","MXP","NAP","VCE","VRN",0]
};
var cLT = {
"code":"LT",
"name":"LITHUANIA",
"airports":[0]
};
var cLU = {
"code":"LU",
"name":"LUXEMBOURG",
"airports":[0]
};
var cLV = {
"code":"LV",
"name":"LATVIA",
"airports":[0]
};
var cMD = {
"code":"MD",
"name":"MOLDOVA",
"airports":[0]
};
var cMT = {
"code":"MT",
"name":"MALTA",
"airports":[0]
};
var cNL = {
"code":"NL",
"name":"NETHERLANDS",
"airports":["AMS",0]
};
var cNO = {
"code":"NO",
"name":"NORWAY",
"airports":["BGO","OSL","SVG","TRD",0]
};
var cPL = {
"code":"PL",
"name":"POLAND",
"airports":["KRK","WAW",0]
};
var cPT = {
"code":"PT",
"name":"PORTUGAL",
"airports":["FAO","LIS",0]
};
var cRO = {
"code":"RO",
"name":"ROMANIA",
"airports":[0]
};
var cRU = {
"code":"RU",
"name":"RUSSIAN FEDERATION",
"airports":["LED",0]
};
var cRS = {
"code":"RS",
"name":"SERBIA",
"airports":[0]
};
var cSE = {
"code":"SE",
"name":"SWEDEN",
"airports":["ARN","GOT",0]
};
var cTR = {
"code":"TR",
"name":"TURKEY",
"airports":["ADB",0]
};
var cUA = {
"code":"UA",
"name":"UKRAINE",
"airports":[0]
};
var cAE = {
"code":"AE",
"name":"UNITED ARAB EMIRATES",
"airports":["DXB",0]
};
var cBH = {
"code":"BH",
"name":"BAHRAIN",
"airports":["BAH",0]
};
var cIR = {
"code":"IR",
"name":"IRAN",
"airports":[0]
};
var cIQ = {
"code":"IQ",
"name":"IRAQ",
"airports":["BGW","BSR",0]
};
var cIL = {
"code":"IL",
"name":"ISRAEL",
"airports":[0]
};
var cJO = {
"code":"JO",
"name":"JORDAN",
"airports":["AMM",0]
};
var cKW = {
"code":"KW",
"name":"KUWAIT",
"airports":["KWI",0]
};
var cLB = {
"code":"LB",
"name":"LEBANON",
"airports":[0]
};
var cQA = {
"code":"QA",
"name":"QATAR",
"airports":["DOH",0]
};
var cSA = {
"code":"SA",
"name":"SAUDI ARABIA",
"airports":["DMM","JED","MED","RUH",0]
};
var cAS = {
"code":"AS",
"name":"AMERICAN SAMOA",
"airports":[0]
};
var cAU = {
"code":"AU",
"name":"AUSTRALIA",
"airports":["ADL","BNE","PER","SYD",0]
};
var cNZ = {
"code":"NZ",
"name":"NEW ZEALAND",
"airports":["AKL",0]
};
var cPF = {
"code":"PF",
"name":"FRENCH POLYNESIA",
"airports":[0]
};
var cAR = {
"code":"AR",
"name":"ARGENTINA",
"airports":["EZE",0]
};
var cBR = {
"code":"BR",
"name":"BRAZIL",
"airports":["GIG","GRU",0]
};
var cCL = {
"code":"CL",
"name":"CHILE",
"airports":[0]
};
var cCO = {
"code":"CO",
"name":"COLOMBIA",
"airports":["BOG","CTG","MDE",0]
};
var cEC = {
"code":"EC",
"name":"ECUADOR",
"airports":[0]
};
var cFK = {
"code":"FK",
"name":"FALKLAND ISLANDS (MALVINAS)",
"airports":[0]
};
var cPE = {
"code":"PE",
"name":"PERU",
"airports":["LIM",0]
};
var cUY = {
"code":"UY",
"name":"URUGUAY",
"airports":[0]
};
var cVE = {
"code":"VE",
"name":"VENEZUELA",
"airports":[0]
};
var cUS = {
"code":"US",
"name":"UNITED STATES",
"airports":["ABQ","ACK","ALB","ANC","AUG","AUS","BDL","BHB","BID","BOS","BTV","BUF","BUR","BWI","CHS","CLT","DCA","DEN","DFW","DTW","EWB","EWR","EYW","FLL","GNV","HNL","HOU","HPN","HYA","IAD","ITO","JAX","JFK","KOA","LAS","LAX","LEB","LGA","LGB","LIH","LNY","MCN","MCO","MKK","MSS","MSY","MVY","NYC","OAK","OGG","OGS","ORD","ORH","PBI","PDX","PHL","PHX","PIT","PNS","PVC","PVD","PWM","RDU","RIC","RKD","ROC","RSW","RUT","SAN","SAV","SEA","SFO","SJC","SLC","SLK","SMF","SRQ","SWF","SYR","TLH","TPA","WAS","XBO","XFL","XSF","ZLA","MCT2","BQN2","CPX2","MAZ2","PSE2","SJU2","VQS2","STT2","STX2",0]
};

// Extra Airport Data for US
var oMCT2 = {"code":"MCT","name":"Muscat, Oman (MCT)","cc":"US","jb":false,"duplicate": true};
var oBQN2 = {"code":"BQN","name":"Aguadilla, PR (BQN)","cc":"US","jb":true,"duplicate": true};
var oCPX2 = {"code":"CPX","name":"Culebra, PR (CPX)","cc":"US","jb":false,"duplicate": true};
var oMAZ2 = {"code":"MAZ","name":"Mayaguez, PR (MAZ)","cc":"US","jb":false,"duplicate": true};
var oPSE2 = {"code":"PSE","name":"Ponce, PR (PSE)","cc":"US","jb":true,"duplicate": true};
var oSJU2 = {"code":"SJU","name":"San Juan, PR (SJU)","cc":"US","jb":true,"duplicate": true};
var oVQS2 = {"code":"VQS","name":"Vieques, PR (VQS)","cc":"US","jb":false,"duplicate": true};
var oSTT2 = {"code":"STT","name":"St. Thomas (STT)","cc":"US","jb":true,"duplicate": true};
var oSTX2 = {"code":"STX","name":"St. Croix (STX)","cc":"US","jb":true,"duplicate": true};

// Route Data
var rABQ = ['ABZ','ACK','ADD','ALB','AMD','ANU','APL','AUA','AUG','AXA','AZS','BBK','BDA','BEW','BFN','BGI','BHB','BLR','BOM','BOS','BTV','BUF','BUQ','CCJ','CDG','CHS','CLT','CMB','COK','CPH','CPT','CPX','DAR','DEL','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','FLL','GBE','GCM','GND','GRJ','HDS','HRE','HYA','HYD','IAD','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KWI','LEB','LHR','LOS','LRM','LUN','LVI','MAA','MAN','MAZ','MCO','MLE','MPM','MQP','MRU','MSU','MTS','MUB','MVY','NAS','NBO','NEV','NLA','NYC','OGS','PAP','PBI','PHW','PLS','PLZ','POP','POS','PTG','PUJ','PVC','PWM','PZB','RCB','RDU','RKD','ROC','RUH','RUT','SAV','SDQ','SJU','SKB','SLK','SNN','STI','STT','STX','SVD','SXM','SYR','SZK','TNR','TPA','TRV','UTN','VFA','VIJ','VQS','WAS','WDH','WVB','XBO','XDR','XFL',0];
var rABZ = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','HYA','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SMF','SRQ','STI','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rACE = ['DEN','LAS','LAX','SAN','SEA','SFO','XSF','ZLA',0];
var rACK = ['ABQ','ABZ','ALB','AMD','AMS','ANC','APL','ARN','AUA','AUG','AUS','AXA','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BHB','BHX','BKK','BLR','BOD','BOG','BOM','BOS','BQN','BRS','BRU','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CUN','DAC','DCA','DEL','DEN','DFW','DMM','DOH','DTW','DUB','DUR','DXB','EDI','EIS','ELS','EWR','FDF','FLL','FRA','GBE','GLA','GND','GRJ','GVA','HDS','HEL','HNL','HOU','HPN','HRE','HYD','IAD','ISB','ITO','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIM','LIS','LNY','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MDE','MKK','MLE','MPM','MQP','MRU','MSS','MSU','MSY','MTS','MUB','MUC','MXP','NAS','NCE','NEV','NYC','OAK','OGG','OGS','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PHL','PHW','PHX','PIT','PLS','PLZ','POL','POS','PRG','PSE','PTG','PUJ','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SAV','SDQ','SEA','SEN','SEZ','SFO','SIN','SJC','SJU','SLC','SLK','SMF','SNN','SRQ','STI','STT','STX','SVD','SXM','SYD','SYR','SZK','TET','TNR','TPA','TRD','TRV','UTN','UTT','VFA','VIJ','VNX','VQS','WAS','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rADB = ['DCA','JFK','NYC','WAS',0];
var rADD = ['ABQ','BUF','BUR','CHS','DEN','FLL','HOU','JAX','LAS','LAX','LGB','MCO','MSY','OAK','ORD','PBI','PHX','ROC','RSW','SAN','SAV','SFO','SJC','SLC','SMF','TPA','XBO','XDR','XFL','XSF','ZLA',0];
var rAGP = ['BWI','DCA','DEN','DTW','EWR','JFK','LAS','MCO','PBI','RDU','SFO',0];
var rALB = ['ABQ','ACK','ANC','AUS','BDA','BGI','BOG','BOS','BQN','BUR','CHS','CLT','CPX','DCA','DEN','DFW','DTW','EIS','FLL','HOU','IAD','JAX','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHX','PSE','PUJ','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','STI','STT','STX','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rAMD = ['ABQ','ACK','AUA','AUS','BDA','BGI','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','LAS','LAX','LGB','LIR','MBJ','MCO','MSY','MVY','NAS','ORD','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SJU','SLC','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rAMS = ['ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SMF','SRQ','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rANC = ['ACK','ALB','AUG','AUS','BHB','BOS','EWB','HPN','HYA','JFK','LAS','LEB','LGB','MSS','MVY','NYC','OAK','PVC','PVD','RKD','RUT','SEA','SFO','SLC','SLK','SMF','XBO','XSF','ZLA',0];
var rANU = ['ABQ','AUS','BDL','BOS','BTV','BUF','BUR','BWI','DCA','DEN','EWR','FLL','HPN','IAD','JAX','JFK','LAS','LAX','LGA','LGB','MCO','NYC','OAK','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PUJ','PWM','RDU','RIC','ROC','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','STT','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rAPL = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rARN = ['ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SMF','SRQ','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL',0];
var rATH = ['DCA','JFK','MCO','XBO','XDR','XFL','XSF','ZLA',0];
var rAUA = ['ABQ','ABZ','ACK','AMD','AMS','ARN','AUG','BAH','BGO','BHB','BHD','BHX','BKK','BLK','BLR','BOM','BOS','BRS','BTV','BUF','BUR','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','DAC','DCA','DEL','DEN','DMM','DOH','DTW','DUB','DXB','EDI','EWR','FRA','GLA','HEL','HKG','HYA','HYD','IAD','ISB','JAX','JFK','KEF','KHI','KIR','KUL','LAS','LAX','LEB','LGB','LGW','LHE','LHR','MAA','MAN','MCT','MLE','MSY','MUC','MVY','MXP','NCE','NYC','OAK','ORD','OSL','PDX','PEW','PHL','PHX','PRG','PVC','PVG','PWM','RDU','RKD','ROC','RUH','RUT','SAN','SEA','SEZ','SFO','SIN','SJC','SLC','SLK','SMF','SYR','TRD','TRV','WAS','XBO','XSF','ZLA','ZRH',0];
var rAUG = ['ABQ','ACK','ANC','AUA','AUS','AXA','BDA','BGI','BOG','BOS','BQN','BUF','BUR','BWI','CHS','CLT','CPX','CUN','DCA','DEN','DFW','DTW','EIS','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POS','PSE','PUJ','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','STT','STX','SYR','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rAUS = ['ABZ','ACK','ALB','AMD','AMS','ANC','ANU','APL','ARN','AUG','AXA','AZS','BAH','BBK','BDA','BDL','BEW','BFN','BGI','BGO','BHB','BIM','BKK','BLR','BOG','BOM','BOS','BQN','BTV','BUF','BUQ','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CPT','CPX','CUN','CUR','DAC','DAR','DCA','DEL','DMM','DOH','DUB','DUR','DXB','EDI','EIS','ELH','ELS','EWR','EYW','FBM','FLL','FPO','FRA','GBE','GLA','GNV','GRJ','HDS','HEL','HKT','HPN','HRE','HYA','HYD','IAD','ISB','JAX','JED','JFK','JNB','KEF','KHI','KIM','KIN','KUL','KWI','LAS','LEB','LGA','LGB','LGW','LHE','LHR','LLW','LRM','LUN','LVI','MAA','MAN','MAZ','MCO','MCT','MDE','MED','MHH','MLE','MPM','MQP','MRU','MSU','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NEV','NLA','NYC','OAK','OGS','ORH','OSL','PAP','PDX','PEW','PHW','PLS','PLZ','POL','POP','POS','PTG','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SDQ','SEA','SEZ','SFO','SIN','SJU','SKB','SLC','SLK','SMF','SNN','STI','STT','STX','SWF','SXM','SYR','SZK','TCB','TET','TLH','TNR','TPA','TRV','UTN','UTT','VFA','VIJ','VNX','VQS','WAS','WDH','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rAXA = ['ABQ','ACK','AUG','AUS','BDL','BHB','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DTW','EWR','FLL','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHL','PHX','PIT','PUJ','PVC','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SLK','SMF','STI','SWF','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rAZS = ['ABQ','BKK','BLR','BOM','BOS','BTV','BUF','BUR','COK','DEN','DOH','DXB','EDI','HYD','JFK','KUL','LAS','LAX','LGB','LHE','MAA','NYC','OAK','PDX','PER','PHX','PWM','RDU','ROC','SAN','SEA','SFO','SIN','SJC','SLC','SMF','SYR','XBO','XSF','ZLA',0];
var rBAH = ['ACK','AUA','AUS','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MSY','MVY','NAS','OAK','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SMF','STI','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rBBK = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','DEN','HOU','JAX','JFK','LAS','LAX','LGB','MCO','NYC','OAK','ORD','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rBCN = ['BWI','DCA','DEN','DTW','EWR','JFK','LAS','PBI','RDU','SFO','WAS','XFL',0];
var rBDA = ['ABQ','ABZ','ACK','ALB','AMD','AMS','ARN','AUG','BGO','BHB','BHX','BIO','BKK','BLK','BLR','BOD','BOM','BOS','BRS','BRU','BTV','BUF','BUR','BWI','CCU','CDG','CMB','COK','CPH','DAC','DCA','DEL','DEN','DFW','DMM','DOH','DTW','DUB','DXB','EDI','EWB','EWR','FLL','FRA','GLA','HAJ','HEL','HYA','HYD','IAD','IOM','ISB','JAX','JER','JFK','KEF','KHI','KIR','KUL','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIN','LYS','MAA','MAN','MCO','MLE','MRU','MSS','MSY','MUC','MVY','NCE','NYC','OAK','OGS','OSL','PBI','PDX','PER','PHL','PHX','PIT','PRG','PVC','PVD','PVG','PWM','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SEA','SEN','SEZ','SFO','SIN','SJC','SLC','SLK','SMF','SNN','STR','SXF','SYR','TPA','TRV','VIE','VRN','WAS','XBO','XFL','XSF','ZLA','ZRH',0];
var rBDL = ['ANU','AUS','AXA','BGI','BIM','BOG','BQN','CHS','CPX','CTG','CUN','DCA','DOM','EIS','ELH','EYW','FDF','FLL','FPO','GGT','GHB','GND','GNV','JAX','KIN','LAS','LAX','LIM','LRM','MAZ','MBJ','MCO','MDE','MHH','NAS','NEV','PBI','PNS','POS','PSE','PTP','PUJ','RSW','SDQ','SFO','SJO','SJU','SKB','SLC','SLU','STI','STT','STX','SVD','SXM','TCB','TLH','TPA','VIJ','VQS','WAS','XDR','XFL','XSF','ZLA',0];
var rBEW = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rBFN = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','HYA','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','RSW','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rBGI = ['ABQ','ACK','ALB','AMD','AUG','BDL','BHB','BKK','BLR','BOM','BOS','BTV','BUF','BUR','BWI','CCJ','CCU','CGK','CMB','COK','DAC','DCA','DEL','DEN','DTW','DXB','EWR','FLL','HPN','HYD','IAD','JAX','JFK','KHI','KUL','LAS','LAX','LEB','LGA','LGB','MAA','MCO','MCT','MLE','MVY','NYC','OAK','OGS','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PUJ','PVC','PVD','PVG','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SIN','SJC','SJU','SLC','SLK','SMF','STT','STX','SYR','TPA','TRV','WAS','XBO','XSF','ZLA',0];
var rBGO = ['ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MCO','MSY','NAS','ORD','PAP','PBI','PHL','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJU','SRQ','STI','SYR','TPA',0];
var rBGW = ['HOU','MSY',0];
var rBHB = ['ABQ','ACK','ANC','AUA','AUS','AXA','BDA','BGI','BOG','BOS','BQN','BUF','BUR','BWI','CHS','CLT','CPX','CUN','DCA','DEN','DFW','DTW','EIS','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POS','PSE','PUJ','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','STI','STT','STX','SYR','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rBHD = ['AUA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','EWR','FLL','HOU','IAD','JFK','LAS','LAX','MCO','MSY','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SAN','SAV','SFO','SJU','TPA','WAS','XFL',0];
var rBHX = ['ACK','AUA','BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MCO','MSY','NYC','ORD','PBI','PDX','PHL','PHX','PIT','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','STI','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rBID = ['FLL','MCO','PVD',0];
var rBIM = ['AUS','BDL','BOS','DCA','EWR','HPN','JAX','JFK','KIN','LAS','LAX','LGA','LIM','MDE','NYC','ORH','PIT','RIC','SDQ','SFO','SJU','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rBIO = ['BDA','BUF','BWI','DCA','DFW','EWR','FLL','HOU','IAD','JFK','LAX','MCO','ORD','PHL','PIT','RDU','RIC','RSW','SAV','SFO','TPA',0];
var rBKK = ['ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rBLK = ['AUA','BDA','BUF','BWI','CHS','DCA','DEN','DFW','EWR','FLL','HOU','JFK','LAS','LAX','MCO','MSY','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SAN','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rBLL = ['BUF','BWI','DCA','FLL','JFK','LAX','MCO','RSW','SDQ','SFO','SJU','STI','TPA',0];
var rBLQ = ['BUF','BWI','DCA','DFW','EWR','FLL','IAD','JFK','MCO','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SFO','TPA','WAS','XFL','XSF',0];
var rBLR = ['ABQ','ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SJU','SLC','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rBLZ = ['BOS','BTV','BUF','FLL','LAS','LAX','MCO','PWM','ROC','SYR',0];
var rBNE = ['BOS','JFK','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rBOD = ['ACK','BDA','BUF','BWI','CHS','DCA','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rBOG = ['ABZ','ACK','ALB','AMD','AMS','ARN','AUG','AUS','BAH','BDL','BGO','BHB','BKK','BLR','BOM','BOS','BUF','CCJ','CCU','CDG','CGK','CMB','COK','CPH','DAC','DCA','DEL','DMM','DOH','DUB','DXB','EDI','EWR','EYW','FLL','FPO','FRA','GLA','GNV','HEL','HKG','HPN','HYA','HYD','ISB','JAX','JFK','KEF','KHI','KUL','KWI','LAS','LAX','LEB','LGA','LGW','LHE','LHR','MAA','MAN','MCO','MCT','MHH','MLE','MVY','MXP','NYC','OGS','ORH','OSL','PEW','PNS','PVC','PVD','PVG','RIC','RKD','RSW','RUH','RUT','SFO','SIN','SLK','SWF','SYR','TLH','TPA','TRV','WAS','XBO','XFL','XSF','ZLA',0];
var rBOH = ['BUF','BWI','CHS','DCA','EWR','FLL','HOU','JFK','LAX','MCO','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rBOJ = ['DCA','MCO','WAS',0];
var rBOM = ['ABQ','ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','HYA','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SRQ','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rBOS = ['ABQ','ABZ','ACK','ALB','AMD','AMS','ANC','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BHB','BIM','BKK','BLR','BLZ','BNE','BOG','BOM','BQN','BRU','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTG','CUN','CUR','DAC','DCA','DEL','DEN','DFW','DKR','DMM','DOH','DOM','DTW','DUB','DUR','DXB','EDI','EIS','ELH','ELS','EWR','EYW','FDF','FLL','FPO','FRA','GBE','GCM','GGT','GHB','GLA','GND','GNV','GRJ','GVA','HDS','HEL','HKG','HKT','HNL','HOU','HRE','HYA','HYD','IAD','ISB','ITO','JAX','JFK','JNB','KBL','KEF','KHI','KIM','KIN','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIM','LIR','LLW','LNY','LRM','LUN','LVI','MAA','MAN','MAZ','MBJ','MCN','MCO','MCT','MDE','MHH','MKK','MLE','MPM','MQP','MRU','MSU','MSY','MTS','MUB','MUC','MXP','NAS','NEV','NLA','NYC','OAK','OGG','OGS','ORD','OSL','PAP','PBI','PDX','PER','PEW','PHL','PHW','PHX','PIT','PLS','PLZ','PNS','POL','POP','POS','PSE','PTG','PTP','PUJ','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SAV','SDQ','SEA','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SKT','SLC','SLK','SMF','SNN','SRQ','STI','STT','STX','SVD','SVG','SXM','SYD','SYR','SZK','TCB','TET','TLH','TNR','TPA','TRD','TRV','UTN','UTT','UVF','VFA','VIJ','VNX','VQS','WAS','WDH','XDR','XFL','XSF','ZLA','ZRH',0];
var rBQN = ['ABZ','ACK','ALB','AMS','ARN','AUG','AUS','BDL','BGO','BHB','BKK','BOM','BOS','BTV','BUF','CDG','CHS','CLT','CMB','CPH','DCA','DEL','DOH','DUB','DXB','EDI','EWR','EYW','FLL','FRA','GLA','GNV','HEL','HOU','HPN','HYA','IAD','JAX','JFK','KEF','KHI','LAS','LAX','LEB','LGA','LGB','LGW','LHR','MAN','MCN','MCO','MSY','MUC','MVY','NYC','OGS','ORD','OSL','PHX','PNS','PVC','PVD','PVG','PWM','RDU','RKD','ROC','RSW','RUT','SAN','SAV','SEA','SFO','SLK','SNN','SWF','SYR','TLH','WAS','XBO','XSF','ZLA','ZRH',0];
var rBRS = ['ACK','AUA','BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MCO','NYC','ORD','PBI','PHL','PHX','PIT','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJU','STI','TPA','WAS','XDR','XFL','XSF','ZLA',0];
var rBRU = ['ACK','BDA','BOS','BUF','BWI','CHS','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','MSY','NYC','ORD','PBI','PHL','PIT','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJU','STI','TPA','WAS','XFL',0];
var rBSR = ['BUF','FLL','MCO','PBI','XFL',0];
var rBTV = ['ABQ','ABZ','AMD','AMS','ANU','APL','ARN','AUA','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BKK','BLR','BLZ','BOM','BQN','BUF','BUQ','BUR','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTG','CUN','CUR','DAC','DEL','DEN','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','FDF','FLL','FRA','GBE','GCM','GLA','GND','GRJ','GVA','HDS','HEL','HKG','HKT','HOU','HRE','HYA','HYD','IAD','ITO','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KOA','KUL','LAS','LAX','LGB','LHE','LHR','LIH','LIR','LLW','LRM','LUN','LVI','MAA','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSU','MSY','MTS','MUB','MUC','MXP','NAS','NEV','NLA','NYC','OAK','OGG','ORD','OSL','PBI','PDX','PER','PHW','PHX','PLS','PLZ','POL','POP','POS','PSE','PTG','PTP','PUJ','PZB','RCB','RDU','ROC','RSW','RUH','SAN','SAV','SDQ','SEA','SEZ','SFO','SGN','SIN','SJC','SJU','SKB','SKT','SLC','SMF','SNN','SRQ','STI','STT','STX','SVD','SXM','SZK','TET','TNR','TPA','TRV','UTN','UTT','UVF','VFA','VIJ','VNX','VQS','WAS','WDH','XDR','XFL','XSF','ZLA','ZRH',0];
var rBUD = ['BUF','BWI','DCA','EWR','FLL','JFK','MCO','ORD','PHL','PIT','RSW','SAV','SEA','SFO','TPA',0];
var rBUF = ['ABQ','ABZ','ACK','ADD','AMD','AMS','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLL','BLQ','BLR','BLZ','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BSR','BTV','BUD','BUQ','BUR','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTG','CUN','CUR','CWL','DAC','DAR','DBV','DEL','DEN','DFW','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','EYW','FBM','FCO','FDF','FLL','FRA','GBE','GCM','GLA','GND','GOT','GRJ','GVA','HAJ','HAM','HDS','HEL','HKG','HKT','HNL','HOU','HRE','HYA','HYD','IAD','IOM','ISB','ITO','JAX','JED','JER','JFK','JNB','KBL','KEF','KHI','KIM','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LED','LGB','LGW','LHE','LHR','LIH','LIN','LIR','LIS','LLW','LNY','LOS','LRM','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MHH','MKK','MLE','MPM','MQP','MRU','MSU','MSY','MTS','MUB','MUC','MVY','MXP','NAP','NAS','NBO','NCE','NCL','NEV','NLA','NYC','OAK','OGG','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PGF','PHW','PHX','PLS','PLZ','PMI','PNS','POL','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','RSW','RUH','SAN','SAV','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SKT','SLC','SMF','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SXF','SXM','SYD','SZK','TET','TLH','TLS','TNR','TPA','TRD','TRV','UTN','UTT','UVF','VCE','VFA','VIE','VIJ','VNX','VQS','VRN','WAS','WAW','WDH','WVB','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rBUQ = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rBUR = ['ACK','ADD','ALB','AMD','ANU','APL','AUA','AUG','AXA','BAH','BBK','BDA','BEW','BFN','BGI','BHB','BLR','BOM','BOS','BQN','BTV','BUF','BUQ','CCJ','CDG','CLT','CMB','COK','CPH','CPT','CPX','DAR','DMM','DOH','DOM','DUB','DUR','DXB','EIS','ELS','FLL','GBE','GND','GRJ','HDS','HRE','HYA','HYD','IAD','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KWI','LAS','LEB','LHR','LLW','LOS','LUN','LVI','MAA','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSU','MTS','MUB','MVY','NAS','NBO','NEV','NLA','NYC','OGS','PAP','PBI','PHW','PLS','PLZ','POL','POS','PTG','PTP','PUJ','PVC','PWM','PZB','RCB','RDU','RKD','ROC','RSW','RUH','RUT','SDQ','SJU','SKB','SLK','SNN','STI','STT','STX','SVD','SYR','SZK','TET','TNR','TRV','UTN','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL',0];
var rBWI = ['ABZ','ACK','AGP','AMD','AMS','ANU','ARN','AUA','AUG','AXA','BAH','BCN','BDA','BGI','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLL','BLQ','BLR','BOD','BOH','BOM','BOS','BRS','BRU','BUD','CCU','CDG','CGK','CMB','COK','CPH','CPX','CUN','CWL','DAC','DBV','DEL','DEN','DFW','DMM','DOH','DOM','DUB','DUS','DXB','EDI','EIS','EWB','FCO','FLL','FRA','GLA','GVA','HAJ','HAM','HEL','HKT','HOU','HYA','HYD','IOM','ISB','JAX','JED','JER','KEF','KHI','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIN','LIS','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MED','MLE','MRU','MSS','MSY','MUC','MVY','NAP','NAS','NBO','NCE','NCL','NEV','NYC','OAK','OGS','ORD','ORK','OSL','PBI','PDX','PER','PEW','PGF','PHX','PMI','PRG','PUJ','PVC','PVD','RKD','RSW','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJU','SLK','SNN','SRQ','STI','STR','STT','STX','SVG','SXF','TLS','TPA','TRD','TRV','VCE','VIE','VIJ','VQS','VRN','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rCCJ = ['ABQ','ACK','AUA','AUS','BGI','BOG','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MSY','NAS','OAK','ORD','PAP','PBI','PHX','POP','POS','PSE','PUJ','PWM','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SMF','STI','SXM','SYR','TPA','UVF','XBO','XDR','XFL','XSF','ZLA',0];
var rCCU = ['ACK','AUA','AUS','BDA','BGI','BOG','BOS','BTV','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','UVF','XBO','XDR','XFL','XSF','ZLA',0];
var rCDG = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SMF','SRQ','STI','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rCGK = ['ACK','AUA','AUS','BGI','BOG','BOS','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','UVF','XBO','XDR','XFL','XSF','ZLA',0];
var rCHS = ['ABQ','ABZ','ACK','ADD','ALB','AMD','AMS','APL','ARN','AUG','BAH','BBK','BDA','BDL','BEW','BFN','BGO','BHB','BHD','BHX','BKK','BLK','BLR','BOD','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUQ','BUR','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CPT','DAC','DAR','DCA','DEL','DEN','DMM','DOH','DTW','DUB','DUR','DXB','EDI','ELS','EWB','EWR','FRA','GBE','GLA','GRJ','GVA','HDS','HEL','HKG','HOU','HPN','HRE','HYA','HYD','IOM','ISB','ITO','JED','JER','JFK','JNB','KEF','KHI','KIM','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIN','LIS','LLW','LUN','LVI','LYS','MAA','MAN','MCT','MED','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NBO','NCE','NLA','NYC','OAK','OGG','OGS','ORD','ORK','OSL','PDX','PER','PEW','PHL','PHW','PHX','PLZ','POL','PRG','PTG','PVC','PVD','PWM','PZB','RCB','RKD','ROC','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SIN','SJC','SJU','SLC','SLK','SMF','SNN','STI','STR','SVG','SXF','SYR','SZK','TET','TLS','TNR','TRD','TRV','UTN','UTT','VFA','VIE','VNX','VRN','WAS','XBO','XDR','XSF','ZLA','ZRH',0];
var rCLT = ['ABQ','ABZ','ACK','ALB','AMD','AMS','ARN','AUG','BAH','BDA','BGO','BHB','BHD','BHX','BKK','BLR','BOM','BOS','BQN','BRS','BTV','BUF','BUR','CCU','CDG','CGK','CMB','COK','CPH','CWL','DAC','DEL','DEN','DMM','DOH','DUB','DXB','EDI','EWB','EWR','FRA','GLA','HEL','HKG','HKT','HOU','HYA','HYD','IOM','ISB','ITO','JED','JFK','JNB','KEF','KHI','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIN','LIS','LYS','MAA','MAN','MCT','MED','MLE','MRU','MSS','MUC','MVY','NAS','NCE','NCL','NYC','OAK','OGG','OGS','ORD','ORK','OSL','PDX','PER','PEW','PHL','PHX','PLS','PRG','PUJ','PVC','PVD','PWM','RKD','ROC','RUH','RUT','SAN','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJU','SLC','SLK','SMF','SNN','STI','SXF','SYR','TRV','VIE','XBO','XDR','XSF','ZLA','ZRH',0];
var rCMB = ['ABQ','ACK','AUA','AUS','BDA','BGI','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SJU','SLC','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rCOK = ['ABQ','ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','OAK','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SMF','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rCPH = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SMF','SRQ','STI','SXM','SYR','TPA','WAS','XBO','XFL','XSF','ZLA',0];
var rCPT = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','DEN','HOU','HYA','IAD','JAX','JFK','LAS','LAX','LGB','MCO','MVY','OAK','ORD','PDX','PHX','PWM','RDU','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA',0];
var rCPX = ['ABQ','ACK','ALB','AUG','AUS','BDL','BHB','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DTW','EWR','FLL','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','MCO','MVY','NAS','OAK','OGS','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PUJ','PVC','PVD','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SLK','SMF','STI','SWF','SXM','SYR','TPA',0];
var rCRK = ['POS',0];
var rCTA = ['DCA','JFK','NYC','WAS',0];
var rCTG = ['AMD','BDL','BKK','BLR','BOM','BOS','BTV','BUF','BUR','CCU','CMB','COK','DAC','DCA','DEL','DEN','DMM','DOH','DXB','EDI','EWR','FLL','HPN','HYD','ISB','JAX','JFK','KHI','KUL','KWI','LAS','LAX','LGA','LGB','MAA','MCO','MCT','MLE','NYC','OAK','PDX','PIT','PWM','RDU','ROC','RUH','SAN','SEA','SEZ','SFO','SIN','SJC','SLC','SMF','SYR','TPA','TRV','WAS','XBO','XFL','XSF','ZLA',0];
var rCUN = ['ABZ','ACK','AMD','AMS','APL','ARN','AUG','BAH','BDL','BEW','BFN','BGO','BHB','BKK','BLR','BOM','BOS','BTV','BUF','BUQ','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','DAC','DCA','DEL','DMM','DOH','DTW','DUB','DUR','DXB','EDI','ELH','ELS','EWR','EYW','FLL','FPO','FRA','GBE','GLA','GNV','HDS','HEL','HPN','HRE','HYA','HYD','IAD','ISB','JAX','JED','JFK','JNB','KEF','KHI','KIM','KUL','KWI','LAS','LEB','LGA','LGW','LHE','LHR','LLW','MAA','MAN','MCO','MCT','MED','MLE','MPM','MQP','MRU','MSU','MTS','MUC','MVY','MXP','NYC','ORD','OSL','PEW','PHL','PHW','PIT','PLZ','PNS','POL','PTG','PVC','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SEA','SEZ','SFO','SIN','SJU','SKT','SLC','SLK','SNN','STT','SYR','SZK','TCB','TET','TLH','TNR','TPA','TRV','UTT','VNX','WAS','XBO','XFL','XSF',0];
var rCUR = ['ABQ','BOS','BTV','BUF','BUR','DEN','JAX','JFK','LAS','LAX','LGB','NYC','OAK','PDX','PHX','PWM','ROC','SAN','SEA','SFO','SJC','SLC','SMF','SYR','XBO','XSF','ZLA',0];
var rCWL = ['BUF','BWI','CLT','DCA','DEN','DFW','DTW','EWR','FLL','IAD','JAX','JFK','LAS','LAX','LGB','MCO','NYC','ORD','PBI','PHL','PHX','PIT','RDU','RIC','SAN','SEA','SFO','TPA','WAS','XFL',0];
var rDAC = ['ACK','AUA','AUS','BDA','BGI','BOG','BOS','BTV','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','UVF','XBO','XDR','XFL','XSF','ZLA',0];
var rDAR = ['ABQ','AUS','BUF','BUR','CHS','DEN','HOU','JAX','LAS','LAX','LGB','MCO','MSY','OAK','ORD','PDX','PHX','ROC','RSW','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XDR','XFL','XSF','ZLA',0];
var rDBV = ['BUF','BWI','DCA','DFW','EWR','FLL','IAD','JFK','MCO','ORD','PHL','PIT','RIC','RSW','TPA',0];
var rDCA = ['ABZ','ACK','ADB','AGP','ALB','AMD','AMS','ANU','ARN','ATH','AUA','AUG','AUS','AXA','BAH','BCN','BDA','BDL','BGI','BGO','BHB','BHD','BHX','BIM','BIO','BKK','BLK','BLL','BLQ','BLR','BOD','BOG','BOH','BOJ','BOM','BOS','BQN','BRS','BRU','BUD','CCU','CDG','CGK','CHS','CMB','COK','CPH','CPX','CTA','CUN','CWL','DAC','DBV','DEL','DEN','DFW','DMM','DOH','DOM','DUB','DUS','DXB','EDI','EIS','ELH','EWB','EWR','EYW','FAO','FCO','FDF','FLL','FPO','FRA','GGT','GHB','GLA','GND','GNV','GVA','HAJ','HAM','HEL','HKT','HOU','HYA','HYD','IOM','ISB','JAX','JED','JER','JFK','KEF','KHI','KIN','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIM','LIN','LIR','LIS','LRM','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MED','MHH','MLE','MRU','MSS','MSY','MUC','MVY','NAP','NAS','NBO','NCE','NCL','NEV','OAK','OGS','ORD','ORK','OSL','PBI','PDX','PER','PEW','PGF','PHX','PLS','PMI','PNS','POP','POS','PRG','PSE','PTP','PUJ','PVC','PVD','RKD','RNS','RSW','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SLC','SLK','SLU','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SXF','SXM','TCB','TLH','TPA','TRD','TRV','VCE','VIE','VIJ','VQS','VRN','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rDEL = ['ABQ','ACK','AUA','AUS','BDA','BGI','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SJU','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rDEN = ['ABZ','ACE','ACK','ADD','AGP','ALB','AMD','AMS','ANU','APL','ARN','AUA','AUG','AXA','AZS','BAH','BBK','BCN','BDA','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BLK','BLR','BOM','BOS','BRS','BRU','BTV','BUF','BUQ','BWI','CCJ','CDG','CHS','CMB','COK','CPH','CPT','CPX','CTG','CWL','DAR','DCA','DEL','DMM','DOH','DOM','DUB','DUR','DUS','DXB','EDI','EIS','ELS','EWB','EWR','FCO','FLL','FRA','GBE','GCM','GLA','GND','GRJ','GVA','HAM','HDS','HEL','HPN','HRE','HYA','HYD','IAD','IOM','JAX','JED','JFK','JNB','KEF','KHI','KIM','KIR','KWI','LEB','LGW','LHR','LIN','LIS','LLW','LOS','LRM','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','NAS','NBO','NCE','NCL','NEV','NLA','NYC','OGS','ORK','OSL','PBI','PHL','PHW','PIT','PLS','PLZ','POL','POP','POS','PRG','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SDQ','SEN','SEZ','SJU','SKB','SLK','SNN','STI','STT','STX','SVD','SXF','SXM','SYR','SZK','TET','TNR','TPA','TRV','UTN','UVF','VFA','VIE','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL',0];
var rDFW = ['ABZ','ACK','ALB','AMD','AMS','ARN','AUG','BAH','BDA','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLQ','BLR','BOD','BOM','BOS','BRS','BRU','BUF','BWI','CCU','CDG','CGK','CMB','COK','CPH','CWL','DAC','DBV','DCA','DEL','DMM','DOH','DTW','DUB','DXB','EDI','EWB','EWR','FRA','GLA','HAJ','HEL','HKT','HPN','HYA','HYD','IAD','IOM','ISB','JED','JER','JFK','KEF','KHI','KIR','KUL','KWI','LEB','LGW','LHE','LHR','LIN','LIS','LYS','MAA','MAN','MCT','MLE','MRU','MSS','MUC','MVY','NAP','NBO','NCE','NCL','NYC','OGS','ORK','OSL','PEW','PHL','PIT','PLS','PRG','PUJ','PVC','PVD','RIC','RKD','RUH','RUT','SCQ','SDQ','SEN','SEZ','SIN','SJU','SLK','SNN','STR','STT','SXF','SXM','TLS','TRV','VCE','VIE','VRN','WAS','WAW','XBO','XDR','ZRH',0];
var rDKR = ['BOS','JFK',0];
var rDMM = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SMF','STI','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rDOH = ['ABQ','ACK','AUA','AUS','AZS','BDA','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SMF','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rDOM = ['ABQ','BDL','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DTW','EWR','FLL','HPN','IAD','JAX','JFK','LAS','LAX','LGA','LGB','MCO','MVY','NYC','OAK','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PUJ','PVD','PWM','RIC','ROC','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','STT','STX','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rDTW = ['ABZ','ACK','AGP','ALB','AMD','AMS','ARN','AUA','AUG','AXA','BAH','BCN','BDA','BGI','BHB','BHX','BKK','BLR','BOD','BOM','BOS','BRS','BRU','CCU','CDG','CGK','CHS','CMB','COK','CPH','CPX','CUN','CWL','DAC','DEL','DFW','DMM','DOH','DOM','DUB','DUS','DXB','EDI','EIS','EWB','EWR','FCO','FLL','FRA','GLA','GVA','HAM','HEL','HKT','HYA','HYD','IOM','ISB','JED','JFK','KEF','KHI','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIN','LIS','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MLE','MRU','MSY','MUC','MVY','NAS','NBO','NCE','NCL','NEV','NYC','PBI','PER','PEW','PHL','PHX','PLS','PRG','PUJ','PVC','PVD','RKD','RSW','RUH','RUT','SAN','SEA','SEN','SEZ','SGN','SIN','SJU','SLK','SNN','SRQ','STT','STX','SXF','SXM','TPA','TRV','VIE','VIJ','VQS','XFL','ZLA','ZRH',0];
var rDUB = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rDUR = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','HYA','IAD','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','OAK','ORD','PBI','PDX','PHX','PLS','PWM','RDU','ROC','RSW','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA',0];
var rDUS = ['BWI','DCA','DEN','DTW','EWR','JFK','LAS','PBI','RDU','SFO',0];
var rDXB = ['ABQ','ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','HYA','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rEBB = ['LAX','LGB',0];
var rEDI = ['ABQ','ACK','AUA','AUS','AZS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','HYA','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POP','PSE','PUJ','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','SYR','TPA','WAS','XFL','XSF','ZLA',0];
var rEIS = ['ABQ','ACK','ALB','AUG','AUS','BDL','BHB','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DTW','EWR','FLL','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','MCO','MVY','NAS','NYC','OAK','OGS','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PUJ','PVC','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SLK','SMF','STI','SWF','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rELH = ['AUS','BDL','BOS','CUN','DCA','EWR','HPN','JAX','JFK','KIN','LAS','LAX','LGA','LIM','MBJ','MDE','NYC','ORH','PIT','PUJ','PVD','RDU','RIC','SFO','SJU','WAS','XBO','XFL','XSF','ZLA',0];
var rELS = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rEWB = ['ANC','BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MCO','MSY','NYC','OAK','ORD','PBI','PDX','PHL','PHX','PIT','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','STI','TPA','WAS','XFL','XSF','ZLA',0];
var rEWR = ['ABZ','ACK','AGP','AMD','AMS','ANU','ARN','AUA','AUG','AUS','AXA','BAH','BCN','BDA','BGI','BGO','BHB','BHD','BHX','BIM','BIO','BKK','BLK','BLQ','BLR','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BUD','BUF','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPX','CTG','CUN','CWL','DAC','DBV','DCA','DEL','DEN','DFW','DMM','DOH','DOM','DTW','DUB','DUS','DXB','EDI','EIS','ELH','EWB','EYW','FCO','FDF','FLL','FPO','FRA','GLA','GND','GNV','GVA','HAJ','HAM','HEL','HKT','HOU','HYA','HYD','IOM','ISB','JAX','JED','JER','KEF','KHI','KIN','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIM','LIN','LIS','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MDE','MHH','MLE','MRU','MSS','MSY','MUC','MVY','NAP','NAS','NBO','NCE','NCL','NEV','NYC','OAK','OGS','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PHX','PIT','PLS','PMI','PNS','POS','PRG','PSE','PTP','PUJ','PVC','PVG','RDU','RKD','RSW','RUH','RUT','SAN','SAV','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SLC','SLK','SLU','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SXF','SXM','TCB','TLH','TLS','TPA','TRD','TRV','VCE','VIE','VIJ','VQS','VRN','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rEYW = ['AUS','BDL','BOG','BOS','BQN','BUF','CUN','DCA','EWR','HPN','JAX','JFK','KIN','LAS','LAX','LGA','LIM','MBJ','MDE','NAS','NYC','ORH','PAP','PIT','POS','PSE','PUJ','PVD','RDU','RIC','SDQ','SFO','SJU','SLC','SWF','SYR','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rFAO = ['DCA','LAS',0];
var rFBM = ['AUS','BUF','LAS','LAX','LGB','ORD','ROC','SYR','XFL',0];
var rFCO = ['BUF','BWI','DCA','DEN','DTW','EWR','FLL','JFK','LAS','MCO','NYC','ORD','PBI','PHL','RDU','RSW','SEA','SFO','TPA','WAS','XFL',0];
var rFDF = ['ACK','BDL','BOS','BTV','BUF','DCA','EWR','FLL','JFK','LAS','LAX','MCO','MVY','NYC','ORD','PDX','PUJ','PWM','ROC','SDQ','SFO','STI','STT','STX','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rFLL = ['ABQ','ABZ','ACK','ADD','ALB','AMD','AMS','ANU','APL','ARN','AUG','AUS','AXA','BAH','BDA','BDL','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BID','BIO','BKK','BLK','BLL','BLQ','BLR','BLZ','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BSR','BTV','BUD','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CMB','COK','CPH','CPX','CTG','CUN','CWL','DAC','DBV','DCA','DEL','DEN','DMM','DOH','DOM','DTW','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FCO','FDF','FRA','GBE','GLA','GND','GRJ','GVA','HAJ','HAM','HDS','HEL','HKG','HKT','HNL','HPN','HRE','HYA','HYD','IAD','IOM','ISB','JAX','JED','JER','JFK','JNB','KEF','KHI','KIM','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGA','LGB','LGW','LHE','LHR','LIH','LIM','LIN','LIS','LLW','LNY','LRM','LVI','LYS','MAA','MAN','MAZ','MBJ','MCT','MDE','MED','MKK','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUC','MVY','MXP','NAP','NAS','NCE','NCL','NEV','NYC','OAK','OGG','OGS','ORD','ORH','ORK','OSL','PAP','PDX','PER','PEW','PHL','PHW','PHX','PIT','PLZ','PMI','POL','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVD','PVG','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SKT','SLC','SLK','SLU','SMF','SNN','STI','STR','STT','STX','SVD','SVG','SWF','SXF','SXM','SYD','SYR','SZK','TET','TLS','TNR','TRD','TRV','UTN','UTT','VCE','VFA','VIE','VIJ','VNX','VQS','VRN','WAS','WAW','XBO','XDR','XSF','ZLA','ZRH',0];
var rFPO = ['AUS','BDL','BOG','BOS','CUN','DCA','EWR','HPN','JAX','JFK','KIN','LAS','LAX','LGA','LIM','MBJ','MDE','NYC','ORH','PAP','PIT','POS','PUJ','PVD','RDU','RIC','SDQ','SFO','SJO','SJU','SWF','WAS','XBO','XDR','XFL','XSF',0];
var rFRA = ['ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PDX','PHL','PHX','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SMF','SRQ','STI','SXM','SYR','TPA',0];
var rGBE = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','HYA','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rGCM = ['ABQ','ABZ','AMD','APL','BAH','BEW','BFN','BKK','BLR','BOM','BOS','BTV','BUF','BWI','CCJ','CCU','CGK','CMB','COK','DAC','DCA','DEL','DEN','DMM','DOH','DTW','DUR','DXB','EDI','ELS','EWR','GBE','HDS','HYD','ISB','JFK','JNB','KHI','KUL','KWI','LHE','LHR','MAA','MAN','MCT','MLE','MPM','MQP','MRU','MSU','MTS','MXP','NYC','OAK','PDX','PEW','PHW','PTG','PVG','PWM','PZB','RCB','RDU','RIC','ROC','RUH','SEA','SEZ','SFO','SIN','SJC','SLC','SMF','SYR','SZK','TNR','TRV','UTT','VNX','WAS','XBO','XSF',0];
var rGGT = ['BDL','BOS','DCA','HPN','JAX','JFK','LAS','LAX','LGA','LIM','NYC','ORH','PIT','RIC','SFO','WAS','XBO','XFL','XSF','ZLA',0];
var rGHB = ['BDL','BOS','DCA','HPN','JAX','JFK','LAS','LAX','LGA','LIM','MDE','NYC','ORH','PIT','RIC','SFO','WAS','XBO','XFL','XSF','ZLA',0];
var rGLA = ['ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','NAS','NYC','ORD','PAP','PBI','PDX','PHL','PHX','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SRQ','STI','SYR','TPA','WAS','XFL','XSF','ZLA',0];
var rGND = ['ABQ','ACK','BDL','BOS','BTV','BUF','BUR','DCA','DEN','EWR','FLL','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SDQ','SEA','SFO','SJU','SLC','STT','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rGNV = ['AUS','BDL','BOG','BOS','BQN','CUN','DCA','EWR','HPN','JFK','KIN','LAX','LGA','LIM','MBJ','MDE','NAS','NYC','ORH','PAP','POS','PUJ','PVD','RIC','SDQ','SFO','SJO','SJU','SLC','SWF','WAS','XBO','XDR','XFL','XSF',0];
var rGOT = ['BUF','MCO',0];
var rGRJ = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','DEN','FLL','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rGVA = ['ACK','BOS','BTV','BUF','BWI','CHS','DCA','DEN','DTW','EWR','FLL','IAD','JAX','JFK','LAS','LAX','MCO','MSY','ORD','PBI','PHL','PIT','PWM','RDU','ROC','RSW','SDQ','SEA','SFO','STI','SYR','TPA',0];
var rHAJ = ['BDA','BUF','BWI','DCA','DFW','EWR','FLL','HOU','IAD','JFK','LAX','MCO','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rHAM = ['BUF','BWI','DCA','DEN','DTW','EWR','FLL','JAX','JFK','LAS','MCO','ORD','PBI','RDU','RSW','SDQ','SFO','SJU','STI','TPA',0];
var rHDS = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PLS','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rHEL = ['ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PHX','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SMF','SRQ','STI','SXM','SYR','TPA',0];
var rHKG = ['AUA','BOG','BOS','BTV','BUF','CHS','CLT','FLL','IAD','JAX','MCO','MVY','PBI','PWM','RDU','ROC','RSW','SAV','SDQ','SJU','STI','SYR','TPA','WAS','XBO','XDR','XFL',0];
var rHKT = ['AUS','BOS','BTV','BUF','BWI','CLT','DCA','DFW','DTW','EWR','FLL','IAD','JAX','JFK','MCO','MSY','NYC','ORD','PBI','PHL','PSE','PWM','RDU','RIC','ROC','SDQ','SJU','STI','SYR','TPA','WAS','XBO','XDR','XFL',0];
var rHNL = ['ACK','BOS','BUF','FLL','JFK','LGB','MCO','NYC','PBI','ROC','XBO','XFL','XSF','ZLA',0];
var rHOU = ['ABZ','ACK','ADD','ALB','AMD','AMS','APL','ARN','AUG','AZS','BAH','BBK','BDA','BEW','BFN','BGO','BGW','BHB','BHD','BHX','BIO','BKK','BLK','BLR','BOD','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUQ','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','DAC','DAR','DCA','DEL','DMM','DOH','DUB','DUR','DXB','EDI','ELS','EWB','EWR','FRA','GBE','GLA','GRJ','HAJ','HDS','HEL','HPN','HRE','HYA','HYD','IAD','IOM','ISB','JED','JER','JFK','JNB','KEF','KHI','KIM','KIR','KUL','KWI','LEB','LGW','LHE','LHR','LIN','LIS','LRM','LUN','LVI','LYS','MAA','MAN','MCT','MED','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NBO','NCE','NLA','NYC','OGS','ORD','ORK','OSL','PEW','PHL','PHW','PIT','PLS','PLZ','POP','PRG','PSE','PTG','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RUH','RUT','SCQ','SDQ','SEN','SEZ','SIN','SJU','SLK','SNN','STI','STR','SVG','SXF','SXM','SYR','SZK','TLS','TNR','TRD','TRV','UTN','UTT','VFA','VIE','VNX','VRN','WAS','WDH','XBO','XDR','ZRH',0];
var rHPN = ['ACK','ANC','ANU','AUS','AXA','BGI','BIM','BOG','BQN','CHS','CPX','CUN','DEN','DFW','DOM','EIS','ELH','EYW','FLL','FPO','GGT','GHB','GNV','HOU','JAX','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MHH','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHX','PNS','PSE','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJO','SJU','SLC','STI','STT','STX','TCB','TLH','TPA','VIJ','VQS','WAS','XDR','XFL','XSF','ZLA',0];
var rHRE = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rHYA = ['ABQ','ABZ','ANC','AUA','AUS','AXA','BDA','BFN','BGI','BOG','BOM','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CPT','CPX','CUN','DCA','DEN','DFW','DTW','DUR','DXB','EDI','EIS','EWR','FLL','GBE','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LHR','LIM','MAN','MAZ','MBJ','MCO','MDE','MQP','MSU','MSY','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POS','PSE','PUJ','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','STT','STX','SVD','SYR','TPA','VIJ','VQS','WAS','XDR','XFL','XSF','ZLA',0];
var rHYD = ['ABQ','ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SJU','SLC','STI','SXM','SYR','TPA','UVF',0];
var rIAD = ['ABQ','ABZ','ACK','ALB','AMD','AMS','ANU','ARN','AUA','AUG','AUS','AXA','BAH','BDA','BGI','BGO','BHB','BHD','BHX','BIO','BKK','BLQ','BLR','BOD','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUR','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CPT','CPX','CUN','CWL','DAC','DBV','DEL','DEN','DFW','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','EWB','FLL','FRA','GLA','GVA','HAJ','HEL','HKG','HKT','HOU','HYA','HYD','IOM','ISB','ITO','JAX','JED','JER','JFK','JNB','KEF','KHI','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIN','LIS','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MED','MLE','MRU','MSS','MSY','MUC','MVY','NAP','NAS','NBO','NCE','NCL','NEV','NYC','OAK','OGG','OGS','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PHX','PLS','PMI','PRG','PSE','PUJ','PVC','PVD','PWM','RKD','ROC','RSW','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJU','SLC','SLK','SMF','SNN','SRQ','STI','STR','STT','STX','SXF','SXM','SYR','TLS','TPA','TRV','VCE','VIE','VIJ','VQS','VRN','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rIOM = ['BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JFK','LAS','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAV','SFO','TPA','WAS','XFL',0];
var rISB = ['ACK','AUA','AUS','BDA','BOG','BOS','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PIT','PLS','POP','PSE','PUJ','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL',0];
var rITO = ['ACK','BOS','BTV','BUF','CHS','CLT','IAD','JFK','LGB','PWM','RDU','ROC','SAV','SYR','XBO','XSF','ZLA',0];
var rJAX = ['ABQ','ABZ','ACK','ADD','ALB','AMD','AMS','ANU','APL','ARN','AUA','AUG','AUS','AXA','BAH','BBK','BDA','BDL','BEW','BFN','BGI','BGO','BHB','BHX','BIM','BKK','BLR','BOD','BOG','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CPT','CPX','CUN','CUR','CWL','DAC','DAR','DCA','DEL','DEN','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELH','ELS','EWB','EWR','EYW','FLL','FPO','FRA','GBE','GGT','GHB','GLA','GND','GRJ','GVA','HAM','HDS','HEL','HKG','HKT','HPN','HRE','HYA','HYD','IAD','ISB','JED','JFK','JNB','KBL','KEF','KHI','KIM','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGA','LGB','LGW','LHE','LHR','LIH','LIS','LUN','LVI','LYS','MAA','MAN','MAZ','MCT','MDE','MED','MHH','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NCE','NCL','NEV','NLA','NYC','OAK','OGG','OGS','ORD','ORH','ORK','OSL','PAP','PDX','PER','PEW','PHL','PHW','PHX','PIT','PLZ','POL','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PWM','PZB','RCB','RIC','RKD','ROC','RUH','RUT','SAN','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SLC','SLK','SLU','SMF','SNN','STT','STX','SVD','SXM','SYR','SZK','TCB','TET','TNR','TRV','UTN','UVF','VFA','VIJ','VNX','VQS','WAS','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rJED = ['AUS','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','LGB','LIR','MCO','MSY','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAN','SAV','TPA','WAS','XFL','ZLA',0];
var rJER = ['BDA','BUF','BWI','CHS','DCA','DFW','EWR','FLL','HOU','IAD','JFK','LAX','MCO','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rJFK = ['ABQ','ABZ','ACK','ADB','AGP','AMD','AMS','ANC','ANU','APL','ARN','ATH','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BCN','BDA','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BIM','BIO','BKK','BLK','BLL','BLQ','BLR','BNE','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUD','BUF','BUQ','BUR','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTA','CTG','CUN','CUR','CWL','DAC','DBV','DCA','DEL','DEN','DFW','DKR','DMM','DOH','DOM','DTW','DUB','DUR','DUS','DXB','EDI','EIS','ELH','ELS','EWB','EYW','FCO','FDF','FLL','FPO','FRA','GBE','GCM','GGT','GHB','GLA','GND','GNV','GRJ','GVA','HAJ','HAM','HDS','HEL','HKT','HNL','HOU','HRE','HYA','HYD','IAD','IOM','ISB','ITO','JAX','JED','JER','JNB','KBL','KEF','KHI','KIM','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIM','LIN','LIR','LIS','LNY','LRM','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MDE','MHH','MKK','MLE','MPM','MQP','MRU','MSS','MSU','MSY','MTS','MUB','MUC','MVY','NAP','NAS','NBO','NCE','NCL','NEV','NLA','OAK','OGG','OGS','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PGF','PHW','PHX','PIT','PLS','PLZ','PMI','PNS','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVG','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SAV','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SLC','SLK','SMF','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SXF','SXM','SYD','SYR','SZK','TCB','TET','TLH','TLS','TPA','TRD','TRV','UTN','UVF','VCE','VFA','VIE','VIJ','VNX','VQS','VRN','WAS','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rJNB = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CLT','CUN','DEN','FLL','GCM','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MSY','MVY','NAS','OAK','ORD','PBI','PDX','PHX','PWM','RDU','ROC','RSW','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XDR','XFL','XSF','ZLA',0];
var rKBL = ['BOS','BUF','JAX','JFK','MCO','NYC','ORD','PBI','PSE','SDQ','STI','XBO','XDR','XFL',0];
var rKEF = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SMF','SRQ','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL',0];
var rKHI = ['ABQ','ACK','AUA','AUS','BDA','BGI','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rKIM = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rKIN = ['ABQ','ABZ','ACK','ALB','AMS','ARN','AUG','AUS','BAH','BDL','BHB','BIM','BKK','BLR','BOM','BOS','BTV','BUF','BUR','CCJ','CCU','CDG','CGK','CMB','COK','CPH','DAC','DCA','DEL','DEN','DMM','DOH','DUB','DXB','EDI','ELH','EWR','EYW','FLL','FPO','FRA','GLA','GNV','HEL','HPN','HYA','HYD','IAD','ISB','JFK','KEF','KHI','KUL','KWI','LAS','LAX','LEB','LGA','LGB','LHE','LHR','MAA','MAN','MCO','MCT','MHH','MLE','MQP','MRU','MTS','MVY','MXP','NYC','OAK','OGS','OSL','PBI','PDX','PEW','PHX','PNS','PVC','PVG','PWM','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SEA','SEZ','SFO','SIN','SLC','SLK','SNN','SYR','TCB','TLH','TPA','TRV','WAS','XBO','XFL','XSF','ZLA',0];
var rKIR = ['ACK','AUA','BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MCO','MSY','NYC','OAK','ORD','PBI','PDX','PHL','PHX','PIT','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','STI','TPA',0];
var rKOA = ['ACK','BOS','BTV','BUF','CHS','CLT','FLL','IAD','JAX','JFK','LGB','PBI','PWM','RDU','ROC','SAV','SYR','XBO','XSF','ZLA',0];
var rKUL = ['ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BTV','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL',0];
var rKWI = ['ABQ','ACK','AUS','BOG','BOS','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','RDU','RIC','ROC','RSW','SAN','SAV','STI','SYR','TPA','WAS','XBO','XFL','ZLA',0];
var rLAS = ['ABZ','ACE','ACK','ADD','AGP','ALB','AMD','AMS','ANC','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BCN','BDA','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BIM','BLK','BLR','BLZ','BOG','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUQ','BUR','BWI','CCJ','CDG','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTG','CUN','CUR','CWL','DAR','DCA','DEL','DMM','DOH','DOM','DTW','DUB','DUR','DUS','DXB','EDI','EIS','ELH','ELS','EWB','EWR','EYW','FAO','FBM','FCO','FDF','FLL','FPO','FRA','GBE','GGT','GHB','GLA','GND','GRJ','GVA','HAM','HDS','HEL','HPN','HRE','HYA','HYD','IAD','IOM','JAX','JED','JFK','JNB','KEF','KHI','KIM','KIN','KIR','KWI','LEB','LGA','LGB','LGW','LHR','LIN','LIS','LLW','LOS','LRM','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MHH','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NCE','NCL','NEV','NLA','NYC','OAK','OGS','ORK','OSL','PAP','PBI','PDX','PHL','PHW','PIT','PLS','PLZ','POL','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RUH','RUT','SAV','SDQ','SEA','SEN','SEZ','SFO','SJO','SJU','SKB','SLC','SLK','SMF','SNN','SRQ','STI','STT','STX','SVD','SWF','SXF','SXM','SYR','SZK','TCB','TET','TNR','TPA','TRV','UTN','UTT','UVF','VFA','VIE','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rLAX = ['ABZ','ACE','ACK','ADD','ALB','AMD','AMS','ANU','APL','ARN','AUA','AUG','AXA','AZS','BAH','BBK','BDA','BDL','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BIM','BIO','BLK','BLL','BLR','BLZ','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUQ','BWI','CCJ','CDG','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTG','CUN','CUR','CWL','DAR','DCA','DMM','DOH','DOM','DTW','DUB','DUR','DXB','EBB','EDI','EIS','ELH','ELS','EWB','EWR','EYW','FBM','FDF','FLL','FPO','FRA','GBE','GGT','GHB','GLA','GND','GNV','GRJ','GVA','HAJ','HDS','HEL','HPN','HRE','HYA','HYD','IAD','IOM','JAX','JED','JER','JFK','JNB','KEF','KHI','KIM','KIN','KIR','KWI','LEB','LGA','LGW','LHR','LIM','LIN','LIS','LLW','LOS','LRM','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MHH','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NCE','NCL','NEV','NLA','NYC','OGS','ORK','OSL','PBI','PHL','PHW','PIT','PLS','PLZ','POL','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SCQ','SDQ','SEN','SEZ','SJO','SJU','SKB','SLK','SLU','SNN','STI','STR','STT','STX','SVD','SVG','SWF','SXF','SXM','SYR','SZK','TCB','TET','TLH','TLS','TNR','TPA','TRD','TRV','UTN','UTT','UVF','VFA','VIE','VIJ','VNX','VQS','VRN','WAS','WDH','WVB','XBO','XDR','XFL','ZRH',0];
var rLEB = ['ABQ','ACK','ANC','AUA','AUS','AXA','BDA','BGI','BOG','BOS','BQN','BUF','BUR','BWI','CHS','CLT','CPX','CUN','DCA','DEN','DFW','DTW','EIS','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POS','PSE','PUJ','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','STT','STX','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rLED = ['BUF','MCO',0];
var rLGA = ['ANU','AUS','AXA','BGI','BIM','BOG','BQN','CPX','CTG','CUN','DOM','EIS','ELH','EYW','FLL','FPO','GGT','GHB','GNV','JAX','KIN','LAS','LAX','LIM','MAZ','MBJ','MCO','MHH','NAS','NEV','PAP','PBI','PNS','POS','PSE','PUJ','RSW','SDQ','SFO','SJO','SJU','SRQ','STT','STX','TCB','TLH','TPA','VIJ','VQS','XDR','XFL','XSF','ZLA',0];
var rLGB = ['ABZ','ACK','ADD','ALB','AMD','AMS','ANC','ANU','APL','ARN','AUA','AUG','AUS','AXA','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BHB','BHX','BLR','BOM','BOS','BQN','BRS','BTV','BUF','BUQ','BWI','CCJ','CDG','CHS','CLT','CMB','COK','CPH','CPT','CPX','CWL','DAR','DCA','DMM','DOH','DOM','DTW','DUB','DUR','DXB','EBB','EDI','EIS','ELS','EWB','EWR','FBM','FLL','FRA','GBE','GLA','GND','GRJ','HDS','HEL','HNL','HPN','HRE','HYA','HYD','IAD','ITO','JAX','JED','JFK','JNB','KEF','KHI','KIM','KIN','KIR','KOA','KWI','LAS','LEB','LGW','LHR','LIH','LLW','LOS','LUN','LVI','MAA','MAN','MAZ','MBJ','MCO','MCT','MKK','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MVY','MXP','NAS','NBO','NCL','NEV','NLA','NYC','OAK','OGG','OGS','OSL','PAP','PBI','PDX','PHL','PHW','PIT','PLS','PLZ','POL','POP','POS','PSE','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SDQ','SEA','SEZ','SFO','SJU','SKB','SLC','SLK','SMF','SNN','STI','STT','STX','SVD','SYR','SZK','TET','TNR','TRV','UTN','UTT','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','XSF',0];
var rLGW = ['ACK','AUA','AUS','BDA','BOG','BOS','BQN','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MCO','MSY','NAS','NYC','ORD','PBI','PHL','PHX','PIT','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJO','SJU','SRQ','STI','TPA','WAS','XBO',0];
var rLHE = ['ACK','AUA','AUS','AZS','BDA','BOG','BOS','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PBI','PHL','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL',0];
var rLHR = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','HYA','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJO','SJU','SLC','SMF','SRQ','STI','STT','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rLIH = ['ACK','BOS','BTV','BUF','CLT','FLL','IAD','JAX','JFK','LGB','MCO','NYC','PBI','PWM','RDU','ROC','SAV','SRQ','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rLIM = ['ACK','ALB','AUG','AUS','BDL','BHB','BIM','BOS','DCA','ELH','EWR','EYW','FLL','FPO','GGT','GHB','GNV','HPN','HYA','JAX','JFK','LEB','LGA','MCO','MHH','MVY','NYC','OGS','PBI','PNS','PVC','RDU','RIC','RKD','RSW','RUT','SLK','TCB','TLH','TPA','WAS','XBO','XFL',0];
var rLIN = ['BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JFK','LAS','LAX','MCO','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAV','SFO','TPA',0];
var rLIR = ['AMD','BKK','BLR','BOM','BOS','BTV','BUF','CCU','CMB','COK','DAC','DEL','DEN','DMM','DOH','DXB','HYD','ISB','JED','JFK','KHI','KUL','KWI','LGB','LHE','MAA','MLE','MRU','NYC','ORD','PDX','PHX','PWM','RDU','ROC','RUH','SAN','SEA','SEZ','SIN','SJC','SYR','TNR','TRV','XBO','XSF','ZLA',0];
var rLIS = ['ACK','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','MSY','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAN','SAV','SEA','SFO','TPA',0];
var rLLW = ['AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','LAS','LAX','LGB','MCO','ORD','PBI','PHX','PWM','ROC','SEA','SFO','SLC','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rLNY = ['ACK','BOS','BUF','FLL','JFK','MCO','NYC','XBO','XFL',0];
var rLOS = ['ABQ','BUF','BUR','DEN','LAS','LAX','LGB','MSY','OAK','ORD','PDX','SAN','SEA','SFO','SLC','XFL','XSF','ZLA',0];
var rLPA = ['SFO','XSF',0];
var rLRM = ['ABQ','ACK','AUS','BDL','BKK','BLR','BOM','BOS','BTV','BUF','BUR','CCU','CMB','COK','DAC','DCA','DEL','DEN','DMM','DOH','DXB','EDI','FLL','HYD','ISB','JFK','KHI','KUL','LAS','LAX','LGB','LHE','LHR','MAA','MCO','MLE','MSY','NYC','OAK','ORD','PDX','PER','PHX','PWM','RDU','ROC','RUH','SAN','SEA','SEZ','SFO','SIN','SJC','SLC','SMF','STT','SYR','TPA','TRV','XBO','XSF','ZLA',0];
var rLUN = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','DEN','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA',0];
var rLVI = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','DEN','FLL','HOU','JAX','JFK','LAS','LAX','LGB','MCO','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rLYS = ['ACK','BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAN','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rMAA = ['ABQ','ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SJU','SLC','STI','SXM','SYR','TPA','UVF',0];
var rMAN = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','HYA','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJO','SJU','SLC','SMF','SRQ','STI','SYR','TPA','WAS','XBO','XFL',0];
var rMAZ = ['ABQ','ACK','ALB','AUG','AUS','BDL','BHB','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DTW','EWR','FLL','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','MCO','MVY','NYC','OAK','OGS','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PVC','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SEA','SFO','SJC','SJU','SLC','SLK','SMF','STI','STT','STX','SWF','SXM','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rMBJ = ['ABZ','ACK','ALB','AMD','AUG','BAH','BDL','BHB','BKK','BLR','BOM','BOS','BTV','BUF','BUR','BWI','CCJ','CCU','CGK','CHS','CMB','COK','CPH','DAC','DCA','DEL','DEN','DMM','DOH','DUB','DUR','DXB','EDI','ELH','EWR','EYW','FLL','FPO','GNV','HDS','HPN','HYA','HYD','IAD','ISB','JAX','JFK','JNB','KHI','KUL','KWI','LAS','LAX','LEB','LGA','LGB','LHE','LHR','MAA','MAN','MCO','MCT','MLE','MPM','MQP','MRU','MTS','MVY','NYC','OAK','OGS','ORD','ORH','PDX','PHW','PHX','PIT','PNS','PTG','PVC','PVD','PVG','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SEA','SEZ','SFO','SIN','SJC','SJU','SLC','SLK','SMF','SWF','SYR','SZK','TCB','TLH','TNR','TPA','TRV','VNX','WAS','XBO','XSF','ZLA',0];
var rMCN = ['BOS','BQN','PSE','SJU','XBO',0];
var rMCO = ['ABQ','ABZ','ACK','ADD','AGP','ALB','AMD','AMS','ANU','APL','ARN','ATH','AUG','AUS','AXA','BAH','BBK','BDA','BDL','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BID','BIO','BKK','BLK','BLL','BLQ','BLR','BLZ','BOD','BOG','BOH','BOJ','BOM','BOS','BQN','BRS','BRU','BSR','BTV','BUD','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CPT','CPX','CUN','CWL','DAC','DAR','DBV','DCA','DEL','DEN','DMM','DOH','DOM','DTW','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FCO','FDF','FRA','GBE','GLA','GND','GOT','GRJ','GVA','HAJ','HAM','HDS','HEL','HKG','HKT','HNL','HPN','HRE','HYA','HYD','IAD','IOM','ISB','JED','JER','JFK','JNB','KBL','KEF','KHI','KIM','KIN','KIR','KUL','KWI','LAS','LAX','LEB','LED','LGA','LGB','LGW','LHE','LHR','LIH','LIM','LIN','LIS','LLW','LNY','LRM','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCT','MDE','MED','MKK','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NAP','NAS','NCE','NCL','NEV','NYC','OAK','OGG','OGS','ORD','ORH','ORK','OSL','PAP','PDX','PER','PEW','PGF','PHL','PHW','PHX','PIT','PLZ','PMI','POL','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RIC','RKD','ROC','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SKT','SLC','SLK','SLU','SMF','SNN','STI','STR','STT','STX','SVD','SVG','SWF','SXF','SXM','SYD','SYR','SZK','TET','TLS','TNR','TRD','TRV','UTN','UTT','UVF','VCE','VFA','VIE','VIJ','VNX','VQS','VRN','WAS','WAW','XBO','XDR','XSF','ZLA','ZRH',0];
var rMCT = ['ACK','AUA','AUS','BGI','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MSY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rMDE = ['ACK','ALB','AUG','AUS','BDL','BHB','BIM','BOS','DCA','ELH','EWR','EYW','FLL','FPO','GHB','GNV','HPN','HYA','JFK','LEB','LGA','MCO','MHH','MVY','NYC','OGS','PBI','PNS','PVC','RDU','RIC','RKD','RSW','RUT','SLK','TCB','TLH','TPA','WAS','XBO','XFL',0];
var rMED = ['AUS','BWI','CHS','CLT','CUN','DCA','FLL','HOU','IAD','JAX','MCO','PBI','PHL','RDU','RSW','SAV','TPA','WAS','XFL',0];
var rMHH = ['AUS','BDL','BOG','BOS','BUF','DCA','EWR','HPN','JAX','JFK','KIN','LAS','LAX','LGA','LIM','MDE','NYC','ORH','PAP','PIT','PVD','RDU','RIC','SDQ','SFO','SJU','SLC','SWF','SYR','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rMKK = ['ACK','BOS','BUF','FLL','JFK','LGB','MCO','NYC','PBI','ROC','XBO','XFL',0];
var rMLE = ['ABQ','ACK','AUA','AUS','BDA','BGI','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SMF','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rMNL = ['POS',0];
var rMPM = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PLS','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rMQP = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','HYA','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PLS','PWM','ROC','RSW','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rMRU = ['ABQ','ACK','AUS','BDA','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','MBJ','MCO','MSY','MVY','NAS','OAK','ORD','PBI','PDX','PHL','PHX','PIT','PLS','POP','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SEA','SFO','SJC','SLC','SMF','STI','SYR','TPA',0];
var rMSS = ['ACK','ANC','BDA','BWI','CHS','CLT','DCA','DEN','DFW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MCO','MSY','NYC','OAK','PBI','PDX','PHL','PHX','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','STI','TPA','WAS','XFL','XSF','ZLA',0];
var rMSU = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','HYA','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rMSY = ['ABZ','ACK','ADD','ALB','AMD','AMS','ARN','AUA','AUG','AZS','BAH','BDA','BGI','BGO','BGW','BHB','BHD','BHX','BKK','BLK','BLR','BOM','BOS','BQN','BRU','BTV','BUF','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CUR','DAC','DAR','DCA','DEL','DMM','DOH','DTW','DUB','DXB','EDI','EWB','EWR','FRA','GLA','GVA','HEL','HKT','HYA','HYD','IAD','ISB','JED','JFK','JNB','KEF','KHI','KIR','KUL','KWI','LEB','LGW','LHE','LHR','LIS','LOS','LRM','MAA','MAN','MCT','MLE','MRU','MSS','MUC','MVY','MXP','NBO','NYC','OGS','ORD','ORK','OSL','PHL','PIT','PLS','POP','POS','PRG','PSE','PUJ','PVC','PVD','PWM','RDU','RIC','RKD','ROC','RUH','RUT','SDQ','SEZ','SGN','SIN','SJU','SLK','SNN','STI','SVG','SXM','SYR','TRV','UVF','WAS','XBO','XDR','ZRH',0];
var rMTS = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','KIN','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PLS','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rMUB = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','DEN','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rMUC = ['ACK','AUA','AUS','BDA','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','MSY','NAS','ORD','PBI','PHL','PIT','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJU','SRQ','STI','SYR','TPA',0];
var rMVY = ['ABQ','ABZ','AMD','ANC','APL','ARN','AUA','AUS','AXA','BAH','BDA','BEW','BFN','BGI','BKK','BLR','BOG','BOM','BQN','BUF','BUQ','BUR','BWI','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CUN','DAC','DCA','DEL','DEN','DFW','DMM','DOH','DOM','DTW','DUB','DUR','DXB','EDI','EIS','ELS','EWR','FDF','FLL','FRA','GBE','GND','GRJ','HDS','HEL','HKG','HOU','HRE','HYD','IAD','ISB','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KUL','KWI','LAS','LAX','LGB','LHE','LHR','LIM','LUN','MAA','MAN','MAZ','MBJ','MCO','MDE','MLE','MPM','MQP','MRU','MSU','MSY','MTS','MUB','MXP','NAS','NEV','NLA','NYC','OAK','ORD','OSL','PAP','PBI','PDX','PER','PHL','PHW','PHX','PIT','PLS','PLZ','POS','PSE','PTG','PUJ','PZB','RCB','RDU','RIC','ROC','RSW','RUH','SAN','SAV','SDQ','SEA','SEZ','SFO','SIN','SJC','SJU','SLC','SMF','SRQ','STI','STT','STX','SVD','SYR','SZK','TNR','TPA','TRV','UTN','VFA','VIJ','VNX','VQS','WAS','WDH','XBO','XDR','XFL','XSF','ZLA',0];
var rMXP = ['ACK','AUA','AUS','BOG','BOS','BTV','BUF','CHS','CUN','FLL','GCM','HOU','JAX','KIN','LAS','LAX','LGB','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PLS','POP','PSE','PUJ','PWM','ROC','SDQ','SFO','SJU','STI','SXM','SYR','TPA','WAS','XBO','XFL','XSF','ZLA',0];
var rNAP = ['BUF','BWI','DCA','DFW','EWR','FLL','IAD','JFK','MCO','NYC','PHL','PIT','RIC','WAS','XFL',0];
var rNAS = ['ABQ','ABZ','ACK','ALB','AMD','AMS','APL','ARN','AUG','AUS','AXA','BAH','BDL','BEW','BFN','BGO','BHB','BKK','BLR','BOM','BOS','BTV','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CLT','CMB','COK','CPH','CPX','DAC','DCA','DEL','DEN','DMM','DOH','DTW','DUB','DUR','DXB','EDI','EIS','ELS','EWR','EYW','FLL','FRA','GBE','GLA','GNV','HDS','HEL','HPN','HRE','HYA','HYD','IAD','ISB','JAX','JFK','JNB','KEF','KHI','KIM','KUL','KWI','LAS','LAX','LEB','LGA','LGB','LGW','LHE','LHR','MAA','MAN','MCO','MCT','MLE','MPM','MQP','MRU','MSU','MTS','MUC','MVY','MXP','NEV','NYC','OAK','OGS','ORD','ORH','OSL','PDX','PER','PEW','PHL','PHW','PHX','PIT','PLZ','PNS','POL','PTG','PVC','PVD','PVG','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SEA','SEZ','SFO','SIN','SJC','SJU','SKT','SLC','SLK','SMF','SNN','STT','STX','SVG','SWF','SYR','SZK','TET','TLH','TNR','TPA','TRD','TRV','UTT','VIJ','VNX','VQS','WAS','XBO','XFL','XSF','ZLA','ZRH',0];
var rNBO = ['ABQ','AUS','BUF','BUR','BWI','CHS','DCA','DEN','DFW','DTW','EWR','HOU','IAD','JAX','JFK','LAS','LAX','LGB','MSY','OAK','ORD','PHL','PHX','RDU','RIC','ROC','SAN','SAV','SFO','SJC','SLC','SMF','SYR','TPA',0];
var rNCE = ['ACK','AUA','BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAN','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rNCL = ['BUF','BWI','CLT','DCA','DEN','DFW','DTW','EWR','FLL','IAD','JAX','JFK','LAS','LAX','LGB','MCO','NYC','ORD','PBI','PHL','PHX','PIT','RDU','RIC','SAN','SEA','SFO','TPA','WAS','XFL','XSF',0];
var rNEV = ['ABQ','ACK','AUG','AUS','BDL','BHB','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DTW','EWR','FLL','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','MCO','MVY','NAS','NYC','OAK','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PUJ','PVC','PWM','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SLK','SMF','STI','SWF','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rNLA = ['ABQ','AUS','BOS','BTV','BUF','BUR','CHS','DEN','HOU','JAX','JFK','LAS','LAX','LGB','MVY','NYC','OAK','ORD','PDX','PHX','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rNYC = ['ABQ','ABZ','ACK','ADB','ALB','AMS','ANC','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BBK','BDA','BEW','BFN','BGI','BHB','BHD','BHX','BIM','BLK','BLQ','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUQ','BUR','BWI','CDG','CHS','CLT','CPH','CTA','CTG','CUN','CUR','CWL','DEN','DFW','DMM','DOH','DOM','DTW','DUB','EDI','EIS','ELH','ELS','EWB','EWR','EYW','FCO','FDF','FLL','FPO','GBE','GCM','GGT','GHB','GLA','GND','GNV','GRJ','HAJ','HDS','HKT','HNL','HOU','HPN','HRE','HYA','IAD','IOM','ISB','JAX','JED','JER','KBL','KEF','KHI','KIM','KIN','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIM','LIR','LNY','LRM','LVI','LYS','MAN','MAZ','MBJ','MCO','MCT','MDE','MHH','MKK','MLE','MPM','MQP','MSS','MSU','MSY','MTS','MUB','MVY','MXP','NAP','NAS','NCE','NCL','NEV','NLA','OAK','OGG','OGS','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PGF','PHW','PHX','PIT','PLS','PMI','PNS','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVG','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SAV','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SLC','SLK','SLU','SMF','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SXF','SXM','SYD','SYR','SZK','TCB','TET','TLH','TLS','TPA','TRD','UTN','UVF','VCE','VFA','VIE','VNX','VQS','VRN','WAS','WAW','XBO','XDR','XFL','XSF','ZLA',0];
var rOAK = ['ACK','ADD','ALB','AMS','ANC','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BHB','BOS','BQN','BTV','BUF','BUQ','BWI','CCJ','CDG','CHS','CLT','COK','CPH','CPT','CPX','DAR','DCA','DMM','DOH','DOM','DUB','DUR','DXB','EIS','ELS','EWB','EWR','FLL','GBE','GCM','GND','GRJ','HDS','HPN','HRE','HYA','IAD','JAX','JFK','JNB','KEF','KIM','KIN','KIR','LAS','LEB','LGB','LHR','LOS','LUN','LVI','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MVY','NAS','NBO','NEV','NLA','NYC','OGS','PAP','PBI','PDX','PHL','PHW','PLS','PLZ','POL','POP','POS','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SDQ','SEA','SJU','SKB','SLC','SLK','SNN','SRQ','STI','STT','STX','SVD','SXM','SYR','SZK','TET','TNR','TPA','TRV','UTN','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','ZLA',0];
var rOGG = ['ACK','BOS','BTV','BUF','CHS','CLT','FLL','IAD','JAX','JFK','LGB','MCO','NYC','PBI','PWM','RDU','ROC','SAV','SYR','XBO','XFL','XSF','ZLA',0];
var rOGS = ['ABQ','ACK','AUS','BDA','BGI','BOG','BOS','BQN','BUR','BWI','CHS','CLT','CPX','DCA','DEN','DFW','EIS','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NYC','OAK','PAP','PBI','PDX','PHL','PHX','PSE','PUJ','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','STI','STT','STX','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rORD = ['ABZ','ACK','ADD','ALB','AMD','AMS','ANU','APL','ARN','AUA','AUG','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLQ','BLR','BOD','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUD','BUF','BUQ','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CUN','CUR','CWL','DAC','DAR','DBV','DCA','DEL','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FBM','FCO','FDF','FLL','FRA','GBE','GCM','GLA','GND','GRJ','GVA','HAJ','HAM','HDS','HEL','HKT','HOU','HPN','HRE','HYA','HYD','IAD','IOM','ISB','JAX','JED','JER','JFK','JNB','KBL','KEF','KHI','KIM','KIN','KIR','KUL','KWI','LEB','LGW','LHE','LHR','LIN','LIS','LLW','LOS','LRM','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSU','MSY','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NCE','NCL','NEV','NLA','NYC','ORK','OSL','PAP','PBI','PER','PEW','PHL','PHW','PLS','PLZ','POL','POP','POS','PRG','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SCQ','SDQ','SEA','SEN','SEZ','SIN','SJU','SKB','SLK','SLU','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SXF','SXM','SYR','SZK','TET','TLS','TNR','TPA','TRD','TRV','UTN','UTT','UVF','VCE','VFA','VIE','VIJ','VNX','VQS','VRN','WAS','WAW','WDH','WVB','XBO','XDR','XFL','ZRH',0];
var rORH = ['ANU','AUS','BGI','BIM','BOG','CPX','DOM','EIS','ELH','EYW','FLL','FPO','GGT','GHB','GNV','JAX','MAZ','MBJ','MCO','MHH','NEV','PBI','PNS','POS','PSE','PUJ','RSW','SDQ','SJO','SJU','STT','STX','TCB','TPA','VIJ','VQS','XDR','XFL',0];
var rORK = ['ACK','BUF','BWI','CHS','CLT','DCA','DEN','DFW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','MSY','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SAN','SAV','SFO','TPA','WAS','XFL','XSF','ZLA',0];
var rOSL = ['ACK','AUA','AUS','BDA','BOG','BOS','BQN','BTV','BUF','BWI','CHS','CLT','CUN','DCA','DEN','DFW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PIT','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJU','SRQ','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rPAP = ['ABQ','ABZ','ACK','ALB','AMS','ARN','AUG','BAH','BDL','BGO','BHB','BKK','BLR','BOM','BOS','BUR','CCJ','CCU','CDG','CGK','CMB','COK','CPH','DAC','DCA','DEL','DMM','DOH','DUB','DXB','EDI','EWR','EYW','FLL','FPO','FRA','GLA','GNV','HEL','HYA','HYD','ISB','JAX','JFK','KEF','KHI','KUL','KWI','LAS','LAX','LEB','LGA','LGB','LHR','MAA','MAN','MCO','MCT','MHH','MLE','MVY','MXP','NYC','OAK','OGS','OSL','PBI','PDX','PER','PEW','PIT','PNS','PVC','PVG','RDU','RKD','RSW','RUH','RUT','SAN','SEA','SEZ','SFO','SIN','SLC','SLK','SNN','TCB','TLH','TPA','TRV','WAS','XBO','XSF','ZLA',0];
var rPBI = ['ABQ','ABZ','ACK','ADD','AGP','ALB','AMD','AMS','ANU','APL','ARN','AUG','AXA','BAH','BCN','BDA','BDL','BEW','BFN','BGI','BGO','BHB','BHX','BKK','BLR','BOD','BOM','BOS','BRS','BRU','BSR','BTV','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CPX','CWL','DAC','DCA','DEL','DEN','DMM','DOH','DOM','DTW','DUB','DUR','DUS','DXB','EDI','EIS','ELS','EWB','EWR','FCO','FRA','GBE','GLA','GND','GRJ','GVA','HAM','HDS','HEL','HKG','HKT','HNL','HPN','HRE','HYA','HYD','IAD','IOM','ISB','JED','JFK','JNB','KBL','KEF','KHI','KIM','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGA','LGB','LGW','LHE','LHR','LIH','LIM','LIN','LIS','LLW','LUN','LVI','LYS','MAA','MAN','MAZ','MCT','MDE','MED','MKK','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NCE','NCL','NEV','NYC','OAK','OGG','OGS','ORD','ORH','OSL','PAP','PDX','PER','PEW','PHL','PHW','PHX','PIT','PLZ','POL','POS','PRG','PTG','PTP','PUJ','PVC','PVD','PVG','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RUH','RUT','SAN','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJU','SKB','SKT','SLC','SLK','SLU','SMF','SNN','STR','STT','STX','SVD','SXF','SXM','SYR','SZK','TET','TLS','TNR','TRV','UTN','UTT','VFA','VIE','VIJ','VNX','VQS','WAS','XBO','XSF','ZLA','ZRH',0];
var rPDX = ['ACK','ALB','AMS','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BBK','BDA','BEW','BFN','BGI','BHB','BHX','BOS','BTV','BUF','BUQ','BWI','CDG','CHS','CLT','CPH','CPT','CPX','DAR','DCA','DOM','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FDF','FLL','FRA','GBE','GCM','GLA','GND','GRJ','HDS','HPN','HRE','HYA','IAD','JAX','JFK','JNB','KEF','KIM','KIR','LAS','LEB','LGB','LHR','LOS','LRM','LUN','LVI','MAN','MAZ','MBJ','MCO','MCT','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MVY','NAS','NEV','NLA','NYC','OAK','OGS','PBI','PHL','PHW','PLS','PLZ','POL','POP','POS','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUT','SAV','SDQ','SFO','SJU','SKB','SLC','SLK','SNN','SRQ','STI','STT','STX','SVD','SXM','SYR','SZK','TET','TNR','TPA','UTN','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','XSF','ZLA',0];
var rPER = ['ACK','AZS','BDA','BOS','BTV','BUF','BWI','CHS','CLT','DCA','DTW','EWR','FLL','IAD','JAX','JFK','LRM','MCO','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL',0];
var rPEW = ['ACK','AUA','AUS','BOG','BOS','BUF','BWI','CHS','CLT','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','MCO','NAS','NYC','ORD','PAP','PBI','PHL','PIT','POP','PUJ','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','WAS','XBO','XDR','XFL',0];
var rPGF = ['BUF','BWI','DCA','JFK','MCO','NYC','PHL','WAS',0];
var rPHL = ['ABZ','ACK','AMD','AMS','ANU','ARN','AUG','AXA','BAH','BDA','BGI','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLQ','BLR','BOD','BOH','BOM','BOS','BRS','BRU','BUD','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPX','CWL','DAC','DBV','DEL','DEN','DFW','DMM','DOH','DOM','DTW','DUB','DXB','EDI','EIS','EWB','FCO','FLL','FRA','GLA','GVA','HAJ','HEL','HKT','HOU','HYA','HYD','IOM','ISB','JAX','JED','JER','KEF','KHI','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIN','LIS','LYS','MAA','MAN','MAZ','MCO','MCT','MED','MLE','MRU','MSS','MUC','MVY','NAP','NBO','NCE','NCL','NEV','OAK','OGS','ORD','ORK','OSL','PBI','PDX','PER','PEW','PGF','PHX','PMI','PRG','PUJ','PVC','PVD','RKD','RSW','RUH','RUT','SAN','SAV','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJU','SLK','SNN','SRQ','STI','STR','STT','STX','SVG','SXF','TLS','TPA','TRD','TRV','VCE','VIE','VIJ','VQS','VRN','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rPHW = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PLS','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rPHX = ['ABZ','ACK','ADD','ALB','AMD','AMS','ANU','APL','ARN','AUA','AUG','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BHB','BHX','BLR','BOM','BOS','BQN','BRS','BTV','BUF','BUQ','BWI','CCJ','CDG','CHS','CMB','COK','CPH','CPT','CPX','CWL','DAR','DCA','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FLL','FRA','GBE','GLA','GND','GRJ','HDS','HEL','HPN','HRE','HYA','HYD','IAD','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KIR','KWI','LEB','LGW','LHR','LLW','LRM','LUN','LVI','MAA','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MVY','NAS','NBO','NCL','NEV','NLA','NYC','OGS','PAP','PBI','PHL','PHW','PIT','PLS','PLZ','POL','POP','POS','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SDQ','SEZ','SJU','SKB','SLK','SNN','SRQ','STI','STT','STX','SVD','SXM','SYR','SZK','TET','TNR','TPA','TRV','UTN','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL',0];
var rPIT = ['ABZ','ACK','AMD','AMS','ANU','ARN','AUG','AUS','AXA','BAH','BDA','BGI','BGO','BHB','BHD','BHX','BIM','BIO','BKK','BLK','BLQ','BLR','BOD','BOG','BOH','BOM','BOS','BRS','BRU','BUD','CCU','CDG','CMB','COK','CPH','CPX','CUN','CWL','DAC','DBV','DEL','DEN','DFW','DMM','DOH','DOM','DUB','DXB','EDI','EIS','ELH','EWB','EWR','EYW','FLL','FPO','FRA','GGT','GHB','GLA','GVA','HAJ','HEL','HOU','HYA','HYD','IOM','ISB','JAX','JED','JER','JFK','KEF','KHI','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIN','LIS','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MHH','MLE','MRU','MSY','MUC','MVY','NAP','NAS','NCE','NCL','NEV','NYC','OAK','ORK','OSL','PBI','PDX','PER','PEW','PHX','PMI','POS','PRG','PUJ','PVC','PVD','RKD','RSW','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SIN','SJC','SJO','SJU','SLK','SNN','SRQ','STI','STR','STT','STX','SXF','TCB','TLS','TPA','TRV','VCE','VIE','VIJ','VQS','VRN','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rPLS = ['ABQ','ABZ','ACK','AMD','AUG','AUS','BAH','BHB','BKK','BLR','BOM','BOS','BTV','BUF','BUR','BWI','CCU','CGK','CHS','CMB','COK','CPH','DAC','DCA','DEL','DEN','DFW','DMM','DOH','DTW','DUB','DUR','DXB','EDI','EWR','HDS','HYA','HYD','IAD','ISB','JFK','KHI','KUL','KWI','LAS','LAX','LEB','LGB','LHE','LHR','MAA','MAN','MCT','MLE','MPM','MQP','MRU','MSY','MTS','MVY','MXP','NYC','OAK','ORD','PDX','PER','PHW','PHX','PIT','PVC','PVG','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RUH','RUT','SAN','SEA','SEZ','SFO','SIN','SJC','SLC','SLK','SMF','SYR','SZK','TRV','WAS','XBO','XSF','ZLA',0];
var rPLZ = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NAS','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA',0];
var rPMI = ['BUF','BWI','DCA','EWR','FLL','IAD','JFK','MCO','NYC','PHL','PIT','RIC','WAS','XFL',0];
var rPNS = ['BDL','BOG','BOS','BQN','BUF','CUN','DCA','EWR','HPN','JFK','KIN','LGA','LIM','MBJ','MDE','NAS','NYC','ORH','PAP','POS','PSE','PUJ','PVD','SDQ','SJU','SLC','SYR','WAS','XBO','XDR',0];
var rPOL = ['ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','JAX','LAS','LAX','LGB','MCO','NAS','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rPOP = ['ABQ','ABZ','AMD','AMS','ARN','BAH','BGO','BKK','BLR','BOM','BOS','BTV','BUF','BUR','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','DAC','DCA','DEL','DEN','DMM','DOH','DTW','DUB','DXB','EDI','EWR','FRA','GLA','HEL','HYD','ISB','JFK','KEF','KHI','KUL','KWI','LAS','LAX','LGB','LHE','LHR','MAA','MAN','MCT','MLE','MRU','MXP','NYC','OAK','ORD','OSL','PDX','PER','PEW','PHL','PHX','PVC','PVG','PWM','RDU','ROC','RUH','SAN','SEA','SEZ','SFO','SIN','SJC','SLC','SMF','SYR','TRV','WAS','XBO','XSF','ZLA',0];
var rPOS = ['ABQ','ACK','AUG','BDL','BHB','BKK','BLR','BOM','BOS','BTV','BUF','BUR','CCJ','CCU','CGK','CRK','DAC','DCA','DEL','DEN','DXB','EWR','EYW','FLL','FPO','GNV','HYA','HYD','JAX','JFK','KHI','KUL','LAS','LAX','LEB','LGB','MAA','MCO','MNL','MSY','MVY','NYC','OAK','ORD','PBI','PDX','PHX','PNS','PUJ','PVC','PWM','RDU','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SIN','SJC','SJU','SLC','SLK','SMF','STT','STX','SYR','TLH','TPA','XBO','XFL','XSF','ZLA',0];
var rPRG = ['ACK','AUA','BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','MSY','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAN','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rPSE = ['ABZ','ACK','ALB','AMD','AMS','ARN','AUG','AUS','BAH','BDL','BGO','BHB','BKK','BLR','BOM','BOS','BTV','BUF','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','DAC','DCA','DEL','DMM','DOH','DUB','DXB','EDI','EWR','EYW','FLL','FRA','GLA','HEL','HKT','HOU','HPN','HYA','HYD','IAD','ISB','JAX','JFK','KBL','KEF','KHI','KUL','LAS','LAX','LEB','LGA','LGB','LHE','LHR','MAA','MAN','MCN','MCO','MCT','MLE','MSY','MVY','MXP','NYC','OGS','ORD','OSL','PER','PNS','PVC','PVD','PWM','RDU','RKD','ROC','RUT','SAN','SAV','SEA','SFO','SGN','SIN','SLK','SWF','SYR','TLH','TRV','WAS','XBO','XSF','ZLA',0];
var rPTG = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rPTP = ['BDL','BOS','BTV','BUF','BUR','DCA','DEN','EWR','FLL','JAX','JFK','LAS','LAX','LGB','MCO','NYC','OAK','ORD','PBI','PDX','PHX','PUJ','PWM','ROC','SAN','SDQ','SEA','SFO','SJU','SLC','STI','STT','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rPUJ = ['ABQ','ABZ','ACK','ALB','AMD','AMS','ANU','ARN','AUG','AUS','AXA','BAH','BDL','BGI','BGO','BHB','BKK','BLR','BOM','BOS','BTV','BUF','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CMB','COK','CPH','CPX','DAC','DCA','DEL','DEN','DFW','DMM','DOH','DOM','DTW','DUB','DXB','EDI','EIS','ELH','EWR','EYW','FDF','FLL','FPO','FRA','GLA','GNV','HEL','HPN','HYA','HYD','IAD','ISB','JAX','JFK','KEF','KHI','KUL','LAS','LAX','LEB','LGA','LGB','LGW','LHE','LHR','MAA','MAN','MCO','MCT','MLE','MSY','MUC','MVY','MXP','NEV','NYC','OAK','OGS','ORD','ORH','OSL','PBI','PDX','PER','PEW','PHL','PHX','PIT','PNS','POS','PTP','PVC','PVG','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SEA','SEZ','SFO','SIN','SJC','SJU','SKB','SKT','SLC','SLK','SLU','SMF','STT','STX','SVD','SXM','SYR','TPA','TRV','VIJ','VQS','WAS','XBO','XFL','XSF','ZLA',0];
var rPVC = ['ABQ','ANC','AUA','AUS','AXA','BDA','BGI','BOG','BQN','BUF','BUR','BWI','CHS','CLT','CPX','CUN','DCA','DEN','DFW','DTW','EIS','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POP','POS','PSE','PUJ','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','STT','STX','SXM','SYR','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rPVD = ['ANC','AUS','BDA','BGI','BID','BOG','BQN','BUF','BWI','CHS','CLT','CPX','CUN','DCA','DEN','DFW','DOM','DTW','ELH','EYW','FLL','FPO','GNV','HOU','IAD','JAX','KIN','LAS','LAX','LGB','MBJ','MCO','MDE','MHH','MSY','NAS','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PNS','PSE','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJO','SJU','STI','STX','TCB','TLH','TPA','VQS','XDR','XFL','XSF',0];
var rPVG = ['AUA','BDA','BGI','BOG','BQN','EWR','FLL','GCM','JFK','KIN','MBJ','NAS','NYC','PAP','PBI','PLS','POP','PUJ','SDQ','SJU','STI','SXM','UVF','XDR','XFL',0];
var rPWM = ['ABQ','AMD','AMS','ANU','APL','ARN','AUA','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BKK','BLR','BLZ','BOM','BQN','BRU','BUF','BUQ','BUR','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTG','CUN','CUR','DAC','DEL','DEN','DMM','DOH','DOM','DUB','DUR','DXB','EIS','ELS','FDF','FLL','FRA','GBE','GCM','GLA','GND','GRJ','GVA','HDS','HEL','HKG','HKT','HOU','HRE','HYD','IAD','ITO','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KOA','KUL','LAS','LAX','LGB','LGW','LHE','LHR','LIH','LIR','LLW','LRM','LUN','LVI','MAA','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSU','MSY','MTS','MUB','MUC','MXP','NAS','NEV','NYC','OAK','OGG','ORD','OSL','PBI','PDX','PER','PHW','PHX','PLS','PLZ','POL','POP','POS','PSE','PTG','PTP','PUJ','PZB','RCB','RDU','ROC','RSW','RUH','SAN','SAV','SDQ','SEA','SEZ','SFO','SGN','SIN','SJC','SJU','SKB','SKT','SLC','SMF','SNN','SRQ','STI','STT','STX','SVD','SXM','SYR','SZK','TET','TNR','TPA','TRV','UTN','UTT','UVF','VFA','VIJ','VNX','VQS','WAS','XDR','XFL','XSF','ZLA','ZRH',0];
var rPZB = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PLS','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rRCB = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PLS','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rRDU = ['ABQ','ABZ','ACK','AGP','ALB','AMD','AMS','ANU','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BCN','BDA','BGI','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLQ','BLR','BOD','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUR','CCU','CDG','CGK','CMB','COK','CPH','CPT','CPX','CTG','CUN','CWL','DAC','DEL','DEN','DMM','DOH','DUB','DUR','DUS','DXB','EDI','EIS','ELH','EWB','EWR','EYW','FCO','FLL','FPO','FRA','GCM','GLA','GVA','HAJ','HAM','HEL','HKG','HKT','HOU','HYA','HYD','IOM','ISB','ITO','JED','JER','JFK','JNB','KEF','KHI','KIN','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIM','LIN','LIR','LIS','LRM','LYS','MAA','MAN','MAZ','MBJ','MCT','MDE','MED','MHH','MLE','MRU','MSS','MSY','MUC','MVY','NAS','NBO','NCE','NCL','NYC','OAK','OGG','OGS','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PHX','PLS','POP','PRG','PSE','PUJ','PVC','PVD','PWM','RKD','ROC','RSW','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJU','SLC','SLK','SMF','SNN','STI','STR','STT','STX','SXF','SXM','SYR','TCB','TLS','TPA','TRV','UVF','VIE','VIJ','VQS','VRN','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rRIC = ['ABZ','ACK','ALB','AMD','AMS','ANU','ARN','AUA','AUG','AUS','AXA','BAH','BGI','BGO','BHB','BHD','BHX','BIM','BIO','BKK','BLK','BLQ','BLR','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BUF','CCU','CDG','CGK','CMB','COK','CPH','CPX','CTG','CUN','CWL','DAC','DBV','DEL','DEN','DFW','DMM','DOH','DOM','DUB','DXB','EDI','EIS','ELH','EWB','EYW','FLL','FPO','FRA','GGT','GHB','GLA','GNV','HAJ','HEL','HKT','HOU','HYA','HYD','IOM','ISB','JAX','JED','JER','JFK','KEF','KHI','KIN','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIN','LIS','LYS','MAA','MAN','MAZ','MBJ','MCO','MCT','MHH','MLE','MRU','MSS','MSY','MUC','MVY','NAP','NAS','NBO','NCE','NCL','NEV','NYC','OAK','OGS','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PHX','PLS','PMI','POS','PRG','PSE','PUJ','PVC','PVD','RKD','RSW','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SLK','SNN','SRQ','STI','STR','STT','STX','SXF','SXM','TCB','TLH','TLS','TPA','TRV','VCE','VIE','VIJ','VQS','VRN','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rRKD = ['ABQ','ACK','ANC','AUA','AUS','AXA','BDA','BGI','BOG','BOS','BQN','BUF','BUR','BWI','CHS','CLT','CPX','CUN','DCA','DEN','DFW','DTW','EIS','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POS','PSE','PUJ','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','STT','STX','SYR','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rRNS = ['DCA','WAS',0];
var rROC = ['ABQ','ABZ','ACK','ADD','AMD','AMS','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BHB','BKK','BLR','BLZ','BOM','BOS','BQN','BRU','BTV','BUQ','BUR','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTG','CUN','CUR','DAC','DAR','DEL','DEN','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','FBM','FDF','FLL','FRA','GBE','GCM','GLA','GND','GRJ','GVA','HDS','HEL','HKG','HKT','HNL','HOU','HRE','HYA','HYD','IAD','ISB','ITO','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KOA','KUL','KWI','LAS','LAX','LGB','LGW','LHE','LHR','LIH','LIR','LLW','LRM','LUN','LVI','MAA','MAN','MAZ','MBJ','MCO','MCT','MKK','MLE','MPM','MQP','MRU','MSU','MSY','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NEV','NLA','NYC','OAK','OGG','ORD','OSL','PBI','PDX','PER','PEW','PHW','PHX','PLS','PLZ','POL','POP','POS','PSE','PTG','PTP','PUJ','PVC','PWM','PZB','RCB','RDU','RKD','RSW','RUH','SAN','SAV','SDQ','SEA','SEZ','SFO','SGN','SIN','SJC','SJU','SKB','SLC','SMF','SNN','SRQ','STI','STT','STX','SVD','SXM','SZK','TET','TNR','TPA','TRD','TRV','UTN','UTT','UVF','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rRSW = ['ABZ','ACK','ADD','ALB','AMD','AMS','ARN','AUG','AUS','BAH','BDA','BDL','BFN','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLL','BLQ','BLR','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUD','BUF','BUR','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CUN','DAC','DAR','DBV','DCA','DEL','DEN','DMM','DOH','DTW','DUB','DUR','DXB','EDI','EWB','EWR','FCO','FRA','GLA','GVA','HAJ','HAM','HEL','HKG','HPN','HYA','HYD','IAD','IOM','ISB','JED','JER','JFK','JNB','KEF','KHI','KIN','KIR','KUL','KWI','LAX','LEB','LGA','LGB','LGW','LHE','LHR','LIM','LIN','LIS','LYS','MAA','MAN','MBJ','MCT','MDE','MED','MLE','MQP','MRU','MSS','MUC','MVY','NAS','NCE','NYC','OAK','OGS','ORD','ORH','ORK','OSL','PAP','PDX','PER','PEW','PHL','PHX','PIT','PRG','PVC','PVD','PWM','RDU','RIC','RKD','ROC','RUH','RUT','SCQ','SDQ','SEA','SEN','SEZ','SFO','SIN','SJC','SJO','SJU','SLC','SLK','SMF','SNN','STR','SVG','SXF','SYR','TLS','TRD','TRV','VCE','VIE','VRN','WAS','WAW','XBO','XSF','ZLA','ZRH',0];
var rRUH = ['ABQ','ACK','AUA','AUS','BDA','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','OAK','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SLC','SMF','STI','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rRUT = ['ABQ','ACK','ANC','AUA','AUS','AXA','BDA','BGI','BOG','BOS','BQN','BUR','BWI','CHS','CLT','CPX','CUN','DCA','DEN','DFW','DTW','EIS','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POS','PSE','PUJ','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','STT','STX','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSAN = ['ABZ','ACE','ACK','ADD','ALB','AMD','AMS','ANU','APL','ARN','AUG','AXA','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BLK','BLR','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUQ','BWI','CCJ','CDG','CHS','CLT','CMB','COK','CPH','CPT','CPX','CWL','DAR','DCA','DEL','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FLL','FRA','GBE','GLA','GND','GRJ','HDS','HEL','HPN','HRE','HYA','HYD','IAD','JAX','JED','JFK','JNB','KEF','KHI','KIM','KIN','KIR','KWI','LEB','LGW','LHR','LIS','LOS','LUN','LVI','LYS','MAA','MAN','MAZ','MCO','MCT','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','NAS','NBO','NCE','NCL','NEV','NLA','NYC','OGS','ORK','OSL','PBI','PHL','PHW','PIT','PLS','PLZ','POL','POS','PRG','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RUH','RUT','SAV','SDQ','SEZ','SJU','SKB','SLK','SNN','STI','STT','STX','SVD','SYR','SZK','TET','TNR','TRV','UTN','UTT','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','ZLA','ZRH',0];
var rSAV = ['ABQ','ABZ','ACK','ADD','ALB','AMD','AMS','APL','ARN','AUG','AUS','BAH','BBK','BEW','BFN','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLR','BOD','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUD','BUF','BUQ','BUR','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CPT','DAC','DAR','DEL','DEN','DMM','DOH','DUB','DUR','DXB','EDI','ELS','EWB','EWR','FRA','GBE','GLA','GRJ','HAJ','HDS','HEL','HKG','HPN','HRE','HYA','HYD','IOM','ISB','ITO','JED','JER','JFK','JNB','KEF','KHI','KIM','KIR','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIN','LIS','LUN','LVI','LYS','MAA','MAN','MCT','MED','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','NBO','NCE','NLA','NYC','OAK','OGG','OGS','ORD','ORK','OSL','PDX','PER','PEW','PHL','PHW','PHX','PLZ','PRG','PSE','PTG','PVC','PVD','PWM','PZB','RCB','RKD','ROC','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SIN','SJU','SLC','SLK','SMF','SNN','STR','SVG','SXF','SYR','SZK','TLS','TNR','TRD','TRV','UTN','UTT','VFA','VIE','VNX','VRN','XBO','XDR','XSF','ZLA','ZRH',0];
var rSCQ = ['BUF','BWI','CHS','DCA','DFW','EWR','FLL','HOU','IAD','JFK','LAX','MCO','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SAV','SEA','SFO','TPA','WAS','XFL','XSF','ZLA',0];
var rSDQ = ['ABQ','ABZ','ACK','ALB','AMD','AMS','ANU','ARN','AUG','AUS','AXA','BAH','BDL','BGI','BGO','BHB','BHX','BIM','BKK','BLL','BLR','BOM','BOS','BRS','BRU','BTV','BUF','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPX','DAC','DCA','DEL','DEN','DFW','DMM','DOH','DOM','DTW','DUB','DXB','EDI','EIS','EWB','EWR','EYW','FDF','FLL','FPO','FRA','GLA','GND','GNV','GVA','HAM','HEL','HKG','HKT','HOU','HPN','HYA','HYD','IAD','ISB','JAX','JFK','KBL','KEF','KHI','KIR','KUL','LAS','LAX','LEB','LGA','LGB','LGW','LHE','LHR','MAA','MAN','MCO','MCT','MHH','MLE','MSS','MSY','MUC','MVY','MXP','NEV','NYC','OAK','OGS','ORD','ORH','OSL','PBI','PDX','PER','PEW','PHL','PHX','PIT','PNS','POS','PTP','PVC','PVD','PVG','PWM','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SAV','SEA','SEZ','SFO','SGN','SIN','SJC','SJU','SKB','SKT','SLC','SLK','SLU','SMF','SNN','STT','STX','SVD','SVG','SWF','SXM','SYR','TCB','TLH','TPA','TRD','TRV','VIJ','VQS','WAS','XBO','XFL','XSF','ZLA','ZRH',0];
var rSEA = ['ACE','ACK','ALB','AMS','ANC','ANU','APL','AUA','AUG','AUS','AXA','BBK','BDA','BEW','BFN','BGI','BHB','BHX','BLK','BOD','BOH','BOS','BQN','BRS','BRU','BTV','BUD','BUF','BUQ','BWI','CDG','CHS','CLT','CPH','CPT','CPX','CUN','CWL','DAR','DCA','DOM','DTW','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FCO','FLL','FRA','GBE','GLA','GND','GRJ','GVA','HAJ','HDS','HPN','HRE','HYA','IAD','JAX','JER','JFK','JNB','KIM','KIN','KIR','LAS','LEB','LGB','LGW','LHR','LIS','LLW','LOS','LUN','LVI','LYS','MAN','MAZ','MBJ','MCO','MCT','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','NAS','NCE','NCL','NEV','NLA','NYC','OAK','OGS','ORD','PAP','PBI','PHL','PHW','PIT','PLS','PLZ','POL','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUT','SAV','SCQ','SDQ','SEN','SEZ','SFO','SJU','SKB','SLC','SLK','SMF','SNN','SRQ','STI','STR','STT','STX','SVD','SXM','SYR','SZK','TET','TNR','TPA','UTN','VFA','VIJ','VNX','VQS','VRN','WAS','WDH','WVB','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rSEN = ['ACK','BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rSEZ = ['ACK','AUA','AUS','BDA','BOS','BTV','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','STI','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSFO = ['ABZ','ACE','ACK','ADD','AGP','ALB','AMS','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BCN','BDA','BDL','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BIM','BIO','BLK','BLL','BLQ','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BRU','BTV','BUD','BUF','BUQ','BWI','CCJ','CDG','CHS','CLT','COK','CPH','CPT','CPX','CWL','DAR','DCA','DMM','DOH','DOM','DUB','DUR','DUS','DXB','EDI','EIS','ELH','ELS','EWB','EWR','EYW','FCO','FDF','FLL','FPO','FRA','GBE','GCM','GGT','GHB','GLA','GND','GNV','GRJ','GVA','HAJ','HAM','HDS','HEL','HPN','HRE','HYA','IAD','IOM','JAX','JER','JFK','JNB','KEF','KHI','KIM','KIN','KIR','LAS','LEB','LGA','LGB','LGW','LHR','LIM','LIN','LIS','LLW','LOS','LPA','LRM','LUN','LVI','LYS','MAN','MAZ','MBJ','MCO','MCT','MHH','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NCE','NCL','NEV','NLA','NYC','OGS','ORK','OSL','PAP','PBI','PDX','PHL','PHW','PIT','PLS','PLZ','POL','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SCQ','SDQ','SEA','SEN','SEZ','SJO','SJU','SKB','SLC','SLK','SLU','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SWF','SXF','SXM','SYR','SZK','TCB','TET','TLH','TLS','TNR','TPA','TRD','TRV','UTN','VFA','VIE','VIJ','VNX','VQS','VRN','WAS','WDH','WVB','XBO','XDR','XFL','ZLA','ZRH',0];
var rSGN = ['BOS','BTV','BUF','BWI','CLT','DCA','DTW','EWR','FLL','IAD','JAX','JFK','MCO','MSY','NYC','PBI','PHL','PSE','PWM','RDU','RIC','ROC','SDQ','SJU','STI','SYR','TPA','WAS','XBO','XDR','XFL',0];
var rSIN = ['ACK','AUA','AUS','AZS','BDA','BGI','BOG','BOS','BTV','BUF','BWI','CHS','CLT','CTG','CUN','DCA','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','NYC','ORD','PAP','PBI','PHL','PIT','PLS','POP','POS','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAV','SDQ','SJU','STI','SXM','SYR','TPA','UVF','WAS','XBO','XDR','XFL',0];
var rSJC = ['ABZ','ACK','ADD','ALB','AMS','ANU','APL','ARN','AUA','AUG','AXA','BAH','BBK','BDA','BEW','BFN','BGI','BHB','BHX','BOM','BOS','BQN','BTV','BUF','BUQ','BWI','CCJ','CDG','CHS','CLT','COK','CPH','CPT','CPX','DAR','DCA','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FLL','FRA','GBE','GCM','GLA','GRJ','HDS','HEL','HPN','HRE','HYA','IAD','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KIR','LEB','LGW','LHR','LUN','LVI','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MVY','NAS','NBO','NEV','NLA','NYC','OGS','PAP','PBI','PHL','PHW','PIT','PLS','PLZ','POL','POP','PTG','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SDQ','SEZ','SJU','SKB','SLK','SNN','SRQ','STI','STT','STX','SVD','SYR','SZK','TET','TNR','TPA','TRV','UTN','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL',0];
var rSJO = ['BDL','BOS','DCA','EWR','FLL','FPO','GNV','HPN','JAX','JFK','LAS','LAX','LGA','LGW','LHR','MAN','MCO','NYC','PVD','RIC','RSW','SFO','SJU','SLC','SWF','TLH','WAS','XBO','XFL','XSF','ZLA',0];
var rSJU = ['ABQ','ABZ','ACK','ALB','AMD','AMS','ANU','ARN','AUG','AUS','AXA','BAH','BDL','BGI','BGO','BHB','BHD','BHX','BIM','BKK','BLK','BLL','BLR','BOD','BOH','BOM','BOS','BRS','BRU','BTV','BUF','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPX','CUN','DAC','DCA','DEL','DEN','DFW','DMM','DOH','DOM','DTW','DUB','DXB','EDI','EIS','ELH','EWB','EWR','EYW','FLL','FPO','FRA','GLA','GND','GNV','HAJ','HAM','HEL','HKG','HKT','HOU','HPN','HYA','HYD','IAD','ISB','JAX','JER','JFK','KEF','KHI','KIR','KUL','LAS','LAX','LEB','LGA','LGB','LGW','LHE','LHR','LYS','MAA','MAN','MAZ','MBJ','MCN','MCO','MCT','MHH','MLE','MSS','MSY','MUC','MVY','MXP','NAS','NCE','NEV','NYC','OAK','OGS','ORD','ORH','OSL','PBI','PDX','PER','PEW','PHL','PHX','PIT','PNS','POS','PRG','PTP','PUJ','PVC','PVD','PVG','PWM','RDU','RIC','RKD','ROC','RSW','RUT','SAN','SAV','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SKB','SKT','SLC','SLK','SLU','SMF','SNN','STI','STR','STT','STX','SVD','SVG','SWF','SXM','SYR','TCB','TLH','TLS','TPA','TRD','TRV','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rSKB = ['ABQ','AUS','BDL','BOS','BTV','BUF','BUR','DCA','DEN','EWR','FLL','JAX','JFK','LAS','LAX','LGB','MCO','NYC','OAK','ORD','PBI','PDX','PHX','PUJ','PWM','ROC','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSKT = ['BOS','BTV','BUF','CUN','FLL','MCO','NAS','PBI','PUJ','PWM','SDQ','SJU','STI','TPA','XBO','XDR','XFL',0];
var rSLC = ['ABZ','ACK','ADD','ALB','AMD','ANC','ANU','APL','AUA','AUG','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BHB','BLR','BOM','BOS','BTV','BUF','BUQ','CCJ','CHS','CMB','COK','CPT','CPX','DAR','DCA','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','EWR','EYW','FLL','GBE','GCM','GND','GNV','GRJ','HDS','HPN','HRE','HYA','HYD','IAD','JFK','JNB','KHI','KIM','KIN','LAS','LEB','LGA','LGB','LHR','LLW','LOS','LUN','LVI','MAA','MAN','MAZ','MBJ','MCO','MCT','MHH','MLE','MPM','MQP','MRU','MSU','MTS','MUB','MVY','NAS','NBO','NEV','NLA','NYC','OAK','OGS','PAP','PBI','PDX','PHW','PLS','PLZ','PNS','POL','POP','POS','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RKD','ROC','RSW','RUH','RUT','SAV','SDQ','SEA','SFO','SJO','SJU','SKB','SLK','SMF','SNN','SRQ','STI','STT','STX','SVD','SXM','SYR','SZK','TET','TLH','TNR','TPA','TRV','UTN','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','XSF','ZLA',0];
var rSLK = ['ABQ','ACK','ANC','AUA','AUS','AXA','BDA','BGI','BOG','BOS','BQN','BUR','BWI','CHS','CLT','CPX','CUN','DCA','DEN','DFW','DTW','EIS','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIM','MAZ','MBJ','MCO','MDE','MSY','NAS','NEV','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PLS','POS','PSE','PUJ','RDU','RIC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','STI','STT','STX','TPA','VIJ','VQS','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSLU = ['BDL','DCA','EWR','FLL','JAX','LAX','MCO','NYC','ORD','PBI','PUJ','SDQ','SFO','SJU','STT','STX','TPA','WAS','XDR','XFL','XSF','ZLA',0];
var rSMF = ['ABZ','ACK','ADD','ALB','AMS','ANC','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BHB','BOS','BQN','BTV','BUF','BUQ','CCJ','CDG','CHS','COK','CPH','CPT','CPX','DAR','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','FLL','FRA','GBE','GCM','GRJ','HDS','HEL','HRE','HYA','IAD','JAX','JFK','JNB','KEF','KIM','KIN','LAS','LEB','LGB','LHR','LUN','LVI','MAN','MAZ','MBJ','MCO','MCT','MLE','MPM','MQP','MRU','MSU','MTS','MUB','MVY','NAS','NBO','NEV','NLA','NYC','OGS','PAP','PBI','PHW','PLS','PLZ','POL','POP','PTG','PUJ','PVC','PWM','PZB','RCB','RKD','ROC','RSW','RUH','RUT','SAV','SDQ','SEA','SJU','SKB','SLC','SLK','SNN','SRQ','STI','STT','STX','SVD','SXM','SYR','SZK','TET','TNR','TPA','TRV','UTN','VFA','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','ZLA',0];
var rSNN = ['ABQ','ACK','AUS','BDA','BOS','BQN','BTV','BUF','BUR','BWI','CHS','CLT','CUN','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','MCO','MSY','NAS','NYC','OAK','ORD','PAP','PBI','PDX','PHL','PHX','PIT','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','SRQ','STI','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSRQ = ['ABZ','ACK','AMS','ARN','AUG','BDA','BGO','BOM','BOS','BTV','BUF','BWI','CDG','CPH','DCA','DEN','DTW','DUB','DXB','EDI','EWR','FRA','GLA','HEL','HYA','IAD','JFK','KEF','LAS','LEB','LGA','LGW','LHR','LIH','MAN','MUC','MVY','NYC','OAK','ORD','OSL','PDX','PHL','PHX','PVC','PWM','RIC','RKD','ROC','RUT','SEA','SFO','SJC','SLC','SMF','SNN','SYR','WAS','XBO','XSF',0];
var rSTI = ['ABQ','ABZ','ACK','ALB','AMD','AMS','ARN','AUG','AUS','AXA','BAH','BDL','BGO','BHB','BHX','BKK','BLL','BLR','BOM','BOS','BRS','BRU','BTV','BUF','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPX','DAC','DCA','DEL','DEN','DFW','DMM','DOH','DTW','DUB','DXB','EDI','EIS','EWB','EWR','FDF','FLL','FRA','GLA','GVA','HAM','HEL','HKG','HKT','HOU','HPN','HYA','HYD','IAD','ISB','JFK','KBL','KEF','KHI','KIR','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','MAA','MAN','MAZ','MCO','MCT','MLE','MRU','MSS','MSY','MUC','MVY','MXP','NEV','NYC','OAK','OGS','ORD','OSL','PDX','PER','PEW','PHL','PHX','PIT','PTP','PVC','PVD','PVG','PWM','RDU','RIC','RKD','ROC','RUH','RUT','SAN','SEA','SEZ','SFO','SGN','SIN','SJC','SJU','SKT','SLC','SLK','SMF','SNN','STT','STX','SVG','SYR','TRD','TRV','VIJ','VQS','WAS','XBO','XFL','XSF','ZLA','ZRH',0];
var rSTR = ['BDA','BUF','BWI','CHS','DCA','DFW','EWR','FLL','HOU','IAD','JFK','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAV','SEA','SFO','SJU','TPA','WAS','XFL','XSF','ZLA',0];
var rSTT = ['ABQ','ACK','ALB','ANU','AUG','AUS','BDL','BGI','BHB','BOS','BTV','BUF','BUR','BWI','CUN','DCA','DEN','DOM','DTW','EWR','FDF','FLL','GND','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','LHR','LRM','MAZ','MCO','MVY','NAS','NYC','OAK','OGS','ORD','ORH','PBI','PDX','PHL','PHX','PIT','POS','PTP','PUJ','PVC','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SLK','SLU','SMF','STI','SVD','SWF','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSTX = ['ABQ','ACK','ALB','AUG','AUS','BDL','BGI','BHB','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DOM','DTW','EWR','FDF','FLL','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','MAZ','MCO','MVY','NAS','NYC','OAK','OGS','ORD','ORH','PBI','PDX','PHL','PHX','PIT','POS','PUJ','PVC','PVD','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SLK','SLU','SMF','STI','SVD','SWF','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSVD = ['ABQ','ACK','BDL','BOS','BTV','BUF','BUR','DCA','DEN','EWR','FLL','HYA','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NYC','OAK','ORD','PBI','PDX','PHX','PUJ','PWM','ROC','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SMF','STT','STX','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSVG = ['BOS','BUF','BWI','CHS','DCA','EWR','FLL','HOU','JFK','LAX','MCO','MSY','NAS','NYC','ORD','PHL','RSW','SAV','SDQ','SFO','SJU','STI','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rSWF = ['AUS','AXA','BOG','BQN','CPX','CUN','EIS','EYW','FLL','FPO','GNV','KIN','LAS','LAX','LIM','MAZ','MCO','MHH','NAS','NEV','PSE','SDQ','SFO','SJO','SJU','STT','STX','TLH','TPA','VIJ','VQS','XDR','XFL','XSF','ZLA',0];
var rSXF = ['BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JFK','LAS','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAV','SFO','TPA','WAS','XFL','XSF','ZLA',0];
var rSXM = ['ABQ','ACK','AMD','AMS','ARN','AUS','BDL','BKK','BLR','BOM','BOS','BTV','BUF','BUR','BWI','CCJ','CCU','CGK','CHS','CLT','CMB','COK','CPH','CPX','DAC','DCA','DEL','DEN','DFW','DOH','DTW','DUB','DXB','EWR','FLL','FRA','HEL','HOU','HYD','IAD','ISB','JAX','JFK','KEF','KHI','KUL','LAS','LAX','LGB','LHE','LHR','MAA','MAZ','MCO','MCT','MLE','MSY','MXP','NYC','OAK','ORD','OSL','PBI','PDX','PER','PEW','PHL','PHX','PIT','PUJ','PVC','PVG','PWM','RDU','RIC','ROC','SAN','SDQ','SEA','SFO','SIN','SJC','SJU','SLC','SMF','SYR','TPA','TRV','VQS','WAS','XBO','XFL','XSF','ZLA',0];
var rSYD = ['ACK','BOS','BUF','FLL','JFK','MCO','NYC','XBO','XFL',0];
var rSYR = ['ABQ','ABZ','ACK','AMD','AMS','ANU','APL','ARN','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BGO','BHB','BKK','BLR','BLZ','BOG','BOM','BOS','BQN','BUQ','BUR','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CPT','CPX','CTG','CUN','CUR','DAC','DAR','DEL','DEN','DMM','DOH','DOM','DUB','DUR','DXB','EDI','EIS','ELS','EYW','FBM','FDF','FLL','FRA','GBE','GCM','GLA','GND','GRJ','GVA','HDS','HEL','HKG','HKT','HOU','HRE','HYA','HYD','IAD','ISB','ITO','JAX','JFK','JNB','KEF','KHI','KIM','KIN','KOA','KUL','KWI','LAS','LAX','LGB','LHE','LHR','LIH','LIR','LLW','LRM','LUN','LVI','MAA','MAN','MAZ','MBJ','MCO','MCT','MHH','MLE','MPM','MQP','MRU','MSU','MSY','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NEV','NLA','NYC','OAK','OGG','ORD','OSL','PBI','PDX','PER','PEW','PHW','PHX','PLS','PLZ','PNS','POL','POP','POS','PSE','PTG','PTP','PUJ','PVC','PWM','PZB','RCB','RDU','RKD','RSW','RUH','SAN','SAV','SDQ','SEA','SEZ','SFO','SGN','SIN','SJC','SJU','SKB','SLC','SMF','SNN','SRQ','STI','STT','STX','SVD','SXM','SZK','TET','TLH','TNR','TPA','TRV','UTN','UTT','UVF','VFA','VIJ','VNX','VQS','WAS','WDH','WVB','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rSZK = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PLS','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rTCB = ['AUS','BDL','BOS','CUN','DCA','EWR','HPN','JAX','JFK','KIN','LAS','LAX','LGA','LIM','MBJ','MDE','NYC','ORH','PAP','PIT','PVD','RDU','RIC','SDQ','SFO','SJU','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rTET = ['ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','JAX','JFK','LAS','LAX','LGB','MCO','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rTLH = ['AUS','BDL','BOG','BOS','BQN','BUF','CUN','DCA','EWR','HPN','JFK','KIN','LAX','LGA','LIM','MBJ','MDE','NAS','NYC','PAP','POS','PSE','PVD','RIC','SDQ','SFO','SJO','SJU','SLC','SWF','SYR','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rTLS = ['BUF','BWI','CHS','DFW','EWR','FLL','HOU','IAD','JFK','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAV','SFO','SJU','WAS','XFL','XSF','ZLA',0];
var rTNR = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','LAS','LAX','LGB','LIR','MBJ','MCO','MVY','NAS','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rTPA = ['ABQ','ABZ','ACK','ADD','ALB','AMD','AMS','ANU','APL','ARN','AUG','AUS','AXA','BAH','BBK','BDA','BDL','BEW','BFN','BGI','BGO','BHB','BHD','BHX','BIO','BKK','BLK','BLL','BLQ','BLR','BOD','BOG','BOH','BOM','BOS','BRS','BRU','BTV','BUD','BUF','BUQ','BWI','CCJ','CCU','CDG','CGK','CMB','COK','CPH','CPT','CPX','CUN','CWL','DAC','DAR','DBV','DCA','DEL','DEN','DMM','DOH','DOM','DTW','DUB','DUR','DXB','EDI','EIS','ELS','EWB','EWR','FCO','FDF','FRA','GBE','GLA','GND','GRJ','GVA','HAJ','HAM','HDS','HEL','HKG','HKT','HPN','HRE','HYA','HYD','IAD','IOM','ISB','JED','JER','JFK','JNB','KEF','KHI','KIM','KIN','KIR','KUL','KWI','LAS','LAX','LEB','LGA','LGW','LHE','LHR','LIH','LIM','LIN','LIS','LLW','LRM','LUN','LVI','LYS','MAA','MAN','MAZ','MBJ','MCT','MDE','MED','MLE','MPM','MQP','MRU','MSS','MSU','MTS','MUB','MUC','MVY','MXP','NAS','NBO','NCE','NCL','NEV','NLA','NYC','OAK','OGS','ORD','ORH','ORK','OSL','PAP','PDX','PER','PEW','PHL','PHW','PHX','PIT','PLZ','POL','POS','PRG','PTG','PTP','PUJ','PVC','PVD','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RUH','RUT','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJU','SKB','SKT','SLC','SLK','SLU','SMF','SNN','STR','STT','STX','SVD','SVG','SWF','SXF','SXM','SYR','SZK','TET','TNR','TRD','TRV','UTN','UTT','UVF','VCE','VFA','VIE','VIJ','VNX','VQS','VRN','WAS','WAW','XBO','XSF','ZRH',0];
var rTRD = ['ACK','AUA','BOS','BUF','BWI','CHS','DCA','EWR','FLL','HOU','JFK','LAX','MCO','NAS','NYC','ORD','PHL','ROC','RSW','SAV','SDQ','SFO','SJU','STI','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rTRV = ['ABQ','ACK','AUA','AUS','BDA','BGI','BOG','BOS','BTV','BUF','BUR','BWI','CHS','CLT','CTG','CUN','DCA','DEN','DFW','DTW','EWR','FLL','GCM','HOU','IAD','JAX','JFK','KIN','LAS','LAX','LGB','LIR','LRM','MBJ','MCO','MSY','MVY','NAS','OAK','ORD','PAP','PBI','PHL','PHX','PIT','PLS','POP','PSE','PUJ','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SFO','SJC','SJU','SLC','SMF','STI','SXM','SYR','TPA','UVF',0];
var rUTN = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','DEN','FLL','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rUTT = ['ACK','AUS','BOS','BTV','BUF','CHS','CUN','FLL','GCM','HOU','LAS','LAX','LGB','MCO','NAS','ORD','PBI','PWM','ROC','SAN','SAV','SYR','TPA','XBO','XFL',0];
var rUVF = ['ABQ','AMD','BKK','BLR','BOM','BOS','BTV','BUF','BUR','BWI','CCJ','CCU','CGK','CMB','COK','DAC','DCA','DEL','DEN','DTW','DXB','EWR','HYD','JFK','KHI','KUL','LAS','LAX','LGB','LHE','LHR','MAA','MCT','MLE','NYC','OAK','ORD','PDX','PHX','PVG','PWM','RDU','ROC','SAN','SEA','SFO','SIN','SJC','SLC','SMF','SYR','TRV','WAS','XBO','XSF','ZLA',0];
var rVCE = ['BUF','BWI','DCA','DFW','EWR','FLL','IAD','JFK','MCO','NYC','ORD','PHL','PIT','RIC','RSW','TPA','WAS','XFL',0];
var rVFA = ['ABQ','ACK','AUS','BOS','BTV','BUF','BUR','CHS','DEN','FLL','HOU','JAX','JFK','LAS','LAX','LGB','MCO','MVY','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rVIE = ['BDA','BUF','BWI','CHS','CLT','DCA','DEN','DFW','DTW','EWR','FLL','HOU','IAD','JFK','LAS','LAX','MCO','NYC','ORD','PBI','PHL','PIT','RDU','RIC','RSW','SAV','SFO','TPA','WAS','XFL','XSF','ZLA',0];
var rVIJ = ['ABQ','ACK','ALB','AUG','AUS','BDL','BHB','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DTW','EWR','FLL','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','MCO','MVY','NAS','OAK','OGS','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PUJ','PVC','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SLK','STI','SWF','SYR','TPA',0];
var rVNX = ['ACK','AUS','BOS','BTV','BUF','BUR','CHS','CUN','DEN','FLL','GCM','HOU','JAX','JFK','LAS','LAX','LGB','MBJ','MCO','MVY','NAS','NYC','OAK','ORD','PBI','PDX','PHX','PWM','ROC','SAN','SAV','SEA','SFO','SJC','SLC','SMF','SYR','TPA','XBO','XFL','XSF','ZLA',0];
var rVQS = ['ABQ','ACK','ALB','AUG','AUS','BDL','BHB','BOS','BTV','BUF','BUR','BWI','DCA','DEN','DTW','EWR','FLL','HPN','HYA','IAD','JAX','JFK','LAS','LAX','LEB','LGA','LGB','MCO','MVY','NAS','NYC','OAK','OGS','ORD','ORH','PBI','PDX','PHL','PHX','PIT','PUJ','PVC','PVD','PWM','RDU','RIC','RKD','ROC','RUT','SAN','SDQ','SEA','SFO','SJC','SJU','SLC','SLK','SMF','STI','SWF','SXM','SYR','TPA','WAS','XBO','XDR','XFL','XSF','ZLA',0];
var rVRN = ['BDA','BUF','BWI','CHS','DCA','DFW','EWR','FLL','HOU','IAD','JFK','LAX','MCO','NYC','ORD','PHL','PIT','RDU','RIC','RSW','SAV','SEA','SFO','TPA','WAS','XFL','XSF','ZLA',0];
var rWAS = ['ABQ','ABZ','ACK','ADB','ALB','AMD','AMS','ANU','ARN','AUA','AUG','AUS','AXA','BAH','BCN','BDA','BDL','BGI','BHB','BHD','BHX','BIM','BKK','BLK','BLQ','BLR','BNE','BOD','BOG','BOH','BOJ','BOM','BOS','BQN','BRS','BRU','BTV','BUF','BUR','CDG','CHS','CMB','COK','CPH','CTA','CUN','CWL','DEL','DEN','DFW','DMM','DOH','DOM','DUB','DXB','EDI','EIS','ELH','EWB','EYW','FCO','FDF','FLL','FPO','GGT','GHB','GLA','GND','GNV','HAJ','HKG','HKT','HOU','HPN','HYA','IOM','ISB','JAX','JED','JER','JFK','KEF','KHI','KIN','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIM','LIR','LYS','MAN','MAZ','MBJ','MCO','MCT','MED','MHH','MLE','MSS','MSY','MVY','MXP','NAP','NAS','NCE','NCL','NEV','NYC','OAK','OGS','ORD','ORK','OSL','PAP','PBI','PDX','PER','PEW','PGF','PHX','PLS','PMI','PNS','POP','POS','PRG','PSE','PTP','PUJ','PVC','PWM','RKD','RNS','ROC','RSW','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SLC','SLK','SLU','SMF','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SXF','SXM','SYR','TCB','TLH','TLS','TPA','TRD','VCE','VIE','VQS','VRN','WAW','XBO','XDR','XFL','XSF','ZLA','ZRH',0];
var rWAW = ['BUF','BWI','DCA','DFW','EWR','FLL','IAD','JFK','MCO','NYC','ORD','PHL','PIT','RIC','RSW','TPA','WAS','XFL',0];
var rWDH = ['ABQ','AUS','BOS','BTV','BUF','BUR','DEN','HOU','LAS','LAX','LGB','MVY','OAK','ORD','PDX','PHX','ROC','SAN','SEA','SFO','SJC','SLC','SMF','SYR',0];
var rWVB = ['ABQ','BUF','BUR','DEN','LAS','LAX','LGB','OAK','ORD','PDX','PHX','ROC','SAN','SEA','SFO','SJC','SLC','SMF','SYR','XFL','XSF','ZLA',0];
var rXBO = ['ABQ','ABZ','ACK','ADD','ALB','AMD','AMS','ANC','ANU','APL','ARN','ATH','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BEW','BFN','BGI','BHB','BHX','BIM','BKK','BLR','BNE','BOG','BOM','BQN','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CTG','CUN','CUR','DAC','DAR','DCA','DEL','DEN','DFW','DMM','DOH','DOM','DUB','DXB','EIS','ELH','ELS','EWR','EYW','FDF','FLL','FPO','GBE','GCM','GGT','GHB','GND','GNV','GRJ','HDS','HKG','HKT','HNL','HOU','HRE','IAD','ISB','ITO','JAX','JFK','JNB','KBL','KEF','KHI','KIM','KIN','KOA','KUL','KWI','LAS','LAX','LEB','LGB','LGW','LHE','LHR','LIH','LIM','LIR','LLW','LNY','LRM','LVI','MAN','MAZ','MBJ','MCN','MCO','MCT','MDE','MHH','MKK','MLE','MPM','MQP','MSU','MSY','MTS','MUB','MVY','MXP','NAS','NEV','NLA','NYC','OAK','OGG','OGS','ORD','OSL','PAP','PBI','PDX','PER','PEW','PHL','PHW','PHX','PIT','PLS','PNS','POL','POP','POS','PSE','PTG','PTP','PUJ','PVC','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SAV','SDQ','SEA','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SKT','SLC','SLK','SMF','SNN','SRQ','STI','STT','STX','SVD','SVG','SXM','SYD','SYR','SZK','TCB','TET','TLH','TNR','TPA','TRD','UTN','UTT','UVF','VFA','VNX','VQS','WAS','XDR','XFL','XSF','ZLA',0];
var rXDR = ['ABQ','ABZ','ACK','ADD','ALB','AMD','AMS','ANU','ARN','ATH','AUG','AUS','AXA','BAH','BDL','BHB','BHX','BIM','BKK','BLR','BNE','BOM','BOS','BRS','BTV','BUF','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','DAC','DAR','DCA','DEL','DEN','DFW','DMM','DOH','DOM','DTW','DUB','DXB','EIS','EWR','EYW','FDF','FLL','FPO','GND','GNV','HKG','HKT','HOU','HPN','HYA','IAD','ISB','JAX','JFK','JNB','KBL','KEF','KHI','KUL','LAS','LAX','LEB','LGA','LGB','LHE','LHR','MAZ','MCO','MCT','MHH','MLE','MSY','MVY','NEV','NYC','OAK','OGS','ORD','ORH','OSL','PBI','PDX','PER','PEW','PHL','PHX','PIT','PNS','PTP','PVC','PVD','PVG','PWM','RDU','RIC','RKD','ROC','RUH','RUT','SAN','SAV','SEA','SEZ','SFO','SGN','SIN','SJC','SJU','SKB','SKT','SLC','SLK','SLU','SMF','SNN','STT','STX','SVD','SVG','SWF','SYR','TCB','TLH','TPA','TRD','VQS','WAS','XBO','XFL','XSF','ZLA',0];
var rXFL = ['ABQ','ABZ','ACK','ADD','ALB','AMD','AMS','ANU','APL','ARN','ATH','AUG','AUS','AXA','BAH','BBK','BCN','BDA','BDL','BEW','BFN','BHB','BHD','BHX','BIM','BKK','BLK','BLQ','BLR','BNE','BOD','BOG','BOH','BOM','BOS','BRS','BRU','BSR','BTV','BUF','BUQ','BUR','BWI','CCJ','CCU','CDG','CGK','CHS','CMB','COK','CPH','CTG','CUN','CWL','DAC','DAR','DCA','DEL','DEN','DMM','DOH','DOM','DTW','DUB','DXB','EDI','EIS','ELH','ELS','EWB','EWR','EYW','FBM','FCO','FDF','FPO','GBE','GGT','GHB','GLA','GND','GNV','GRJ','HAJ','HDS','HKG','HKT','HNL','HPN','HRE','HYA','IAD','IOM','ISB','JAX','JED','JER','JFK','JNB','KBL','KEF','KHI','KIM','KIN','KUL','KWI','LAS','LAX','LEB','LGA','LGB','LHE','LHR','LIH','LIM','LLW','LNY','LOS','LVI','LYS','MAN','MAZ','MCT','MDE','MED','MHH','MKK','MLE','MPM','MQP','MSS','MSU','MTS','MUB','MVY','MXP','NAP','NAS','NCE','NCL','NEV','NLA','NYC','OAK','OGG','OGS','ORD','ORH','ORK','OSL','PAP','PDX','PER','PEW','PHL','PHW','PHX','PIT','PMI','POL','POS','PRG','PTG','PTP','PVC','PVD','PVG','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RUH','RUT','SAN','SCQ','SDQ','SEA','SEN','SEZ','SFO','SGN','SIN','SJC','SJO','SJU','SKB','SKT','SLC','SLK','SLU','SMF','SNN','STI','STR','STT','STX','SVD','SVG','SWF','SXF','SYD','SYR','SZK','TCB','TET','TLH','TLS','TNR','TRD','UTN','UTT','VCE','VFA','VIE','VNX','VQS','VRN','WAS','WAW','WVB','XBO','XDR','XSF','ZLA','ZRH',0];
var rXSF = ['ABZ','ACE','ACK','ADD','ALB','AMD','AMS','ANC','ANU','APL','ATH','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BDL','BEW','BFN','BHB','BHX','BIM','BKK','BLK','BLQ','BLR','BNE','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BTV','BUF','BUQ','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','DAC','DAR','DCA','DEL','DMM','DOH','DOM','DUB','DXB','EDI','EIS','ELH','ELS','EWB','EWR','EYW','FDF','FLL','FPO','GBE','GCM','GGT','GHB','GLA','GND','GNV','GRJ','HAJ','HDS','HNL','HPN','HRE','HYA','IAD','ITO','JAX','JER','JFK','JNB','KHI','KIM','KIN','KOA','LAS','LEB','LGA','LGB','LHR','LIH','LIM','LLW','LOS','LPA','LRM','LVI','LYS','MAZ','MBJ','MCO','MCT','MHH','MLE','MPM','MQP','MSS','MSU','MTS','MUB','MVY','MXP','NAS','NCE','NCL','NEV','NLA','NYC','OGG','OGS','ORK','OSL','PAP','PBI','PDX','PHL','PHW','PIT','PLS','POL','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAV','SCQ','SDQ','SEA','SEN','SEZ','SJO','SJU','SKB','SLC','SLK','SLU','SNN','SRQ','STI','STR','STT','STX','SVD','SVG','SWF','SXF','SXM','SYR','SZK','TCB','TET','TLH','TLS','TNR','TPA','TRD','UTN','VFA','VIE','VNX','VQS','VRN','WAS','WVB','XBO','XDR','XFL','ZLA',0];
var rZLA = ['ABZ','ACE','ACK','ADD','ALB','AMD','AMS','ANC','ANU','APL','ATH','AUA','AUG','AUS','AXA','AZS','BAH','BBK','BDA','BDL','BEW','BFN','BGI','BHB','BHX','BIM','BKK','BLK','BLR','BNE','BOD','BOG','BOH','BOM','BOS','BQN','BRS','BTV','BUF','BUQ','BWI','CCJ','CCU','CDG','CGK','CHS','CLT','CMB','COK','CPH','CTG','CUN','CUR','DAC','DAR','DCA','DEL','DMM','DOH','DOM','DTW','DUB','DXB','EDI','EIS','ELH','ELS','EWB','EWR','EYW','FDF','FLL','GBE','GGT','GHB','GLA','GND','GRJ','HAJ','HDS','HNL','HPN','HRE','HYA','IAD','ITO','JAX','JED','JER','JFK','JNB','KHI','KIM','KIN','KOA','KWI','LAS','LEB','LGA','LHR','LIH','LIM','LLW','LOS','LRM','LVI','LYS','MAZ','MBJ','MCO','MCT','MHH','MLE','MPM','MQP','MSS','MSU','MTS','MUB','MVY','MXP','NAS','NCE','NEV','NLA','NYC','OAK','OGG','OGS','ORK','OSL','PAP','PBI','PDX','PHL','PHW','PIT','PLS','POL','POP','POS','PRG','PSE','PTG','PTP','PUJ','PVC','PWM','PZB','RCB','RDU','RIC','RKD','ROC','RSW','RUH','RUT','SAN','SAV','SCQ','SDQ','SEA','SEN','SEZ','SFO','SJO','SJU','SKB','SLC','SLK','SLU','SMF','SNN','STI','STR','STT','STX','SVD','SVG','SWF','SXF','SXM','SYR','SZK','TCB','TET','TLH','TLS','TNR','TRD','UTN','UVF','VFA','VIE','VNX','VQS','VRN','WAS','WVB','XBO','XDR','XFL','XSF',0];
var rZRH = ['ACK','AUA','AUS','BDA','BOS','BQN','BTV','BUF','BWI','CHS','CLT','DCA','DFW','DTW','EWR','FLL','HOU','IAD','JAX','JFK','LAS','LAX','MCO','MSY','NAS','ORD','PBI','PHL','PIT','PWM','RDU','RIC','ROC','RSW','SAN','SAV','SDQ','SEA','SFO','SJU','STI','SYR','TPA','WAS','XFL',0];

// Region Data
var aRegions = [
{"code":"INTERLINE.AFRICA","name":"Africa","countries":["AO","BJ","BW","CD","CG","DZ","EG","ER","ET","GA","GH","GQ","KE","LY","LS","MA","MG","MZ","MU","MW","NA","NG","OM","SD","SN","SZ","SC","TN","TZ","UG","YE","ZA","ZM","ZW"]},
{"code":"http://www.jetblue.com/ajax-data/INTERLINE.ASIA","name":"Asia","countries":["AF","BD","CN","HK","ID","IN","JP","KZ","KH","KR","LK","MV","MY","PK","PH","SG","TH","TM","UZ","VN"]},
{"code":"INTERLINE.CARRIBEAN_C-AMERICA","name":"Caribbean & Central America","countries":["AW","AI","AG","BS","BZ","BM","BO","BB","CR","CW","KY","DM","DO","GP","GD","GT","HN","HT","JM","KN","LC","MX","MQ","NI","PA","PR","SV","SX","TC","TT","VC","VG","VI"]},
{"code":"INTERLINE.EUROPE","name":"Europe","countries":["AL","AT","AZ","BE","BG","BA","BY","CH","CY","CZ","DE","DK","ES","EE","FI","FR","GB","GE","GR","HR","HU","IE","IS","IT","LT","LU","LV","MD","MT","NL","NO","PL","PT","RO","RU","RS","SE","TR","UA"]},
{"code":"INTERLINE.MIDDLEEAST","name":"Middle East","countries":["OM","AE","BH","IR","IQ","IL","JO","KW","LB","QA","SA"]},
{"code":"INTERLINE.PACIFIC","name":"Pacific","countries":["AS","AU","NZ","PF"]},
{"code":"INTERLINE.S-AMERICA","name":"South America","countries":["BO","AR","BR","CL","CO","EC","FK","PE","UY","VE"]},
{"code":"http://www.jetblue.com/ajax-data/INTERLINE.US","name":"United States","countries":["US"]},
0]

// End City Data Block
