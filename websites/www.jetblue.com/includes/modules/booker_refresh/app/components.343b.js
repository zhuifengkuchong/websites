/*! 16-05-2015 */
BookerWrapper = angular.module("BookerWrapper").controller("Booker", [ "$scope", "$rootScope", "$timeout", "BookerConfig", "jbOverlayFactory", "AirportData", "$filter", "$window", function($scope, $rootScope, $timeout, BookerConfig, jbOverlayFactory, AirportData, $filter, $window) {
    var storageLib, recentSearchesStub, key, getRegionName, getSelectedRegionContries;
    key = {
        left: 37,
        up: 38,
        right: 39,
        down: 40,
        enter: 13,
        esc: 27,
        tab: 9
    };
    $rootScope.getBookerInfo = function() {
        return {
            flight_type: $scope.itinerarySelect,
            from_field: $scope.fromSelectedObj.code,
            from_field_full: $scope.fromSelectedObj.label,
            to_field: $scope.toSelectedObj.code,
            to_field_full: $scope.toSelectedObj.label,
            fare_display: $scope.currency,
            adult_count: $scope.passenger.flightsAdults,
            minor_count: $scope.passenger.flightsMinors,
            infant_count: $scope.passenger.flightsInfants,
            departDate: $scope.departCal.date,
            returnDate: $scope.returnCal.date
        };
    };
    $scope.showErrors = false;
    $scope.showWarnings = false;
    $scope.getawayMinorAge = {};
    $scope.errorMessages = [];
    $scope.warningMessages = [];
    $scope.seasonalFlightMsg = "";
    $scope.isSeasonalFlight = false;
    $scope.citySelectorFooterText = "";
    $scope.MC = {};
    if (!$scope.promo) {
        $scope.promo = "";
    }
    $scope.type = "homepage";
    $scope.config = {};
    $.extend($scope.config, BookerConfig);
    $scope.$watch("type", function() {
        $.extend(true, $scope, BookerConfig.scopeDefaults.all);
        if (!!$scope.type && !!BookerConfig.scopeDefaults[$scope.type]) {
            $.extend(true, $scope, BookerConfig.scopeDefaults[$scope.type]);
        }
        setFlexDefault();
        $scope.initAirportData();
        $scope.$emit("BOOKER_CONTROLLER_LOADED");
    });
    $scope.initAirportData = function() {
        var formatLookup, odTypeLookup;
        odTypeLookup = {
            homepage: {
                flights: "ALL",
                getaways: "GETAWAYS"
            },
            mini: {
                flights: "ALL"
            },
            multicity: {
                flights: "ALL"
            },
            bff: {
                flights: "JBONLY"
            },
            vacations: {
                getaways: "GETAWAYS"
            }
        };
        formatLookup = {
            homepage: {
                flights: false,
                getaways: false
            },
            mini: {
                flights: false
            },
            multicity: {
                flights: false
            },
            bff: {
                flights: false
            },
            vacations: {
                getaways: false
            }
        };
        AirportData.init({
            odType: odTypeLookup[$scope.type][$scope.searchType],
            format: formatLookup[$scope.type][$scope.searchType],
            success: function() {
                $scope.fromLAirports = $scope.toLAirports = AirportData.StationList({
                    format: "suggestion",
                    filter: $scope.citySelector.filter
                });
                $scope.fromPAirports = $scope.toPAirports = AirportData.StationList({
                    sort: "ASC",
                    group: "region",
                    regionOptions: {
                        flatten: [ "United States" ]
                    },
                    filter: $scope.citySelector.filter
                });
                var defaultObj = {
                    label: "",
                    code: "",
                    region: "United States"
                };
                if (!$scope.fromSelectedObj || !$scope.fromSelectedObj.code) {
                    $scope.fromSelectedObj = {};
                    $.extend($scope.fromSelectedObj, defaultObj);
                }
                if (!$scope.toSelectedObj || !$scope.toSelectedObj.code) {
                    $scope.toSelectedObj = {};
                    $.extend($scope.toSelectedObj, defaultObj);
                }
                $scope.populateFromInit();
                if ($scope.type == "multicity") {
                    multicitySelections();
                }
            },
            error: function() {
                $scope.$emit("OD API FAILURE");
            }
        });
    };
    $scope.setAdditionalProducts = function(type) {
        $scope.additionalProducts = [];
        if (BookerConfig[type].additionalProducts && BookerConfig[type].additionalProducts.length > 0) {
            for (var j = 0; j < BookerConfig[type].additionalProducts.length; j++) {
                $scope.additionalProducts.push({
                    model: 1,
                    options: $scope.formatAdditionalProducts(BookerConfig[type].additionalProducts[j])
                });
            }
        }
    };
    $scope.formatAdditionalProducts = function(prod) {
        var vals = prod.validAmounts;
        var options = [];
        for (var i = 0; i < vals.length; i++) {
            var option = {};
            if (vals[i] == 1) {
                option.name = vals[i] + " " + prod.labels.singular;
            } else {
                option.name = vals[i] + " " + prod.labels.plural;
            }
            option.value = vals[i];
            options.push(option);
        }
        return options;
    };
    $scope.redemption = $scope.currency == "tb" ? "true" : "false";
    $scope.$watch("searchType", function() {
        $scope.initAirportData();
        $scope.setAdditionalProducts($scope.searchType);
        $scope.updatePassengerOptions();
        $scope.onSearchTypeChange();
        $(window).trigger("BOOKER_SEARCHTYPE_CHANGE");
    });
    $scope.openModal = function($event, src, text) {
        var h, w;
        h = $window.innerHeight < 600 ? $window.innerHeight * .75 : 600;
        h = h + "px";
        w = $window.innerWidth < 850 ? $window.innerWidth * .75 : 850;
        w = w + "px";
        if (src) {
            jbOverlayFactory.init({
                title: text,
                template: src,
                height: h,
                width: w
            });
            jbOverlayFactory.show();
            $event.preventDefault();
        }
    };
    setFlexDefault = function() {
        if ($scope.config.flexibleSearch.enabled && $scope.type == "homepage") {
            $scope.flexSearchOptIn = $scope.config.flexibleSearch.optInDefaultValue;
        } else {
            $scope.flexSearchOptIn = false;
        }
    };
    function setSchedExtDate() {
        if (scheduleExtDate && new Date(scheduleExtDate.month + "/" + scheduleExtDate.day + "/" + scheduleExtDate.year) > new Date()) {
            tempdate = new Date(scheduleExtDate.month + "/" + scheduleExtDate.day + "/" + scheduleExtDate.year);
        }
        $scope.scheduleExtDate = tempdate;
    }
    setSchedExtDate();
    function defaultCal() {
        this.date = null;
        this.isOpen = false;
        this.options = {
            startingDay: 0,
            showWeeks: false,
            showButtonBar: false
        };
        this.minDate = new Date();
        this.maxDate = $scope.scheduleExtDate;
        this.extDate = $scope.scheduleExtDate;
        this.extDate = this.extDate.toDateString();
    }
    $scope.departCal = new defaultCal();
    $scope.returnCal = new defaultCal();
    $scope.openDepartCalendar = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.departDateError = "";
        $scope.returnCal.isOpen = false;
        $scope.departCal.isOpen = !!!$scope.departCal.isOpen;
    };
    $scope.openReturnCalendar = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.returnDateError = "";
        $scope.departCal.isOpen = false;
        $scope.returnCal.isOpen = !!!$scope.returnCal.isOpen;
    };
    $scope.$watch("http://www.jetblue.com/includes/modules/booker_refresh/app/departCal.date", function(newVal, oldVal) {
        $(".clndr2 .active").removeClass("active");
        if (!!newVal) {
            $scope.returnCal.minDate = newVal;
            if ($scope.returnCal.date < $scope.returnCal.minDate) {
                $scope.returnCal.date = "";
            }
        }
    });
    $scope.format = "MM-dd-yyyy";
    getRegionName = function(airportCode) {
        var regionCode, regionName = "";
        if (airportCode && AirportData.Station(airportCode)) {
            regionCode = AirportData.Station(airportCode).RegionCodes[0];
            regionName = AirportData.regions[regionCode].Name;
        }
        return regionName;
    };
    var defaultObj = {
        label: "",
        code: "",
        region: "United States"
    };
    $scope.isInterlineRoute = "false";
    $scope.isSharedMarketRoute = "false";
    $scope.setRouteFlags = function() {
        var lookup;
        lookup = AirportData.Route($scope.fromSelectedObj.code, $scope.toSelectedObj.code);
        if (!!lookup) {
            $scope.isInterlineRoute = !!lookup.i ? "true" : "false";
            $scope.isSharedMarketRoute = !!lookup.c ? "true" : "false";
        }
    };
    $scope.onFromSelection = function(label, code) {
        var region;
        if (!label) {
            return;
        }
        if (!code) {
            code = AirportData.Station(label).Code;
        }
        region = getRegionName(code);
        $scope.toLAirports = AirportData.StationList({
            filter: $scope.citySelector.filter.concat([ {
                destination: code
            } ]),
            format: "suggestion"
        });
        $scope.fromSelectedObj = {
            label: label,
            code: code,
            region: region
        };
        $scope.fromResult = label;
        $scope.toPAirports = AirportData.StationList({
            filter: $scope.citySelector.filter.concat([ {
                destination: code
            } ]),
            sort: "ASC",
            group: "region",
            regionOptions: {
                flatten: [ "United States" ]
            }
        });
        if (!$scope.toLAirports.length) {
            $scope.toLAirports = AirportData.StationList({
                filter: $scope.citySelector.filter,
                format: "suggestion"
            });
        }
        if (!$scope.toPAirports.length) {
            $scope.toPAirports = AirportData.StationList({
                filter: $scope.citySelector.filter,
                sort: "ASC",
                group: "region",
                regionOptions: {
                    flatten: [ "United States" ]
                }
            });
        }
        if (region) {
            angular.forEach($scope.fromPAirports, function(regionitem) {
                if (regionitem.region == region) {
                    regionitem.selected = true;
                } else {
                    regionitem.selected = false;
                }
                angular.forEach(regionitem, function(region) {
                    angular.forEach(regionitem.countries, function(country) {
                        angular.forEach(country.airports, function(airport) {
                            if (airport.Code == code) {
                                airport.selected = true;
                            } else {
                                airport.selected = false;
                            }
                        });
                    });
                });
            });
        } else {
            angular.forEach($scope.toPAirports, function(regionitem) {
                regionitem.selected = false;
            });
            $scope.fromPAirports[0].selected = true;
        }
        $scope.setRouteFlags();
        if (typeof $scope.toSelectedObj.label === "string" && $scope.toSelectedObj.label.length > 0) {
            $scope.checkReturnCity();
        }
    };
    $scope.onToSelection = function(label, code) {
        var region;
        if (!label) {
            return;
        }
        if (!code) {
            code = AirportData.Station(label).Code;
        }
        region = getRegionName(code);
        $scope.fromLAirports = AirportData.StationList({
            filter: $scope.citySelector.filter.concat([ {
                origin: code
            } ]),
            format: "suggestion"
        });
        $scope.toSelectedObj = {
            label: label,
            code: code,
            region: region
        };
        $scope.toResult = label;
        $scope.fromPAirports = AirportData.StationList({
            filter: $scope.citySelector.filter.concat([ {
                origin: code
            } ]),
            sort: "ASC",
            group: "region",
            regionOptions: {
                flatten: [ "United States" ]
            }
        });
        if (region) {
            angular.forEach($scope.toPAirports, function(regionitem) {
                if (regionitem.region == region) {
                    regionitem.selected = true;
                } else {
                    regionitem.selected = false;
                }
                angular.forEach(regionitem, function(region) {
                    angular.forEach(regionitem.countries, function(country) {
                        angular.forEach(country.airports, function(airport) {
                            if (airport.Code == code) {
                                airport.selected = true;
                            } else {
                                airport.selected = false;
                            }
                        });
                    });
                });
            });
        } else {
            angular.forEach($scope.toPAirports, function(regionitem) {
                regionitem.selected = false;
            });
            $scope.toPAirports[0].selected = true;
        }
        $scope.setRouteFlags();
        if (typeof $scope.fromSelectedObj.label === "string" && $scope.fromSelectedObj.label.length > 0) {
            $scope.checkDepartCity();
        }
    };
    $scope.onFromClear = function() {
        if ($scope.toResult == "") {
            $scope.resetODOptions();
        }
    };
    $scope.onToClear = function() {
        if ($scope.fromResult == "") {
            $scope.resetODOptions();
        }
    };
    $scope.resetODOptions = function() {
        $scope.toLAirports = AirportData.StationList({
            filter: $scope.citySelector.filter,
            format: "suggestion"
        });
        $scope.toPAirports = AirportData.StationList({
            filter: $scope.citySelector.filter,
            sort: "ASC",
            group: "region",
            regionOptions: {
                flatten: [ "United States" ]
            }
        });
        $scope.fromLAirports = AirportData.StationList({
            filter: $scope.citySelector.filter,
            format: "suggestion"
        });
        $scope.fromPAirports = AirportData.StationList({
            filter: $scope.citySelector.filter,
            sort: "ASC",
            group: "region",
            regionOptions: {
                flatten: [ "United States" ]
            }
        });
        angular.forEach($scope.toPAirports, function(regionitem) {
            regionitem.selected = false;
            angular.forEach(regionitem, function(region) {
                angular.forEach(regionitem.countries, function(country) {
                    angular.forEach(country.airports, function(airport) {
                        airport.selected = false;
                    });
                });
            });
        });
        angular.forEach($scope.fromPAirports, function(regionitem) {
            regionitem.selected = false;
            angular.forEach(regionitem, function(region) {
                angular.forEach(regionitem.countries, function(country) {
                    angular.forEach(country.airports, function(airport) {
                        airport.selected = false;
                    });
                });
            });
        });
        angular.forEach($scope.fromPAirports, function(regionitem) {
            regionitem.selected = false;
        });
        $scope.fromPAirports[0].selected = true;
        angular.forEach($scope.toPAirports, function(regionitem) {
            regionitem.selected = false;
        });
        $scope.toPAirports[0].selected = true;
    };
    function multicitySelections() {
        $scope.clearTripLine = function(id) {
            $scope["toSelectedObj" + id] = $.extend({}, defaultObj);
            $scope["fromSelectedObj" + id] = $.extend({}, defaultObj);
            $scope.MC["fromResult" + id] = "";
            $scope["fromSelectedObj" + id].label = "";
            $scope["fromSelectedObj" + id].code = "";
            $scope.MC["toResult" + id] = "";
            $scope["toSelectedObj" + id].label = "";
            $scope["toSelectedObj" + id].code = "";
            $scope["departCal" + parseInt(id)].date = null;
        };
        $scope.departCal1 = new defaultCal();
        $scope.departCal2 = new defaultCal();
        $scope.departCal3 = new defaultCal();
        $scope.departCal4 = new defaultCal();
        $scope.thirdTripLine = $scope.fourthTripLine = "disabled_line";
        $scope.openMultiDepartCalendar = function($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            switch (id) {
              case 1:
                $scope.departCal2.isOpen = false;
                $scope.departCal3.isOpen = false;
                $scope.departCal4.isOpen = false;
                break;

              case 2:
                $scope.departCal1.isOpen = false;
                $scope.departCal3.isOpen = false;
                $scope.departCal4.isOpen = false;
                break;

              case 3:
                $scope.departCal1.isOpen = false;
                $scope.departCal2.isOpen = false;
                $scope.departCal4.isOpen = false;
                break;

              case 4:
                $scope.departCal1.isOpen = false;
                $scope.departCal2.isOpen = false;
                $scope.departCal3.isOpen = false;
                break;
            }
            $scope["departCal" + id].isOpen = !!!$scope["departCal" + id].isOpen;
        };
        $scope.$watch("http://www.jetblue.com/includes/modules/booker_refresh/app/departCal1.date", function(newVal, oldVal) {
            $(".clndr2 .active").removeClass("active");
            $(".clndr3 .active").removeClass("active");
            $(".clndr4 .active").removeClass("active");
            if (!!newVal) {
                $scope.departCal2.minDate = newVal;
                $scope.departCal3.minDate = newVal;
                $scope.departCal4.minDate = newVal;
                if ($scope.departCal2.date < $scope.departCal1.date) {
                    $scope.departCal2.date = null;
                    $scope.departCal3.date = null;
                    $scope.departCal4.date = null;
                }
            }
        });
        $scope.$watch("http://www.jetblue.com/includes/modules/booker_refresh/app/departCal2.date", function(newVal, oldVal) {
            $(".clndr1 .active").removeClass("active");
            $(".clndr2 .active").removeClass("active");
            $(".clndr4 .active").removeClass("active");
            if (!!newVal) {
                $scope.departCal3.minDate = newVal;
                $scope.departCal4.minDate = newVal;
                if ($scope.departCal3.date < $scope.departCal2.date) {
                    $scope.departCal3.date = null;
                    $scope.departCal4.date = null;
                }
            }
        });
        $scope.$watch("http://www.jetblue.com/includes/modules/booker_refresh/app/departCal3.date", function(newVal, oldVal) {
            $(".clndr1 .active").removeClass("active");
            $(".clndr2 .active").removeClass("active");
            $(".clndr4 .active").removeClass("active");
            if (!!newVal) {
                $scope.departCal4.minDate = newVal;
                if ($scope.departCal4.date < $scope.departCal3.date) {
                    $scope.departCal4.date = null;
                }
            }
        });
        $scope.$watchCollection("[departCal3.date, MC.fromResult3, MC.toResult3]", function(newVal, oldVal) {
            if ((typeof $scope.departCal3.date === "undefined" || $scope.departCal3.date === null) && ($scope.MC.fromResult3 === "" || typeof $scope.MC.fromResult3 === "undefined") && ($scope.MC.toResult3 === "" || typeof $scope.MC.toResult3 === "undefined")) {
                $scope.thirdTripLine = "disabled_line";
            } else {
                $scope.thirdTripLine = "";
            }
        });
        $scope.$watchCollection("[departCal4.date, MC.fromResult4, MC.toResult4]", function(newVal, oldVal) {
            if ((typeof $scope.departCal4.date === "undefined" || $scope.departCal4.date === null) && ($scope.MC.fromResult4 == "" || typeof $scope.MC.fromResult4 === "undefined") && ($scope.MC.toResult4 == "" || typeof $scope.MC.toResult4 === "undefined")) {
                $scope.fourthTripLine = "disabled_line";
            } else {
                $scope.fourthTripLine = "";
            }
        });
        $scope.format = "MM-dd-yyyy";
        $scope.fromLAirports1 = $scope.fromLAirports2 = $scope.fromLAirports3 = $scope.fromLAirports4 = $scope.toLAirports1 = $scope.toLAirports2 = $scope.toLAirports3 = $scope.toLAirports4 = AirportData.StationList({
            format: "suggestion"
        });
        $scope.fromPAirports1 = $scope.fromPAirports2 = $scope.fromPAirports3 = $scope.fromPAirports4 = $scope.toPAirports1 = $scope.toPAirports2 = $scope.toPAirports3 = $scope.toPAirports4 = AirportData.StationList({
            sort: "ASC",
            group: "region",
            regionOptions: {
                flatten: [ "United States" ]
            }
        });
        $scope.fromSelectedObj1 = {};
        $scope.toSelectedObj1 = {};
        $scope.fromSelectedObj2 = {};
        $scope.toSelectedObj2 = {};
        $scope.fromSelectedObj3 = {};
        $scope.toSelectedObj3 = {};
        $scope.fromSelectedObj4 = {};
        $scope.toSelectedObj4 = {};
        $.extend($scope.fromSelectedObj1, defaultObj);
        $.extend($scope.toSelectedObj1, defaultObj);
        $.extend($scope.fromSelectedObj2, defaultObj);
        $.extend($scope.toSelectedObj2, defaultObj);
        $.extend($scope.fromSelectedObj3, defaultObj);
        $.extend($scope.toSelectedObj3, defaultObj);
        $.extend($scope.fromSelectedObj4, defaultObj);
        $.extend($scope.toSelectedObj4, defaultObj);
        $scope.onMultiFromSelection = function(label, code, id) {
            var region;
            if (!label) {
                return;
            }
            if (!code) {
                code = AirportData.Station(label).Code;
            }
            region = getRegionName(code);
            $scope["toLAirports" + id] = AirportData.StationList({
                filter: $scope.citySelector.filter.concat([ {
                    destination: code
                } ]),
                format: "suggestion"
            });
            $scope["toPAirports" + id] = AirportData.StationList({
                filter: $scope.citySelector.filter.concat([ {
                    destination: code
                } ]),
                sort: "ASC",
                group: "region",
                regionOptions: {
                    flatten: [ "United States" ]
                }
            });
            $scope["fromSelectedObj" + id].label = label;
            $scope["fromSelectedObj" + id].code = code;
            $scope["fromSelectedObj" + id].region = region;
            $scope.MC["fromResult" + id] = label;
            $scope.mcCityError["fromResult" + id] = "";
            if (region) {
                angular.forEach($scope["toPAirports" + id], function(regionitem) {
                    if (regionitem.region == region) {
                        regionitem.selected = true;
                    } else {
                        regionitem.selected = false;
                    }
                    angular.forEach(regionitem, function(region) {
                        angular.forEach(regionitem.countries, function(country) {
                            angular.forEach(country.airports, function(airport) {
                                if (airport.Code == code) {
                                    airport.selected = true;
                                } else {
                                    airport.selected = false;
                                }
                            });
                        });
                    });
                });
            } else {
                angular.forEach($scope["toPAirports" + id], function(regionitem) {
                    regionitem.selected = false;
                });
                $scope["toPAirports" + id][0].selected = true;
            }
        };
        $scope.onMultiToSelection = function(label, code, id) {
            var region;
            if (!label) {
                return;
            }
            if (!code) {
                code = AirportData.Station(label).Code;
            }
            region = getRegionName(code);
            $scope["fromLAirports" + id] = AirportData.StationList({
                filter: $scope.citySelector.filter.concat([ {
                    destination: code
                } ]),
                format: "suggestion"
            });
            $scope["fromPAirports" + id] = AirportData.StationList({
                filter: $scope.citySelector.filter.concat([ {
                    destination: code
                } ]),
                sort: "ASC",
                group: "region",
                regionOptions: {
                    flatten: [ "United States" ]
                }
            });
            $scope["toSelectedObj" + id].label = label;
            $scope["toSelectedObj" + id].code = code;
            $scope["toSelectedObj" + id].region = region;
            $scope.MC["toResult" + id] = label;
            $scope.mcCityError["toResult" + id] = "";
            if (region) {
                angular.forEach($scope["fromPAirports" + id], function(regionitem) {
                    if (regionitem.region == region) {
                        regionitem.selected = true;
                    } else {
                        regionitem.selected = false;
                    }
                    angular.forEach(regionitem, function(region) {
                        angular.forEach(regionitem.countries, function(country) {
                            angular.forEach(country.airports, function(airport) {
                                if (airport.Code == code) {
                                    airport.selected = true;
                                } else {
                                    airport.selected = false;
                                }
                            });
                        });
                    });
                });
            } else {
                angular.forEach($scope["fromPAirports" + id], function(regionitem) {
                    regionitem.selected = false;
                });
                $scope["fromPAirports" + id][0].selected = true;
            }
        };
        $scope.onMultiFromClear = function(id) {
            return;
            if ($scope.MC["toResult" + id] == "" && $scope.MC["fromResult" + id] == "") {
                $scope.resetMultiODOptions(id);
            }
        };
        $scope.onMultiToClear = function(id) {
            return;
            if ($scope.MC["fromResult" + id] == "" && $scope.MC["toResult" + id] == "") {
                $scope.resetMultiODOptions(id);
            }
        };
        $scope.resetMultiODOptions = function(id) {
            console.log("================================== RESET FOR:" + id);
            $scope["toLAirports" + id] = AirportData.StationList({
                filter: $scope.citySelector.filter,
                format: "suggestion"
            });
            $scope["toPAirports" + id] = AirportData.StationList({
                filter: $scope.citySelector.filter,
                sort: "ASC",
                group: "region",
                regionOptions: {
                    flatten: [ "United States" ]
                }
            });
            $scope["fromLAirports" + id] = AirportData.StationList({
                filter: $scope.citySelector.filter,
                format: "suggestion"
            });
            $scope["fromPAirports" + id] = AirportData.StationList({
                filter: $scope.citySelector.filter,
                sort: "ASC",
                group: "region",
                regionOptions: {
                    flatten: [ "United States" ]
                }
            });
            angular.forEach($scope["toPAirports" + id], function(regionitem) {
                regionitem.selected = false;
                angular.forEach(regionitem, function(region) {
                    angular.forEach(regionitem.countries, function(country) {
                        angular.forEach(country.airports, function(airport) {
                            airport.selected = false;
                        });
                    });
                });
            });
            angular.forEach($scope["fromPAirports" + id], function(regionitem) {
                regionitem.selected = false;
                angular.forEach(regionitem, function(region) {
                    angular.forEach(regionitem.countries, function(country) {
                        angular.forEach(country.airports, function(airport) {
                            airport.selected = false;
                        });
                    });
                });
            });
            angular.forEach($scope["fromPAirports" + id], function(regionitem) {
                regionitem.selected = false;
            });
            $scope["fromPAirports" + id][0].selected = true;
            angular.forEach($scope["toPAirports" + id], function(regionitem) {
                regionitem.selected = false;
            });
            $scope["toPAirports" + id][0].selected = true;
        };
        $scope.mcCityError = {
            fromResult1: "",
            toResult1: "",
            fromResult1: "",
            toResult2: ""
        };
        $scope.mcDateError = {
            departCal1: "",
            departCal2: ""
        };
        $scope.checkMCDate = function(model) {
            setTimeout(function() {
                $scope.mcDateError[model] = "";
                if (isInvalidDate($scope[model]["date"])) {
                    $scope.mcDateError[model] = BookerConfig.errorMessages.GENERIC_DATE_ERROR;
                }
            }, 0);
        };
        $scope.checkMCCity = function(model, id) {
            console.log("MC BLUR CHECK");
            $scope.mcCityError[model] = "";
            var value = $scope.MC[model];
            var showError = false;
            if (value) {
                if (model.indexOf("from") >= 0) {
                    showError = $scope["fromSelectedObj" + id].label != value;
                    if ($scope.MC["toResult" + id] == "") {
                        $scope.resetMultiODOptions(id);
                    }
                } else {
                    showError = $scope["toSelectedObj" + id].label != value;
                    if ($scope.MC["fromResult" + id] == "") {
                        $scope.resetMultiODOptions(id);
                    }
                }
            }
            if (!$scope.MC[model] || !value || showError) {
                $scope.mcCityError[model] = BookerConfig.errorMessages.GENERIC_CITY_ERROR;
            }
        };
    }
    $scope.setDefaultPassengers = function() {
        var itinTypes, passengerTypes, passengerGroups, labelUpdateEls;
        $scope.passenger = {};
        itinTypes = $.map($scope.config.searchTypes.types, function(el) {
            return el.value;
        });
        passengerGroups = $.map(itinTypes, function(el) {
            return $scope.config[el].ageGroups;
        });
        $.each(passengerGroups, function(i, el) {
            $scope.passenger[el.id] = el.defaultAmount;
        });
        $scope.passengerError = "";
    };
    $scope.setDefaultPassengers();
    $scope.passengerIterable = {};
    $scope.updatePassengerOptions = function() {
        var adultscount, ageGroups, activeAgeGroups, infantGroups, itinTypes, noinfants, oneinfant, twoinfants, threeinfants, passengerCount, passengerCountLookup, maxpassengers;
        noinfants = [ {
            name: "0 Lap Infants (Under 2)",
            value: 0
        } ];
        oneinfant = [ {
            name: "0 Lap Infants (Under 2)",
            value: 0
        }, {
            name: "1 Lap Infant",
            value: 1
        } ];
        twoinfants = [ {
            name: "0 Lap Infants (Under 2)",
            value: 0
        }, {
            name: "1 Lap Infant",
            value: 1
        }, {
            name: "2 Lap Infants",
            value: 2
        } ];
        threeinfants = [ {
            name: "0 Lap Infants (Under 2)",
            value: 0
        }, {
            name: "1 Lap Infant",
            value: 1
        }, {
            name: "2 Lap Infants",
            value: 2
        }, {
            name: "3 Lap Infants",
            value: 3
        } ];
        $scope.passengerIterable = {};
        jQuery.each($scope.passenger, function(k, v) {
            if (v > 0) {
                $scope.passengerIterable[k] = new Array(parseInt(v));
            }
        });
        itinTypes = $.map($scope.config.searchTypes.types, function(el) {
            return el.value;
        });
        ageGroups = $.map(itinTypes, function(el) {
            if (el) {
                return $scope.config[el].ageGroups;
            }
        });
        infantGroups = $.map(ageGroups, function(el) {
            if (el.bookingGroup == "INF") {
                return el;
            }
        });
        $scope.passengerError = "";
        $.each(infantGroups, function(i, el) {
            var parentgroup = $scope.passenger[el.id.replace("Infants", "Adults")], childgroup = $scope.passenger[el.id];
            if (parentgroup >= childgroup) {
                if (parentgroup === 0) {
                    el.options = noinfants;
                } else if (parentgroup == 1) {
                    el.options = oneinfant;
                } else if (parentgroup == 2) {
                    el.options = twoinfants;
                } else {
                    el.options = threeinfants;
                }
            }
        });
        activeAgeGroups = $.map($scope.config[$scope.searchType || $scope.config.searchTypes.defaultType].ageGroups, function(el) {
            if (!!el.ticketable) {
                return el.id;
            }
        });
        passengerCountLookup = $.map(activeAgeGroups, function(el) {
            return $scope.passenger[el];
        });
        passengerCount = 0;
        for (var i = 0; i < passengerCountLookup.length; i++) {
            passengerCount += passengerCountLookup[i];
        }
        $scope.passengerCount = passengerCount;
        if ($scope.searchType == "flights") {
            if (!$scope.passenger.flightsAdults && !$scope.passenger.flightsMinors) {
                $scope.passengerError = BookerConfig.errorMessages.NO_PASSENGER_ERROR;
            }
            if ($scope.passenger.flightsAdults < $scope.passenger.flightsInfants) {
                $scope.passengerError = BookerConfig.errorMessages.TOO_MANY_LAP_INFANTS;
            }
            if ($scope.passengerCount > $scope.config.CONSTANTS.GUESTLIMIT) {
                $scope.passengerError = BookerConfig.errorMessages.TOO_MANY_PASSENGERS;
            }
        } else if ($scope.searchType == "getaways") {
            if (!$scope.passenger.getawaysAdults && !$scope.passenger.getawaysMinors) {
                $scope.passengerError = BookerConfig.errorMessages.NO_PASSENGER_ERROR;
            }
            if ($scope.passenger.getawaysAdults < $scope.passenger.getawaysInfants) {
                $scope.passengerError = BookerConfig.errorMessages.TOO_MANY_LAP_INFANTS;
            }
            if ($scope.passengerCount > $scope.config.CONSTANTS.GUESTLIMIT) {
                $scope.passengerError = BookerConfig.errorMessages.TOO_MANY_PASSENGERS;
            }
        }
    };
    $scope.updatePassengerOptions();
    angular.forEach($scope.passenger, function(value, key) {
        $scope.$watch(function() {
            return $scope.passenger[key];
        }, function() {
            $scope.updatePassengerOptions();
        });
    });
    $(window).bind("BOOKER_PASSENGERSELECT_CHANGE", function() {
        $scope.updatePassengerOptions();
        $scope.$apply();
    });
    $("body").delegate(".passengerSelect select", "change", function() {
        $(window).trigger("BOOKER_PASSENGERSELECT_CHANGE");
    });
    $scope.onSearchTypeChange = function() {
        $scope.itinerarySelect = "RT";
        if ($scope.searchType === "flights") {
            $scope.recentSearches = storageLib.get("recentSearchesFlight");
        } else {
            $scope.recentSearches = storageLib.get("recentSearchesGetAways");
        }
    };
    $scope.populateFromInit = function() {
        var thisairport, cookieval, defaultIATA = BookerConfig.CONSTANTS.REPOPULATION_FALLBACK_AIRPORT_IATA;
        cookieval = $.cookie("base_airport") == "false" ? false : !!$.cookie("base_airport") ? $.cookie("base_airport") : false;
        storageLib = jbStorageLib.init();
        storageLib.setMaxCount("recentSearchesFlight", BookerConfig.CONSTANTS.RECENT_SEARCHES_FLIGHT_MAX_COUNT);
        storageLib.setMaxCount("recentSearchesGetAways", BookerConfig.CONSTANTS.RECENT_SEARCHES_GETAWAY_MAX_COUNT);
        $scope.recentSearches = storageLib.get("recentSearchesFlight") || [];
        $scope.onFromSelection();
        $scope.onToSelection();
        if ($scope.recentSearches.length) {
            $scope.onFromSelection($scope.recentSearches[0].fromLabel, $scope.recentSearches[0].fromCode);
        } else if (!!cookieval) {
            if (AirportData.Station(cookieval)) {
                thisairport = AirportData.Station(cookieval);
                $timeout(function() {
                    $scope.onFromSelection(thisairport.DefaultDisplayName, cookieval);
                });
            }
        } else if (typeof jbGeoLocationImpl != "undefined" && jbGeoLocationImpl) {
            jQuery(jbGeoLocationImpl).bind("GEO_LOCATION_JSON_LOADED", function(e, location) {
                if (typeof location.Code === "string") {
                    thisairport = AirportData.Station(location.Code);
                    $timeout(function() {
                        $scope.onFromSelection(thisairport.DefaultDisplayName, location.Code);
                    });
                } else {
                    thisairport = AirportData.Station(defaultIATA);
                    $timeout(function() {
                        $scope.onFromSelection(thisairport.DefaultDisplayName, defaultIATA);
                    });
                }
            });
            jQuery(jbGeoLocationImpl).trigger("GEO_LOCATION_JSON_REQUESTED");
        }
        $scope.$emit("BOOKER_SCOPE_REINITIALIZE");
    };
    $scope.setDateValues = function(searchObj) {
        var date = new Date();
        date.setHours(0, 0, 0, 0);
        if (new Date(searchObj.departDate).getTime() >= date.getTime()) {
            if ($scope.type == "bff") {
                $scope.departCal.date = new Date(searchObj.departDate);
                if ($scope.departCal.date instanceof Date && new Date(searchObj.returnDate) >= $scope.departCal.date) {
                    $scope.returnCal.date = new Date(searchObj.returnDate);
                }
            } else {
                $timeout(function() {
                    $scope.departCal.date = new Date(searchObj.departDate);
                    if ($scope.departCal.date instanceof Date && new Date(searchObj.returnDate) >= $scope.departCal.date) {
                        $scope.returnCal.date = new Date(searchObj.returnDate);
                    }
                });
            }
        }
    };
    $rootScope.setFormValues = function(searchObj) {
        if (searchObj && searchObj.fromLabel && searchObj.fromCode && searchObj.toLabel && searchObj.toCode && searchObj.departDate) {
            $scope.onFromSelection(searchObj.fromLabel, searchObj.fromCode);
            $scope.onToSelection(searchObj.toLabel, searchObj.toCode);
            $scope.itinerarySelect = searchObj.intineraryType;
            $scope.setDateValues(searchObj);
        } else {
            console.warn("Error in setFormValues");
        }
        if (searchObj.fare) {
            $scope.currency = searchObj.fare;
        }
        var prefix = "flights";
        if ($scope.searchType == "getaways") {
            prefix = "getaways";
        }
        if ($scope.searchType == "getaways" && searchObj.adultCount > 0 || $scope.searchType != "getaways") {
            $scope.passenger[prefix + "Adults"] = searchObj.adultCount;
        }
        $scope.updatePassengerOptions();
        $scope.passenger[prefix + "Minors"] = searchObj.minorCount;
        $scope.passenger[prefix + "Infants"] = searchObj.infantCount;
        $scope.updatePassengerOptions();
        if (searchObj.minorAges != undefined && $scope.searchType == "getaways") {
            $scope.getawayMinorAge = searchObj.minorAges;
        }
        $scope.checkAllFields();
        $(window).trigger("BOOKER_RECENTSEARCH_POPULATE");
    };
    $scope.isrsDisplayed = false;
    $scope.rsDisplayMenu = function(length) {
        $scope.isrsDisplayed = !$scope.isrsDisplayed;
        if ($scope.isrsDisplayed) {
            $(".options .option").removeClass("focus");
            $scope.rsopen = "sbactive";
        } else {
            $scope.rsopen = "";
        }
    };
    var getKeyboardEventResult = function(keyEvent) {
        return window.event ? keyEvent.keyCode : keyEvent.which;
    };
    $scope.onKeyDownR = function($event) {
        var onKeyDownResult = getKeyboardEventResult($event);
        if (!$scope.recentSearches.length) {
            return;
        }
        if (onKeyDownResult === key.down || onKeyDownResult === key.right) {
            if ($scope.isrsDisplayed) {
                $($event.target).next().find(".option").first().focus();
                $($event.target).next().find(".option").removeClass("focus");
                $($event.target).next().find(".option").first().addClass("focus");
            } else {
                $scope.rsDisplayMenu();
            }
        }
        if (onKeyDownResult === key.tab || onKeyDownResult === key.esc) {
            if ($scope.isrsDisplayed) {
                $scope.rsDisplayMenu();
            }
        }
    };
    $scope.onOptionKeyDown = function($event) {
        var onKeyDownResult = getKeyboardEventResult($event);
        if (onKeyDownResult === key.down || onKeyDownResult === key.right) {
            if ($($event.target).next().length === 0) {
                return;
            }
            $($event.target).closest(".options").find(".option").removeClass("focus");
            $($event.target).next().addClass("focus");
            $($event.target).next().focus();
        }
        if (onKeyDownResult === key.up || onKeyDownResult === key.left) {
            if ($scope.isrsDisplayed) {
                if ($($event.target).prev().hasClass("options_header")) {
                    return;
                }
                $($event.target).closest(".options").find(".option").removeClass("focus");
                $($event.target).prev().addClass("focus");
                $($event.target).prev().focus();
            }
        }
    };
    $scope.rsInit = function() {
        $(document).bind("click", function(event) {
            if ($(".recentSearches").find(event.target).length > 0) {
                return;
            }
            $scope.isrsDisplayed = false;
            $scope.rsopen = "";
        });
    };
    function saveSearch(searchType) {
        if ($scope.fromSelectedObj.label && $scope.fromSelectedObj.code && searchType) {
            var searchObj = {
                key: searchType,
                value: {
                    fromLabel: $scope.fromSelectedObj.label,
                    fromCode: $scope.fromSelectedObj.code,
                    toLabel: $scope.toSelectedObj.label,
                    toCode: $scope.toSelectedObj.code,
                    departDate: $scope.departCal.date,
                    returnDate: $scope.returnCal.date,
                    intineraryType: $scope.itinerarySelect,
                    adultCount: $scope.searchType == "getaways" ? $scope.passenger.getawaysAdults : $scope.passenger.flightsAdults,
                    minorCount: $scope.searchType == "getaways" ? $scope.passenger.getawaysMinors : $scope.passenger.flightsMinors,
                    infantCount: $scope.searchType == "getaways" ? $scope.passenger.getawaysInfants : $scope.passenger.flightsInfants
                }
            };
            if ($scope.searchType == "getaways" && $scope.passenger.getawaysMinors > 0) {
                searchObj.value.minorAges = $scope.getawayMinorAge;
            }
            storageLib.set(searchObj);
        } else {
            console.warn("Error in saveSearch");
        }
        var legacyCookie = $.map($scope.config.searchTypes.types, function(el, i) {
            if (el.value == $scope.searchType) {
                return el.legacyCookie;
            }
        });
        if (legacyCookie.length > 0) {
            var departDate = new Date($scope.departCal.date);
            var returnDate = new Date($scope.returnCal.date);
            var currentEntry = {
                drop_down_value: $scope.fromSelectedObj.code + " > " + $scope.toSelectedObj.code + ", " + departDate.toLocaleDateString(),
                search_type: "find_flights",
                flight_type: $scope.itinerarySelect,
                from_field: $scope.fromSelectedObj.label,
                departure_field: departDate.toDateString(),
                adult_count: $scope.searchType == "getaways" ? $scope.passenger.getawaysAdults : $scope.passenger.flightsAdults,
                kid_count: $scope.searchType == "getaways" ? $scope.passenger.getawaysMinors : $scope.passenger.flightsMinors,
                infant_count: $scope.searchType == "getaways" ? $scope.passenger.getawaysInfants : $scope.passenger.flightsInfants,
                to_field: $scope.toSelectedObj.label,
                return_field: returnDate.toDateString(),
                fare_display: "lowest",
                flexible: "false"
            };
            storageLib.setMaxCount(legacyCookie[0], 3);
            storageLib.setCookie({
                value: currentEntry,
                key: legacyCookie
            });
        }
    }
    function isInvalidDate(inputDate) {
        return !inputDate || isNaN(inputDate.getTime()) || inputDate.getTime() < 0;
    }
    function isInvalidMinorAge(age) {
        return !(/^\d{1,2}$/.test(age) && age >= BookerConfig.CONSTANTS.MIN_MINOR_AGE && age <= BookerConfig.CONSTANTS.MAX_MINOR_AGE);
    }
    $rootScope.bookerFormCheck = function($event) {
        var ignoreDate = $event.ignore == "date";
        var erroMsg, warnMsg, seasonalFlightMsg, submitForm, modal, mymodal, i, l;
        erroMsg = seasonalFlightMsg = warnMsg = "";
        submitForm = true;
        modal = jbOverlayFactory;
        if ($scope.itinerarySelect != "MC") {
            if (!$scope.fromResult || !checkAirportValidity($scope.fromResult, $scope.fromPAirports)) {
                erroMsg += BookerConfig.errorMessages.DEPART_CITY_ERROR + "\n";
                $scope.departCityError = BookerConfig.errorMessages.DEPART_CITY_ERROR;
            }
            if (!$scope.toResult || !checkAirportValidity($scope.toResult, $scope.toPAirports)) {
                erroMsg += BookerConfig.errorMessages.ARRIVAL_CITY_ERROR + "\n";
                $scope.returnCityError = BookerConfig.errorMessages.ARRIVAL_CITY_ERROR;
            }
            if (isInvalidDate($scope.departCal.date) && !ignoreDate) {
                erroMsg += BookerConfig.errorMessages.DEPART_DATE_ERROR + "\n";
                $scope.departDateError = BookerConfig.errorMessages.DEPART_DATE_ERROR;
            }
            if ($scope.itinerarySelect == "RT" && !ignoreDate) {
                if (isInvalidDate($scope.returnCal.date)) {
                    erroMsg += BookerConfig.errorMessages.RETURN_DATE_ERROR + "\n";
                    $scope.returnDateError = BookerConfig.errorMessages.RETURN_DATE_ERROR;
                }
            }
            if ($scope.currency == "tb" && AirportData.Route($scope.fromSelectedObj.code, $scope.toSelectedObj.code).i == 1) {
                erroMsg += BookerConfig.errorMessages.INTERLINE_POINTS + "\n";
            }
            if ($scope.searchType == "getaways") {
                if ($scope.passengerIterable.getawaysMinors && $scope.passengerIterable.getawaysMinors.length > 1) {
                    l = $scope.passengerIterable.getawaysMinors.length;
                    for (i = 1; i < l + 1; i++) {
                        if (!$scope.getawayMinorAge[i] || isInvalidMinorAge(Number($scope.getawayMinorAge[i]))) {
                            erroMsg += BookerConfig.errorMessages.INVALID_CHILD_AGE + " " + i + " " + BookerConfig.errorMessages.MINOR_AGE_RESTRICTION + "\n";
                        }
                    }
                }
            }
        } else {
            if (!$scope.MC.fromResult1 || !checkAirportValidity($scope.MC.fromResult1, $scope.fromPAirports)) {
                erroMsg += BookerConfig.errorMessages.DEPART_CITY_ERROR + " 1\n";
            }
            if (!$scope.MC.fromResult2 || !checkAirportValidity($scope.MC.fromResult2, $scope.fromPAirports)) {
                erroMsg += BookerConfig.errorMessages.DEPART_CITY_ERROR + " 2\n";
            }
            if ($scope.MC.toResult3 && !$scope.MC.fromResult3 || $scope.MC.fromResult3 && !checkAirportValidity($scope.MC.fromResult3, $scope.fromPAirports)) {
                erroMsg += BookerConfig.errorMessages.DEPART_CITY_ERROR + " 3\n";
            }
            if ($scope.MC.toResult4 && !$scope.MC.fromResult4 || $scope.MC.fromResult4 && !checkAirportValidity($scope.MC.fromResult4, $scope.fromPAirports)) {
                erroMsg += BookerConfig.errorMessages.DEPART_CITY_ERROR + " 4\n";
            }
            if (!$scope.MC.toResult1 || !checkAirportValidity($scope.MC.toResult1, $scope.toPAirports1)) {
                erroMsg += BookerConfig.errorMessages.ARRIVAL_CITY_ERROR + " 1\n";
            }
            if (!$scope.MC.toResult2 || !checkAirportValidity($scope.MC.toResult2, $scope.toPAirports2)) {
                erroMsg += BookerConfig.errorMessages.ARRIVAL_CITY_ERROR + " 2\n";
            }
            if ($scope.MC.fromResult3 && !$scope.MC.toResult3 || $scope.MC.toResult3 && !checkAirportValidity($scope.MC.toResult3, $scope.toPAirports3)) {
                erroMsg += BookerConfig.errorMessages.ARRIVAL_CITY_ERROR + " 3\n";
            }
            if ($scope.MC.fromResult4 && !$scope.MC.toResult4 || $scope.MC.toResult4 && !checkAirportValidity($scope.MC.toResult4, $scope.toPAirports4)) {
                erroMsg += BookerConfig.errorMessages.ARRIVAL_CITY_ERROR + " 4\n";
            }
            if (!ignoreDate && isInvalidDate($scope.departCal1.date)) {
                erroMsg += BookerConfig.errorMessages.DEPART_DATE_ERROR + " 1\n";
            }
            if (!ignoreDate && isInvalidDate($scope.departCal2.date)) {
                erroMsg += BookerConfig.errorMessages.DEPART_DATE_ERROR + " 2\n";
            }
            if (!ignoreDate && ($scope.MC.fromResult3 && $scope.MC.toResult3) && isInvalidDate($scope.departCal3.date)) {
                erroMsg += BookerConfig.errorMessages.DEPART_DATE_ERROR + " 3\n";
            }
            if (!ignoreDate && ($scope.MC.fromResult4 && $scope.MC.toResult4) && isInvalidDate($scope.departCal4.date)) {
                erroMsg += BookerConfig.errorMessages.DEPART_DATE_ERROR + " 4\n";
            }
        }
        if ($scope.searchType == "flights" && !$scope.passenger.flightsAdults && !$scope.passenger.flightsMinors) {
            erroMsg += BookerConfig.errorMessages.NO_PASSENGER_ERROR + "\n";
        }
        if ($scope.searchType == "flights" && $scope.passenger.flightsAdults < $scope.passenger.flightsInfants) {
            erroMsg += BookerConfig.errorMessages.TOO_MANY_LAP_INFANTS + "\n";
        }
        if ($scope.passengerCount > $scope.config.CONSTANTS.GUESTLIMIT) {
            erroMsg += BookerConfig.errorMessages.TOO_MANY_PASSENGERS + "\n";
        }
        var today = function(date) {
            if (date instanceof Date) {
                var today = new Date();
                return date.getDate() == today.getDate() && date.getMonth() == today.getMonth() && date.getYear() == today.getYear();
            } else {
                return false;
            }
        };
        var longTrip = function(start, end) {
            if (start instanceof Date && end instanceof Date) {
                return (end - start) / 1e3 / 60 / 60 / 24 >= 28;
            } else {
                return false;
            }
        };
        var shortTrip = function(start, end) {
            if (start instanceof Date && end instanceof Date) {
                return (end - start) / 1e3 / 60 / 60 / 24 < 1;
            } else {
                return false;
            }
        };
        if ($scope.type != "bff" && $scope.itinerarySelect != "MC") {
            if (!ignoreDate && longTrip($scope.departCal.date, $scope.returnCal.date)) {
                warnMsg += BookerConfig.warningMessages.LONG_TRIP + "\n";
            }
            if (!ignoreDate && $scope.itinerarySelect != "OW" && shortTrip($scope.departCal.date, $scope.returnCal.date)) {
                warnMsg += BookerConfig.warningMessages.SHORT_TRIP + "\n";
            }
            var departCountry = AirportData.airportLookup[$scope.fromSelectedObj.code];
            var destinationCountry = AirportData.airportLookup[$scope.toSelectedObj.code];
            if ($scope.itinerarySelect == "OW" && departCountry.CountryCode != destinationCountry.CountryCode) {
                var warn = true;
                if (BookerConfig.countryExceptions[departCountry.Code] || BookerConfig.countryExceptions[destinationCountry.Code]) {
                    var depC = BookerConfig.countryExceptions[departCountry.Code] ? BookerConfig.countryExceptions[departCountry.Code] : departCountry.CountryCode;
                    var destC = BookerConfig.countryExceptions[destinationCountry.Code] ? BookerConfig.countryExceptions[destinationCountry.Code] : destinationCountry.CountryCode;
                    warn = depC != destC;
                }
                if (warn) {
                    warnMsg += BookerConfig.warningMessages.OW_INTERNATIONAL + "\n";
                }
            }
            if (!ignoreDate && today($scope.departCal.date)) {
                warnMsg += BookerConfig.warningMessages.TODAYS_DATE + "\n";
            }
        } else if ($scope.itinerarySelect == "MC") {
            if (!ignoreDate && today($scope.departCal1.date)) {
                warnMsg += BookerConfig.warningMessages.TODAYS_DATE + "\n";
            }
        }
        seasonalFlightMsg += checkSeasonalFlights($scope.fromResult, $scope.toResult);
        if (erroMsg) {
            $scope.errorMessages = erroMsg.split("\n");
            $scope.showErrors = true;
            $scope.isSeasonalFlight = false;
            submitForm = false;
            $event.preventDefault();
        } else if (seasonalFlightMsg && $scope.type != "bff") {
            $scope.seasonalFlightMsg = seasonalFlightMsg;
            $scope.isSeasonalFlight = true;
            $scope.showErrors = true;
            submitForm = false;
            $event.preventDefault();
        } else if (warnMsg) {
            $scope.warningMessages = warnMsg.split("\n");
            $scope.showWarnings = true;
            submitForm = false;
            $event.preventDefault();
        } else if ($scope.searchType == "getaways") {
            $scope.submitVacationsHandler();
            submitForm = false;
            $event.preventDefault();
        } else if (!!$scope.flexSearchOptIn) {
            $scope.handleBFFRedirect();
            submitForm = false;
        } else if ($scope.searchType != "getaways" && !$scope.passenger.flightsAdults && $scope.passenger.flightsMinors && !$scope.acceptUMNR) {
            submitForm = false;
            showUMNR(modal, $event);
        }
        return submitForm;
    };
    $scope.acceptUMNR = false;
    var showUMNR = function(modal, event) {
        modal.init({
            title: BookerConfig.errorMessages.UNACCOMPANIED_MINOR_TITLE,
            template: "http://www.jetblue.com/includes/modules/booker_refresh/components/booker/dist/partials/_umnrresolution.html",
            triggerEvent: event
        });
        modal.show();
        $rootScope.$on("resolveUMNR", function(event, data) {
            $scope.acceptUMNR = true;
            if ($scope.type != "bff") {
                $scope.saveAndSubmit(true);
            }
        });
    };
    $scope.continueSubmit = function($event) {
        var modal = jbOverlayFactory;
        var submit = true;
        $scope.showWarnings = false;
        $scope.warningMessages = [];
        if ($scope.searchType != "getaways" && !$scope.passenger.flightsAdults && $scope.passenger.flightsMinors && !$scope.acceptUMNR) {
            submit = false;
            showUMNR(modal, $event);
        } else {
            $scope.saveAndSubmit(submit);
        }
    };
    $scope.handleSubmit = function($event) {
        submitForm = $rootScope.bookerFormCheck($event);
        if (submitForm) {
            $scope.saveAndSubmit(submitForm);
        }
    };
    $scope.submitVacationsHandler = function() {
        $scope.getawaysInfantList = [];
        var infantslookup = $scope.passenger.getawaysInfants;
        while (infantslookup > 0) {
            $scope.getawaysInfantList.push("infant" + infantslookup);
            infantslookup--;
        }
        $scope.getawaysMinorsList = [];
        var minorslookup = $scope.passenger.getawaysMinors;
        while (minorslookup > 0) {
            $scope.getawaysMinorsList.push({
                name: "child_age" + minorslookup,
                value: $scope.getawayMinorAge[minorslookup]
            });
            minorslookup--;
        }
        var getawaysForm = $(".getawaysform");
        if ($scope.currency === "tb") {
            getawaysForm.attr("action", $scope.config.getaways.form.redemptionAction).attr("method", $scope.config.getaways.form.redemptionMethod);
        } else {
            getawaysForm.attr("action", getawaysForm.attr("data-forward-action"));
        }
        saveSearch("recentSearchesGetAways");
        try {
            $scope.$apply();
            $(".getawaysform").submit();
        } catch (e) {
            console.warn("Error in getaway submission");
        }
    };
    $scope.handleBFFRedirect = function() {
        saveSearch("recentSearchesFlight");
        var bffstring = "";
        bffstring += "?";
        bffstring += "origin=" + $scope.fromSelectedObj.code + "&";
        bffstring += "destination=" + $scope.toSelectedObj.code + "&";
        bffstring += "departure=" + $filter("date")($scope.departCal.date, "dd-MM-yyyy");
        if (!!$scope.passenger.flightsAdults) {
            bffstring += "&adult=" + $scope.passenger.flightsAdults;
        }
        if (!!$scope.passenger.flightsMinors) {
            bffstring += "&kid=" + $scope.passenger.flightsMinors;
        }
        if (!!$scope.passenger.flightsInfants) {
            bffstring += "&infant=" + $scope.passenger.flightsInfants;
        }
        if ($scope.currency == "tb") {
            bffstring += "&fare=POINTS";
        }
        window.location = $scope.config.flexibleSearch.baseURL + bffstring;
    };
    function appendZero(num) {
        if (num < 10) {
            return "0" + num;
        } else {
            return num;
        }
    }
    $scope.resolveChildrenAsUMNR = function() {
        $("#jbBooker-groups-UNN").val($scope.passenger.flightsMinors);
        $scope.passenger.flightsMinors = 0;
        setTimeout(function() {
            $scope.saveAndSubmit(true);
        }, 10);
    };
    $scope.resolveChildrenAsAdult = function() {
        $scope.passenger.flightsAdults = $scope.passenger.flightsMinors;
        $scope.passenger.flightsMinors = 0;
        setTimeout(function() {
            $scope.saveAndSubmit(true);
        }, 10);
    };
    $scope.departDateError = "";
    $scope.returnDateError = "";
    $scope.departCityError = "";
    $scope.returnCityError = "";
    $scope.passengerError = "";
    $scope.checkDepartDate = function() {
        setTimeout(function() {
            $scope.departDateError = "";
            if (isInvalidDate($scope.departCal.date)) {
                $scope.departDateError = BookerConfig.errorMessages.DEPART_DATE_ERROR;
            }
            $scope.$apply();
        }, 0);
    };
    $scope.checkReturnDate = function() {
        setTimeout(function() {
            $scope.returnDateError = "";
            if ($scope.itinerarySelect == "RT") {
                if (isInvalidDate($scope.returnCal.date)) {
                    $scope.returnDateError = BookerConfig.errorMessages.RETURN_DATE_ERROR;
                }
                $scope.$apply();
            }
        }, 0);
    };
    $scope.checkDepartCity = function() {
        $scope.departCityError = "";
        if (!$scope.fromResult || !checkAirportValidity($scope.fromResult, $scope.fromPAirports)) {
            $scope.departCityError = BookerConfig.errorMessages.DEPART_CITY_ERROR;
        }
    };
    $scope.checkReturnCity = function() {
        $scope.returnCityError = "";
        if (!$scope.toResult || !checkAirportValidity($scope.toResult, $scope.toPAirports)) {
            $scope.returnCityError = BookerConfig.errorMessages.ARRIVAL_CITY_ERROR;
        }
    };
    $scope.clearInlineMsg = function() {
        if ($scope.itinerarySelect !== "RT") {
            $scope.returnDateError = "";
        }
    };
    $scope.checkAllFields = function() {
        $scope.updatePassengerOptions();
        if ($scope.departCal.date) {
            $scope.checkDepartDate();
        }
        if ($scope.departCal.date) {
            $scope.checkReturnDate();
        }
        $scope.checkDepartCity();
        $scope.checkReturnCity();
        $scope.clearInlineMsg();
    };
    $scope.saveAndSubmit = function(isToSubmit) {
        isToSubmit = isToSubmit || false;
        if (!!isToSubmit && $scope.searchType != "getaways") {
            saveSearch("recentSearchesFlight");
            $scope.showErrors = false;
            try {
                $(".jbBooker form").attr("action", $(".jbBooker form").attr("data-forward-action")).submit();
            } catch (e) {
                console.warn("Error in flight form submission.");
            }
        } else if (!!isToSubmit && $scope.searchType == "getaways") {
            $scope.submitVacationsHandler();
        }
    };
    function checkAirportValidity(inputLabel, regionsArray) {
        var valid = false;
        var regionLength = regionsArray.length;
        for (var i = 0; i < regionLength; i++) {
            var countryLength = regionsArray[i].Countries.length;
            for (var j = 0; j < countryLength; j++) {
                var airportsLength = regionsArray[i].Countries[j].airports.length;
                for (var k = 0; k < airportsLength; k++) {
                    if (inputLabel.toLowerCase() === regionsArray[i].Countries[j].airports[k].label.toLowerCase()) {
                        valid = true;
                        return valid;
                    }
                }
            }
        }
    }
    function checkSeasonalFlights() {
        if (!dayOfWeekRoutes || !seasonalRoutes) {
            return;
        }
        var fromCode, toCode, dailyLookup, seasonalLookup;
        fromCode = $scope.fromSelectedObj.code;
        toCode = $scope.toSelectedObj.code;
        seasonalLookup = [];
        $(seasonalRoutes).map(function(key, route) {
            if (route[0] == fromCode && route[1] == toCode) {
                seasonalLookup.push(route);
            }
        });
        dailyLookup = [];
        $(dayOfWeekRoutes).map(function(key, route) {
            if (route[0] == fromCode && route[1] == toCode) {
                dailyLookup.push(route);
            }
        });
        if (seasonalLookup.length) {
            return BookerConfig.errorMessages.SEASONAL_ITINERARY;
        } else if (dailyLookup.length) {
            return BookerConfig.errorMessages.DAYS_OF_WEEK_ITINERARY;
        } else {
            return "";
        }
    }
    $scope.closeErrors = function() {
        $scope.showErrors = false;
        $scope.showWarnings = false;
        $scope.errorMessages = [];
        $scope.warningMessages = [];
    };
    $scope.$on("OD API FAILURE", function() {
        $scope.fatalAppError = true;
    });
} ]).directive("booker", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "http://www.jetblue.com/includes/modules/booker_refresh/components/booker/dist/partials/booker.html"
    };
}).controller("UnaccompaniedMinorCtrl", [ "$scope", "$rootScope", "BookerConfig", "jbOverlayFactory", "$timeout", function($scope, $rootScope, BookerConfig, jbOverlayFactory, $timeout) {
    var attrs = jbOverlayFactory.getAttributes();
    $scope.msg = BookerConfig.errorMessages.UNACCOMPANIED_MINOR_CONFIRM;
    $scope.accepted = function() {
        $rootScope.$broadcast("resolveUMNR", attrs.triggerEvent);
        closeModal();
    };
    $scope.rejected = function() {
        closeModal();
    };
    var closeModal = function() {
        jbOverlayFactory.hide();
    };
} ]).directive("ageFilter", function() {
    return {
        require: "ngModel",
        link: function(scope, element, attr, ngModelCtrl) {
            scope.maxAge = attr.maxAge;
            function fromUser(text) {
                var transformedInput = parseInt(text);
                var transformedInputText = transformedInput + "";
                if (transformedInputText !== text && transformedInput <= scope.maxAge) {
                    ngModelCtrl.$setViewValue(transformedInputText);
                    ngModelCtrl.$render();
                } else if ((transformedInputText !== text || transformedInput > scope.maxAge) && text !== undefined) {
                    while (transformedInput > scope.maxAge) {
                        transformedInput = Math.floor(transformedInput / 10);
                    }
                    transformedInputText = "" + transformedInput;
                    ngModelCtrl.$setViewValue(transformedInputText);
                    ngModelCtrl.$render();
                }
                return transformedInputText;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

angular.module("BookerWrapper").run([ "$templateCache", function($templateCache) {
    "use strict";
    $templateCache.put("http://www.jetblue.com/includes/modules/booker_refresh/components/booker/dist/partials/_umnrresolution.html", '<div class="umnrResolution" ng-controller="UnaccompaniedMinorCtrl"><div>{{msg}}</div><div class="buttons"><div class="yes dialog_btn" ng-click="accepted()">OK</div><div class="no dialog_btn" ng-click="rejected()">Cancel</div></div></div>');
    $templateCache.put("http://www.jetblue.com/includes/modules/booker_refresh/components/booker/dist/partials/booker.html", '<div class="jbBooker {{type}}" aria-role="/includes/modules/booker_refresh/application"><form data-forward-action="{{config[searchType].form.action}}" method="{{config[searchType].form.method}}" novalidate><input type="hidden" name="lang" value="en" id="jbBookerLang"><input type="hidden" name="pos" value="JETBLUE" id="jbBookerPOS"><input type="hidden" name="flexibleSearch" value="TRUE" id="jbBookerFlexSearch"><input type="hidden" name="searchType" value="NORMAL"><fieldset class="searchSelect" ng-show="type==\'homepage\'||type==\'standalone\'"><legend>{{config.searchTypes.legend}}</legend><ul><li ng-repeat="type in config.searchTypes.types"><input id="jbBookerSearchType{{type.name}}" type="radio" value="{{type.value}}" ng-model="$parent.searchType" name="jbBookerSearchType" ng-change="onSearchTypeChange()"><label for="jbBookerSearchType{{type.name}}" class="piejs" ng-class="{checked:$parent.searchType === type.value}">{{type.label}}</label></li></ul></fieldset><fieldset class="recentSearches" ng-init="rsInit()" ng-hide="type == \'multicity\'"><legend>{{config.legends.recentSearches}}</legend><div class="select {{rsopen}}" ng-keydown="onKeyDownR($event)"><a href="javascript:void(0)" class="label" ng-click="rsDisplayMenu(recentSearches.length || 0)" ng-class="{no_searches: !recentSearches.length}" alt="click to see recent seraches">{{recentSearches.length || 0}} recent searches</a><div class="options piejs" ng-show="isrsDisplayed"><div class="options_header" ng-click="rsDisplayMenu()">{{recentSearches.length || 0}} recent searches</div><a href="javascript:void(0)" ng-keydown="onOptionKeyDown($event)" class="option" ng-repeat="search in recentSearches" ng-click="setFormValues(search); rsDisplayMenu();">{{search.fromCode}} &gt; {{search.toCode}}, {{search.departDate | date:"MM-dd-yyyy"}}</a></div></div></fieldset><fieldset class="itinerarySelect" ng-show="searchType == \'flights\'"><legend>{{config.flights.itineraries.legend}}</legend><ul><li ng-class="( itinerarySelect == type.value )?\'selected_opt\':\'\'" ng-repeat="type in config.flights.itineraries.types" ng-hide="($parent.type != \'multicity\' && type.value == \'MC\') || searchType != \'flights\' "><input type="radio" ng-model="$parent.itinerarySelect" id="jbBookerItin{{type.value}}" value="{{type.value}}" name="journeySpan" aria-required="true" ng-change="$parent.clearInlineMsg()"><label class="fir" for="jbBookerItin{{type.value}}"></label><label for="jbBookerItin{{type.value}}">{{type.label}}</label></li><li ng-show="type != \'multicity\' && type!=\'bff\' && searchType == \'flights\'"><a href="{{config.multicityLink.url}}" class="type_multicity" alt="{{config.multicityLink.alt}}">{{config.multicityLink.text}} <span class="linkArrow"></span></a></li><li ng-show="config[searchType].terms" class="terms"><a class="{{config[searchType].terms.class}}" href="{{config[searchType].terms.url}}">{{config[searchType].terms.text}} <span class="linkArrow"></span></a></li></ul></fieldset><fieldset class="itinerarySelect {{type}}" ng-show="searchType == \'getaways\'"><legend>{{config.legends.currency}}</legend><ul><li ng-repeat="currencyObj in config.getaways.currencies" ng-if="type !== \'vacations\'" ng-class="( $parent.$parent.currency == currencyObj.valueEnum )?\'selected_opt\':\'\'"><input id="jbBookerCurrency-getaways-{{$index}}" name="jbBookerCurrency-getaways" ng-click="print(currency)" ng-model="$parent.$parent.currency" type="radio" ng-value="currencyObj.valueEnum" aria-required="true"><label class="fir" for="jbBookerCurrency-getaways-{{$index}}"></label><label for="jbBookerCurrency-getaways-{{$index}}">{{currencyObj.name}}</label></li><li ng-show="config[searchType].terms" class="terms"><a class="{{config[searchType].terms.class}}" href="{{config[searchType].terms.url}}">{{config[searchType].terms.text}} <span class="linkArrow"></span></a></li></ul></fieldset><div class="clear_fix"></div><fieldset ng-show="errors.length > 0"><legend>You have <span>{{errors.length}}</span> errors below</legend><ul><li ng-repeat="error in errors" ng-class="{error: error.status == \'error\', warn: error.status == \'warn\' }"></li></ul></fieldset><fieldset class="originDestinationSelect" ng-hide="itinerarySelect==\'MC\'"><legend>{{config.legends.originDestinationSelect}}</legend><div class="formcontrol"><div class="inline_error" ng-show="departCityError||returnCityError">{{departCityError}}</div><label class="hidden" for="jbBookerDepart">From</label><div class="clear-fix"></div><cityselector class="auto" ng-model="fromResult" click-activation="true" list-data="fromLAirports" popup-data="fromPAirports" suggestion-type="from" on-type="onFromCityType" on-select="onFromSelection" on-clear="onFromClear" selected-object="fromSelectedObj" attr-inputclass="DepartBox" attr-inputid="jbBookerDepart" aria-role="textbox" aria-required="true" blur-function="checkDepartCity()" footer-text="citySelectorFooterText"></cityselector><input type="hidden" name="origin" value="{{fromSelectedObj.code}}" ng-if="itinerarySelect!=\'MC\'"></div><div class="formcontrol"><div class="inline_error" ng-show="departCityError||returnCityError">{{returnCityError}}</div><label class="hidden" for="jbBookerArrive">To</label><div class="clear-fix"></div><cityselector class="auto" ng-model="toResult" click-activation="true" list-data="toLAirports" on-type="onToCityType" popup-data="toPAirports" suggestion-type="to" on-select="onToSelection" on-clear="onToClear" selected-object="toSelectedObj" attr-inputclass="ArriveBox" attr-inputid="jbBookerArrive" aria-role="textbox" aria-required="true" blur-function="checkReturnCity()" footer-text="citySelectorFooterText"></cityselector><input type="hidden" name="destination" value="{{toSelectedObj.code}}" ng-if="itinerarySelect!=\'MC\'"></div><input type="hidden" ng-value="isInterlineRoute" name="roundTripFaresFlag"><input type="hidden" ng-value="isSharedMarketRoute" name="sharedMarketFlag"></fieldset><fieldset class="dateSelect" ng-hide="itinerarySelect==\'MC\'"><legend>{{config.legends.dateSelect}}</legend><label class="hidden" for="jbBookerCalendarDepart">Leave on</label><div class="calendar_wrapper" ng-class="{cal_active: departCal.isOpen}"><div class="inline_error" ng-show="departDateError||returnDateError">{{departDateError}}</div><div class="clear-fix"></div><div class="clndr clndr1 formcontrol"><span class="input-group-btn cal-btn cal-btn-depart btn-default" ng-click="openDepartCalendar($event)"><i class="glyphicon glyphicon-calendar"></i></span> <input type="text" autocomplete="off" id="jbBookerCalendarDepart" placeholder="Depart date: {{format|uppercase}}" aria-required="true" class="form-control" datepicker-popup="{{format}}" ng-model="http://www.jetblue.com/includes/modules/booker_refresh/app/departCal.date" journey-type="departing" extdate="{{departCal.extDate}}" is-open="departCal.isOpen" type="depart" datepicker-options="departCal.options" min="departCal.minDate" max-date="departCal.maxDate" ng-required="true" close-text="Close" ng-blur="checkDepartDate()"></div></div><label class="hidden" for="jbBookerCalendarReturn" ng-hide="itinerarySelect==\'OW\'">Return on</label><div class="calendar_wrapper" ng-class="{cal_active: returnCal.isOpen}"><div class="inline_error" ng-show="departDateError||returnDateError">{{returnDateError}}</div><div class="clear-fix"></div><div class="clndr clndr2 formcontrol" ng-hide="itinerarySelect==\'OW\'"><span class="input-group-btn cal-btn cal-btn-arrive btn-default" ng-click="openReturnCalendar($event)"><i class="glyphicon glyphicon-calendar"></i></span> <input type="text" autocomplete="off" id="jbBookerCalendarReturn" placeholder="Return date: {{format|uppercase}}" class="form-control" ng-model="http://www.jetblue.com/includes/modules/booker_refresh/app/returnCal.date" journey-type="returning" extdate="{{returnCal.extDate}}" is-open="returnCal.isOpen" datepicker-popup="{{format}}" min="returnCal.minDate" max-date="returnCal.maxDate" datepicker-options="returnCal.options" ng-required="itinerarySelect==\'RT\'" close-text="Close" ng-blur="checkReturnDate()"></div></div><input type="hidden" name="departureDate" value="{{ departCal.date | date:\'yyyy-MM-dd\' }}" ng-if="itinerarySelect!=\'MC\'"> <input type="hidden" name="returnDate" value="{{ returnCal.date | date:\'yyyy-MM-dd\' }}" ng-if="itinerarySelect!==\'OW\'"></fieldset><fieldset class="multicityItin" ng-if="itinerarySelect==\'MC\'"><legend>{{config.legends.multicityItin}}</legend><div class="city_selector_wrapper"><div class="mc_pre_number"><div class="piejs">1</div><span>&nbsp;</span></div><div class="formcontrol"><div class="inline_error" ng-show="mcCityError.fromResult1">{{mcCityError.fromResult1}}</div><div class="clear-fix"></div><cityselector blur-function="checkMCCity(\'fromResult1\', 1)" class="auto" ng-model="MC.fromResult1" click-activation="true" list-data="fromLAirports" popup-data="fromPAirports1" on-select="onMultiFromSelection" on-clear="onMultiFromClear(1)" selected-object="fromSelectedObj1" attr-inputclass="DepartBox" attr-inputid="jbBookerDepart" aria-role="textbox" aria-required="true" id="1" footer-text="citySelectorFooterText"></cityselector></div><div class="formcontrol"><div class="inline_error" ng-show="mcCityError.toResult1">{{mcCityError.toResult1}}</div><div class="clear-fix"></div><cityselector blur-function="checkMCCity(\'toResult1\', 1)" class="auto" ng-model="MC.toResult1" click-activation="true" list-data="toLAirports1" popup-data="toPAirports1" on-select="onMultiToSelection" on-clear="onMultiToClear(1)" selected-object="toSelectedObj1" attr-inputclass="ArriveBox" attr-inputid="jbBookerArrive" aria-role="textbox" aria-required="true" id="1" footer-text="citySelectorFooterText"></cityselector></div><div class="calendar_wrapper" ng-class="{cal_active: departCal1.isOpen}"><div class="inline_error" ng-show="mcDateError.departCal1">{{mcDateError.departCal1}}</div><div class="clear-fix"></div><div class="clndr clndr1 formcontrol"><span class="input-group-btn cal-btn-depart cal-btn btn-default" ng-click="openMultiDepartCalendar($event, 1); mcDateError.departCal1=\'\';"><i class="glyphicon glyphicon-calendar"></i></span> <input type="text" ng-blur="checkMCDate(\'departCal1\')" autocomplete="off" id="jbBookerCalendarDepart" placeholder="Depart date: {{format|uppercase}}" aria-required="true" class="form-control" datepicker-popup="{{format}}" ng-model="http://www.jetblue.com/includes/modules/booker_refresh/app/departCal1.date" journey-type="departing" extdate="{{departCal1.extDate}}" is-open="departCal1.isOpen" datepicker-options="departCal1.options" min="departCal1.minDate" max-date="departCal1.maxDate" ng-required="true" close-text="Close"></div></div></div><div class="city_selector_wrapper"><div class="mc_pre_number"><div class="piejs">2</div><span>&nbsp;</span></div><div class="formcontrol"><div class="inline_error" ng-show="mcCityError.fromResult2">{{mcCityError.fromResult2}}</div><div class="clear-fix"></div><cityselector blur-function="checkMCCity(\'fromResult2\', 2)" class="auto" ng-model="MC.fromResult2" click-activation="true" list-data="fromLAirports2" popup-data="fromPAirports2" on-select="onMultiFromSelection" on-clear="onMultiFromClear(2)" selected-object="fromSelectedObj2" attr-inputclass="DepartBox" attr-inputid="jbBookerDepart" aria-role="textbox" aria-required="true" id="2" footer-text="citySelectorFooterText"></cityselector></div><div class="formcontrol"><div class="inline_error" ng-show="mcCityError.toResult2">{{mcCityError.toResult2}}</div><div class="clear-fix"></div><cityselector blur-function="checkMCCity(\'toResult2\', 2)" class="auto" ng-model="MC.toResult2" click-activation="true" list-data="toLAirports2" popup-data="toPAirports2" on-select="onMultiToSelection" on-clear="onMultiToClear(2)" selected-object="toSelectedObj2" attr-inputid="jbBookerArrive" attr-inputclass="ArriveBox" aria-role="textbox" aria-required="true" id="2" footer-text="citySelectorFooterText"></cityselector></div><div class="calendar_wrapper" ng-class="{cal_active: departCal2.isOpen}"><div class="inline_error" ng-show="mcDateError.departCal2">{{mcDateError.departCal2}}</div><div class="clear-fix"></div><div class="clndr clndr2 formcontrol"><span class="input-group-btn cal-btn-depart cal-btn btn-default" ng-click="openMultiDepartCalendar($event, 2);mcDateError.departCal2=\'\';"><i class="glyphicon glyphicon-calendar"></i></span> <input type="text" ng-blur="checkMCDate(\'departCal2\')" autocomplete="off" id="jbBookerCalendarDepart" placeholder="Depart date: {{format|uppercase}}" aria-required="true" class="form-control" datepicker-popup="{{format}}" ng-model="http://www.jetblue.com/includes/modules/booker_refresh/app/departCal2.date" journey-type="departing" is-open="departCal2.isOpen" datepicker-options="departCal2.options" min="departCal2.minDate" max-date="departCal2.maxDate" ng-required="true" extdate="{{departCal2.extDate}}" close-text="Close"></div></div></div><div class="city_selector_wrapper"><div class="mc_pre_number" ng-class="thirdTripLine"><div class="piejs">3</div><span class="optional_text">{{config.legends.optionalText}}</span> <a href="javascript:void(0)" class="clear_entry" ng-click="clearTripLine(3)">{{config.legends.clearEntry}}</a></div><div class="formcontrol"><cityselector class="auto" ng-model="MC.fromResult3" click-activation="true" list-data="fromLAirports3" popup-data="fromPAirports3" on-select="onMultiFromSelection" on-clear="onMultiFromClear(3)" selected-object="fromSelectedObj3" attr-inputclass="DepartBox" attr-inputid="jbBookerDepart" aria-role="textbox" aria-required="true" id="3" footer-text="citySelectorFooterText"></cityselector></div><div class="formcontrol"><cityselector class="auto" ng-model="MC.toResult3" click-activation="true" list-data="toLAirports3" popup-data="toPAirports3" on-select="onMultiToSelection" on-clear="onMultiToClear(3)" selected-object="toSelectedObj3" attr-inputclass="ArriveBox" attr-inputid="jbBookerArrive" aria-role="textbox" aria-required="true" id="3" footer-text="citySelectorFooterText"></cityselector></div><div class="calendar_wrapper" ng-class="{cal_active: departCal3.isOpen}"><div class="clndr clndr3 formcontrol"><span class="input-group-btn cal-btn-depart cal-btn btn-default" ng-click="openMultiDepartCalendar($event, 3)"><i class="glyphicon glyphicon-calendar"></i></span> <input type="text" autocomplete="off" id="jbBookerCalendarDepart" placeholder="Depart date: {{format|uppercase}}" aria-required="true" class="form-control" datepicker-popup="{{format}}" ng-model="http://www.jetblue.com/includes/modules/booker_refresh/app/departCal3.date" journey-type="departing" is-open="departCal3.isOpen" datepicker-options="departCal3.options" min="departCal3.minDate" max-date="departCal3.maxDate" ng-required="true" extdate="{{departCal3.extDate}}" close-text="Close"></div></div></div><div class="city_selector_wrapper"><div class="mc_pre_number" ng-class="fourthTripLine"><div class="piejs">4</div><span class="optional_text">{{config.legends.optionalText}}</span> <a href="javascript:void(0)" class="clear_entry" ng-click="clearTripLine(4)">{{config.legends.clearEntry}}</a></div><div class="formcontrol"><cityselector class="auto" ng-model="MC.fromResult4" click-activation="true" list-data="fromLAirports4" popup-data="fromPAirports4" on-select="onMultiFromSelection" on-clear="onMultiFromClear(4)" selected-object="fromSelectedObj4" attr-inputclass="DepartBox" attr-inputid="jbBookerDepart" aria-role="textbox" aria-required="true" id="4" footer-text="citySelectorFooterText"></cityselector></div><div class="formcontrol"><cityselector class="auto" ng-model="MC.toResult4" click-activation="true" list-data="toLAirports4" popup-data="toPAirports4" on-select="onMultiToSelection" on-clear="onMultiToClear(4)" selected-object="toSelectedObj4" attr-inputclass="ArriveBox" attr-inputid="jbBookerArrive" aria-role="textbox" aria-required="true" id="4" footer-text="citySelectorFooterText"></cityselector></div><div class="calendar_wrapper" ng-class="{cal_active: departCal4.isOpen}"><div class="clndr clndr4 formcontrol"><span class="input-group-btn cal-btn-depart cal-btn btn-default" ng-click="openMultiDepartCalendar($event, 4)"><i class="glyphicon glyphicon-calendar"></i></span> <input type="text" autocomplete="off" id="jbBookerCalendarDepart" placeholder="Depart date: {{format|uppercase}}" aria-required="true" class="form-control" datepicker-popup="{{format}}" ng-model="http://www.jetblue.com/includes/modules/booker_refresh/app/departCal4.date" journey-type="departing" is-open="departCal4.isOpen" datepicker-options="departCal4.options" min="departCal4.minDate" max-date="departCal4.maxDate" ng-required="true" extdate="{{departCal4.extDate}}" close-text="Close"></div></div></div><input type="hidden" name="originDestinationOptions[0].originPoint" value="{{fromSelectedObj1.code}}"> <input type="hidden" name="originDestinationOptions[1].originPoint" value="{{fromSelectedObj2.code}}"> <input type="hidden" name="originDestinationOptions[2].originPoint" value="{{fromSelectedObj3.code}}"> <input type="hidden" name="originDestinationOptions[3].originPoint" value="{{fromSelectedObj4.code}}"> <input type="hidden" name="originDestinationOptions[0].destinationPoint" value="{{toSelectedObj1.code}}"> <input type="hidden" name="originDestinationOptions[1].destinationPoint" value="{{toSelectedObj2.code}}"> <input type="hidden" name="originDestinationOptions[2].destinationPoint" value="{{toSelectedObj3.code}}"> <input type="hidden" name="originDestinationOptions[3].destinationPoint" value="{{toSelectedObj4.code}}"> <input type="hidden" name="departureDate" value="{{departCal1.date | date: \'yyyy-MM-dd\'}}" ng-if="itinerarySelect==\'MC\'"> <input type="hidden" name="originDestinationOptions[0].departureDate" value="{{departCal1.date | date: \'yyyy-MM-dd\'}}"> <input type="hidden" name="originDestinationOptions[1].departureDate" value="{{departCal2.date | date: \'yyyy-MM-dd\'}}"> <input type="hidden" name="originDestinationOptions[2].departureDate" value="{{departCal3.date | date: \'yyyy-MM-dd\'}}"> <input type="hidden" name="originDestinationOptions[3].departureDate" value="{{departCal4.date | date: \'yyyy-MM-dd\'}}"></fieldset><div class="inline_error passanger_error" ng-show="passengerError">{{passengerError}}</div><div class="clear-fix"></div><fieldset class="passengerSelect" ng-hide="type == \'changeFlights\'" ng-class="{ getaways  : searchType == \'getaways\' }"><legend><span ng-hide="searchType == \'getaways\'">{{config.legends.passengerSelect}}</span> <span ng-show="searchType == \'getaways\'">{{config.legends.passengerSelectGetaways}}</span></legend><div><div ng-repeat="group in config[searchType].ageGroups" style="position:relative;float:left"><label for="jbBookerGroup-{{$index}}" class="hidden">{{group.name}} <span class="description">({{group.description}})</span></label><div jb-select id="jbBookerGroup-{{$index}}" ng-model="$parent.passenger[group.id]" ng-options="group.options" class="passengerGroup" name="{{ [\'numAdults\', \'numChildren\', \'numInfants\'][$index] }}"></div><div style="position:absolute" class="ageEnumeration minor_list_wrapper" ng-if="group.ageEnumerationRequired && $parent.passenger[group.id] > 0 "><div class="minor_list_inner" id="{{\'minor_count_\'+$parent.passengerIterable[group.id].length}}"><label style="float:left" for="booker-{{group.id}}-enum">Ages:</label><div style="float:left" ng-repeat="item in $parent.passengerIterable[group.id] track by $index"><input type="text" age-filter max-age="{{config.CONSTANTS.MAX_MINOR_AGE}}" spellcheck="false" autocorrect="off" autocapitalize="off" autocorrect="off" autocomplete="off" id="booker-{{group.id}}-enum-{{$index+1}}" placeholder="{{group.labels.singular}} {{$index+1}}" ng-model="$parent.getawayMinorAge[$index+1]" pattern="/^[0-9]+$/;" max="group.maximumAge" required></div></div></div></div><div class="hotel_room" ng-show="config[searchType].additionalProducts" ng-repeat="product in additionalProducts"><label for="jbBooker{{SearchType}}additionalProducts{{name}}"></label><div jb-select ng-options="product.options" ng-model="product.model" id="jbBooker{{SearchType}}additionalProducts{{name}}"></div></div><div ng-show="type == \'vacations\' " class="passengerAdjacentLink"><a href="{{config.vacationsServices.url}}" alt="{{config.vacationsServices.alt}}">{{config.vacationsServices.text}}</a></div></div></fieldset><fieldset class="currencySelect {{type}}" ng-class="{noFlex: !(config.flexibleSearch.enabled && type==\'homepage\' && searchType == \'flights\')}" ng-show="searchType !== \'getaways\'"><legend>{{config.legends.currency}}</legend><ul><li ng-class="( $parent.$parent.currency == currencyObj.valueEnum )?\'selected_opt\':\'\'" ng-if="(searchType == \'flights\') && (type !== \'vacations\') &&  itinerarySelect!=\'MC\'" ng-repeat="currencyObj in config.flights.currencies"><input id="jbBookerCurrency-flights-{{$index}}" name="jbBookerCurrency-flights" ng-model="$parent.$parent.currency" type="radio" ng-value="currencyObj.valueEnum" aria-required="true"><label class="fir" for="jbBookerCurrency-flights-{{$index}}"></label><label for="jbBookerCurrency-flights-{{$index}}">{{currencyObj.name}}</label></li><input type="hidden" name="jbBookerCurrency-flights" value="{{mcCurrency}}" ng-if="itinerarySelect==\'MC\'"> <input type="hidden" name="refundable" value="{{currency == \'refundable\'}}"><li ng-repeat="currencyObj in config.getaways.currencies" ng-if="type == \'vacations\'" ng-class="( $parent.$parent.currency == currencyObj.valueEnum )?\'selected_opt\':\'\'"><input id="jbBookerCurrency-getaways-{{$index}}" name="jbBookerCurrency-getaways" ng-model="$parent.$parent.currency" type="radio" ng-value="currencyObj.valueEnum" aria-required="true"><label class="fir" for="jbBookerCurrency-getaways-{{$index}}"></label><label for="jbBookerCurrency-getaways-{{$index}}">{{currencyObj.name}}</label></li><li ng-show="type == \'vacations\'"><a href="{{config[searchType].terms.url}}" alt="{{config[searchType].terms.alt}}">{{config[searchType].terms.text}}</a></li><li ng-show=" type == \'vacations\'" class="nonstopOption" ng-class="{checked:nonstop}"><input type="checkbox" id="nonstopcheck" ng-model="nonstop"><label class="fir" for="nonstopcheck"></label><label for="nonstopcheck"><span class="visuallyhidden">Show only</span>Nonstop flights</label><input type="hidden" name="nonstop" value="nonstop" ng-if="nonstop == true"></li></ul><input type="hidden" name="fareFamily" value="{{ currency == \'tb\' ? \'TRUEBLUE\' : refundable ? \'REFUNDABLE\' : \'LOWESTFARE\' }}"> <input type="hidden" name="fareDisplay" value="{{ currency == \'tb\' ? \'points\' : refundable ? \'refundable\' : \'lowest\' }}"></fieldset><hr class="hr_divider_lg" ng-hide="type != \'multicity\'"><fieldset ng-show="config.flexibleSearch.enabled && type==\'homepage\' && searchType == \'flights\'" class="flexsearch" ng-class="{checked: flexSearchOptIn}"><legend>{{config.flexibleSearch.legend}}</legend><input type="checkbox" id="jbBookerFlexSearchOption" name="jbBookerFlexSearch" ng-model="flexSearchOptIn"><label for="jbBookerFlexSearchOption" class="fir"></label><label for="jbBookerFlexSearchOption">{{config.flexibleSearch.optInText}}</label></fieldset><fieldset class="additionalInfo"><legend>{{config.additionalInfo.legend}}</legend><ul><li ng-repeat="link in config.additionalInfo.links"><a href="{{link.url}}" ng-click="openModal($event, bwconfig.additionalInfo.links[link.class]? (bwconfig.additionalInfo.links[link.class].modalContent) : link.modalContent, link.modalHeaderText)" class="{{link.class}}"><img ng-src="{{link.img.src}}" ng-show="{{link.img.src}}" alt="{link.img.alt}}" class="booker-additionalInfo-{{$index}}">{{link.text}}</a></li></ul></fieldset><fieldset class="promo_code_box" ng-if="promoVisible"><legend>{{config.legends.promoCode}}</legend><label>Promo code</label><input type="text" placeholder="promo code" name="promoCode" ng-model="promo" spellcheck="false" autocorrect="off" autocapitalize="off" autocorrect="off" autocomplete="off"></fieldset><br ng-show="promoVisible" class="clear-fix"><input class="piejs" type="submit" ng-hide="type==\'bff\'" value="{{itinerarySelect==\'MC\'? config.submit.textPlural : config.submit.text}}" ng-click="handleSubmit($event)"><div class="form_errors_overlay" ng-show="showErrors||fatalAppError || showWarnings"><div class="error_top"><div class="error_heading" ng-hide="isSeasonalFlight|| !errorMessages.length">Oops!</div><div class="error_close_button piejs" ng-click="closeErrors()" ng-hide="fatalAppError">&times;</div></div><div class="fallbackerrormessage" ng-hide="isSeasonalFlight||errorMessages.length||showWarnings">It looks like something has gone wrong here. Please try refreshing the page or contact 1-800-JETBLUE if the issue persists.</div><div class="error_wrapper" ng-hide="isSeasonalFlight"><div class="error_message_container"><div class="error_message" ng-repeat="errorMessage in errorMessages" ng-bind="errorMessage"></div></div></div><div class="error_wrapper" ng-hide="isSeasonalFlight||errorMessages.length"><div class="warning_message_container"><div class="error_message warn_message" ng-repeat="warningMessage in warningMessages" ng-bind="warningMessage"></div></div></div><div class="error_wrapper" ng-show="isSeasonalFlight"><div class="error_heading"><h2></h2></div><div class="error_message_container"><div class="error_message_single" ng-bind="seasonalFlightMsg"></div></div></div><div class="cta clear-fix" ng-if="isSeasonalFlight||errorMessages.length||showWarnings"><div class="ok_button piejs" ng-click="closeErrors()" ng-hide="isSeasonalFlight || (!errorMessages.length && showWarnings)">OK</div><div class="continue_button" ng-show="isSeasonalFlight||(showWarnings && !errorMessages.length)"><input class="piejs" type="submit" ng-click="continueSubmit($event)" value="{{isSeasonalFlight ? \'CONTINUE\' : \'OK\'}}"></div></div></div><div class="clear-fix"></div></form><div ng-if="searchType==\'getaways\'"><form class="getawaysform" name="form_arc_packages" data-forward-action="{{config.getaways.form.action}}" method="{{config.getaways.form.method}}"><input type="hidden" name="search" value="true"> <input type="hidden" name="air1" value="true"> <input type="hidden" name="room1" value="true"> <input type="hidden" name="car1" value="false"> <input type="hidden" name="roundtrip" value="{{(itinerarySelect == \'RT\') ? true:false}}"> <input type="hidden" name="area2" value="{{toSelectedObj.code}}"> <input type="hidden" name="time1" value="0"> <input type="hidden" name="adults" value="{{passenger.getawaysAdults}}"> <input type="hidden" name="children" value="{{passenger.getawaysMinors}}"> <input type="hidden" name="dynamic_children" value="{{passenger.getawaysMinors}}"> <input type="hidden" name="area1" value="{{fromSelectedObj.code}}"> <input type="hidden" name="serviceclass" value="coach"> <input type="hidden" name="num_rooms" value="{{additionalProducts[0].model}}"> <input type="hidden" name="infants_in_lap" value="{{passenger.getawaysInfants}}"> <input type="hidden" name="date1" value="{{departCal.date | date:\'MM/dd/yyyy\'}}"> <input type="hidden" name="date2" value="{{returnCal.date | date:\'MM/dd/yyyy\'}}"> <input type="hidden" name="event" value="LOGIN_SEARCH"> <input type="hidden" name="nav" value="{{currency == \'tb\' ? \'Redeem\' : \'default\'}}"> <input type="hidden" name="origin" value="http://www.jetblue.com/getaways/error.asp"><div ng-repeat="obj in getawaysMinorsList"><input type="hidden" name="{{obj.name}}" value="{{obj.value}}"></div><div ng-repeat="obj in getawaysInfantList"><input type="hidden" name="{{obj}}" value="in_lap"></div></form></div></div>');
    $templateCache.put("http://www.jetblue.com/includes/modules/booker_refresh/components/booker/dist/partials/modal.faq.html", '<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></head><body><div id="faq-modal" class="modal"><div class="header"><div class="header-inner"><h3>FAQs</h3><div class="context-switch"><a class="close-btn" href="javascript:void(0);">Close</a></div></div></div><div class="body"><div class="body-inner"><h4>Infants</h4><p>A child between the ages of three days old until their second birthday is considered a lap child and does not need to pay for a seat. Customers traveling with lap children are required to call 1-800-JETBLUE (538-2583), hearing or speech impaired: TTY/TDD 1-800-336-5530, after booking their flight to provide JetBlue with the name and birth date of the lap child. While infants are not subject to a fare, they may be assessed U.S. and foreign taxes and fees when traveling internationally, depending on the destination. Get more information about <a href="http://help.jetblue.com/SRVS/CGI-BIN/webisapi.dll?New,Kb=askBlue,case=obj(2327)">traveling with an infant</a>.</p><h4>Unaccompanied minors</h4><p>An unaccompanied minor is a child between the ages of five and under 14 years traveling alone. There is a $100 fee each way, for each unaccompanied minor traveling on JetBlue. Get information about <a href="http://help.jetblue.com/SRVS/CGI-BIN/webisapi.dll?New,Kb=askBlue,case=obj(675)">drop off and pick up information</a>.</p><h4>JetPaws &#8482; Pet Program</h4><p>As part of the <a href="http://www.jetblue.com/travel/pets/">JetPaws&#8482; Pet Program</a>, JetBlue gladly accepts small cats and dogs in the aircraft cabin on both domestic and international flights (no other animals are allowed). There is a non-refundable pet fee of $100 each way. To book your pet, please call 1-800-JETBLUE (538-2583). Customers with hearing or speech impairments can call our toll-free TTY/TDD telephone number, 1-800-336-5530. See a <a href="http://help.jetblue.com/SRVS/CGI-BIN/webisapi.dll?New,Kb=askBlue,case=obj(2032)">complete list of requirements for traveling with a pet</a>. Get more information about <a href="http://help.jetblue.com/SRVS/CGI-BIN/webisapi.dll?New,Kb=askBlue,case=obj(1095)">traveling with a certified service animal or emotional support animal</a>.</p><h4>Checked baggage requirements</h4><p>Customers will be charged a $40 fee when checking a second bag. Get more information about <a href="http://help.jetblue.com/SRVS/CGI-BIN/webisapi.dll?New,Kb=askBlue,case=obj(634)">checked baggage weight, size and other requirements</a>.</p><h4>8+ travelers?</h4><p>Book up to 7 people at a time online or call 1-800-JETBLUE (538-2583), hearing or speech impaired: TTY/TDD 1-800-336-5530.</p><p style="padding-bottom: 30px">Traveling with 10+ people? Please <a href="http://help.jetblue.com/SRVS/CGI-BIN/webisapi.dll/,/?St=247,E=0000000000050375899,K=6715,Sxi=14,Case=obj(378395)">click here</a>.</p></div></div></div></body></html>');
} ]);
/*
 * angular-ui-bootstrap
 * http://angular-ui.github.io/bootstrap/
 * 
 * Dependent on jbSelect for goToMonth
 * Version: 0.11.0 - 2014-05-01
 * License: MIT
 */
angular.module("jbBootstrapClndr", ["jbClndrTpls","ui.bootstrap.datepicker"]);
angular.module("jbClndrTpls", ["http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/datepicker.html","http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/day.html","http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/month.html","http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/popup.html","http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/year.html"]);

angular.module('ui.bootstrap.dateparser', [])

.service('dateParser', ['$locale', 'orderByFilter', function($locale, orderByFilter) {

  this.parsers = {};

  var formatCodeToRegex = {
    'yyyy': {
      regex: '\\d{4}',
      apply: function(value) { this.year = +value; }
    },
    'yy': {
      regex: '\\d{2}',
      apply: function(value) { this.year = +value + 2000; }
    },
    'y': {
      regex: '\\d{1,4}',
      apply: function(value) { this.year = +value; }
    },
    'MMMM': {
      regex: $locale.DATETIME_FORMATS.MONTH.join('|'),
      apply: function(value) { this.month = $locale.DATETIME_FORMATS.MONTH.indexOf(value); }
    },
    'MMM': {
      regex: $locale.DATETIME_FORMATS.SHORTMONTH.join('|'),
      apply: function(value) { this.month = $locale.DATETIME_FORMATS.SHORTMONTH.indexOf(value); }
    },
    'MM': {
      regex: '0[1-9]|1[0-2]',
      apply: function(value) { this.month = value - 1; }
    },
    'M': {
      regex: '[1-9]|1[0-2]',
      apply: function(value) { this.month = value - 1; }
    },
    'dd': {
      regex: '[0-2][0-9]{1}|3[0-1]{1}',
      apply: function(value) { this.date = +value; }
    },
    'd': {
      regex: '[1-2]?[0-9]{1}|3[0-1]{1}',
      apply: function(value) { this.date = +value; }
    },
    'EEEE': {
      regex: $locale.DATETIME_FORMATS.DAY.join('|')
    },
    'EEE': {
      regex: $locale.DATETIME_FORMATS.SHORTDAY.join('|')
    }
  };

  this.createParser = function(format) {
    var map = [], regex = format.split('');

    angular.forEach(formatCodeToRegex, function(data, code) {
      var index = format.indexOf(code);

      if (index > -1) {
        format = format.split('');

        regex[index] = '(' + data.regex + ')';
        format[index] = '$'; // Custom symbol to define consumed part of format
        for (var i = index + 1, n = index + code.length; i < n; i++) {
          regex[i] = '';
          format[i] = '$';
        }
        format = format.join('');

        map.push({ index: index, apply: data.apply });
      }
    });

    return {
      regex: new RegExp('^' + regex.join('') + '$'),
      map: orderByFilter(map, 'index')
    };
  };

  this.parse = function(input, format) {
    if ( !angular.isString(input) ) {
      return input;
    }

    format = $locale.DATETIME_FORMATS[format] || format;

    if ( !this.parsers[format] ) {
      this.parsers[format] = this.createParser(format);
    }

    var parser = this.parsers[format],
        regex = parser.regex,
        map = parser.map,
        results = input.match(regex);

    if ( results && results.length ) {
      var fields = { year: 1900, month: 0, date: 1, hours: 0 }, dt;

      for( var i = 1, n = results.length; i < n; i++ ) {
        var mapper = map[i-1];
        if ( mapper.apply ) {
          mapper.apply.call(fields, results[i]);
        }
      }

      if ( isValid(fields.year, fields.month, fields.date) ) {
        dt = new Date( fields.year, fields.month, fields.date, fields.hours);
      }

      return dt;
    }
  };

  // Check if date is valid for specific month (and year for February).
  // Month: 0 = Jan, 1 = Feb, etc
  function isValid(year, month, date) {
    if ( month === 1 && date > 28) {
        return date === 29 && ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0);
    }

    if ( month === 3 || month === 5 || month === 8 || month === 10) {
        return date < 31;
    }

    return true;
  }
}]);

angular.module('ui.bootstrap.position', [])

/**
 * A set of utility methods that can be use to retrieve position of DOM elements.
 * It is meant to be used where we need to absolute-position DOM elements in
 * relation to other, existing elements (this is the case for tooltips, popovers,
 * typeahead suggestions etc.).
 */
  .factory('$position', ['$document', '$window', function ($document, $window) {

    function getStyle(el, cssprop) {
      if (el.currentStyle) { //IE
        return el.currentStyle[cssprop];
      } else if ($window.getComputedStyle) {
        return $window.getComputedStyle(el)[cssprop];
      }
      // finally try and get inline style
      return el.style[cssprop];
    }

    /**
     * Checks if a given element is statically positioned
     * @param element - raw DOM element
     */
    function isStaticPositioned(element) {
      return (getStyle(element, 'position') || 'static' ) === 'static';
    }

    /**
     * returns the closest, non-statically positioned parentOffset of a given element
     * @param element
     */
    var parentOffsetEl = function (element) {
      var docDomEl = $document[0];
      var offsetParent = element.offsetParent || docDomEl;
      while (offsetParent && offsetParent !== docDomEl && isStaticPositioned(offsetParent) ) {
        offsetParent = offsetParent.offsetParent;
      }
      return offsetParent || docDomEl;
    };

    return {
      /**
       * Provides read-only equivalent of jQuery's position function:
       * http://api.jquery.com/position/
       */
      position: function (element) {
        var elBCR = this.offset(element);
        var offsetParentBCR = { top: 0, left: 0 };
        var offsetParentEl = parentOffsetEl(element[0]);
        if (offsetParentEl != $document[0]) {
          offsetParentBCR = this.offset(angular.element(offsetParentEl));
          offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop;
          offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft;
        }

        var boundingClientRect = element[0].getBoundingClientRect();
        return {
          width: boundingClientRect.width || element.prop('offsetWidth'),
          height: boundingClientRect.height || element.prop('offsetHeight'),
          top: elBCR.top - offsetParentBCR.top,
          left: elBCR.left - offsetParentBCR.left
        };
      },

      /**
       * Provides read-only equivalent of jQuery's offset function:
       * http://api.jquery.com/offset/
       */
      offset: function (element) {
        var boundingClientRect = element[0].getBoundingClientRect();
        return {
          width: boundingClientRect.width || element.prop('offsetWidth'),
          height: boundingClientRect.height || element.prop('offsetHeight'),
          top: boundingClientRect.top + ($window.pageYOffset || $document[0].documentElement.scrollTop),
          left: boundingClientRect.left + ($window.pageXOffset || $document[0].documentElement.scrollLeft)
        };
      },

      /**
       * Provides coordinates for the targetEl in relation to hostEl
       */
      positionElements: function (hostEl, targetEl, positionStr, appendToBody) {

        var positionStrParts = positionStr.split('-');
        var pos0 = positionStrParts[0], pos1 = positionStrParts[1] || 'center';

        var hostElPos,
          targetElWidth,
          targetElHeight,
          targetElPos;

        hostElPos = appendToBody ? this.offset(hostEl) : this.position(hostEl);

        targetElWidth = targetEl.prop('offsetWidth');
        targetElHeight = targetEl.prop('offsetHeight');

        var shiftWidth = {
          center: function () {
            return hostElPos.left + hostElPos.width / 2 - targetElWidth / 2;
          },
          left: function () {
            return hostElPos.left;
          },
          right: function () {
            return hostElPos.left + hostElPos.width;
          }
        };

        var shiftHeight = {
          center: function () {
            return hostElPos.top + hostElPos.height / 2 - targetElHeight / 2;
          },
          top: function () {
            return hostElPos.top;
          },
          bottom: function () {
            return hostElPos.top + hostElPos.height;
          }
        };

        switch (pos0) {
          case 'right':
            targetElPos = {
              top: shiftHeight[pos1](),
              left: shiftWidth[pos0]()
            };
            break;
          case 'left':
            targetElPos = {
              top: shiftHeight[pos1](),
              left: hostElPos.left - targetElWidth
            };
            break;
          case 'bottom':
            targetElPos = {
              top: shiftHeight[pos0](),
              left: shiftWidth[pos1]()
            };
            break;
          default:
            targetElPos = {
              top: hostElPos.top - targetElHeight,
              left: shiftWidth[pos1]()
            };
            break;
        }

        return targetElPos;
      }
    };
  }]);

angular.module('ui.bootstrap.datepicker', ['ui.bootstrap.dateparser', 'ui.bootstrap.position'])

.constant('datepickerConfig', {
  formatDay: 'd',
  formatMonth: 'MMMM',
  formatYear: 'yyyy',
  formatDayHeader: 'EEE',
  formatDayTitle: 'MMMM yyyy',
  formatMonthTitle: 'yyyy',
  datepickerMode: 'day',
  minMode: 'day',
  maxMode: 'year',
  showWeeks: false,
  startingDay: 0,
  yearRange: 0.83,
  min: null,
  maxDate: null
})

.controller('DatepickerController', ['$scope', '$attrs', '$parse', '$interpolate', '$timeout', '$log', 'dateFilter', 'datepickerConfig', function($scope, $attrs, $parse, $interpolate, $timeout, $log, dateFilter, datepickerConfig) {
  var self = this,
      ngModelCtrl = { $setViewValue: angular.noop }; // nullModelCtrl;


  // Modes chain
  this.modes = ['day', 'month', 'year'];

  if(!self.date || self.date === null){
    self.date = new Date();
  }

  // Configuration attributes
  angular.forEach(['formatDay', 'formatMonth', 'formatYear', 'formatDayHeader', 'formatDayTitle', 'formatMonthTitle',
                   'minMode', 'maxMode', 'showWeeks', 'startingDay', 'yearRange'], function( key, index ) {
    self[key] = angular.isDefined($attrs[key]) ? (index < 8 ? $interpolate($attrs[key])($scope.$parent) : $scope.$parent.$eval($attrs[key])) : datepickerConfig[key];
  });

  // Watchable attributes
  angular.forEach(['min', 'maxDate'], function( key ) {
    if ( $attrs[key] ) {
      $scope.$parent.$watch($parse($attrs[key]), function(value) {
        self[key] = value ? new Date(value) : null;
        self.refreshView();
      });
    } else {
      self[key] = datepickerConfig[key] ? new Date(datepickerConfig[key]) : null;
    }
  });


  $scope.datepickerMode = $scope.datepickerMode || datepickerConfig.datepickerMode;
  $scope.uniqueId = 'datepicker-' + $scope.$id + '-' + Math.floor(Math.random() * 10000);
  self.activeDate = angular.isDefined($attrs.initDate) ? $scope.$parent.$eval($attrs.initDate) : new Date();
  

  // $scope.arrowVisibility = {};

  $scope.back = true;
  $scope.forward = true;
  this.updateArrowKeyVisibility =  function(){
    var minMonth, maxMonth, selectedMonth = self.activeDate.getMonth();
    if(self.min){
      minMonth = self.min.getMonth();
      if(minMonth === selectedMonth){
        $scope.back = false;
      }
      else{
        $scope.back = true;
      }
    }
    if(self.maxDate){
      maxMonth = self.maxDate.getMonth();
      if(selectedMonth === maxMonth){
        $scope.forward = false;
      }
      else{
        $scope.forward = true;
      }
    }
  };

  this.updateArrowKeyVisibility();

  $scope.isActive = function(dateObject) {
    if (self.compare(dateObject.date, self.activeDate) === 0) {
      $scope.activeDateId = dateObject.uid;
      return true;
    }
    return false;
  };
  $scope.isActiveDay = function(dateObject){
    return dateObject.date.getMonth() === self.activeDate.getMonth();
  };
  $scope.isActiveRow = function(rowObject){
    var activeDays = 0;
    angular.forEach(rowObject, function(dateObject){
     
      if( $scope.isActiveDay(dateObject) ){
        activeDays++
      }
    })
    
    return activeDays > 0;
  };

  this.init = function( ngModelCtrl_ ) {
    ngModelCtrl = ngModelCtrl_;

    ngModelCtrl.$render = function() {
      self.render();
    };
  };

  this.render = function() {
    if ( ngModelCtrl.$modelValue ) {
      var date = new Date( ngModelCtrl.$modelValue ),
          isValid = !isNaN(date);
      if ( isValid ) {
        this.activeDate = date;
      } else {
        $log.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.');
      }
      ngModelCtrl.$setValidity('date', isValid);
    }
    this.refreshView();
  };

  this.refreshView = function() {
    if ( this.element ) {
      this._refreshView();

      var date = ngModelCtrl.$modelValue ? new Date(ngModelCtrl.$modelValue) : null;
      ngModelCtrl.$setValidity('date-disabled', !date || (this.element && !this.isDisabled(date)));
    }
  };

  this.createDateObject = function(date, format) {
    var model = ngModelCtrl.$modelValue ? new Date(ngModelCtrl.$modelValue) : null;
    return {
      date: date,
      label: dateFilter(date, format),
      selected: model && this.compare(date, model) === 0,
      disabled: this.isDisabled(date),
      current: this.compare(date, new Date()) === 0
    };
  };

  this.isDisabled = function( date ) {
    return ((this.min && this.compare(date, this.min) < 0) || (this.maxDate && this.compare(date, this.maxDate) > 0) || ($attrs.dateDisabled && $scope.dateDisabled({date: date, mode: $scope.datepickerMode})));
  };

  // Split array into smaller arrays
  this.split = function(arr, size) {
    var arrays = [];
    while (arr.length > 0) {
      arrays.push(arr.splice(0, size));
    }
    return arrays;
  };

  $scope.select = function( date, dtObj ) {
    if(dtObj && dtObj.disabled){
       return;
    }
    if ( $scope.datepickerMode === self.minMode ) {
      var dt = ngModelCtrl.$modelValue ? new Date( ngModelCtrl.$modelValue ) : new Date(0, 0, 0, 0, 0, 0, 0);
      dt.setFullYear( date.getFullYear(), date.getMonth(), date.getDate() );
      ngModelCtrl.$setViewValue( dt );
      ngModelCtrl.$render();
    } else {
      self.activeDate = date;
      $scope.datepickerMode = self.modes[ self.modes.indexOf( $scope.datepickerMode ) - 1 ];
    }
  };

  $scope.move = function( direction ) {
    var year = self.activeDate.getFullYear() + direction * (self.step.years || 0),
        month = self.activeDate.getMonth() + direction * (self.step.months || 0);
    self.activeDate.setFullYear(year, month, 1);
    self.refreshView();
  };

  $scope.toggleMode = function( direction ) {
    direction = direction || 1;

    if (($scope.datepickerMode === self.maxMode && direction === 1) || ($scope.datepickerMode === self.minMode && direction === -1)) {
      return;
    }

    $scope.datepickerMode = self.modes[ self.modes.indexOf( $scope.datepickerMode ) + direction ];
  };

  // Key event mapper
  $scope.keys = { 13:'enter', 32:'space', 33:'pageup', 34:'pagedown', 35:'end', 36:'home', 37:'left', 38:'up', 39:'right', 40:'down' };

  var focusElement = function() {
	  $timeout(function(){
		  self.element[0].focus();
		  },0,false);
  };

  // Listen for focus requests from popup directive
  $scope.$on('datepicker.focus', function(){
	  if(!(self.compare(self.activeDate, self.min) >= 0)){
		  self.activeDate = new Date(self.min);
	  }
	  self.refreshView();
	  focusElement();
	  });
  $scope.$on("GO_TO_MONTH", function($event, value){
	  if(self.activeDate.getMonth() != value){
		  self.activeDate.setMonth(value);
		  if(self.min != undefined && value < self.min.getMonth()){ //check to increment year
			  self.activeDate.setYear(self.min.getFullYear()+1);
		  }else{
			  self.activeDate.setYear(self.min.getFullYear()); //default to the minDate's year
		  }
		  self.activeDate.setDate(1);

	  }

	  self.refreshView();
  })



  $scope.keydown = function( evt ) {
    var key = $scope.keys[evt.which];
    //IE8 key fix
    if(!key){
    	key = $scope.keys[evt.keyCode];
    }
    
    if ( !key || evt.shiftKey || evt.altKey ) {
      return;
    }

    evt.preventDefault();
    evt.stopPropagation();

    if (key === 'enter' || key === 'space') {
      if ( self.isDisabled(self.activeDate)) {
        return; // do nothing
      }
      $scope.select(self.activeDate);
      focusElement();
    } else if (evt.ctrlKey && (key === 'up' || key === 'down')) {
      $scope.toggleMode(key === 'up' ? 1 : -1);
      focusElement();
    } else {
      self.handleKeyDown(key, evt);
      self.refreshView();
    }
  };

}])

.directive( 'datepicker', function () {
  return {
    restrict: 'EA',
    replace: true,
    templateUrl: 'http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/datepicker.html',
    scope: {
      datepickerMode: '=?',
      dateDisabled: '&'
    },
    require: ['datepicker', '?^ngModel'],
    controller: 'DatepickerController',
    link: function(scope, element, attrs, ctrls) {
      var datepickerCtrl = ctrls[0], ngModelCtrl = ctrls[1];

      if ( ngModelCtrl ) {
        datepickerCtrl.init( ngModelCtrl );
      }
    }
  };
})

.directive('daypicker', ['dateFilter', function (dateFilter) {
  return {
    restrict: 'EA',
    replace: true,
    templateUrl: 'http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/day.html',
    require: '^datepicker',
    link: function(scope, element, attrs, ctrl) {
      scope.showWeeks = ctrl.showWeeks;

      ctrl.step = { months: 1 };
      ctrl.element = element;

      var DAYS_IN_MONTH = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      function getDaysInMonth( year, month ) {
        return ((month === 1) && (year % 4 === 0) && ((year % 100 !== 0) || (year % 400 === 0))) ? 29 : DAYS_IN_MONTH[month];
      }

      function getDates(startDate, n) {
        var dates = new Array(n), current = new Date(startDate), i = 0;
        current.setHours(12); // Prevent repeated dates because of timezone bug
        while ( i < n ) {
          dates[i++] = new Date(current);
          current.setDate( current.getDate() + 1 );
        }
        return dates;
      }

      ctrl._refreshView = function() {
        var year = ctrl.activeDate.getFullYear(),
          month = ctrl.activeDate.getMonth(),
          firstDayOfMonth = new Date(year, month, 1),
          difference = ctrl.startingDay - firstDayOfMonth.getDay(),
          numDisplayedFromPreviousMonth = (difference > 0) ? 7 - difference : - difference,
          firstDate = new Date(firstDayOfMonth);

        if ( numDisplayedFromPreviousMonth > 0 ) {
          firstDate.setDate( - numDisplayedFromPreviousMonth + 1 );
        }

        // 35 is the number of days on a six-month calendar
        var noOfRowsInCalendar = 6;
        var days = getDates(firstDate, noOfRowsInCalendar*7);
        for (var i = 0; i < noOfRowsInCalendar*7; i ++) {
          days[i] = angular.extend(ctrl.createDateObject(days[i], ctrl.formatDay), {
            secondary: days[i].getMonth() !== month,
            uid: scope.uniqueId + '-' + i
          });
        }

        scope.labels = new Array(7);
        for (var j = 0; j < 7; j++) {
          scope.labels[j] = {
            single: (days[j].date + "").slice(0,1),
            abbr: dateFilter(days[j].date, ctrl.formatDayHeader),
            full: dateFilter(days[j].date, 'EEEE')
          };
        }

        scope.title = dateFilter(ctrl.activeDate, ctrl.formatDayTitle);
        scope.rows = ctrl.split(days, 7);

        if ( scope.showWeeks ) {
          scope.weekNumbers = [];
          var weekNumber = getISO8601WeekNumber( scope.rows[0][0].date ),
              numWeeks = scope.rows.length;
          while( scope.weekNumbers.push(weekNumber++) < numWeeks ) {}
        }
        ctrl.updateArrowKeyVisibility();
      };

      ctrl.compare = function(date1, date2) {
        //returns whole-day difference between two date objects
        return Math.floor((new Date( date1.getFullYear(), date1.getMonth(), date1.getDate() ) - new Date( date2.getFullYear(), date2.getMonth(), date2.getDate() ) ) / 86400000);
      };

      function getISO8601WeekNumber(date) {
        var checkDate = new Date(date);
        checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7)); // Thursday
        var time = checkDate.getTime();
        checkDate.setMonth(0); // Compare with Jan 1
        checkDate.setDate(1);
        return Math.floor(Math.round((time - checkDate) / 86400000) / 7) + 1;
      }

      ctrl.handleKeyDown = function( key, evt ) {
        var date = ctrl.activeDate.getDate(), dateChange = false; 
        // activeDateObj = new Date();
        if (key === 'left' && (ctrl.compare(ctrl.activeDate, ctrl.min) > 0 )) {
          date = date - 1;   // up
          dateChange = true;
        } else if (key === 'up' && (ctrl.compare(ctrl.activeDate, ctrl.min) > 6 )) {
          date = date - 7;   // down
          dateChange = true;
        } else if (key === 'right' && (ctrl.compare(ctrl.maxDate, ctrl.activeDate) > 0)) {
          date = date + 1;   // down
          dateChange = true;
        } else if (key === 'down' && (ctrl.compare(ctrl.maxDate, ctrl.activeDate) > 6)) {
          date = date + 7;
          dateChange = true;
        } 
        // else if (key === 'pageup' || key === 'pagedown') {
        //   var month = ctrl.activeDate.getMonth() + (key === 'pageup' ? - 1 : 1);
        //   ctrl.activeDate.setMonth(month, 1);
        //   date = Math.min(getDaysInMonth(ctrl.activeDate.getFullYear(), ctrl.activeDate.getMonth()), date);
        // } else if (key === 'home') {
        //   date = 1;
        // } else if (key === 'end') {
        //   date = getDaysInMonth(ctrl.activeDate.getFullYear(), ctrl.activeDate.getMonth());
        // }
        if(dateChange) {
          ctrl.activeDate.setDate(date);
        }

      };

      //This function is added to satisfy jb need
      function checkDateDisbaled(date){
        return (!!date.disabled);
      }
      ctrl.refreshView();
    }
  };
}])

.constant('datepickerPopupConfig', {
  datepickerPopup: 'yyyy-MM-dd',
  currentText: 'Today',
  clearText: 'Clear',
  closeText: 'Done',
  closeOnDateSelection: true,
  appendToBody: false,
  showButtonBar: false
})

.directive('datepickerPopup', ['$compile', '$parse', '$document', '$position', 'dateFilter', 'dateParser', 'datepickerPopupConfig',
function ($compile, $parse, $document, $position, dateFilter, dateParser, datepickerPopupConfig) {
  return {
    restrict: 'EA',
    require: 'ngModel',
    scope: {
      isOpen: '=?',
      currentText: '@',
      clearText: '@',
      closeText: '@',
      dateDisabled: '&',
      journeyType:'@',
      extdate: "@"
    },
    link: function(scope, element, attrs, ngModel) {
      var dateFormat,
          closeOnDateSelection = angular.isDefined(attrs.closeOnDateSelection) ? scope.$parent.$eval(attrs.closeOnDateSelection) : datepickerPopupConfig.closeOnDateSelection,
          appendToBody = angular.isDefined(attrs.datepickerAppendToBody) ? scope.$parent.$eval(attrs.datepickerAppendToBody) : datepickerPopupConfig.appendToBody;

      scope.showButtonBar = angular.isDefined(attrs.showButtonBar) ? scope.$parent.$eval(attrs.showButtonBar) : datepickerPopupConfig.showButtonBar;

      

      scope.getText = function( key ) {
        return scope[key + 'Text'] || datepickerPopupConfig[key + 'Text'];
      };

      attrs.$observe('datepickerPopup', function(value) {
          dateFormat = value || datepickerPopupConfig.datepickerPopup;
          ngModel.$render();
      });

      // popup element used to display calendar
      var popupEl = angular.element('<div datepicker-popup-wrap><div datepicker></div></div>');
      popupEl.attr({
        'ng-model': 'date',
        'ng-change': 'dateSelection()'
      });

      function cameltoDash( string ){
        return string.replace(/([A-Z])/g, function($1) { return '-' + $1.toLowerCase(); });
      }

      // datepicker element
      var datepickerEl = angular.element(popupEl.children()[0]);
      if ( attrs.datepickerOptions ) {
        angular.forEach(scope.$parent.$eval(attrs.datepickerOptions), function( value, option ) {
          datepickerEl.attr( cameltoDash(option), value );
        });
      }
      scope.calendarOptions= [];
      scope.goToMonth = (new Date()).getMonth();
      
      var months = ["January", "February", "March", "April", "May","June","July", "August", "September", "October", "November","December"];
      
      var updateMonthOptions = function(){
    	  if(scope.min && scope.maxDate){
	    	  var start = scope.min.getMonth();
	    	  var year = scope.min.getFullYear();
	    	  var end = scope.maxDate.getMonth();
	    	  if(end < start){ //period goes over january
	    		  end = 12 + end;
	    	  }
	    	  var changeGoToMonth = true;
	    	  scope.calendarOptions = []
	    	  for(var i = start; i <= end; i++){
	    		  var adjustedYear = year;
	    		  if(i > 11){
	    			  adjustedYear++;
	    		  }
	    		  if(i%12 == scope.goToMonth){ //If the current month open is valid, do not update
	    			  changeGoToMonth = false;
	    		  }
	    		  scope.calendarOptions.push({name: months[i%12]+" "+adjustedYear, value: i%12});
	    	  }
	    	  if(changeGoToMonth){
	    		  scope.goToMonth = start;
	    	  }
    	  }
      }
    
      scope.$watch('goToMonth', function(value){
    	  scope.$broadcast('GO_TO_MONTH', value);
      })
      angular.forEach(['min', 'maxDate'], function( key ) {
        if ( attrs[key] ) {
          scope.$parent.$watch($parse(attrs[key]), function(value){
            scope[key] = value;
            updateMonthOptions();
          });
          datepickerEl.attr(cameltoDash(key), key);
        }
      });
      if (attrs.dateDisabled) {
        datepickerEl.attr('date-disabled', 'dateDisabled({ date: date, mode: mode })');
      }

      // Validates that the input string is a valid date formatted as "mm-dd-yyyy"
      function isValidDate(dateString)
      {
          // First check for the pattern
          if(!/^\d{2}-\d{2}-\d{4}$/.test(dateString))
              return false;

          // Parse the date parts to integers
          var parts = dateString.split("-");
          var day = parseInt(parts[1], 10);
          var month = parseInt(parts[0], 10);
          var year = parseInt(parts[2], 10);

          // Check the ranges of month and year
          if(year < 1000 || year > 3000 || month == 0 || month > 12)
              return false;

          var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

          // Adjust for leap years
          if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
              monthLength[1] = 29;

          // Check the range of the day
          return day > 0 && day <= monthLength[month - 1];
      };

      function parseDate(viewValue) {
        if (!viewValue) {
          ngModel.$setValidity('date', true);
          return null;
        } else if (angular.isDate(viewValue) && !isNaN(viewValue)) {
          ngModel.$setValidity('date', true);
          return viewValue;
        } else if (angular.isString(viewValue)) {
          if(!isValidDate(viewValue)){
            ngModel.$setValidity('date', false);
            return undefined;
          }
          var date = dateParser.parse(viewValue, dateFormat) || new Date(viewValue);
          if (isNaN(date)) {
            ngModel.$setValidity('date', false);
            return undefined;
          } else {
            ngModel.$setValidity('date', true);
            return date;
          }
        } else {
          ngModel.$setValidity('date', false);
          return undefined;
        }
      }
      ngModel.$parsers.unshift(parseDate);

      // Inner change
      scope.dateSelection = function(dt) {
        if (angular.isDefined(dt)) {
          scope.date = dt;
        }
        ngModel.$setViewValue(scope.date);
        ngModel.$render();

        if ( closeOnDateSelection ) {
          scope.isOpen = false;
          element[0].focus();
        }
      };

      element.bind('input change keyup', function() {
        scope.$apply(function() {
          scope.date = ngModel.$modelValue;
        });
      });

      element.bind("click", function($event){
        scope.isOpen = true;
      })

      // Outter change
      ngModel.$render = function() {
        var date = ngModel.$viewValue ? dateFilter(ngModel.$viewValue, dateFormat) : '';
        element.val(date);
        scope.date = parseDate( ngModel.$modelValue );
      };

      var documentClickBind = function(event) {
        if (scope.isOpen && event.target !== element[0]) {
          scope.$apply(function() {
            scope.isOpen = false;
          });
        }
      };

      var keydown = function(evt, noApply) {
        scope.keydown(evt);
      };
      element.bind('keydown', keydown);

      scope.keydown = function(evt) {
        if (evt.which === 27) {
          evt.preventDefault();
          evt.stopPropagation();
          scope.close();
        } else if (evt.which === 40 && !scope.isOpen) {
          scope.isOpen = true;
        }else if((evt.which === 8 || evt.which === 46 ) && scope.isOpen){
          scope.isOpen = false;
        }
        else if( evt.which === 9 ){
          scope.isOpen = false;
        }
      };

      scope.$watch('isOpen', function(value) {
        if (value) {
          scope.$broadcast('datepicker.focus');
          scope.position = appendToBody ? $position.offset(element) : $position.position(element);
          scope.position.top = scope.position.top + element.prop('offsetHeight');

          $document.bind('click', documentClickBind);
        } else {
          $document.unbind('click', documentClickBind);
        }
      });

      scope.select = function( date ) {
        if (date === 'today') {
          var today = new Date();
          if (angular.isDate(ngModel.$modelValue)) {
            date = new Date(ngModel.$modelValue);
            date.setFullYear(today.getFullYear(), today.getMonth(), today.getDate());
          } else {
            date = new Date(today.setHours(0, 0, 0, 0));
          }
        }
        scope.dateSelection( date );
      };

      scope.close = function() {
        scope.isOpen = false;
        element[0].focus();
      };

      var $popup = $compile(popupEl)(scope);
      if ( appendToBody ) {
        $document.find('body').append($popup);
      } else {
        element.after($popup);
      }

      scope.$on('$destroy', function() {
        $popup.remove();
        element.unbind('keydown', keydown);
        $document.unbind('click', documentClickBind);
      });
    }
  };
}])

.directive('datepickerPopupWrap', function() {
  return {
    restrict:'EA',
    replace: true,
    transclude: true,
    templateUrl: 'http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/popup.html',
    link:function (scope, element, attrs) {
      element.bind('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
      });
    }
  };
})
.service('placeholderSniffer', [
    '$document',
    function ($document) {
      this.emptyClassName = 'empty', this.hasPlaceholder = function () {
        var test = $document[0].createElement('input');
        return test.placeholder !== void 0;
      };
    }
  ]).directive('placeholder', [
    '$timeout',
    '$document',
    '$interpolate',
    '$animate',
    'placeholderSniffer',
    '$filter',
    function ($timeout, $document, $interpolate, $animate, placeholderSniffer, $filter) {
      if (placeholderSniffer.hasPlaceholder())
        return {};
      var documentListenersApplied = false, supportsAnimatePromises = parseFloat(angular.version.full) >= 1.3;
      return {
        restrict: 'A',
        require: '?ngModel',
        priority: 110,
        link: function (scope, elem, attrs, ngModel) {
                if(!attrs.datepickerOptions){
        return;
      }
          if(typeof attrs.datepickerPopup !== "undefined"){
            ngModel.$formatters.push(function(val){
              return $filter('date')(val, attrs.datepickerPopup)
            });
            ngModel.$parsers.push(function(val){
              return new Date(val);
            });
          }
          var orig_val = getValue(), domElem = elem[0], elemType = domElem.nodeName.toLowerCase(), isInput = elemType === 'input' || elemType === 'textarea', is_pwd = attrs.type === 'password', text = attrs.placeholder, emptyClassName = placeholderSniffer.emptyClassName, hiddenClassName = 'ng-hide', clone;
          if (!isInput) {
            return;
          }
          attrs.$observe('placeholder', function (newValue) {
            changePlaceholder(newValue);
          });
          if (is_pwd) {
            setupPasswordPlaceholder();
          }
          setValue(orig_val);
          elem.bind('focus', function () {
            if (elem.hasClass(emptyClassName)) {
              elem.val('');
              elem.removeClass(emptyClassName);
              domElem.select();
            }
          });
          elem.bind('blur', updateValue);
          if (!ngModel) {
            elem.bind('change', function () {
              changePlaceholder($interpolate(elem.attr('placeholder'))(scope));
            });
          }
          if (ngModel) {
            ngModel.$render = function () {
              setValue(ngModel.$viewValue);
              if (domElem === document.activeElement && !elem.val()) {
                domElem.select();
              }
            };
          }
          if (!documentListenersApplied) {
            $document.on('selectstart', function (e) {
              var elmn = angular.element(e.target);
              if (elmn.hasClass(emptyClassName) && elmn.prop('disabled')) {
                e.preventDefault();
              }
            });
            documentListenersApplied = true;
          }
          function updateValue(e) {
            var val = elem.val();
            if (elem.hasClass(emptyClassName) && val && val === text) {
              return;
            }
            conditionalDefer(function () {
              setValue(val);
            });
          }
          function conditionalDefer(callback) {
            //// this is the stock behavior, but makes for weird view inconsidencies with old ie, which defeats the purpose of the polyfill
            // if (document.documentMode <= 11) {
            //   $timeout(callback, 0);
            // } else {
            //   callback();
            // }
            //// heres the better implementation:
            callback();
            //// less code is often better code
          }
          function setValue(val) {
            if (!val && val !== 0 && domElem !== document.activeElement) {
              elem.addClass(emptyClassName);
              elem.val(!is_pwd ? text : '');
            } else {
              elem.removeClass(emptyClassName);
              elem.val($filter('date')(val, attrs.datepickerPopup));
            }
            if (is_pwd) {
              updatePasswordPlaceholder();
              asyncUpdatePasswordPlaceholder();
            }
          }
          function getValue() {
            if (ngModel) {
              return scope.$eval(attrs.ngModel) || '';
            }
            return getDomValue() || '';
          }
          function getDomValue() {
            var val = elem.val();
            if (val === attrs.placeholder) {
              val = '';
            }
            return val;
          }
          function changePlaceholder(value) {
            if (elem.hasClass(emptyClassName) && elem.val() === text) {
              elem.val('');
            }
            text = value;
            updateValue();
          }
          function setAttrUnselectable(elmn, enable) {
            if (enable) {
              elmn.attr('unselectable', 'on');
            } else {
              elmn.removeAttr('unselectable');
            }
          }
          function setupPasswordPlaceholder() {
            clone = angular.element('<input type="text" value="' + text + '"/>');
            stylePasswordPlaceholder();
            clone.addClass(emptyClassName).addClass(hiddenClassName).bind('focus', hidePasswordPlaceholderAndFocus);
            domElem.parentNode.insertBefore(clone[0], domElem);
            var watchAttrs = [
                attrs.ngDisabled,
                attrs.ngReadonly,
                attrs.ngRequired,
                attrs.ngShow,
                attrs.ngHide
              ];
            for (var i = 0; i < watchAttrs.length; i++) {
              if (watchAttrs[i]) {
                scope.$watch(watchAttrs[i], asyncUpdatePasswordPlaceholder);
              }
            }
          }
          function updatePasswordPlaceholder() {
            stylePasswordPlaceholder();
            if (isNgHidden()) {
              clone.addClass(hiddenClassName);
            } else if (elem.hasClass(emptyClassName) && domElem !== document.activeElement) {
              showPasswordPlaceholder();
            } else {
              hidePasswordPlaceholder();
            }
          }
          function asyncUpdatePasswordPlaceholder() {
            if (supportsAnimatePromises) {
              $animate.addClass(elem, '').then(updatePasswordPlaceholder);
            } else {
              $animate.addClass(elem, '', updatePasswordPlaceholder);
            }
          }
          function stylePasswordPlaceholder() {
            clone.val(text).attr('class', elem.attr('class') || '').attr('style', elem.attr('style') || '').prop('disabled', elem.prop('disabled')).prop('readOnly', elem.prop('readOnly')).prop('required', elem.prop('required'));
            setAttrUnselectable(clone, elem.attr('unselectable') === 'on');
          }
          function showPasswordPlaceholder() {
            elem.addClass(hiddenClassName);
            clone.removeClass(hiddenClassName);
          }
          function hidePasswordPlaceholder() {
            clone.addClass(hiddenClassName);
            elem.removeClass(hiddenClassName);
          }
          function hidePasswordPlaceholderAndFocus() {
            hidePasswordPlaceholder();
            domElem.focus();
          }
          function isNgHidden() {
            var hasNgShow = typeof attrs.ngShow !== 'undefined', hasNgHide = typeof attrs.ngHide !== 'undefined';
            if (hasNgShow || hasNgHide) {
              return hasNgShow && !scope.$eval(attrs.ngShow) || hasNgHide && scope.$eval(attrs.ngHide);
            } else {
              return false;
            }
          }
        }
      };
    }
  ]);
angular.module("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/datepicker.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/datepicker.html",
	    "<div ng-switch=\"datepickerMode\" role=\"/includes/modules/booker_refresh/application\" ng-keydown=\"keydown($event)\">\n" +
	    "  <daypicker ng-switch-when=\"day\" tabindex=\"0\"></daypicker>\n" +
	    "  <monthpicker ng-switch-when=\"month\" tabindex=\"0\"></monthpicker>\n" +
	    "  <yearpicker ng-switch-when=\"year\" tabindex=\"0\"></yearpicker>\n" +
	    "</div>");
	}]);

	angular.module("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/day.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/day.html",
	    "<table class='calendar' role=\"grid\" aria-labelledby=\"{{uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\">\n" +
	    "  <thead>\n" +
	    "    <tr>\n" +
	    "      <th><div type=\"button\" class=\"cal_left_arrow btn btn-default btn-sm pull-left\" ng-show=\"back\" ng-click=\"move(-1)\" tabindex=\"-1\"></div></th>\n" +
	    "      <th colspan=\"{{5 + showWeeks}}\"><div id=\"{{uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"cal_month_name btn btn-default btn-sm\" tabindex=\"-1\" style=\"width:100%;\"><strong>{{title}}</strong></div></th>\n" +
	    "      <th><div ng-show=\"forward\" class=\" cal_right_arrow btn btn-default btn-sm pull-right\" ng-click=\"move(1)\" tabindex=\"-1\"></div></th>\n" +
	    "    </tr>\n" +
	    "    <tr>\n" +
	    "      <th ng-show=\"showWeeks\" class=\"text-center\"></th>\n" +
	    "      <th ng-repeat=\"label in labels track by $index\" class=\"text-center\"><small aria-label=\"{{label.full}}\">{{label.single}}</small></th>\n" +
	    "    </tr>\n" +
	    "  </thead>\n" +
	    "  <tbody>\n" +
	    "    <tr ng-repeat=\"row in rows track by $index\" ng-if=\"isActiveRow(row)\">\n" +
	    "      <td ng-show=\"showWeeks\" class=\"text-center h6\"><em>{{ weekNumbers[$index] }}</em></td>\n" +
	    "      <td ng-repeat=\"http://www.jetblue.com/includes/modules/booker_refresh/app/dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{dt.uid}}\" aria-disabled=\"{{!!dt.disabled}}\">\n" +
	    "    <div type=\"button\" style=\"width:100%;\" class=\"cal_date_value btn btn-default btn-sm\" ng-class=\"{'btn-info': dt.selected, active: isActive(dt)} \" ng-if=\"isActiveDay(dt)\" ng-click=\"select(dt.date, dt)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\"><span ng-class=\"{'text-muted': dt.secondary, 'text-info': dt.current, 'disabled': dt.disabled}\">{{dt.label}}</span></div>\n" +
	    "      </td>\n" +
	    "    </tr>\n" +
	    "  </tbody>\n" +
	    "</table>\n" +
	    "");
	}]);

	angular.module("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/month.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/month.html",
	    "<table role=\"grid\" aria-labelledby=\"{{uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\">\n" +
	    "  <thead>\n" +
	    "    <tr>\n" +
	    "      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-left\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-left\"></i></button></th>\n" +
	    "      <th><button id=\"{{uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"btn btn-default btn-sm\" ng-click=\"toggleMode()\" tabindex=\"-1\" style=\"width:100%;\"><strong>{{title}}</strong></button></th>\n" +
	    "      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-right\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-right\"></i></button></th>\n" +
	    "    </tr>\n" +
	    "  </thead>\n" +
	    "  <tbody>\n" +
	    "    <tr ng-repeat=\"row in rows track by $index\">\n" +
	    "      <td ng-repeat=\"http://www.jetblue.com/includes/modules/booker_refresh/app/dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{dt.uid}}\" aria-disabled=\"{{!!dt.disabled}}\">\n" +
	    "        <button type=\"button\" style=\"width:100%;\" class=\"btn btn-default\" ng-class=\"{'btn-info': dt.selected, active: isActive(dt)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\"><span ng-class=\"{'text-info': dt.current}\">{{dt.label}}</span></button>\n" +
	    "      </td>\n" +
	    "    </tr>\n" +
	    "  </tbody>\n" +
	    "</table>\n" +
	    "");
	}]);

	angular.module("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/popup.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/popup.html",
	    "<div  class=\"dropdown-menu\" ng-style=\"{display: (isOpen && 'block') || 'none', top: (position.top-4)+'px'}\" ng-keydown=\"keydown($event)\"><ul>\n" +
	    " <li class='outer_header_wrapper'><div class='outer_header'>Choose your {{journeyType}} flight</div></li>\n" +
	    " <li ng-transclude></li>\n" +
	    " <li ng-if=\"showButtonBar\">\n" +
	    "   <span class=\"btn-group\">\n" +
	    "     <button type=\"button\" class=\"btn btn-sm btn-info\" ng-click=\"select('today')\">{{ getText('current') }}</button>\n" +
	    "     <button type=\"button\" class=\"btn btn-sm btn-danger\" ng-click=\"select(null)\">{{ getText('clear') }}</button>\n" +
	    "   </span>\n" +
	    "   <button type=\"button\" class=\"btn btn-sm btn-success pull-right\" ng-click=\"close()\">{{ getText('close') }}</button>\n" +
	    " </li>\n" +
	    "</ul>\n" +
	    "<div class='onsale'> <p>Flights available for sale through {{extdate|date}}</p></div>\n"+
	    "<div class='jumpTo clear-fix'>Jump to:<div jb-select ng-options='calendarOptions' ng-model='goToMonth'></div>\n"+
	    "</div><div class='ieFixMask' style='height:300px;width:100%;display:block'></div></div></div>");
	}]);

	angular.module("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/year.html", []).run(["$templateCache", function($templateCache) {
	  $templateCache.put("http://www.jetblue.com/includes/modules/booker_refresh/app/template/datepicker/year.html",
	    "<table role=\"grid\" aria-labelledby=\"{{uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\">\n" +
	    "  <thead>\n" +
	    "    <tr>\n" +
	    "      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-left\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-left\"></i></button></th>\n" +
	    "      <th colspan=\"3\"><button id=\"{{uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"btn btn-default btn-sm\" ng-click=\"toggleMode()\" tabindex=\"-1\" style=\"width:100%;\"><strong>{{title}}</strong></button></th>\n" +
	    "      <th><button type=\"button\" class=\"btn btn-default btn-sm pull-right\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"glyphicon glyphicon-chevron-right\"></i></button></th>\n" +
	    "    </tr>\n" +
	    "  </thead>\n" +
	    "  <tbody>\n" +
	    "    <tr ng-repeat=\"row in rows track by $index\">\n" +
	    "      <td ng-repeat=\"http://www.jetblue.com/includes/modules/booker_refresh/app/dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{dt.uid}}\" aria-disabled=\"{{!!dt.disabled}}\">\n" +
	    "        <button type=\"button\" style=\"width:100%;\" class=\"btn btn-default\" ng-class=\"{'btn-info': dt.selected, active: isActive(dt)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\"><span ng-class=\"{'text-info': dt.current}\">{{dt.label}}</span></button>\n" +
	    "      </td>\n" +
	    "    </tr>\n" +
	    "  </tbody>\n" +
	    "</table>\n" +
	    "");
	}]);

var jbOverlay = angular.module('jbOverlayModule', [])

//JB Simple Overlay: takes title, template, height, width
.factory('jbOverlayFactory', [ '$rootScope', '$timeout', function($rootScope, $timeout) {
    var self = {};
    //var overlayEl = $(".jb-overlay-holder");
    //var contEl = $(".jb-overlay-holder").find(".jb-overlay-cont");
    //var childOverlayEl = overlayEl.find(".jb-overlay-child-holder"); //child overlay element
    //var childContEl = childOverlayEl.find(".jb-overlay-child-cont"); //child overlay content element

    self.init = function(args, cb){

        if (!args || (!args.template && !args.content)) {
            console.warn("Enter either a 'template' or 'content'");     
        }

        cb = cb || function(){};

        //Overwrite defaults and even pass in more when initializing overlay
        self.attributes = $.extend(true, {
            template: "",
            innerHTML: "Pass in some content",
            title: "",
            height: "400px",
            width: "600px"
        }, args);

        $(".jb-overlay-cont").css({
            height: self.attributes.height,
            width: self.attributes.width
        });
        
        //Callback
        self.cb = cb;

        //Tell the controller that attributes changed
        $rootScope.$broadcast('JBOVERLAY_INIT');
        
    };
    
    
    self.initChild = function(args){
        if (!args || !args.content) {
            console.warn("Enter either a 'template' or 'content'");     
        }
        //Overwrite defaults and even pass in more when initializing overlay child
        self.childAttributes = $.extend(true, {
        	childContent: "No information available.",
        	childTitle: ""
        }, args);
        
        //Tell the controller that attributes changed for child
        $rootScope.$broadcast('JBOVERLAY_CHILD_INIT');

    };
    
    
    self.hide = function(){
    	$(".jb-overlay-holder").hide();
        $('html').removeClass('disableTouchScroll');

        $rootScope.$broadcast('JBOVERLAY_HIDDEN');
    };

    self.show = function(){
    	$('.jb-overlay-close').attr('tabindex' , '0');
    	$timeout(function(){
    		$('.jb-overlay-close').focus();
    	}, 10);
    	$(".jb-overlay-holder").fadeIn('fast');

        $('html').addClass('disableTouchScroll');
        
        $(".jb-overlay-cont").css({
            'margin-left': $(".jb-overlay-cont").width()*(-0.5),
            'margin-top': $(".jb-overlay-cont").height()*(-0.5)        
        });
    };
    
    //displays child element
    self.showChild = function(){
    	$(".jb-overlay-child-holder").fadeIn('fast');
    	$(".jb-overlay-child-holder").css({'z-index':'2000'});
    	$(".jb-overlay-child-cont").css({
            'margin-left':$(".jb-overlay-child-cont").width()*(-0.5),
            'margin-top': $(".jb-overlay-child-cont").height()*(-0.5)        
        });
        $('.jb-overlay-cont').css({'z-index':'-2'});
    };
    
    self.hideChild = function(){
    	$(".jb-overlay-child-holder").hide();
        $('.jb-overlay-cont').css({'z-index':''});
    };

    self.getAttributes = function(){
        return self.attributes;        
    };
    
    //returns the childAttributes object
    self.getChildAttributes = function(){
        return self.childAttributes;        
    } ;

    return self;
}])

.controller('jbOverlayCtrl', function ($scope, $rootScope, $compile, $http, jbOverlayFactory) {
    $scope.randomID = "jbo"+ new Date().getTime();

    //Listen for changes in the factory and apply attributes to scope
    $scope.close = function(){
    	jbOverlayFactory.hide();
    };
    $scope.keyClose = function($event){
    	if($event.keyCode == 13){
    		$scope.close();
    	}
    }
    
    $scope.closeChild = function(){
    	jbOverlayFactory.hideChild();
    };
    $rootScope.$on('JBOVERLAY_INIT', function(){
        angular.extend($scope, jbOverlayFactory.getAttributes());

        if ($scope.template === "") {
            compileLayout("<div>{{content}}</div>", true); 
        }
        else {
                compileLayout($scope.template);
        }
    });

    function compileLayout(layout, plain) {
    	$scope.innerHTML = layout; 
        
        if (plain) {
            $scope.$apply();
        }   

        jbOverlayFactory.cb(); 
        jbOverlayFactory.cb = function(){};
    }
  //Listen for changes in the factory and apply attributes to scope
    $rootScope.$on('JBOVERLAY_CHILD_INIT', function(){
        angular.extend($scope, jbOverlayFactory.getChildAttributes());
        $scope.$apply();

    });
    $scope.$on('$destroy', function(){
    });
})
.directive('jbOverlay', function ($http, $compile) {
    return {
        restrict: "C",
        transclude: true,
        template: '<div id="{{randomID}}" class="jb-overlay-holder" ng-controller="jbOverlayCtrl"><div class="jb-overlay-bg" ng-click="close()"></div><div class="jb-overlay-cont piejs" style="position:relative"><div class="jb-overlay-title piejs">{{title}}</div><div class="jb-overlay-close piejs" ng-click="close()" ng-keydown="keyClose($event)">&times;</div><div class="jb-overlay-body piejs"><div ng-include="innerHTML"> </div></div></div><div class="jb-overlay-child"></div></div>',
        link: function(scope, element, attrs) {

        }
    };
})
.directive('jbOverlayChild', function ($http, $compile) {
    return {
        restrict: "C",
        transclude: true,
        template: '<div id="{{randomID}}" class="jb-overlay-child-holder" style="position:absolute;"><div class="jb-overlay-child-cont" style="position:relative"><div class="jb-overlay-child-title">{{childTitle}}</div><div class="jb-overlay-child-close piejs" ng-click="closeChild()">&times;</div><div class="jb-overlay-child-body" >{{childContent}}</div></div></div>',
        link: function(scope, element, attrs) {

        }
    };
});

/* --- Made by justgoscha and licensed under MIT license --- */
angular.module('cityselector', ['http://www.jetblue.com/template/cityselector/suggestion.html'])
.directive('cityselector',["$timeout", function($timeout) {
    var index = -1;

    return {
        restrict: 'E',
        scope: {
            searchParam: '=ngModel',
            airports: '=listData',
            onType: '=onType',
            onSelect: '=onSelect',
            onClear: '=onClear',
            suggestionType: '@', //To identify type of suggestion box
            regions: '=popupData',
            selectedObject: '=',
            id: '=',
            blurFunction: '&',
            footerText : "="
        },
        controller: ['$rootScope', '$scope', 'AirportData', '$timeout', "$sce", function($rootScope, $scope, AirportData, $timeout, $sce) {
            // the index of the airports that's currently selected
            $scope.selectedIndex = 0;
            $scope.popupVisible = false;
            // set new index
            $scope.setIndex = function(i) {
                $scope.selectedIndex = parseInt(i);
            };

            $scope.isFocused = false;

            this.setIndex = function(i) {
                $scope.setIndex(i);
                $scope.$apply();
            };

            $scope.getIndex = function(i) {
                return $scope.selectedIndex;
            };

            $scope.footerText = $sce.trustAsHtml($scope.footerText || "");

            // watches if the parameter filter should be changed
            var watching = true;

            //the minimum number of characters to do a search or show results
            $scope.SEARCH_MIN_STRING_LEN = 3;

            // autocompleting drop down on/off
            $scope.completing = false;

            // starts autocompleting on typing in something
            $scope.$watch('searchParam', function(newValue, oldValue) {

                if(oldValue !== newValue && !!newValue && newValue.length < $scope.SEARCH_MIN_STRING_LEN ){
                    $scope.isFocused = true;
                }

                if(typeof newValue === "string" && newValue.length === 0){
                    if ($scope.onClear){
                        $scope.onClear();
                    }
                };

                if (oldValue === newValue || !oldValue || !newValue || (newValue && newValue.length < $scope.SEARCH_MIN_STRING_LEN && newValue.length > oldValue.length)) {
                    return;
                }

                

                if (watching && typeof $scope.searchParam !== 'undefined' && $scope.searchParam !== null) {

                    $scope.completing = true;
                    $scope.searchFilter = $scope.searchParam;
                    $scope.selectedIndex = 0;
                }

                // Added for JB requirement
                if ($scope.searchParam && $scope.searchParam.length < $scope.SEARCH_MIN_STRING_LEN) {
                    $scope.select();
                    $scope.setIndex(-1);
                }

                // function thats passed to on-type attribute gets executed
                if ($scope.onType){
                    $scope.onType($scope.searchParam);
                }
            });


            // for hovering over airports
            this.preSelect = function(suggestion) {

                watching = false;

                // this line determines if it is shown
                // in the input field before it's selected:
                //$scope.searchParam = suggestion;

                $scope.$apply();
                watching = true;

            };

            $scope.preSelect = this.preSelect;

            this.preSelectOff = function() {
                watching = true;
            };

            $scope.preSelectOff = this.preSelectOff;

            // selecting a suggestion with RIGHT ARROW or ENTER
            $scope.select = function(label, code) {
                $scope.closeOverlay();
                if (label) {
                    $scope.searchParam = label;
                    $scope.searchFilter = label;
                    if ($scope.onSelect)
                        $scope.onSelect(label, code, $scope.id);
                }
                watching = false;
                $scope.completing = false;
                $scope.isFocused = false;
                setTimeout(function() {
                    watching = true;
                }, 50);
                $scope.setIndex(-1);
                $scope.checkValidity();
            };

            var getKeyboardEventResult = function(keyEvent) {

                return window.event ? keyEvent.which : keyEvent.keyCode;
            };

            $scope.airportsAnchorLinks = $('.airports-list-container a');

            $scope.onSelectKey = function(event, label, code) {
                var index, eventTarget;


                 //if it is a standard browser
                 if (typeof event.target != 'undefined'){
                  eventTarget = event.target;
                 }
                 //otherwise it is IE then adapt syntax
                 else{
                  eventTarget = event.srcElement;
                 }


                var onKeyDownResult = getKeyboardEventResult(event);
                if (onKeyDownResult === 13) {
                    $scope.select(label, code);
                    // $scope.closeOverlay();
                } else if (onKeyDownResult === 37) {
                    //Left
                    $('.region-list-container').find('.region-item.active').focus();
                } else if (onKeyDownResult === 38) {
                    //Top
                    index = $.inArray(eventTarget, $scope.airportsAnchorLinks);
                    if (index > 0) {
                        $($scope.airportsAnchorLinks[index - 1]).focus();
                    }
                } else if (onKeyDownResult === 40) {
                    //Top
                    index = $.inArray(eventTarget, $scope.airportsAnchorLinks);
                    if (index < ($scope.airportsAnchorLinks.length - 1)) {
                        $($scope.airportsAnchorLinks[index + 1]).focus();
                    }
                }

                if (onKeyDownResult != 9) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                }

            };

            $scope.closeOverlay = function() {
                $scope.popupVisible = false;
                $('html').removeClass('overlay-open')
            };

            $scope.showPopUp=function(){
                $scope.popupVisible = true;
                $scope.$emit("CITYSELECTOR_FOCUS_REGION_TAB");
                $('html').addClass('overlay-open');
                $scope.completing = false;

            }

            $scope.showPopUpFromKeyPress = function($event){
                var onKeyDownResult = getKeyboardEventResult($event);
                if(onKeyDownResult == 13){ //enter
                    $event.preventDefault();
                    $event.stopImmediatePropagation();
                    $scope.showPopUp();
                }
            }
            

            

            $scope.checkValidity = function() {
                setTimeout(function() {
                    if ($scope.blurFunction && typeof $scope.blurFunction === "function") {
                        $scope.blurFunction();
                    }
                }, 0);
            };

            // use this as a filter to show only airports 
            // $scope.cityMatcher = function(stack,needle){ 
            //     var pattern;

            //     needle = needle.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"); //escape the needle so we can use it in a regexp

            //     pattern =new RegExp("\\b"+needle,"gi");  //match suggestions based on the autocomplete text search  matching the beginning of any word in the needle provided in the suggestion  

            //     return stack.match(pattern); 
            // };

            //use this to resort airport suggestion based on an arbitrary regexp
            //in this case a greedy, boundary-word match and a lazy string match form an index
            $scope.suggestionSorter = function(sortObj){

                //angular sorter takes an adjustment value that corrwsponds how many itesm to move an object up in a list
                // something like a smoothsort algo
                //negative values move items up the list

                var adjIndex = 0, isBoundaryMatch, isLazyMatch, isBlue, isMAC, needle, stack, boundarypattern, lazypattern, cityListLength, adjustments;
                
                needle = $scope.searchParam;
                stack = sortObj.needle.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"); //escape stuff that needs escaping
                labelstack = sortObj.label.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
                boundarypattern =new RegExp("\\b"+needle,"gi");  //match suggestions based on the autocomplete text search  matching the beginning of any word in the needle provided in the suggestion 
                startpattern = new RegExp("\^"+needle,"gi");
                lazypattern = new RegExp(needle,"gi");
                isBlue = sortObj.isBlueCity;
                isMAC = sortObj.isMAC;
                cityListLength = $scope.airports.length;



                isStringMatch =labelstack.match(startpattern);
                isBoundaryMatch=stack.match(boundarypattern);
                isLazyMatch=stack.match(lazypattern);

                //indexes for how much to move an object up based on match critera
                //move small items up n
                //move large change up nlogn

                adjustments = {
                    "iatamatch" : 5 * cityListLength,
                    "isBlue" :  10 * cityListLength,
                    "startmatch" : 2 * cityListLength,
                    "macMatch" : 4,
                    "boundarymatch" : 2,
                    "stringmatch" : 1
                }



                //weight iata matches highest
                if( !!sortObj&& !!needle&& sortObj.value.toLowerCase() == needle.toLowerCase() ){
                   adjIndex -= adjustments.iatamatch;
                }

                if(isStringMatch){
                    adjIndex -= adjustments.startmatch;
                }

                //show boundary matches before lazy matches
                if( isBoundaryMatch ){
                    adjIndex -= adjustments.boundarymatch;
                }
                else if( isLazyMatch ){
                    adjIndex -= adjustments.stringmatch;
                }
            
                //show airports above macs
                if( !isMAC ){
                    adjIndex -=adjustments.macMatch;
                }

                //show partners below macs
                if( isBlue ){
                    adjIndex -= adjustments.isBlue;
                }

                return adjIndex;

            }

        }],
        link: function(scope, element, attrs, $filter, $window) {
            var attr = '';

            var key = {
                left: 37,
                up: 38,
                right: 39,
                down: 40,
                enter: 13,
                esc: 27,
                tab: 9,
                space: 32
            };

            var getKeyboardEventResult = function(keyEvent) {
                return keyEvent.which !== "undefined" ? keyEvent.which : keyEvent.keyCode;
            };


            // Default atts
            scope.attrs = {
                "placeholder": "",
                "class": "",
                "id": "",
                "inputclass": "",
                "inputid": ''
            };
            
            scope.$on('CITYSELECTOR_FOCUS_REGION_TAB', function(){
                console.log(scope)

                if(typeof scope.regions !== "undefined"){
                    return;
                }

                if (scope.regions.filter(function(el){ return el.selected }).length == 0 ){
                    scope.regions[0].selected = true;
                    if( scope.regions[0].countries[0].airports.filter(function(el){ return el.selected }).length == 0  ){
                        scope.regions[0].countries[0].airports[0].selected=true;
                    }
                }
                $timeout(function(){
                    $(".region-item.active").focus();
                })
               
            })


            

            for (var a in attrs) {
                attr = a.replace('attr', '').toLowerCase();
                // add attribute overriding defaults
                // and preventing duplication
                if (a.indexOf('attr') === 0) {
                    scope.attrs[attr] = attrs[a];
                }
            }

            
            //hide popup and focus corresponding input on any escape
            angular.element(document).bind("keydown", function(e) {
                var keycode = getKeyboardEventResult(e);
                if ( scope.popupVisible  && keycode == key.esc) {
                    scope.closeOverlay();
                    scope.$apply();
                    $('#' + scope.attrs.inputid).focus();
                }
            });

            //show popup when users click flight link
            //todo: bind on element in view
            angular.element($(element[0]).find('.flight_button')).bind("click", function(){ 
                scope.showPopUp();
            })
            .bind("keydown", function($event){ 
                var thiskey = getKeyboardEventResult($event);
                if( thiskey === key.space || thiskey === key.enter ){
                    scope.showPopUp();
                    scope.$emit("CITYSELECTOR_FOCUS_REGION_TAB")
                }
            });


            //handler 
            angular.element(element[0].firstChild).bind("keydown", function(ev){
                var eventKey, index, suggestionlist, l, caller;

                eventKey  = typeof ev.which !== "undefined" ? ev.which : ev.keyCode;
                caller = typeof ev.srcElement !== "undefined" ? ev.srcElement : ev.target;

                l = $(element).find('.airport_list li').length;

                switch(eventKey){
                    case key.down:
                        index = scope.getIndex() + 1;
                        if (index < -1) {
                            index = l - 1;
                        } else if (index >= 1 && index !== l) {
                            scope.setIndex(index);
                            scope.preSelectOff();
                            scope.$apply();
                            break;
                        }else if(index === l){
                            index = 0;
                            scope.setIndex(index);
                            scope.preSelectOff();
                            scope.$apply();
                            break;
                        }
                        scope.setIndex(index);

                        if (index !== -1)
                            scope.preSelect(angular.element(angular.element(this).find('li')[index]).text());
                        break;
                    break;
                    case key.up:
                        index = scope.getIndex() - 1;
                        if (index < -1) {
                            index = l - 1;
                        } else if (index >= l) {
                            scope.setIndex(index);
                            scope.preSelectOff();
                            scope.$apply();
                            break;
                        }
                        if(index === -1){
                            index = l-1;
                            scope.setIndex(index);
                            scope.preSelectOff();
                            scope.$apply();
                            break;
                        }
                        scope.setIndex(index);

                        if (index !== -1)
                            scope.preSelect(angular.element(angular.element(this).find('li')[index]).text());

                        scope.$apply();

                    break;
                    case key.enter:
                        if( caller.className.indexOf('flight_button') == -1){
                            ev.preventDefault();
                        }
                        scope.setIndex(-1);
                        scope.select($($('.airport_list .active').get(0)).text());
                        $timeout(function(){
                            scope.blurFunction();
                        });
                    break;
                    case key.esc:
                        // disable airports on escape
                        scope.select();
                        scope.setIndex(-1);
                        scope.$apply();
                        ev.preventDefault();
                        break;
                    default:
                        break;
                }

            });

            $($(angular.element(element[0].firstChild)).find('input[type=text]')).bind('click focus', function() {


                if($(this).val().length >= scope.SEARCH_MIN_STRING_LEN ){
                    scope.completing = true;
                    scope.searchParam = scope.searchFilter = $(this).val();
                }

                scope.isFocused = true;
                scope.$apply();
                
            });

            $($(angular.element(element[0].firstChild)).find('input[type=text]')).bind('blur', function() {
                setTimeout(function(){
                    if (!$(".show-more  a").is(":focus")) {
                        scope.isFocused = false;
                        
                        scope.$apply();
                    }
                }, 200);
            });

            $(document).click(function(e) {
                if ($(e.target).hasClass('body_overlay')) {
                    scope.closeOverlay();
                }

                if ($(e.target).hasClass('airport_suggestion')) {
                    return;
                } else {
                    if ($(e.target).hasClass('autocomplete_input')) {
                        if ($(e.target).val().length < scope.SEARCH_MIN_STRING_LEN) {
                            scope.completing = false;
                            scope.$apply();
                        }
                        return;
                    }
                    scope.completing = false;
                    scope.$apply();
                }
            });

            //When tabbing out of a cityselector, suggestion box remains open; close it
            $(".suggestions_wrapper ul:last-child li:last-child a").live('blur', function(){
                scope.blurFunction();
                scope.completing = false;
                scope.isFocused = false;
                scope.$apply();
            });
            //Same as above, fix for ipad
            $(angular.element(element[0].firstChild)).live('blur', function(){
                scope.blurFunction();
                scope.completing = false;
                scope.isFocused = false;
                scope.$apply();
            })
            //Go back to the first link when tabbing from the last overlay city
            $(".airports-list-container a.city").live('blur', function(){
                setTimeout(function(){
                    if (!$("#airports-list-wrapper a").is(":focus")) {
                       $(".region-list-container .region-item").first().focus();
                    }

                }, 200);
            });

        },
        templateUrl:"http://www.jetblue.com/template/cityselector/suggestion.html"
    };
}])
.directive('suggestion', function() {
    return {
        restrict: 'E',
        require: '^cityselector', // ^look for controller on parents element
        scope: {},
        link: function(scope, element, attrs, autoCtrl) {
            element.bind('mouseenter', function() {
                autoCtrl.preSelect(attrs.val);
                autoCtrl.setIndex(attrs.index);
            });

            element.bind('mouseleave', function() {
                autoCtrl.preSelectOff();
            });
        }
    };
})
.directive('hoverClass', function () {
    return {
        restrict: 'A',
        scope: {
            hoverClass: '@'
        },
        link: function (scope, element) {
            element.on('mouseenter', function() {
                angular.element(element[0].parentElement.childNodes).removeClass(scope.hoverClass);
                element.addClass(scope.hoverClass);
            });
            element.on('mouseleave', function() {
                element.removeClass(scope.hoverClass);
            });
        }
    };
})
.directive('regionTabs',function($timeout){
    return{
        restrict:"A",
        scope:{
            items:"=regionTabs"
        },
        transclude: true,
        controller:["$scope", function($scope){
            $scope.selectTab=function(tab){
                angular.forEach($scope.items, function(tab){
                    tab.active = false;
                })
                tab.active = true;
                $timeout(function(){
                    if($('.airports-list-container div:visible .selected').length){
                        $('.airports-list-container div:visible .selected').find("a").focus()
                    }
                    else{
                        $('.airports-list-container div:visible li li:first').addClass('active').find('a').focus()
                    }
                },10);
            }
            
        }],
        template:"<div class=\"region-tabs\">"+
            "<div class=\"region-list-container\">" +
             "<div class=\"region-label\">Region</div>" +
                "<a href='javascript:void(0)'"+
                    "ng-repeat=\"item in items track by $index \" "+
                    "class=\"region-item  {{item.doubleLine}}\" "+
                    "ng-class=\"{active : item.active}\" "+
                    "ng-keydown=\"keydown($event,item)\" "+
                    "ng-click=\"selectTab(item)\" "+
                    "ng-bind=\"item.region\""+
                    "hover-class=\"hover\"></a>" +
            "</div>"+
            "<div class=\"country-list-holder\" ng-transclude></div>"+
         "</div>",
        link:function(scope, element, attrs){

            angular.forEach(scope.items, function(tab){

                if(typeof tab.selected === "boolean" && tab.selected == true){
                    tab.active = true;
                }
                else{
                    tab.active = false;
                }
                
                if (tab.region.length > 19 && tab.region.match(" ").length) {
                    tab.doubleLine = "double-region-line";
                }
                else {
                    tab.doubleLine = "";
                }
            });
            if( scope.items.filter(function(el){ return el.active }).length == 0 ){
                scope.items[0].active = true;
                scope.items[0].countries[0].airports[0].selected = true;
            }

            scope.keydown=function($event, tab){
                // console.log($event, tab)
                var key = {
                    left: 37,
                    up: 38,
                    right: 39,
                    down: 40,
                    enter: 13,
                    esc: 27,
                    tab: 9,
                    space: 32
                };
                if($event.keyCode === key.right){
                    // console.log('right?')
                    $timeout(function(){
                        $('.airports-list-container div:visible').removeClass('active');
                        if($('.airports-list-container div:not(".ng-hide") .selected').length){
                            $('.airports-list-container div:visible .selected').find("a").focus()
                        }
                        else{
                            $('.airports-list-container div:visible ul li ul li:first').addClass('active').find('a').focus()
                        }
                    });
           
                }
                if($event.keyCode === key.down){
                    $($event.target.nextElementSibling).focus();
                }
                else if($event.keyCode === key.up){
                    $($event.target.previousElementSibling).focus();
                }
                else if($event.keyCode === key.enter || $event.keyCode === key.space){
                    scope.selectTab(tab);
                }
            }
        }
    }
})
.directive('regionAirportList', function($compile){
    return {
        restrict: "A",
        require : "^regionTabs",
        transclude: true,
        link:function link(scope, element, attrs){ 
            // console.log('link', scope, element, attrs);

            if(scope.region.region == "United States"){
                element.addClass("isUS")
            }
            else{
                element.addClass("notUS");
                if( scope.region.countries.length == 2){
                    element.addClass("twoCountries");
                }
                else if( scope.region.countries.length == 1 ){
                    element.addClass("oneCountry");
                }
            }
            if (typeof Modernizr !== "undefined" && !Modernizr.csscolumns) {
                var cLength, Nc1, Nc2, Nc3, noOfColumns = 3,
                    extraElements, template, countries, airports;

                airportLiTemplate = "<li ng-repeat=\"airport in country.airports track by $index \""+
                                    "class=\"popupLinks {{'city '+ isSearched(airport.DefaultDisplayName)}}\" "+
                                    "ng-class=\"{jetblue :airport.IsBlueCity , partner :!airport.IsBlueCity, mac: airport.IsMACCode, selected:airport.selected  }\" data-info=\"{{airport}}\""+
                                    "hover-class=\"active\" >"+
                                    "<a href=\"javascript:void(0)\"" +
                                        "ng-keydown=\"keydown($event, airport)\" "+
                                        "ng-click=\"select(airport)\""+
                                        "ng-bind=\"airport.DefaultDisplayName\"></a>"+
                                "</li>";

                
                if(scope.region.region != "United States"){

                    template = "<div class=\"region noCSSColumns\" ng-hide=\"!region.active\">"+
                    "<div class=\"description-wrapper\">"+
                                "<ul class=\"city-desc clearfix\" style=\"display: block;\">"+
                                    "<li class=\"first\"><h3>Cities</h3></li>"+
                                    "<li><span>JetBlue city</span></li>"+
                                    "<li class=\"partner\"><span>Partner city</span></li>"+
                                "</ul>"+
                            "</div>"+
                    "<div class=\"region-wrap\">"+
                    "<div class=\"left\">"+
                        "<ul ng-repeat=\"country in regionArray1\">"+
                            "<li>"+
                                "<h3 class=\"country\">{{country.country}}</h3>"+
                                "<ul class=\"country-list\">"+
                                    airportLiTemplate+
                                "</ul>"+
                            "</li>"+
                        "</ul>"+
                    "</div>"+
                    "<div class=\"middle\">"+
                        "<ul ng-repeat=\"country in regionArray2\">"+
                            "<li>"+
                                "<h3 class=\"country\">{{country.country}}</h3>"+
                                "<ul class=\"country-list\">"+
                                     airportLiTemplate+
                                "</ul>"+
                            "</li>"+
                        "</ul>"+
                        "</div>"+
                        "<div class=\"right\">"+
                        "<ul ng-repeat=\"country in regionArray3\">"+
                            "<li>"+
                                "<h3 class=\"country\">{{country.country}}</h3>"+
                                "<ul class=\"country-list\">"+
                                     airportLiTemplate+
                                "</ul>"+
                            "</li>"+
                        "</ul>"+
                    "</div>"+
                    "</div></div>";
                    
                    cLength = scope.region.countries.length;
                    countries = scope.region.countries
                    Nc1 = Nc2 = Nc3 = Math.floor(cLength / noOfColumns);
                    extraElements = cLength % noOfColumns;
                    switch (extraElements) {
                        case 1:
                            ++Nc1;
                            break;
                        case 2:
                            ++Nc1;
                            ++Nc2;
                            break;

                    }
                    scope.regionArray1 = [];
                    scope.regionArray2 = [];
                    scope.regionArray3 = [];
                    for (var i = 0; i < cLength; i++) {
                        if (Nc1 > 0) {
                            scope.regionArray1.push(scope.region.countries[i]);
                            Nc1--;
                        } else if (Nc2 > 0) {
                            scope.regionArray2.push(scope.region.countries[i]);
                            Nc2--;
                        } else if (Nc3 > 0) {
                            scope.regionArray3.push(scope.region.countries[i]);
                            Nc3--;
                        }
                    }

                }
                else{

                    template = "<div class=\"region noCSSColumns\" ng-hide=\"!region.active\">"+
                    "<div class=\"description-wrapper\">"+
                                "<ul class=\"city-desc clearfix\" style=\"display: block;\">"+
                                    "<li class=\"first\"><h3>Cities</h3></li>"+
                                    "<li><span>JetBlue city</span></li>"+
                                    "<li class=\"partner\"><span>Partner city</span></li>"+
                                "</ul>"+
                            "</div>"+
                        "<div class=\"region-wrap\">"+
                        "<div class=\"left\">"+
                            "<ul>"+
                                "<li ng-repeat=\"airport in itemArray1\""+
                            "class=\"popupLinks {{'city '+ isSearched(airport.DefaultDisplayName)}}\" "+
                            "ng-class=\"{jetblue :airport.IsBlueCity , partner :!airport.IsBlueCity, mac: airport.IsMACCode, selected:airport.selected  }\" data-info=\"{{airport}}\""+
                            "hover-class=\"active\" >"+
                            "<a href=\"javascript:void(0)\"" +
                                "ng-keydown=\"keydown($event, airport)\" "+
                                "ng-click=\"select(airport)\""+
                                "ng-bind=\"airport.DefaultDisplayName\"></a>"+
                                "</li>"+
                            "</ul>"+
                        "</div>"+
                        "<div class=\"middle\">"+
                            "<ul>"+
                                "<li ng-repeat=\"airport in itemArray2\""+
                            "class=\"popupLinks {{'city '+ isSearched(airport.DefaultDisplayName)}}\" "+
                            "ng-class=\"{jetblue :airport.IsBlueCity , partner :!airport.IsBlueCity, mac: airport.IsMACCode, selected:airport.selected  }\" data-info=\"{{airport}}\""+
                            "hover-class=\"active\" >"+
                            "<a href=\"javascript:void(0)\"" +
                                "ng-keydown=\"keydown($event, airport)\" "+
                                "ng-click=\"select(airport)\""+
                                "ng-bind=\"airport.DefaultDisplayName\"></a>"+
                                "</li>"+
                            "</ul>"+
                        "</div>"+
                        "<div class=\"right\">"+
                            "<ul>"+
                                "<li ng-repeat=\"airport in itemArray3\""+
                            "class=\"popupLinks {{'city '+ isSearched(airport.DefaultDisplayName)}}\" "+
                            "ng-class=\"{jetblue :airport.IsBlueCity , partner :!airport.IsBlueCity, mac: airport.IsMACCode, selected:airport.selected  }\" data-info=\"{{airport}}\""+
                            "hover-class=\"active\" >"+
                            "<a href=\"javascript:void(0)\"" +
                                "ng-keydown=\"keydown($event, airport)\" "+
                                "ng-click=\"select(airport)\""+
                                "ng-bind=\"airport.DefaultDisplayName\"></a>"+
                                "</li>"+

                            "</ul>"+
                        "</div>"+
                        "</div>"+
                        "</div>";

                    aLength = scope.region.countries[0].airports.length;
                    airports = scope.region.countries[0].airports
                    Nc1 = Nc2 = Nc3 = Math.floor(aLength / noOfColumns);
                    extraElements = aLength % noOfColumns;
                    switch (extraElements) {
                        case 1:
                            ++Nc1;
                            break;
                        case 2:
                            ++Nc1;
                            ++Nc2;
                            break;

                    }
                    scope.itemArray1 = scope.itemArray2 = scope.itemArray3 = {};
                    scope.itemArray1 = [];
                    scope.itemArray2 = [];
                    scope.itemArray3 = [];
                    for (var i = 0; i < aLength; i++) {
                        if (Nc1 > 0) {
                            scope.itemArray1.push(airports[i]);
                            Nc1--;
                        } else if (Nc2 > 0) {
                            scope.itemArray2.push(airports[i]);
                            Nc2--;
                        } else if (Nc3 > 0) {
                            scope.itemArray3.push(airports[i]);
                            Nc3--;
                        }
                    }


                }

                
                // console.log('region1', scope.regionArray1)
                element.replaceWith($compile(template)(scope));

            }
        },
        controller:["$scope", function($scope){
            $scope.select=function(airport){
                // console.log('select', $scope, airport);
                //todo: bind this better
                $scope.$parent.$parent.$parent.select(airport.DefaultDisplayName, airport.Code)
            }
            
               
               
               
            $scope.keydown=function($event, airport){

                // console.log($event, airport);
                var index, eventTarget, cityIndex, cityArray, hilightClass, selectedItem;
                
                //if it is a standard browser
                if (typeof event.target != 'undefined'){
                    eventTarget = event.target;
                }
                //otherwise it is IE then adapt syntax 
                else{
                    eventTarget = event.srcElement;
                }
                var key = {
                    left: 37,
                    up: 38,
                    right: 39,
                    down: 40,
                    enter: 13,
                    esc: 27,
                    tab: 9,
                    space: 32
                };
                if($event.keyCode === key.left){
                    $('.region-list-container').find('.region-item.active').focus();
                }
                cityArray = $('.city:visible');
                hilightClass = 'selected';

                if($('.city.selected:visible').length){
                    selectedItem = $('.city.'+hilightClass+':visible');
                }
                else{
                    hilightClass = 'active';
                    selectedItem = $('.city.'+hilightClass+':visible');
                }
                
                cityIndex = cityArray.index(selectedItem);

                if($event.keyCode === key.down || ( $event.keyCode === key.tab  && !$event.shiftKey )){
                    $(cityArray[cityIndex]).removeClass(hilightClass);
                    cityIndex++;
                    if(cityIndex>(cityArray.length-1)){
                        cityIndex = 0;
                    }
                    $(cityArray[cityIndex]).find('a').focus();
                    $(cityArray[cityIndex]).addClass('selected');
                    $event.preventDefault();
                }
                else if($event.keyCode === key.up || ( $event.keyCode === key.tab  && $event.shiftKey )){
                    $(cityArray[cityIndex]).removeClass(hilightClass);
                    cityIndex--;
                    if(cityIndex<0){
                        cityIndex = (cityArray.length-1);
                    }
                    $(cityArray[cityIndex]).find('a').focus();
                    $(cityArray[cityIndex]).addClass('selected');
                    $event.preventDefault();
                }
                else if($event.keyCode === key.enter || $event.keyCode === key.space){
                    $event.preventDefault();
                    $scope.select(airport);
                    $('#' + $scope.attrs.inputid).focus();
                }
            }
        }],
        template:"<div ng-transclude ng-hide=\"!region.active\"></div>",
        replace:true
    }
});
angular.module("http://www.jetblue.com/template/cityselector/suggestion.html", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("http://www.jetblue.com/template/cityselector/suggestion.html",
        "<div class=\"autocomplete {{ attrs.class }}\" id=\"{{ attrs.id }}\" ng-class=\"{focused : isFocused}\">" +
        "<div class=\"input_wrapper\">" +
            "<a href='javascript:void(0)' class=\"flight_button {{attrs.inputclass}} \" ></a>" +
            "<input type=\"text\" autocomplete=\"off\"  autocorrect=\"off\" ng-model=\"searchParam\" cityformatter placeholder=\"{{ attrs.placeholder }}\" class=\"{{ attrs.inputclass }} autocomplete_input\" id=\"{{ attrs.inputid }}\" ng-blur=\"checkValidity()\"/>" +
            "<div class='clearfix'></div>"+
        "</div>" +
        "<div class=\"suggestions_wrapper\" ng-if=\"isFocused\"  >" +
            "<ul class=\"airport_list\" ng-if=\"completing && airports.length>0\">" +
                "<li class=\"airport_suggestion\"" +
                    "ng-repeat=\"suggestion in airports | filter:{needle:searchFilter}| orderBy:suggestionSorter track by $index\"" +
                    "index=\"{{ $index }}\"" +
                    "val=\"{{ suggestion.value }}\"" +
                    "ng-class=\"{ active: ($index === selectedIndex), blueCity : suggestion.isBlueCity, partnerCity : !suggestion.isBlueCity, mac: suggestion.IsMACCode || suggestion.isMAC  }\"" +
                    "ng-mousedown=\"select(suggestion.label, suggestion.value)\"" +
                    "hover-class=\"active\""+
                    
                ">" +
                    "<div>"+
                        "<a ng-bind=\"suggestion.label\" ng-mousedown=\"select(suggestion.label, suggestion.value)\">{{suggestion.label}}</a>"+
                    "</div>" +
                "</li>" +
            "</ul>" +
            "<ul class=\"city-desc\" ng-if='!!searchParam && searchParam.length >= SEARCH_MIN_STRING_LEN '>" +
                "<li class=\"first\"><span>JetBlue City</span></li>" +
                "<li class=\"partner\">Partner City</li>" +
            "</ul>" +
            "<ul class=\"show-more\">" +
                "<li class=\"viewCity first\"><a ng-click=\"showPopUp($event)\" ng-keydown=\"showPopUpFromKeyPress($event)\" href=\"javascript:void(0)\">See full city list<span class=\"right-arrow\"></span></a></li>" +
                "<li><a href=\"http://www.jetblue.com/wherewejet/\" class=\"wwjlink\">Where we jet<span class=\"right-arrow\"></span></a></li>" +
            "</ul>" +
        "</div>" +
        "</div>" +
        "<div class=\"body_overlay\" ng-show=\"popupVisible\">" +
        "<div class=\"overlayContainer\" ng-if=\"popupVisible\">" +
        "<div class=\"airport_popup_cont\">" +
        "<div class=\"airport_popup_title piejs\">Select a region and a city below</div>" +
        "<a href=\"javascript:void(0)\" class=\"airport_popup_close piejs\" ng-click=\"closeOverlay()\">&times;</a>" +
        "</div>" +

        "<div id=\"airports-list-wrapper\" ng-class='{hasFooter : !!footerText }'>" +
            "<div >" +
                "<div region-tabs=\"regions\">"+
                    "<div class=\"airports-list-container\" >" +
                        "<div ng-repeat=\"region in regions\" region-airport-list=\"regions\" >"+
                            "<div class=\"description-wrapper\">"+
                                "<ul class=\"city-desc clearfix\" style=\"display: block;\">"+
                                    "<li class=\"first\"><h3>Cities</h3></li>"+
                                    "<li><span>JetBlue city</span></li>"+
                                    "<li class=\"partner\"><span>Partner city</span></li>"+
                                "</ul>"+
                            "</div>"+
                            "<div class=\"region-wrap\">"+
                            "<ul ng-repeat=\"country in region.countries\">"+
                                "<li>"+
                                    "<h3 class=\"country\">{{country.country}}</h3>"+
                                    "<ul class=\"country-list\">"+
                                        "<li ng-repeat=\"airport in country.airports track by $index \""+
                                            "class=\"popupLinks {{'city '+ isSearched(airport.DefaultDisplayName)}}\" "+
                                            "ng-class=\"{jetblue :airport.IsBlueCity , partner :!airport.IsBlueCity, mac: airport.IsMACCode, selected:airport.selected  }\" data-info=\"{{airport}}\""+
                                            "hover-class=\"active\" >"+
                                            "<a href=\"javascript:void(0)\"" +
                                                "ng-keydown=\"keydown($event, airport)\" "+
                                                "ng-click=\"select(airport)\""+
                                                "ng-bind=\"airport.DefaultDisplayName\"></a>"+
                                        "</li>"+
                                    "</ul>"+
                                "</li>"+
                            "</ul>"+
                            "</div>"+
                        "</div>"+
                    "</div>"+
                "</div>"+
            "</div>" +
            "<p class='footerText' ng-bind-html=\"footerText\"></p>"+
        "</div>" +
        "</div>");
}]);
var jbStorageLib = (function(){
	var instance, publicInstance = {};
	function storeValue(key, count){
		var maxCount = {};
		function _addNewItem(option){
			var objectToStore, 
				successFlag = false,
				maxHistoryCount ;
			if(option && option.key && option.value){
				maxHistoryCount = maxCount[option.key] || 3;
				if(!localStorage[option.key]){
					objectToStore = {};
					objectToStore.maxCount =  maxHistoryCount;
					objectToStore.values = [];
				}
				else{
					objectToStore = JSON.parse(localStorage[option.key]);
					if(objectToStore.values.length === objectToStore.maxCount){
						objectToStore.values.shift();
					}
				}
				objectToStore.values.push(option.value);
				localStorage[option.key] = JSON.stringify(objectToStore);
				successFlag = true;
			}
			return successFlag;
		}
		function _getItem(key){
			var responseObject = {};
			if(localStorage[key] && typeof localStorage[key] !== "undefined"){
			 responseObject = JSON.parse(localStorage[key]).values.reverse();
			}
            return responseObject;
		}
		function _setMaxCount(key, count){
			if(key && count){
				maxCount[key] = count;
			}
		}
		function _addNewCookie(option){
			if(option && option.key && option.value){
		//		document.cookie= option.key+"="+encodeURL(JSON.stringify(option.value));
				
				
				
				storeHistory = function(currentEntry, type) {
				    var cookie, cookies = readCookie(type);
				 
				    cookie = [currentEntry];
				    
				    if(!maxCount[type]){
				    	_setMaxCount(type, 1);
				    }
				  
				    //save maxCount cookies only
				    for(var i = 0; i<cookies.length; i++){
				    	if(i < maxCount[type]){
				    		cookie.push(cookies[i]);
				    	}				    	
				    }
				 
				    var cookieStr = escape(JSON.stringify(cookie));
				    var expireDate = new Date();
				    expireDate.setDate(expireDate.getDate() + 1000);
				    document.cookie = type + " = " + cookieStr + "; expires=" + expireDate.toUTCString() + "; path=/";
				};
				readCookie = function(type) {
				    var cookies = document.cookie.split(";");
				    var historyCookie;
				    if (cookies === null) return [];
				 
				    for (var i = 0; i < cookies.length; i++) {
				        if (jQuery.trim(cookies[i].substr(0, cookies[i].indexOf("="))) == type)
				            historyCookie = unescape(cookies[i].substr(cookies[i].indexOf("=") + 1));
				    }
				 
				    if (historyCookie === undefined || historyCookie === "undefined")
				        return [];
				    else
				        return JSON.parse(historyCookie);
				};

				storeHistory(option.value, option.key);
				
			}	
		}
		return{
			set : _addNewItem,
			setCookie : _addNewCookie,
			get : _getItem,
			setMaxCount : _setMaxCount
		};
	}
	function initJBStorageLib(){
		if(!instance){
				instance = new storeValue();
			}
		return instance;
	}
	publicInstance.init = initJBStorageLib; 
	return publicInstance;
})();
BookerWrapper= angular.module('BookerWrapper')
.controller("Checkin", ['$scope', '$timeout', 'CheckinConfig', 'AirportData', 'jbOverlayFactory', function($scope, $timeout, CheckinConfig, AirportData, jbOverlayFactory){

	var getSelectedRegionContries, getRegionName;

	AirportData.init();

	getRegionName = function (airportCode){
		var regionCode, regionName = '';
		if(airportCode && AirportData.Station(airportCode)){
			regionCode = AirportData.Station(airportCode).RegionCodes[0];
			regionName = AirportData.regions[regionCode].Name;
		}
		return regionName;
	};

	getSelectedRegionContries= function(regions, region){
		return $.map(regions, function(el){
			if (el.region === region){
				return el.countries;
			}
		});
	};

	$scope.showToolTip = function(){
		jbOverlayFactory.init({title: CheckinConfig.PNRExample.headerText,template: "http://www.jetblue.com/includes/modules/booker_refresh/components/checkin/dist/partials/PNRExample.html",image: CheckinConfig.PNRExample.image}, null);
		jbOverlayFactory.show();
	}

	$scope.ciconfig = {};
	$.extend($scope.ciconfig, CheckinConfig);


	$scope.citySelectorFooter ="";
	$scope.fromLAirports = AirportData.StationList({format:"suggestion"});
	$scope.fromPAirports = AirportData.StationList({
		sort:'ASC',
		group:"region", 
		"regionOptions":{
			"flatten":["United States"]
		}
	});

	var defaultObj= {
		label : '',
		code : '',
		region : 'United States',
		countries : getSelectedRegionContries($scope.fromPAirports, 'United States')
	};

	$scope.fromSelectedObj = {};
	$.extend($scope.fromSelectedObj, defaultObj);

	$scope.errors=[];



	$scope.onFromSelection = function (label, code) {
		if(!code){
			code = AirportData.Station(label);
            if((typeof code) == "object" && code.Code){ //Fix for PROD bug 5/14, code needs to be "XXX" not object
            	code = code.Code;
            }
		}
		if(label && code){
			$scope.fromSelectedObj.label = label;
			$scope.fromSelectedObj.code = code;
			$scope.fromSelectedObj.region = getRegionName(code);
			$scope.fromSelectedObj.countries  = getSelectedRegionContries($scope.fromPAirports, $scope.fromSelectedObj.region);
			$scope.fromResult = label;
		}
	};

	$scope.onFromCityType = function(typedthings){
	};

	// To handle inline errors
	$scope.firstNameError = "";
	$scope.lastNameError = "";
	$scope.PNRError = "";
	function isInvalidName(name){
		return (!(/^[a-zA-Z ]*$/.test(name)) || typeof name == 'undefined' || name.length < 1)
	}
	$scope.checkFirstName = function(){
		$scope.firstNameError = "";
		if ( isInvalidName($scope.firstName) ){
			$scope.firstNameError = $scope.ciconfig.messages.errors.INVALID_FIRST_NAME;
		}
	};

	$scope.checkLastName = function(){
		$scope.lastNameError = "";
		if( isInvalidName($scope.lastName) ){
			$scope.lastNameError = $scope.ciconfig.messages.errors.INVALID_LAST_NAME;
		}
	};

	$scope.checkDepartCity = function(){
		$scope.departCityError = "";
		if( !$scope.fromSelectedObj.code ||  $scope.fromSelectedObj.code === "" || $scope.fromSelectedObj.code.length<3 )
		{
			$scope.departCityError = $scope.ciconfig.messages.errors.INVALID_DEPARTURE;
		}
	};

	$scope.checkPNR = function(){
		$scope.PNRError = "";
		if ( typeof $scope.pnr == 'undefined' || $scope.pnr.length < 6 || $scope.pnr.length>7 || !$scope.pnr.match(/^[a-z0-9]+$/i) )
		{
			$scope.PNRError = $scope.ciconfig.messages.errors.INVALID_PNR;
		}
	};

	//feed cityselector cities

	//validate name, pnr length, and valid city

	//on submit, build form action url

	$scope.handleSubmit=function(){
		console.log("SUBMIT")
		var form=$('.jbCheckin form');
			$scope.cierrors = [];

		//validate
		//no pnr or invalid pnr
		if ( typeof $scope.pnr == 'undefined' || $scope.pnr.length < 6 || $scope.pnr.length>7 || !$scope.pnr.match(/^[a-z0-9]+$/i) )
		{
			$scope.cierrors.push($scope.ciconfig.messages.errors.INVALID_PNR);
			$scope.PNRError = $scope.ciconfig.messages.errors.INVALID_PNR;
		}
		if( !$scope.fromSelectedObj.code ||  $scope.fromSelectedObj.code === "" || $scope.fromSelectedObj.code.length<3 )
		{
			$scope.cierrors.push($scope.ciconfig.messages.errors.INVALID_DEPARTURE);
			$scope.departCityError = $scope.ciconfig.messages.errors.INVALID_DEPARTURE;
		}
		//no first name
		if ( isInvalidName($scope.firstName) ){
			$scope.cierrors.push($scope.ciconfig.messages.errors.INVALID_FIRST_NAME);
			$scope.firstNameError = $scope.ciconfig.messages.errors.INVALID_FIRST_NAME;
		}
		//no last name
		if( isInvalidName($scope.firstName) ){
			$scope.cierrors.push($scope.ciconfig.messages.errors.INVALID_LAST_NAME);
			$scope.lastNameError = $scope.ciconfig.messages.errors.INVALID_LAST_NAME;
		}

		//boot if there are errors; extend to scope to make visible
		if ($scope.cierrors.length){
			$scope.errors=$scope.cierrors;
			$scope.showErrors = true;
			return;
		}

		//manipulate checkin string
		var checkinReqParams = '';

		checkinReqParams += 'recordLocator='+$scope.pnr;
		checkinReqParams += '&departureCity='+$scope.fromSelectedObj.code;
		checkinReqParams += '&firstName='+$scope.firstName;
		checkinReqParams += '&lastName='+$scope.lastName;

		form.attr('action', form.attr('data-forward-action')+"?"+checkinReqParams);

		//submit
		try{
			form.submit();
		}
		catch(e){
			EL.catcher("error in checkin submit")
		}
	};
	$scope.closeErrors = function(){
		$scope.showErrors = false;
	};

}]).directive('confirmFilter', function() {
	//This directive prevents users from inputing non alpha characters
	  return {
		    require: 'ngModel',
		    link: function (scope, element, attr, ngModelCtrl) {
		      function fromUser(text) {
		    	  var newText = text.replace(/[^a-zA-Z ]/g, "");
		    	  if(newText != text){
		    		  ngModelCtrl.$setViewValue(newText);
			            ngModelCtrl.$render();
			            return newText;
		    	  }else{
		    		  return text;
		    	  }
		      }
		      ngModelCtrl.$parsers.push(fromUser);
		    }
	  }
}).directive('checkin',function(){
	return{
		restrict:"E",
		replace:true,
		templateUrl:"http://www.jetblue.com/includes/modules/booker_refresh/components/checkin/dist/partials/checkin.html"
	};
});

BookerWrapper= angular.module('BookerWrapper')
.controller("FlightStatus", ['$scope', '$rootScope', '$timeout', '$filter', "FlightStatusConfig", 'AirportData', "$sce", function($scope, $rootScope, $timeout, $filter, FlightStatusConfig, AirportData, $sce){

	$scope.fsconfig={};
	$.extend($scope.fsconfig, FlightStatusConfig);
	$scope.$watch('type', function(){
		//wrapper specific configs
		if( !!$scope.type && !!FlightStatusConfig.scopeDefaults.all ){
			$.extend(true, $scope.fsconfig, FlightStatusConfig.scopeDefaults.all);
		}

		$scope.$emit("FLIGHT_STATUS_LOADED");
	});
	var getSelectedRegionContries, getRegionName;

	AirportData.init();

	getRegionName = function (airportCode){
		var regionCode, regionName = '';
		if(airportCode && AirportData.Station(airportCode)){
			regionCode = AirportData.Station(airportCode).RegionCodes[0];
			regionName = AirportData.regions[regionCode].Name;
		}
		return regionName;
	};

	getSelectedRegionContries= function(regions, region){
		return $.map(regions, function(el){
			if (el.region === region){
				return el.countries;
			}
		});
	};

	//generate model for fancy lookin dropdown
	$scope.lookupdays=[];
	$scope.setLookupDays = function(){
		var today = new Date ();
		var yesterday = new Date();
		yesterday.setDate(yesterday.getDate() - 1);
		var tomorrow = new Date();
		tomorrow.setDate(tomorrow.getDate() + 1);

		var lookupdays = [
			{
				"name": "Yesterday, "+$filter('date')(yesterday, "EEEE MMMM dd"),
				"value": $filter('date')(yesterday, "MM/dd/yyyy")
			},
			{
				"name": "Today, "+$filter('date')(today, "EEEE MMMM dd") ,
				"value": $filter('date')(today, "MM/dd/yyyy")
			},
			{
				"name": "Tomorrow, "+$filter('date')(tomorrow, "EEEE MMMM dd") ,
				"value": $filter('date')(tomorrow, "MM/dd/yyyy")
			},
		];
		$scope.lookupdays = lookupdays;
	};
	$scope.setLookupDays();

	//set default lookup day
	//todo:  this is currently setting by label and not value and is being overwirtten in bookerwrapper fs controller
	$scope.flightDate = $scope.lookupdays[1].name;

	$scope.flightStatusSearchType = 'byCities';
	$scope.fromResult = '';

	$scope.fromLAirports = $scope.toLAirports = AirportData.StationList({filter:[{blueCity:true}],format:"suggestion"});
	$scope.fromPAirports = $scope.toPAirports = AirportData.StationList({
		filter:[{blueCity:true}],
		sort:'ASC',
		group:"region", 
		"regionOptions":{
			"flatten":["United States"]
		}
	});

	var defaultObj= {
		label : '',
		code : '',
		region : 'United States',
		countries : getSelectedRegionContries($scope.fromPAirports, 'United States')
	};

	$scope.fromSelectedObj = {};
	$scope.toSelectedObj = {};
	$.extend($scope.fromSelectedObj, defaultObj);
	$.extend($scope.toSelectedObj, defaultObj);

    $rootScope.FSSelectFrom = function (code) {
        if (AirportData.Station([code])) {
            $scope.onFromSelection(AirportData.Station([code]).DefaultDisplayName, code);
        }
    }
    $rootScope.FSSelectTo = function (code) {
        if (AirportData.Station([code])) {
            $scope.onToSelection(AirportData.Station([code]).DefaultDisplayName, code);
        }
    };
 
    $rootScope.FSSelectDate = function (date) {
        $scope.flightDate = $filter('date')(new Date(date), "MM/dd/yyyy");
    }
 


	$scope.onFromSelection = function(label, code){
		var region;

		if(!label){
			return;
		}

		if(!code){
			code = AirportData.Station(label).Code
		}

		region = getRegionName(code);

		$scope.toLAirports = AirportData.StationList({filter:[{destination:code},{"blueCity":true}],format:"suggestion"});

		$scope.fromSelectedObj ={
			"label" : label,
			"code" : code,
			"region" : region
		};
		$scope.fromResult = label;

		$scope.toPAirports = AirportData.StationList({
			filter:[{destination:code},{"blueCity":true}],
			sort:'ASC',
			group:"region", 
			"regionOptions":{
				"flatten":["United States"]
			}
		});

        if(region){
            angular.forEach($scope.fromPAirports, function(regionitem){
                if(regionitem.region == region){
                    regionitem.selected = true;
                }
                else{
                	regionitem.selected = false;
                }
                angular.forEach(regionitem,function(region){
                	angular.forEach(regionitem.countries, function(country){
                		angular.forEach(country.airports, function(airport){
                    		if( airport.Code == code ){
                    			airport.selected = true;
                    		}
                    		else{
                    			airport.selected = false;
                    		}
                		});
                	});
                });
            })
        }
        else{
            angular.forEach($scope.toPAirports, function(regionitem){
        		regionitem.selected = false;
        	})
            $scope.fromPAirports[0].selected=true;
        }

	};

	$scope.onToSelection = function(label, code){

		var region;

		if(!label){
			return;
		}

		if(!code){
			code = AirportData.Station(label).Code
		}

		region = getRegionName(code);

		$scope.fromLAirports = AirportData.StationList({filter:[{origin:code},{"blueCity":true}],format:"suggestion"});

		$scope.toSelectedObj ={
			"label" : label,
			"code" : code,
			"region" : region
		};
		$scope.toResult = label;


		$scope.fromPAirports = AirportData.StationList({
			filter:[{origin:code},{"blueCity":true}],
			sort:'ASC',
			group:"region",
			"regionOptions":{
				"flatten":["United States"]
			}
		});

		if(region){
            angular.forEach($scope.toPAirports, function(regionitem){
                if(regionitem.region == region){
                    regionitem.selected = true;
                }
                else{
                	regionitem.selected = false;
                }
                angular.forEach(regionitem,function(region){
                	angular.forEach(regionitem.countries, function(country){
                		angular.forEach(country.airports, function(airport){
                    		if( airport.Code == code ){
                    			airport.selected = true;
                    		}
                    		else{
                    			airport.selected = false;
                    		}
                		});
                	});
                });
            })
        }
        else{
        	angular.forEach($scope.toPAirports, function(regionitem){
        		regionitem.selected = false;
        	})
            $scope.toPAirports[0].selected=true;
        }

	};

	$scope.onFromCityType = function(typedthings){
	};


	$scope.onToCityType = function(typedthings){
	};

	// Code for inline error

	$scope.departCityError = '';
	$scope.returnCityError = '';
	$scope.flighNoError = '';

	$scope.checkDepartCity = function(){
		$scope.departCityError = '';
		if( !$scope.fromSelectedObj.code ||  $scope.fromSelectedObj.code === "" || $scope.fromSelectedObj.code.length<3 )
		{
			$scope.departCityError = $scope.fsconfig.messages.errors.INVALID_DEPARTURE;
		}
	};

	$scope.checkReturnCity = function(){
		$scope.returnCityError = '';
		if( !$scope.toSelectedObj.code ||  $scope.toSelectedObj.code === "" || $scope.toSelectedObj.code.length<3 )
		{
			$scope.returnCityError = $scope.fsconfig.messages.errors.INVALID_ARRIVAL;
		}
	};

	$scope.checkFlighNo = function(){
		$scope.flighNoError = '';
		if( !$scope.lookupNumber ||  $scope.lookupNumber === "" ||  !$scope.lookupNumber.match(/^[a-z0-9]+$/i) )
		{
			$scope.flighNoError = $scope.fsconfig.messages.errors.INVALID_FLIGHT_NUMBER;
		}
	}

	$scope.handleSubmit=function(){

		var form = $('.jbFlightStatus form'), reqParams = "";
		$scope.fserrors = [];
		//check that [origin and destination are defined] OR there is a flight number
		//and that there is a date

			//origin [$scope.fromSelectedObj] has a valid iata code

			//destination [$scope.toSelectedObj] has a valid iata code

			//flightnumber is alphanumberic

			//flightDate is a valid date
		if($scope.flightStatusSearchType == 'byCities') {
			if( !$scope.fromSelectedObj.code ||  $scope.fromSelectedObj.code === "" || $scope.fromSelectedObj.code.length<3 )
			{
				$scope.fserrors.push($scope.fsconfig.messages.errors.INVALID_DEPARTURE+"\n");
				$scope.departCityError = $scope.fsconfig.messages.errors.INVALID_DEPARTURE;
			}
			if( !$scope.toSelectedObj.code ||  $scope.toSelectedObj.code === "" || $scope.toSelectedObj.code.length<3 )
			{
				$scope.fserrors.push($scope.fsconfig.messages.errors.INVALID_ARRIVAL);
				$scope.returnCityError = $scope.fsconfig.messages.errors.INVALID_ARRIVAL;
			}
		} else if ($scope.flightStatusSearchType == 'byFlightNumber') {
			if( !$scope.lookupNumber ||  $scope.lookupNumber === "" ||  !$scope.lookupNumber.match(/^[a-z0-9]+$/i) )
			{
				$scope.fserrors.push($scope.fsconfig.messages.errors.INVALID_FLIGHT_NUMBER);
				$scope.flighNoError = $scope.fsconfig.messages.errors.INVALID_FLIGHT_NUMBER;
			}
		}


		//show error if found
		if ($scope.fserrors.length){
			$scope.errors=$scope.fserrors;
			$scope.showErrors = true;
			return;
		}


		//no errors, submit to fs app
		//yes this is hokey but the service has some particular requirements
		reqParams += "FlightDate="+encodeURIComponent($scope.flightDate.replace(/\b0(?=\d)/g, ''));
		reqParams += "&FlightNum="+($scope.lookupNumber||"");
		reqParams += "&FlightOrgn="+($scope.fromSelectedObj.code||"");
		reqParams += "&FlightDestn="+($scope.toSelectedObj.code||"");
		reqParams += "&FlightStatusType="+($scope.flightStatusSearchType||"");
		reqParams += "&fs_from_field="+($scope.fromSelectedObj.label.replace(/(\s)/gi,"+").replace(/(\,)/gi, "%2C").replace(/(\()/gi, "%28").replace(/(\))/gi, "%29")||"");
		reqParams += "&fs_to_field="+($scope.toSelectedObj.label.replace(/(\s)/gi,"+").replace(/(\,)/gi, "%2C").replace(/(\()/gi, "%28").replace(/(\))/gi, "%29")||"");
		reqParams += "&flight_number_field="+($scope.lookupNumber||"");
		reqParams += "&departure_date="+encodeURIComponent($scope.flightDate.replace(/\b0(?=\d)/g, ''));


		form.attr("action", form.attr("data-forward-action")+"?"+reqParams);
		try{
			form.submit();
		}
		catch(e){
			console.warn("Error in Flight Status Form submission");
		}


	};
	$scope.closeErrors = function(){
		$scope.showErrors = false;
	};

	$scope.partnerAirlineText = $scope.fsconfig.messages.citySelector.FOOTER_TEXT_PARTNER_AIRLINES;

}])
.config(fsSceConfig)
.directive('flightstatus',function(){
	return{
		restrict:"E",
		replace:true,
		templateUrl:"http://www.jetblue.com/includes/modules/booker_refresh/components/flightstatus/dist/partials/flightstatus.html"
	};
});

function fsSceConfig($sceDelegateProvider){

    $sceDelegateProvider.resourceUrlWhitelist(['self', 'http://www*.jetblue.com/**', 'https://www*.jetblue.com/**'])

}

fsSceConfig.$inject = ['$sceDelegateProvider'];


var jbSelect = angular.module('jbSelectModule',[])
/*
 * Creates a select box with a style select box above it. Requires options of type : {name: "Display Name", value:"2"} and model.
 * 
 *Example: 
 * <div jb-select id="jbBookerGroup-{{$index}}" ng-model="$parent.passenger[group.id]" ng-options="group.options" class="passengerGroup" name="guestTypes[{{$index}}].amount">
</div>

 */
.directive('jbSelect', function ($compile) {
    return {
    	restrict : 'A',
        template: '<div class="dropdown jbSelectContainer {{active}} {{disabledClass}}">'+ 
        		'<select style="display:none" name="{{name}}" ng-model="ngModel" ng-options="option.value as option.name for option in ngOptions"> </select>'+
        		'<div tabindex="0" ng-focus="open()" ng-touchstart="toggleOpen()"  ng-mousedown="toggleOpen()" ng-keydown="keydown($event)" class="jbSelectLabel" ng-blur="close($event)">{{findValue()}}<span class="jbSelectArrow"></span></div>'+
        		'<ol class="jbDropdownList piejs"> <li ng-class="{focused : focusIndex == $index}" ng-mouseover="hover($index)" ng-touchstart="updateModel(option.value)" ng-mousedown="updateModel(option.value)"' +
        		'ng-repeat="option in ngOptions">{{option.name}}</li> </ol></div>',
        scope: {
            ngModel: '=',
            ngOptions: '='
        },
        controller : function($scope, $window){
        	$scope.active = "";
        	$scope.close = function($event){
        		$scope.active = "";
        	}
        	
        	$scope.keydown = function($event){
        		$scope.active ="open";
        		if($event.keyCode == 38){ //up
            		$scope.focusIndex = (($scope.focusIndex-1) % $scope.ngOptions.length);
        		}else if($event.keyCode == 40){ //down
            		$scope.focusIndex = (($scope.focusIndex+1) % $scope.ngOptions.length);
        		}else if($event.keyCode == 13 ){ //enter
        			$scope.updateModel($scope.ngOptions[$scope.focusIndex].value);
        		} else{
        			return
        		}
        			$event.preventDefault();
        	};
        	
        	$scope.hover = function(index){
        		$scope.focusIndex = index;
        	}
        	
        	$scope.open = function(){
        		if($scope.disabled != "true"){
        			$scope.focusIndex = 0;
        			$scope.active = "open";
        		}
        	}
        	
        	$scope.toggleOpen = function(){
        		if($scope.active == ""){
        			$scope.open();
        		}else{
        			$scope.close();
        		}
        	}
        	$scope.updateModel = function(val){
        		$scope.ngModel = val;
        		$scope.close();
        	}        	
        	$scope.findValue = function(){
        		if($scope.ngOptions){
	        		for(var i = 0; i < $scope.ngOptions.length; i++){
	        			if($scope.ngOptions[i] && $scope.ngOptions[i].value == $scope.ngModel){
	        				return $scope.ngOptions[i].name;
	        			} 
	        		}
        		} 
        		return $scope.ngModel;
        	}

        },
    	link:function(scope, element, attrs){
    		scope.name = attrs.name;
    		scope.disabled = attrs.ngDisabled;
    		if(scope.disabled == "true"){
    			scope.disabledClass = "disabled";
    		}
    		scope.$watch(
    		          function(scope) {
    		            return scope.$eval(attrs.compile);
    		          },
    		          function(value) {
    		            element.html(value);
    		            $compile(element.contents())(scope);
    		          }
    		        );
    	}
    };
});
angular.module('AirportDataProvider', [])
	.service('AirportData', ['$http', '$rootScope', function($http, $rootScope) {
	var airportData = this, odAPIFailure = false;

	this.init = function(config, cb) {
		//config{}
			//url
			//prefix
			//postfix
			//success
			//error
		//cb: callback to chain
		// config = $.extend({}, config, {
		// 	url: "http://www2.jetblue.com/apis/ODAPI/JSData.aspx?ODType=All",
		// 	prefix: "",
		// 	postfix: ""
		// });
		if(typeof config !== "undefined"){
			if(typeof config.success !== "function" && typeof cb === "function"){
				config.success = cb;
			}
		}
		else {
			config = {};
		}

		config.odType = config.odType || false;
		config.format = config.format || false;


		this.__getODData({type:config.odType, format:config.format});

		//todo: return a promise here once odapi async is in place
		if (!odAPIFailure){
			if(typeof config.success === "function"){
				config.success.call();
			}
			return true;
		}
		else{
			console.warn('AirportData: OD API failure.  Broadcasting a global window event for anything listening.');
			jQuery(window).trigger('OD API FAILURE');

			if(config.error){
				console.warn('AirportData: OD API Failure.  Executing configured application fallback.');
				config.error.call();
			}
			return false;
		}
	};
	this.StationList = function(config) {
		//config{}
		//filter: [{},{}] origin:IATA, destination:IATA, blueCity: boolean, region:name, country:name
		//format: `` airport, station, iata, or suggestion
		//sort: {} property:ASC/DESC
		//group: `` region or country
		//keepEmptyStations !! flag to keep stations without origin/destination pairs
		var model = airportData.airportArray;

		config = config || {};

		if(typeof model === "undefined"){
			console.warn('station list called when model empty');
			return;
		}

		if( !(typeof keepEmptyStations === "boolean" && !!keepEmptyStations) ){
			//filter out any station without origin and destination code
			model = model.filter(function(el){
				return ( airportData.__getDestinationsFromOrigin(el.Code)!==false && airportData.__getOriginsFromDestination(el.Code) !==false );
			});
		}
		


		if (config.filter instanceof Array) {
			$.each(config.filter, function(i, el) {
				if (typeof el.origin === "string") {
					model = model.filter(function(k, v) {
						if ($.inArray(k.Code, airportData.__getOriginsFromDestination(el.origin)) > -1) {
							return k;
						}
					});
				} else if (typeof el.destination === "string") {
					model = model.filter(function(k, v) {
						if ($.inArray(k.Code, airportData.__getDestinationsFromOrigin(el.destination)) > -1) {
							return k;
						}
					});
				} else if (typeof el.region === "string") {
					var filteredRegionCode = "";
					$.each(airportData.regions, function(k, v) {
						if (el.region == v.Name) {
							filteredRegionCode = k;
						}
					});
					model = model.filter(function(k, v) {
						if ($.inArray(filteredRegionCode, k.RegionCodes) > -1) {
							return k;
						}
					});
				} else if (typeof el.country === "string") {
					model = model.filter(function(k, v) {
						if (k.CountryName === el.country) {
							return k;
						}
					});
				} else if (typeof el.blueCity === "boolean") {
					model = model.filter(function(k, v) {
						if (k.IsBlueCity === el.blueCity) {
							return k;
						}
					});
				}
				else if ( typeof el.iata !== "undefined" ){
					var filter = el.iata;

					if( typeof el.iata === "string" ){
						filter = filter.split(" ");
					}

					if(! filter instanceof Array ){
						return false;
					}
					model = model.filter(function(k, v){
						if( $.inArray(k.Code, filter) == -1 ){
							return k;
						}
					});
				}
			});
		}

		if (!!config.format) {
			if (config.format === "suggestion") {
				model = airportData.__getSuggestionsFromAirports(model);
				//no sort or grouping is available for suggestion,
				//it is inhernetly a flat list for an autocomplete suggestions
				//short circuit return
				return model;
			// } else if (config.format === "station") {
			// 	model = model.map();
			} else if (config.format === "IATA") {
				model = $.map(model, function(el, i) {
					return el.Code;
				});
			// } else if (config.format === "airport") {
			// 	//do nothing
			}
		}

		if (typeof config !== "undefined" && typeof model !== "undefined" && !!config.sort) {
			//ASC,DESC
			model = model.sort(function(a, b) {
				//Region
				if (config.group === 'region' && a.RegionCodes[0] !== b.RegionCodes[0])
				{
					if (a.RegionCodes[0] === 'US')
					{
						return config.sort === 'DESC' ? 1 : -1;
					}
					else if (b.RegionCodes[0] === 'US')
					{
						return config.sort === 'DESC' ? -1 : 1;
					}

					var aRegionName = airportData.regions[a.RegionCodes[0]].Name,
						bRegionName = airportData.regions[b.RegionCodes[0]].Name
					;
					if (aRegionName < bRegionName)
					{
						return config.sort === 'DESC' ? 1 : -1;
					}
					else if (aRegionName > bRegionName)
					{
						return config.sort === 'DESC' ? -1 : 1;
					}
				}

				//Country or same Region
				if ((config.group === 'country' || config.group === 'region') && a.CountryCode !== b.CountryCode)
				{
					if (a.CountryName < b.CountryName)
					{
						return config.sort === 'DESC' ? 1 : -1;
					}
					else if (a.CountryName > b.CountryName)
					{
						return config.sort === 'DESC' ? -1 : 1;
					}
				}

				//Ungrouped or same Country + Region
				if (a.DefaultDisplayName < b.DefaultDisplayName) {
					return config.sort === 'DESC' ? 1 : -1;
				}
				if (a.DefaultDisplayName > b.DefaultDisplayName) {
					return config.sort === 'DESC' ? -1 : 1;
				}
				return 0;
			});
		}

		if (!!config.group) {
			if (config.group == "country" || config.group == "region") {
				model = airportData.__groupAirportsByCountry(model);
			}

			if (config.group == "region") {
				model = airportData.__groupCountriesByRegion(model, config.regionOptions);
			}
		}

		return model;
	};
	this.Station = function(input, config) {
		if(typeof input === "undefined"){
			return false;
		}
		if (typeof airportData.airportLookup[input] !== "undefined") {
			return airportData.airportLookup[input];
		}
		else if( typeof airportData.labelLookup[input] !== "undefined" ){
			return airportData.airportLookup[airportData.labelLookup[input]];
		}
		else if( typeof config !== "undefined" && typeof config.fuzzy == "boolean" && !!config.fuzzy ){
			var displayNames = airportData.__getLabelLookup(),
				fuzzyLookup = false
			;

			$.each(displayNames, function(displayName)
			{
				if (displayName.toUpperCase().indexOf(input.toUpperCase()) > -1) //Found match
				{
					if (fuzzyLookup === false)
					{
						fuzzyLookup = airportData.Station(displayName); //First match found
					}
					else
					{
						fuzzyLookup = false; //Second match found
						return false; //Break
					}
				}
			});

			return fuzzyLookup;
		}
		else{
			return false;
		}
	};
	this.Route = function(originIATA, destinationIATA) {
		if (typeof originIATA === "undefined" || typeof destinationIATA === "undefined") {
			return false;
		}

		return airportData.__getRouteProperties(originIATA, destinationIATA);

	};
	this.__assertLoadedODData = function(type)
	{
		var odData,
			validODTypes =
			[
				'DEALS',
				'JBONLY',
				'BFF',
				'FLIGHTSTATUS',
				'GETAWAYS',
				'ALL'
			];

        if (typeof type === "string"){
            odData = odTypeIsLoaded(type);
            
            if(typeof odData.Error !== "undefined"){
                odData = false;
            }
        }
        else{
            for (var i = validODTypes.length - 1; i >= 0; i--)
            {
                odData = odTypeIsLoaded(validODTypes[i]);
                if(typeof odData.Error !== "undefined"){
                    odData = false;
                }
                if (odData)
                {
                    break;
                }
            }
        }

		function odTypeIsLoaded(type)
		{
			return typeof window['od' + type] === 'undefined' ? false : window['od' + type];
		}

		
		return odData;
	};
	this.__getODData = function(config) {
		//config{}
		//url
		//prefix
		//postfix


		//AJAX is nice and all but od service doesnt support it
		//yet so we're stubbing it with an inline ref to odALL
		//deal with it

		// var request, response;

		// return $http.get(config.url)
		// 	.success(function(data){

		// 		data = data.replace(/^(var\sodALL\s\=\s)/,"")
		// 		data = data.replace(/(\;)$/,"")

		// 		airportData.__setAirports(data)

		// });

		var odData = airportData.__assertLoadedODData(config.type);
		if (odData)
		{
			airportData.__setAirports(odData, config.format);
			airportData.__setRoutePairs(odData, config.format);
			airportData.__setRegions(odData, config.format);
			airportData.__setCountries(odData, config.format);
			airportData.__setLabelLookup(odData, config.format);
		}
		else
		{
			$rootScope.$broadcast('OD API FAILURE');
			odAPIFailure = true;
		}
	};
	this.__setAirports = function(data, options) {

		//copy airports from one iata to another if for some reason you want to do that

		if (typeof options === "object"){
			if ( typeof options.copyAirports === "object" ){
				$.each(options.copyAirports, function(key, value){
					if ( typeof value === "string" ){
						data.Airports[key] = data.Airports[value];
					}
					else if ( typeof value === "function" ){ 
						data.Airports[key] = value.call();
					}
				});

			}
		}

		this.airportArray = $.map(data.Airports, function(el) {
			el.isBlueCity = el.IsBlueCity;
			el.label = el.DefaultDisplayName;
			el.needle = [el.Name, el.DefaultDisplayName].join(' ');
			el.value = el.Code;
			return el;
		});

		this.airportLookup = data.Airports;
	};
	this.__setRoutePairs = function(data, options) {
		var stack = [];

		stack = data.RouteSets;


		// copy routes from one station for another if for some reason you want to do that

		if (typeof options === "object"){
			if ( typeof options.copyRoutes === "object" ){
				$.each(options.copyRoutes, function(key, value){
					if ( typeof value === "string" ){
						stack[key] = stack[value];
					}
					else if ( typeof value === "function" ){ 
						stack[key] = value.call();
					}
				});

			}
		}

		//go through each route and add a OriginCodes property
		//if it doesnt already exist

		$.each(stack, function(originkey, originobj) {
			$.each(originobj.DestinationCodes, function(index, destinationCode)
			{
				if(typeof data.RouteSets[destinationCode] !== "undefined" ){
					data.RouteSets[destinationCode].OriginCodes = data.RouteSets[destinationCode].OriginCodes || [];
					data.RouteSets[destinationCode].OriginCodes.push(originkey);
				}
				
			});

		});

		this.routePairs = stack;
	};
	this.__setCountries = function(data) {
		this.countries = data.Countries;
	};
	this.__setRegions = function(data) {
		this.regions = data.Regions;
	};
	this.__setLabelLookup = function(data){
		airportData.labelLookup = {};
		$.each(airportData.airportLookup, function(k,v){ airportData.labelLookup[v.DefaultDisplayName]=v.Code;});
	};
	this.__getAirports = function() {
		return this.airports;
	};
	this.__getRoutePairs = function() {
		return this.routePairs;
	};
	this.__getCountries = function() {
		return this.countries;
	};
	this.__getRegions = function() {
		return this.regions;
	};
	this.__getLabelLookup = function(){
		return this.labelLookup;
	};
	this.__getOriginsFromDestination = function(IATA) {

		if( typeof airportData.__getRoutePairs()[IATA] !== "undefined" && typeof airportData.__getRoutePairs()[IATA].OriginCodes !== "undefined"){
			return airportData.__getRoutePairs()[IATA].OriginCodes;
		}
		else{
			return false;
		}
		

	};
	this.__getDestinationsFromOrigin = function(IATA) {

		if( typeof airportData.__getRoutePairs()[IATA] !== "undefined" && typeof airportData.__getRoutePairs()[IATA].DestinationCodes !== "undefined"){
			return airportData.__getRoutePairs()[IATA].DestinationCodes;
		}
		else{
			return false;
		}

	};
	this.__getSuggestionsFromAirports = function(airports) {
		//take from existing

		if(!airports){
			return;
		}


		var alphaSortedAirports = [],
			suggestionsByCity = [],
			suggestionsByMAC = [],
			partnerCitySuggestions = [],
			suggestionsByCountry = [];

		function alphaSortAirports(a, b) {
			if (a.DefaultDisplayName < b.DefaultDisplayName) {
				return -1;
			}
			if (a.DefaultDisplayName > b.DefaultDisplayName) {
				return 1;
			}
			return 0;
		}

		function suggestionByCityModel(airport) {
			return {
				label: airport.DefaultDisplayName,
				needle: [
					airport.DefaultDisplayName,
					airport.City,
					airport.Code,
					airport.CountryName
				].join(' '),
				value: airport.Code,
				isBlueCity: airport.IsBlueCity,
				isMAC: false,
				basis: 'city'
			};
		}

		function suggestionByMACModel(airport) {
			var childMACCodeArray = airport.ChildrenMACCodes.map(function(airportCode) {
				return [
					airportCode,
					airportData.Station(airportCode).City,
					airportData.Station(airportCode).DefaultDisplayName
				].join(' ');
			});
			return {
				label: airport.DefaultDisplayName,
				needle: [
					airport.DefaultDisplayName,
					airport.Code,
					childMACCodeArray.join(' ')
				].join(' '),
				value: airport.Code,
				isBlueCity: airport.IsBlueCity,
				isMAC: true,
				basis: 'MAC'
			};
		}

		function partnerCitySuggestionModel(airport) {
			return {
				label: airport.DefaultDisplayName,
				needle: [
					airport.DefaultDisplayName,
					airport.City,
					airport.Code,
					airport.CountryName
				].join(' '),
				value: airport.Code,
				isBlueCity: airport.IsBlueCity,
				isMAC: false,
				basis: 'partner'
			};
		}


		alphaSortedAirports = airports.sort(alphaSortAirports);
		$.each(alphaSortedAirports, function(airportIndex, airport) {
			//suggestionsByCity
			if (!airport.IsMACCode && airport.IsBlueCity) {
				suggestionsByCity.push(suggestionByCityModel(airport));
			}
			//suggestionsByMAC
			if (airport.IsMACCode) {
				suggestionsByMAC.push(suggestionByMACModel(airport));
			}
			//partnerCitySuggestions
			if (!airport.IsBlueCity) {
				partnerCitySuggestions.push(partnerCitySuggestionModel(airport));
			}
		});

		return suggestionsByCity.concat(suggestionsByMAC, partnerCitySuggestions);
	};
	this.__groupAirportsByCountry = function(model) {
		var countryIndexReference = {};
		if (typeof model === "undefined" ){
			return;
		}
		return model.reduce(function(countries, airport, index) {
			if (typeof countryIndexReference[airport.CountryCode] === 'undefined') {
				countryIndexReference[airport.CountryCode] = countries.length;
				countries.push({
					country: airportData.countries[airport.CountryCode].Name,
					airports: []
				});
			}

			var countryIndex = countryIndexReference[airport.CountryCode];
			countries[countryIndex].airports.push(airport);
			return countries;
		}, []);
	};
	this.__groupCountriesByRegion = function(model, regionOptions) {

		var regionIndexReference = {};

		if (typeof model === "undefined" ){
			return;
		}

		var groupedByRegion = model.reduce(function(regions, country, countryIndex) {
			var countryData = airportData.countries[country.airports[0].CountryCode];

			countryData.RegionCodes.forEach(function(regionCode)
			{
				var regionName = airportData.regions[regionCode].Name;
				if (typeof regionIndexReference[regionCode] === 'undefined') {
					regionIndexReference[regionCode] = regions.length;
					regions.push({
						region: airportData.regions[regionCode].Name,
						Countries: [],
						countries: []
					});
				}

				var regionIndex = regionIndexReference[regionCode];
				regions[regionIndex].Countries.push(country);
				regions[regionIndex].countries.push(country);
			});

			return regions;
		}, []);

		if (regionOptions) {
			if (regionOptions.flatten) {
				groupedByRegion = airportData.__flattenRegions(groupedByRegion, regionOptions.flatten);
			}
		}

		return groupedByRegion;
	};
	this.__flattenRegions = function(model, regionsToFlatten) {
		if (typeof model === "undefined") {
			return;
		}

		var flattenedRegions = [];

		model.forEach(function(region) {
			if (regionsToFlatten.indexOf(region.region) !== -1) {
				var flattenedAirports = region.countries.reduce(function(flattenedCountries, country)
				{
					return flattenedCountries.concat(country.airports);
				}, []);

				flattenedAirports.sort(function(a,b){
					if (a.DefaultDisplayName < b.DefaultDisplayName) {
						return typeof config !== "undefined" && config.sort === 'DESC' ? 1 : -1;
					}
					if (a.DefaultDisplayName > b.DefaultDisplayName) {
						return typeof config !== "undefined" && config.sort === 'DESC' ? -1 : 1;
					}
				});

				var flattenedRegion = [{
					airports: flattenedAirports,
					country: region.region
				}];

				flattenedRegions.push({
					region: region.region,
					Countries: flattenedRegion,
					countries: flattenedRegion
				});
			} else {
				flattenedRegions.push(region);
			}
		});

		return flattenedRegions;
	};
	this.__getRouteProperties = function(originIATA, destinationIATA){
		var stack, needle, props, res;

		stack = airportData.__getRoutePairs();

		if(typeof stack[originIATA] === "undefined"){
			//no route for origin
			return false;
		}
		else{
			stack = stack[originIATA];
		}

		needle = (typeof stack.DestinationCodes !== "undefined") ? stack.DestinationCodes.indexOf(destinationIATA) : -1;

		//route origin-destination pair does not exist
		if(needle < 0){
			return false;
		}

		res ={
				"origin":originIATA,
				"destination":destinationIATA
		};

		props = (typeof stack.DestinationProperties !== "undefined") ? stack.DestinationProperties[needle] : {};

		res = $.extend(res, props);

		return res;
	};

	return airportData;

}]);
var jbPie = angular.module('jbPieModule',[])
/*
 * This directive will attach elements with class piejs to PIE.js if it exists on window. This is a fix for rounded corner support in ie8.
 * Note: custom css changes may need to accompany this as the css3pie elements sometimes create layering issues 
 *
 * This should be included in index to load PIE when on ie
 * 	<!--[if lt IE 9]>
	  <script type="text/javascript" src="styles/PIE_IE678.js"/*tpa=http://www.jetblue.com/includes/modules/booker_refresh/app/styles/PIE_IE678.js*/></script>
	<![endif]-->
	<!--[if IE 9]>
	  <script type="text/javascript" src="styles/PIE_IE9.js"/*tpa=http://www.jetblue.com/includes/modules/booker_refresh/app/styles/PIE_IE9.js*/></script>
	<![endif]-->

 */
.directive('piejs', function(){
	return{
		restrict:'C',
		link : function(scope, element, attr){
			function applyPIE(args){
				if(window.PIE && typeof PIE.attach == 'function'){
					PIE.attach(element[0]);
				}
			};
			applyPIE();
			scope.$on('$destroy', function(){
				if(window.PIE && typeof PIE.detach == 'function'){
					PIE.detach(element[0]);
				}
			})
		}
	}
});
/*! angular-shims-placeholder - v0.4.1 - 2015-04-09
* https://github.com/cvn/angular-shims-placeholder
* Copyright (c) 2015 Chad von Nau; Licensed MIT */
(function (angular, document, undefined) {
  'use strict';
  angular.module('ng.shims.placeholder', []).service('placeholderSniffer', [
    '$document',
    function ($document) {
      this.emptyClassName = 'empty', this.hasPlaceholder = function () {
        var test = $document[0].createElement('input');
        return test.placeholder !== void 0;
      };
    }
  ]).directive('placeholder', [
    '$timeout',
    '$document',
    '$interpolate',
    '$animate',
    'placeholderSniffer',
    function ($timeout, $document, $interpolate, $animate, placeholderSniffer) {
      if (placeholderSniffer.hasPlaceholder())
        return {};
      var documentListenersApplied = false, supportsAnimatePromises = parseFloat(angular.version.full) >= 1.3;
      return {
        restrict: 'A',
        require: '?ngModel',
        priority: 110,
        link: function (scope, elem, attrs, ngModel) {
          var orig_val = getValue(), domElem = elem[0], elemType = domElem.nodeName.toLowerCase(), isInput = elemType === 'input' || elemType === 'textarea', is_pwd = attrs.type === 'password', text = attrs.placeholder, emptyClassName = placeholderSniffer.emptyClassName, hiddenClassName = 'ng-hide', clone;
          if (!isInput) {
            return;
          }
          attrs.$observe('placeholder', function (newValue) {
            changePlaceholder(newValue);
          });
          if (is_pwd) {
            setupPasswordPlaceholder();
          }
          setValue(orig_val);
          elem.bind('focus', function () {
            if (elem.hasClass(emptyClassName)) {
              elem.val('');
              elem.removeClass(emptyClassName);
              domElem.select();
            }
          });
          elem.bind('blur', updateValue);
          if (!ngModel) {
            elem.bind('change', function () {
              changePlaceholder($interpolate(elem.attr('placeholder'))(scope));
            });
          }
          if (ngModel) {
            ngModel.$render = function () {
              setValue(ngModel.$viewValue);
              if (domElem === document.activeElement && !elem.val()) {
                domElem.select();
              }
            };
          }
          if (!documentListenersApplied) {
            $document.on('selectstart', function (e) {
              var elmn = angular.element(e.target);
              if (elmn.hasClass(emptyClassName) && elmn.prop('disabled')) {
                e.preventDefault();
              }
            });
            documentListenersApplied = true;
          }
          function updateValue(e) {
            var val = elem.val();
            if (elem.hasClass(emptyClassName) && val && val === text) {
              return;
            }
            conditionalDefer(function () {
              setValue(val);
            });
          }
          function conditionalDefer(callback) {
            if (document.documentMode <= 11) {
              $timeout(callback, 0);
            } else {
              callback();
            }
          }
          function setValue(val) {
            if (!val && val !== 0 && domElem !== document.activeElement) {
              elem.addClass(emptyClassName);
              elem.val(!is_pwd ? text : '');
            } else {
              elem.removeClass(emptyClassName);
              elem.val(val);
            }
            if (is_pwd) {
              updatePasswordPlaceholder();
              asyncUpdatePasswordPlaceholder();
            }
          }
          function getValue() {
            if (ngModel) {
              return scope.$eval(attrs.ngModel) || '';
            }
            return getDomValue() || '';
          }
          function getDomValue() {
            var val = elem.val();
            if (val === attrs.placeholder) {
              val = '';
            }
            return val;
          }
          function changePlaceholder(value) {
            if (elem.hasClass(emptyClassName) && elem.val() === text) {
              elem.val('');
            }
            text = value;
            updateValue();
          }
          function setAttrUnselectable(elmn, enable) {
            if (enable) {
              elmn.attr('unselectable', 'on');
            } else {
              elmn.removeAttr('unselectable');
            }
          }
          function setupPasswordPlaceholder() {
            clone = angular.element('<input type="text" value="' + text + '"/>');
            stylePasswordPlaceholder();
            clone.addClass(emptyClassName).addClass(hiddenClassName).bind('focus', hidePasswordPlaceholderAndFocus);
            domElem.parentNode.insertBefore(clone[0], domElem);
            var watchAttrs = [
                attrs.ngDisabled,
                attrs.ngReadonly,
                attrs.ngRequired,
                attrs.ngShow,
                attrs.ngHide
              ];
            for (var i = 0; i < watchAttrs.length; i++) {
              if (watchAttrs[i]) {
                scope.$watch(watchAttrs[i], asyncUpdatePasswordPlaceholder);
              }
            }
          }
          function updatePasswordPlaceholder() {
            stylePasswordPlaceholder();
            if (isNgHidden()) {
              clone.addClass(hiddenClassName);
            } else if (elem.hasClass(emptyClassName) && domElem !== document.activeElement) {
              showPasswordPlaceholder();
            } else {
              hidePasswordPlaceholder();
            }
          }
          function asyncUpdatePasswordPlaceholder() {
            if (supportsAnimatePromises) {
              $animate.addClass(elem, '').then(updatePasswordPlaceholder);
            } else {
              $animate.addClass(elem, '', updatePasswordPlaceholder);
            }
          }
          function stylePasswordPlaceholder() {
            clone.val(text).attr('class', elem.attr('class') || '').attr('style', elem.attr('style') || '').prop('disabled', elem.prop('disabled')).prop('readOnly', elem.prop('readOnly')).prop('required', elem.prop('required'));
            setAttrUnselectable(clone, elem.attr('unselectable') === 'on');
          }
          function showPasswordPlaceholder() {
            elem.addClass(hiddenClassName);
            clone.removeClass(hiddenClassName);
          }
          function hidePasswordPlaceholder() {
            clone.addClass(hiddenClassName);
            elem.removeClass(hiddenClassName);
          }
          function hidePasswordPlaceholderAndFocus() {
            hidePasswordPlaceholder();
            domElem.focus();
          }
          function isNgHidden() {
            var hasNgShow = typeof attrs.ngShow !== 'undefined', hasNgHide = typeof attrs.ngHide !== 'undefined';
            if (hasNgShow || hasNgHide) {
              return hasNgShow && !scope.$eval(attrs.ngShow) || hasNgHide && scope.$eval(attrs.ngHide);
            } else {
              return false;
            }
          }
        }
      };
    }
  ]);
}(window.angular, window.document));