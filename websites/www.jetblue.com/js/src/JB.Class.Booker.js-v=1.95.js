/** 
* Flight Booker Base Class.
* @function JB.Class.Booker
* @param {String} container The id or class selector of the flight booker markup. Example: "#jb-primary-links"

@mx 3.0 changes:
@mx3.0-201472
@mx3.0-201477
@MX3.0.0.1- for Switchfly
@mx3.0.1-201478 //show continue/hideX changed from go back 
@mx3.0.1-2014710 //hide close back to original
@MX3.0.2- removed CPX from bypass
@MX3.1-20140819 - turn off console and fix carib column placement
*/

//if (window.console) console.info("~~~~~~~ Flight Booker Base Class. @mx 3.1-2014-0819");
 
var itemPerColumn = 34;
var isFlightsHotelSavings = false;
/* Arnaldo Capo */
// This function is to bypass is jetBlue Airways for partners. (One-Off MX 1.8)
function bypassIsJB(airport) {
       
    // @MX3.0.2- for Switchfly
    return $.inArray(airport.code, ['AXA', 'MAZ', 'NEV', 'EIS', 'VQS', 'VIJ']) !== -1;  
}
function getOrigins(airports) {

    //THIS IS FOR THE SAVE WITH GETAWAYS BOOKER CITYLIST
    var jbCities = [];
    for (var i = 0; i < airports.length; i++) {

    //    console.log("... apc " + airports[i].code + " isFlightsHotelSavings " + isFlightsHotelSavings);

        if ((airports[i].jb && airports[i].code.length == 3
                && $.inArray(airports[i].code, MAClist) == -1)
            || bypassIsJB(airports[i])) { //exclude special destinations, exclude MAC cities
            jbCities.push(airports[i]);
        }
    }
    return jbCities;
}
function getDestinations(airports) {
    //THIS IS FOR THE SAVE WITH GETAWAYS BOOKER CITYLIST
    var jbCities = [];
    for (var i = 0; i < airports.length; i++) {
        if ((airports[i].jb
                && $.inArray(airports[i].code, MAClist) == -1)
            || bypassIsJB(airports[i])) {//exclude MAC cities
            jbCities.push(airports[i]);
        }
    }
    return jbCities;
}

function constructHTMLTemplate(airports, gwItemsPerColumn) {

    //THIS IS FOR THE SAVE WITH GETAWAYS BOOKER CITYLIST
    var templates = {};


    for (var i = 0; i < airports.length; i++) {
        var partner = (airports[i].jb) ? "" : " class='partner'";
        var style = $.inArray(airports[i].code, MAClist) == -1 ? "" : " class='MAC'";
        var routes = airports[i].routes.toString().replace(/\,/gi, " ").replace(/0/gi, "");
        if (!!!templates[airports[i].cc]) templates[airports[i].cc] = { html: [], count: 0, sets: 0, region: airports[i].region.code };
        if (!!!templates[airports[i].cc].html[0] && airports[i].cc !== "US") templates[airports[i].cc].html[0] = "<ul><li class='list-head " + airports[i].country + "'>" + airports[i].country + "</li>"; // global space value
        if (!!!templates[airports[i].cc].html[templates[airports[i].cc].sets]) templates[airports[i].cc].html[templates[airports[i].cc].sets] = "<ul>";
        templates[airports[i].cc].html[templates[airports[i].cc].sets] += "<li" + partner + " code='" + airports[i].code + "' routes='" + routes + "'><a" + style + " href='#' rel='" + airports[i].code + "' title=\"" + airports[i].label + "\" index='" + i + "'>" + airports[i].label + "</a></li>";
        templates[airports[i].cc].count++;
        if (!!airports[i + 1] && (airports[i].cc !== airports[i + 1].cc)) templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
        if (i == (airports.length - 1)) {
            templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
            break;
        }
        if ((templates[airports[i].cc].count % gwItemsPerColumn) == 0) {
            templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
            templates[airports[i].cc].sets++;
        }
    }

    return templates;
}

function constructPanel(panel, templates, regionCounts) {
    //THIS IS FOR THE SAVE WITH GETAWAYS BOOKER CITYLIST


    for (country in templates) {
        if (!!!templates[templates[country].region]) templates[templates[country].region] = {};
        if (!!!templates[templates[country].region].count) templates[templates[country].region].count = 0;
        switch (templates[country].region) {
            case "http://www.jetblue.com/js/src/INTERLINE.US":
                var $tab = panel.tabs.filter("div[role='http://www.jetblue.com/js/src/INTERLINE.US']");
                $tab.columns = $tab.find(".city-list");
                for (var i = 0; i < templates[country].html.length; i++) {
                    $tab.columns.eq(i).append(templates[country].html[i]);
                }
                regionCounts.usCount++;
                break;
            case "INTERLINE.AFRICA":
                var $tab = panel.tabs.filter("div[role='INTERLINE.AFRICA']");
                $tab.columns = $tab.find(".city-list");
                for (var i = 0; i < templates[country].html.length; i++) {
                   templates[templates[country].region].count += templates[country].count + 2;
                    var column = 0;
                    $tab.columns.eq(column).append(templates[country].html[i]);
                }
                regionCounts.afCount++;
                break;
            case "INTERLINE.MIDDLEEAST":
                var $tab = panel.tabs.filter("div[role='INTERLINE.MIDDLEEAST']");
                $tab.columns = $tab.find(".city-list");
                for (var i = 0; i < templates[country].html.length; i++) {
                     templates[templates[country].region].count += templates[country].count + 2;
                    var column = 0;
                    $tab.columns.eq(column).append(templates[country].html[i]);
                }
                regionCounts.meCount++;
                break;
            case "INTERLINE.PACIFIC":
                var $tab = panel.tabs.filter("div[role='INTERLINE.PACIFIC']");
                $tab.columns = $tab.find(".city-list");
                for (var i = 0; i < templates[country].html.length; i++) {
                     templates[templates[country].region].count += templates[country].count + 2;
                   var column = 0;
                    $tab.columns.eq(column).append(templates[country].html[i]);
                }
                regionCounts.paCount++;
                break;
            case "INTERLINE.CARRIBEAN_C-AMERICA":
                var $tab = panel.tabs.filter("div[role='INTERLINE.CARRIBEAN_C-AMERICA']");
                $tab.columns = $tab.find(".city-list");
                for (var i = 0; i < templates[country].html.length; i++) {
                    templates[templates[country].region].count += templates[country].count + 2;
                    var column = 0;
                    $tab.columns.eq(column).append(templates[country].html[i]);
                }
                regionCounts.caCount++;
                break;
            case "http://www.jetblue.com/js/src/INTERLINE.ASIA":
                var $tab = panel.tabs.filter("div[role='http://www.jetblue.com/js/src/INTERLINE.ASIA']");
                $tab.columns = $tab.find(".city-list");
                for (var i = 0; i < templates[country].html.length; i++) {
                    templates[templates[country].region].count += templates[country].count + 2;
                     var column = 0;
                    $tab.columns.eq(column).append(templates[country].html[i]);
                }
                regionCounts.asCount++;
                break;
            case "INTERLINE.S-AMERICA":
                var $tab = panel.tabs.filter("div[role='INTERLINE.S-AMERICA']");
                $tab.columns = $tab.find(".city-list");
                for (var i = 0; i < templates[country].html.length; i++) {
                     templates[templates[country].region].count += templates[country].count + 2;
                    var column = 0;
                    $tab.columns.eq(column).append(templates[country].html[i]);
                }
                regionCounts.saCount++;
                break;
            case "INTERLINE.EUROPE":
                var $tab = panel.tabs.filter("div[role='INTERLINE.EUROPE']");
                $tab.columns = $tab.find(".city-list");
                for (var i = 0; i < templates[country].html.length; i++) {
                    templates[templates[country].region].count += templates[country].count;
                    var column = Math.floor(templates[templates[country].region].count / 10);
                    $tab.columns.eq(column).append(templates[country].html[i]);
                }
                regionCounts.euCount++;
                break;
        }



    }



}

function finishSubmitProcess(form, historyDate, fromValue, dpartValue, toValue, returnValue) {
    //store search history
    var currentEntry = {
        "drop_down_value": $fflightForm['origin'].value + " > " + $fflightForm['destination'].value + ", " + historyDate,
        "search_type": "find_flights",
        "flight_type": $fflightForm['journeySpan'].value == "RT" ? "round_trip" : "one_way",
        "from_field": fromValue,
        "departure_field": dpartValue,
        "adult_count": $fflightForm['numAdults'].value,
        "kid_count": $fflightForm['numChildren'].value,
        "infant_count": $fflightForm['numInfants'].value,
        "to_field": toValue,
        "return_field": returnValue ? returnValue : "null",
        "fare_display": $fflightForm['fareDisplay'].value,
        "flexible": $('#flexible').is(':checked')
    };
    storeHistory(currentEntry, "fflight-form-data");

    if ($('#flexible').is(':checked')) {
        var url = "/BestFareFinder/?origin=" + $fflightForm['origin'].value + "&destination=" + $fflightForm['destination'].value + "&trip=" + $fflightForm['journeySpan'].value + "&fare=" + $fflightForm['fareDisplay'].value + "&adult=" + form["adult_count"].value + "&kid=" + form["kid_count"].value + "&infant=" + form["infant_count"].value + "&departure=" + dpartValue;
        window.location = url;
    } else {
        form.submit();
    }

    // clear form values
    form["adult_count"].value = 1;
    form["kid_count"].value = 0;
    form["infant_count"].value = 0;

}

function alignOkCancelErrorButtons() {
    if (!$('.cancel-btn').is(":visible")) {

        $('.ok-btn').css('margin-left', 0);

    } else {
        $('.ok-btn').css('margin-left', 10);
    }

}

JB.Class.Booker = JB.Class.extend({
    /**
    * Library dependencies
    * @type object
    */
    require: {
        "jQuery.validator": "http://www.jetblue.com/js/src/vendor/jquery.validate.min",
        "JB.Model.getAirports": "src/JB.Model",
        "JB.Helper.beautifier": "src/JB.Helper",
        "JB.Class.Modal": "src/JB.Class.Modal",
        "JB.Class.Autocomplete": "src/JB.Class.Autocomplete",
        "JB.Class.Calendar": "src/JB.Class.Calendar",
        "JB.Class.RecentSearch": "src/JB.Class.RecentSearch"
    },
    /**
    * @constructor
    */
    init: function (container, options) {
        this.options = {
            bookFlightURL: "#flight",
            bookVacationURL: "#vacation",
            views: {
                findFlight: "fflight",
                checkin: "checkin",
                flightStatus: "fstatus",
                findVacation: "fvacation",
                error: "error"
            },
            errors: {
                departure: GlobalErrors.MainBooker.departure,
                destination: GlobalErrors.MainBooker.destination,
                departDate: GlobalErrors.MainBooker.departDate,
                returnDate: GlobalErrors.MainBooker.returnDate,
                maxInfants: GlobalErrors.MainBooker.maxInfants,
                minPassengers: GlobalErrors.MainBooker.minPassengers,
                maxPassengers: GlobalErrors.MainBooker.maxPassengers,
                kidsAge: GlobalErrors.MainBooker.kidsAge,
                todayWarning: GlobalErrors.MainBooker.todayWarning,
                confirmationNumber: GlobalErrors.MainBooker.confirmationNumber,
                firstName: GlobalErrors.MainBooker.firstName,
                lastName: GlobalErrors.MainBooker.lastName,
                flightNumber: GlobalErrors.MainBooker.flightNumber,
                partnerTB: GlobalErrors.MainBooker.partnerTB,
                sameDay: GlobalErrors.MainBooker.sameDay,
                owInt: GlobalErrors.MainBooker.owInt,
                fareCalendarInterlineNA: GlobalErrors.MainBooker.fareCalendarInterlineNA,
                fdInterlineNoDate: GlobalErrors.MainBooker.fdInterlineNoDate,
                seasonalRouteError: GlobalErrors.MainBooker.seasonalRouteError,
                dayOfWeekRouteError: GlobalErrors.MainBooker.dayOfWeekRouteError,
                seasonalRouteError_vacations: GlobalErrors.MainBooker.seasonalRouteError_vacations,
                dayOfWeekRouteError_vacations: GlobalErrors.MainBooker.dayOfWeekRouteError_vacations,
                returnMonthPastDepartureError: GlobalErrors.MainBooker.returnMonthPastDepartureError
            },
            mini: false,
            today: JB.Session.calendar.today,
            endDate: JB.Session.calendar.endDate
        };
        $.extend(this.options, options || {});
        this.container = $(container);
        this._super();
    },
    /** @ignore */
    _build: function () {
        //this is used to check unacomp minor popup in submitting FF form
        var customErrorHandlerUsed = false;

        var self = this;


        this.booker = this.container;
        this.booker.selects = this.booker.find("select");
        this.booker.radio = this.booker.find("input[type=radio]");
        this.booker.stateHistory = {
            previous: null,
            current: "fflight"
        };
        this.booker.togglers = {
            checkin: this.booker.find(".context-switch .checkin-btn-box"),
            flightStatus: this.booker.find(".context-switch .flight-status-btn-box"),
            close: this.booker.find(".context-switch .close-btn")
        };
        this.booker.views = {
            findFlight: this.options.views.findFlight,
            isFindVacation: false,
            checkin: this.options.views.checkin,
            flightStatus: this.options.views.flightStatus,
            error: this.options.views.error
        };

        // Book Flights Panel
        this.booker.bookFlights = this.booker.find("div.book-flights");
        this.booker.bookFlights.type = {
            findFlight: this.options.views.findFlight,
            findVacation: this.options.views.findVacation
        };
        this.booker.bookFlights.togglers = {
            findFlight: this.booker.bookFlights.find(".flights-btn").parent(),
            findVacation: this.booker.bookFlights.find(".vacations-btn").parent()
        };
        self.changeViewTo(self.booker.views.findFlight);
        this.booker.submitFF = {
            submit: function (form) {
                $fflightForm = document.getElementById("fflight");
                var fromValue = form['from_field'].value;

                //if MAC city add 3 letter airport code.
                var match = /\w{3}(?=\)$)/;
                if (!match.exec(fromValue)) {
                    for (var j = 0; j < MAClist.length; j++) {

                        var macCity = window["o" + MAClist[j]];

                        if (macCity.name == fromValue) {
                            $fflightForm['origin'].value = macCity.code;
                            break;
                        }
                    }
                } else { $fflightForm['origin'].value = fromValue.substring(fromValue.length - 4, fromValue.length - 1); }

                var toValue = form['to_field'].value;

                if (!match.exec(toValue)) {
                    for (var j = 0; j < MAClist.length; j++) {

                        var macCity = window["o" + MAClist[j]];

                        if (macCity.name == toValue) {
                            $fflightForm['destination'].value = macCity.code;
                            break;
                        }
                    }
                } else { $fflightForm['destination'].value = toValue.substring(toValue.length - 4, toValue.length - 1); }

                var dpartValue = form['departure_field'].value;
                var d = new Date(dpartValue);
                var mm = d.getMonth() + 1;
                var dd = d.getDate();
                $fflightForm['departureDate'].value = d.getFullYear() + "-" + (mm < 10 ? "0" + mm : mm) + "-" + (dd < 10 ? "0" + dd : dd);
                var historyDate = (mm < 10 ? "0" + mm : mm) + "/" + (dd < 10 ? "0" + dd : dd) + "/" + d.getFullYear();

                if ($fflightForm['journeySpan'].value == "RT") {
                    var returnValue = form['return_field'].value;
                    var d = new Date(returnValue);
                    var mm = d.getMonth() + 1;
                    var dd = d.getDate();
                    $fflightForm['returnDate'].value = d.getFullYear() + "-" + (mm < 10 ? "0" + mm : mm) + "-" + (dd < 10 ? "0" + dd : dd);
                } else {
                    $fflightForm['returnDate'].value = "";
                }
                $fflightForm['numAdults'].value = form["adult_count"].value;
                $fflightForm['numChildren'].value = form["kid_count"].value;
                $fflightForm['numInfants'].value = form["infant_count"].value;

                if ($fflightForm['numAdults'].value <= 0 && $fflightForm['numChildren'].value > 0) {



                    //minorMsg and minorHdr are vars from a js loaded via library.aspx
                    //that file is IM controlled to set the message they want

                    if (minorMsg == "" || (!minorMsg)) {
                        //set default message if none exists
                        minorMsg = "\n\nChildren between the ages of 5 and under 14 years who will be traveling alone are considered unaccompanied minors.\n\nUnaccompanied minors may only travel on non-stop flights. They cannot travel on connecting flights or direct flights (flights that make a stop, but do not change aircraft).\n\nFlights booked are subject to an \044100 fee per person each way for unaccompanied minors.\n\nOnce booked, please call 1-800-JETBLUE (538-2583) to provide us with the name, address and phone number, as it appears on the photo ID, of the person dropping off and picking up the child. The fee will also be assessed at that time.\n";
                    }
                    //Use a flag to bypass the error ok button handlers
                    customErrorHandlerUsed = true;
                    //Get the current state - its possible we might be coming from a validation message and already be in 'error' mode    
                    var state = self.booker.stateHistory.current;


                    self.booker.error.messages.html(""); // Clear Messages
                    self.booker.error.messages.append("<p id='unacompMinorError'>" + minorMsg + "</p>");
                    self.booker.error.buttons.ok.find('span').text("OK");

                    if (state != self.booker.views.error) self.changeViewTo(self.booker.views.error);
                    jQuery('#oops').text(minorHdr);

                    self.booker.error.buttons.ok.click(function (e) {
                        self.booker.error.buttons.ok.find('span').text("OK");
                        finishSubmitProcess(form, historyDate, fromValue, dpartValue, toValue, returnValue);

                    });
                    $('.close-btn').click(function (e) {
                        self.booker.error.buttons.ok.find('span').text("OK");
                        finishSubmitProcess(form, historyDate, fromValue, dpartValue, toValue, returnValue);
                    });
                }
                else {
                    finishSubmitProcess(form, historyDate, fromValue, dpartValue, toValue, returnValue);
                }

            }
        };
        this.booker.submitFV = {
            submit: function (form) {
                $fvacationForm = document.getElementById("fvacation");

                //setVacationSubmitUrl();
                if ($('input[name=nav]').val() == "default") {
                    $('#fvacation').attr('action', cashHandler);
                } else {

                    $('#fvacation').attr('action', pointsHandler);
                }

                var fromValue = form['vac_from_field'].value;
                $fvacationForm['area1'].value = fromValue.substring(fromValue.length - 4, fromValue.length - 1);
                $fvacationForm['panInput_5'].value = fromValue;
                var toValue = form['vac_to_field'].value;
                $fvacationForm['panInput_6'].value = toValue;

                var dpartValue = form['vac_departure_field'].value;
                var d = new Date(dpartValue);
                var mm = d.getMonth() + 1;
                var dd = d.getDate();
                $fvacationForm['date1'].value = (mm < 10 ? "0" + mm : mm) + "/" + (dd < 10 ? "0" + dd : dd) + "/" + d.getFullYear();
                var retValue = form['vac_return_field'].value;
                var d = new Date(retValue);
                var mm = d.getMonth() + 1;
                var dd = d.getDate();
                $fvacationForm['date2'].value = (mm < 10 ? "0" + mm : mm) + "/" + (dd < 10 ? "0" + dd : dd) + "/" + d.getFullYear();

                $fvacationForm['adults'].value = form['vac_adult_count'].value;
                $fvacationForm['dynamic_children'].value = form['kid_vacation'].value;
                $fvacationForm['infants_in_lap'].value = form['vac_infant_count'].value;
                $fvacationForm['child_age1'].value = form['kid_age_1'].value;
                $fvacationForm['child_age2'].value = form['kid_age_2'].value;
                $fvacationForm['child_age3'].value = form['kid_age_3'].value;
                $fvacationForm['num_rooms'].value = form['hotel_room_count'].value;

                if ($('#cash_fare').is(':checked')) {
                    $fvacationForm['nav'].value = 'default';
                }

                if ($('#cashPoints_fare').is(':checked')) {
                    $fvacationForm['nav'].value = 'Redeem';
                }


                $fvacationForm['area2'].value = toValue.substring(toValue.length - 4, toValue.length - 1);

                //This is not used anymore - this is refering to the old static booker-vacations.js file 
                //                jQuery.each(VacationDests, function (key, value) {
                //                    if (value[1] == toValue)
                //                        $fvacationForm['area2'].value = value[0].replace(/_/g, " ");
                //                });

                //store search history
                var currentEntry = {
                    "drop_down_value": $fvacationForm['area1'].value + " > " + toValue.substring(toValue.length - 4, toValue.length - 1) + ", " + $fvacationForm['date1'].value,
                    "search_type": "find_vacations",
                    "flight_type": "round_trip",
                    "vac_from_field": fromValue,
                    "vac_departure_field": dpartValue,
                    "vac_adult_count": $fvacationForm['adults'].value,
                    "kid_vacation": $fvacationForm['dynamic_children'].value,
                    "kid_age_1": $fvacationForm['child_age1'].value,
                    "kid_age_2": $fvacationForm['child_age2'].value,
                    "kid_age_3": $fvacationForm['child_age3'].value,
                    "vac_infant_count": $fvacationForm['infants_in_lap'].value,
                    "vac_to_field": toValue,
                    "vac_return_field": retValue,
                    "hotel_room_count": $fvacationForm['num_rooms'].value,
                    "vac_fare_display": $fvacationForm['nav'].value
                };

                storeHistory(currentEntry, "fvacation-form-data");
                form.submit();
                // clear form values
                form['vac_adult_count'].value = 2;
                form['kid_vacation'].value = 0;
                form['vac_infant_count'].value = 0;
            }
        };

        // Find Flights Form
        this.booker.findFlight = this.booker.find("form.fflight-form");
        this.booker.findFlight.flightTypes = this.booker.findFlight.find("input[name=flight_type]");
        this.booker.findFlight.recentSearches = this.booker.findFlight.find(".recent-search-dropdown");
        this.booker.findFlight.adultsCount = this.booker.findFlight.find(".adult-dropdown select");
        this.booker.findFlight.kidsCount = this.booker.findFlight.find(".kid-dropdown select");
        this.booker.findFlight.infantsCount = this.booker.findFlight.find(".infant-dropdown select");
        this.booker.findFlight.fareTypes = this.booker.findFlight.find("input[name=fare_display]");
        this.booker.findFlight.togglers = {
            from: this.booker.findFlight.find(".from-btn"),
            to: this.booker.findFlight.find(".to-btn"),
            departDate: this.booker.findFlight.find(".depart-wrap"),
            returnDate: this.booker.findFlight.find(".return-wrap")
        };
        // Find Vacations Form
        this.booker.findVacation = this.booker.find("form.fvacation-form");
        this.booker.findVacation.recentSearches = this.booker.findVacation.find(".recent-search-dropdown");
        this.booker.findVacation.adultsCount = this.booker.findVacation.find(".adult-dropdown select");
        this.booker.findVacation.kidsCount = this.booker.findVacation.find(".kid-vacation-dropdown select");
        this.booker.findVacation.infantsCount = this.booker.findVacation.find(".infant-dropdown select");
        this.booker.findVacation.hotelRoomsCount = this.booker.findVacation.find(".hotel-dropdown select");
        this.booker.findVacation.fareTypes = this.booker.findVacation.find("input[name=vac_fare_display]");
        this.booker.findVacation.togglers = {
            from: this.booker.findVacation.find(".from-btn"),
            to: this.booker.findVacation.find(".to-btn"),
            departDate: this.booker.findVacation.find(".depart-wrap"),
            returnDate: this.booker.findVacation.find(".return-wrap")
        };
        // Checkin Form
        this.booker.checkin = this.booker.find("form.checkin-form");
        // Flight Status Form
        this.booker.flightStatus = this.booker.find("form.fstatus-form");
        this.booker.flightStatus.departureDate = this.booker.flightStatus.find(".departure-dropdown select");

        this.booker.flightStatus.view = {
            findByCity: this.booker.flightStatus.find(".byCityDiv"),
            findByFlightNumber: this.booker.flightStatus.find(".byFlightNumberDiv")
        };
        this.booker.flightStatus.togglers = {
            findByCity: this.booker.flightStatus.find(".bycities-btn").parent(),
            findByFlightNumber: this.booker.flightStatus.find(".byflightnum-btn").parent()
        };

        //Flight Form Toggle
        this.booker.flightStatus.togglers.findByCity.click(function (e) {
            e.preventDefault();
            if ($(this).hasClass("active")) return;
            $ffstatusForm = document.getElementById("ffstatus");
            self.booker.flightStatus.togglers.findByFlightNumber.removeClass("active");
            $(this).addClass("active");
            self.booker.flightStatus.view.findByFlightNumber.hide();
            self.booker.flightStatus.view.findByCity.show();
            $ffstatusForm['FlightStatusType'].value = "byCities";
        });
        this.booker.flightStatus.togglers.findByFlightNumber.click(function (e) {
            e.preventDefault();
            if ($(this).hasClass("active")) return;
            $ffstatusForm = document.getElementById("ffstatus");
            self.booker.flightStatus.togglers.findByCity.removeClass("active");
            $(this).addClass("active");
            self.booker.flightStatus.view.findByCity.hide();
            self.booker.flightStatus.view.findByFlightNumber.show();
            $ffstatusForm['FlightStatusType'].value = "byFlightNumber";
        });

        // Error Panel
        this.booker.error = this.booker.find("div.error-modal");
        this.booker.error.messages = this.booker.error.find("div.error-message");
        this.booker.error.buttons = {
            cancel: this.booker.error.find(".cancel-btn"),
            ok: this.booker.error.find(".ok-btn")
        };
        this.booker.error.buttons.ok.bind('click', function (target) {

            if ($('.error-message').find('#unacompMinorError').length != 0) {
                //Since this message is after the form submits 
                //if we click ok from that point just exit out of this function
                //form is continued from that point.
                return;
            }
            // @mx 3.0 req. #285 - start
            if ($('.error-message').find('#seasonalError').length != 0 || $('.error-message').find('#dayOfWeekError').length != 0) {
                var previousStateWasFlights = self.booker.stateHistory.previous === "fflight",
                    checkSubmitted = previousStateWasFlights ? self.booker.findFlight : self.booker.findVacation,
                    formToSubmit = previousStateWasFlights ? self.booker.submitFF : self.booker.submitFV,
                    $formToSubmit = previousStateWasFlights ? $fflightForm : $fvacationForm
                ;
                if (checkSubmitted.submitted) return false;
                else checkSubmitted.submitted = true;
                formToSubmit.submit($formToSubmit);
            }
            // @mx 3.0 req. #285 - end
            if ($('.error-message').find('#flexibleInterlineDateError').length != 0 && $('#flexible').is(':checked')) {
                if (self.booker.findFlight.submitted) return false;
                else self.booker.findFlight.submitted = true;
                $('#flexible').attr('checked', false);
                self.booker.submitFF.submit($fflightForm);
            }

            if ($('.error-message').find('#returnDateisMonthPastDeptError').length != 0) {

                if (self.booker.findFlight.submitted) return false;
                else self.booker.findFlight.submitted = true;
                self.booker.submitFF.submit($fflightForm);
            }

            //this handles the vacation form - save w getaways booker
            if ($('.error-message').find('#vac_ReturnDateisMonthPastDeptError').length != 0) {

                if (self.booker.findVacation.submitted) return false;
                else self.booker.findVacation.submitted = true;
                self.booker.submitFV.submit($fvacationForm);
            }

            if ($('.error-message').find('#a321EntertainmentError').length != 0) {

                if (self.booker.findFlight.submitted) return false;
                else self.booker.findFlight.submitted = true;
                self.booker.submitFF.submit($fflightForm);
            }

            //this handles the vacation form - save w getaways booker
            if ($('.error-message').find('#vac_a321EntertainmentError').length != 0) {

                if (self.booker.findVacation.submitted) return false;
                else self.booker.findVacation.submitted = true;
                self.booker.submitFV.submit($fvacationForm);
            }

        });
        this.booker.selects.beautifier(); // Beautify selects
        this.booker.radio.beautifier(); // Beautify radio buttons

        if (!this.booker.stateHistory.current) this.changeViewTo(this.booker.views.findFlight); // Show default view.

        // Togglers Event handlers
        // Checkin Toggle
        this.booker.togglers.checkin.click(function (e) {
            e.preventDefault();
			self.changeViewTo(self.booker.views.checkin);
			/*
            var flashEnabled = swfobject.getFlashPlayerVersion().major,
					supportedVersion = 10;

            if (flashEnabled >= supportedVersion) {
                self.changeViewTo(self.booker.views.checkin);
            } else {
                self.booker.error.messages.html(""); // Clear Messages
                self.booker.error.messages.append("<p>It appears you do not have the latest version of Flash installed. To install or update Flash, please <a href=\"http://get.adobe.com/flashplayer/\">click here</a>.</p><p>If you cannot install Flash and would still like to check in, you may do so through mobile check-in, but you will still need to print your boarding pass at the airport. Continue to <a href=\"https://mobile.jetblue.com/mt/checkin.jetblue.com/mobile/B6/Locate.jsp\">mobile check-in</a>.</p>");
                self.changeViewTo(self.booker.views.error);
            }
			*/
        });

        //Flexible date check box 
        $(".bookerFlexibleCheckbox").ready(function () {
            var target = this;
            if (!$('#flexible').is(':checked')) {
                $('.bookerFlexibleCheckbox').removeClass('bookerFlexible_active');
            } else {
                $('.bookerFlexibleCheckbox').addClass('bookerFlexible_active');
            }
        });
        $(".bookerFlexibleCheckbox").click(function () {
            var target = this;
            if ($(target).hasClass('bookerFlexible_active')) {
                $('#flexible').attr('checked', false);
                $('.bookerFlexibleCheckbox').removeClass('bookerFlexible_active');
            } else {
                $('#flexible').attr('checked', true);
                $('.bookerFlexibleCheckbox').addClass('bookerFlexible_active');
            }
        });

        // Checkin Toggle
        this.booker.togglers.flightStatus.click(function (e) {
            e.preventDefault();
            self.changeViewTo(self.booker.views.flightStatus);
        });
        // Close Toggle
        this.booker.togglers.close.click(function (e) {
            e.preventDefault();
            var state = self.booker.stateHistory.previous;
            if (self.booker.views.isError) {
                var previous = state;
                state = self.booker.stateHistory.current;
                self.changeViewTo(state, function () {
                    self.booker.views.isError = false;
                    self.booker.stateHistory.previous = previous;
                });
                return;
            }
            self.changeViewTo(state);
        });
        // Find Types Toggle
        // Find Flight
        this.booker.bookFlights.togglers.findFlight.click(function (e) {
            e.preventDefault();
            if ($(this).hasClass("active")) return;
            self.booker.removeClass(self.booker.bookFlights.type.findVacation);
            self.booker.bookFlights.togglers.findVacation.removeClass("active");
            $(this).addClass("active");
            self.booker.views.isFindVacation = false;
            // @mx3.0.?
            isFlightsHotelSavings = false;
            self.booker.stateHistory.current = self.booker.bookFlights.type.findFlight;

            // clear out the state of the calendar placeholders
            // @setter $(".selecter").datepicker('setDate', null);
            self.booker.datePicker.calendar.departCal.datepicker('setDate', null);
            self.booker.datePicker.calendar.returnCal.datepicker('setDate', null);

            // the following sets the Alt Field for the datepicker
            // @setter $( ".selector" ).datepicker( "option", "altField", '#actualDate' );
            self.booker.datePicker.calendar.departCal.datepicker('option', 'altField', '#depart_field');
            self.booker.datePicker.calendar.returnCal.datepicker('option', 'altField', '#return_field');

            // reset the placeholder text within the depart and return wrappers
            self.booker.datePicker.departText.text('Depart date');
            self.booker.datePicker.returnText.text('Return date');

            // reset the datepickers & refresh
            // @example $('.selector').datepicker('refresh');
            self.booker.datePicker.departDate = undefined;
            self.booker.datePicker.returnDate = undefined;
            self.booker.datePicker.durationDates = [];
            self.booker.datePicker.calendar.departCal.datepicker('refresh');
            self.booker.datePicker.calendar.returnCal.datepicker('refresh');

            self.booker.datePicker.departClickedAlready = false;
        });
        // Find Vacation
        this.booker.bookFlights.togglers.findVacation.click(function (e) {
            e.preventDefault();
            if ($(this).hasClass("active")) return;
            self.booker.addClass(self.booker.bookFlights.type.findVacation);
            self.booker.bookFlights.togglers.findFlight.removeClass("active");
            $(this).addClass("active");
            self.booker.views.isFindVacation = true;
            // @mx3.0.?
            isFlightsHotelSavings = true;
            self.booker.stateHistory.current += (" " + self.booker.bookFlights.type.findVacation);
            self.booker.findFlight.flightTypes.filter("[value=round_trip]").attr("checked", "checked").trigger("change");

            // clear out the state of the calendar placeholders
            // @setter $(".selecter").datepicker('setDate', null);
            self.booker.datePicker.calendar.departCal.datepicker('setDate', null);
            self.booker.datePicker.calendar.returnCal.datepicker('setDate', null);

            // the following sets the Alt Field for the datepicker
            // @setter $( ".selector" ).datepicker( "option", "altField", '#actualDate' );			// if (window.console) { console.log("self.booker.datePicker: ", self.booker.datePicker); }
            self.booker.datePicker.calendar.departCal.datepicker('option', 'altField', '#vac_departure_field');
            self.booker.datePicker.calendar.returnCal.datepicker('option', 'altField', '#vac_return_field');

            // reset the placeholder text within the depart and return wrappers
            self.booker.datePicker.departText.text('Depart date');
            self.booker.datePicker.returnText.text('Return date');

            // reset the datepickers & refresh
            // @example $('.selector').datepicker('refresh');
            self.booker.datePicker.departDate = undefined;
            self.booker.datePicker.returnDate = undefined;
            self.booker.datePicker.durationDates = [];
            self.booker.datePicker.calendar.departCal.datepicker('refresh');
            self.booker.datePicker.calendar.returnCal.datepicker('refresh');

            self.booker.datePicker.departClickedAlready = false;
        });
        // Find Flight Recent Searches Dropdown Click handler
        this.booker.findFlight.recentSearches.beautifier();
        this.booker.findVacation.recentSearches.beautifier();

        // Booker Datepicker Instance
        this.booker.datePicker = new JB.Class.Calendar(this.container, {
            today: self.options.today,
            end: self.options.endDate
        });
        // handle certain state for "Back" key
        $fflightForm = document.getElementById("fflight");
        if (document.getElementById("one-flight").checked) {
            self.booker.datePicker.returnWrap.addClass("disabled");
            // disable the returnDate datepicker and reset it
            self.booker.datePicker.calendar.helper.hide();
            self.booker.datePicker.calendar.returnCal.datepicker('disable');
            self.booker.datePicker.calendar.returnCal.datepicker('setDate', null)
            self.booker.datePicker.returnDate = undefined;
            $fflightForm['journeySpan'].value = "OW"
        } else {
            $fflightForm['journeySpan'].value = "RT"
            self.booker.datePicker.returnWrap.removeClass("disabled");
            // enable the returnDate datepicker
            self.booker.datePicker.calendar.helper.show();
            self.booker.datePicker.calendar.returnCal.datepicker('enable');
            self.booker.datePicker.calendar.returnCal.datepicker('refresh');
            self.booker.datePicker.returnText.text(self.returnDateString);
        }
        if (document.getElementById("lowest_fare").checked) {
            $fflightForm['fareFamily'].value = "LOWESTFARE";
            $fflightForm['fareDisplay'].value = "lowest";
        }
        //        if (document.getElementById("refundable_fare").checked) {
        //            $fflightForm['fareFamily'].value = "REFUNDABLE";
        //            $fflightForm['fareDisplay'].value = "refundable";
        //        }
        if (document.getElementById("points_fare").checked) {
            $fflightForm['fareFamily'].value = "TRUEBLUE";
            $fflightForm['fareDisplay'].value = "points";
        }

        // Find Flight OneWay Radio Button Toggler Event
        this.booker.findFlight.flightTypes.change(function (e) {
            $fflightForm = document.getElementById("fflight");
            if (this.value === "one_way") {
                self.booker.datePicker.returnWrap.addClass("disabled");
                // disable the returnDate datepicker and reset it
                self.booker.datePicker.calendar.helper.hide();
                self.booker.datePicker.calendar.returnCal.datepicker('disable');
                self.booker.datePicker.calendar.returnCal.datepicker('setDate', null)
                self.booker.datePicker.returnDate = undefined;
                self.booker.datePicker.durationDates = [];
                self.booker.datePicker.returnText.text('Return date');
                self.booker.datePicker.calendar.departCal.datepicker('refresh');
                $fflightForm['journeySpan'].value = "OW"
            }
            else {
                $fflightForm['journeySpan'].value = "RT"
                self.booker.datePicker.returnWrap.removeClass("disabled");
                // enable the returnDate datepicker
                self.booker.datePicker.calendar.helper.show();
                self.booker.datePicker.calendar.returnCal.datepicker('enable');
                self.booker.datePicker.calendar.returnCal.datepicker('refresh');
                self.booker.datePicker.returnText.text(self.returnDateString);
            }
        });
        // Find Fare type Radio Button Toggler Event
        this.booker.findFlight.fareTypes.change(function (e) {
            $fflightForm = document.getElementById("fflight");
            if (this.value === "lowest") {
                $fflightForm['fareFamily'].value = "LOWESTFARE";
                $fflightForm['fareDisplay'].value = "lowest";
            }
            //            if (this.value === "refundable") {
            //                $fflightForm['fareFamily'].value = "REFUNDABLE";
            //                $fflightForm['fareDisplay'].value = "refundable";
            //            }
            if (this.value === "points") {
                $fflightForm['fareFamily'].value = "TRUEBLUE";
                $fflightForm['fareDisplay'].value = "points";
            }
        });

        // Find Vacation Fare type Radio Button Toggler Event
        this.booker.findVacation.fareTypes.change(function (e) {
            $vacationForm = document.getElementById("fvacation");
            if (this.value === "default") {
                $vacationForm['nav'].value = "default";
            }
            if (this.value === "redeem") {
                $vacationForm['nav'].value = "Redeem";
            }
        });

        // Find Vacation Fare type Radio Button Toggler Event
        this.booker.findVacation.fareTypes.change(function (e) {
            $vacationForm = document.getElementById("fvacation");
            if (this.value === "default") {
                $vacationForm['nav'].value = "default";
            }
            if (this.value === "Redeem") {
                $vacationForm['nav'].value = "Redeem";
            }
        });

        // Adults Count Select Dropdown Handle
        this.booker.findFlight.adultsCount.add(this.booker.findVacation.adultsCount).change(function (e) {
            var $labels = $(this).parent().find(".dd-text");
            var label = $labels.eq(0).text();
            if (this.value == 1) $labels.text(label.replace(/s$/, ""));
            else if (((this.value > 1) || (this.value == 0)) && !RegExp(/s$/).test(label)) $labels.text(label + "s");
        });
        // Kids Count Select Dropdown Handle
        this.booker.findFlight.kidsCount.change(function (e) {
            var $labels = $(this).parent().find(".dd-text");
            var label = $labels.eq(0).text();
            if (this.value == 1) $labels.text(label.replace(/s$/, ""));
            else if (((this.value > 1) || (this.value == 0)) && !RegExp(/s$/).test(label)) $labels.text(label + "s");
        });
        // Infants Count Select Dropdown Handle
        this.booker.findFlight.infantsCount.add(this.booker.findVacation.infantsCount).change(function (e) {
            var $labels = $(this).parent().find(".dd-text");
            var label = $labels.eq(0).text();
            if (this.value == 1) $labels.text(label.replace(/s$/, ""));
            else if (((this.value > 1) || (this.value == 0)) && !RegExp(/s$/).test(label)) $labels.text(label + "s");
        });
        // Kids Count Vacation Select Dropdown Handle
        this.booker.findVacation.kidsCount.change(function (e) {
            var $labels = $(this).parent().find(".dd-text");
            var label = $labels.eq(0).text();
            if (this.value == 1) $labels.text(label.replace(/s$/, ""));
            else if (((this.value > 1) || (this.value == 0)) && !RegExp(/s$/).test(label)) $labels.text(label + "s");

            if (this.value > 0) $(this).parent().addClass("disabled age kid-" + this.value);
            else $(this).parent().removeClass("disabled age");
        });
        this.booker.findVacation.kidsCount.parent().click(function (e) {
            if (e.target.nodeName.toLowerCase() === "span") $(this).removeClass("disabled age kid-1 kid-2 kid-3").addClass("active");
        });
        // Hotel Rooms Count Select Dropdown Handle
        this.booker.findVacation.hotelRoomsCount.change(function (e) {
            var $labels = $(this).parent().find(".dd-text");
            var label = $labels.eq(0).text();
            if (this.value == 1) $labels.text(label.replace(/s$/, ""));
            else if (((this.value > 1) || (this.value == 0)) && !RegExp(/s$/).test(label)) $labels.text(label + "s");
        });

        // Error Panel Button Event Handlers
        this.booker.error.buttons.cancel.click(function (e) {
            e.preventDefault();
            self.booker.findFlight.submitted = false;
            self.booker.findVacation.submitted = false;
            var state = self.booker.stateHistory.previous;
            if (self.booker.views.isError) {
                var previous = state;
                state = self.booker.stateHistory.current;
                self.changeViewTo(state, function () {
                    self.booker.views.isError = false;
                    self.booker.stateHistory.previous = previous;
                });
                return;
            }
            if (self.booker.findFlight.isWarned) self.booker.findFlight.isWarned = false;
            if (self.booker.findVacation.isWarned) self.booker.findVacation.isWarned = false;
            self.changeViewTo(state);
        });
        this.booker.error.buttons.ok.click(function (e) {
            if (customErrorHandlerUsed) {
                customErrorHandlerUsed = false;
                return;
            }
            e.preventDefault();
            var state = self.booker.stateHistory.previous;
            if (self.booker.views.isError) {
                var previous = state;
                state = self.booker.stateHistory.current;
                self.changeViewTo(state, function () {
                    self.booker.views.isError = false;
                    self.booker.stateHistory.previous = previous;
                });
                return;
            }
            self.changeViewTo(state);
        });

        // Get Airport List Script
        new JB.Model.getAirports($.proxy(function (airports) {
            var airportsUnique = self.filterAirports(airports, ""); // Duplicate airports filtered
            var flightStatusAirports = airportsUnique.slice(0);

            var flightStatusAirports2 = [];
            $.each(flightStatusAirports, function (key, value) {
                if (value != undefined) {
                    if ($.inArray(value.code, MAClist) != -1 || !value.jb) {
                        //flightStatusAirports.splice(key, 1); 
                    }
                    else
                        flightStatusAirports2.push(flightStatusAirports[key]);
                }
            });

            flightStatusAirports = flightStatusAirports2;


            // Recent Searche History Toggle
            this.booker.findFlight.recentSearches.history = new JB.Class.RecentSearch(this.booker.findFlight.recentSearches.find(".recent-results-list"), this.booker.findFlight, {});

            // Booker Modals
            this.booker.modals = {
                faqs: new JB.Class.Modal({
                    openTogglers: ".faq",
                    url: "http://www.jetblue.com/partials/modal.faq.aspx"
                }),
                citiesList: new JB.Class.Modal({
                    openTogglers: this.booker.findFlight.add(this.booker.checkin).add(this.booker.flightStatus).find(".from-btn, .to-btn, .viewCity"),
                    url: "http://www.jetblue.com/partials/modal.city-list.html",
                    onLoad: function (e, instance) {
                        // Modal Instance
                        var self = this;
                        var $modal = instance.modal;
                        // Tabulize
                        this.regions = $modal.find(".region-list li");
                        this.tabs = $modal.find("div.cities").hide();
                        this.tabs.active = 0;
                        this.tabs.eq(this.tabs.active).show();
                        // Clear old city list
                        $('div.city-list > ul').each(function (index, elem) {
                            $(elem).remove();
                        });

                        var jbCities = [];
                        var originalCities = airports;
                        if ($(e.currentTarget).parents("form")[0].id != "fflight") {
                            for (var i = 0; i < airports.length; i++) {
                                if (airports[i].jb) {
                                    jbCities.push(airports[i]);
                                }
                            }
                            airports = jbCities;
                            itemPerColumn = 25;
                        }
                        if ($(e.currentTarget).parents("form")[0].id == "ffstatus")
                            jQuery('.fs-info').css('display', 'block');
                        else
                            jQuery('.fs-info').css('display', 'none');
                        // Generate HTML List Template
                        var templates = {};
                        var usCount = 0;

                        // IE compat mode will show blank US without the below code
                        for (var i = 0; i < airports.length; i++) {
                            // Clueless? Yea, I know. You got lost here.
                            var partner = (airports[i].jb) ? "" : " class='partner'";
                            var style = $.inArray(airports[i].code, MAClist) == -1 ? "" : " class='MAC'";
                            var routes = airports[i].routes.toString().replace(/\,/gi, " ").replace(/0/gi, "");
                            if (!!!templates[airports[i].cc]) templates[airports[i].cc] = { html: [], count: 0, sets: 0, region: airports[i].region.code };
                            if (!!!templates[airports[i].cc].html[0] && airports[i].cc !== "US") templates[airports[i].cc].html[0] = "<ul><li class='list-head " + airports[i].country + "'>" + airports[i].country + "</li>"; // global space value
                            if (!!!templates[airports[i].cc].html[templates[airports[i].cc].sets]) templates[airports[i].cc].html[templates[airports[i].cc].sets] = "<ul>";
                            templates[airports[i].cc].html[templates[airports[i].cc].sets] += "<li" + partner + " code='" + airports[i].code + "' routes='" + routes + "'><a" + style + " href='#' rel='" + airports[i].code + "' title=\"" + airports[i].label + "\" index='" + i + "'>" + airports[i].label + "</a></li>";
                            templates[airports[i].cc].count++;
                            if (!!airports[i + 1] && (airports[i].cc !== airports[i + 1].cc)) templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
                            if (i == (airports.length - 1)) {
                                templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
                                break;
                            }
                            if ((templates[airports[i].cc].count % itemPerColumn) == 0) {
                                templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
                                templates[airports[i].cc].sets++;
                            }
                        }

                        airports = originalCities;
                        // IE compat mode will show blank US without the below code
                        for (country in templates) {
                            if (!!!templates[templates[country].region]) templates[templates[country].region] = {};
                            if (!!!templates[templates[country].region].count) templates[templates[country].region].count = 0;
                            switch (templates[country].region) {
                                case "http://www.jetblue.com/js/src/INTERLINE.US":
                                    var $tab = this.tabs.filter("div[role='http://www.jetblue.com/js/src/INTERLINE.US']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        $tab.columns.eq(i).append(templates[country].html[i]);
                                    }
                                    usCount++;
                                    break;
                            }
                        }
                        // Tab Through Event Hanlder
                        this.regions.click(function (e) {
                            self.regions.eq(self.tabs.active).removeClass("active");
                            self.tabs.eq(self.tabs.active).hide();
                            self.tabs.active = self.regions.index(this);
                            $(this).addClass("active");
                            self.tabs.eq(self.tabs.active).show();
                        });
                    },
                    onOpen: function (e, instance) {
                        //console.log ("onOpen 1 to: from: flights tab");
                        // flights tab?
                        var $target = $(e.currentTarget).parents(".from-wrap, .to-wrap, .depart-city-wrap");
                        var $targetInput = $target.find("input");
                        var $modal = instance.modal;
                        var $fromRelationInput = $target.parents("form").find(".from-wrap input");
                        var $toRelationInput = $target.parents("form").find(".to-wrap input");

                        // Clear old city list
                        $('div.city-list > ul').each(function (index, elem) {
                            $(elem).remove();
                        });

                        var jbCities = [];
                        var originalCities = airports;
                        if ($(e.currentTarget).parents("form")[0].id != "fflight") {
                            for (var i = 0; i < airports.length; i++) {
                                if (airports[i].jb) {
                                    jbCities.push(airports[i]);
                                }
                            }
                            airports = jbCities;
                            itemPerColumn = 25;
                        }
                        if ($(e.currentTarget).parents("form")[0].id == "ffstatus")
                            jQuery('.fs-info').css('display', 'block');
                        else
                            jQuery('.fs-info').css('display', 'none');
                        // Generate HTML List Template
                        var templates = {};

                        var usCount = 0, afCount = 0, meCount = 0, paCount = 0, caCount = 0, asCount = 0, saCount = 0, euCount = 0;

                        for (var i = 0; i < airports.length; i++) {
                            // Clueless? Yea, I know. You got lost here.
                            var partner = (airports[i].jb) ? "" : " class='partner'";
                            var style = $.inArray(airports[i].code, MAClist) == -1 ? "" : " class='MAC'";
                            var routes = airports[i].routes.toString().replace(/\,/gi, " ").replace(/0/gi, "");
                            if (!!!templates[airports[i].cc]) templates[airports[i].cc] = { html: [], count: 0, sets: 0, region: airports[i].region.code };
                            if (!!!templates[airports[i].cc].html[0] && airports[i].cc !== "US") templates[airports[i].cc].html[0] = "<ul><li class='list-head " + airports[i].country + "'>" + airports[i].country + "</li>"; // global space value
                            if (!!!templates[airports[i].cc].html[templates[airports[i].cc].sets]) templates[airports[i].cc].html[templates[airports[i].cc].sets] = "<ul>";
                            templates[airports[i].cc].html[templates[airports[i].cc].sets] += "<li" + partner + " code='" + airports[i].code + "' routes='" + routes + "'><a" + style + " href='#' rel='" + airports[i].code + "' title=\"" + airports[i].label + "\" index='" + i + "'>" + airports[i].label + "</a></li>";
                            templates[airports[i].cc].count++;
                            if (!!airports[i + 1] && (airports[i].cc !== airports[i + 1].cc)) templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
                            if (i == (airports.length - 1)) {
                                templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
                                break;
                            }
                            if ((templates[airports[i].cc].count % itemPerColumn) == 0) {
                                templates[airports[i].cc].html[templates[airports[i].cc].sets] += "</ul>";
                                templates[airports[i].cc].sets++;
                            }
                        }

                        airports = originalCities;
                        // Populate Modal DOM
                        for (country in templates) {
                            if (!!!templates[templates[country].region]) templates[templates[country].region] = {};
                            if (!!!templates[templates[country].region].count) templates[templates[country].region].count = 0;
                            switch (templates[country].region) {
                                case "http://www.jetblue.com/js/src/INTERLINE.US":
                                    var $tab = this.tabs.filter("div[role='http://www.jetblue.com/js/src/INTERLINE.US']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        $tab.columns.eq(i).append(templates[country].html[i]);
                                    }
                                    usCount++;
                                    break;
                                case "INTERLINE.AFRICA":
                                    var $tab = this.tabs.filter("div[role='INTERLINE.AFRICA']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        templates[templates[country].region].count += templates[country].count + 2;
                                        var column = 0;
                                        // @mx 3.0 req. #285 - comment 2 lines below
                                        //if (templates[templates[country].region].count > 20) column = 1;
                                        //if (templates[templates[country].region].count > 40) column = 2;
                                        $tab.columns.eq(column).append(templates[country].html[i]);
                                    }
                                    afCount++;
                                    break;
                                case "INTERLINE.MIDDLEEAST":
                                    var $tab = this.tabs.filter("div[role='INTERLINE.MIDDLEEAST']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        templates[templates[country].region].count += templates[country].count + 2;
                                        var column = 0;
                                        // @mx 3.0 req. #285 - comment 2 lines below
                                        //if (templates[templates[country].region].count > 20) column = 1;
                                        //if (templates[templates[country].region].count > 40) column = 2;
                                        $tab.columns.eq(column).append(templates[country].html[i]);
                                    }
                                    meCount++;
                                    break;
                                case "INTERLINE.PACIFIC":
                                    var $tab = this.tabs.filter("div[role='INTERLINE.PACIFIC']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        templates[templates[country].region].count += templates[country].count + 2;
                                        var column = 0;
                                        // @mx 3.0 req. #285 - comment 2 lines below
                                        //if (templates[templates[country].region].count > 20) column = 1;
                                        //if (templates[templates[country].region].count > 40) column = 2;
                                        $tab.columns.eq(column).append(templates[country].html[i]);
                                    }
                                    paCount++;
                                    break;
                                case "INTERLINE.CARRIBEAN_C-AMERICA":
                                    var $tab = this.tabs.filter("div[role='INTERLINE.CARRIBEAN_C-AMERICA']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        templates[templates[country].region].count += templates[country].count + 2;
                                        var column = 0;
                                        // @mx 3.0 req. #285 - comment 2 lines below
                                        //if (templates[templates[country].region].count > 22) column = 1;
                                        //if (templates[templates[country].region].count > 44) column = 2;
                                        $tab.columns.eq(column).append(templates[country].html[i]);

                                    }
                                    caCount++;
                                    break;
                                case "http://www.jetblue.com/js/src/INTERLINE.ASIA":
                                    var $tab = this.tabs.filter("div[role='http://www.jetblue.com/js/src/INTERLINE.ASIA']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        templates[templates[country].region].count += templates[country].count + 2;
                                        var column = 0;
                                        // @mx 3.0 req. #285 - comment 2 lines below
                                        //if (templates[templates[country].region].count > 20) column = 1;
                                        //if (templates[templates[country].region].count > 40) column = 2;
                                        $tab.columns.eq(column).append(templates[country].html[i]);
                                    }
                                    asCount++;
                                    break;
                                case "INTERLINE.S-AMERICA":
                                    var $tab = this.tabs.filter("div[role='INTERLINE.S-AMERICA']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        templates[templates[country].region].count += templates[country].count + 2;
                                        var column = 0;
                                        // @mx 3.0 req. #285 - comment 2 lines below
                                        //if (templates[templates[country].region].count > 20) column = 1;
                                        //if (templates[templates[country].region].count > 40) column = 2;
                                        $tab.columns.eq(column).append(templates[country].html[i]);
                                    }
                                    saCount++;
                                    break;
                                case "INTERLINE.EUROPE":
                                    var $tab = this.tabs.filter("div[role='INTERLINE.EUROPE']");
                                    $tab.columns = $tab.find(".city-list");
                                    for (var i = 0; i < templates[country].html.length; i++) {
                                        templates[templates[country].region].count += templates[country].count;
                                        // @mx 3.0 req. #285 - comment 2 lines below
                                        //var column = Math.floor(templates[templates[country].region].count / 10);
                                        //if (column > 2) column = 2;
                                        $tab.columns.eq(0).append(templates[country].html[i]);
                                    }
                                    euCount++;
                                    break;
                            }
                        }
                           
                      
                        var allInterlines = [
                            "INTERLINE.AFRICA",
                            "INTERLINE.MIDDLEEAST",
                            "INTERLINE.PACIFIC",
                            "INTERLINE.CARRIBEAN_C-AMERICA",
                            "http://www.jetblue.com/js/src/INTERLINE.ASIA",
                            "INTERLINE.S-AMERICA",
                            "INTERLINE.EUROPE"
                        ];
                        var fixedWidth = $('.cities').width() / 3;
                        for (var i = 0; i < allInterlines.length; i++) {
                          //  console.info("** flights tab1 ************ " + allInterlines[i]);
                            var $tab = this.tabs.filter("div[role='" + allInterlines[i] + "']"),
                                $cols = $tab.find('.city-list'),
                                tabWasVisible = $tab.is(":visible"),
                                currentColIndex = 0,
                                $currentCol = $cols.eq(currentColIndex),
                                currentColHeight = 0
                            ;
                            $tab.show().width(fixedWidth);
                            var heightThreshold = $currentCol.outerHeight(true) / 3;
                            $cols.find("ul").each(function () {
                                var $this = $(this),
                                    thisHeight = $this.outerHeight(true)
                                ;
                                if (currentColIndex > 0) {
                                    $this.appendTo($currentCol);
                                }
                                if (currentColIndex < 2 && currentColHeight + thisHeight >= heightThreshold) {
                                    // console.info($this.find('li:first').text() + "\ncurrentColHeight: " + currentColHeight + "\nthisHeight: " + thisHeight + "\nheightTreshold: " + heightThreshold);
                                    heightThreshold = currentColHeight + thisHeight;
                                    currentColHeight = 0;
                                    currentColIndex++;
                                    $currentCol = $cols.eq(currentColIndex);
                                }
                                currentColHeight += thisHeight;
                            });
                            $tab.attr('style', '');
                            if (!tabWasVisible) {
                                $tab.hide();
                            }
                        } 
                        
                        this.regionsTitle = $modal.find(".region-list li");
                        var $usTitle = this.regionsTitle.filter("li[role='http://www.jetblue.com/js/src/INTERLINE.US']");
                        var $afTitle = this.regionsTitle.filter("li[role='INTERLINE.AFRICA']");
                        var $meTitle = this.regionsTitle.filter("li[role='INTERLINE.MIDDLEEAST']");
                        var $paTitle = this.regionsTitle.filter("li[role='INTERLINE.PACIFIC']");
                        var $caTitle = this.regionsTitle.filter("li[role='INTERLINE.CARRIBEAN_C-AMERICA']");
                        var $asTitle = this.regionsTitle.filter("li[role='http://www.jetblue.com/js/src/INTERLINE.ASIA']");
                        var $saTitle = this.regionsTitle.filter("li[role='INTERLINE.S-AMERICA']");
                        var $euTitle = this.regionsTitle.filter("li[role='INTERLINE.EUROPE']");

                        usCount == 0 ? $usTitle.addClass('hideTitle') : $usTitle.removeClass('hideTitle');
                        afCount == 0 ? $afTitle.addClass('hideTitle') : $afTitle.removeClass('hideTitle');
                        meCount == 0 ? $meTitle.addClass('hideTitle') : $meTitle.removeClass('hideTitle');
                        paCount == 0 ? $paTitle.addClass('hideTitle') : $paTitle.removeClass('hideTitle');
                        caCount == 0 ? $caTitle.addClass('hideTitle') : $caTitle.removeClass('hideTitle');
                        asCount == 0 ? $asTitle.addClass('hideTitle') : $asTitle.removeClass('hideTitle');
                        saCount == 0 ? $saTitle.addClass('hideTitle') : $saTitle.removeClass('hideTitle');
                        euCount == 0 ? $euTitle.addClass('hideTitle') : $euTitle.removeClass('hideTitle');

                        $(e.currentTarget).parents("form")[0].id != "fflight" ? $('.city-desc').css('display', 'none') : $('.city-desc').css('display', 'block');
                        // Create a RegExp that checks for three letters proceeded by a close paren and end of string
                        var match = /\w{3}(?=\)$)/;
                        // Execute a match on the targetInput's value
                        var code = match.exec($targetInput.val());
                        // Store whether the input field is active (i.e. 'Contains text that matches the RegExp')
                        var active = false;
                        // Set active to true if there is a match returned
                       // if (window.console) { console.log("code: ", code); }
                        if (code) { $targetInput.data("selected", code); }
                       // if (window.console) { console.log("$targetInput.data('selected'): ", $targetInput.data('selected')); }
                        self.booker.modals.currentTargetInput = $targetInput;
                        $modal.regions = $modal.find("ul.region-list li").show();
                        $modal.lists = $modal.find(".city-list");
                        $modal.lists.find("ul").show();
                        $modal.lists.find("li:not(.list-head)").show();
                        $modal.lists.find('a.active').removeClass("active");
                        var index = this.tabs.index($modal.lists.find("li a[rel='" + $targetInput.data("selected") + "']").addClass("active").parents("div.cities"));
                       // if (window.console) { console.log("index: ", index); }
                        if (index !== -1) this.regions.eq(index).trigger("click");
                        else this.regions.eq(0).trigger("click");
                        $modal.lists.click(function (e) {
                            e.preventDefault();
                            if (e.target.nodeName.toLowerCase() !== "a") return false;
                            $modal.lists.find('a.active').removeClass("active");
                            $(e.target).addClass("active");
                            $targetInput.val(e.target.title);
                            $targetInput.data("selected", e.target.rel);
                            if ($targetInput[0] === $fromRelationInput[0]) {
                                $toRelationInput.autocomplete("option", "source", self.filterAirports(airports, e.target.rel));
                                $toRelationInput.removeData("selected").val("");
                            }
                            instance.close(e);
                        });

                        //Flight Status hide MAC cities
                        if ($('#booker').hasClass('fstatus')) {
                            // Clear MAC city for flight status
                            $('div.city-list > ul > li').each(function (index, elem) {
                                if ($(elem).find('a').hasClass('MAC')) {
                                    $(elem).remove();
                                }
                            });
                        }

                        if (($targetInput[0] === $toRelationInput[0]) && ($fromRelationInput.val() === "")) $fromRelationInput.removeData("selected");
                        if ($targetInput[0] === $fromRelationInput[0]) return;
                        // Filter Availability View
                        var fromFieldValue = jQuery('#from_field').attr('value');

                        var fromCode;

                        //add airport code for MAC cities that don't show airport code. 
                        if (!match.exec(fromFieldValue)) {
                            for (var j = 0; j < MAClist.length; j++) {

                                var macCity = window["o" + MAClist[j]];

                                if (macCity.name == fromFieldValue) {
                                    fromCode = macCity.code;
                                    break;
                                }
                            }
                        } else { fromCode = (fromFieldValue.length > 3 ? fromFieldValue.substring(fromFieldValue.length - 4, fromFieldValue.length - 1) : fromFieldValue.toUpperCase()); }

                        if ($(e.currentTarget).parents("form")[0].id != "fflight") fromCode = $fromRelationInput.data("selected");
                        if (!!fromCode) {
                            $modal.lists.find("ul").hide();
                            $modal.lists.find("li:not(.list-head)").hide();
                            $modal.lists.availables = $modal.lists.find("li[routes*=" + fromCode + "]").show();
                            // Filter List Heads
                            $modal.lists.availables.parent("ul").show();
                            // Tabs Filter
                            $modal.regions.hide();
                            $modal.lists.availables.parents("div.cities").each(function (i, el) {
                                $modal.regions.filter("li[role='" + $(el).attr("role") + "']").show();
                            });
                            $modal.regions.filter(":visible").eq(0).trigger("click");
                            // Toggle Tab
                            var index = this.tabs.index($modal.lists.find("li a[rel='" + $targetInput.data("selected") + "']").addClass("active").parents("div.cities"));
                            if (index > 0) this.regions.eq(index).trigger("click");
                        }

                        //mike g - bind to booker behavior
                        try {
                            $(window).trigger('Booker_Opened');
                        } catch (e) {

                        }
                    },

                    onClose: function (e, instance) {
                        var $modal = instance.modal;
                        var $fromRelationInput = self.booker.modals.currentTargetInput.parents("form").find(".from-wrap input");
                        var $toRelationInput = self.booker.modals.currentTargetInput.parents("form").find(".to-wrap input");
                        if ((self.booker.modals.currentTargetInput[0] === $toRelationInput[0]) && (self.booker.stateHistory.current === self.options.views.findFlight)) self.booker.findFlight.togglers.departDate.trigger("click");
                        if (self.booker.modals.currentTargetInput[0] === $fromRelationInput[0]) $toRelationInput.focus();
                        $modal.lists = $modal.find(".city-list");
                        $modal.lists.unbind("click");
                    }
                })
            };


            // Find Flight City Search Autocompleters
            this.booker.findFlight.autocompleter = {
                from: new JB.Class.Autocomplete(this.booker.findFlight.find(".from-wrap"), {
                    source: airportsUnique,
                    onSelect: function (e, ui) {
                        try {
                            $(e.target).data("selected", ui.item.code);
                        } catch (e) { }
                        self.booker.findFlight.find(".to-wrap input").autocomplete("option", "source", self.filterAirports(airports, ui.item.code)).val("").removeData("selected").focus();

                    }
                }),
                to: new JB.Class.Autocomplete(this.booker.findFlight.find(".to-wrap"), {
                    source: airportsUnique,
                    onSelect: function (e, ui) {
                        try {
                            $(e.target).data("selected", ui.item.code).blur();
                        } catch (e) { }
                        self.booker.findFlight.togglers.departDate.trigger("click");

                    }
                })
            };

            if (!this.options.mini) {
                this.booker.findVacation.recentSearches.history = new JB.Class.RecentSearch(this.booker.findVacation.recentSearches.find(".recent-results-list"), this.booker.findVacation, {});

                // Get Vacations List
                new JB.Model.getVacations($.proxy(function (vacations) {
                    // Add Vacations Origins List Modal

                    var gwItemsPerColumn = 25;

                    var regionCounts = new Object();
                    regionCounts.usCount = 0;
                    regionCounts.afCount = 0;
                    regionCounts.meCount = 0;
                    regionCounts.paCount = 0;
                    regionCounts.caCount = 0;
                    regionCounts.asCount = 0;
                    regionCounts.saCount = 0;
                    regionCounts.euCount = 0;



                    this.booker.modals.originsList = new JB.Class.Modal({
                        openTogglers: this.booker.findVacation.find(".from-btn, .from-wrap .viewCity"),
                        url: "http://www.jetblue.com/partials/modal.city-list.html",
                        onLoad: function (e, instance) {
                            // Generate HTML List Template
                            var templates = constructHTMLTemplate(getOrigins(vacations), gwItemsPerColumn);


                            // Modal Instance
                            var self = this;
                            var $modal = instance.modal;
                            //$modal.addClass("origins-list"); // Add style class
                            // Tabulize
                            this.regions = $modal.find(".region-list li"); //.hide().filter("[role='http://www.jetblue.com/js/src/INTERLINE.US']").show();
                            this.tabs = $modal.find("div.cities").hide();
                            this.tabs.active = 0;
                            this.tabs.eq(this.tabs.active).show();

                            // Clear old city list
                            $('div.city-list > ul').each(function (index, elem) {
                                $(elem).remove();
                            });

                            jQuery('.fs-info').css('display', 'none');



                            // IE compat mode will show blank US without the below code
                            for (country in templates) {
                                if (!!!templates[templates[country].region]) templates[templates[country].region] = {};
                                if (!!!templates[templates[country].region].count) templates[templates[country].region].count = 0;
                                switch (templates[country].region) {
                                    case "http://www.jetblue.com/js/src/INTERLINE.US":
                                        var $tab = this.tabs.filter("div[role='http://www.jetblue.com/js/src/INTERLINE.US']");
                                        $tab.columns = $tab.find(".city-list");
                                        for (var i = 0; i < templates[country].html.length; i++) {
                                            $tab.columns.eq(i).append(templates[country].html[i]);
                                        }
                                        regionCounts.usCount++;
                                        break;
                                }
                            }

                            // Tab Through Event Hanlder
                            this.regions.click(function (e) {
                                self.regions.eq(self.tabs.active).removeClass("active");
                                self.tabs.eq(self.tabs.active).hide();
                                self.tabs.active = self.regions.index(this);
                                $(this).addClass("active");
                                self.tabs.eq(self.tabs.active).show();
                            });

                        },
                        onOpen: function (e, instance) {
                             //console.log ("onOpen 2 - flights+ tab from:");
                            // Generate HTML List Template
                            var templates = constructHTMLTemplate(getOrigins(vacations), gwItemsPerColumn);

                            var $target = $(e.currentTarget).parents(".from-wrap, .to-wrap, .depart-city-wrap");
                            var $targetInput = $target.find("input");
                            var $modal = instance.modal;
                            var $fromRelationInput = $target.parents("form").find(".from-wrap input");
                            var $toRelationInput = $target.parents("form").find(".to-wrap input");

                            // Clear old city list
                            $('div.city-list > ul').each(function (index, elem) {

                                $(elem).remove();
                            });

                            jQuery('.fs-info').css('display', 'none');

                            // Populate Modal DOM
                            constructPanel(this, templates, regionCounts);

                            // @mc

                            // @mx3.0-201472 Req. #285 - start  
                            var allInterlines = [
                            "INTERLINE.AFRICA",
                            "INTERLINE.MIDDLEEAST",
                            "INTERLINE.PACIFIC",
                            "INTERLINE.CARRIBEAN_C-AMERICA",
                            "http://www.jetblue.com/js/src/INTERLINE.ASIA",
                            "INTERLINE.S-AMERICA",
                            "INTERLINE.EUROPE"
                        ];
                            var fixedWidth = $('.cities').width() / 3;
                            for (var i = 0; i < allInterlines.length; i++) {
                               // console.info("~~~~~~~~ " + allInterlines[i]);
                                var $tab = this.tabs.filter("div[role='" + allInterlines[i] + "']"),
                                $cols = $tab.find('.city-list'),
                                tabWasVisible = $tab.is(":visible"),
                                currentColIndex = 0,
                                $currentCol = $cols.eq(currentColIndex),
                                currentColHeight = 0
                            ; 

                                $tab.show().width(fixedWidth);
                                var heightThreshold = $currentCol.outerHeight(true) / 3;
                                $cols.find("ul").each(function () {
                                    var $this = $(this),
                                    thisHeight = $this.outerHeight(true)
                                ;
                                    if (currentColIndex > 0) {
                                        $this.appendTo($currentCol);
                                    }
                                    if (currentColIndex < 2 && currentColHeight + thisHeight >= heightThreshold) {
                                       //console.info ("flights+");
                                      // console.info($this.find('li:first').text() + "\ncurrentColHeight: " + currentColHeight + "\nthisHeight: " + thisHeight + "\nheightTreshold: " + heightThreshold);
                                        heightThreshold = currentColHeight + thisHeight;
                                        currentColHeight = 0;
                                        currentColIndex++;
                                        $currentCol = $cols.eq(currentColIndex);
                                    }
                                    currentColHeight += thisHeight;
                                });
                                $tab.attr('style', '');
                                if (!tabWasVisible) {
                                    $tab.hide();
                                }
                            }
                            // @mx3.0-201472 Req. #285 - end

                            this.regionsTitle = $modal.find(".region-list li");
                            var $usTitle = this.regionsTitle.filter("li[role='http://www.jetblue.com/js/src/INTERLINE.US']");
                            var $afTitle = this.regionsTitle.filter("li[role='INTERLINE.AFRICA']");
                            var $meTitle = this.regionsTitle.filter("li[role='INTERLINE.MIDDLEEAST']");
                            var $paTitle = this.regionsTitle.filter("li[role='INTERLINE.PACIFIC']");
                            var $caTitle = this.regionsTitle.filter("li[role='INTERLINE.CARRIBEAN_C-AMERICA']");
                            var $asTitle = this.regionsTitle.filter("li[role='http://www.jetblue.com/js/src/INTERLINE.ASIA']");
                            var $saTitle = this.regionsTitle.filter("li[role='INTERLINE.S-AMERICA']");
                            var $euTitle = this.regionsTitle.filter("li[role='INTERLINE.EUROPE']");

                            regionCounts.usCount == 0 ? $usTitle.addClass('hideTitle') : $usTitle.removeClass('hideTitle');
                            regionCounts.afCount == 0 ? $afTitle.addClass('hideTitle') : $afTitle.removeClass('hideTitle');
                            regionCounts.meCount == 0 ? $meTitle.addClass('hideTitle') : $meTitle.removeClass('hideTitle');
                            regionCounts.paCount == 0 ? $paTitle.addClass('hideTitle') : $paTitle.removeClass('hideTitle');
                            regionCounts.caCount == 0 ? $caTitle.addClass('hideTitle') : $caTitle.removeClass('hideTitle');
                            regionCounts.asCount == 0 ? $asTitle.addClass('hideTitle') : $asTitle.removeClass('hideTitle');
                            regionCounts.saCount == 0 ? $saTitle.addClass('hideTitle') : $saTitle.removeClass('hideTitle');
                            regionCounts.euCount == 0 ? $euTitle.addClass('hideTitle') : $euTitle.removeClass('hideTitle');

                            $('.city-desc').css('display', 'block');
                            // Create a RegExp that checks for three letters proceeded by a close paren and end of string
                            var match = /\w{3}(?=\)$)/;
                            // Execute a match on the targetInput's value
                            var title = $targetInput.val();
                            var code = match.exec(title);
                            // Store whether the input field is active (i.e. 'Contains text that matches the RegExp')
                            var active = false;
                            // Set active to true if there is a match returned
                            if (code) { $targetInput.data("selected", code); }
                            self.booker.modals.currentTargetInput = $targetInput;
                            $modal.regions = $modal.find("ul.region-list li").show();
                            $modal.lists = $modal.find(".city-list");
                            $modal.lists.find("ul").show();
                            $modal.lists.find("li:not(.list-head)").show();
                            $modal.lists.find('a.active').removeClass("active");
                            var index = this.tabs.index($modal.lists.find("li a[title=\"" + title + "\"]").addClass("active").parents("div.cities"));
                            if (index !== -1) this.regions.eq(index).trigger("click");
                            else this.regions.eq(0).trigger("click");
                            $modal.lists.click(function (e) {
                                e.preventDefault();
                                if (e.target.nodeName.toLowerCase() !== "a") return false;
                                $modal.lists.find('a.active').removeClass("active");
                                $(e.target).addClass("active");
                                $targetInput.val(e.target.title);
                                $targetInput.data("selected", e.target.rel);
                                if ($targetInput[0] === $fromRelationInput[0]) {
                                    $toRelationInput.autocomplete("option", "source", self.filterAirports(getDestinations(vacations), e.target.rel));
                                    $toRelationInput.removeData("selected").val("");
                                }
                                instance.close(e);
                            });
                            if (($targetInput[0] === $toRelationInput[0]) && ($fromRelationInput.val() === "")) $fromRelationInput.removeData("selected");
                            if ($targetInput[0] === $fromRelationInput[0]) return;

                            // Filter Availability View
                            var fromFieldValue = jQuery('#vac_from_field').attr('value');

                            var fromCode;

                            //add airport code for MAC cities that don't show airport code. 
                            if (!match.exec(fromFieldValue)) {
                                for (var j = 0; j < MAClist.length; j++) {
                                    var macCity = window["o" + MAClist[j]];
                                    if (macCity.name == fromFieldValue) {
                                        fromCode = macCity.code;
                                        break;
                                    }
                                }
                            } else { fromCode = (fromFieldValue.length > 3 ? fromFieldValue.substring(fromFieldValue.length - 4, fromFieldValue.length - 1) : fromFieldValue.toUpperCase()); }

                            fromCode = $fromRelationInput.data("selected");
                            if (!!fromCode) {
                                $modal.lists.find("ul").hide();
                                $modal.lists.find("li:not(.list-head)").hide();
                                $modal.lists.availables = $modal.lists.find("li[routes*=" + fromCode + "]").show();
                                // Filter List Heads
                                $modal.lists.availables.parent("ul").show();
                                // Tabs Filter
                                $modal.regions.hide();
                                $modal.lists.availables.parents("div.cities").each(function (i, el) {
                                    $modal.regions.filter("li[role='" + $(el).attr("role") + "']").show();
                                });
                                $modal.regions.filter(":visible").eq(0).trigger("click");
                                // Toggle Tab
                                var index = this.tabs.index($modal.lists.find("li a[title=\"" + title + "\"]").addClass("active").parents("div.cities"));
                                if (index > 0) this.regions.eq(index).trigger("click");
                            }

                            //mike g - bind to booker behavior
                            try {
                                $(window).trigger('Booker_Opened');
                            } catch (e) {

                            }

                        },
                        onClose: function (e, instance) {
                            var $modal = instance.modal;
                            var $fromRelationInput = self.booker.modals.currentTargetInput.parents("form").find(".from-wrap input");
                            var $toRelationInput = self.booker.modals.currentTargetInput.parents("form").find(".to-wrap input");
                            if (self.booker.modals.currentTargetInput[0] === $fromRelationInput[0]) $toRelationInput.focus();
                            $modal.lists = $modal.find(".city-list");
                            $modal.lists.unbind("click");
                        }
                    });

                    // Add Vacations Destinations List Modal
                    this.booker.modals.destinationsList = new JB.Class.Modal({
                        openTogglers: this.booker.findVacation.find(".to-btn, .to-wrap .viewCity"),
                        url: "http://www.jetblue.com/partials/modal.city-list.html",
                        onLoad: function (e, instance) {
                            // Generate HTML List Template
                            var templates = constructHTMLTemplate(getDestinations(vacations, gwItemsPerColumn));


                            // Modal Instance
                            var self = this;
                            var $modal = instance.modal;
                            //$modal.addClass("vacations-list"); // Add style class
                            //$modal.find(".header h3").text("Select a vacation destination below") // Update Header


                            // Tabulize
                            this.regions = $modal.find(".region-list li"); //.hide().filter("[role='http://www.jetblue.com/js/src/INTERLINE.US']").show();
                            this.tabs = $modal.find("div.cities").hide();
                            this.tabs.active = 0;
                            this.tabs.eq(this.tabs.active).show();

                            // Clear old city list
                            $('div.city-list > ul').each(function (index, elem) {
                                $(elem).remove();
                            });

                            jQuery('.fs-info').css('display', 'none');

                            // IE compat mode will show blank US without the below code
                            for (country in templates) {
                                if (!!!templates[templates[country].region]) templates[templates[country].region] = {};
                                if (!!!templates[templates[country].region].count) templates[templates[country].region].count = 0;
                                switch (templates[country].region) {
                                    case "http://www.jetblue.com/js/src/INTERLINE.US":
                                        var $tab = this.tabs.filter("div[role='http://www.jetblue.com/js/src/INTERLINE.US']");
                                        $tab.columns = $tab.find(".city-list");
                                        for (var i = 0; i < templates[country].html.length; i++) {
                                            $tab.columns.eq(i).append(templates[country].html[i]);
                                        }
                                        regionCounts.usCount++;
                                        break;
                                }
                            }
                            // Tab Through Event Hanlder
                            this.regions.click(function (e) {
                                self.regions.eq(self.tabs.active).removeClass("active");
                                self.tabs.eq(self.tabs.active).hide();
                                self.tabs.active = self.regions.index(this);
                                $(this).addClass("active");
                                self.tabs.eq(self.tabs.active).show();
                            });
                        },
                        onOpen: function (e, instance) {
                            // console.log ("onOpen 3 flights+ tab To:");
                            // Generate HTML List Template - vacations?
                            var templates = constructHTMLTemplate(getDestinations(vacations), gwItemsPerColumn);

                            var $target = $(e.currentTarget).parents(".from-wrap, .to-wrap, .depart-city-wrap");
                            var $targetInput = $target.find("input");
                            var $modal = instance.modal;
                            var $fromRelationInput = $target.parents("form").find(".from-wrap input");
                            var $toRelationInput = $target.parents("form").find(".to-wrap input");

                            // Clear old city list
                            $('div.city-list > ul').each(function (index, elem) {
                                $(elem).remove();
                            });

                            jQuery('.fs-info').css('display', 'none');
                            constructPanel(this, templates, regionCounts);

                            // @mx3.1-20140819 - start  
                            var allInterlines = [
                            "INTERLINE.AFRICA",
                            "INTERLINE.MIDDLEEAST",
                            "INTERLINE.PACIFIC",
                            "INTERLINE.CARRIBEAN_C-AMERICA",
                            "http://www.jetblue.com/js/src/INTERLINE.ASIA",
                            "INTERLINE.S-AMERICA",
                            "INTERLINE.EUROPE"
                        ];
                            var fixedWidth = $('.cities').width() / 3;
                            for (var i = 0; i < allInterlines.length; i++) {
                               // console.info("~~~~~~~~ " + allInterlines[i]);
                                var $tab = this.tabs.filter("div[role='" + allInterlines[i] + "']"),
                                $cols = $tab.find('.city-list'),
                                tabWasVisible = $tab.is(":visible"),
                                currentColIndex = 0,
                                $currentCol = $cols.eq(currentColIndex),
                                currentColHeight = 0
                            ; 

                                $tab.show().width(fixedWidth);
                                var heightThreshold = $currentCol.outerHeight(true) / 3;
                                $cols.find("ul").each(function () {
                                    var $this = $(this),
                                    thisHeight = $this.outerHeight(true)
                                ;
                                    if (currentColIndex > 0) {
                                        $this.appendTo($currentCol);
                                    }
                                    if (currentColIndex < 2 && currentColHeight + thisHeight >= heightThreshold) {
                                      // console.info($this.find('li:first').text() + "\ncurrentColHeight: " + currentColHeight + "\nthisHeight: " + thisHeight + "\nheightTreshold: " + heightThreshold);
                                        heightThreshold = currentColHeight + thisHeight;
                                        currentColHeight = 0;
                                        currentColIndex++;
                                        $currentCol = $cols.eq(currentColIndex);
                                    }
                                    currentColHeight += thisHeight;
                                });
                                $tab.attr('style', '');
                                if (!tabWasVisible) {
                                    $tab.hide();
                                }
                            }
                            // @mx3.1-20140819 - end

                            this.regionsTitle = $modal.find(".region-list li");
                            var $usTitle = this.regionsTitle.filter("li[role='http://www.jetblue.com/js/src/INTERLINE.US']");
                            var $afTitle = this.regionsTitle.filter("li[role='INTERLINE.AFRICA']");
                            var $meTitle = this.regionsTitle.filter("li[role='INTERLINE.MIDDLEEAST']");
                            var $paTitle = this.regionsTitle.filter("li[role='INTERLINE.PACIFIC']");
                            var $caTitle = this.regionsTitle.filter("li[role='INTERLINE.CARRIBEAN_C-AMERICA']");
                            var $asTitle = this.regionsTitle.filter("li[role='http://www.jetblue.com/js/src/INTERLINE.ASIA']");
                            var $saTitle = this.regionsTitle.filter("li[role='INTERLINE.S-AMERICA']");
                            var $euTitle = this.regionsTitle.filter("li[role='INTERLINE.EUROPE']");

                            regionCounts.usCount == 0 ? $usTitle.addClass('hideTitle') : $usTitle.removeClass('hideTitle');
                            regionCounts.afCount == 0 ? $afTitle.addClass('hideTitle') : $afTitle.removeClass('hideTitle');
                            regionCounts.meCount == 0 ? $meTitle.addClass('hideTitle') : $meTitle.removeClass('hideTitle');
                            regionCounts.paCount == 0 ? $paTitle.addClass('hideTitle') : $paTitle.removeClass('hideTitle');
                            regionCounts.caCount == 0 ? $caTitle.addClass('hideTitle') : $caTitle.removeClass('hideTitle');
                            regionCounts.asCount == 0 ? $asTitle.addClass('hideTitle') : $asTitle.removeClass('hideTitle');
                            regionCounts.saCount == 0 ? $saTitle.addClass('hideTitle') : $saTitle.removeClass('hideTitle');
                            regionCounts.euCount == 0 ? $euTitle.addClass('hideTitle') : $euTitle.removeClass('hideTitle');

                            $('.city-desc').css('display', 'block');
                            // Create a RegExp that checks for three letters proceeded by a close paren and end of string
                            var match = /\w{3}(?=\)$)/;
                            // Execute a match on the targetInput's value
                            var title = $targetInput.val();
                            var code = match.exec(title);
                            // Store whether the input field is active (i.e. 'Contains text that matches the RegExp')
                            var active = false;
                            // Set active to true if there is a match returned
                            if (code) { $targetInput.data("selected", code); }
                            self.booker.modals.currentTargetInput = $targetInput;
                            $modal.regions = $modal.find("ul.region-list li").show();
                            $modal.lists = $modal.find(".city-list");
                            $modal.lists.find("ul").show();
                            $modal.lists.find("li:not(.list-head)").show();
                            $modal.lists.find('a.active').removeClass("active");
                            var index = this.tabs.index($modal.lists.find("li a[title=\"" + title + "\"]").addClass("active").parents("div.cities"));
                            if (index !== -1) this.regions.eq(index).trigger("click");
                            else this.regions.eq(0).trigger("click");
                            $modal.lists.click(function (e) {
                                e.preventDefault();
                                if (e.target.nodeName.toLowerCase() !== "a") return false;
                                $modal.lists.find('a.active').removeClass("active");
                                $(e.target).addClass("active");
                                $targetInput.val(e.target.title);
                                $targetInput.data("selected", e.target.rel);
                                if ($targetInput[0] === $fromRelationInput[0]) {
                                    $toRelationInput.autocomplete("option", "source", self.filterAirports(getDestinations(vacations), e.target.rel));
                                    $toRelationInput.removeData("selected").val("");
                                }
                                instance.close(e);
                            });
                            if (($targetInput[0] === $toRelationInput[0]) && ($fromRelationInput.val() === "")) $fromRelationInput.removeData("selected");
                            if ($targetInput[0] === $fromRelationInput[0]) return;
                            // Filter Availability View
                            var fromFieldValue = jQuery('#vac_from_field').attr('value');

                            var fromCode;

                            //add airport code for MAC cities that don't show airport code. 
                            if (!match.exec(fromFieldValue)) {
                                for (var j = 0; j < MAClist.length; j++) {
                                    var macCity = window["o" + MAClist[j]];
                                    if (macCity.name == fromFieldValue) {
                                        fromCode = macCity.code;
                                        break;
                                    }
                                }
                            } else { fromCode = (fromFieldValue.length > 3 ? fromFieldValue.substring(fromFieldValue.length - 4, fromFieldValue.length - 1) : fromFieldValue.toUpperCase()); }

                            if (!!fromCode) {
                                $modal.lists.find("ul").hide();
                                $modal.lists.find("li:not(.list-head)").hide();
                                $modal.lists.availables = $modal.lists.find("li[routes*=" + fromCode + "]").show();
                                // Filter List Heads
                                $modal.lists.availables.parent("ul").show();
                                // Tabs Filter
                                $modal.regions.hide();
                                $modal.lists.availables.parents("div.cities").each(function (i, el) {
                                    $modal.regions.filter("li[role='" + $(el).attr("role") + "']").show();
                                });
                                $modal.regions.filter(":visible").eq(0).trigger("click");
                                // Toggle Tab
                                var index = this.tabs.index($modal.lists.find("li a[title=\"" + title + "\"]").addClass("active").parents("div.cities"));
                                if (index > 0) this.regions.eq(index).trigger("click");
                            }

                            //mike g - bind to booker behavior
                            try {
                                $(window).trigger('Booker_Opened');
                            } catch (e) {

                            }
                        },
                        onClose: function (e, instance) {
                            var $modal = instance.modal;
                            var $fromRelationInput = self.booker.modals.currentTargetInput.parents("form").find(".from-wrap input");
                            var $toRelationInput = self.booker.modals.currentTargetInput.parents("form").find(".to-wrap input");
                            if (self.booker.modals.currentTargetInput[0] === $toRelationInput[0]) self.booker.findVacation.togglers.departDate.trigger("click");
                            $modal.lists = $modal.find(".city-list");
                            $modal.lists.unbind("click");
                        }
                    });

                    // Find Vacations City Search Autocompleters
                    this.booker.findVacation.autocompleter = {
                        from: new JB.Class.Autocomplete(this.booker.findVacation.find(".from-wrap"), {
                            source: getOrigins(vacations),
                            onSelect: function (e, ui) {
                                $(e.target).data("selected", ui.item.code);
                                self.booker.findVacation.find(".to-wrap input").autocomplete("option", "source", self.filterAirports(getDestinations(vacations), ui.item.code)).val("").removeData("selected").focus();
                            }
                        }),
                        to: new JB.Class.Autocomplete(this.booker.findVacation.find(".to-wrap"), {
                            source: getDestinations(vacations),
                            onSelect: function (e, ui) {
                                $(e.target).data("selected", ui.item.code).blur();
                                self.booker.findVacation.togglers.departDate.trigger("click");
                            }
                        })
                    };
                }, this));

                // Flight Status City Search Autocompleters
                this.booker.flightStatus.autocompleter = {
                    from: new JB.Class.Autocomplete(this.booker.flightStatus.find(".from-wrap"), {
                        source: flightStatusAirports,
                        onSelect: function (e, ui) {
                            $(e.target).data("selected", ui.item.code);
                            self.booker.flightStatus.find(".to-wrap input").autocomplete("option", "source", self.filterAirports(flightStatusAirports, ui.item.code)).val("").removeData("selected").focus();
                        }
                    }),
                    to: new JB.Class.Autocomplete(this.booker.flightStatus.find(".to-wrap"), {
                        source: flightStatusAirports,
                        onSelect: function (e, ui) {
                            $(e.target).data("selected", ui.item.code);
                            $("#check-status-btn").focus();
                        }
                    })
                };
                // Flight Status Departure Date Dropdown Handle
                this.booker.flightStatus.departureDate.change(function (e) {
                    var $labels = $(this).parent().find(".dd-text");
                    var thisSelectedText = this.options[this.selectedIndex].text;
                    $labels.text(thisSelectedText);
                    $ffstatusForm = document.getElementById("ffstatus");
                    $ffstatusForm['FlightDate'].value = this.value;
                    //$ffstatusForm['FlightDate2'].value = this.value;
                });

                /*
                this.booker.flightStatus.departDate.change(function (e) {
                $ffstatusForm = document.getElementById("ffstatus");
                $ffstatusForm['FlightDate1'].value = this.value;
                });
                this.booker.flightStatus.departDate2.change(function (e) {
                $ffstatusForm = document.getElementById("ffstatus");
                $ffstatusForm['FlightDate2'].value = this.value;
                });
                */
                this.booker.checkin.autocompleter = {
                    departureCity: new JB.Class.Autocomplete(this.booker.checkin.find(".depart-city-wrap"), {
                        source: airportsUnique,
                        onSelect: function (e, ui) {
                            $(e.target).data("selected", ui.item.code);
                            $("#first_name_field").focus();
                        }
                    })
                };
            }
        }, this));

        // Add a custom validator method to make sure that value doesn't match placeholder value.
        jQuery.validator.addMethod("checkPlaceholderMatch", function (value, element) {
            //if(window.console) console.log('check placeholders');
            if (element.value == $(element).attr('placeholder')) return false;
            return true;
        }, "");

        //add a method to see if input is alphanumeric
        jQuery.validator.addMethod("alphanumeric",function (value, element){
           return /^[a-z0-9]+$/i.test(value);
        },"");

        // Find Flight Form Validation and Submit Handle
        this.booker.findFlight.validator = this.booker.findFlight.validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            focusInvalid: false,
            rules: {
                from_field: "required",
                to_field: "required",
                departure_field: {
                    required: function (el) {
                        if ($('#flexible').is(':checked')) {
                            return false;
                        } else {
                            return true;
                        }
                    },
                    date: function (el) {
                        if (!/Invalid|NaN/.test(new Date(el.value))) {
                            if ((new Date().toDateString() === new Date(el.value).toDateString()) && !!!(self.booker.findFlight.isWarned)) self.booker.findFlight.isWarned = true;
                        }
                        return true;
                    }
                },
                return_field: {
                    required: function (el) {
                        if (self.booker.findFlight.togglers.returnDate.hasClass("disabled") || $('#flexible').is(':checked')) {
                            return false;
                        } else {
                            return true;
                        }
                    },
                    date: true
                },
                adult_count: {
                    min: function (el) {
                        if ((self.booker.findFlight.kidsCount.val() <= 0) && (el.value <= 0) && (self.booker.findFlight.adultsCount.val() <= 0) && (el.value <= 0)) return 1;
                        else return 0;
                    }
                },
                kid_count: {
                    min: function (el) {
                        //if ((self.booker.findFlight.adultsCount.val() <= 0) && (el.value <= 0)) return 1;
                        //else 
                        return 0;
                    }
                }
            },
            messages: {
                from_field: this.options.errors.departure,
                to_field: this.options.errors.destination,
                departure_field: {
                    required: this.options.errors.departDate,
                    date: this.options.errors.departDate
                },
                return_field: {
                    required: this.options.errors.returnDate,
                    date: this.options.errors.returnDate
                },
                adult_count: {
                    min: this.options.errors.minPassengers
                },
                kid_count: {
                    min: this.options.errors.minPassengers
                }
            },
            showErrors: function (errorMap, errorList) {
                self.booker.error.messages.html(""); // Clear Messages
                if ($.isArray(errorList) && !!(errorList.length)) {
                    var errors = "";
                    for (var i = 0; i < errorList.length; i++) {
                        errors += errorList[i].message + "<br>";
                    }
                    self.booker.error.messages.append("<p>" + errors + "</p>");
                    self.changeViewTo(self.booker.views.error);
                }
            },
            invalidHandler: function (form, validator) {
            },
            submitHandler: function (form) {
                var fromValue = form['from_field'].value;
                var toValue = form['to_field'].value;

                var oVarFrom;
                var oVarTo;

                //get aiport codes for MAC cities without 3 letter aiport code
                var match = /\w{3}(?=\)$)/;
                if (!match.exec(fromValue)) {
                    for (var j = 0; j < MAClist.length; j++) {

                        var macCity = window["o" + MAClist[j]];

                        if (macCity.name == fromValue) {
                            oVarFrom = macCity;
                            break;
                        }
                    }
                } else { oVarFrom = window['o' + (fromValue.length > 3 ? fromValue.substring(fromValue.length - 4, fromValue.length - 1) : fromValue.toUpperCase())]; }

                if (!match.exec(toValue)) {
                    for (var j = 0; j < MAClist.length; j++) {

                        var macCity = window["o" + MAClist[j]];

                        if (macCity.name == toValue) {
                            oVarTo = macCity;
                            break;
                        }
                    }
                } else { oVarTo = window['o' + (toValue.length > 3 ? toValue.substring(toValue.length - 4, toValue.length - 1) : toValue.toUpperCase())]; }


                //var oVarFrom = window['o' + (fromValue.length > 3 ? fromValue.substring(fromValue.length - 4, fromValue.length - 1) : fromValue.toUpperCase())];
                //var oVarTo = window['o' + (toValue.length > 3 ? toValue.substring(toValue.length - 4, toValue.length - 1) : toValue.toUpperCase())];

                if (oVarFrom == undefined) {
                    self.booker.error.messages.html(""); // Clear Messages
                    self.booker.error.messages.append("<p>" + self.options.errors.departure + "</p>");
                    self.changeViewTo(self.booker.views.error);
                    return false;
                } else {
                    form['from_field'].value = oVarFrom.name;
                }
                if (oVarTo == undefined) {
                    self.booker.error.messages.html(""); // Clear Messages
                    self.booker.error.messages.append("<p>" + self.options.errors.destination + "</p>");
                    self.changeViewTo(self.booker.views.error);
                    return false;
                } else {
                    form['to_field'].value = oVarTo.name;
                }

                var isFromJB = oVarFrom.jb;
                var isToJB = oVarTo.jb;
                var fromCC = oVarFrom.cc;
                var toCC = oVarTo.cc;
                var fType = form['fareDisplay'].value;
                $fflightForm = document.getElementById("fflight");
                var isOW = $fflightForm['journeySpan'].value == "OW";
                var fromCC = oVarFrom.cc;
                var toCC = oVarTo.cc;
                var fromUS = (fromCC == "US" || fromCC == "PR" || fromCC == "VI");
                var toUS = (toCC == "US" || toCC == "PR" || toCC == "VI");
                var isInt = (fromUS != toUS);
                var pointsSearchExcludeHybridMarket = ['ACK', 'MVY', 'STT', 'STX'];
                var flexibleDateExcludeCity = ['ACK', 'MVY', 'STT', 'STX', 'XBO', 'XDR', 'XFL', 'XSF', 'ZLA'];

                var isFlexibleDateExcludeCity = ((!isFromJB || $.inArray(oVarFrom.code, flexibleDateExcludeCity) > -1) || (!isToJB || $.inArray(oVarTo.code, flexibleDateExcludeCity) > -1));

                if ((parseInt(form["adult_count"].value) + parseInt(form["kid_count"].value)) > 7) {
                    self.booker.error.messages.html(""); // Clear Messages
                    self.booker.error.messages.append("<p>" + self.options.errors.maxPassengers + "</p>");
                    self.changeViewTo(self.booker.views.error);
                    return false;
                }
                if ((parseInt(form["infant_count"].value) > 3) || (parseInt(form["infant_count"].value) > parseInt(form["adult_count"].value))) {
                    self.booker.error.messages.html(""); // Clear Messages
                    self.booker.error.messages.append("<p>" + self.options.errors.maxInfants + "</p>");
                    self.changeViewTo(self.booker.views.error);
                    return false;
                }
                //error message for points
                if (!(isFromJB && isToJB) && fType == "points" && $.inArray(oVarFrom.code, pointsSearchExcludeHybridMarket) == -1 && $.inArray(oVarTo.code, pointsSearchExcludeHybridMarket) == -1) {
                    self.booker.error.messages.html(""); // Clear Messages
                    self.booker.error.messages.append("<p>" + self.options.errors.partnerTB + "</p>");
                    self.changeViewTo(self.booker.views.error);
                    return false;
                }
                //Error message for extended return date
                //28 refers to 4 weeks time span
                var depd = new Date(form['departure_field'].value);
                depd.setDate(depd.getDate() + 28);
                var retd = new Date(form['return_field'].value);
                var isReturnDateisMonthPastDept = (retd > depd) && !isOW;



                //A321 Entertainment Affected Check
                var A321InflightAffected = false;
                var inA321AffectedRoute = false;
                var deptInA321AffectedDates = false;
                var returnInA321AffectedDates = false;
                var selectedDepartureDate = new Date(form['departure_field'].value);
                var selectedReturnDate = undefined;
                if (!isOW) selectedReturnDate = new Date(form['return_field'].value);
                var A321InflightAffectedMessage = "";
                $.each(A321InflightImpact, function (i, value) {

                    //Check Defined routes
                    $.each(A321InflightImpact[i][0], function (j, value) {
                        if (([oVarFrom.code, oVarTo.code]).toString() === (A321InflightImpact[i][0][j]).toString()) {

                            inA321AffectedRoute = true;
                        }
                    });

                    if (inA321AffectedRoute) {
                        if (selectedDepartureDate >= A321InflightImpact[i][1] && selectedDepartureDate <= A321InflightImpact[i][2]) {
                            deptInA321AffectedDates = true;
                        }
                        if (!isOW && (selectedReturnDate >= A321InflightImpact[i][1] && selectedReturnDate <= A321InflightImpact[i][2])) {
                            returnInA321AffectedDates = true;
                        }
                    }
                    if (inA321AffectedRoute && (deptInA321AffectedDates || returnInA321AffectedDates)) {

                        //Show message
                        A321InflightAffected = true;

                        var departureFlights = "";
                        var returnFlights = "";
                        //If departure date is affected add departing flights to message
                        if (deptInA321AffectedDates) {
                            departureFlights = A321InflightImpact[i][3].toString();
                        }
                        else {
                            departureFlights = "";
                        }
                        //if both dept and return are shown add the comma
                        if (returnInA321AffectedDates && deptInA321AffectedDates) {
                            returnFlights = ", " + A321InflightImpact[i][4].toString();
                        }
                        else if (returnInA321AffectedDates) {
                            returnFlights = A321InflightImpact[i][4].toString();
                        }
                        else {
                            returnFlights = "";
                        }
                        //if this is the mini booker - show shortend version
                        if (self.options.mini) {
                            A321InflightAffectedMessage = GlobalErrors.MainBooker.A321InflightPrefixMini + departureFlights + returnFlights + GlobalErrors.MainBooker.A321InflightSuffixMini;
                        }
                        else {
                            A321InflightAffectedMessage = GlobalErrors.MainBooker.A321InflightPrefix + departureFlights + returnFlights + GlobalErrors.MainBooker.A321InflightSuffix;
                        }


                        //Break out current each
                        return false;
                    }
                });



                //error message for flexible date
                if ($('#flexible').is(':checked') && !isFareCalendarInterlineAvailable && isFlexibleDateExcludeCity) {
                    self.booker.error.messages.html(""); // Clear Messages

                    var isBlankDate = form['departure_field'].value == '' || (form['return_field'].value == '' && !isOW);

                    if (!isBlankDate) {


                        self.booker.error.messages.append("<p id='flexibleInterlineDateError'>" + self.options.errors.fareCalendarInterlineNA + "</p>");
                        if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='returnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                        if (A321InflightAffected) self.booker.error.messages.append("<p id='a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        // self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                        alignOkCancelErrorButtons();
                        jQuery('#oops').html('&nbsp;');
                    }
                    else {
                        self.booker.error.messages.append("<p id='flexibleDateError'>" + self.options.errors.fdInterlineNoDate + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        jQuery('#oops').html('&nbsp;');
                        self.booker.error.buttons.cancel.click(function (e) {
                            self.booker.error.buttons.ok.find('span').text("OK");
                            self.booker.error.messages.html(""); // Clear Messages 
                        });
                        $('.close-btn').click(function (e) {
                            self.booker.error.buttons.ok.find('span').text("OK");
                            self.booker.error.messages.html(""); // Clear Messages 
                        });
                    }

                    return false;
                }
                // @mx 3.0 req. #285 - start-1
                var isSeasonalFlight = false;
                var isdayOfWeekFlight = false;

                if (!$('#flexible').is(':checked')) {
                    // Seasonal flight                    
                    $.each(seasonalRoutes, function (index, value) {

                        if ($([oVarFrom.code, oVarTo.code]).not(seasonalRoutes[index]).length == 0 && $(seasonalRoutes[index]).not([oVarFrom.code, oVarTo.code]).length == 0) {
                            self.booker.error.messages.html(""); // Clear Messages                               
                            self.booker.error.messages.append("<p id='seasonalError'>" + self.options.errors.seasonalRouteError + "</p>");
                            if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='returnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                            if (A321InflightAffected) self.booker.error.messages.append("<p id='a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                            self.booker.error.buttons.ok.find('span').text("Continue");
                          //  $('.close-btn').hide(); // @mx3.0.1-201478 - reverted 2014-0804
                            self.changeViewTo(self.booker.views.error);
                            // @mx3.0.1-201478 - self.booker.error.buttons.cancel.removeClass('hideCancelBtn'); 
                            alignOkCancelErrorButtons();
                            jQuery('#oops').html('&nbsp;');
                            //Best Fare Finder URL + flight search parameters
                            $('#seasonalRouteBFF').attr('href', "/bestfarefinder/?origin=" + oVarFrom.code + "&destination=" + oVarTo.code + "&trip=" + $fflightForm['journeySpan'].value + "&fare=" + $fflightForm['fareDisplay'].value + "&adult=" + form['adult_count'].value + "&kid=" + form['kid_count'].value + "&infant=" + form['infant_count'].value + "&departure=" + form['departure_field'].value);

                            self.booker.error.buttons.cancel.click(function (e) {
                                self.booker.error.buttons.ok.find('span').text("OK");
                                self.booker.error.messages.html(""); // Clear Messages 
                            });
                            $('.close-btn').click(function (e) {
                                self.booker.error.buttons.ok.find('span').text("OK");
                                self.booker.error.messages.html(""); // Clear Messages 
                            });

                            isSeasonalFlight = true;
                            return false;
                        }
                    });

                    if (!isSeasonalFlight) {
                        //day of week flight       
                        $.each(dayOfWeekRoutes, function (index, value) {

                            if ($([oVarFrom.code, oVarTo.code]).not(dayOfWeekRoutes[index]).length == 0 && $(dayOfWeekRoutes[index]).not([oVarFrom.code, oVarTo.code]).length == 0) {
                                self.booker.error.messages.html(""); // Clear Messages                                
                                self.booker.error.messages.append("<p  id='dayOfWeekError'>" + self.options.errors.dayOfWeekRouteError + "</p>");
                                if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='returnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                                if (A321InflightAffected) self.booker.error.messages.append("<p id='a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                                self.booker.error.buttons.ok.find('span').text("Continue");
                                self.changeViewTo(self.booker.views.error);
                                // self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                                alignOkCancelErrorButtons();
                                //  $('.close-btn').hide(); // @mx3.0.1-2014710
                                jQuery('#oops').html('&nbsp;');
                                //Best Fare Finder URL + flight search parameters
                                $('#dayOfWeekRouteBFF').attr('href', "/bestfarefinder/?origin=" + oVarFrom.code + "&destination=" + oVarTo.code + "&trip=" + $fflightForm['journeySpan'].value + "&fare=" + $fflightForm['fareDisplay'].value + "&adult=" + form['adult_count'].value + "&kid=" + form['kid_count'].value + "&infant=" + form['infant_count'].value + "&departure=" + form['departure_field'].value);

                                self.booker.error.buttons.cancel.click(function (e) {
                                    self.booker.error.buttons.ok.find('span').text("OK");
                                    self.booker.error.messages.html(""); // Clear Messages 
                                });
                                $('.close-btn').click(function (e) {
                                    self.booker.error.buttons.ok.find('span').text("OK");
                                    self.booker.error.messages.html(""); // Clear Messages 
                                });

                                isdayOfWeekFlight = true;
                                return false;
                            }
                        });
                    }
                }


                var isSameDay = (form['departure_field'].value == form['return_field'].value) && !isOW;
                var isToday = self.booker.findFlight.isWarned && !!!(self.booker.findFlight.isWarned.checked);
                var isOWInt = isOW && isInt;
                var isFlexibleDate = $('#flexible').is(':checked');
                if (((isSameDay && !isFlexibleDate) || (isToday && !isFlexibleDate) || isOWInt) && (!isSeasonalFlight && !isdayOfWeekFlight)) {
                    self.booker.error.messages.html(""); // Clear Messages
                    if (isSameDay) self.booker.error.messages.append("<p>" + self.options.errors.sameDay + "</p>");
                    if (isToday) self.booker.error.messages.append("<p>" + self.options.errors.todayWarning + "</p>");
                    if (isOWInt) self.booker.error.messages.append("<p>" + self.options.errors.owInt + "</p>");
                    if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='returnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                    if (A321InflightAffected) self.booker.error.messages.append("<p id='a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                    self.changeViewTo(self.booker.views.error);
                    //self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                    alignOkCancelErrorButtons();
                    jQuery('#oops').html('&nbsp;');
                    self.booker.error.buttons.ok.click(function (e) {
                        if (self.booker.findFlight.submitted) return false;
                        else self.booker.findFlight.submitted = true;
                        if (isToday) self.booker.findFlight.isWarned.checked = true;
                        self.booker.submitFF.submit(form);
                    });
                    return false;
                } else if (isSeasonalFlight || isdayOfWeekFlight) {
                    return false;
                }
                else if (isReturnDateisMonthPastDept) {
                    self.booker.error.messages.html(""); // Clear Messages
                    if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='returnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                    if (A321InflightAffected) self.booker.error.messages.append("<p id='a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                    self.booker.error.buttons.ok.find('span').text("OK");
                    self.changeViewTo(self.booker.views.error);
                    // self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                    alignOkCancelErrorButtons();
                    jQuery('#oops').html('&nbsp;');

                    self.booker.error.buttons.cancel.click(function (e) {
                        self.booker.error.buttons.ok.find('span').text("OK");
                        self.booker.error.messages.html(""); // Clear Messages 
                    });
                    $('.close-btn').click(function (e) {
                        self.booker.error.buttons.ok.find('span').text("OK");
                        self.booker.error.messages.html(""); // Clear Messages 
                    });
                    return false;
                }
                else if (A321InflightAffected) {
                    self.booker.error.messages.html(""); // Clear Messages
                    if (A321InflightAffected) self.booker.error.messages.append("<p id='a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                    self.booker.error.buttons.ok.find('span').text("OK");
                    self.changeViewTo(self.booker.views.error);
                    // self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                    alignOkCancelErrorButtons();
                    jQuery('#oops').html('&nbsp;');

                    self.booker.error.buttons.cancel.click(function (e) {
                        self.booker.error.buttons.ok.find('span').text("OK");
                        self.booker.error.messages.html(""); // Clear Messages 
                    });
                    $('.close-btn').click(function (e) {
                        self.booker.error.buttons.ok.find('span').text("OK");
                        self.booker.error.messages.html(""); // Clear Messages 
                    });
                    return false;
                }
                else { // no error, submit form
                    self.booker.submitFF.submit(form);
                }
                //form.submit();
            }
        });

        if (!this.options.mini) {
            // Find Vacation Form Validation and Submit Handle
            this.booker.findVacation.validator = this.booker.findVacation.validate({
                onfocusout: false,
                onkeyup: false,
                onclick: false,
                focusInvalid: false,
                rules: {
                    vac_from_field: "required",
                    vac_to_field: "required",
                    vac_departure_field: {
                        required: true,
                        date: function (el) {
                            if (!/Invalid|NaN/.test(new Date(el.value))) {
                                if ((new Date().toDateString() === new Date(el.value).toDateString()) && !!!(self.booker.findVacation.isWarned)) self.booker.findVacation.isWarned = true;
                            }
                            return true;
                        }
                    },
                    vac_return_field: {
                        required: true,
                        date: true
                    },
                    vac_adult_count: {
                        min: function (el) {
                            if ((self.booker.findVacation.kidsCount.val() <= 0) && (el.value <= 0)) return 1;
                            else return 0;
                        }
                    },
                    kid_vacation: {
                        min: function (el) {
                           // if (window.console) console.log('kid vacation select');
                            if ((self.booker.findVacation.adultsCount.val() <= 0) && (el.value <= 0)) return 1;
                            else return 0;
                        }
                    },
                    kid_age_1: {
                        required: function (el) {
                            if (el.value == $(el).attr('placeholder')) el.value = "";
                            return !!(self.booker.findVacation.kidsCount.val() >= 1);
                        },
                        range: [2, 18]
                    },
                    kid_age_2: {
                        required: function (el) {
                            if (el.value == $(el).attr('placeholder')) el.value = "";
                            return !!(self.booker.findVacation.kidsCount.val() >= 2);
                        },
                        range: [2, 18]
                    },
                    kid_age_3: {
                        required: function (el) {
                            if (el.value == $(el).attr('placeholder')) el.value = "";
                            return !!(self.booker.findVacation.kidsCount.val() >= 3);
                        },
                        range: [2, 18]
                    }
                },
                messages: {
                    vac_from_field: this.options.errors.departure,
                    vac_to_field: this.options.errors.destination,
                    vac_departure_field: {
                        required: this.options.errors.departDate,
                        date: this.options.errors.departDate
                    },
                    vac_return_field: {
                        required: this.options.errors.returnDate,
                        date: this.options.errors.returnDate
                    },
                    vac_adult_count: {
                        min: this.options.errors.minPassengers
                    },
                    kid_vacation: {
                        min: this.options.errors.minPassengers
                    },
                    kid_age_1: {
                        required: this.options.errors.kidsAge.replace(/{age}/gi, "1"),
                        range: this.options.errors.kidsAge.replace(/{age}/gi, "1")
                    },
                    kid_age_2: {
                        required: this.options.errors.kidsAge.replace(/{age}/gi, "2"),
                        range: this.options.errors.kidsAge.replace(/{age}/gi, "2")
                    },
                    kid_age_3: {
                        required: this.options.errors.kidsAge.replace(/{age}/gi, "3"),
                        range: this.options.errors.kidsAge.replace(/{age}/gi, "3")
                    }
                },
                showErrors: function (errorMap, errorList) {
                    self.booker.error.messages.html(""); // Clear Messages
                    if ($.isArray(errorList) && !!(errorList.length)) {
                        var errors = "";
                        for (var i = 0; i < errorList.length; i++) {
                            errors += errorList[i].message + "<br>";
                        }
                        self.booker.error.messages.append("<p>" + errors + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        self.booker.findVacation.find(".kid-age input[placeholder]").trigger("focusout"); // Hack for IE
                    }
                },
                invalidHandler: function (form, validator) {
                },
                submitHandler: function (form) {
                    var vacFrom = form['vac_from_field'].value.toUpperCase();
                    var vacTo = form['vac_to_field'].value.toUpperCase();


                    //This is defined in the booker-vacations.js and shouldnt be used 
                    //vacFrom is currently the label name and this code wasnt be executed - removing in MX 1.6

                    //                    if (vacFrom.length == 3) {
                    //                        var vacFromName = "";
                    //                        for (var i = 0; i < FlightOrigins.length; i++) {
                    //                            if (vacFrom == FlightOrigins[i][0]) {
                    //                                vacFromName = FlightOrigins[i][1];
                    //                                break;
                    //                            }
                    //                        }
                    //                        if (vacFromName == "") {
                    //                            self.booker.error.messages.html(""); // Clear Messages
                    //                            self.booker.error.messages.append("<p>" + self.options.errors.departure + "</p>");
                    //                            self.changeViewTo(self.booker.views.error);
                    //                            return false;
                    //                        } else {
                    //                            form['vac_from_field'].value = vacFromName;
                    //                        }
                    //                    }
                    //                    if (vacTo.length == 3) {
                    //                        var vacToName = "";
                    //                        for (var i = 0; i < VacationDests.length; i++) {
                    //                            if (vacTo == VacationDests[i][0]) {
                    //                                vacToName = VacationDests[i][1];
                    //                                break;
                    //                            }
                    //                        }
                    //                        if (vacToName == "") {
                    //                            self.booker.error.messages.html(""); // Clear Messages
                    //                            self.booker.error.messages.append("<p>" + self.options.errors.departure + "</p>");
                    //                            self.changeViewTo(self.booker.views.error);
                    //                            return false;
                    //                        } else {
                    //                            form['vac_to_field'].value = vacToName;
                    //                        }
                    //                    }


                    //set oVarFrom & oVarTo
                    var oVarFrom;
                    var oVarTo;

                    //get aiport codes for MAC cities without 3 letter aiport code
                    var match = /\w{3}(?=\)$)/;
                    if (!match.exec(vacFrom)) {
                        for (var j = 0; j < MAClist.length; j++) {

                            var macCity = window["o" + MAClist[j]];

                            if (macCity.name == vacFrom) {
                                oVarFrom = macCity;
                                break;
                            }
                        }
                    } else { oVarFrom = window['o' + (vacFrom.length > 3 ? vacFrom.substring(vacFrom.length - 4, vacFrom.length - 1) : vacFrom.toUpperCase())]; }

                    if (!match.exec(vacTo)) {
                        for (var j = 0; j < MAClist.length; j++) {

                            var macCity = window["o" + MAClist[j]];

                            if (macCity.name == vacTo) {
                                oVarTo = macCity;
                                break;
                            }
                        }
                    } else { oVarTo = window['o' + (vacTo.length > 3 ? vacTo.substring(vacTo.length - 4, vacTo.length - 1) : vacTo.toUpperCase())]; }


                    //Error message for extended return date
                    //28 refers to 4 weeks time span
                    $fvacationForm = document.getElementById("fvacation");

                    var depd = new Date(form['vac_departure_field'].value);
                    depd.setDate(depd.getDate() + 28);
                    var retd = new Date(form['vac_return_field'].value);
                    var isReturnDateisMonthPastDept = (retd > depd);


                    //A321 Entertainment Affected Check
                    var A321InflightAffected = false;
                    var inA321AffectedRoute = false;
                    var deptInA321AffectedDates = false;
                    var returnInA321AffectedDates = false;
                    var selectedDepartureDate = new Date(form['vac_departure_field'].value);
                    var selectedReturnDate = new Date(form['vac_return_field'].value);
                    var A321InflightAffectedMessage = "";

                    // @mx3.0-201477 
                    if (oVarFrom == undefined) {
                        self.booker.error.messages.html(""); // Clear Messages
                        self.booker.error.messages.append("<p>" + self.options.errors.departure + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        $('.ok-btn').click(function (e) {
                            self.booker.error.buttons.ok.find('span').text("OK");
                            self.booker.error.messages.html(""); // Clear Messages 
                        });
                        return false;
                    } else {
                        form['vac_from_field'].value = oVarFrom.name;
                    }
                    if (oVarTo == undefined) {
                        self.booker.error.messages.html(""); // Clear Messages
                        self.booker.error.messages.append("<p>" + self.options.errors.destination + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        $('.ok-btn').click(function (e) {
                            self.booker.error.buttons.ok.find('span').text("OK");
                            self.booker.error.messages.html(""); // Clear Messages  
                        });
                        return false;
                    } else {
                        form['vac_to_field'].value = oVarTo.name;
                    }
                    // @mx3.0 - defect end block   


                    $.each(A321InflightImpact, function (i, value) {

                        //Check Defined routes
                        $.each(A321InflightImpact[i][0], function (j, value) {
                            if (([oVarFrom.code, oVarTo.code]).toString() === (A321InflightImpact[i][0][j]).toString()) {

                                inA321AffectedRoute = true;
                            }
                        });

                        if (inA321AffectedRoute) {
                            if (selectedDepartureDate >= A321InflightImpact[i][1] && selectedDepartureDate <= A321InflightImpact[i][2]) {
                                deptInA321AffectedDates = true;
                            }
                            if (selectedReturnDate >= A321InflightImpact[i][1] && selectedReturnDate <= A321InflightImpact[i][2]) {
                                returnInA321AffectedDates = true;
                            }
                        }
                        if (inA321AffectedRoute && (deptInA321AffectedDates || returnInA321AffectedDates)) {

                            //Show message
                            A321InflightAffected = true;

                            var departureFlights = "";
                            var returnFlights = "";
                            //If departure date is affected add departing flights to message
                            if (deptInA321AffectedDates) {
                                departureFlights = A321InflightImpact[i][3].toString();
                            }
                            else {
                                departureFlights = "";
                            }
                            //if both dept and return are shown add the comma
                            if (returnInA321AffectedDates && deptInA321AffectedDates) {
                                returnFlights = ", " + A321InflightImpact[i][4].toString();
                            }
                            else if (returnInA321AffectedDates) {
                                returnFlights = A321InflightImpact[i][4].toString();
                            }
                            else {
                                returnFlights = "";
                            }

                            A321InflightAffectedMessage = GlobalErrors.VacationsBooker.A321InflightPrefix + departureFlights + returnFlights + GlobalErrors.VacationsBooker.A321InflightSuffix;



                            //Break out current each
                            return false;
                        }
                    });



                    if ((parseInt(form["vac_adult_count"].value) + parseInt(form["kid_vacation"].value)) > 7) {
                        self.booker.error.messages.html(""); // Clear Messages
                        self.booker.error.messages.append("<p>" + self.options.errors.maxPassengers + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        return false;
                    }
                    if ((parseInt(form["vac_infant_count"].value) > 3) || (parseInt(form["vac_infant_count"].value) > parseInt(form["vac_adult_count"].value))) {
                        self.booker.error.messages.html(""); // Clear Messages
                        self.booker.error.messages.append("<p>" + self.options.errors.maxInfants + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        return false;
                    }


                    var isSeasonalFlight = false;
                    var isdayOfWeekFlight = false;

                    // @mx3.0-201477 
                    if (oVarFrom == undefined) return false;
                    if (oVarTo == undefined) return false;

                    $.each(seasonalRoutes, function (index, value) {

                        if ($([oVarFrom.code, oVarTo.code]).not(seasonalRoutes[index]).length == 0 && $(seasonalRoutes[index]).not([oVarFrom.code, oVarTo.code]).length == 0) {
                            self.booker.error.messages.html(""); // Clear Messages                               
                            self.booker.error.messages.append("<p id='seasonalError'>" + self.options.errors.seasonalRouteError_vacations + "</p>");
                            if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='returnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                            if (A321InflightAffected) self.booker.error.messages.append("<p id='a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                            // @mx3.0.1-201478 
                            //self.booker.error.buttons.ok.removeClass('ok-btn'); // @Mx3.0.1 show continue.
                            // self.booker.error.buttons.ok.addClass('hideOKBtn');
                            self.booker.error.buttons.ok.find('span').text("Continue"); // @MX3.0.1 - text change
                            // $('.close-btn').hide(); // @mx3.0.1-2014710
                            self.changeViewTo(self.booker.views.error);
                            //  self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                            alignOkCancelErrorButtons();
                            jQuery('#oops').html('&nbsp;');
                            //Best Fare Finder URL + flight search parameters
                            //$('#seasonalRouteBFF').attr('href', "/bestfarefinder/?origin=" + oVarFrom.code + "&destination=" + oVarTo.code + "&trip=" + $fflightForm['journeySpan'].value + "&fare=" + $fflightForm['fareDisplay'].value + "&adult=" + form['adult_count'].value + "&kid=" + form['kid_count'].value + "&infant=" + form['infant_count'].value + "&departure=" + form['departure_field'].value);

                            self.booker.error.buttons.cancel.click(function (e) {
                                self.booker.error.buttons.ok.find('span').text("OK");
                                self.booker.error.messages.html(""); // Clear Messages 

                                self.booker.error.buttons.ok.removeClass('hideOKBtn'); // @MX3.0 - reset show OK button

                            });
                            $('.close-btn').click(function (e) {
                                self.booker.error.buttons.ok.find('span').text("OK");
                                self.booker.error.messages.html(""); // Clear Messages 
                                self.booker.error.buttons.ok.removeClass('hideOKBtn'); // @MX3.0 - reset show OK button

                            });

                            isSeasonalFlight = true;
                            return false;
                        }
                    });

                    if (!isSeasonalFlight) {
                        //day of week flight       
                        $.each(dayOfWeekRoutes, function (index, value) {

                            if ($([oVarFrom.code, oVarTo.code]).not(dayOfWeekRoutes[index]).length == 0 && $(dayOfWeekRoutes[index]).not([oVarFrom.code, oVarTo.code]).length == 0) {
                                self.booker.error.messages.html(""); // Clear Messages                                
                                self.booker.error.messages.append("<p  id='dayOfWeekError'>" + self.options.errors.dayOfWeekRouteError_vacations + "</p>");
                                if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='returnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                                if (A321InflightAffected) self.booker.error.messages.append("<p id='a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                                self.booker.error.buttons.ok.find('span').text("Continue");
                                self.changeViewTo(self.booker.views.error);
                                // self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                                alignOkCancelErrorButtons();
                                jQuery('#oops').html('&nbsp;');
                                //Best Fare Finder URL + flight search parameters
                                //$('#dayOfWeekRouteBFF').attr('href', "/bestfarefinder/?origin=" + oVarFrom.code + "&destination=" + oVarTo.code + "&trip=" + $fflightForm['journeySpan'].value + "&fare=" + $fflightForm['fareDisplay'].value + "&adult=" + form['adult_count'].value + "&kid=" + form['kid_count'].value + "&infant=" + form['infant_count'].value + "&departure=" + form['departure_field'].value);

                                self.booker.error.buttons.cancel.click(function (e) {
                                    self.booker.error.buttons.ok.find('span').text("OK");
                                    self.booker.error.messages.html(""); // Clear Messages 
                                });
                                $('.close-btn').click(function (e) {
                                    self.booker.error.buttons.ok.find('span').text("OK");
                                    self.booker.error.messages.html(""); // Clear Messages 
                                });

                                isdayOfWeekFlight = true;
                                return false;
                            }
                        });
                    }
                    // @mx 3.0 req. #285 - end-1

                    var isVacSameDay = form['vac_departure_field'].value == form['vac_return_field'].value;
                    var isVacToday = self.booker.findVacation.isWarned && !!!(self.booker.findVacation.isWarned.checked);
                    // @mx 3.0 req. #285 - change line below
                    if ((isVacSameDay || isVacToday) && (!isSeasonalFlight && !isdayOfWeekFlight)) {
                        // @mx 3.0 req. #285 - change line above
                        self.booker.error.messages.html(""); // Clear Messages
                        if (isVacSameDay) self.booker.error.messages.append("<p>" + self.options.errors.sameDay + "</p>");
                        if (isVacToday) self.booker.error.messages.append("<p>" + self.options.errors.todayWarning + "</p>");
                        if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='vac_ReturnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                        if (A321InflightAffected) self.booker.error.messages.append("<p id='vac_a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        // self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                        alignOkCancelErrorButtons();
                        jQuery('#oops').html('&nbsp;');
                        self.booker.error.buttons.ok.click(function (e) {
                            if (self.booker.findVacation.submitted) return false;
                            else self.booker.findVacation.submitted = true;
                            if (isVacToday) {
                                self.booker.findVacation.isWarned.checked = true;
                                $(form).find("input[type=text]").each(function (i, input) {
                                    if (input.value == $(this).attr('placeholder')) input.value = "";
                                });
                            }
                            self.booker.submitFV.submit(form);
                        });
                        return false;
                    }
                    // @mx 2.0 req. #285 - insert below
                    else if (isSeasonalFlight || isdayOfWeekFlight) {
                        return false;
                    }
                    // @mx 2.0 req. #285 - insert above
                    else if (isReturnDateisMonthPastDept) {
                        self.booker.error.messages.html(""); // Clear Messages
                        if (isReturnDateisMonthPastDept) self.booker.error.messages.append("<p id='vac_ReturnDateisMonthPastDeptError'>" + self.options.errors.returnMonthPastDepartureError + "</p>");
                        if (A321InflightAffected) self.booker.error.messages.append("<p id='vac_a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                        self.booker.error.buttons.ok.find('span').text("OK");
                        self.changeViewTo(self.booker.views.error);
                        // self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                        alignOkCancelErrorButtons();
                        jQuery('#oops').html('&nbsp;');

                        self.booker.error.buttons.cancel.click(function (e) {
                            self.booker.error.buttons.ok.find('span').text("OK");
                            self.booker.error.messages.html(""); // Clear Messages 
                        });
                        $('.close-btn').click(function (e) {
                            self.booker.error.buttons.ok.find('span').text("OK");
                            self.booker.error.messages.html(""); // Clear Messages 
                        });
                        return false;
                    }
                    else if (A321InflightAffected) {
                        self.booker.error.messages.html(""); // Clear Messages
                        if (A321InflightAffected) self.booker.error.messages.append("<p id='vac_a321EntertainmentError'>" + A321InflightAffectedMessage + "</p>");
                        self.booker.error.buttons.ok.find('span').text("OK");
                        self.changeViewTo(self.booker.views.error);
                        //   self.booker.error.buttons.cancel.removeClass('hideCancelBtn');
                        alignOkCancelErrorButtons();
                        jQuery('#oops').html('&nbsp;');

                        self.booker.error.buttons.cancel.click(function (e) {
                            self.booker.error.buttons.ok.find('span').text("OK");
                            self.booker.error.messages.html(""); // Clear Messages 
                        });
                        $('.close-btn').click(function (e) {
                            self.booker.error.buttons.ok.find('span').text("OK");
                            self.booker.error.messages.html(""); // Clear Messages 
                        });
                        return false;
                    }
                    else {
                        $(form).find("input[type=text]").each(function (i, input) {
                            if (input.value == $(this).attr('placeholder')) input.value = "";
                        });
                        self.booker.submitFV.submit(form);
                    }
                }
            });

            // Add Checkin Confirmation Help Modal
            this.booker.modals.confirmationHelp = new JB.Class.Modal({
                url: "http://www.jetblue.com/partials/pnr-example.html",
                openTogglers: this.booker.checkin.find("span.lrg-rnd-qmark")
            });

            // Flight Checkin Form Submit Handle
            this.booker.checkin.validator = this.booker.checkin.validate({
                onfocusout: false,
                onkeyup: false,
                onclick: false,
                focusInvalid: false,
                rules: {
                    confirm_field: {
                        checkPlaceholderMatch: true,
                        required: true,
                        minlength: 6,
                        maxlength: 7,
                        alphanumeric : true
                    },
                    depart_city_field: {
                        checkPlaceholderMatch: true,
                        required: true
                    },
                    first_name_field: {
                        checkPlaceholderMatch: true,
                        required: true
                    },
                    last_name_field: {
                        checkPlaceholderMatch: true,
                        required: true
                    }
                },
                messages: {
                    confirm_field: {
                        checkPlaceholderMatch: this.options.errors.confirmationNumber,
                        required: this.options.errors.confirmationNumber,
                        minlength: this.options.errors.confirmationNumber,
                        maxlength: this.options.errors.confirmationNumber,
                        alphanumeric: this.options.errors.confirmationNumber
                    },
                    depart_city_field: {
                        checkPlaceholderMatch: this.options.errors.departure,
                        required: this.options.errors.departure
                    },
                    first_name_field: {
                        checkPlaceholderMatch: this.options.errors.firstName,
                        required: this.options.errors.firstName
                    },
                    last_name_field: {
                        checkPlaceholderMatch: this.options.errors.lastName,
                        required: this.options.errors.lastName
                    }
                },
                showErrors: function (errorMap, errorList) {
                    self.booker.error.messages.html(""); // Clear Messages
                    if ($.isArray(errorList) && !!(errorList.length)) {
                        var errors = "";
                        for (var i = 0; i < errorList.length; i++) {
                            errors += errorList[i].message + "<br>";
                        }
                        self.booker.error.messages.append("<p>" + errors + "</p>");
                        var stateHistory = self.booker.stateHistory;
                        self.changeViewTo(self.booker.views.error, function () {
                            self.booker.views.isError = true;
                            self.booker.stateHistory = stateHistory;
                        });

                    }
                },
                invalidHandler: function (form, validator) {
                },
                submitHandler: function (form) {
                    // Reset to default values
                    // $(form).find("input[type=text]").each(function (i, input) {
                    //     if(input.value == $(this).attr('placeholder')) input.value = "";
                    // });
                    $fcheckinForm = document.getElementById("fcheckin");
                    var fcheckinAction = $fcheckinForm.action + "?";
                    fcheckinAction += "recordLocator=" + form['confirm_field'].value;
                    var origin = form['depart_city_field'].value;
                    var oOrigin = window['o' + (origin.length > 3 ? origin.substring(origin.length - 4, origin.length - 1) : origin.toUpperCase())];
                    if (oOrigin == undefined) {
                        self.booker.error.messages.html(""); // Clear Messages
                        self.booker.error.messages.append("<p>" + self.options.errors.departure + "</p>");
                        self.changeViewTo(self.booker.views.error);
                        return false;
                    } else {
                        form['depart_city_field'].value = oOrigin.name;
                        fcheckinAction += "&departureCity=" + oOrigin.code;
                    }
                    fcheckinAction += "&firstName=" + form['first_name_field'].value;
                    fcheckinAction += "&lastName=" + form['last_name_field'].value;
                    $fcheckinForm.action = fcheckinAction;

                    form.submit();
                }
            });

            // Flight Status Form Submit Handle
            this.booker.flightStatus.validator = this.booker.flightStatus.validate({
                onfocusout: false,
                onkeyup: false,
                onclick: false,
                focusInvalid: false,
                rules: {
                    fs_from_field: {
                        required: function (el) {
                            if ($('input[name="FlightStatusType"]').val() == "byCities") {
                                return !self.booker.flightStatus[0]["fs_from_field"].value ? true : false;
                            }
                            return false;
                        }
                    },
                    fs_to_field: {
                        required: function (el) {
                            if ($('input[name="FlightStatusType"]').val() == "byCities") {
                                return !self.booker.flightStatus[0]["fs_to_field"].value ? true : false;
                            }
                            return false;
                        }
                    },
                    flight_number_field: {
                        required: function (el) {
                            if ($('input[name="FlightStatusType"]').val() == "byFlightNumber") {
                                if (el.value === el.defaultValue) el.value = "";
                                return !el.value ? true : false;
                            }
                            else {
                                if (el.value === el.defaultValue) el.value = "";
                                return false;
                            }
                        },
                        minlength: 1,
                        maxlength: 4
                    }
                },
                messages: {
                    fs_from_field: {
                        required: this.options.errors.departure
                    },
                    fs_to_field: {
                        required: this.options.errors.destination
                    },
                    flight_number_field: {
                        required: this.options.errors.flightNumber,
                        minlength: this.options.errors.flightNumber,
                        maxlength: this.options.errors.flightNumber
                    }
                },
                showErrors: function (errorMap, errorList) {
                    self.booker.error.messages.html(""); // Clear Messages
                    if ($.isArray(errorList) && !!(errorList.length)) {
                        var errors = "";
                        for (var i = 0; i < errorList.length; i++) {
                            errors += errorList[i].message + "<br>";
                        }
                        self.booker.error.messages.append("<p>" + errors + "</p>");



                        var stateHistory = self.booker.stateHistory;
                        self.changeViewTo(self.booker.views.error, function () {
                            self.booker.views.isError = true;
                            self.booker.stateHistory = stateHistory;
                        });

                    }
                },
                invalidHandler: function (form, validator) {
                },
                submitHandler: function (form) {
                    $ffstatusForm = document.getElementById("ffstatus");


                    if ($ffstatusForm['FlightStatusType'].value == "byCities") {
                        var fromValue = form['fs_from_field'].value;
                        $ffstatusForm['FlightOrgn'].value = (fromValue.length > 3 ? fromValue.substring(fromValue.length - 4, fromValue.length - 1) : fromValue.toUpperCase());
                        var oFromVar = window['o' + $ffstatusForm['FlightOrgn'].value];
                        if (oFromVar == undefined) {
                            self.booker.error.messages.html(""); // Clear Messages
                            self.booker.error.messages.append("<p>" + self.options.errors.departure + "</p>");
                            self.changeViewTo(self.booker.views.error);
                            return false;
                        } else {
                            form['fs_from_field'].value = oFromVar.name;
                            //$ffstatusForm['panInput_3'].value = oFromVar.name;
                        }
                        var toValue = form['fs_to_field'].value;
                        $ffstatusForm['FlightDestn'].value = (toValue.length > 3 ? toValue.substring(toValue.length - 4, toValue.length - 1) : toValue.toUpperCase());
                        var oToVar = window['o' + $ffstatusForm['FlightDestn'].value];
                        if (oToVar == undefined) {
                            self.booker.error.messages.html(""); // Clear Messages
                            self.booker.error.messages.append("<p>" + self.options.errors.destination + "</p>");
                            self.changeViewTo(self.booker.views.error);
                            return false;
                        } else {
                            form['fs_to_field'].value = oToVar.name;
                            // $ffstatusForm['panInput_4'].value = oToVar.name;
                        }
                        form['flight_number_field'].value = "";
                        $ffstatusForm['FlightNum'].value = "";
                    }
                    else {
                        $ffstatusForm['FlightNum'].value = form['flight_number_field'].value;
                    }

                    $ffstatusForm['FlightDate'].value = $ffstatusForm['FlightDate'].value;


                    form.submit();
                }
            });
        }
    },
    /**
    * changeViewTo switches content display to the passed in state.
    * @param {String} state The state to change to.
    * @param {Function} [callback] Optional callback function. 
    */
    changeViewTo: function (state, callback) {
        if (state == "error") {
            jQuery('#oops').html('Oops!');
            this.booker.error.buttons.cancel.addClass('hideCancelBtn');
            alignOkCancelErrorButtons();

        }
        this.booker.removeClass();
        this.booker.addClass("booker col4 " + state);
        this.booker.stateHistory = {
            previous: this.booker.stateHistory.current,
            current: state
        };
        if ($.isFunction(callback)) callback(this);
        return this.booker.stateHistory;
    },
    /**
    * filterAirports Filters out airport by code in array and returns new array.
    * @param {Array} state The state to change to.
    * @param {String} code Airports to filter.
    */
    filterAirports: function (array, code) {
        var escapeRegex = function (value) {
            return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        };
        var filter = function (array, code) {
            var matcher = new RegExp(escapeRegex(code), "i");
            return $.grep(array, function (value) {
                if (!!!(value.duplicate)) return matcher.test(value.routes);
            });
        };
        return filter(array, code);
    },
    /**
    * filterDestination Filters out vacation destinations by code in array and returns new array.
    * @param {Array} state The state to change to.
    * @param {String} code Flight Origin to filter.
    */
    filterDestination: function (array, code) {
        var escapeRegex = function (value) {
            return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        };
        var filter = function (array, code) {
            var matcher = new RegExp(escapeRegex(code), "i");
            return $.grep(array, function (value) {
                return matcher.test(value.origins);
            });
        };
        return filter(array, code);
    }

});

$(document).ready(function () {
    if ($('#flexible').length == 0 && $('#bookerHorizontalLine').length == 0) {
        // flexible checkbox does not exist. change Fare type top margin
        $('.fare-type').addClass('noFlexDateCheckbox');
        $('#fvacation .fare-type').removeClass('noFlexDateCheckbox');
        $('#booker-mini .fare-type').removeClass('noFlexDateCheckbox');
        $('#booker-mini .submit-btn-wrap').addClass('noFlexDateCheckbox');
    }
    $('#confirm_field').attr('maxlength', 7).bind("change paste", function(e){ console.log(e); var input = e.target.value+""; if( input != $(this).attr('placeholder') ){ $(this).val(input.slice(0,$(this).attr('maxlength'))) } });
});
