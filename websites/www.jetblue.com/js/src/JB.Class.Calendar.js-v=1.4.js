/**
 * Calendar Base Class.
 * @function JB.Class.Calendar
 * @param {String} container The id or class selector of the input element.
 * @require
 */

JB.Class.Calendar = JB.Class.extend({
    /**
    * Library dependencies
    * @type object
    */
    require: {
        'jQuery.ui.datepicker': 'http://www.jetblue.com/js/src/vendor/jquery-ui-1.8.16.custom.min'
    },
    /**
    * @constructor
    */
    init: function (container, options) {
        /**
        * Options
        */
        this.options = {
            today: new Date(),
            end: new Date(2012, 3, 30),
            fullDay: 1000 * 60 * 60 * 24,
            calDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
            months: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'June', 'July', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'],
            days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            calendar: '#calendar-wrap',
            departCal: '#depart-cal',
            returnCal: '#return-cal',
            departWrap: '.depart-wrap',
            returnWrap: '.return-wrap',
            departBtn: '.depart-btn',
            returnBtn: '.return-btn',
            departField: 'input',
            returnField: 'input',
            dateSpan: 'http://www.jetblue.com/js/src/span.date',
            placeholder: 'span.placeholder'
        };
        $.extend(this.options, options || {});
        this.container = $(container);
        this._super();
    },
    /** @ignore */
    _build: function () {
        var self = this;

        // store the calendars
        this.calendar = this.container.find(this.options.calendar);
        this.calendar.departCal = this.calendar.find(this.options.departCal);
        this.calendar.returnCal = this.calendar.find(this.options.returnCal);
        this.calendar.helper = this.calendar.children('.helper');

        // store the calendar triggers elements
        this.departWrap = this.container.find(this.options.departWrap);
        this.departBtn = this.departWrap.find(this.options.departBtn);
        this.departText = this.departWrap.find(this.options.placeholder);
        this.departField = this.departWrap.find(this.options.departField);
        this.returnWrap = this.container.find(this.options.returnWrap);
        this.returnBtn = this.returnWrap.find(this.options.returnBtn);
        this.returnText = this.returnWrap.find(this.options.placeholder);
        this.returnField = this.returnWrap.find(this.options.returnField);

        // store the date fields inside of calendar
        this.currentDate = this.calendar.find(this.options.dateSpan + ':first');
        this.endDate = this.calendar.find(this.options.dateSpan + ':last');
        this.options.end.setMonth(this.options.end.getMonth() - 1); //balance month offset in Rokkan code for schedule ext

        // build out today & end dates in a design-ready & human-readable format
        this.dates = {
            today: this.options.days[this.options.today.getDay()] + ', ' +
						 this.options.months[this.options.today.getMonth()] + ' ' +
						 (this.options.today.getDate() > 9 ? this.options.today.getDate() : '0' + this.options.today.getDate()),
            end: this.options.months[this.options.end.getMonth()] + ' ' +
					 (this.options.end.getDate() > 9 ? this.options.end.getDate() : '0' + this.options.end.getDate()) + ', ' +
					 this.options.end.getFullYear()
        };
        // set today & end date in the calendar before displaying it.
        this.currentDate.text(this.dates.today);
        var booker_dates = new Date(scheduleExtDate.year, scheduleExtDate.month - 1, scheduleExtDate.day)
        var formatted_date = this.options.months[booker_dates.getMonth()] + ' ' + booker_dates.getDate() + ', ' + scheduleExtDate.year;
        var which_end_date = (typeof WISHLIST !== 'undefined') ? formatted_date : this.dates.end;
        this.endDate.text(which_end_date);
        // create the durationDates array
        this.durationDates = [];
        // store a day's worth of milliseconds for calculating the number of days
        this.fullDay = this.options.fullDay;

        // set the default values for the datepicker
        $.datepicker.setDefaults({
            numberOfMonths: 1,
            gotoCurrent: true,
            dayNamesMin: this.options.calDays,
            dateFormat: 'D, M dd, yy',
            minDate: this.options.today,
            maxDate: this.options.end,
            hideIfNoPrevNext: true,
            beforeShowDay: $.proxy(this.beforePick, this),
            onSelect: $.proxy(this.afterPick, this)
        });
        // bind the datepicker to the #depart-cal container
        this.calendar.departCal.datepicker({
            altField: this.departField
        });

        // bind the datepicker to the #return-cal container
        this.calendar.returnCal.datepicker({
            altField: this.returnField
        });

        this.calendar.departCal.datepicker('setDate', null);
        this.calendar.returnCal.datepicker('setDate', null);

        //Keeping this flag since it seems that other applications are referencing it
        this.departClickedAlready = false;

        // bind the click & focus events to the necessary targets
        this.calendar.helper.bind('click', function (e) {
            e.stopPropagation();
            self.displayCalendar(e);
            self.departClickedAlready = false;
        });
        
        if (this.returnWrap.length === 0) {
            $('#flightDate')
                .bind('focus', function (e) {    
                    e.stopPropagation();
                    self.departClickedAlready = true;
                    self.departWrap.trigger('click');
            });
        }
       
        this.departWrap.bind('click', function (e) {
            e.stopPropagation();

            if (!self.departClickedAlready) {
                self.departClickedAlready = true;
            } else {
                self.departClickedAlready = false;
            }
        
            self.displayCalendar(e);

        });

        this.returnWrap.bind('click', function (e) {
            e.stopPropagation();
            self.displayCalendar(e);
            self.departClickedAlready = false;
        });

        this.departBtn.bind('click', function (e) {
            e.stopPropagation();

            if (self.departBtn.hasClass('active')) {
                self.closeCalendar(e, self);
            } else {
                self.displayCalendar(e);
                self.departClickedAlready = false;
            } 
        });
        
        this.returnBtn.bind('click', function (e) {
            e.stopPropagation();
            
            if (self.returnBtn.hasClass('active')){
                self.closeCalendar(e, self);
            } else {
               if (!self.departClickedAlready) {
                    self.departClickedAlready = true;
                } else {
                    self.departClickedAlready = false;
                }
                self.displayCalendar(e);
            }
        });
    },
    beforePick: function (date) {

        // compare departure date to date and add departure-date class
        if (this.departDate) {
            if (date.toDateString() == this.departDate.toDateString()) {
                return [true, 'departure-date'];
            }
        }
        // compare return date to date and add return-date class
        if (this.returnDate) {
            if (date.toDateString() == this.returnDate.toDateString()) {
                return [true, 'return-date'];
            }
        }
        // find date in durationDates array and add duration-date class
        if ($.inArray(date.toDateString(), this.durationDates) > -1) {
            return [true, 'duration-date'];
        }
        // all other dates have no class
        return [true, ''];
    },
    afterPick: function (dateText, inst) {
        
        var id = inst.id.substr(0, 6),
				cal = id + 'Cal',
				field = id + 'Field',
				placeholder = id + 'Text',
				calType = this.calendar.attr('class');


        // clear the duration dates for recalculation
        this.durationDates = [];
        if (id === 'depart') {
            // store the departure date for later
            this.departDate = new Date(dateText);
            // store the departure date time since 01/01/1970 for later
            this.departureTime = this.departDate.getTime();
            // set the minimum date for the Return calendar
            this.calendar.returnCal.datepicker('option', 'minDate', dateText);
            // set depart back to default state
            $("body").find(".depart-btn").removeClass("active");
            $("body").find(".depart-wrap").removeClass("active");

            //set return to orange active state
            $("body").find(".return-btn").addClass("active");
            $("body").find(".return-wrap").addClass("active");

        } else if (id === 'return') {
            // store the return date for later
            this.returnDate = new Date(dateText);
            // store the return date time since 01/01/1970 for later
            this.returnTime = this.returnDate.getTime();
            //set return icon back to default
            $("body").find(".return-btn").removeClass("active");
            $("body").find(".return-wrap").removeClass("active");
        }
        // check if the departure and return date are set
        if (this.departDate && this.returnDate) {
            // check if the departure and return times are set
            if (this.departureTime && this.returnTime) {
                // find the difference in days between the return date and the departure date
                var durationDays = Math.ceil((this.returnTime - this.departureTime) / this.fullDay);
                // if (window.console){ console.log("durationDays: ", durationDays); }
                if (durationDays > 0) { // there are days between the departure and return date
                    for (var i = 1; i < durationDays; i++) {
                        this.durationDates.push(new Date(this.departDate.getFullYear(), this.departDate.getMonth(), this.departDate.getDate() + i).toDateString());
                    }
                } else { // a departure date later than the return date has been chosen
                    // so, reset the return & duration dates
                    this.returnDate = undefined;
                    this.durationDates = [];
                    this.calendar.returnCal.datepicker('setDate', null);
                    this.returnText.text('Return date');
                }
            }
        }
        // set the placeholder to the value of the hidden input
        this[placeholder].text(dateText).siblings('input').attr('value', dateText);
        // refresh both calendars
        this.calendar.departCal.datepicker('refresh');
        this.calendar.returnCal.datepicker('refresh');


        if (id === 'depart' 
            && !!!(this.returnWrap.hasClass('disabled')) 
            && this.returnWrap.length > 0) {
            this.calendar.attr('class', 'return');
        } else if (id === 'return') {
            // this.calendar.attr('class', 'depart');
            // hide the calendar
            this.calendar.hide();
        } else {
            this.calendar.hide();
        }

    },
    displayCalendar: function (e) {

        // refresh both calendars
        // this.calendar.departCal.datepicker('option', 'minDate', new Date());
        // this.calendar.departCal.datepicker('refresh');
        // this.calendar.returnCal.datepicker('refresh');
        e.stopPropagation();

        var self = this
            , $body = $('body'); //Let's Cache the body element since it's going to be use alot.

        // fix this hack!
        $('.recent-search-dropdown').removeClass('active');
        // fix this hack!

        self.calendar
            .find('.jump-month-dropdown')
            .removeClass('active');

        // grab target's class
        if ($(e.currentTarget).hasClass("disabled")) return;

        var $el = $(e.target),
				elClass = $el.attr('class').substr(0, 6),
				type = ['depart', 'return'],
				version = 0;

        if ($.inArray(elClass, type) === -1) {
            elClass = $el.parent().attr('class').substr(0, 6);

            if (elClass == "depart" && !this.calendar.is(':hidden') && $(this.options.calendar).closest('#booker-mini').length) {
                elClass = $el.parent().attr('class').substr(0, -6);
            }
        }

        if (elClass === type[version]) {
            version = 1;
        }

        if (this.calendar.is(':hidden')) {
            this.calendar.attr('class', elClass).show();

            $(document).bind('click', $.proxy(this.offTargetTrigger, this));

        } else {
            if (($(e.currentTarget).attr("class").substr(0, 6) == "helper") ||
                   ($(this.options.calendar).closest('#booker-mini').length)) {
                this.calendar.attr('class', type[version]);
            }

            $(document).bind('click', $.proxy(this.offTargetTrigger, this));
        }

        //make active calendar orange. has been opened once
        if (self.calendarType != undefined) {
            //has been opened once and closed
            if ($(e.currentTarget).attr("class").substr(0, 6) != "helper") {
                var _on = $(e.currentTarget).attr("class").substr(0, 6)
                    , _off = (_on === 'depart') ? 'return' : 'depart';

                self.calendarType = (_on === 'depart') ? 0 : 1;

                $body.find("." + _on + "-btn," 
                        + " ." + _on + "-wrap")
                    .addClass("active");
                
                $body.find("." + _off + "-btn, " 
                        + "." + _off + "-wrap")
                    .removeClass("active");

                self.calendar
                    .removeClass(_off)
                    .addClass(_on);

                //already open and has been set
            } else {

                if (self.calendarType == 1) {

                    $body.find(".depart-btn").addClass("active");
                    $body.find(".depart-wrap").addClass("active");

                    $body.find(".return-btn").removeClass("active");
                    $body.find(".return-wrap").removeClass("active");

                    self.calendarType = 0;
                } else {

                    if (!$body.find(".return-btn").hasClass("active")) {
                        self.calendarType = 1;

                        $body.find(".return-btn").addClass("active");
                        $body.find(".return-wrap").addClass("active");

                        $body.find(".depart-btn").removeClass("active");
                        $body.find(".depart-wrap").removeClass("active");
                    } else {
                        self.calendarType = 0;

                        $body.find(".depart-btn").addClass("active");
                        $body.find(".depart-wrap").addClass("active");

                        $body.find(".return-btn").removeClass("active");
                        $body.find(".return-wrap").removeClass("active");
                    }
                }
            }
            //first load
        } else {
            if ($(e.currentTarget).attr("class").substr(0, 6) == "depart") {
                self.calendarType = 0

                $body.find(".depart-btn").addClass("active");
                $body.find(".depart-wrap").addClass("active");

            } else if ($(e.currentTarget).attr("class").substr(0, 6) == "return") {
                self.calendarType = 1

                $body.find(".return-btn").addClass("active");
                $body.find(".return-wrap").addClass("active");
            }
        }

        this.departClickedAlready = false;

    },
    offTargetTrigger: function (e) {
        e.stopPropagation();

        var t = e.target,
				$t = $(e.target),
				nodeName = t.nodeName.toLowerCase(),
				closestClass = '.' + this.calendar.attr('class').split(' '),
				calNavClasses = ['ui-icon-circle-triangle-e', 'ui-icon-circle-triangle-w', 'ui-datepicker-next', 'ui-datepicker-prev', 'ui-state-default', 'ui-state-highlight', 'ui-state-active', 'ui-state-hover', 'active'],
				thisCalEl = this.calendar[0];

        // fix this hack
        var type = $t.parent().attr('class');
        if (type === 'recent-search-dropdown') {
            this.calendar.hide();
            self.calendarType = 0

            //change depart-btn and return-btn calendar icon default
            $("body").find(".depart-btn").removeClass("active");
            $("body").find(".depart-wrap").removeClass("active");

            $("body").find(".return-btn").removeClass("active");
            $("body").find(".return-wrap").removeClass("active");

            $(document).unbind('click', $.proxy(this.offTargetTrigger, this));
            return;
        }
        // fix this hack

        if (nodeName === 'a' || nodeName === 'span') {
            var result = false;
            for (var i = 0; i < calNavClasses.length; i++) {
                if (result) { break; }
                result = $t.hasClass(calNavClasses[i]);
            }
        }

        if (!!$t.parents(closestClass).length || $t.hasClass('booker') || result || t == thisCalEl) {
            return;
        }

        this.calendar.hide();
        self.calendarType = 0

        //make depart-btn & return-btn default
        $("body").find(".depart-btn").removeClass("active");
        $("body").find(".depart-wrap").removeClass("active");

        $("body").find(".return-btn").removeClass("active");
        $("body").find(".return-wrap").removeClass("active");

        this.departClickedAlready = false;

        $(document).unbind('click', $.proxy(this.offTargetTrigger, this));
    },
    closeCalendar: function(e, self) {
            e.stopPropagation();

            self.calendar.hide();
            self.calendarType = 0
            // change depart-btn default 
            $("body").find(".depart-btn").removeClass("active");
            $("body").find(".depart-wrap").removeClass("active");

            $("body").find(".return-btn").removeClass("active");
            $("body").find(".return-wrap").removeClass("active");

            self.departClickedAlready = false;
    }
});

/* Arnaldo Capo MX 1.8
* Centralizing functions
*/
jQuery(document).ready(function($){
    if ($('#wl_top').length === 0
            && $('.sulandingPage').length === 0)
        calendarPopulateJumpToMonth(null);
});

function calendarJumpToMonth(monthSelection) {
    var value = monthSelection.options[monthSelection.selectedIndex].value; 

    var year = value.split(' ')[1];
    var month = value.split(' ')[0];

    var monthList = {
        January: 0,
        February: 1,
        March: 2,
        April: 3,
        May: 4,
        June: 5,
        July:6,
        August: 7,
        September: 8,
        October: 9,
        November: 10,
        December: 11
    };

    var minDate = new Date($("#depart-cal").datepicker("option", "minDate"));
    var maxDate = new Date($("#depart-cal").datepicker("option", "maxDate"));
    
    var jumpDate = new Date(year,monthList[month],01);

    if(monthSelection.selectedIndex < monthSelection.length) {
        $('#depart-cal').datepicker('setDate', jumpDate);
        $('#return-cal').datepicker('setDate', jumpDate);
    }
}
function calendarPopulateJumpToMonth(options) {
    //add months to drop down menu <option value="Month-Year">Month-Year</option>
    var startDate = new Date();
    //make start date be day 1
    
    var endDate = new Date(scheduleExtDate.year, scheduleExtDate.month - 1, scheduleExtDate.day);
    if (options) {
        startDate = new Date(options.startDate);
        endDate = new Date(options.endDate);
    }
    startDate.setDate(1);
    var $select = $("#jump_months");
    var $oList = $("#jumpMonthOList");
    var selectText = "";

    //Let's stop this script if the dom element does NOT exists
    if ($select.length === 0 || $oList === 0)
        return;

    // Removing new Array and using brackets
    // More information about [] vs new Array (http://bit.ly/1c0Y4ff)
    var monthList = [];
    monthList[0] = "January";
    monthList[1] = "February";
    monthList[2] = "March";
    monthList[3] = "April";
    monthList[4] = "May";
    monthList[5] = "June";
    monthList[6] = "July";
    monthList[7] = "August";
    monthList[8] = "September";
    monthList[9] = "October";
    monthList[10] = "November";
    monthList[11] = "December";

    var count = 0;
    while (startDate < endDate || 
            (startDate.getMonth() == endDate.getMonth() && 
                startDate.getDate() > endDate.getDate())) {

        selectText = String(monthList[startDate.getMonth()] + " " + startDate.getFullYear());
        var option = "<option value='"+ String(selectText)+"'>"+String(selectText)+"</option>";

        if (count == 0) {
            //default value should be the 3rd month -- Not anymore with MX 1.8
            
            /*  Arnaldo's Notes 
             *  Since calendars are now only one.  It should be the next month
            */

            var defaultSelectDate = new Date(new Date(startDate).setMonth(startDate.getMonth() + 1));
            var defaultSelection = monthList[defaultSelectDate.getMonth()] + " " + defaultSelectDate.getFullYear();
            $('#dd-month').text(defaultSelection);
            count++;
        }

        $select.append(option);
        $oList.append("<li>" + String(selectText) + "</li>");
        startDate = new Date(new Date(startDate).setMonth(startDate.getMonth() + 1));
    }
}