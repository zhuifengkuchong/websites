/**
 * TrueBlue Base Class.
 * @function JB.Class.TrueBlue
 * @param {String} container The id or class selector of the input element.
 * @require
 */

//Global for cookie path
jbRememberEmailCookiePath = location.pathname.replace(/\/[^\/]+$/, '/');

JB.Class.TrueBlue = JB.Class.extend({
    /**
    * Library dependencies
    * @type object
    */
    require: {
    },
    /**
    * @constructor
    */
    init: function (container, options) {
        /**
        * Options
        */
        this.options = {
        };
        $.extend(this.options, options || {});
        this.container = $(container);
        this._super();
    },
    /** @ignore */
    _build: function () {

        var self = this;

        this.trueblue = this.container;
        this.trueblue.form = this.trueblue.find('#true-blue-form');
        this.trueblue.dropdown = this.trueblue.find('#trueblue-dropdown');
        this.againBtn = this.trueblue.find('.again-btn');
        this.closeBtn = this.trueblue.find('.trueblue-switch .close-btn');
        this.signoutBtn = this.trueblue.find('.trueblue-switch .signout');
        this.cancelBtn = this.trueblue.find('#confirmation-inner .cancel-btn');

        // bind againBtn click handler
        this.againBtn.bind('click', $.proxy(this.closeModal, this));
        // bind closeBtn click handler
        this.closeBtn.bind('click', $.proxy(this.closeModal, this));
        // bind signoutBtn click handler
        this.signoutBtn.bind('click', function () { $('#trueblue #main-inner').fadeOut('fast', function () { $('#trueblue #confirmation-inner').fadeIn('fast'); }); });
        // bind signoutBtn click handler
        this.cancelBtn.bind('click', function () { $('#trueblue #confirmation-inner').fadeOut('fast', function () { $('#trueblue #main-inner').fadeIn('fast'); }); });

        // beautify the trueblue dropdown
        this.trueblue.dropdown.beautifier();

        // bind submit handler with validation
        this.trueblue.form.bind('submit', function (e) {
            e.preventDefault();
            var response = $(this).validator();

            if (response) {
                var errorMessage = '';
                for (i = 0; i < response.length; i++) {
                    errorMessage += '<p>' + response[i] + '</p>';
                }
                self.trueblue.addClass('error').find('.error-message').html(errorMessage);
            } else {
                return true;
            }
        });


        // call fieldFlip()
        this.fieldFlip();
    },
    closeModal: function (e) {
        this.trueblue.removeClass('error system');
    },
    fieldFlip: function () {
        // hide/show on load
        jQuery('#password-place').addClass('showPassPlaceholder');

        // Find And Replace
        jQuery('#password-place').bind('focus', function (e) {
            jQuery(this).removeClass('showPassPlaceholder');
            jQuery('#password_field').focus();
        });

        jQuery('#password_field').bind('focus', function (e) {
            // alert('yo');
            jQuery('#password-place').removeClass('showPassPlaceholder');
        });

        // Find And Replace	In Reverse
        jQuery('#password_field').bind('blur', function (e) {
            if (jQuery(this).val() === '') {
                jQuery('#password-place').addClass('showPassPlaceholder');
            }
        });
    }
});



    //remember me check box
$(".remembermeCheckbox").ready(function () {
    var target = this;
    checkRememberEmail();
    if (!$('#rememberme').is(':checked')) {
        $('.remembermeCheckbox').removeClass('rememberme_active');
    } else {
        $('.remembermeCheckbox').addClass('rememberme_active');
    }
});


    $(".remembermeCheckbox").click(function () {
        var target = this;
        if ($(target).hasClass('rememberme_active')) {
            $('#rememberme').attr('checked', false);
            $('.remembermeCheckbox').removeClass('rememberme_active');
        } else {
            $('#rememberme').attr('checked', true);
            $('.remembermeCheckbox').addClass('rememberme_active');
        }
    });



function checkRememberEmail() {


    //If we find a cookie for emailAddress checkbox and place value
    if (($.cookie('jbRememberEmail')) != null) {
        $('#email_field').val(($.cookie('jbRememberEmail')));
        $('#rememberme').prop('checked', true);
    }
    else {

        $('#rememberme').prop('checked', false);
        $.removeCookie("jbRememberEmail", { path: jbRememberEmailCookiePath });
    }

}

function saveRememberMeCookie() {
    //Set cookie for remembering successful user if requested
    if ($('#rememberme').is(':checked')) {
        $.cookie("jbRememberEmail", $('#email_field').val(), { expires: 365, path: jbRememberEmailCookiePath });
    } else {
        $.removeCookie("jbRememberEmail", { path: jbRememberEmailCookiePath });
    }
}





