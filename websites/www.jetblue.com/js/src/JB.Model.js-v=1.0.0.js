/**
 * Models Cache Namespace.
 * Collection of cached data.
 * @namespace
 */

JB.Model.Cache = {};


/**
 * Gets a Script file that contains all the airport codes and generates an Array of airports available to the passed in country code.
 * @param {String} [url] Option URL to Request from.
 * @param {Function} callback Callback function.
 */

JB.Model.getAirports = function (url, callback) {
    var self = this;
    // Parameter Shift
    if ($.isFunction(url)) {
        var callback = url;
        var url = null;
    }
    if (!!JB.Model.Cache.airports && $.isFunction(callback)) {
        callback(JB.Model.Cache.airports);
        return;
    }
    var countries = {};
    $.map(aRegions, function (region) {
        if (!!region.countries) {
            for (var i = 0; i < region.countries.length; i++) {
                countries[region.countries[i]] = {
                    name: region.name,
                    code: region.code,
                    country: region.countries[i]
                }
            }
        }
    });
    var airportsCode = $.map(countries, function (country) {
        return $.extend(eval("c" + country.country).airports, eval("c" + country.country).airports);
    });
    // Sanatized Airports
    var airports = $.map(airportsCode, function (airport) {
        var airport = (typeof airport === "string") ? eval("o" + airport) : null;
        if (!airport) return;
        if (window["r" + airport.code] != undefined) {
            var routes = eval("r" + airport.code);
            return {
                label: airport.name,
                code: airport.code,
                jb: airport.jb,
                cc: airport.cc,
                duplicate: airport.duplicate == undefined ? false : true,
                country: eval("c" + airport.cc).name,
                region: countries[airport.cc],
                routes: routes
            };
        } else {
            return;
        }
    });
    // ASC sort airports by country name
    airports.sort(function (a, b) {
        var labelA = a.country.toLowerCase(), labelB = b.country.toLowerCase();
        if (labelA < labelB) return -1;
        if (labelA > labelB) return 1
        return 0;
    });
    var airportsByCountry = {};
    for (var i = 0; i < airports.length; i++) {
        if (!!!(airportsByCountry[airports[i].country])) airportsByCountry[airports[i].country] = [];
        airportsByCountry[airports[i].country].push(airports[i]);
    }
    var airports = [];
    for (country in airportsByCountry) {
        airportsByCountry[country].sort(function (a, b) {
            var labelA = a.label.toLowerCase(), labelB = b.label.toLowerCase();
            if (labelA < labelB) return -1;
            if (labelA > labelB) return 1
            return 0;
        });
        airports = airports.concat(airportsByCountry[country]);
    }
    if ($.isFunction(callback)) callback(airports);
    JB.Model.Cache.airports = airports; // Cache
};


/**
 * Gets a Script file that contains all the airport codes and generates an Array of airports available to the passed in country code.
 * @param {String} [url] Option URL to Request from.
 * @param {Function} callback Callback function.
 */

JB.Model.getVacations = function (url, callback) {
    var self = this;
    // Parameter Shift
    if ($.isFunction(url)) {
        var callback = url;
        var url = null;
    }

    //Special names for Vacations - different than the airways booker cities 
    var SpecialNames = [
    { "code": "PLS", "name": "Turks and Caicos (PLS)" }
];


    //add vacation specific special destinations
    var oAnaheim_LGB = { "code": "Anaheim_LGB", "name": "Anaheim / Disneyland Resort (LGB)", "cc": "US", "jb": true };
    var oCancun_Hotel_Zone_CUN = { "code": "Cancun_Hotel_Zone_CUN", "name": "Cancun / Hotel Zone (CUN)", "cc": "MX", "jb": true };
    var oCancun_Riviera_Maya_CUN = { "code": "Cancun_Riviera_Maya_CUN", "name": "Cancun / Riviera Maya (CUN)", "cc": "MX", "jb": true };
    var oWalt_Disney_World_MCO = { "code": "Walt_Disney_World_MCO", "name": "Orlando / Walt Disney World Resort (MCO)", "cc": "US", "jb": true };

    var rAnaheim_LGB = rLGB;
    var rCancun_Hotel_Zone_CUN = rCUN;
    var rCancun_Riviera_Maya_CUN = rCUN;
    var rWalt_Disney_World_MCO = rMCO;

    var SpecialUSDests = ["Anaheim_LGB", "Walt_Disney_World_MCO"];
    var SpecialMXDests = ["Cancun_Hotel_Zone_CUN", "Cancun_Riviera_Maya_CUN"];

    //Add these to the countries so they can get built into the airports
    cUS.airports.pop();
    cUS.airports = cUS.airports.concat(SpecialUSDests);
    cUS.airports.push(0);
    cMX.airports.pop();
    cMX.airports = cMX.airports.concat(SpecialMXDests);
    cMX.airports.push(0);

    //Add objects to window to be able to be parsed as airports
    window.oWalt_Disney_World_MCO = oWalt_Disney_World_MCO;
    window.rWalt_Disney_World_MCO = rWalt_Disney_World_MCO;

    window.oAnaheim_LGB = oAnaheim_LGB;
    window.rAnaheim_LGB = rAnaheim_LGB;

    window.oCancun_Hotel_Zone_CUN = oCancun_Hotel_Zone_CUN;
    window.rCancun_Hotel_Zone_CUN = rCancun_Hotel_Zone_CUN;

    window.oCancun_Riviera_Maya_CUN = oCancun_Riviera_Maya_CUN;
    window.rCancun_Riviera_Maya_CUN = rCancun_Riviera_Maya_CUN;



    if (!!JB.Model.Cache.vacations && $.isFunction(callback)) {
        callback(JB.Model.Cache.vacations);
        return;
    }

    //Begin building vacation airports

    var countries = {};
    $.map(aRegions, function (region) {
        if (!!region.countries) {
            for (var i = 0; i < region.countries.length; i++) {
                countries[region.countries[i]] = {
                    name: region.name,
                    code: region.code,
                    country: region.countries[i]
                }
            }
        }
    });


    var airportsCode = $.map(countries, function (country) {
        return $.extend(window["c" + country.country].airports, window["c" + country.country].airports);
    });



    // Sanatized Airports
    var airports = $.map(airportsCode, function (airport) {
        var airport = (typeof airport === "string") ? window["o" + airport] : null;
        if (!airport) return;
        // special names
        for (var i = 0; i < SpecialNames.length; i++) {
            if (airport.code == SpecialNames[i].code) {
                airport.name = SpecialNames[i].name;
                break;
            }
        }
        if (window["r" + airport.code] != undefined) {
            var routes = window["r" + airport.code];
            return {
                label: airport.name,
                code: airport.code,
                jb: airport.jb,
                cc: airport.cc,
                duplicate: airport.duplicate == undefined ? false : true,
                country: window["c" + airport.cc].name,
                region: countries[airport.cc],
                routes: routes
            };
        } else {
            return;
        }
    });



    // ASC sort airports by country name
    airports.sort(function (a, b) {
        var labelA = a.country.toLowerCase(), labelB = b.country.toLowerCase();
        if (labelA < labelB) return -1;
        if (labelA > labelB) return 1
        return 0;
    });
    var airportsByCountry = {};
    for (var i = 0; i < airports.length; i++) {
        if (!!!(airportsByCountry[airports[i].country])) airportsByCountry[airports[i].country] = [];
        airportsByCountry[airports[i].country].push(airports[i]);
    }
    var airports = [];
    for (country in airportsByCountry) {
        airportsByCountry[country].sort(function (a, b) {
            var labelA = a.label.toLowerCase(), labelB = b.label.toLowerCase();
            if (labelA < labelB) return -1;
            if (labelA > labelB) return 1
            return 0;
        });
        airports = airports.concat(airportsByCountry[country]);
    }



    //remove special airports from countries airports
    cUS.airports.pop();
    cUS.airports.pop();
    cUS.airports.pop();
    cUS.airports.push(0);
    cMX.airports.pop();
    cMX.airports.pop();
    cMX.airports.pop();
    cMX.airports.push(0);




    //Undefine so not available to be loaded else where -- objects to window to be able to be parsed as airports
    window.oWalt_Disney_World_MCO = undefined;
    window.rWalt_Disney_World_MCO = undefined;

    window.oAnaheim_LGB = undefined;
    window.rAnaheim_LGB = undefined;

    window.oCancun_Hotel_Zone_CUN = undefined;
    window.rCancun_Hotel_Zone_CUN = undefined;

    window.oCancun_Riviera_Maya_CUN = undefined;
    window.rCancun_Riviera_Maya_CUN = undefined;



    var vacations = airports;


    if ($.isFunction(callback)) callback(vacations);
    JB.Model.Cache.vacations = vacations; // Cache
};

