/**
* JB.StartNavigation Function.
* This function starts all the tricks and coolness of the main navigation.
* @function JB.StartNavigation
* @param {String} selector The id or class selector of the primary nav. Example: "#jb-primary-links"
* @require
*/

function is_touch_device() {
    return (('ontouchstart' in window)
      || (navigator.MaxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0));
}

$(function () {
    //Remove tab index and let the browser decide
    $('[tabindex]').each(function () {
        $(this).removeAttr('tabindex').attr('TabIndexRemoved', "jb.global.js"/*tpa=http://www.jetblue.com/js/src/jb.global.js*/);
    });

    $("#skipToMainContentLink").click(function (ev) {
        ev.preventDefault();

        $('html, body').animate({
            scrollTop: $('#container').offset().top - 5
        }, 200, function () {
            $('#container a:visible:first, #container input:visible:first').first().focus();
        });
    });

    //Prevent page from navigating away when clicking on a main tab that has submenu/dropdown
    if (is_touch_device()) {
        $(".main-nav li.submenu").click(function (evn) {
            if (!$(this).find('.jb-secondary-links').first().hasClass('show-menu')) {
                evn.preventDefault();
            }
        });
    }

    var header_links = $('#jb-header .main-nav .jb-primary-link');
    var header_links_length = header_links.length;

    //Hide Travel Alert from Keyboard Navigation if no alerts
    var checkAlerts = setInterval(function () {
        console.log("KMKMKMK");

        try {
            if ($($('#hdalert').get(0).contentDocument).find('body').length) {
                if (!$($('#hdalert').get(0).contentDocument).find('#Title').length) {
                    $('#hdalert').attr('tabindex', '-1');
                }
                clearInterval(checkAlerts);
            }
        }
        catch (err) {
            clearInterval(checkAlerts);
        }
    }, 500);

    //Disable Scroll via arrow keys
    document.onkeydown = function (e) {
        //if ($('#jb-header .main-nav .hover .jb-primary-link').length > 0) {
        var openSubmenu = $(".main-nav .jb-secondary-links.show-menu");
        if (openSubmenu.length) {
            var cEvent = e || event;
            var code = cEvent.keyCode || cEvent.which;
            switch (code) {
                case 38:
                    (cEvent.preventDefault) ? cEvent.preventDefault() : cEvent.returnValue = false;
                    break;
                case 40:
                    (cEvent.preventDefault) ? cEvent.preventDefault() : cEvent.returnValue = false;
                    break;
                default:
                    break;
            }
        }
    }
});

JB.InitNavigation = function (selector) {
    var options = {
        speed: 280,
        timeout: 100,
        delay: 330
    };
    var jQueryPrimaryNav = $(selector);
    jQueryPrimaryNav.items = jQueryPrimaryNav.children("li");
    jQueryPrimaryNav.currentFocus = '';
    jQueryPrimaryNav.isSubmenuOpen = false;

    function highlightSelectedNav() {
        //var path = location.pathname;
        //if (path && path != "/")
        //    $('#jb-primary-links a[href*="' + path + '"]').closest('li').addClass('active');

        var $activeLI = $('.active').closest('ul').closest('li');

        if ($activeLI.attr('id') != 'nav') {
            $activeLI.addClass('active'); //add class active to the a    
        }
    }

    highlightSelectedNav();

    $('.sign-in').bind('focus', function () {
        CloseSubMenu();
    });

    $('.join').bind('focus', function () {
        CloseSubMenu();
    });

    function OpenSubMenu(linkItem) {
        var jQuerysubNavBlind = $('#jb-backdrop');
        jQuerysubNavBlind.css('height', $(linkItem).children('.jb-secondary-links').height());
        jQuerysubNavBlind.slideDown(function () {
            jQueryPrimaryNav.isSubmenuOpen = true;
            //Set Active Item
            $(linkItem).children('.jb-secondary-links').addClass('show-menu');
            $(linkItem).addClass('hover');
            $(linkItem).siblings().removeClass('hover');
        });
    }

    function CloseSubMenu() {
        var jQuerysubNavBlind = $('#jb-backdrop');
        jQueryPrimaryNav.isSubmenuOpen = false;
        $('.jb-secondary-links').removeClass('show-menu');
        $('.jb-secondary-links').parent('.hover').removeClass('hover');
        jQuerysubNavBlind.slideUp(function () { });
        highlightSelectedNav();
    }

    function SwitchSubMenu(linkItem) {
        $('.jb-secondary-links').removeClass('show-menu');
        $('.jb-secondary-links').parent('.hover').removeClass('hover');
        $(linkItem).children('.jb-secondary-links').addClass('show-menu');
        $(linkItem).addClass('hover');
        $(linkItem).siblings().removeClass('hover');
        highlightSelectedNav();
    }

    var fromPrimeToSec = false;

    var header_links = $('#jb-header .main-nav .jb-primary-link');
    var header_links_length = header_links.length;

    jQueryPrimaryNav.items.each(function (index, item) {
        $(item).children('a.jb-primary-link').bind('keyup', function (e) {
            var cEvent = e || event;
            var code = cEvent.keyCode || cEvent.which;
            switch (code) {
                case 37:
                    //Left
                    if ((index - 1) >= 0) {
                        $(item).blur();
                        $(header_links[index - 1]).focus();
                    } else {
                        $(item).blur();
                        $(header_links[header_links_length - 1]).focus();
                    }
                    break;
                case 39:
                    //Right
                    if ((index + 1) < header_links_length) {
                        $(item).blur();
                        $(header_links[index + 1]).focus();
                    } else {
                        $(item).blur();
                        $(header_links[0]).focus();
                    }
                    break;
                case 40:
                    //Bottom
                    if ($(item).find('.jb-secondary-link').length > 0) {
                        $(item).find('.jb-secondary-link')[0].focus();
                    }
                    break;
                default: break;
            }
        });

        jQueryPrimaryNav.shouldiclose = false;
        //Primary Links hover function
        $(item).hover(function (e) {
            //In
            var cEvent = e || event;
            $(item).addClass('hover');
            setTimeout(function () {
                try {
                    if ($(item).hasClass("hover") && !$('#jb-backdrop').is(':animated')) {

                        if ($(item).hasClass('submenu')) {
                            //Has submenu
                            //Open submenu if not open and set submenu links to current items
                            if (jQueryPrimaryNav.isSubmenuOpen == false) {
                                //Open
                                OpenSubMenu(item);
                            } else {
                                //Switch
                                SwitchSubMenu(item);
                                jQueryPrimaryNav.shouldiclose = false;
                            }
                        }
                    }
                } catch (e) {
                    console.log(e);
                }
            }, options.delay);

        }, function () {
            //Out
            $(item).removeClass('hover');
            setTimeout(function () {
                try {
                    if (!$('.hover.submenu').length > 0 && !$('#jb-backdrop').is(':animated')) {
                        jQueryPrimaryNav.shouldiclose = true;
                        setTimeout(function () {
                            if (jQueryPrimaryNav.shouldiclose) {
                                CloseSubMenu();
                            }
                        }, options.timeout);
                    }
                } catch (e) {
                }
            }, options.delay);
        });

        //Primary Links focus function
        $(item).children('a.jb-primary-link').bind('focusin', function () {
            $(item).addClass('hover');
            setTimeout(function () {
                try {
                    goingDown = false;
                    if ($(item).hasClass("hover") && !$('#jb-backdrop').is(':animated')) {
                        //Check if primary link has submenu
                        if ($(item).hasClass('submenu')) {
                            //Has submenu
                            //Open submenu if not open and set submenu links to current items
                            if (jQueryPrimaryNav.isSubmenuOpen == false) {
                                //Open
                                OpenSubMenu(item);
                            } else {
                                //Switch
                                SwitchSubMenu(item);
                            }
                            //$('#jb-header .jb-secondary-links') hide all
                        } else {
                            //Does not have submenu
                            //Close submenu if open
                            CloseSubMenu();
                        }
                        fromPrimeToSec = true;
                    }
                } catch (e) {
                    console.log(e);
                }
            }, options.delay);
        });

        $(item).children('a.jb-primary-link').bind('focusout', function () {
            $(item).removeClass('hover');
        });

        //Submenu Links
        var submenu_links = $(item).find('.jb-secondary-link');
        var submenu_links_length = submenu_links.length;

        submenu_links.each(function (index, subLinkItem) {

            //FocusIn - Not needed anymore - requirements changed
            $(subLinkItem).bind('focusin', function (e) {
                $('.jb-secondary-links.show-menu').closest('.submenu').addClass('hover');

                //                if (jQueryPrimaryNav.isSubmenuOpen) {
                //                    //Switch
                //                    SwitchSubMenu(item);
                //                } else {
                //                    //Open
                //                    OpenSubMenu(item);
                //                }
            });

            //Arrow Keys
            $(subLinkItem).bind('keyup', function (ev) {

                $('.jb-secondary-links.show-menu').closest('.submenu').addClass('hover');

                var cEvent = ev || event;
                var code = cEvent.keyCode || cEvent.which;
                switch (code) {
                    case 37:
                        //Left
                        if ($(subLinkItem).parents('.additional-jb-link').length == 0 && subLinkItem.href == $(subLinkItem).parents('.jb-secondary-links').find('.jb-secondary-link')[0].href) {
                            //First Secondary Link
                            $(subLinkItem).blur();
                            $(item).children('.jb-primary-link').focus();
                        } else {
                            if ((index - 1) >= 0) {
                                $(subLinkItem).blur();
                                $(submenu_links[index - 1]).focus();
                            } else {
                                $(subLinkItem).blur();
                                $(submenu_links[submenu_links_length - 1]).focus();
                            }
                        }
                        break;
                    case 38:
                        //Top
                        ev.preventDefault();
                        if ($(subLinkItem).parents('.additional-jb-link').length == 0 && subLinkItem.href == $(subLinkItem).parents('.jb-secondary-links').find('.jb-secondary-link')[0].href) {
                            //First Secondary Link
                            $(subLinkItem).blur();
                            $(item).children('.jb-primary-link').focus();
                        } else {
                            if ((index - 1) >= 0) {
                                $(subLinkItem).blur();
                                $(submenu_links[index - 1]).focus();
                            } else {
                                $(subLinkItem).blur();
                                $(submenu_links[submenu_links_length - 1]).focus();
                            }
                        }
                        break;
                    case 39:
                        //Right
                        if ((index + 1) < submenu_links_length) {
                            $(subLinkItem).blur();
                            $(submenu_links[index + 1]).focus();
                        } else {
                            $(subLinkItem).blur();
                            $(submenu_links[0]).focus();
                        }
                        break;
                    case 40:
                        //Down
                        ev.preventDefault();
                        if ((index + 1) < submenu_links_length) {
                            $(subLinkItem).blur();
                            $(submenu_links[index + 1]).focus();
                        } else {
                            $(subLinkItem).blur();
                            $(submenu_links[0]).focus();
                        }

                        break;
                    case 27:
                        $(subLinkItem).blur();
                        $(item).children('.jb-primary-link').focus();
                        break;
                    case 9:
                        if (cEvent.shiftKey && fromPrimeToSec) {
                            //Shift Tab
                            $(subLinkItem).blur();
                            $(item).children('.jb-primary-link').focus();
                        } else {
                            //Tab
                            if (jQueryPrimaryNav.isSubmenuOpen) {
                                //Switch
                                SwitchSubMenu(item);
                            } else {
                                //Open
                                OpenSubMenu(item);
                            }
                            fromPrimeToSec = false;
                        }

                        break;
                    default: break;
                }
            });
        });
    });
}

JB.StartStickyNav = function (selector) {
    var jQueryel = jQuery(selector),
			pageNav = jQuery('#page-nav'),
			sectionDropdown = jQueryel.find('div.section-dropdown');
    var top = jQueryel.offset().top || 183;
    jQuery(window).bind('scroll', function () {
        var yPos = jQuery(this).scrollTop();
        if (yPos >= top) {
            jQuery('body').addClass('fixed-nav-enabled');
        } else {
            jQuery('body').removeClass('fixed-nav-enabled');
        }
    });
    jQuery('#back-to-top').bind('click', function () {
        var scrollTarget = jQuery(this).attr('href');
        jQuery.scrollTo(scrollTarget, 510);
        return false;
    });

    if (pageNav.length) {
        pageNav.find('a').bind('click', function () {
            var scrollTarget = jQuery(this).attr('href'),
					offsetValue;
            if (jQuery('body').hasClass('reset-offset')) {
                var bodyClass = jQuery('body').attr('class');
                var offsetValue = parseInt('-' + (bodyClass.match(/(\d{3})/)));
            } else {
                var offsetValue = -120;
            }
            if (jQuery('body').hasClass('fixed-nav-enabled')) {
                if (jQuery(this).parent().index() > 0) {
                    jQuery.scrollTo(scrollTarget, 510, { offset: { top: offsetValue} });
                } else {
                    jQuery.scrollTo(scrollTarget, 510, { offset: { top: (offsetValue + 18)} });
                }
            } else {
jQuery('body').addClass('fixed-nav-enabled');
                jQuery.scrollTo(scrollTarget, 510, { offset: { top: (offsetValue)} });
            }
            return false;
        });
    }
    if (sectionDropdown.hasClass('also-jumplinks')) {
        sectionDropdown.find('a').bind('click', function () {
            var scrollTarget = jQuery(this).attr('href'),
					offsetValue;
            if (jQuery('body').hasClass('reset-offset')) {
                var bodyClass = jQuery('body').attr('class');
                var offsetValue = parseInt('-' + (bodyClass.match(/[\d\.]+/g)));
            } else {
                var offsetValue = -140;
            }
            jQuery.scrollTo(scrollTarget, 510, { offset: { top: offsetValue} });
            sectionDropdown.children('div.section-nav').hide();
            return false;
        });
    }
    if (sectionDropdown.length) {
        sectionDropdown.children('h5').bind('click', function (e) {
            jQueryel = jQuery(e.target);

            jQuery(this).siblings('div.section-nav').show();
            sectionDropdown.offclick(function () {
              jQueryel.siblings('div.section-nav').hide();
            }, true);
            return false;
        });
    }
};


/**
* JetBlue Ready Function.
* This function executes all the necessary javascript to start the site.
* @function JB.Ready
*/

JB.Ready = function () {
    JB.Config.debug = true;
    JB.Session.countryCode = "cUS";
    JB.Session.isBrowser = JB.Fn.getIsBrowserMatrix(); // Stores Browser Check in Session (for IE browser check purpose.)

    JB.Fn.getAllPageDimensions(); // Set Page Dimensions in Session
    JB.InitNavigation("#jb-primary-links");


    try {
        if (JB.Session.isBrowser.isIE) jQuery("input[placeholder]").onFocusClear();
    } catch (err) { }

    // Jump Link
    if (!!jQuery("a.scrollingjumplink").length) {
        jQuery("a.scrollingjumplink").bind('click', function () {
            var scrollTarget = jQuery(this).attr('href');
            jQuery.scrollTo(scrollTarget, 510, { offset: { top: -110} });
            return false;
        });
    }

    JB.Session.sizer = {
        countTotal: 1,
        countCurrent: 2
    };

    // @MX3.1 #615 (3)
    if (is_touch_device()) {
        JB.Session.isTouch = true;
    } else {
        JB.Session.isTouch = false;
    }


    if (JB.Session.isTouch) {
        jQuery(".submenu").addClass("isTouch");
        JB.Session.isTouchCount = 0;
        JB.Session.hasTouched = false;
        // console.log("Touch enabled:" + JB.Session.isTouch + " isTouchCount: " + JB.Session.isTouchCount);

    }
    // Please do not stack window resize event handlers, as all functions will be triggered.
    jQuery(window).resize(function () {
        JB.Session.sizer.countTotal += 1;
        var resizeDelay = function () {
            if (JB.Session.sizer.countCurrent >= JB.Session.sizer.countTotal) {
                JB.Fn.getAllPageDimensions();
                JB.Session.sizer.countTotal = 1;
                JB.Session.sizer.countCurrent = 2;
            } else {
                JB.Session.sizer.countCurrent += 1;
            }
        };
        setTimeout(resizeDelay, 500);
    });

    // Smart Init
    jQuery("body div[id], body a[id]").each(function (i, el) {
        switch (el.id) {
            case "sticky-nav":
                JB.StartStickyNav("#sticky-nav"); // Start Sticky Nav
                break;
            case "tooltip-pick-me-up":
                jQuery("#tooltip-pick-me-up").tooltip();
                //                new JB.Class.Modal({
                //                    openTogglers: "#tooltip-pick-me-up",
                //                    url: "http://www.jetblue.com/partials/modal.pick-me-up.html",
                //                    onLoad: function (e, instance) {
                //                        var jQuerymodal = instance.modal;
                //                    }
                //                });
                break;
            case "tooltip-confirmation-num":
                jQuery('#tooltip-confirmation-num').tooltip();
                break;
            case "trueblue":
                new JB.Class.TrueBlue('#trueblue', {});
                break;
            case "booker":
                new JB.Class.Booker("#booker", { countryCode: JB.Session.countryCode });
                break;
            case "booker-mini":
                JB.Session.miniBookerInstance = new JB.Class.Booker("#booker-mini", { mini: true, countryCode: JB.Session.countryCode });
                break;
            case "check-flight-status":
                new JB.Class.CheckFlightStatus("#check-flight-status");
                break;
            case "flights-timetable":
                new JB.Class.Timetable("#flights-timetable", { countryCode: JB.Session.countryCode });
                break;
            case "getaways-cities-list":
                jQuery("#getaways-cities-list").find("p.expander span").bind("click", function () {
                    var jQuerytarget = jQuery(this);
                    var jQuerymoreCities = jQuerytarget.closest("#getaways-cities-list");
                    if (jQuerymoreCities.hasClass("hide-cities")) jQuerytarget.text("Show fewer cities");
                    else jQuerytarget.text("Show more cities");
                    jQuerymoreCities.toggleClass("hide-cities");
                });
                jQuery("#getaways-cities-list").find("#city-expander").bind("keypress", function (e) {
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if (code == 13 || code == 32) {
                        console.log("hit space or enter")
                        var jQuerytarget = jQuery(this).find("p.expander span");
                        var jQuerymoreCities = jQuerytarget.closest("#getaways-cities-list");
                        if (jQuerymoreCities.hasClass("hide-cities")) jQuerytarget.text("Show fewer cities");
                        else jQuerytarget.text("Show more cities");
                        jQuerymoreCities.toggleClass("hide-cities");
                    }
                });

                break;
            /*case "group-submission-form":
            var jQuerygroupTravelForm = jQuery("#group-submission-form form");
            jQuerygroupTravelForm.find("select, input").enhance({classNamePrefix: "jb"});
            // Adding a custom phone number validation method to the validator.
            jQuery.validator.addMethod("phoneUS", function(phone_number, element){
            phone_number = phone_number.replace(/\s+/g, ""); 
            return this.optional(element) || phone_number.length > 9 && phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}jQuery/);
            }, "Please specify a valid phone number");
            // Form Validation
            jQuerygroupTravelForm.validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            focusInvalid: false,
            rules: {
            emailaddress: {
            required: true,
            email: true
            },
            fullname: {
            required: true
            },
            phonenumber: {
            required: true,
            phoneUS: true
            },
            organizationname: {
            required: true
            },
            grouptype: {
            required: true
            },
            numberoftravelers: {
            required: true
            },
            roundtriporoneway: {
            required: true
            },
            travelfrom: {
            required: true
            },
            outbounddate: {
            required: true,
            date: true
            },
            outbounddaytime: {
            required: true
            },
            destination: {
            required: true
            },
            returndate: {
            required: true,
            date: true
            },
            returndaytime: {
            required: true
            }
            },
            messages: {
            emailaddress: {
            required: "email required",
            email: "please enter valid email"
            },
            fullname: {
            required: "full name required"
            },
            phonenumber: {
            required: "phone number required",
            phoneUS: "please enter a valid phone number"
            },
            organizationname: {
            required: "organization name required"
            },
            grouptype: {
            required: "group type required"
            },
            numberoftravelers: {
            required: "number of travelers required"
            },
            roundtriporoneway: {
            required: "round trip or oneway required"
            },
            travelfrom: {
            required: "travel from required"
            },
            outbounddate: {
            required: "outbound date",
            date: "enter valid outbound date"
            },
            outbounddaytime: {
            required: "select outbound daytime"
            },
            destination: {
            required: "destinations required"
            },
            returndate: {
            required: "return date required",
            date: "please enter valid return date"
            },
            returndaytime: {
            required: "please select return daytime"
            }
            },
            showErrors: function(errorMap, errorList){
            if(!!(errorList.length)){
            var errors = "";
            for(var i = 0; i < errorList.length; i++){
            errors += errorList[i].message + "\n";
            }
            alert(errors);
            }
            },
            invalidHandler: function(form, validator){
            },
            submitHandler: function(form){
            }
            });
            break;*/ 
        }
    });

    //if (!!jQuery("#container.flights-landing").length) new JB.Class.FaresBrowser("#container.flights-landing", { bookerInstance: JB.Session.miniBookerInstance });
};

JB.Ready();
