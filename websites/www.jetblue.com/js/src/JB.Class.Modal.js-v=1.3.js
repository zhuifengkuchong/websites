/**
 * Modal Overlay Base Class.
 * Creates new or reuses already created Overlay element.
 * @function JB.Class.Modal
 * @param {String} container The id or class selector of element to make modal.
 */

JB.Class.Modal = JB.Class.extend({
	/**
	 * Library dependencies
	 * @type object
	 */
	require: {
	},
	/**
	 * @constructor
	 */
	init: function(container, options){
		/**
		 * Options
		 */
		this.options = {
			url: null,
			openTogglers: null,
			onLoad: function(){},
			onOpen: function(){},
			onClose: function(){},
			speed: 0
		};
		if($.isPlainObject(container)){
			var options = container;
			var container = null;
		}
		$.extend(this.options, options || {});
		this.document = $(document);
		this.container = $(container);
		this._super();
	},
	/** @ignore */
	_build: function(){
		var self = this;
		this.modal = this.container;
		this.overlay = $("body").children("div.overlay");
		if(!!!(this.overlay.length)) this.overlay = $("<div class='overlay'><div class='screen'></div></div>").appendTo("body").hide(0);
		this.screen = this.overlay.children("div.screen").fadeOut(0);
		this.openTogglers = $(this.options.openTogglers);
		this.document.dimension = {
			height: this.document.height()
		};

		// Set Overlay Height
		this.overlay.height(this.document.dimension.height);

		// Open togglers click event handler.
		this.openTogglers.bind("click", $.proxy(this.open, this));

		// Only if modal content exists
		if(!!(this.modal.length)) this._rebuild();
	},
	/** @ignore */
	_rebuild: function(){
		var self = this;
		this.modal.appendTo(this.overlay);
		this.modal.dimension = {
			width: this.modal.width(),
			height: this.modal.height()
		};
		// Modal Close Button Click Event Handler
		this.modal.find("a.close-btn").click($.proxy(this.close, this));
	},
	/**
	 * Opens Modal Window.
	 * @param {Object} [e] Event
	 */
	open: function(e){
		var self = this;
		// If modal content don't exist, grab it via AJAX if url is provided
		if(!!!(this.modal.length) && (this.options.url)){
			// Get AJAX content
			this.getContent(this.options.url, function(data, textStatus, jqXHR){
				self.modal = $("<div class='modal-container'></div>").html(data).find(".modal");
				self.options.onLoad(e, self);
				self._rebuild();
				self.open(e);
			});
			return;
		}
        this.vertcalAlignCenterModal();

        if (this.modal.siblings().length > 1) {
            this.modal.siblings(":not(screen)").hide();
        }

		this.overlay.show(0, function(){
			self.screen.fadeIn(self.options.speed, function(){
				self.modal.fadeIn(self.options.speed);
				// Overlay Close trigger
				self.screen.click($.proxy(self.close, self));
				self.options.onOpen(e, self);
			});
		});
	},
	/**
	 * Closes Modal Window.
	 * @param {Object} [e] Event
	 */
	close: function(e){
		var self = this;
		this.modal.fadeOut(this.options.speed, function(){
			self.screen.fadeOut(self.options.speed, function(){
				self.overlay.hide(0);
				self.screen.unbind("click");
				self.options.onClose(e, self);
			});
		});
	},
	/**
	 * Vertically Aligns Modal to Center of Viewport in relation to Window scroll height.
	 */
	vertcalAlignCenterModal: function(){
		var windowHeight = $(window).height();
		var scrollTop = $(window).scrollTop();
		
		//if(window.console) console.log('windowHeight: ', windowHeight, ' this.modal.dimension.height: ', this.modal.dimension.height);
		
		if(windowHeight > this.modal.dimension.height){
			var offset = ((windowHeight - this.modal.dimension.height) / 2);
			var center = (scrollTop + offset);
			this.modal.css("top", center + "px");
		} else {
			this.modal.css("top", scrollTop + "px");
		}
	},
	/**
	 * Gets content via AJAX request.
	 * @param {String} url The url or path to html partial.
	 * @param {Function} callback The callback function.
	 */
	getContent: function(url, callback){
		$.ajax({
			url: url || this.options.url,
			dataType: "html",
			cache: true,
			success: function(data, textStatus, jqXHR){
				if($.isFunction(callback)) callback(data, textStatus, jqXHR);
			}
		});
	}
});