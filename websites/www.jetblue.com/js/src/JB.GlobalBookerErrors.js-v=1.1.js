//Global Message file for IM
//This file hold all errors for the JB.com Main booker / JB.com Main vacations booker /Vacations site booker / BFF Booker

GlobalErrors = new Object();
GlobalErrors.MainBooker = new Object();
GlobalErrors.VacationsBooker = new Object();
GlobalErrors.BestFareFinderBooker = new Object();

GlobalErrors.MainBooker.departure = "Please choose a valid departure city.";
GlobalErrors.MainBooker.destination = "Please choose a valid arrival city.";
GlobalErrors.MainBooker.departDate = "Please select a date to leave.";
GlobalErrors.MainBooker.returnDate = "Please select a date to return.";
GlobalErrors.MainBooker.maxInfants = "You are allowed one infant per adult, up to a maximum of three infants total.";
GlobalErrors.MainBooker.minPassengers = "You must select at least one passenger.";
GlobalErrors.MainBooker.maxPassengers = "You are allowed a maximum of seven passengers per online booking. If your party is larger than this, please call 1-888-JETBLUE (538-2583), option 2 or go to www.jetblue.com/groups.";
GlobalErrors.MainBooker.kidsAge = "Please enter an age for child {age} (age must be between 2 to 18).";
GlobalErrors.MainBooker.todayWarning = "Because you've selected today's date, only flights that leave at least 1 hour and 30 minutes from the current time will be shown on the next page.";
GlobalErrors.MainBooker.confirmationNumber = "Please enter your confirmation number. It should be six characters (letters and numbers).";
GlobalErrors.MainBooker.firstName = "Please enter the first name of a customer on your itinerary.";
GlobalErrors.MainBooker.lastName = "Please enter the last name of a customer on your itinerary.";
GlobalErrors.MainBooker.flightNumber = "Please enter a valid flight number (one to four digits).";
GlobalErrors.MainBooker.partnerTB = "TrueBlue points cannot be used to search for flights operated by our partner airlines. Please select dollars as your fare type to continue.";
GlobalErrors.MainBooker.sameDay = "Your return date is the same as your departure date. Is such a short trip intentional?";
GlobalErrors.MainBooker.owInt = "You have selected to fly one-way internationally. Because you're traveling to another country, proof of return travel may be required at the airport.";
GlobalErrors.MainBooker.fareCalendarInterlineNA = "We are not able to show you our Best Fare Finder for the cities you've selected. You will now be directed to the flight results page where you can select flights for the dates you've chosen.";
GlobalErrors.MainBooker.fdInterlineNoDate = "We are not able to show you our Best Fare Finder for the cities you've selected. Please choose travel dates in order to continue your flight selection.";
GlobalErrors.MainBooker.seasonalRouteError = "The route you selected only operates seasonally. <br /><br /> If you are flexible with your dates and want to see a full calendar view of our fares, check out the <a id='seasonalRouteBFF' href='http://www.jetblue.com/bestfarefinder/'>Best Fare Finder</a> or click continue.";
GlobalErrors.MainBooker.dayOfWeekRouteError = "The route you selected does not operate every day of the week. <br /><br />If you are flexible and want to see a full calendar view of our fares, check out the <a id='dayOfWeekRouteBFF' href='http://www.jetblue.com/bestfarefinder/'>Best Fare Finder</a> or click continue.";
GlobalErrors.MainBooker.seasonalRouteError_vacations = "The route you selected for your Getaways vacation package only operates seasonally.";
GlobalErrors.MainBooker.dayOfWeekRouteError_vacations = "The route you selected for your Getaways vacation package does not operate every day of the week.";
GlobalErrors.MainBooker.returnMonthPastDepartureError = "You have selected a return date more than 4 weeks after your departure date. Is this intentional?";
GlobalErrors.MainBooker.A321InflightPrefix = "PLEASE NOTE: Flight(s) ";
GlobalErrors.MainBooker.A321InflightSuffix = " on the date(s) you selected will operate on a fresh-from-the-factory airplane, so the in-flight entertainment system will not yet be installed. We strongly encourage you to bring your own entertainment. Flights not listed will be business-as-usual with DirecTV\u00AE programming (in specific coverage areas).";
GlobalErrors.MainBooker.A321InflightPrefixMini = "PLEASE NOTE: Flight(s) ";
GlobalErrors.MainBooker.A321InflightSuffixMini = " on the date(s) you selected will operate on a fresh-from-the-factory airplane, so the in-flight entertainment system will not yet be installed. We strongly encourage you to bring your own entertainment. Flights not listed will be business-as-usual with DirecTV\u00AE programming (in specific coverage areas).";


GlobalErrors.VacationsBooker.departure = "Please choose a valid departure city.";
GlobalErrors.VacationsBooker.destination = "Please choose a valid arrival city.";
GlobalErrors.VacationsBooker.departDate = "Please select a date to leave.";
GlobalErrors.VacationsBooker.returnDate = "Please select a date to return.";
GlobalErrors.VacationsBooker.maxInfants = "You are allowed one infant per adult, up to a maximum of three infants total.";
GlobalErrors.VacationsBooker.minPassengers = "You must select at least one passenger.";
GlobalErrors.VacationsBooker.maxPassengers = "You are allowed a maximum of seven passengers per online booking. If your party is larger than this, please call 1-888-JETBLUE (538-2583), option 2 or go to www.jetblue.com/groups.";
GlobalErrors.VacationsBooker.kidsAge = "Please enter an age for child {age} (age must be between 2 to 18).";
GlobalErrors.VacationsBooker.todayWarning = "Because you've selected today's date, only flights that leave at least 1 hour and 30 minutes from the current time will be shown on the next page.";
GlobalErrors.VacationsBooker.confirmationNumber = "Please enter your confirmation number. It should be six characters (letters and numbers).";
GlobalErrors.VacationsBooker.firstName = "Please enter the first name of a customer on your itinerary.";
GlobalErrors.VacationsBooker.lastName = "Please enter the last name of a customer on your itinerary.";
GlobalErrors.VacationsBooker.flightNumber = "Please enter a valid flight number (one to four digits).";
GlobalErrors.VacationsBooker.partnerTB = "TrueBlue points cannot be used to search for flights operated by our partner airlines. Please select dollars as your fare type to continue.";
GlobalErrors.VacationsBooker.sameDay = "Your return date is the same as your departure date. Is such a short trip intentional?";
GlobalErrors.VacationsBooker.owInt = "You have selected to fly one-way internationally. Because you're traveling to another country, proof of return travel may be required at the airport.";
GlobalErrors.VacationsBooker.returnMonthPastDepartureError = "You have selected a return date more than 4 weeks after your departure date. Is this intentional?";
GlobalErrors.VacationsBooker.A321InflightPrefix = "PLEASE NOTE: Flight(s) ";
GlobalErrors.VacationsBooker.A321InflightSuffix = " on the date(s) selected will operate on a fresh-from-the-factory airplane, so the entertainment system will not be installed. We encourage you to bring your own entertainment.";


GlobalErrors.BestFareFinderBooker.departure = "Please choose a valid departure city.";
GlobalErrors.BestFareFinderBooker.destination = "Please choose a valid arrival city.";
GlobalErrors.BestFareFinderBooker.minPassengers = "You must select at least one passenger.";
GlobalErrors.BestFareFinderBooker.maxPassengers = "You are allowed a maximum of seven passengers per online booking. If your party is larger than this, please call 1-888-JETBLUE (538-2583), option 2 or go to www.jetblue.com/groups.";
GlobalErrors.BestFareFinderBooker.partnerTB = "TrueBlue points cannot be used to search for flights operated by our partner airlines. Please select dollars as your fare type to continue.";
GlobalErrors.BestFareFinderBooker.A321InflightPrefix = "PLEASE NOTE: Flight(s) ";
GlobalErrors.BestFareFinderBooker.A321InflightSuffix = " on the date(s) you selected will operate on a fresh-from-the-factory airplane.  While you’ll be able to enjoy the \"new plane smell,\" the in-flight entertainment system will not yet be installed.<br /><br />If you select one of these flights, we encourage you to bring your own entertainment. All other flights not listed will be business-as-usual and offer our live DirecTV\u00AE programming (in specific coverage areas) and JetBlue Features movies.";



//A321 direct tv message
A321Tv = new Object();
A321Tv.Route = new Object();
A321Tv.Schedule = new Object();
A321Tv.Schedule.sDate = new Date();
A321Tv.Schedule.eDate = new Date();
A321Tv.Flights = new Object();
A321Tv.FlightsRT = new Object();

// Dates are in javascript (jan is 0 - dec is 11)
var A321InflightImpact = new Array([
A321Tv.Route = new Array(['JFK', 'SJU']),
A321Tv.Schedule.sDate = new Date(2014, 0, 25),
A321Tv.Schedule.eDate = new Date(2014, 1, 3),
A321Tv.Flights = new Array(['#0003']),
A321Tv.FlightsRT = new Array(['#0004'])
],
[A321Tv.Route = new Array(['JFK', 'SJU']),
A321Tv.Schedule.sDate = new Date(2014, 1, 13),
A321Tv.Schedule.eDate = new Date(2014, 3, 30),
A321Tv.Flights = new Array(['#0703','#1403']),
A321Tv.FlightsRT = new Array(['#0504', '#1404'])
],
[A321Tv.Route = new Array(['JFK', 'SJU']),
A321Tv.Schedule.sDate = new Date(2014, 4, 15),
A321Tv.Schedule.eDate = new Date(2014, 5, 8),
A321Tv.Flights = new Array(['#0803']),
A321Tv.FlightsRT = new Array(['#0804'])
],
[A321Tv.Route = new Array(['SJU', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 0, 25),
A321Tv.Schedule.eDate = new Date(2014, 1, 3),
A321Tv.Flights = new Array(['#0004']),
A321Tv.FlightsRT = new Array(['#0003'])
],
[A321Tv.Route = new Array(['SJU', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 1, 13),
A321Tv.Schedule.eDate = new Date(2014, 3, 30),
A321Tv.Flights = new Array(['#0504', '#1404']),
A321Tv.FlightsRT = new Array(['#0703','#1403'])
],
[A321Tv.Route = new Array(['SJU', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 4, 15),
A321Tv.Schedule.eDate = new Date(2014, 5, 18),
A321Tv.Flights = new Array(['#0804']),
A321Tv.FlightsRT = new Array(['#0803'])
],
[A321Tv.Route = new Array(['JFK', 'SDQ']),
A321Tv.Schedule.sDate = new Date(2014, 3, 10),
A321Tv.Schedule.eDate = new Date(2014, 3, 30),
A321Tv.Flights = new Array(['#0509']),
A321Tv.FlightsRT = new Array(['#0510'])
],
[A321Tv.Route = new Array(['JFK', 'SDQ']),
A321Tv.Schedule.sDate = new Date(2014, 4, 1),
A321Tv.Schedule.eDate = new Date(2014, 5, 8),
A321Tv.Flights = new Array(['#0209']),
A321Tv.FlightsRT = new Array(['#0210'])
],
[A321Tv.Route = new Array(['SDQ', 'JFK']),
A321Tv.Schedule.sDate = new Date(2013, 3, 10),
A321Tv.Schedule.eDate = new Date(2014, 3, 30),
A321Tv.Flights = new Array(['#0509']),
A321Tv.FlightsRT = new Array(['#0510'])
],
[A321Tv.Route = new Array(['SDQ', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 4, 1),
A321Tv.Schedule.eDate = new Date(2014, 5, 8),
A321Tv.Flights = new Array(['#0210']),
A321Tv.FlightsRT = new Array(['#0209'])
],
[A321Tv.Route = new Array(['JFK', 'STI']),
A321Tv.Schedule.sDate = new Date(2014, 1, 13),
A321Tv.Schedule.eDate = new Date(2014, 1, 24),
A321Tv.Flights = new Array(['#0237']),
A321Tv.FlightsRT = new Array(['#0236'])
],
[A321Tv.Route = new Array(['JFK', 'STI']),
A321Tv.Schedule.sDate = new Date(2013, 3, 10),
A321Tv.Schedule.eDate = new Date(2014, 3, 29),
A321Tv.Flights = new Array(['#0237']),
A321Tv.FlightsRT = new Array(['#0236'])
],
[A321Tv.Route = new Array(['JFK', 'STI']),
A321Tv.Schedule.sDate = new Date(2013, 4, 1),
A321Tv.Schedule.eDate = new Date(2014, 5, 8),
A321Tv.Flights = new Array(['#0437']),
A321Tv.FlightsRT = new Array(['#0436'])
],
[A321Tv.Route = new Array(['STI', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 1, 14),
A321Tv.Schedule.eDate = new Date(2014, 1, 25),
A321Tv.Flights = new Array(['#0236']),
A321Tv.FlightsRT = new Array(['#0237'])
],
[A321Tv.Route = new Array(['STI', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 3, 11),
A321Tv.Schedule.eDate = new Date(2014, 3, 30),
A321Tv.Flights = new Array(['#0236']),
A321Tv.FlightsRT = new Array(['#0237'])
],
[A321Tv.Route = new Array(['STI', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 4, 2),
A321Tv.Schedule.eDate = new Date(2014, 5, 7),
A321Tv.Flights = new Array(['#0436']),
A321Tv.FlightsRT = new Array(['#0437'])
],
[A321Tv.Route = new Array(['PUJ', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 4, 1),
A321Tv.Schedule.eDate = new Date(2014, 5, 8),
A321Tv.Flights = new Array(['#0268']),
A321Tv.FlightsRT = new Array(['#0869'])
],
[A321Tv.Route = new Array(['JFK', 'PUJ']),
A321Tv.Schedule.sDate = new Date(2014, 4, 1),
A321Tv.Schedule.eDate = new Date(2014, 5, 8),
A321Tv.Flights = new Array(['#0869']),
A321Tv.FlightsRT = new Array(['#0268'])
],
[A321Tv.Route = new Array(['JFK', 'AUA']),
A321Tv.Schedule.sDate = new Date(2014, 1, 19),
A321Tv.Schedule.eDate = new Date(2014, 3, 30),
A321Tv.Flights = new Array(['#0557']),
A321Tv.FlightsRT = new Array(['#0958'])
],
[A321Tv.Route = new Array(['AUA', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 1, 19),
A321Tv.Schedule.eDate = new Date(2014, 3, 30),
A321Tv.Flights = new Array(['#0958']),
A321Tv.FlightsRT = new Array(['#0557'])
],
[A321Tv.Route = new Array(['JFK', 'BGI']),
A321Tv.Schedule.sDate = new Date(2014, 0, 25),
A321Tv.Schedule.eDate = new Date(2014, 5, 8),
A321Tv.Flights = new Array(['#0661']),
A321Tv.FlightsRT = new Array(['#0662'])
],
[A321Tv.Route = new Array(['BGI', 'JFK']),
A321Tv.Schedule.sDate = new Date(2014, 0, 25),
A321Tv.Schedule.eDate = new Date(2014, 5, 8),
A321Tv.Flights = new Array(['#0662']),
A321Tv.FlightsRT = new Array(['#0661'])
]);                