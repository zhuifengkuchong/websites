function rtpOpenWindow(){
var windowLocation = 'http://www.amdsurveys.com/se.ashx?s=5A1E27D27459CBF1';
var windowWidth = 660;
var windowHeight = 600;
var overlayWidth = windowWidth + 20;
var screenWidth=screen.width;
if(screenWidth > 1000){
windowLocation += "&width=full";
}
else if(screenWidth > 640 && screenWidth < 1001) {
windowLocation += "&width=medium";
}
else if(screenWidth < 641){
windowLocation += "&width=small";
}
else{
windowLocation += "&width=NA";
}
var screenHeight=screen.height;
var currentURL=encodeURI(escape(window.location.href));
currentURL = currentURL.replace("%253A",":");
windowLocation = windowLocation + '&refurl='+currentURL;
	if(isMobile()){
		window.open(windowLocation + '&refurl='+currentURL,'survey','width='+windowWidth+',height='+windowHeight+',screenX='+((screenWidth-630)/2)+',screenY='+((screenHeight-600)/2)+',top='+((screenHeight-600)/2)+',left='+((screenWidth-630)/2)+',resizable=yes,copyhistory=yes,scrollbars=no');
	}
	else{
		if(!$('#lightbox').length){	
			var lightboxHTML = "<div id='overlay'></div><div id='lightbox' style='top: 20px; width: " + overlayWidth +
								"px; padding: 1px; position: fixed;'><a href='#' id='popClose'>Close</a></div>";
			$(lightboxHTML).appendTo('body');
			$('#lightbox').append("<iframe height='" + windowHeight + "px' width='" + windowWidth +"px' id='surveyframe' src='" + windowLocation + "'></iframe>");
		$('#overlay').css('opacity', '0.8');
		$("#popClose, #overlay").click(function() {
		$('#lightbox, #overlay').remove();
		});
		$(document).keyup(function(e){
				if(e.keyCode === 27)
				$('#lightbox, #overlay').remove();
			});
		}
		else{
			$('#overlay').css('opacity', '0.8');
			$('#lightbox_bg,#popRating,#popTechicons,#popDisclaimer,#popFootnotes, #popClose').remove();
			$('#lightbox').append("<a href='#' id='popClose'>Close</a><iframe height='" + windowHeight + "px' width='" + windowWidth +"px' id='surveyframe' src='" + windowLocation + "'></iframe>");
			$('#lightbox').css({top: $(window).scrollTop() + 30 + 'px', width: overlayWidth});
			$('#lightbox').show();
			$('#overlay').show();
			$('#popClose').click(function() {
				window.location.reload();
			});
			$(document).keyup(function(e){
				if(e.keyCode === 27)
				window.location.reload();
			});
		}
	} 
}

function rtpPrint(inputVar){
var rtpLink = $(inputVar);
var region = window.location.pathname.toString().substring(4,6);
var rtpLabel = getRtpLabel(region);
rtpLink.text(rtpLabel);
try{
$('#rtp').append(rtpLink);
}
catch(error)
{ document.write(rtpLink); }
}
function getRtpLabel(languagevariable){
	switch(languagevariable)
	{
		case "br":
		return  "Fa\u00E7a uma avalia\u00E7\u00E3o desta p\u00E1gina ";
		case "tw":
		return "\u8A55\u50F9\u6B64\u9801\u9762";
		case "cn":	
		case "zh":
		return  "\u8BC4\u4EF7\u6B64\u7F51\u9875";
		case "de":
		return  "Bewerten Sie diese Seite ";
		case "es":
		case "xl":
		return  "Evaluar esta p\u00E1gina ";
		case "fr":
		return  "Evaluer cette page ";
		case "it":
		return  "Valuta questa pagina ";
		case "jp":
		case "ja":
		return  "\u30DA\u30FC\u30B8\u3092\u8A55\u4FA1 ";
		case "kr":
		case "ko":
		return  "\uD398\uC774\uC9C0 \uD3C9\uAC00 ";
		case "la":
		return  "Califique esta p\u00E1gina ";
		case "pl":
		return  "Oce\u0144 t\u0119 stron\u0119 ";
		case "ru":
		return  "\u041E\u0446\u0435\u043D\u0438\u0442\u0435 \u044D\u0442\u0443 \u0441\u0442\u0440\u0430\u043D\u0438\u0446\u0443 ";
		default:
		return  "Rate This Page";
	}
}	