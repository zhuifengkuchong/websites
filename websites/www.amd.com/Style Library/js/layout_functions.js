/*************************************************************************/
/*************************************************************************/	

/* Show/hide top nav */

/*************************************************************************/
/*************************************************************************/	
function CheckIfNoneExist(checkedValue, collection) {
//checks a string (like the page URL) to make sure it doesn't match any of the conditions
    for (var i = 0; i < collection.length; i++) {
        if (checkedValue.indexOf(collection[i]) != -1) {
            return false;
        }
    }
    return true;
}

/*************************************************************************/
/*************************************************************************/	
function loadLanguagePage(pageurl,defaulturl) {
	pageurl = pageurl.replace("http://www.amd.com/Style%20Library/js/www.amd.com", window.location.hostname);
	//pageurl = "http://www-auth.amd.com/en-us/markets";
	$.ajax({
	url: pageurl,
	type: "GET",
	contentType: "text/html; charset=utf-8",
	timeout: 5000
	}).done(function(){
		window.location.href = pageurl;
		}).fail(function(stuff, status){
		window.location.href = defaulturl;
	});
}

/* Show/hide top nav */

/*************************************************************************/
/*************************************************************************/	
function navUp() {
	$(this).find('div').slideUp('fast');
};
	
function navDown() {
	$(this).find('div').slideDown('fast');
};

/*************************************************************************/
/*************************************************************************/	

/* Function to wrap tabs */

/*************************************************************************/
/*************************************************************************/	

function wrapTabs() {
	var finalWidthTotal = 0;
	var newFinalWidthTotal = 0;	
	var widthTotal = 0; 
	var newWidthTotal = 0; 
	var index = 0; 	
	$('#coda-slider-1-nav-ul li a').each(function() {
		widthTotal = $(this).width() + parseInt($(this).css('padding-left'), 10) + parseInt($(this).css('padding-right'), 10);
		finalWidthTotal += widthTotal;					
		if (finalWidthTotal > 705) {
			newWidthTotal = $(this).width() + (parseInt($(this).css('padding-left'), 10) + parseInt($(this).css('padding-right'), 10)) + 2;
			newFinalWidthTotal += newWidthTotal;
			//$(this).css('background-color', 'red');
			$(this).parent().prependTo('#coda-slider-1-nav-ul');
			index++;
			if (index == 1) {
				$(this).addClass('endRow');								
			}			
		}
	});	
	var rightMargin = 705 - newFinalWidthTotal;
	$('#coda-slider-1-nav-ul li a.endRow').css('margin-right', rightMargin + 'px');			
};	
	
/*************************************************************************/
/*************************************************************************/	

/* Function to change and show a ticker item */

/*************************************************************************/
/*************************************************************************/	

function newstick() {
	$('.newsticker').last().after($('.newsticker').first());
	$('span#newsdisplay').fadeTo(500, 1, changeShow($('span#newsdisplay'), $('.newsticker').first().html()));
}
	
function quotetick() {
	$('.QuoteRotator').last().after($('.QuoteRotator').first());
	$('div#quotedisplay').fadeTo(500, 1, changeShow($('div#quotedisplay'), $('.QuoteRotator').first().html()));
}

function changeShow(container, text) {
	container.fadeTo(500, .1,function(){container.html(text)});
}

/*************************************************************************/
/*************************************************************************/	

/* Function to fix the spacing between top nav items and tabs */

/*************************************************************************/
/*************************************************************************/
	
function fixTopNavWidth() {
	for(i=35;i>10;i--) {
		$('li.topNavHeading').css({"padding-left": i, "padding-right": i});
		if($('li.topNavHeading').first().offset().top == $('li.topNavHeading').last().offset().top) {
			break;
		}
	}
}
function fixTabWidth() {
	if($('.ls-nav').length){
		for(i=15;i>9;i--) {
			if($('.ls-nav a').first().offset().top == $('.ls-nav a').last().offset().top) {
				break;
			}
			$('.ls-wrapper a').css({"padding-left": i, "padding-right": i});
		}
	}
}

/*************************************************************************/
/*************************************************************************/	

/* Handlers for left nav click + hover functionality on mobile devices */

/*************************************************************************/
/*************************************************************************/	

function clickSelected(item) {
	if($(window).width() <= 640) {		
		$(item).toggleClass('open');		
		$('#left-col li.static, #left-col li.display').toggleClass('static display');				
	}	
}	

function hoverLeftNav(item) {
	if($(window).width() <= 640) {		
		$('#left-col li').unbind('mouseenter mouseleave mouseover mouseout');				
	}	
}

/*************************************************************************/
/*************************************************************************/	

/*  */

/*************************************************************************/
/*************************************************************************/

function hasCopy(element){
  return $.trim(element.html())
}

/*************************************************************************/
/*************************************************************************/	

/* Remove '&nbsp;' that SharePoint places before header text */

/*************************************************************************/
/*************************************************************************/

function ditchH1Spaces() { 
	$('h1, h2, h3').each(function(){
		$(this).html($(this).html().replace(/&nbsp;/gi,''));
	});	
}

/*************************************************************************/
/*************************************************************************/	

/* Load XML files */

/*************************************************************************/
/*************************************************************************/

function loadXMLDoc(dname) {
    if (window.ActiveXObject || "ActiveXObject" in window) {
        xhttp = new ActiveXObject("Msxml2.XMLHTTP.3.0");
    }
    else {
        xhttp = new XMLHttpRequest();
    }
    xhttp.open("GET", dname, false);
    xhttp.send("");
    return xhttp.responseXML;
}

/*************************************************************************/
/*************************************************************************/	

/* Transforms nav XML and adds the mega menu to the page  */

/*************************************************************************/
/*************************************************************************/

//new top nav stuff

function createTopNav(result){
			//get the links that go in the bottom
			var bottomlinks = $(result).find('bottomlinks');
			var bottomlinksTitle = bottomlinks.attr("title");

			var topNavBottomLink = $('<p class="topNavBottomLink"><span class="bottom-links-heading">' + bottomlinksTitle + '</span></p>');
			bottomlinks.find('link').each(function(){
				var bottomLinkTitle = $(this).attr("title");
				var bottomLinkURL = $(this).attr("url");
				var bottomLink = $('<a></a>');
				bottomLink.attr("href", bottomLinkURL).text(bottomLinkTitle);
				bottomLink.attr("name", "&amp;lpos=megamenu&amp;lid=mm%7c%7cLooking%20For%7c" + encodeURIComponent(bottomLinkTitle));
				bottomLink.appendTo(topNavBottomLink);
			});
			topNavBottomLink.find('a:not(:last-child)').after(" / ");
			//the bottom links aren't attached to anything yet. We'll fix that later.
			//topNavBottomLink.appendTo('body');
			//this is the main nav list
			var navul = $('<ul id="topNav"></ul>');
			var level = $(result).find('level[top="true"]');
			level.each(function(){
			//this builds each top level list item.
				var image = ($(this).find('image').attr('src'));
				var content = $(this).find('content').find('p');
				var headingTitle = $(this).attr("title");
				var headingLink = $(this).attr("url");
				var locationTop = $(this).attr("top");
				var locationBottom = $(this).attr("bottom");
				var group = $(this).find('group');
				var topNavHeading, sectionTitle;
				var navbox = $("<div class='navBox'></div>");
				
				if(typeof headingTitle == undefined || typeof headingTitle == "undefined")
				{
					topNavHeading = $('<li class="topNavHeading"><img src="' + image +'" /></li>');
					sectionTitle = $('<span></span>')
				}
				else{
				topNavHeading = $('<li class="topNavHeading">' + headingTitle + '</li>');
				sectionTitle = $("<span class='sectionTitle'><a href='" + headingLink + 
					"' name='&lpos=megamenu&lid=mm|" + headingTitle + "|" + headingTitle + "'>"
					+ headingTitle + "</a></span>");
				}
				
//				console.log(typeof headingTitle);
				navbox.append(sectionTitle);
				var topNavGroups = $('<ul class="topNavGroup" data-column="1"></ul><ul class="topNavGroup" data-column="2"></ul><ul class="topNavGroup" data-column="3"></ul>');
				group.each(function(){
					var groupTitle = $(this).attr("title");
					var groupLink = $(this).attr("url");
					var groupColumn = $(this).attr("column");
					var link = $(this).find('link');
					var tnSecondaryGroup = $('<li class="tnSecondaryGroup" data-column="' + groupColumn + '"><a href="' + groupLink +'" name="&lpos=megamenu&lid=mm|' + headingTitle +'|' + groupTitle + '">' + groupTitle + '</a></li>');
					var topNavLinkList = $('<ul class="topNavLinkList"></ul>');
					link.each(function(){
						var linkURL = $(this).attr("url");
						var linkText = $(this).text();
						var tnSecondaryLink = $('<li class="topNavLink"><a class="topNavLink" href="' + linkURL +'" name="&lpos=megamenu&lid=mm|' + headingTitle + "|" + groupTitle +'|' + linkText + '">'+ linkText + '</a></li>');
						tnSecondaryLink.appendTo(topNavLinkList);
					});
					topNavLinkList.appendTo(tnSecondaryGroup);
					tnSecondaryGroup.appendTo(navbox);
				});
				topNavGroups.appendTo(navbox);
				navbox.append($('<div style="clear: both; width: 1px; height: 1px;"></div>'));
				topNavBottomLink.clone().appendTo(navbox);
				navbox.find('li.tnSecondaryGroup').each(function(){
					var column = $(this).data('column');
					$(this).appendTo($(this).siblings('ul[data-column="' + column +'"]'));
				});
				//put the promo in the promo spot
				if(content.length > 0){
					var contentString = new String(content);
					try {
						content = content.html();
					}
					catch(err) {
						content = $(content);	
					}
					var promo = $('<li id="topNavPromo" style="list-style-type:none;"></li>');
					try{
					promo.append(content);
					}
					catch(err){
					
					}
					navbox.find('ul[data-column="3"]').append(promo.html());
				}
				//put the heading into the nav box div
				topNavHeading.append(navbox);
				//throw the lists into the navbox
				navul.append(topNavHeading);
			});
			navul.appendTo('div#topNav');
			$('ul[data-column="1"], ul[data-column="2"], ul[data-column="3"]').removeAttr("data-column");
		    setTimeout(function(){fixTopNavWidth()},250);
	    	$('li.topNavHeading').hoverIntent({
				over: navDown,
				out: navUp, 
				timeout: 100
			});
			/*$('.tnSecondaryGroup li').hide();
			$('.tnSecondaryGroup').hoverIntent({
				over: listDown,
				out: listUp,
				timeout: 100
			});*/
	}

function createFooter(result){
	var footerNav = $('#footer-nav');
	var level = $(result).find('level:not(:last)');
	level.each(function(){ 	
		var headingTitle = $(this).attr("title");
		var headingLink = $(this).attr("url");
		var locationBottom = $(this).attr("bottom");
		var expanded = $(this).attr("expand");
		var group = $(this).find('group');
		var topNavHeading, sectionTitle;
		sectionTitle = $('<a class="footerHeaderLink" href="' + headingLink + 
					'" name="&lpos=megafooter&lid=mm|' + headingTitle + '|' + headingTitle + '">'
					+ headingTitle + '</a>');
		if(expanded == "true"){
			footerNav.append($('<div style="clear:both; margin-top:25px"></div>'));
			footerNav.append(sectionTitle);
			var groupList = $('<ul class="bottomNavGroup" />');
			group.each(function(){
				var groupTitle = $(this).attr("title");
				var groupURL = $(this).attr("url");
				var groupLink = $("<li><a name='&lpos=megafooter&lid=mm|" + headingTitle + "|" + groupTitle + "' href='" + groupURL + "'>" + groupTitle + "</a></li>");
				groupList.append(groupLink);
			});
			groupList.find('li:not(:last)').append("&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;");
			footerNav.append(groupList);
		}
		else{
			footerNav.append(sectionTitle).append($('<span class="footerLinkSeparator"> | </span>'));
		}
	});
	$('.footerLinkSeparator').last().hide();
	$('.footer').prepend(footerNav);
}

function createMobileMenu(){
		var mobileMenus = $('<div id="hide_rwd" style="float:right;"></div>');
		var languageMenu = $('<div class="rwd_menu"><div class="slide" style="cursor: pointer;"><img src="../images/globe.png"/*tpa=http://www.amd.com/style library/images/globe.png*//></div><div class="view"><ul class="rwd_menu_dropdown"></ul></div></div>');
		$('li.topNavHeading').find('a').clone().appendTo(languageMenu.find('.rwd_menu_dropdown'));
		var mobileMenu = $('<div class="rwd_menu"><div class="slide" style="cursor: pointer;">Menu</div><div class="view"><ul class="rwd_menu_dropdown" id="menu"></ul></div></div>');
		var sectionTitles = $('.sectionTitle a').clone();
		sectionTitles.appendTo(mobileMenu.find('.rwd_menu_dropdown'));
		mobileMenus.append(languageMenu).append(mobileMenu);
		mobileMenus.find('a').wrap('<li></li>');
		$('.SecondaryNav').after(mobileMenus);
//	    mobileMenus.clone().appendTo('.footer-main');
		$('http://www.amd.com/Style%20Library/js/div.view').hide(); 
		$('div.rwd_menu').click(function(e) {
			var thisMenu = $(this).find('http://www.amd.com/Style%20Library/js/div.view');
			if(thisMenu.is(':visible'))
			{
				thisMenu.fadeOut('fast');
			}
			else {
				$('.view').hide();
	 			thisMenu.fadeIn('fast');
   				e.stopPropagation();
			}
		 });
}

function displayNewNav(region){
	var navurl = "/style library/topnav/topnav" + region + ".xml";
	$.get(navurl,
		function(result){
			createTopNav(result);
			createMobileMenu();
			createFooter(result);
	}), "html"
}
//end all that new top nav stuff

/*************************************************************************/
/*************************************************************************/	

/* Function to retrieve YouTube ID for hero videos for analytics */

/*************************************************************************/
/*************************************************************************/	

function cfAutoPlayVideo(tempVidID, iterrationCNT) {		
	if (typeof cvVideos != "undefined" && tempVidID != "" && typeof cvVideos[tempVidID] != "undefined"  && cvVideos[tempVidID] != null) {
		cvVideos[tempVidID].target.playVideo();
	} else {
		if (iterrationCNT < 8) {
			setTimeout(function () {
				var iterrationCNT2 = iterrationCNT + 1;
				cfAutoPlayVideo(tempVidID, iterrationCNT2);
			}, 250);             
		}
	}
}

/*************************************************************************/
/*************************************************************************/	

/* Reduce padding on tabs until they're on the same line */

/*************************************************************************/
/*************************************************************************/	
	
setTimeout(function(){fixTabWidth()},1000);		


/*************************************************************************/
/*************************************************************************/	

/* Make hub page grids fluid for responsive. Set grid header and content */
/* columns the same height across the rows as they change for responsive */

/*************************************************************************/
/*************************************************************************/


var delay = (function() {
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

function resetGrid() {
	if ($('.originalGrid').length) {
		$('.majorHubGrid').remove();
		$('.originalGrid').show().attr('class', 'majorHubGrid');
	}		
	$('.majorHubGrid').clone().insertAfter('.majorHubGrid').attr('class', 'originalGrid').hide();
	$('.majorHubGrid').css('display', 'table');
	$('.majorHubGridRow').css('display', 'table-row');
	$('.majorHubGridColumn').css('display', 'table-cell');	
}

function gridHeaderHeight() {
	$('.majorHubGrid .majorHubGridRow').each(function(i, el) {
		height1 = $(this).find('.majorHubGridColumn .majorHubGridHeader:eq(0)').height();
		height2 = $(this).find('.majorHubGridColumn .majorHubGridHeader:eq(1)').height();
		height3 = $(this).find('.majorHubGridColumn .majorHubGridHeader:eq(2)').height();
		maxHeight = Math.max(height1, height2, height3);
		$(this).find('.majorHubGridColumn .majorHubGridHeader').height(maxHeight);
	});
}

function responsiveGrid() {
    var pause = 250;
    $(window).resize(function() {
        delay(function() {
            var width = $('.pageWidth').css('width');
            if (width == '1px') {           	
				resetGrid(); 
				gridHeaderHeight();      
				setTimeout(function(){fixTopNavWidth()},250);				
            } else if (width == '2px') {		
				resetGrid();
				$('.majorHubGrid .majorHubGridRow').each(function(i, el) {				
					if (i % 2 === 0) {
						$('<div class="majorHubGridRow" />').append($(this).find('.majorHubGridColumn:eq(2)')).insertAfter($(this));
					} else {
						$(this).prev().append($(this).find('.majorHubGridColumn:eq(0)'));
					}
				});		
				gridHeaderHeight();						
            } else if (width == '3px') {
				resetGrid();				
				$('.majorHubGrid').css('display', 'inline-block');
				$('.majorHubGridRow').css('display', 'inline-block');
				$('.majorHubGridColumn').css('display', 'inline-block');
				$('.majorHubGridHeaderContainer').css('display', 'inline-block');	
            }
        }, pause );
    });
    $(window).resize();
};


/**************************************************************************/
/**************************************************************************/

/* Feature Comparison Functions */

/**************************************************************************/
/**************************************************************************/

//Get parameter value
function getParam(key) {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars[key];
}

//Set compare checkboxes function
function setCompareFeatures() {
	var cbxCount = $("#featureheader :checkbox").size();
	var cbxState = $(this).is(":checked");
	
	//First see if max products already checked
	if (cbxState && $(".fccbxh" + cbxCount).is(":checked"))
	{
		$(this).removeAttr("checked");
		alert("Only four products may be compared");
	}
	//Toggle check ON
	else if (cbxState)
	{
		for (i = 1; i <= cbxCount; i++)
		{
			if($(".fccbxh" + i).attr("checked") != "checked")
			{
				$(".fccbxh" + i).attr("checked","checked");
				
				if ($("#featurecontent :checked").length > 1)
				{
					$("div #fcbtn").show();
					$("div #featurecomparebtn").show();
				}
				break;
			}
		}
	}
	//Toggle check OFF
	else
	{
		for (i = cbxCount; i > 0; i--)
		{
			if($(".fccbxh" + i).attr("checked") == "checked")
			{
				$(".fccbxh" + i).removeAttr("checked");
				break;
			}
		}
		
		if ($("#featurecontent :checked").length < 2)
		{
			$("div #fcbtn").hide();
			$("div #featurecomparebtn").hide();
		}
	}
}

//Compare Features
function compareFeatures() {
	var featurenewQS = "?";
	var prodCounter = 0;
	//var origPageQS = getOrigPageQS();
	
	$("#featurecontent :checked").each(function() {
		var prodIndex = $(this).index("#featurecontent :checked") + 1;
		var prodID = $(this).attr("id");
		
		prodCounter = prodCounter + 1;
		featurenewQS = featurenewQS + "prod" + prodIndex + "=" + prodID;
		
		if ($("#featurecontent :checked").length != prodCounter) {
			featurenewQS = featurenewQS + "&";
		}
	});
	
	var newURL = "http://" + window.location.hostname + "/en-us/products-site/Pages/test-rs-fctable.aspx" + featurenewQS;
	window.location = newURL;
}

/*************************************************************************/
/*************************************************************************/	

/* Makes spec tables inside tabs responsive */

/*************************************************************************/
/*************************************************************************/

function stopVid() {
	//$('.HeroItem').find('iframe').attr('src', '');
	$('.HeroItem').each(function(){	
		if ($(this).find('iframe').length > 0) {
			$(this).find('iframe').attr('src', '');
			$(this).find('.HeroContent').css('display', 'inline-block');
			$(this).find('.videoContainer').css('display', 'none');			
		}		
	});
}

function homeHeroVideo() {
/*
	var fr = 0;
	var pause = 250;
	$('.HeroContent').each(function(){	
		if ($(this).find('iframe').length > 0) {
			var vidSrc = $(this).find('iframe').attr('src') + '?autoplay=1';
			//var vidSrc = $(this).find('iframe').attr('src');
			$(this).find('.homepage-hero-img').prepend('<div class="heroVidBtn"><img src="../Images/home_page_hero_video_button.png"/*tpa=http://www.amd.com/Style Library/Images/home_page_hero_video_button.png*/ /></div>');
			if ($(this).find('iframe').find('videoContainer').length < 1) {
				$(this).find('iframe').wrap('<div class="videoContainer" id="yt' + fr + '"></div>');
				$(this).find('.videoContainer').prepend('<div class="homeHeroVidClose"><a href="#">Close video X</a></div>');				
			}						
			$(this).find('.homepage-hero-text').find('.hero_button').click(function(e) {	
				var p  = $(this).closest('.HeroItem').offset();			
				$(this).closest('.HeroItem').find('iframe').attr('src', vidSrc);
					$(this).closest('.HeroItem').find('.HeroContent').fadeOut('slow', function(){
						$(this).closest('.HeroItem').find('.videoContainer').fadeIn('slow', function(){
					});
				});	
			});					
			$(this).find('.videoContainer .homeHeroVidClose a').click(function(e) {
				$(this).closest('.HeroItem').find('.videoContainer').fadeOut('slow', function(){
					$(this).closest('.HeroItem').find('.HeroContent').fadeIn('slow', function(){	
						$(this).closest('.HeroItem').find('iframe').attr('src', '');										
					});
				});					
			});
			$(this).find('.videoContainer').insertAfter($(this));			
			$(this).find('.homepage-hero-img .heroVidBtn').click(function(e) {
				//console.log('herovideo');
				$(this).closest('.HeroItem').find('iframe').attr('src', vidSrc);
					$(this).closest('.HeroItem').find('.HeroContent').fadeOut('slow', function(){
						$(this).closest('.HeroItem').find('.videoContainer').fadeIn('slow', function(){
					});
				});	
			});
		}
		fr++;	
	});	
	var maxWidth = 960;
	$(window).resize(function() {		
		delay(function() {
			var vidWidth = $(this).find('.videoContainer').width();
			var vidHeight = $(this).find('.videoContainer').height();
			var vidRatio = ratio = maxWidth / vidWidth;			
			$(this).find('.videoContainer').width(vidWidth * vidRatio);
			$(this).find('.videoContainer').height((vidHeight * vidRatio));
		}, pause );
	});
	$(window).resize();	
*/	
}


function homeHeroVideoNew() {
/*
	$('.homePageHeroLink a').each(function(){	
		if($(this).attr('href') == '#video') {
			$(this).click(function(e) {
				$('.homePageHeroContainer').find('iframe').attr('src', vidSrc);
				$('.homePageHero').fadeOut('slow', function(){
					$('.homePageHeroContainer').find('.videoContainer').css('display', 'none');
					$('.homePageHeroContainer').prepend($('.videoContainer'));
					$('.homePageHeroContainer').find('.videoContainer').fadeIn('slow', function(){
					});
				});					
			});
		} else {
			$(this).click(function(e) {
				//e.stopPropagation();					
			});			
		}
	});	
	
	var pause = 250;	
	$('.homePageHeroVideo').css('display', 'none');	
	if ($('.homePageHeroVideo').find('iframe').length > 0) {
		$('.homePageHeroImage').css('cursor', 'pointer');
		$('.homePageHeroContent').css('cursor', 'pointer');
		var vidSrc = $('.homePageHeroVideo').find('iframe').attr('src') + '?autoplay=1';
		if ($('.homePageHeroVideo').find('iframe').find('videoContainer').length < 1) {
			$('.homePageHeroVideo').find('iframe').wrap('<div class="videoContainer" id="yt1"></div>');
			$('.homePageHeroVideo').find('.videoContainer').prepend('<div class="homeHeroVidCloseNew"><a href="#">Close video X</a></div>');				
		}
		//$('.homePageHeroContent').click(function(e) {			
		$('.homePageHeroImage').click(function(e) {
			$('.homePageHeroContainer').find('iframe').attr('src', vidSrc);
			$('.homePageHero').fadeOut('slow', function(){
				$('.homePageHeroContainer').find('.videoContainer').css('display', 'none');
				$('.homePageHeroContainer').prepend($('.videoContainer'));
				$('.homePageHeroContainer').find('.videoContainer').fadeIn('slow', function(){
				});
			});	
		});	
		$('.homePageHeroContent').click(function(e) {
			$('.homePageHeroContainer').find('iframe').attr('src', vidSrc);
			$('.homePageHero').fadeOut('slow', function(){
				$('.homePageHeroContainer').find('.videoContainer').css('display', 'none');
				$('.homePageHeroContainer').prepend($('.videoContainer'));
				$('.homePageHeroContainer').find('.videoContainer').fadeIn('slow', function(){
				});
			});	
		});						
	};		
	$('.videoContainer .homeHeroVidCloseNew a').click(function(e) {
		$('.videoContainer').fadeOut('slow', function(){
			$('.homePageHero').fadeIn('slow', function(){	
				$('.videoContainer').find('iframe').attr('src', '');										
			});
		});					
	});		
	var maxWidth = 960;
	$(window).resize(function() {		
		delay(function() {
			var vidWidth = $(this).find('.videoContainer').width();
			var vidHeight = $(this).find('.videoContainer').height();
			var vidRatio = ratio = maxWidth / vidWidth;			
			$(this).find('.videoContainer').width(vidWidth * vidRatio);
			$(this).find('.videoContainer').height((vidHeight * vidRatio));
		}, pause );
	});
	$(window).resize();		
*/	
}

/*************************************************************************/
/*************************************************************************/	

/* Makes tables inside tabs responsive */

/*************************************************************************/
/*************************************************************************/

var delayTable = (function() {
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();	

function responsiveTable() {
	var pause = 250;
	var dataText;
	if (!$('.resTable').length) {	
		$('.ms-rteTable-default').each(function() {	
			if (!$(this).hasClass('featuresTable') || !$(this).attr("id") == "featuresTable") {
				$(this).clone().removeClass('ms-rteTable-default').addClass('resTable').appendTo($(this).parent());
			}	
		});	
		var j=0;
		$('.resTable').each(function() {
			$(this).prepend('<h3>' + $(this).find('tr.tableTitle').text() + '</h3>');
			$(this).find('tr.tableTitle').remove();
			j++;
			$(this).find('tr').each(function() {
				$(this).find('th, td').each(function() {
					$(this).replaceWith('<div class="dataCell list">' + $(this).html() + '</div>');	
				});				
				$(this).before('<div class="row dataGroup">' + $(this).find(':first-child').html() + '</div>');			
				$(this).children().first().remove();		
				$(this).replaceWith('<div class="row dataRow">' + $(this).html() + '</div>'); 				
			});				
			$(this).find('.row:nth-child(2)').insertAfter($('.dataGroup')).attr('class', 'row dataLabelRow');		
			$(this).find('.dataRow .dataLabel:last-child').remove();
			$(this).find('.row').each(function() {
				var i=0;
				if (!$(this).hasClass('dataLabelRow') && !$(this).hasClass('dataGroup')) {					
					$(this).find('.dataCell').each(function() {
						$(this).attr('class', 'dataVal');
						if ($(this).parent().prev().find('.dataCell:eq(' + i + ')').find('img')) {
							dataText = $(this).parent().prev().find('.dataCell:eq(' + i + ')').find('img').attr('alt');
						} else {
							dataText = $(this).parent().prev().find('.dataCell:eq(' + i + ')').text();
						}
						$(this).before('<div class="dataCell dataLabel">' + dataText + ':</div>');
						i++;
					});
				}											
			});	
		});				
	}		
	$(window).resize(function() {	
		delayTable(function() {
			var width = $(window).width();
			if (width > 900) {		
				$('.ms-rteTable-default').css('display', 'block');
				$('.resTable').css('display', 'none');				
			} else if (width <= 900) {						
				$('.resTable').css('display', 'block');
				$('.ms-rteTable-default').css('display', 'none');
			}
		}, pause );
	});
	$(window).resize();
}	
	
/**************************************************************************/
/**************************************************************************/

/* Change layout of form tables for mobile devices */

/**************************************************************************/
/**************************************************************************/

function responsiveFormTable() {
	if ($('table.formTable') && $(window).width() <= 640) {
		$('table.formTable tr').each(function(){
			if ($(this).find(':first-child') && $(this).find(':first-child').html().length) {
				$(this).before('<tr class="r" id="r' + i + '"><td>' + $(this).find(':first-child').html() + '</td></tr>');
				$(this).not('.r').find('td:first-child').css('display', 'none');
			}
		});	
	} else if ($('table.formTable') && $(window).width() > 640) {
		$('table.formTable tr').each(function(){		
			if ($(this).hasClass('r')) {
				$(this).remove();
			}
			$(this).not('.r').find('td:first-child').css('display', '');
		});		
	}
}

/**************************************************************************/
/**************************************************************************/

/* Code that runs when page is loaded */

/**************************************************************************/
/**************************************************************************/

$(document).ready(function() { 	

/**************************************************************************/
/**************************************************************************/

/* Add gray bg color to hero when class is added to child on page */

/**************************************************************************/
/**************************************************************************/

	if ($('.productListingHubHero').find('.bannerGrayBg')) {
		$('.productListingHubHero').addClass('bannerGrayBg');	
		$('.productListingHubHero').find('.bannerGrayBg').addClass('bannerGrayText');
	}
	
/**************************************************************************/
/**************************************************************************/

/* Actions for the stacked heroes */

/**************************************************************************/
/**************************************************************************/	

	var heroNum = 0;
	var heroLen = $('.HeroItem.stacked').length;
	
	$('.HeroItem.stacked').each(function() {
	
		$(this).attr('id', 'hero' + (heroNum + 1));
	
		// Set hero content height to the height of the hidden image 
		
		if ($(this).find('img').height() > 50) {
		//if ($(this).find('.heroImage').find('img').length) {
			//console.log('heroImage img');
			$(this).find('img').hide();
			$(this).find('img').css('opacity', '0');
			$(this).find('.heroContent').css('height', $(this).find('img').height() + 'px');
			//$(this).find('img').hide();
			var contentHeight = $(this).find('img').height();						
		} else {
			//console.log('not cached');		
			$(this).find('img').load(function() {
			//console.log('image loaded');
				$(this).hide();
				$(this).css('opacity', '0');	
				var imgHeight = $(this).height();
				if (imgHeight > 50) {
					$(this).closest('.heroContent').css('height', imgHeight + 'px');	
					$(this).hide();
					var contentHeight = imgHeight;
				}
			});
				
		}

		// Create rollover action if hero contains a link or a YouTube video
		
		if ($(this).find('a').length || $(this).find('iframe[src*="youtube"]').length) {
			$(this).hover(function(){
			    $(this).find('.heroBg').css('opacity', '0.7');
			    $(this).css('cursor', 'pointer');
			    }, function(){
			    $(this).find('.heroBg').css('opacity', '1');
			    $(this).css('cursor', 'default');
			});
		}

		// Find hero image and set it as the background image of the hero
			
		var prodHubHeroBg = $(this).find('img').attr('src');
		heroNum++;
		if ($(this).find('.imageRight').length) {
			$(this).find('.heroBg').addClass('imageRight');
		} else if ($(this).find('.imageCenter').length) {
			$(this).find('.heroBg').addClass('imageCenter');
		}
		$(this).find('.heroBg').css('background-image', 'url(' + prodHubHeroBg + ')');
		
		// Append pointer graphic to end of CTA if hero contains link but NOT YouTube video
		
		if (!$(this).find('iframe[src*="youtube"]').length && $(this).find('.heroContent').parent('a').length) {
			$(this).find('h2').append('<span class="heroTitlePointer"><img src="../images/hero-title-pointer.png"/*tpa=http://www.amd.com/style library/images/hero-title-pointer.png*/></span>');
		}
		
		// If hero contains YouTube video create close bar, size video + create click actions to fade in/out hero content/video
									
	    $(this).find('.heroVidContainer').css('height', contentHeight);
	    $(this).find('.heroVidContainer').find('p').remove();
	    $(this).find('.heroVidContainer').prepend('<div class="closeVid"><div class="closeBox"><a href="#">Close X</a></div></div>').width();
		if ($(this).find('iframe[src*="youtube"]').length) {
			$(this).css('cursor', 'pointer');
			var thisVid = $(this).find('iframe[src*="youtube"]');
			//var vidSrc = thisVid.attr('src') + '?autoplay=1';
			var vidSrc = thisVid.attr('src');
			var initVidWidth = 968;
			var ratio = thisVid.width() / thisVid.height();	
			thisVid.width(initVidWidth).height(initVidWidth / ratio);
			$(this).click(function(e) {
				if(!$(e.target).closest('a').length) {
					$(this).css('cursor', 'default');
					//thisVid.attr('src', vidSrc);
					var targetVideoId  = parseUri(vidSrc).path.split("/");
					targetVideoId = targetVideoId[targetVideoId.length -1] || "";
					$(this).find('.heroBg').fadeOut('slow', function(){
						$(this).animate({height: thisVid.height()})
						$(this).next().fadeIn('slow', function(){							
						});
						cfAutoPlayVideo(targetVideoId,0);						
					});
				}		
			});
			$(this).find('.closeVid').width(initVidWidth);
			$(this).find('.closeVid a').click(function(e) {
				e.stopPropagation();
				e.preventDefault();				
				$(this).closest('.heroVidContainer').fadeOut('slow', function() {
					$(this).find('iframe[src*="youtube"]').attr('src', '');	
					$(this).closest('.heroVidContainer').prev().fadeIn('slow', function(){
						$(this).closest('.HeroItem').find('.heroVidContainer').find('iframe').attr('src', vidSrc);	
					});
					$(this).closest('.heroVidContainer').prev().animate({height: contentHeight});
				});							
			});    	
	    }  	
	});

/**************************************************************************/
/**************************************************************************/

/* Responsive for stacked hereos change margins + resize video */

/**************************************************************************/
/**************************************************************************/	

	$(window).resize(function() {
		$('.HeroItem.stacked').each(function(){
			if ($('.pageWidth').css('width') == '3px') {
			    var imgHeight = $(this).find('img').height();
			    if (imgHeight > 200) {
					$(this).find('.productHubHeroText').css('margin-top', imgHeight);
				}
			} else {
				$(this).find('.productHubHeroText').css('margin-top', '0');
			}
			var initVidWidth = 968;
			var pageWidth = $('.content').width();
			var thisVid = $(this).find('iframe[src*="youtube"]');
			var ratio = thisVid.width() / thisVid.height();
			if (thisVid.length && pageWidth <= initVidWidth) {
				thisVid.width(pageWidth).height(pageWidth / ratio);		
			} else {			
				thisVid.width(initVidWidth).height(initVidWidth / ratio);
			}
		    $(this).find('.heroVidContainer').css('height', thisVid.height() + $(this).find('.closeVid').outerHeight());
		    var closeWidth = thisVid.width() - $(this).find('.closeVid');
		    $(this).find('.closeVid').width(thisVid.width());
		
		});
	});

/**************************************************************************/
/**************************************************************************/

/* Remove link actions for headers on product hub page when no URL present */

/**************************************************************************/
/**************************************************************************/	
	
	$('.productHubCategory').each(function(){
		if ($(this).find('h3').find('a').attr('href').length <= 1 ) {
			$(this).find('h3').find('a').css({
				'color' : '#000',
				'cursor' : 'default'
			});			
		}
	});


/**************************************************************************/
/**************************************************************************/

/* Set 'resize' var to '0' when page is loaded. This is used so that code */
/* that runs on resize will only run the first time the viewport is resized */

/**************************************************************************/
/**************************************************************************/

	resize = 0;		
	var switched = 0;
	var resizeTimer = null;
		
/**************************************************************************/
/**************************************************************************/

/* Get the page's region code and add link to regional CSS in the header */

/**************************************************************************/
/**************************************************************************/
		
		var pageLang = $('html').attr('lang').toLowerCase();			
		if(pageLang.length > 0) {
			$('head').append('<link href="Unknown_83_filename"/*tpa=http://www.amd.com/Style Library/%27 + pageLang + %27.css*/ type="text/css" rel="stylesheet">');
		}
		
/**************************************************************************/
/**************************************************************************/

/* Function to capitalize each word in text */

/**************************************************************************/
/**************************************************************************/		
				
		$.fn.capitalise = function() {
		    return this.each(function() {
		        var $this = $(this),
		            text = $this.text(),
		            split = text.split(" "),
		            res = [],
		            i,
		            len,
		            component;
		        for (i = 0, len = split.length; i < len; i++) {
		            component = split[i];
		            res.push(component.substring(0, 1).toUpperCase());
		            res.push(component.substring(1));
		            res.push(" "); // put space back in
		        }
		        $this.text(res.join(""));
		    });
		};
		
		
		$('#notifyme').click(function(){
			LightBoxWindow($(this).attr("href"),700,700);
			return false;
		});

/**************************************************************************/
/**************************************************************************/

/* Functions you want to happen when users aren't in authoring */

/**************************************************************************/
/**************************************************************************/

if ($('input#MSOAuthoringConsole_FormContext').val() != '1') {

	responsiveTable();
	homeHeroVideo();
	homeHeroVideoNew();
	//productHubHeroVideo();
	//productDetailVideo();	
	responsiveGrid();

/**************************************************************************/
/**************************************************************************/

/* Show home page content when not using takeover hero */

/**************************************************************************/
/**************************************************************************/
	
	if (!$('#homePageHero').find('.HeroItem.stacked').length) {
		$('.homePageContent').show();
	}
	
/**************************************************************************/
/**************************************************************************/

/* Remove extraneous paragprah tags from video on product detail page */

/**************************************************************************/
/**************************************************************************/
		
	if ($('.productDetailVideo').length) {
		$('.productDetailVideo').hide();
		$('.productDetailVideo').find('p').remove();
	}
	
	if ($('.productDetailTwoColumnLink').length) {
		$('.productDetailVideo').hide();
		$('.productDetailVideo').find('p').remove();
	}
	
/**************************************************************************/
/**************************************************************************/

/* Hide space when no link entered for shop promo links on product detail page */

/**************************************************************************/
/**************************************************************************/
		
	$('.productDetailTwoColumnLink').each(function() {
		if (!$(this).find('a').length) {
			$(this).hide();
		}   	
	});
	
	function scalePageSection() {
		var viewWidth = $(window).width();
		var viewHeight = $(window).height() - $('#masthead').outerHeight() - $('#ms-designer-ribbon').outerHeight();
		var buttonHeight = $(this).find('.btnUp').outerHeight() + $(this).find('.btnDown').outerHeight();
		$('#fullScreenPromoPage').find('.pageSection').each(function() {
			$(this).css({
				'width' : '100%',
				'min-height' : viewHeight
			});
			$(this).find('.pageContentFull').css('min-height', viewHeight - buttonHeight);							    	
		});
	}
	

	$('.contentBoxColor').each(function() {
		if ($.trim($(this).find('.contentBoxColorTitle').text()).length < 1 && $.trim($(this).find('.contentBoxColorText').text()).length < 14) {		
			$(this).find('.contentBoxColorTitle').closest('.contentBoxColor').hide();
		}
	});
	
	//if ($.trim($('#productDetail .productDetailHeroText h2')).text()).length < 1 && $.trim($('#productDetail .productDetailHeroText p')).text()).length < 1) {
	
	//console.log('h2 length: ' + $.trim($('#productDetail .productDetailHeroText h2').text()).length);
	//console.log('p length: ' + $.trim($('#productDetail .productDetailHeroText p').text()).length);	
	//console.log('p hero length: ' + $.trim($('#productDetail .productDetailHero p').text()).length);
	
	if ($.trim($('#productDetail .productDetailHero p').text()).length < 6) {
		$('#productDetail .productDetailHero p').hide();
	}
	
	
	if ($.trim($('#productDetail .productDetailHeroText h2').text()).length < 1 && $.trim($('#productDetail .productDetailHeroText p').text()).length < 3) {
		$('#productDetail .productDetailHeroText').hide();
	}

/*	
	$('.productDetailProcCol').each(function() {	
		if ($(this).find('img').length < 1 && $.trim($(this).text()).length < 30) {		
			$(this).hide();
		}
	});
*/	
	
/**************************************************************************/
/**************************************************************************/

/* Code to scale hero to viewport size 
  (Ignition pages + homepage 'takeover') */

/**************************************************************************/
/**************************************************************************/

	$(window).load(function() {
		function scaleHero() {
			var viewWidth = $(window).width();
			if ($('#ms-designer-ribbon').length) {				
				var viewHeight = $(window).height() - $('#masthead').outerHeight() - $('#ms-designer-ribbon').outerHeight();		
			} else {
				var viewHeight = $(window).height() - $('#masthead').outerHeight();
			}
			var ratio = viewWidth / viewHeight;
			var contentWidth = (945 / viewWidth) * 100; 
			var initOffset = $('#fullScreenPromoPage').offset();
			var initPos = $('#fullScreenPromoPage').position();			
			var mastheadOffset = $('#masthead').outerHeight() + $('#ms-designer-ribbon').outerHeight();	
			
			function scaleIt(thisHero) {
			//console.log('scaleIt');

				 
				if ($('.pageWidth').css('width') != '3px') {
					if (ratio > 2.25) {	
						displayHeight = 'auto'; 
						displayWidth = viewWidth + "px";					
						thisHero.find('.heroBg').css('background-position', 'center 10%');		
					} else {				
						displayHeight = viewHeight + "px"; 
						displayWidth = 'auto';					
						thisHero.find('.heroBg').css('background-position', 'center 0');						
					}
					thisHero.css({
						'width' : '100%',
						'height' : viewHeight
					});			
					thisHero.find('.heroBg').css({
						'width' : '100%',
						'height' : viewHeight,
						'-moz-background-size' : displayWidth + " " + displayHeight,
						'-webkit-background-size' : displayWidth + " " + displayHeight,
						'-o-background-size' : displayWidth + " " + displayHeight,
						'background-size' : displayWidth + " " + displayHeight
					});						
					thisHero.find('.heroContent').css('height', viewHeight);					
					var heroOffset = thisHero.offset();
					var heroPosition = thisHero.position();
					var viewHalf = (viewHeight / 2) + mastheadOffset;
					var viewHalfTop = (viewHeight / 2) - mastheadOffset;					
					if (heroOffset.top < mastheadOffset && heroOffset.top > viewHalfTop) {	
						$('#s4-workspace').scrollTop(thisHero.position().top);
					} else if (heroOffset.top > mastheadOffset && heroOffset.top < viewHalf) {
						$('#s4-workspace').scrollTop(thisHero.position().top);
					}				
				}			
			}
			$('#fullScreenPromoPage').find('.HeroItem.stacked').each(function() {				
				scaleIt($(this));
			});	
			$('#homePageHero').find('.HeroItem.stacked').each(function() {				
				scaleIt($(this));
			});						
		}
			
		scaleHero();
		scalePageSection();

		$(window).resize(function() {		
			scaleHero();
			scalePageSection();			
	    });	
    });

/**************************************************************************/
/**************************************************************************/

/* Show/hide overlays in hero tiles on mouseover/mouseout */

/**************************************************************************/
/**************************************************************************/

	$('.tileHoverContent').each(function() {
		$(this).mouseover(function(e) {
	    	$(this).find('p').css('display', 'block');   	
	    });	    
		$(this).mouseout(function(e) {
	    	$(this).find('p').css('display', 'none');   	
	    });
	 });
    
/*************************************************************************/
/*************************************************************************/	

/* Hide headers when they contain no alphanumeric characters  */

/*************************************************************************/
/*************************************************************************/	

	$("h1, h2, h3").filter( function() {
	    return $.trim($(this).html()) == '';
	}).hide();	
		
/*************************************************************************/
/*************************************************************************/	

/* Check for NewsTicker items and animate them if they exist  */

/*************************************************************************/
/*************************************************************************/	
	
	if($('.newsticker').length > 0) {
		$('.newsticker').first().before('<span id="newsdisplay"></span>');
		$('.newsticker').hide();
		$('.newsticker').hide();
		newstick();
		setInterval(newstick, 7000); 	
	}
	
	if($('.QuoteRotator').length > 0) {
		$('.QuoteRotator').first().before('<div id="quotedisplay"></div>');
		$('.QuoteRotator').hide();
		$('div#quotedisplay').hide();
		quotetick();
		setInterval(quotetick, 15000); 	
	}
	
/**************************************************************************/
/**************************************************************************/

/* Feature Comparison */

/**************************************************************************/
/**************************************************************************/

	var featureQS = window.location.search.substring(1);
	
	if (featureQS) {
		$("#featuretable th").each(function() {
			var featureID = $(this).attr("id");

			//Check each th id to see if it exists in the query string
			if (featureQS.indexOf(featureID) != -1) {
				//If yes, do nothing, continue showing the column
			}
			else if (featureID != "techcolumn")
			{
				//otherwise hide the column
				var hideIndex = $("#" + featureID).index() + 1;
				$("#featuretable td:nth-child(" + hideIndex + "), th:nth-child(" + hideIndex + ")").hide();
			}
		});
	}

/*************************************************************************/
/*************************************************************************/	

/* Hide gray background heroes when they contain no content  */

/*************************************************************************/
/*************************************************************************/	
		
	if (!$('div.generalHero').find('img').length) {
		$('div.generalHero').css('display', 'none');
	}
	
	if (!$('div.minorHubHero').find('img').length) {
		$('div.minorHubHero').css('display', 'none');
	}
				
/*************************************************************************/
/*************************************************************************/	

/* Hide product icon divs when they contain no images  */

/*************************************************************************/
/*************************************************************************/
		
	if (!$('div.productDetailHeroIcon').find('img').length) {
		$('div.productDetailHeroIcon').css('display', 'none');
	}

	if (!$('div.productDetailHeroLink').find('a').length) {
		$('div.productDetailHeroLink').css('display', 'none');
	}
	
	if (!$('div.productDetailHero2Link').find('a').length) {
		$('div.productDetailHero2Link').css('display', 'none');
	}
	
/*************************************************************************/
/*************************************************************************/	

/* Hide secondary hero on product pages if no text + image present  */

/*************************************************************************/
/*************************************************************************/
	
	$('.productDetailHero2').each(function(){
		if ($(this).find('.productDetailHero2Text').text().length <= 76 && $(this).find('.productDetailHero2Image').find('img').length < 1) {		
			$(this).css('display', 'none');
		}
	});	

/*************************************************************************/
/*************************************************************************/	

/* Hide content rows on Game home page if no text + image present  */

/*************************************************************************/
/*************************************************************************/
		
	$('.gamePageRow').each(function(){				
		if (!$(this).find('p, span, ul, ol, a, img, iframe, table').length) {
			$(this).css('display', 'none');
		}		
	});			
	$('.pageContentLeft').each(function(){	
		if ($(this).find('.pageContentText').text().length <= 40 && $(this).find('.pageContentImage').find('img').length < 1) {	
			$(this).css('display', 'none');
		}		
	});	
	$('.pageContentRight').each(function(){	
		if ($(this).find('.pageContentText').text().length <= 40 && $(this).find('.pageContentImage').find('img').length < 1) {	
			$(this).css('display', 'none');
		}		
	});		
	
/*************************************************************************/
/*************************************************************************/	

/* Add DIV wrapper to YouTube/Flikr/Slideshare iframe for responsive */

/*************************************************************************/
/*************************************************************************/	
			
	yt = fk = ss = tw = fb = 0;

	$('iframe[src*="youtube"]').each(function() {
		if ($(this).parent().attr('class') != 'videoContainer') {
			yt++;
			$(this).before('<div class="videoContainer" id="yt' + yt + '"></div>');
			$('div#yt' + yt).append($(this));		
		}
    }); 
   
	$('iframe[src*="http://www.amd.com/Style%20Library/js/flickr.com"]').each(function() {
		if ($(this).parent().attr('class') != 'videoContainer') {	
			fk++;
			$(this).before('<div class="videoContainer" id="fk' + fk + '"></div>');
			$('div#fk' + fk).append($(this));
		}
    });  
  
	$('iframe[src*="http://www.amd.com/Style%20Library/js/slideshare.net"]').each(function() {
		if ($(this).parent().attr('class') != 'videoContainer') {	
			ss++;
			$(this).before('<div class="videoContainer" id="ss' + ss + '"></div>');
			$('div#ss' + ss).append($(this));
		}			
    }); 
	
/*************************************************************************/
/*************************************************************************/	

/* END NON-AUTHORING FUNCTIONS */

/*************************************************************************/
/*************************************************************************/	

}

/*************************************************************************/
/*************************************************************************/	

/* ACTIONS TO RUN ONLY ON AUTHORING MODE */

/*************************************************************************/
/*************************************************************************/	

if ($('input#MSOAuthoringConsole_FormContext').val() == '1') {

/**************************************************************************/
/**************************************************************************/

/* Set #s4-workspace overflow to visible in authoring mode */

/**************************************************************************/
/**************************************************************************/

	$('#s4-workspace').css('overflow-x', 'visible');	
}	

/*************************************************************************/
/*************************************************************************/	

/* END AUTHORING FUNCTIONS */

/*************************************************************************/
/*************************************************************************/	
		
var defaultval = $.trim($('div.keywordlabel').text());
$('input[name=SupportKeywords]').val(defaultval);
$('input[name=SupportKeywords]').focus(function() {
    if(this.value == defaultval) {
        this.value = '';
    }
});
$('input[name=SupportKeywords]').blur(function() {
    if(this.value == '') {
    }
});
	    
/**************************************************************************/
/**************************************************************************/

/* Change color for active text in search input */

/**************************************************************************/
/**************************************************************************/
	    	    
$('#search_csquery').focus(function() {
	$(this).addClass('searchFocus');
});	     
	        	    
/**************************************************************************/
/**************************************************************************/

/* Change top margin of masthead */

/**************************************************************************/
/**************************************************************************/

if($('#suiteBar').length){
	$('div#masthead').css("margin-top",0);
}
$('div.accordion-bar').toggle(
	function() {
 		$(this).siblings('div.accordion-content').slideDown('slow');
 		$(this).find('div.accordion-arrow').attr('class', 'accordion-arrow-open');
 	}, 
	function() { 
		$(this).siblings('div.accordion-content').slideUp('fast'); 
		$(this).find('div.accordion-arrow-open').attr('class', 'accordion-arrow');
		return false; 
	}
); 
$('div.page-footer-bar').toggle(
	function() {
 		$(this).siblings('div.page-footer-content').slideDown('slow');
 	}, 
	function() { 
		$(this).siblings('div.page-footer-content').slideUp('fast'); 
		return false; 
	}
); 
	 	
/**************************************************************************/
/**************************************************************************/

/* Display the top nav. Putting this in the layouts_functions so there */
/* aren't any race conditions */

/**************************************************************************/
/**************************************************************************/

	var urlStem = location.pathname.toString();
	var region = urlStem.substring(4,6);
	try{
	displayNewNav(region);
	}
	catch(err){
		if(window.console){
		//console.log("Top nav didn't display due to an error. \n" + err.message);
		}
	}
		$('http://www.amd.com/Style%20Library/js/div.view').hide(); 
		$('div.rwd_menu').click(function(e) {
		var thisMenu = $(this).find('http://www.amd.com/Style%20Library/js/div.view');
		  if(thisMenu.is(':visible'))
		  {
			  thisMenu.fadeOut('fast');
		  }
		  else {
			$('.view').hide();
		 	thisMenu.fadeIn('fast');
    		e.stopPropagation();
		 }
		 });
	$('li.topNavHeading').hoverIntent({
		over: navDown,
		out: navUp, 
		timeout: 100
	});	 
	var pagePath = urlStem.substring(6);
	var usOnlyPages = ["press-releases/Pages", "server/benchmarks"];
	var nonePresent = CheckIfNoneExist(pagePath, usOnlyPages);
	if(nonePresent){
		$('.topNavHeading').last().find('a').each(function(){
			var defurl = $(this).attr('href');
    		$(this).click(function(e){
    			e.preventDefault();
    			loadLanguagePage(defurl + pagePath, defurl);
    		});
    	});
    }

	ditchH1Spaces();

	var headerLink = "";
	
/**************************************************************************/
/**************************************************************************/

/* Get URL from anchor in grid headers and set it to click action for grid */

/**************************************************************************/
/**************************************************************************/
		
	$('.majorHubGridHeader').each(function(index) {	
		var headerLink = headerLink = $(this).find('a').attr('href');
		$(this).wrap('<a href="' + headerLink + '" class="majorHubGridHeaderLink"></a>');				
	});	
	$('.majorHubGridHeaderFull').each(function(index) {
		var headerLink = headerLink = $(this).find('a').attr('href');
		$(this).parent().parent().wrap('<a href="' + headerLink + '"></a>');
	});	
	
/**************************************************************************/
/**************************************************************************/

/* Wechat QR code popup for zh-cn */

/**************************************************************************/
/**************************************************************************/

	var wechatBtn = $('img[src$="Unknown_83_filename"/*tpa=http://www.amd.com/Style%20Library/js/wechat.png*/]');
	var popUp = $('<div class="wechatPopup"><div class="wechatPopupClose"><a href="#">关闭&nbsp;&nbsp;X</a></div></div>');	
	popUp.appendTo('body');
	
	function wechatPos() {
		if ($(wechatBtn).length) {
			$('.wechatPopup').css('display', 'block');
			var offset = $(wechatBtn).offset();
			var popupTop = offset.top - $('.wechatPopup').outerHeight();
			var popupLeft = offset.left;
			$('.wechatPopup').offset({ top: popupTop, left: popupLeft });
		}				
	}
	
	$(wechatBtn).on('click', function(e) {	
		e.preventDefault();	
		if ($('.wechatPopup').css('display') != 'none') {
			wechatPos();
		}
		$(wechatBtn).css('cursor', 'default');					
	});	
	
	$('.wechatPopupClose a').click(function(e){	
		e.preventDefault();
		$(wechatBtn).css('cursor', 'pointer');
		$('.wechatPopup').css('display', 'none');
	});
	
	$('#s4-workspace').scroll(function() {	
		if ($('.wechatPopup').css('display') != 'none') {
			wechatPos();
		}
	});
	
	$(window).resize(function() {
		if ($('.wechatPopup').css('display') != 'none') {
			wechatPos();
		}	
		if ($('.pageWidth').css('width') == '3px') {
			$(wechatBtn).css('cursor', 'pointer');
			$(wechatBtn).parent().attr('href', 'http://weixin.qq.com/r/snUtNWvEfjYBrS9W9yCg').attr('target','_blank');
			$(wechatBtn).on('click', function() {	
				$(wechatBtn).css('cursor', 'pointer');					
			});			
		} else {
			$(wechatBtn).parent().attr('href', '#').attr('target','_self');
			$(wechatBtn).on('click', function() {	
				wechatPos();	
				$(wechatBtn).css('cursor', 'default');					
			});		
		}
	});	
		
/**************************************************************************/
/**************************************************************************/

/* Hide button area on Great Page banners when no button present */

/**************************************************************************/
/**************************************************************************/
	
	if (hasCopy($('.greatPageHeroButtons').find('a'))) {
		$('.greatPageHeroButtons').css('display', 'inline-block');
	} else {
		$('.greatPageHeroButtons').css('display', 'none');
	}	
	
/**************************************************************************/
/**************************************************************************/

/* Stacked heroes handler */

/**************************************************************************/
/**************************************************************************/

	if ($('.HeroItem.stacked').length || $('.HeroItem.homePageHeroContainer').length) {	
		var heroes = $('.HeroItem');
		// Tosses it in .pageContentFull or .pageContent. If neither exists, it hides it. //
		var $heroesparent = heroes.parent("div");
		if($('.pageContentFull').length > 0) {
			$heroesparent.prependTo($('.pageContentFull:first')).css({"position":"relative"});
		}
	}
	
	$('.homePageHero').show();
	
/**************************************************************************/
/**************************************************************************/

/* Scalable heroes scrolling + snapping */

/**************************************************************************/
/**************************************************************************/

 
	
	
	

		
/**************************************************************************/
/**************************************************************************/

/* Hide homepage content/tiles if scalable 'takeover' hero is used */

/**************************************************************************/
/**************************************************************************/
	
	if ($('#homePageHero').find('.HeroItem.stacked').length) {	
		$('.homePageContentFull').hide();		
	}
		
/**************************************************************************/
/**************************************************************************/

/* Rotating heroes handler */

/**************************************************************************/
/**************************************************************************/
	
	if ($('.HeroItem').length && !$('.HeroItem.stacked').length && !$('.HeroItem.homePageHeroContainer').length) {
		var heroes = $('.HeroItem');
		heroes.hide();
		//tosses it in .pageContentFull or .pageContent. If neither exists, it hides it. 
		var $heroesparent = heroes.parent("div");
		if($('.pageContentFull').length > 0) {
			$heroesparent.prependTo($('div.pageContentFull:eq(0)')).css({"position":"relative"});
		} else if ($('.pageContent').length > 0){
			if ($('.pageContent.gameHome').length > 0) {
				$heroesparent.prependTo($('div.pageContent:eq(0)')).css({"position":"relative"});
			} else {
				$heroesparent.insertAfter($('h1')).css({"position":"relative"});
			}
		} else {
			$heroesparent.hide();
		}
		showHero(0, 2000);
		if (heroes.length > 1) {
			var heronav = "<div id='heronav'>";
			heroes.each(function(index) {
				var i = 1 + index;
				heronav = heronav + "<div class='herobtn' data-hero='" + index + "'>" + i + "</div>";
			});
			heronav = heronav + "</div>";
			heroes.last().after(heronav);
			$('#heronav').after('<div style="clear: both;"></div>');
			$('#heronav').find('div').eq(0).addClass('active');
		} else {
			heroes.last().after("<div class='herobtnEmpty'></div>");
		}
		var windowHash = window.location.hash.replace('#','');
		if (windowHash != ""){
			var hashIndex = $(".HeroContent").index($("." + windowHash));
			$('.herobtn').removeClass("active");
			$('.herobtn').eq(hashIndex).addClass("active");
			showHero(hashIndex, 1);
		}
	}	
		
	$('.herobtn').click(function(){		
		stopVid();
		$('.herobtn').removeClass("active");
		$(this).addClass("active");
		var heroIndex = $(this).data('hero');
		if(!$('.HeroItem').eq(heroIndex).hasClass('active')){			
			showHero(heroIndex, 1000);
		}
	});
	
	function showHero(heroIndex, speed) {
		var activeHero = $('.HeroItem').eq(heroIndex);			
		$('.HeroItem').css({"position":"absolute"}).removeClass('active').fadeOut(speed);
		activeHero.css({"position":"relative"}).fadeIn(speed).addClass('active');
		if ($('.pageFooter').length ){
			$('.pageFooter').find('p.HeroDisclaimer').remove();
			var disclaimer = $('<p class="HeroDisclaimer"></p>');
			disclaimer.html(activeHero.find('div.HeroDisclaimer').html());
			disclaimer.appendTo('.pageFooter:eq(0)').show();
		}
		else if($('#legalLabel').length) {
			$('#legalLabel').find('p.HeroDisclaimer').remove();
			var disclaimer = $('<p class="HeroDisclaimer"></p>');
			disclaimer.html(activeHero.find('div.HeroDisclaimer').html());
			$(disclaimer).appendTo('#legalLabel').show();
			$('#legalLabel').show();
		}
	}
	z=0;
	$('.pageContent#game .HeroItem').each(function() {		
		if ($(this).find('h2').text().length < 1) {
			$(this).find('.gameHeroText').css('padding', '0');
		}		
	});	
	
	$('.hubPageHeroContainer').prependTo($('#s4-bodyContainer'));

	if ($('.hubPageHeroBackground')) {
		var hubHeroBg = $('.hubPageHeroBackground').find('img').attr('src');
		$('.hubPageHero').css('background-image', 'url(' + hubHeroBg + ')');
	}
		
/*		
	if ($('.homePageHeroBackground')) {
		var heroBg = $('.homePageHeroBackground').find('img').attr('src');
		$('.homePageHero').css('background-image', 'url(' + heroBg + ')');
	}
*/	
	if ($('.saLandingPageHeroBackground')) {
		var heroImg = $('.saLandingPageHeroBackground').find('img').attr('src');
		$('.saLandingPageHero').css('background-image', 'url(' + heroImg + ')');
	}
	

/**************************************************************************/
/**************************************************************************/

/* Manual Rotator handler */

/**************************************************************************/
/**************************************************************************/
		
	if($('.RotatorItem').length) {
		var rotators = $('.RotatorItem');
		rotators.hide();
		var $rotatorsparent = rotators.parent("div");
		$rotatorsparent.css({"position":"relative"});
		showRotator(0, 2000);
		if(rotators.length > 1)
		{
		var rotatornav = "<div id='rotatornav' style='clear: both'>";
		rotators.each(function(index){
			var i = 1 + index;
			rotatornav = rotatornav + "<div class='rotatorbtn' data-rotator='" + index + "'>" + i + "</div>";
		});
		rotatornav = rotatornav + "</div>";
		rotators.last().after(rotatornav);
		$('#rotatornav').after('<div style="clear: both;"></div>');
		$('#rotatornav').find('div').eq(0).addClass('active');
		}
	}
	
	$('.rotatorbtn').one('click', rotatefunction);
	function rotatefunction(){
		$('.rotatorbtn').removeClass("active");
		$(this).addClass("active");
		var rotatorIndex = $(this).data('rotator');
		if(!$('.RotatorItem').eq(rotatorIndex).hasClass('active')){
				var activeRotator = $('.RotatorItem').eq(rotatorIndex);
				var outgoingRotator = $('.RotatorItem.active');
				if(outgoingRotator.index() < rotatorIndex)
				{
					outgoingRotator.css({"position":"absolute"}).removeClass('active').fadeOut(1000);
					activeRotator.css({"position":"relative"}).fadeIn(1000).addClass('active');
	
				}
				else
				{					
					outgoingRotator.css({"position":"absolute","top":"0","left":"0"}).removeClass('active').fadeOut(1000);
					activeRotator.css({"position":"relative","top":"0","left":"0"}).fadeIn(1000).addClass('active');
					/*
					outgoingRotator.css({"position":"absolute","top":"0","left":"0"}).removeClass("active").fadeOut(1000, function(){
					activeRotator.addClass("active").css({"position":"relative"});
					activeRotator.fadeIn(1000); 
					});*/
				}
			}
			setTimeout(function(){$('.rotatorbtn').one('click', rotatefunction)}, 0);
	}
	function showRotator(rotatorIndex, speed) {
		$('.RotatorItem.active').removeClass('active');
		$('.RotatorItem').eq(rotatorIndex).fadeIn(speed).addClass('active').css({"position":"relative"});
	}

	
/**************************************************************************/
/**************************************************************************/

/* Page footer accordion */

/**************************************************************************/
/**************************************************************************/

	if(!$('div.ms-formfieldvaluecontainer').length) {	
		footerAccordion = '<div class="footerAccordion">';
		footerAccordion += '<a href="#" id="footerLink" class="footerClosed">';	
		footerAccordion += $('div#legalLabel').text();
		footerAccordion += '</a>';	
		footerAccordion += '</div>';	
		
		/* Check if pageFooter in authoring mode contains following tags and the scalable 'takeover' hero isn't being used */
		
		if (hasCopy($('.pageFooter').find('p, ul, ol, a')) && !$('#homePageHero').find('.HeroItem.stacked').length) {		
			//if ($('.pageFooter').find('p').length > 1) {			
			
				//$('.footer').before(footerAccordion);
				$('.footerContainer').before(footerAccordion);
				$('.footer').css('margin-top','0');
				$('.pageFooter').css('display','none');
				$('.pageFooter').insertAfter('.footerAccordion');
				$('.pageFooter').find('strong').contents().unwrap();
			//} else {
			//	$('.pageFooter').css('display', 'none');
			//}			
		} else {
			$('.pageFooter').css('display', 'none');
		}
		$('a#footerLink').mouseover(function() {
			if ($(this).hasClass('footerClosed')) {
				$(this).removeClass('footerClosed');	
				$(this).addClass('footerClosedOver');
			} else {
				$(this).removeClass('footerOpen');	
				$(this).addClass('footerOpenOver');		
			}			
		});	
		$('a#footerLink').mouseout(function() {
			if ($(this).hasClass('footerClosedOver')) {
				$(this).removeClass('footerClosedOver');	
				$(this).addClass('footerClosed');
			} else {
				$(this).removeClass('footerOpenOver');	
				$(this).addClass('footerOpen');		
			}			
		});	
		$('a#footerLink').toggle(function(e) {
			$('.pageFooter').slideDown('slow');		
			$(this).removeClass('footerClosedOver');	
			$(this).addClass('footerOpenOver');
		}, function() {	
			$('.pageFooter').slideUp('slow');
			$(this).removeClass('footerOpenOver');
			$(this).addClass('footerClosedOver');
		});
	}	
	
/*************************************************************************/
/*************************************************************************/

/* Align images with corresponding list item on Corporate History page  */

/*************************************************************************/
/*************************************************************************/	
	
	if($('.historyList')) {	
		$('.historyContent img').each(function(){						
			var parentItem = $(this).parent('li').prev('li');
			var parentText = $(this).parent('li').prev('li').text();						
			var newHTML = "<div class='historyImg'><div style='float:left'><image src='" + $(this).prop('src') + "'></div><div><ul><li>" + parentText + "</li></ul></div></div>"			
			$(parentItem).replaceWith(newHTML);			
			$(this).parent('li').css({display: 'none'});
		});
	}	
	
	
      $('http://www.amd.com/Style%20Library/js/h3.head').click(function(e){
        e.preventDefault();
	
			$('#accordion .content').slideUp();
			$('#accordion h3').removeClass('active');
		
        $(this).closest('li').find('.content').slideToggle().toggleClass('expanded');
		$(this).toggleClass('active');
      });

      
      $('h3#title-1').click(function(e){
        e.preventDefault();
        	$('#numbers .number').removeClass('active');
	     $('div#hotspot-1').toggleClass('active');      
      });
      
      $('h3#title-2').click(function(e){
        e.preventDefault();
        	$('#numbers .number').removeClass('active');
	     $('div#hotspot-2').toggleClass('active');      
      });
      
      $('h3#title-3').click(function(e){
        e.preventDefault();
        	$('#numbers .number').removeClass('active');
	     $('div#hotspot-3').toggleClass('active');      
      });
      
      $('h3#title-4').click(function(e){
        e.preventDefault();
        	$('#numbers .number').removeClass('active');
	     $('div#hotspot-4').toggleClass('active');      
      });
      
      $('h3#title-5').click(function(e){
        e.preventDefault();
        	$('#numbers .number').removeClass('active');
	     $('div#hotspot-5').toggleClass('active');      
      });
      
      $('h3#title-6').click(function(e){
        e.preventDefault();
        	$('#numbers .number').removeClass('active');
	     $('div#hotspot-6').toggleClass('active');      
      });
      
      $('div#hotspot-1').click(function(e){
        e.preventDefault();
        	$('#accordion .content').slideUp();
			$('#accordion h3').removeClass('active');
			$('#numbers .number').removeClass('active');
        $('#content-1').slideToggle();
        $(this).toggleClass('active');
        $('h3#title-1').toggleClass('active');
        
       });
       
       $('div#hotspot-2').click(function(e){
        e.preventDefault();
        	$('#accordion .content').slideUp();
			$('#accordion h3').removeClass('active');
			$('#numbers .number').removeClass('active');
        $('#content-2').slideToggle();
        $(this).toggleClass('active');
        $('h3#title-2').toggleClass('active');
        
       });
       
       $('div#hotspot-3').click(function(e){
        e.preventDefault();
        	$('#accordion .content').slideUp();
			$('#accordion h3').removeClass('active');
			$('#numbers .number').removeClass('active');
        $('#content-3').slideToggle();
        $(this).toggleClass('active');
        $('h3#title-3').toggleClass('active');
        
       });
       
       $('div#hotspot-4').click(function(e){
        e.preventDefault();
        	$('#accordion .content').slideUp();
			$('#accordion h3').removeClass('active');
			$('#numbers .number').removeClass('active');
        $('#content-4').slideToggle();
        $(this).toggleClass('active');
        $('h3#title-4').toggleClass('active');
        
       });
       
       $('div#hotspot-5').click(function(e){
        e.preventDefault();
        	$('#accordion .content').slideUp();
			$('#accordion h3').removeClass('active');
			$('#numbers .number').removeClass('active');
        $('#content-5').slideToggle();
        $(this).toggleClass('active');
        $('h3#title-5').toggleClass('active');
        
       });
       
       $('div#hotspot-6').click(function(e){
        e.preventDefault();
        	$('#accordion .content').slideUp();
			$('#accordion h3').removeClass('active');
			$('#numbers .number').removeClass('active');
        $('#content-6').slideToggle();
        $(this).toggleClass('active');
        $('h3#title-6').toggleClass('active');
        
       });
       
			

	
/*************************************************************************/
/*************************************************************************/	

/* Hide content area when it contains no content  */

/*************************************************************************/
/*************************************************************************/	
		
	$('.columnContainer').each(function(){
		if (!$(this).find('h1, h2, h3, p, span, ul, ol, a, img, iframe, table').length) {
			$(this).css('display', 'none');
		}	
		if ($(this).find('.landingPageThreeColumn').length && $(this).find('.landingPageThreeColumn').find('h3').text().length < 1 && !$(this).find('.landingPageThreeColumn').find('p, span, ul, ol, a, img, iframe, table').length) {
			$(this).css('display', 'none');
		}
		if ($(this).find('.landingPageThreeColumnLast').length && $(this).find('.landingPageThreeColumnLast').find('h3').text().length < 1 && !$(this).find('.landingPageThreeColumnLast').find('p, span, ul, ol, a, img, iframe, table').length) {
			$(this).css('display', 'none');
		}
		
		if ($(this).find('.twoColumnLeft .columnText').length && !$(this).find('.twoColumnLeft .columnText').find('h1, h2, h3, p, span, ul, ol, a, img, iframe, table').length) {
			$(this).css('display', 'none');
		}
	});
			
/*************************************************************************/
/*************************************************************************/	

/* Binds handlers to left nav for click and hover functionality */
/* on mobile devices */

/*************************************************************************/
/*************************************************************************/	
	
	$('#left-col li a.selected').bind('click', function(e) {
		e.preventDefault();
		clickSelected(this);												
	});	
	
	$('#left-col li').bind('mouseover', function(e) {
		e.preventDefault();
		hoverLeftNav(this);												
	});	
	   	

$('#s4-workspace').scroll(function() {
	var scrollTop = $(this).scrollTop();
    $('.homePageHero').css('background-position', 'center ' + ((scrollTop * 0.25)) + 'px');	
});

$('#s4-workspace').scroll(function() {
	var scrollTop = $(this).scrollTop();
    $('.saLandingPageContainer').css('background-position', 'center ' + ((scrollTop * 0.25)) + 'px');  	
});


/*************************************************************************/
/*************************************************************************/	

/* Code that's called when viewport is resized */

/*************************************************************************/
/*************************************************************************/	

	$(window).resize(function(e) {	
	
		if ($('.homePageHeroImage').length > 0) {
			var p = $('.homepageTiles');
			var position = p.position();		
			var z = $('.homePageHeroImage');
			var positionz = z.position();
					
			if($(window).width() > 1000) {		
				$('.homePageHeroImage').css('left', '0');			
			} else if ($(window).width() <= 1000 && $(window).width() > 780) {			
				$('.homePageHeroImage').css('left', (position.left - 450) + 'px');
			} else if ($(window).width() <= 780 && $(window).width() > 640) {			
				$('.homePageHeroImage').css('left', (position.left - 550) + 'px');				
			} else if ($(window).width() <= 640 && $(window).width() > 450) {	
				$('.homePageHeroImage').css('left', '-590px');									
			} else if ($(window).width() <= 450) {		
				$('.homePageHeroImage').css('left', '-690px');
			}
		}
				
/*************************************************************************/
/*************************************************************************/	

/* Move current page to the top of the left nav for mobile devices */

/*************************************************************************/
/*************************************************************************/

		if ($('.pageWidth').css('width') == '3px') {		
			$('.container').prepend($('h2'));
			if ($('#left-col li.selected').length == 1) {
				$('#left-col li.selected').hide().clone(true).prependTo($('#left-col ul:first-child')).addClass('mobileTopItem').show();
			}	
		} else {
			$('#left-col li.selected.mobileTopItem').remove();
			$('#left-col li.selected').show();
		}
	
/*************************************************************************/
/*************************************************************************/	

/* Place hero images above hero text on mobile (Hero images were placed */
/* after text as workaround for disappearing image bug in SharePoint) */

/*************************************************************************/
/*************************************************************************/			

	});
	
});




$(window).load(function() {
	if ($('input#MSOAuthoringConsole_FormContext').val() == '1') {

/**************************************************************************/
/**************************************************************************/

/* Functions that run ONLY in authoring mode */

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/
/**************************************************************************/

/* Hide AddThis in authoring mode */

/**************************************************************************/
/**************************************************************************/
	
		$('.addthis-smartlayers').css('display', 'none');
		$('#at4-share').css('display', 'none');
		$('#at4-follow').css('display', 'none');
		$('#at4-whatsnext').css('display', 'none');
		$('#at4-thankyou').css('display', 'none');
				
	}
});	