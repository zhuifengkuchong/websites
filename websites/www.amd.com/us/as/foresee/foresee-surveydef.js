FSR.surveydefs = [{
    name: 'browse',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
	
		locales: [{
            locale: 'en',
			sp: 50,
            lf: 3
        }, {
            locale: 'la',
            sp: 50,
            lf: 3
        }, {
            locale: 'br',
            sp: 50,
            lf: 3
        }, {
            locale: 'de',
            sp: 50,
            lf: 3
        }, {
            locale: 'es',
            sp: 50,
            lf: 3
        }, {
            locale: 'fr',
            sp: 50,
            lf: 3
        }, {
            locale: 'it',
            sp: 50,
            lf: 3
        }, {
            locale: 'ru',
            sp: 50,
            lf: 3
        }, {
            locale: 'jp',
            sp: 50,
            lf: 3
        }, {
            locale: 'kr',
            sp: 50,
            lf: 3
        }, {
            locale: 'cn',
            sp: 50,
            lf: 3
        }, {
            locale: 'tw',
            sp: 50,
            lf: 3
        }]
		
    },
    include: {
        urls: ['.']
    }
}];
FSR.properties = {
    repeatdays: 90,
    
    repeatoverride: false,
    
    altcookie: {},
    
    language: {
    
        locale: 'en',
        src: 'location',
        locales: [{
            match: ['/us/', '/uk/', '/au/', '/hk/', '/in/', '/sg/', '/my/', '/ca/'],
            locale: 'en'
        }, {
            match: ['/la/', '/es-la/', '/es-xl/', '/en-mx/', '/es-mx/'],
            locale: 'la'
        }, {
            match: ['/br/', '/pt-br/'],
            locale: 'br'
        }, {
            match: ['/de/', '/de-de/'],
            locale: 'de'
        }, {
            match: ['/es/', '/es-es/'],
            locale: 'es'
        }, {
            match: ['/fr/', '/fr-fr/'],
            locale: 'fr'
        }, {
            match: ['/it/', '/it-it/'],
            locale: 'it'
        }, {
            match: ['/ru/', '/ru-ru/'],
            locale: 'ru'
        }, {
            match: ['/jp/', '/ja-jp/'],
            locale: 'jp'
        }, {
            match: ['/kr/', '/ko-kr/'],
            locale: 'kr'
        }, {
            match: ['/cn/', '/zh-cn/', '.cn', 'http://www.amd.com/us/as/foresee/querycn.php'],
            locale: 'cn'
        }, {
            match: ['/tw/', '/zh-tw/'],
            locale: 'tw'
        }]
    },
    
    exclude: {
        cookies: [{
            name: 'sPageVisited2',
            value: 'true'
        }]
    },
    
    zIndexPopup: 10000,
    
    ignoreWindowTopCheck: false,
    
    ipexclude: 'fsr$ip',
    
    mobileHeartbeat: {
        delay: 60, /*mobile on exit heartbeat delay seconds*/
        max: 3600 /*mobile on exit heartbeat max run time seconds*/
    },
    
    invite: {
    
        // For no site logo, comment this line:
        siteLogo: "sitelogo.gif"/*tpa=http://www.amd.com/us/as/foresee/sitelogo.gif*/,
        
        //alt text fore site logo img
        siteLogoAlt: "",
        
        /* Desktop */
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback",
            
            locales: {
                // Latin America
                "la": {
                    headline: "¡Agradeceremos sus comentarios!",
                    blurb: "Gracias por visitar nuestro sitio web. Ha sido seleccionado para participar en una breve encuesta de satisfacción del cliente para permitirnos conocer cómo mejorar su experiencia.",
                    noticeAboutSurvey: "La encuesta está diseñada para medir su experiencia total, échele un vistazo cuando <u>finalice</u> su visita.",
                    attribution: "Esta encuesta es realizada por una empresa independiente llamada ForeSee, en representación del sitio que está visitando.",
                    closeInviteButtonText: "No, gracias",
                    declineButton: "No, gracias",
                    acceptButton: "Sí, proporcionaré mis comentarios"
                },
                // Brazil
                "br": {
                    headline: "Gostaríamos de receber os seus comentários!",
                    blurb: "Obrigado por ter visitado o nosso sítio da Web. Foi seleccionado para participar de um inquérito de satisfação de cliente, para nos informar como podemos melhorar a sua experiência.",
                    noticeAboutSurvey: "O inquérito foi concebido para avaliar toda a sua experiência do sítio da Web. Procure-o na <u>conclusão</u> da sua visita.",
                    attribution: "Este inquérito é conduzido por uma empresa independente, a ForeSee, em nome do sítio da Web que está a visitar.",
                    closeInviteButtonText: "Não obrigado",
                    declineButton: "Não obrigado",
                    acceptButton: "Sim, farei os meus comentários"
                },
                // German
                "de": {
                    headline: "Wir würden uns über Ihre Meinung freuen!",
                    blurb: "Vielen Dank für Ihren Besuch auf unserer Website. Sie wurden zufällig ausgewählt, an einer Umfrage zur Kundenzufriedenheit teilzunehmen, anhand der wir nachvollziehen können, wie wir unsere Website noch verbessern können.",
                    noticeAboutSurvey: "Mithilfe der Umfrage soll Ihre gesamte Erfahrung mit der Seite beurteilt werden. Bitte sehen Sie sich die Umfrage am <u>Ende</u> Ihres Besuchs an.",
                    attribution: "Diese Umfrage wird vom unabhängigen Unternehmen ForeSee im Auftrag der Betreiber dieser Seite durchgeführt.",
                    closeInviteButtonText: "Nein, Danke",
                    declineButton: "Nein, Danke",
                    acceptButton: "Ja, ich gebe ein Feedback ab"
                },
                // Spanish Traditional
                "es": {
                    headline: "¡Nos gustaría saber tu opinión!",
                    blurb: "Gracias por visitar nuestro sitio Web. Has sido elegido para participar en una breve encuesta de satisfacción del cliente que nos permitirá saber cómo podemos mejorar tu experiencia.",
                    noticeAboutSurvey: "La encuesta está diseñada para medir tu experiencia y aparecerá al <u>final</u> de tu visita.",
                    attribution: "Esta encuesta la realiza una firma independiente, ForeSee, en nombre del sitio que estás visitando.",
                    closeInviteButtonText: "No, gracias",
                    declineButton: "No, gracias",
                    acceptButton: "Sí, daré mi opinión"
                },
                // French
                "fr": {
                    headline: "Nous aimerions connaître votre opinion!",
                    blurb: "Merci de votre visite de notre site. Vous avez été choisi au hasard pour participer à une étude de satisfaction de clientèle qui nous permettra de savoir comment améliorer votre expérience sur notre site web.",
                    noticeAboutSurvey: "L'étude est conçue pour mesurer la totalité de votre opinion sur notre site et apparaîtra à la <u>fin de votre visite.</u>",
                    attribution: "Cette étude est réalisée par ForeSee, une société indépendante, pour le compte du site que vous visitez actuellement.",
                    closeInviteButtonText: "Non merci",
                    declineButton: "Non merci",
                    acceptButton: "Oui, je souhaite donner mon opinion"
                },
                // Italian
                "it": {
                    headline: "Vorremmo la tua opinione!",
                    blurb: "Grazie per aver visitato il nostro sito Web. Sei stato selezionato per partecipare a un breve sondaggio sulla soddisfazione dei clienti, per farci sapere se possiamo migliorare la loro esperienza.",
                    noticeAboutSurvey: "Il sondaggio mira a valutare l'intera esperienza: cercalo al <u>termine</u> della tua visita.",
                    attribution: "Il sondaggio è condotto dall'azienda indipendente ForeSee, per conto del sito che stai visitando.",
                    closeInviteButtonText: "No, grazie",
                    declineButton: "No, grazie",
                    acceptButton: "Sì, darò il mio feedback"
                },
                // Russian
                "ru": {
                    headline: "Нам интересно Ваше мнение.",
                    blurb: "Благодарим Вас за то, что зашли на наш сайт. Вас выбрали для участия в исследовании удовлетворенности клиентов, чтобы мы могли узнать, как сделать наш сайт лучше.",
                    noticeAboutSurvey: "Цель опроса — оценка Ваших впечатлений от пребывания на сайте. Опрос можно будет пройти <u>по окончании</u> посещения сайта.",
                    attribution: "Опрос проводится независимой компанией ForeSee от имени владельца данного сайта.",
                    closeInviteButtonText: "Нет, спасибо",
                    declineButton: "Нет, спасибо",
                    acceptButton: "Да, я поделюсь своим мнением"
                },
                // Japanese
                "jp": {
                    headline: "お客様のフィードバックをお送りください！",
                    blurb: "当ウェブサイトにアクセスいただき、ありがとうございます。お客様は簡単な顧客満足度調査の回答者として選出されました。お客様のエクスペリエンス改善に役立つご意見・ご希望をお寄せください。",
                    noticeAboutSurvey: "この調査は、お客様のエクスペリエンス全体に対する評価を把握するため作られ、<u>閲覧の最後に</u>表示されます。",
                    attribution: "この調査は、現在ご覧のサイトの代理として、独立企業である ForeSee Results が実施するものです。",
                    closeInviteButtonText: "いいえ、結構です",
                    declineButton: "いいえ、結構です",
                    acceptButton: "はい、フィードバックを送ります"
                },
                // Korean
                "kr": {
                    headline: "귀하의 소중한 피드백을 기다립니다!",
                    blurb: "당사의 웹 사이트를 방문해 주셔서 감사합니다. 귀하께서는 웹 사이트 개선 방안을 알아보기 위한 간단한 고객 만족도 설문 조사 참여자로 선정되셨습니다.",
                    noticeAboutSurvey: "본 설문 조사는 전체 사이트에 대한 이용 소감을 알아보기 위한 것으로, <u>방문이 끝날 때</u> 표시됩니다.",
                    attribution: "본 설문 조사는 귀하가 방문한 사이트를 대신하여 개인 업체인 ForeSee에서 실행합니다.",
                    closeInviteButtonText: "참여하지 않겠습니다",
                    declineButton: "참여하지 않겠습니다",
                    acceptButton: "예, 피드백을 제공하겠습니다"
                },
                // Chinese (Simplified Chinese)
                "cn": {
                    headline: "欢迎您提供反馈意见！",
                    blurb: "感谢您访问我们的网站。 为了解如何才能改善用户体验，我们挑选了一组用户参加客户满意度调查，您有幸被选中了。",
                    noticeAboutSurvey: "本次调查的目的是评估您的总体体验，请在<u>结束本次访问</u>时查看调查问卷。",
                    attribution: "本次调查由独立公司 ForeSee 代表本网站举办。",
                    closeInviteButtonText: "不，谢谢",
                    declineButton: "不，谢谢",
                    acceptButton: "好的，我愿意提供反馈意见"
                },
                // Taiwan (Traditional Chinese)
                "tw": {
                    headline: "歡迎您提供意見回饋！",
                    blurb: "感謝您造訪我們的網站。您已獲選參加客戶滿意度意見調查，以協助我們瞭解如何改善您的使用體驗。",
                    noticeAboutSurvey: "本意見調查旨在評估您的整體使用體驗，將會在<u>您要離開本網站</u>時出現。",
                    attribution: "本意見調查是由獨立的公司 ForeSee 代表本網站進行。",
                    closeInviteButtonText: "不，謝謝",
                    declineButton: "不，謝謝",
                    acceptButton: "是的，我願意提供意見回饋"
                }
            }
        }]],
        
        exclude: {
            urls: [],
            referrers: [],
            userAgents: [],
            browsers: [],
            cookies: [],
            variables: []
        },
        include: {
            local: ['.']
        },
        
        delay: 0,
        timeout: 0,
        
        hideOnClick: false,
        
        hideCloseButton: false,
        
        css: 'foresee-dhtml.css'/*tpa=http://www.amd.com/us/as/foresee/foresee-dhtml.css*/,
        
        hide: [],
        
        hideFlash: false,
        
        type: 'dhtml',
        /* desktop */
        // url: 'http://www.amd.com/us/as/foresee/invite.html'
        /* mobile */
        url: 'http://www.amd.com/us/as/foresee/invite-mobile.html',
        back: 'url'
    
        //SurveyMutex: 'SurveyMutex'
    },
    
    tracker: {
        width: '690',
        height: '415',
        timeout: 3,
        adjust: true,
        alert: {
            enabled: true,
            message: 'The survey is now available.',
            
            locales: [{
                locale: 'la',
                message: 'Ahora está disponible su encuesta.'
            }, {
                locale: 'br',
                message: 'O seu inquérito está agora disponível.'
            }, {
                locale: 'de',
                message: 'Ihre Umfrage ist jetzt verfügbar.'
            }, {
                locale: 'es',
                message: 'La encuesta está ahora disponible.'
            }, {
                locale: 'fr',
                message: 'Votre étude est maintenant disponible.'
            }, {
                locale: 'it',
                message: 'Ora è disponibile il tuo sondaggio.'
            }, {
                locale: 'ru',
                message: 'Теперь можно перейти к опросу.'
            }, {
                locale: 'jp',
                message: '調査を実施する準備ができました。 '
            }, {
                locale: 'kr',
                message: '이제 설문 조사에 참여할 수 있습니다. '
            }, {
                locale: 'cn',
                message: '现在可以参加调查了。 '
            }, {
                locale: 'tw',
                message: '您現在可以開始填寫意見調查問卷。 '
            }]
        },
        
        locales: [{
            locale: 'en',
            url: 'http://www.amd.com/us/as/foresee/tracker.html'
        }, {
            locale: 'la',
            url: 'http://www.amd.com/us/as/foresee/tracker_LA.html'
        }, {
            locale: 'br',
            url: 'http://www.amd.com/us/as/foresee/tracker_BR.html'
        }, {
            locale: 'de',
            url: 'http://www.amd.com/us/as/foresee/tracker_DE.html'
        }, {
            locale: 'es',
            url: 'http://www.amd.com/us/as/foresee/tracker_ES.html'
        }, {
            locale: 'fr',
            url: 'http://www.amd.com/us/as/foresee/tracker_FR.html'
        }, {
            locale: 'it',
            url: 'http://www.amd.com/us/as/foresee/tracker_IT.html'
        }, {
            locale: 'ru',
            url: 'http://www.amd.com/us/as/foresee/tracker_RU.html'
        }, {
            locale: 'jp',
            url: 'http://www.amd.com/us/as/foresee/tracker_JP.html'
        }, {
            locale: 'kr',
            url: 'http://www.amd.com/us/as/foresee/tracker_KR.html'
        }, {
            locale: 'cn',
            url: 'http://www.amd.com/us/as/foresee/tracker_CN.html'
        }, {
            locale: 'tw',
            url: 'http://www.amd.com/us/as/foresee/tracker_TW.html'
        }]
    },
    
    survey: {
        width: 690,
        height: 600
    },
    
    qualifier: {
        footer: '<div div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="http://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
        width: '690',
        height: '500',
        bgcolor: '#333',
        opacity: 0.7,
        x: 'center',
        y: 'center',
        delay: 0,
        buttons: {
            accept: 'Continue'
        },
        hideOnClick: false,
        css: 'foresee-dhtml.css'/*tpa=http://www.amd.com/us/as/foresee/foresee-dhtml.css*/,
        url: 'http://www.amd.com/us/as/foresee/qualifying.html'
    },
    
    cancel: {
        url: 'http://www.amd.com/us/as/foresee/cancel.html',
        width: '690',
        height: '400'
    },
    
    pop: {
        what: 'survey',
        after: 'leaving-site',
        pu: false,
        tracker: true
    },
    
    meta: {
        referrer: true,
        terms: true,
        ref_url: true,
        url: true,
        url_params: false,
        user_agent: false,
        entry: false,
        entry_params: false
    },
    
    events: {
        enabled: true,
        id: true,
        codes: {
            purchase: 800,
            items: 801,
            dollars: 802,
            followup: 803,
            information: 804,
            content: 805
        },
        pd: 7,
        custom: {}
    },
    
    previous: false,
    
    analytics: {
        google_local: false,
        google_remote: false
    },
    
    cpps: {
       GA_UID: {
            source: 'function',
            value: function(){
                var out = "";
                if (typeof ga != 'undefined' && ga != null && typeof ga.getAll()[0] != 'undefined' && ga.getAll()[0] != null) {
                    out += ga.getAll()[0].get('clientId');
                }
                return out;
            }
        }
    },
    
    mode: 'first-party'
};
