/*
 * YouTube scode
 * Last Update by Ahmad @ 08:30 EST 03/09/2015
 */

var YTplayersLoaded=cvVideoDuration=cvCurrentVideoTime=cvVideoSegment1=cvVideoSegment2=cvVideoSegment3=cvVideoSegment4=0;
var cvYTevent = new Array();
var cvYTeventID = new Array();
var cvonYouTubeIframeAPIReady="";
var tag = document.createElement('script');
tag.src = "../../../../www.youtube.com/iframe_api.js"/*tpa=https://www.youtube.com/iframe_api*/;

var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var videoArray = new Array();
var playerArray = new Array();

(function($) {
	function trackYouTube()
	{
		var i = 0;
		jQuery('iframe').each(function() {
			if(($(this).attr('src')===undefined) || ($(this).attr('src').indexOf("youtube") == -1)){
				var vidSrc = "";
			}else{
				if($(this).context.src.indexOf("&#58;")!=-1) YThttps = $(this).context.src.replace("&#58;", ":");
				if($(this).context.src.substring(0,2)=="//") YThttps = $(this).context.src.replace("//", "https://");
				else YThttps = $(this).context.src.replace("http://", "https://");
				if (YThttps.indexOf("?") !=-1) {$(this).context.src= YThttps+"&enablejsapi=1&rel=0;origin=http://"+cfUtility(cvURL,'server');}
				else{$(this).context.src= YThttps+"?enablejsapi=1&rel=0;&origin=http://"+cfUtility(cvURL,'server');}
				video = $(this);
				vidSrc = video.attr('src');
			}
			// var regex = /h?t?t?p?s?\:?\/\/www\.youtube\.com\/embed\/([\w-]{11})(?:\?.*)?/;
			var regex = /h?t?t?p?s?\:?\/\/www\.youtube-?n?o?c?o?o?k?i?e?\.com\/embed\/([\w-]{11})(?:\?.*)?/;
			var matches = vidSrc.match(regex);

			if(matches && matches.length > 1){
				videoArray[i] = matches[1];
				$(this).attr('id', matches[1]);
				i++;			
			}
		});	YTplayersLoaded=1; 
	}
	$(document).ready(function() {
	// $(window).load(function() {
		trackYouTube();
	});
})(jQuery);

function onYouTubeIframeAPIReady() { 
	cvonYouTubeIframeAPIReady = setInterval(function(){cfonYouTubeIframeAPIReady();}, 100);
}

function cfonYouTubeIframeAPIReady(){ 
	if(YTplayersLoaded==1) { 
		window.clearInterval(cvonYouTubeIframeAPIReady);
		for (var i = 0; i < videoArray.length; i++) {
			playerArray[i] = new YT.Player(videoArray[i], {
				events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
				}
			});
		}
	}
}

cvYTEventArray=0;
function onPlayerReady(event) { 
	//event.target.playVideo();
	//console.log(event);
	//console.log(event.target.A);
	//console.log(event.target.A.videoData.video_id);
	//console.log(event.target.A.videoData.title);

	//globalize events
	cvYTevent[cvYTEventArray] = event;
	//console.log(cvYTevent[cvYTEventArray].target.A.videoData.video_id);
//	cvYTeventID[cvYTEventArray] = event.target.a.id;
	// cvYTeventID[cvYTEventArray] = (typeof event.target.a != "undefined") ? event.target.a.id : ((typeof event.target.A != "undefined" && typeof event.target.A.videoData != "undefined" && typeof event.target.A.videoData.video_id != "undefined") ? event.target.A.videoData.video_id : ((typeof event.target.B != "undefined") ? event.target.B.videoData.video_id : "not available"));
	cvYTeventID[cvYTEventArray] = (typeof event.target.c.id != "undefined") ? event.target.c.id : "";

	cvYTEventArray++;

	//autoplay
	cvVideoIDparam = cfGetQParam(cvURL,'videoid');
//	cvYTeventIDTest = event.target.a.id;
	//cvYTeventIDTest = (typeof event.target.a != "undefined") ? event.target.a.id : event.target.A.videoData.video_id;
	// cvYTeventIDTest = (typeof event.target.a != "undefined") ? event.target.a.id : ((typeof event.target.A != "undefined" && typeof event.target.A.videoData != "undefined" && typeof event.target.A.videoData.video_id != "undefined") ? event.target.A.videoData.video_id : ((typeof event.target.B != "undefined") ? event.target.B.videoData.video_id : "not available"));
	cvYTeventIDTest  = (typeof event.target.c.id != "undefined") ? event.target.c.id : "";
	if(cvVideoIDparam == cvYTeventIDTest.toLowerCase()) {
		event.target.playVideo();
	}
	//alert(event.target.getPlaylist());
}

var _pauseFlag = false;

function cfFFonPlayerReady(){
	if(cvYTEventArray!=0) {
		clearInterval(cvFFonPlayerReady);
		cvYTevent[cfYTgetVideoIndexNew($('.homePageHeroContainer').find('iframe').attr('src'))].target.playVideo();
	}
}



function onPlayerStateChange(event) { 
//	cvVideoID = event.target.a.id;
	// cvVideoID = (typeof event.target.a != "undefined") ? event.target.a.id : event.target.A.videoData.video_id;
	// cvVideoID = (typeof event.target.a != "undefined") ? event.target.a.id : ((typeof event.target.A != "undefined" && typeof event.target.A.videoData != "undefined" && typeof event.target.A.videoData.video_id != "undefined") ? event.target.A.videoData.video_id : ((typeof event.target.B != "undefined") ? event.target.B.videoData.video_id : "not available"));
	cvVideoID =  (typeof event.target.c.id != "undefined") ? event.target.c.id : "";
	// cvVideoTitle = (typeof event.target.A != "undefined") ? event.target.A.videoData.title : "";
	// cvVideoTitle = ((typeof event.target.A != "undefined" && typeof event.target.A.videoData != "undefined" && typeof event.target.A.videoData.title != "undefined") ? event.target.A.videoData.title : ((typeof event.target.B != "undefined") ? event.target.B.videoData.title : "not available"));
	cvVideoTitle  = (typeof event.target.c.ownerDocument.title != "undefined") ? event.target.c.ownerDocument.title : "not available";
	s.account = ReportSuiteID;
	s.linkTrackVars = "prop12,prop13,prop14,prop15,prop16,prop22,prop25,prop64,prop65,prop67,eVar6,eVar13,eVar14,eVar20,eVar41,eVar42,eVar43,eVar44,eVar56,eVar64,eVar65,eVar66,eVar67,events"; 
	s.prop12 = s.prop16 = s.pageName;
	s.prop25 = ReportSuiteID;
	s.prop64 = s.eVar64 = "YT|"+cvVideoID+"|"+cvVideoTitle;
	s.prop65 = s.eVar65 = "YT|"+cvVideoID+"|"+s.prop12;
	if(cvDNTDomains.indexOf(cfUtility(cvURL,'server')) == -1) {
		s.visitorNamespace="amd";
		s.trackingServer="http://www.amd.com/us/as/sp2013/metrics.amd.com";
		s.trackingServerSecure="http://www.amd.com/us/as/sp2013/smetrics.amd.com";
	}

	if (event.data ==YT.PlayerState.PLAYING){
		cvlinkTrackEvents = 'event88,';
		s.eVar20 = cfCVAConversion("VP");
		cvMetrics = cvCnt = 0; cfTrackPlayerProgress(event); //segment tracking
		_pauseFlag = false;
	} 
	if (event.data ==YT.PlayerState.ENDED){
		window.clearInterval(cvTrackPlayerProgressInterval);
		s.eVar66 = "6:M:100";
		s.linkTrackEvents = "event87,event88,event89,event90,event20,event21,event22,event23,event24,event25";
		s.events = 'event25,event87='+ (cvVideoDuration-cvVideoSegment4) +',event89,event90';
		cvSTL = 1;
		s.tl(this,'o',cvURL);
		s.eVar13 = s.prop13 = cvlinkTrackEvents=''; cvSTL = 0;
	} 
	if (event.data ==YT.PlayerState.PAUSED && _pauseFlag == false){
		window.clearInterval(cvTrackPlayerProgressInterval); 
		_pauseFlag = true;
	}
	if (event.data ==YT.PlayerState.BUFFERING){}
	if (event.data ==YT.PlayerState.CUED){} 
} 

function cfTrackPlayerProgress(event) {
	if(event.target && event.target.getDuration) {

		s.prop67 = s.eVar67 = cvVideoDuration = Math.floor(event.target.getDuration());
		cvCurrentVideoTime = Math.round(event.target.getCurrentTime());
		cvVideoSegment1 = Math.round(cvVideoDuration/4); // 25%
		cvVideoSegment2 = Math.round(cvVideoSegment1*2); // 50%
		cvVideoSegment3 = Math.round(cvVideoSegment1*3); // 75%
		cvVideoSegment4 = Math.round(cvVideoDuration/10*9)// 90%
//console.log("CURRENT: "+	cvCurrentVideoTime+"   |   DURATION: "+	cvVideoDuration+"   |   "+cvVideoSegment1+"|"+cvVideoSegment2+"|"+cvVideoSegment3+"|"+cvVideoSegment4+"|");
		if(cvCurrentVideoTime == 1) {
			s.eVar66 = "1:M:0-25";
			cvlinkTrackEvents += 'event20,event87=1,event90';
			cvMetrics=1;
		} else if(cvVideoSegment1 == cvCurrentVideoTime) {
			s.eVar66 = "2:M:25-50";
			cvlinkTrackEvents += 'event21,event87='+ (cvVideoSegment1-1) +',event90';
			cvMetrics=1;
		} else if(cvVideoSegment2 == cvCurrentVideoTime) {
			s.eVar66 = "3:M:50-75";
			cvlinkTrackEvents += 'event22,event87='+ (cvVideoSegment2-cvVideoSegment1) +',event90';
			cvMetrics=1;
		} else if(cvVideoSegment3 == cvCurrentVideoTime) {
			s.eVar66 = "4:M:75-90";
			cvlinkTrackEvents += 'event23,event87='+ (cvVideoSegment3-cvVideoSegment2) +',event90';
			cvMetrics=1;
		} else if(cvVideoSegment4 == cvCurrentVideoTime) {
			s.eVar66 = "5:M:90-100";
			cvlinkTrackEvents += 'event24,event87='+ (cvVideoSegment4-cvVideoSegment3) +',event90';
			cvMetrics=1;
		}
		if (cvMetrics == 1) {
			if(cvlinkTrackEvents.indexOf("event88") != -1) {
				s.eVar13 = s.prop13 = "Play|"+s.prop65;
				s.prop15 = "YT";
				s.eVar20 = cfCVAConversion("YT");
			}
			s.linkTrackEvents = "event87,event88,event89,event90,event20,event21,event22,event23,event24,event25";
			s.events =  cvlinkTrackEvents;
			cvSTL = 1;

			//nFusion MediaMind (MM) BOF
			if (cvURLCheck.indexOf("experience.amd.com/hp/hp-envy-iicg") != -1) { //03-14-2014|05-04-2014
				if (s.prop13.indexOf("Play|") != -1) mmConversionTag(466718, this); 
			}
			if (cvURLCheck.indexOf("aw-cl-global-11111119") != -1) { //06-27-2014|12-31-2014
				if (s.prop13.indexOf("Play|") != -1) mmConversionTag(498795, this); 
			}
			if (cvURLCheck.indexOf("en-us/markets/if-it-can-game") != -1) { //10-20-2014|12-31-2014
				if (s.prop13.indexOf("Play|") != -1) mmConversionTag(562226, this); 
			}
			// if (cvURLCheck.indexOf("en-gb/markets/if-it-can-game") != -1) { //10-28-2014|12-31-2014
				// if (s.prop13.indexOf("Play|") != -1) mmConversionTag(553344, this); 
			// }
			if (cvURLCheck.indexOf("en-gb/markets/if-it-can-game") != -1) { //11-18-2014|12-31-2014
				if (s.prop13.indexOf("Play|") != -1) mmConversionTag(564254, this); 
			}
			//nFusion MediaMind (MM) EOF
			
			s.tl(this,'o',cvURL);
			//clear variables after metrics call
			cvMetrics=0; s.eVar13 = s.prop13 = cvlinkTrackEvents=''; cvSTL = 0;
			//if (cvSPwebtrack==1) {return ewt.track({name:s.eVar66+'|'+s.eVar65,type:'video',link:o }); }
			if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.eVar66+'|'+s.eVar65,type:'video',link:o }); } )(); }

		}
		if (typeof(cfVideoFX)=="function"){cfVideoFX();}
		if (cvCnt != 1){
			cvTrackPlayerProgressInterval = setInterval(function(){cfTrackPlayerProgress(event);}, 1000);
			cvCnt = 1;
		}
	}
}
//https://developers.google.com/youtube/iframe_api_reference
//https://code.google.com/apis/ajax/playground/?exp=youtube#polling_the_player

//http://www.lunametrics.com/blog/2012/10/22/automatically-track-youtube-videos-events-google-analytics/
//http://microsite.omniture.com/t2/help/en_US/sc/appmeasurement/video/index.html#Using_JavaScript_to_Track_a_Video_Player

