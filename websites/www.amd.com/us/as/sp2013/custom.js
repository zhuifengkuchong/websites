/*
 * SP2013 Custom Script
 * Last Update 14:25 EST 05/29/2015
 */
 
 /* parseUri BOF */
function parseUri(str) { //http://blog.stevenlevithan.com/archives/parseuri
// Object { // https://www.amd.uk.com/abc/def/xyz/index.html?aaa=111&bbb=222#hashing
	// source: full URL: https://www.amd.uk.com/abc/def/xyz/index.html?aaa=111&bbb=222#hashing
	// protocol: https
	// authority: full domain: www.amd.uk.com
	// userInfo: username:password
	// user: username
	// password: password
	// host: full domain: www.amd.uk.com
	// port: port number
	// relative: /abc/def/xyz/index.html?aaa=111&bbb=222#hashing
	// path: /abc/def/xyz/index.html
	// directory: /abc/def/xyz/
	// file: index.html
	// query: aaa=111&bbb=222
	// queryKey: parseUri(uri).queryKey["cmpid"];
	// anchor: hashing
	// domain: amd
	// subdomain: www
	// tld: uk.com
	// pathLevel: parseUri(uri).pathLevel[3]; (7 maximum)
	// pathLevel[0]: www.amd.uk.com
	// pathLevel[1]: www.amd.uk.com/abc
	// pathLevel[2]: www.amd.uk.com/abc/def
// }
	var	pu   = parseUri.options,
		m   = pu.parser[pu.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[pu.key[i]] = m[i] || "";

	uri[pu.q.name] = {};
	uri[pu.key[12]].replace(pu.q.parser, function ($0, $1, $2) {
		if ($1) uri[pu.q.name][$1] = $2;
	});

	var uriSplit = uri.host.split('.');
	switch ( uriSplit.length ){
        case 2:
          uri.subdomain = null;
          uri.domain = uriSplit[0];
          uri.tld = uriSplit[1];
          break;
        case 3:
          uri.subdomain = uriSplit[0];
          uri.domain = uriSplit[1];
          uri.tld = uriSplit[2];
          break;
        case 4:
          uri.subdomain = uriSplit[0];
          uri.domain = uriSplit[1];
          uri.tld = uriSplit[2] + '.' + uriSplit[3];
          break;
		default:
          uri.subdomain = uri.domain = uri.tld = null;
    }

	var pathSplit = uri.path.split('/');
	pathSplit[0] = uri.host;
	for (p=1; p<pathSplit.length; p++) {
			if (pathSplit[p]!="") pathSplit[p]=pathSplit[p-1]+"/"+pathSplit[p]; //avoid extra entry with trailing slash
	}
	if (pathSplit.length<8) {
		for (p=pathSplit.length; p<8; p++) {
		pathSplit[p]="";
		}
	}
	uri.pathLevel = pathSplit;
	uri.noquery = uri.protocol + "://" + uri.host + uri.path;
	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};

/* parseUri EOF */
 
//load jQuery
if (typeof jQuery === "undefined") {
	var jq = document.createElement('script');
	jq.type = 'text/javascript';
	jq.src = "../../../../support.amd.com/Style Library/js/jquery.min.js"/*tpa=http://support.amd.com/Style%20Library/js/jquery.min.js*/;
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(jq, s);
} 
window.dataLayer = window.dataLayer || [];
var sAA = {}; var cvOldLinkClickObject = {}; cvSPwebtrack = 0;
cvURLCheck = cvSurveyURLCheck = window.location.href.toLowerCase();

//==============================================
try {var cvURL = decodeURI(document.URL.toLowerCase());}
catch(err){var cvURL = unescape(document.URL.toLowerCase());}
//add trailing slash on folders BOF
if  (!((/((\.[a-z]{3,4})|(\/))$/i.test(cvURL)) || (/#|\?/i.test(cvURL)))) cvURL =cvURL+"/";
if ((/#|\?/i.test(cvURL))) {
	if ((cvURL.indexOf("/#") == -1) && (cvURL.indexOf("/?") == -1)) {
		if ((cvURL.indexOf("#") != -1) && (cvURL.indexOf("?") != -1)) {
			if (cvURL.indexOf("#") > cvURL.indexOf("?")) { //? is first
				cvURLtempSplit=cvURL.split("?");
				cvURL=cvURLtempSplit[0]+"/?"+cvURLtempSplit[1];
			} else { //# is first
				cvURLtempSplit=cvURL.split("#");
				cvURL=cvURLtempSplit[0]+"/#"+cvURLtempSplit[1];
			}
		}
		else if (cvURL.indexOf("#") != -1) {
			cvURLtempSplit=cvURL.split("#");
			cvURL=cvURLtempSplit[0]+"/#"+cvURLtempSplit[1];
		}
		else if (cvURL.indexOf("?") != -1) {
			cvURLtempSplit=cvURL.split("?");
			cvURL=cvURLtempSplit[0]+"/?"+cvURLtempSplit[1];
		}
	}
} //add trailing slash on folders EOF
cvURL = cvURL.replace(/#$/,""); //remove trailing hash
//==============================================



// APP Consent LightBox
var APPWidth = 700;
var APPHeight = 700;
var APPw = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
if (APPw<APPWidth) { APPWidth = APPw - 10;}
if (APPWidth<630) {APPWidth=630;}
if (/.*partner-auth.amd.com/i.test(parseUri(cvURL).host) || /.*partner.amd.com/i.test(parseUri(cvURL).host)) {
			
		if (/.*partner-auth.amd.com\/de-de/i.test(cvURL) || /.*partner.amd.com\/de-de/i.test(cvURL)) {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/agree_de/",APPWidth,APPHeight, "http://www.amd.com/de-de/");
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		} else if (/.*partner-auth.amd.com\/es-xl/i.test(cvURL) || /.*partner.amd.com\/es-xl/i.test(cvURL)) {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/agree_es/",APPWidth,APPHeight, "http://www.amd.com/es-xl/");
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		} else 	if (/.*partner-auth.amd.com\/en-gb/i.test(cvURL) || /.*partner.amd.com\/en-gb/i.test(cvURL)) {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/agree_uk/",APPWidth,APPHeight, "http://www.amd.com/en-gb/");
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		} else 	if (/.*partner-auth.amd.com\/pt-br/i.test(cvURL) || /.*partner.amd.com\/pt-br/i.test(cvURL)) {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/agree_pt/",APPWidth,APPHeight, "http://www.amd.com/pt-br/");
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		}  else 	if (/.*partner-auth.amd.com\/ru-ru/i.test(cvURL) || /.*partner.amd.com\/ru-ru/i.test(cvURL)) {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/agree_ru/",APPWidth,APPHeight, "http://www.amd.com/ru-ru/");
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		} else 	if (/.*partner-auth.amd.com\/pl-pl/i.test(cvURL) || /.*partner.amd.com\/pl-pl/i.test(cvURL)) {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/agree_pl/",APPWidth,APPHeight, "http://www.amd.com/pl-pl/");
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		} else 	if (/.*partner-auth.amd.com\/fr-fr/i.test(cvURL) || /.*partner.amd.com\/fr-fr/i.test(cvURL)) {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/agree_fr/",APPWidth,APPHeight, "http://www.amd.com/fr-fr/");
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		} else 	if (/.*partner-auth.amd.com\/tr-tr/i.test(cvURL) || /.*partner.amd.com\/tr-tr/i.test(cvURL)) {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/agree_tr/",APPWidth,APPHeight, "http://www.amd.com/tr-tr/");
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		} else {
			var cvCAPP = cfGetCookie('c_APPconsent') || "";
			if (cvCAPP != "Agreed") {
				LightBoxWindow2 ("http://experience.amd.com/APPLegalConsent/",APPWidth,APPHeight, "../../../en-us.htm"/*tpa=http://www.amd.com/*/);
			} else {
				cfSetCookie('c_APPconsent',"Agreed",365);
			}
		}
	}

// Report AddThis event to Omniture
//function shareEventHandler(evt) {
//    if (evt.type == 'addthis.menu.share') {
//
//       s.linkTrackVars='action,events';
//       s.linkTrackEvents='AddThisShareEvents';
//       s.events='AddThisShareEvents';
//       s.action=evt.data.service;
//       s.tl(this,'o', evt.data.service);
//    }
//}

// Listen for the share event
//$(document).ready(function() {
//	if (typeof addthis != "undefined") {
//		addthis.addEventListener('addthis.menu.share', shareEventHandler);
//	}
//});

//driver installed BOF
if ((cvURLCheck.indexOf("http://www.amd.com/us/as/sp2013/driverinstalled/index.html") != -1) && (cvURLCheck.indexOf("%%") != -1)) {
	cvURLCheck=replaceAll(cvURLCheck,"%%","%");
	window.location.href=cvURLCheck;
}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(string, find, replace) {
  return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}
//driver installed EOF

//load external js
function _load_js(j){
	document.write('<script type="text/javascript" src="'+j+'"></scr'+'ipt>');
}


// remove _ga paramter from cross domain query strings
// http://brianshim.com/webtricks/cross-domain-analytics-without-query-string/
// uses http://medialize.github.io/URI.js/
//	_load_js('uri.js'/*tpa=http://www.amd.com/us/as/sp2013/uri.js*/);
//	$(window).load(function() {
//
//          url = URI(window.location.href).removeSearch("_ga");
//          history.replaceState("state", "", url);
//      });


//Silverpop web tracking
if((cvURLCheck.indexOf("http://www.amd.com/us/as/sp2013/experience.amd.com") == -1) && (cvURLCheck.indexOf("http://www.amd.com/us/as/sp2013/amd.mkt6303.com") == -1) && (cvURLCheck.indexOf("http://www.amd.com/us/as/sp2013/www.pages03.net") == -1)){
	cvSPwebtrack=0;
	_load_js('sp.js'/*tpa=http://www.amd.com/us/as/sp2013/sp.js*/);
}
cvAddThis = 0;
cvAMDWebProperties = "http://www.amd.com/us/as/sp2013/,amd.com,amd.jp,amd.de,amd.com.cn,amd.com.tw,amd.com.hk,amd.co.jp,amdtaiwan.com.tw,amd64.co.kr,amdathlon.com,amdlive.com,amdcompare.com,amd-firefly.com,amd.ru,ati.com,ati.de,ati.jp,accelerateresults.com,graphics-scout.com,marketbuilder3.com,atiplus,amd64.org,amd-unleash.com,amd-bilder.de,entertainmentsimplified.com,amdassets.com,x86-64.org,amdquizzes.com,amdrewards.com,atistickers.com,lookbeyond.de,amdinfo.co.kr,amdchannel.com.cn,amdlivestrong.com,amdmotorsports.com,amdprocycling.com,amdsports.com,amdadvantage.com,amdsurveys.com,amdtaiwan.com,amd-member.com,outstart.com,amdvisionary.com,amd-site.com,seamicro.com";	// AMD Web Properties
cvAMDWebProperties += "http://www.amd.com/us/as/sp2013/,phx.corporate-ir.net,en25.com,eloqua.com,amdimprovision.com.br,flixfacts.co.uk,amd3.c-3.com,radeonramdisk.com,fireprographics.com,amd-ext.uat5.hosted.jivesoftware.com,amd4u.com,standupforfirepro.com,radeonmemory.com,amd.mkt6303.com,www.pages03.net,gizmosphere.org"; //other properties
//cvAMDWebProperties += "http://www.amd.com/us/as/sp2013/,atiplushp.com,atipluslenovo.com"; //old domains
cvDNTDomains ="";
cvSocialDomains=new Array("163","12seconds","4travel","advogato","ameba","anobii","asmallworld","backtype","badoo","bebo","bigadda","bigtent","biip","blackplanet","blogspot","blogster","blomotion","bolt","brightkite","buzznet","cafemom","care2","classmates","cloob","collegeblender","cyworld","cyworld","dailymotion","daum","delicious","deviantart","digg","diigo","disqus","draugiem","facebook","faceparty","fc2","flickr","flixster","fotolog","foursquare","friendfeed","friendsreunited","friendster","fubar","gaiaonline","geni","goodreads","grono","habbo","hatena","hexun","hi5","hyves","ibibo","identi","ifeng","imeem","infoseek","intensedebate","irc-galleria","iwiw","jaiku","kaixin001","kaixin002","kakaku","kanshin","kozocom","last","linkedin","livejournal","me2day","meetup","mister-wong","mixi","mixx","mouthshut","multiply","myheritage","mylife","myspace","myspace","myyearbook","nasza-klasa","netlog","nettby","netvibes","nicovideo","ning","odnoklassniki","orkut","pakila","people","photobucket","pinterest","plaxo","plurk","qq","reddit","renren","seesaa","skyrock","slideshare","smcb","smugmug","sohu","sonico","studivz","stumbleupon","t","tabelog","tagged","taringa","tripit","trombi","trytrend","tuenti","tumblr","twine","twitter","twitch","uhuru","viadeo","vimeo","vkontakte","vox","wayn","weibo","weourfamily","wer-kennt-wen","wordpress","xanga","xing","yaplog","yelp","youtube","yuku","zooomr","google");
cvSearchDomains=new Array("abcsok","alicesuche","alltheweb","altavista","anonymouse","aol","aolrecherche","aolsearch","aolsvc","ask","baidu","biglobe","bing","centrum","chip","clix","cuil","daum","eniro","euroseek","excite","fireball","goo","google","googlesyndication","icq","infoseek","ixquick","kvasir","libero","live","livedoor","lycos","mail","msn","myway","naver","netscape","nifty","rambler","recherche","recherchet2","search","searchalot","seznam","startpagina","suche","szukaj","terra","tiscali","toile","ucweb","uol","vinden","virgilio","walla","yahoo","yandex","web","youdao","webalta");
cvAMDShortDomains=new Array("amd","amdsurveys","amd-member","seamicro");
cvOtherPartnerExcludeDomains=new Array("dataram","adcash","gobongo","youradexchange","openadserving","otvetim-vopros","undefined","drotator","uxup","wpengine");
cvPartnerExcludeDestination_LID=new Array("ms-backgroundImage"," ms-backgroundImage");
cvPartnerExcludeDomains=cvOtherPartnerExcludeDomains.join(",")+","+cvSearchDomains.join(",")+","+cvSocialDomains.join(",")+cvAMDWebProperties; //PartnerRef
cvSourceExcludeDomains=new Array("http://www.amd.com/us/as/sp2013/devgurus.amd.com","http://www.amd.com/us/as/sp2013/devforums.amd.com","http://www.amd.com/us/as/sp2013/forums.amd.com","http://www.amd.com/us/as/sp2013/support.amd.com"); //PartnerRef
cvDownloadExtentions = "run,msi,rpm,gz,tar,bz2,dmg,sit,gzip,patch,exe,zip,wav,mp3,wma,mov,mpg,avi,wmv,mp4,doc,pdf,xls,ppt,pps,csv,jpg,png,bmp,wmf,tif,docx,xlsx,pptx,cbr";	// File extensions to track as downloadable content
cvDriverExtensions = "run,msi,rpm,gz,tar,bz2,dmg,sit,gzip,exe,zip"; // File extensions to include for driver downloads
cvNonDownloadExtensions = "do,aspx,ashx,asp,jp,ru,it"; // File extensions to exclude for driver downloads
cvDriverDownloadDomains = ["http://www.amd.com/us/as/sp2013/support.amd.com","http://www.amd.com/us/as/sp2013/www2.ati.com"]; // array of AMD driver download domains

tmpDestination_LID = ""; ReportSuiteID=""; cvTagName = ""; cvPTagName = ""; cvPPTagName = ""; cvPPPTagName = ""; cvLinkType = ""; cvRsid = ""; cvRsidVal = ""; Destination_URL_wParam = ""; Destination_Name = ""; Destination_LPOS=""; Destination_LID = ""; Destination_ID = ""; nodesForm = ""; cvElqFormId = ""; cvFormName = ""; cvFormPassed=''; cvElqAssetType = ""; cvTimedReading = ""; cvSearchKeywordLoop=0;cvVideoID = "";cvURL_fragment="";cvSTL =  0;cvGTMID="";cvCVAReferrerURL="";cvLinkedSearch=0; cvCookieDURL="";cvCookieRURL="";csTotalCounts=0;cvFFonPlayerReady=""; cvGACheckSearchAjax=""; cvSearchURL = ""; cvGASearchURL = ""; cvOSSEvents="";
sAA.referrer=document.referrer.toLowerCase();
cvUTCdate=getCurrentTimeUTC();


//nFusion MediaMind (MM)/Sizmek BOF
cvMMconversionTag=0;
if (1==0) {}
//else if (cvURLCheck.indexOf("community.amd.com/play/hd-8000-oem") != -1) {cvMMconversionTag=302577;}
//else if (/community\.amd\.com\/.*amd-radeon-hd-8000-series-graphics-oem/i.test(cvURLCheck)) {cvMMconversionTag=302577;}
//else if (cvURLCheck.indexOf("http://www.amd.com/us/as/sp2013/amd.com/us/consumer/if-it-can-game/pages/if-it-can-game.aspx") != -1) {cvMMconversionTag=403498;} //01-14-2014|02-07-2014
else if (cvURLCheck.indexOf("aw-cl-global-11111119") != -1) {cvMMconversionTag=498793;} //06-27-2014|12-31-2014
else if (cvURLCheck.indexOf("amd.com/en-us/innovations/software-technologies/mantle") != -1) {cvMMconversionTag=465794;} //03-17-2014|04-25-2014
else if (cvURLCheck.indexOf("experience.amd.com/hp/hp-envy-iicg") != -1) {cvMMconversionTag=468458;} //03-14-2014|05-04-2014
else if (/www.amd.com\/en-us\/solutions\/pro(|\/)$/i.test(cvURLCheck)) {cvMMconversionTag=529312;}
// else if (cvURLCheck.indexOf("en-us/markets/if-it-can-game") != -1) {cvMMconversionTag=542014;} //10-20-2014|12-31-2014
else if (cvURLCheck.indexOf("en-us/markets/if-it-can-game") != -1) {cvMMconversionTag=562219;} //11-11-2014|12-31-2014
//else if (cvURLCheck.indexOf("en-gb/markets/if-it-can-game") != -1) {cvMMconversionTag=553338;} //10-28-2014|12-31-2014
else if (cvURLCheck.indexOf("en-gb/markets/if-it-can-game") != -1) {cvMMconversionTag=564251;} //11-18-2014|12-31-2014

if (cvMMconversionTag>0) {
//-------------------------------------------------------------------------------------------
//DO NOT EDIT
//http://ds.serving-sys.com/BurstingRes/CustomScripts/mmConversionTagV4.js minimized using http://closure-compiler.appspot.com/home
function mmCreateConversionTagHolder(b){var a=document.createElement("iframe");a.id="mmConversionTagIframe"+b;a.name="mmConversionTagIframe"+b;a.style.width="0";a.style.height="0";a.style.hspace="0";a.style.vspace="0";a.style.frameboarder="0";a.style.scrolling="no";a.style.borderColor="#000000";a.style.display="none";document.body.appendChild(a)}function mmRedirect(b,a){"undefined"!=typeof a&&"_SELF"==a.toUpperCase()?document.location.href=b:window.open(b,"_blank")}
function mmExecutePublisherCode(b){var a=("undefined"==typeof b.replace?b.toString():b).replace(/\bmmConversionTag?.*?return?\s?false?\;/i,"");"undefined"==typeof b.replace&&(a=a.replace(/\bfunction\s?\w*\(\)?.*?\n/i,""),a=a.replace(/^\{/i,""),a=a.replace(/\}$/i,""));setTimeout(a,1)}function mmIframeLoadHandler(b,a,e,c){c.src="";c.detachEvent?c.detachEvent("onload",c.mmIframeLoadHandlerRef):c.removeEventListener?c.removeEventListener("load",c.mmIframeLoadHandlerRef,!1):c.onload=null}
function mmConversionTag(b,a,e,c,g){try{mmCreateConversionTagHolder(b);var f="";typeof("object"==a)&&"undefined"!=typeof a.getAttribute&&null!=a.getAttribute("onclick")&&(f=a.getAttribute("onclick"));var h="https:"==document.location.protocol?"https://":"http://",k=Math.round(1E6*Math.random());"undefined"==typeof g&&(g="");var d=document.getElementById("mmConversionTagIframe"+b);d.attachEvent?d.attachEvent("onload",d.mmIframeLoadHandlerRef=function(){mmIframeLoadHandler(c,e,f,d)}):d.addEventListener?
d.addEventListener("load",d.mmIframeLoadHandlerRef=function(){mmIframeLoadHandler(c,e,f,d)},!1):d.onload=function(){mmIframeLoadHandler(c,e,f,d)};"undefined"!=typeof c&&null!=c&&mmRedirect(c,e);(null==e||"undefined"==typeof e||"undefined"!=typeof e&&"_SELF"!=e.toUpperCase())&&""!=f&&mmExecutePublisherCode(f);d.src=h+"bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ifrm=1&ActivityID="+b+g+"&rnd="+k}catch(l){}};
//-------------------------------------------------------------------------------------------

}
//nFusion MediaMind (MM)/Sizmek EOF

//nFusion Predicta (MFA) BOF
cvMFAconversionTag="0";

//if (cvURLCheck.indexOf("http://www.amd.com/us/as/sp2013/amd.com/br/products/notebook/Pages/consumer-notebooks.aspx?cmpid=DM-ba-LA-6MSBUEKG7O".toLowerCase()) != -1) {cvMFAconversionTag="202efca8de-208";} //06-11-2013|01-31-2014

if (cvMFAconversionTag!="0") {
//DO NOT EDIT
	function MFAConversionTracker(cvMFAconversionTag) {
	try{
	var MFAConversionTracker = new MFACV(cvMFAconversionTag);
	MFAConversionTracker.track(); }catch(k){}
	}
	(function(d,s,f){var a,b,l,t=setTimeout;a = d.createElement(s);a.async = "async";a.src = '../../../../mfa.md.predicta.net/mrm-ad/convjs.js'/*tpa=https://mfa.md.predicta.net/mrm-ad/convjs*/;b=d.getElementsByTagName(s)[0];b.parentNode.insertBefore(a,b);l = function(){if(typeof MFACV != "undefined"){f();}else{t(function(){l();}, 50);}};return function(){t(function(){l();}, 50);}();}(document,'script',MFAConversionTracker));
}
//nFusion Predicta (MFA) EOF

			
//Google DoubleClick FloodLight BOF
//DO NOT EDIT
	function callStaticFL(source, type, cat,u1,u2) {
		var axel = Math.random() + "";
		var num = axel * 1000000000000000000;
		num=1;
		var tag_url = new Image();
		tag_url.src = "http://ad.doubleclick.net/activity;src=" + source + ";type=" + type + ";cat=" + cat +  ";u1=" + u1 +  ";u2=" + u2 + ";ord=1;num="+num+"?";
	}
//Google DoubleClick FloodLight EOF

/* Set report suite ID dynamically based on domain */
cvLang ="";
function cfCheckRSID(cvURL) {
	ReportSuiteID = "";
	var cvCurrency = "USD";
	cvLang = cfDetectLanguage() || "";
	var cvHostName = cfUtility(cvURL,'server');

	if ((cvURL.indexOf("driverinstalled") != -1) || (cvURL.indexOf("driver-installed") != -1)) { // cvDI = Driver Installed
		ReportSuiteID = "amdvother18";
		//if (cvSPwebtrack==1) {(function(){return ewt.track({name:cvUTCdate,type:'driver-installed',link:o }); } )(); }
	}
	else if (cvHostName == "http://www.amd.com/us/as/sp2013/www.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/www2.amd.com") {
		if (cvURL.indexOf('/ccc/') != -1) {
			ReportSuiteID = "amdvother18,amdvother13";
			//ReportSuiteID = "amdvother23";
		} else if (cvURL.indexOf("/vision/shop") != -1) {
			if (cvURL.indexOf("/cool-apps/pages") != -1) {
				ReportSuiteID = "amdvother18,amdvother22,amdvother13";
			}
			else {
				ReportSuiteID = "amdvother18,amdvother22";
			}
		} else if (cvURL.indexOf("/products/") != -1){
			if (cvURL.indexOf("/embedded/") != -1){
				if (/\/(processors|graphics|chipsets)\//i.test(cvURL)) {
					ReportSuiteID = 'amdvembedded,amdvother20,amdvbusiness';
				} else ReportSuiteID = 'amdvembedded,amdvother20,amdvbusiness'; //default /embedded/
			} else if (cvURL.indexOf("/graphics/") != -1){
				if (cvURL.indexOf("/desktop/") != -1){
					if (cvURL.indexOf("/r9/") != -1){
						ReportSuiteID = 'amdvother18,amdvother22';
					} else if (cvURL.indexOf("/r7/") != -1){
						ReportSuiteID = 'amdvother18,amdvother22';
					} else ReportSuiteID = 'amdvother18,amdvother19'; //default /desktop/
				} else if (cvURL.indexOf("/notebook/") != -1){
					if (cvURL.indexOf("/r9-m200/") != -1){
						ReportSuiteID = 'amdvother18,amdvother22';
					} else if (cvURL.indexOf("/r7-m200/") != -1){
						ReportSuiteID = 'amdvother18,amdvother22';
					} else if (cvURL.indexOf("/8900m/") != -1){
						ReportSuiteID = 'amdvother18,amdvother22';
					} else ReportSuiteID = 'amdvother18,amdvother19'; //default /notebook/
				} else if (cvURL.indexOf("/workstation/") != -1){
					ReportSuiteID = 'amdvother18,amdvother20,amdvbusiness,amdvother16';
				} else if (cvURL.indexOf("/help-me-choose/") != -1){
					ReportSuiteID = 'amdvother18,amdvother19';
				} else ReportSuiteID = 'amdvother18,amdvother19'; //default /graphics/
			} else if (cvURL.indexOf("/processors/") != -1){
				if (cvURL.indexOf("/desktop/") != -1){
					ReportSuiteID = 'amdvother07,amdvother19';
				} else if (cvURL.indexOf("/notebook-tablet/") != -1){
					ReportSuiteID = 'amdvother07,amdvother19';
				} else ReportSuiteID = 'amdvother07,amdvother19'; //default /processors/
			} else if (cvURL.indexOf("/server/") != -1){
				ReportSuiteID = 'amdvother03,amdvother20,amdvbusiness';
			} else if (cvURL.indexOf("/pricing/") != -1){
				if (cvURL.indexOf("opteron") != -1) {
					ReportSuiteID = 'amdvother03,amdvother20,amdvbusiness';
				} else if (cvURL.indexOf("fx") != -1) {
					ReportSuiteID = 'amdvother03,amdvother20,amdvbusiness';
				} else if (/fx|a-series/i.test(cvURL)) {
					ReportSuiteID = 'amdvother07';
				} else ReportSuiteID = 'amdvother07,amdvother19'; //default /pricing/
			} else if (cvURL.indexOf("/chipsets/") != -1){
				ReportSuiteID = 'amdvother07,amdvother20,amdvbusiness';
			} else if (cvURL.indexOf("/memory/") != -1){
				ReportSuiteID = 'amdvother07,amdvother22,amdvother14';
			} else ReportSuiteID = ''; //default /products/
		} else if (cvURL.indexOf("/markets/") != -1){
			if (cvURL.indexOf("/partners/") != -1){
				ReportSuiteID = 'amdvother20';
			} else if (cvURL.indexOf("/embedded/") != -1){
				ReportSuiteID = 'amdvembedded,amdvother20,amdvbusiness';
			} else if (cvURL.indexOf("/commercial/") != -1){
				ReportSuiteID = 'amdvother03,amdvother20,amdvbusiness,amdvother16';
			} else if (cvURL.indexOf("/consumer/") != -1){
				ReportSuiteID = 'amdvother17,amdvother19';
			} else if (cvURL.indexOf("/if-it-can-game/") != -1){
				ReportSuiteID = 'amdvother17,amdvother19';
			} else if (cvURL.indexOf("/game/") != -1){
				if (cvURL.indexOf("/shop/") != -1){
					ReportSuiteID = 'amdvgame,amdvother22,amdvother18,amdvshop';
				} else if (cvURL.indexOf("/downloads/") != -1){
					ReportSuiteID = 'amdvgame,amdvother22,amdvother18,amdvsupport';
				} else ReportSuiteID = 'amdvgame,amdvother22,amdvother18'; //default /markets/game
			} else ReportSuiteID = ''; //default /markets/
		} else if (cvURL.indexOf("/solutions/") != -1){
			if (/\/(tablets|laptops|desktops)\//i.test(cvURL)) {
				ReportSuiteID = 'amdvother17,amdvother19';
			} else if (cvURL.indexOf("/servers/") != -1){
				ReportSuiteID = 'amdvother03,amdvother20,amdvbusiness';
			// } else if (cvURL.indexOf("/workstations/") != -1) {
			} else if (/\/(workstations|professional)\//i.test(cvURL)) {
				ReportSuiteID = 'amdvother18,amdvother20,amdvbusiness,amdvother16';
			} else if (/\/software-partners\/(altair|ansys|citrix|comsol|dassault|dem|ensight|ptc|remotefx|siemens|spaceclaim|teradici|vmware)/i.test(cvURL)) {
				ReportSuiteID = 'amdvother18,amdvother20,amdvbusiness,amdvother16';
			} else if (cvURL.indexOf("/embedded/") != -1){
				ReportSuiteID = 'amdvembedded,amdvother20,amdvbusiness';
			} else if (cvURL.indexOf("/pro/") != -1){
				ReportSuiteID = 'amdvother17,amdvother20,amdvbusiness';
			} else ReportSuiteID = ''; //default /solutions/
		} else if (cvURL.indexOf("/who-we-are/") != -1){
			if (cvURL.indexOf("/404/") != -1){
				ReportSuiteID = 'amdv404';
			} else if (cvURL.indexOf("/site-map/") != -1){
				ReportSuiteID = '';
			} else ReportSuiteID = 'amdvother08'; //default /who-we-are/
		} else if (cvURL.indexOf("/press-releases/") != -1){
				ReportSuiteID = 'amdvother08';
		} else if (cvURL.indexOf("/innovations/") != -1){
			if (/\/(2010-present|2000-2009|1970-1999)\//i.test(cvURL)) {
				ReportSuiteID = 'amdvother08';
			} else if (cvURL.indexOf("/servers/") != -1){
				ReportSuiteID = 'amdvother08';
			} else if (cvURL.indexOf("/software-technologies/") != -1){
				if (/\/(apu|multicore|photo-composer|turbo-core|compute-cores|hsa|screen-mirror|face-login|gesture-control|experiences-faq|wireless-display|discovery)\//i.test(cvURL)) {
					ReportSuiteID = 'amdvother17,amdvother19';
				} else if (/\/(catalyst|gcn|eyefinity|crossfire|hd3d|dual-graphics|tressfx|trueaudio|mantle|enduro)\//i.test(cvURL)) {
					ReportSuiteID = 'amdvother18,amdvother22';
				} else if (cvURL.indexOf("/amp/") != -1){
					ReportSuiteID = 'amdvother07,amdvother22,amdvother14';
				} else if (cvURL.indexOf("/das/") != -1){
					ReportSuiteID = 'amdvembedded,amdvother20,amdvbusiness';
				} else if (/\/(direct-connect|open-compute)\//i.test(cvURL)) {
					ReportSuiteID = 'amdvother03,amdvother20,amdvbusiness';
				} else if (cvURL.indexOf("/app-acceleration/") != -1){
					ReportSuiteID = 'amdvother18,amdvother22,amdvother13';
				} else if (cvURL.indexOf("/app-zone/") != -1){
					ReportSuiteID = 'amdvother18,amdvother22,amdvother13';
				} else if (/\/(hypertransport|quick-stream|surround-house)\//i.test(cvURL)) {
					ReportSuiteID = 'amdvother08';
				} else ReportSuiteID = ''; //default /software-technologies/
			} else ReportSuiteID = 'amdvother08'; //default /innovations/
		} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/amdhomepage.aspx") != -1){
			ReportSuiteID = ''; //homepage
		} else if  (/((en-us|de-de|en-gb|es-es|es-xl|fr-fr|it-it|ja-jp|ko-kr|pl-pl|pt-br|ru-ru|tr-tr|zh-cn|zh-tw)|(en-us\/|de-de\/|en-gb\/|es-es\/|es-xl\/|fr-fr\/|it-it\/|ja-jp\/|ko-kr\/|pl-pl\/|pt-br\/|ru-ru\/|tr-tr\/|zh-cn\/|zh-tw\/))$/i.test(cvURL)) {
			ReportSuiteID = ''; //homepage
		} else {
			ReportSuiteID = "amdvother";//default www.amd.com; no audience
		}
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/wwwd.amd.com") {
		ReportSuiteID = "amdvembedded,amdvother20";
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/game.amd.com") {
		ReportSuiteID = "amdvother18,amdvother22,amdvgame";
	} else if ((cvHostName == "http://www.amd.com/us/as/sp2013/fpp.amd.com") || (cvHostName == "http://www.amd.com/us/as/sp2013/registration-fpp.amd.com") || (cvHostName == "http://www.amd.com/us/as/sp2013/partner.amd.com")) {
		ReportSuiteID = "amdvother07,amdvother20,amdvfpp-global,amdvbusiness";
		if (cvURL.indexOf("/en-us/") != -1) {
			ReportSuiteID = "amdvfpp-na," + ReportSuiteID;
		} else if (/(en-gb|fr-fr|de-de|it-it|pl-pl|ru-ru|es-es|tr-tr)/i.test(cvURL)) {
			ReportSuiteID = "amdvfpp-emea," + ReportSuiteID;
		} else if (/(pt-br|en-mx|es-mx)/i.test(cvURL)) {
			ReportSuiteID = "amdvfpp-la," + ReportSuiteID;
		} else if (/(en-in|en-sa|ko-kr|ja-jp|jp-jp)/i.test(cvURL)) {
			ReportSuiteID = "amdvfpp-apac," + ReportSuiteID;
		} else if (cvURL.indexOf("zh-cn") != -1) {
			ReportSuiteID = "amdvfpp-cn," + ReportSuiteID;
		}
	} else if ((cvHostName == "http://www.amd.com/us/as/sp2013/support.amd.com") || (cvHostName == "http://www.amd.com/us/as/sp2013/emailcustomercare.amd.com")) {
		if (cvURL.indexOf('/search/') != -1) {
			ReportSuiteID = "amdvsupport,amdvother02";
		} else if ((cvURL.indexOf('download/ccc') != -1) || (cvURL.indexOf('/ccc/') != -1)){
			ReportSuiteID = "amdvother22"; //ccc
			//ReportSuiteID = "amdvother23";
		} else {
			ReportSuiteID = "amdvsupport";
		}
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/onlineservices.amd.com") {
		if (cvURL.indexOf("/icss/") != -1) {
			ReportSuiteID = "amdvsupport";
		}
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/sites.amd.com") {
		if (cvURL.indexOf("/microsoft/business") != -1) {
			ReportSuiteID = "amdvother17,amdvother20,amdvbusiness,amdvmicrosoft";
		} else if (cvURL.indexOf("/microsoft/") != -1) {
			ReportSuiteID = "amdvother17,amdvother19,amdvmicrosoft";
		} else if ((cvURL.indexOf("/recommended/") != -1) || (cvURL.indexOf("/desktopstoryteller") != -1)) {
			ReportSuiteID = "amdvother18,amdvother22";
		} else if (cvURL.indexOf("/game/shop") != -1) {
			ReportSuiteID = "amdvother18,amdvother22,amdvgame";
		} else if (cvURL.indexOf("/game/downloads") != -1) {
			ReportSuiteID = "amdvsupport,amdvother22,amdvgame";
		} else if (cvURL.indexOf("/game/") != -1) {
			ReportSuiteID = "amdvother18,amdvother22,amdvgame";
		} else if (cvURL.indexOf("/shop-amd/") != -1) {
			ReportSuiteID = "amdvshop";
		} else if (cvURL.indexOf("/underground/") != -1) {
			ReportSuiteID = "amdvother18,amdvother22,amdvgame";
		} else if (/promo.*(battlefield4|bf4|never-settle|nsreloadedforever)/i.test(cvURL)) {
			ReportSuiteID = "amdvother18,amdvother22,amdvgame";
		} else if ((cvURL.indexOf("server") != -1) || (cvURL.indexOf("opteron") != -1) || (cvURL.indexOf("oems") != -1) || (cvURL.indexOf("partners") != -1)) {
			ReportSuiteID = "amdvother03,amdvother20,amdvbusiness";
		} else if (((cvURL.indexOf('business/ccc/') != -1) || (cvURL.indexOf('/ccc/') != -1)) && (cvURL.indexOf('vision-redirect') == -1)) {
			ReportSuiteID = "amdvother16";
			//ReportSuiteID = "amdvother23";
		} else if (cvURL.indexOf("/processors/") != -1) {
			ReportSuiteID = "amdvother07";
		} else if (/promo.*(invincible|amdfx)/i.test(cvURL)) {
			ReportSuiteID = "amdvother07,amdvother22";
		} else if (cvURL.indexOf("/business/") != -1) {
			ReportSuiteID = "amdvother03,amdvother20,amdvbusiness";
		} else if ((cvURL.indexOf("/vision/") != -1) || (cvURL.indexOf("http://www.amd.com/us/as/sp2013/amdvisionary.com") != -1)) {
			ReportSuiteID = "amdvother17,amdvother19";
		} else {
			ReportSuiteID = "amdvother";
		}
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/community.amd.com"){
		if ((cvURL.indexOf("game") != -1) || (cvURL.indexOf("gaming") != -1)) {
			ReportSuiteID = "amdvother18,amdvother22,amdvgame,amdvblogs";
		} else if ((cvURL.indexOf("partners") != -1) || (cvURL.indexOf("business") != -1)) {
			ReportSuiteID = "amdvother20,amdvbusiness,amdvblogs";
		} else {
			ReportSuiteID = "amdvblogs";
		}
	} else if ((cvURL.indexOf("http://www.amd.com/us/as/sp2013/amddevcentral.com") != -1) || cvHostName == "http://www.amd.com/us/as/sp2013/developer.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/media.amddevcentral.com" || cvHostName == "http://www.amd.com/us/as/sp2013/download-developer.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/devgurus.amd.com") {
		ReportSuiteID = "amdvother21,amdvdev,amdvbusiness";
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/shop.amd.com") {
		cvShopAudience="";
		ReportSuiteID = "amdvshop";
		if (cvURL.indexOf('/gaming') != -1) {
			cvShopAudience = ReportSuiteID += ",amdvother22";
		} else if (cvURL.indexOf('/home') != -1) {
			cvShopAudience = ReportSuiteID += ",amdvother19";
		} else if (cvURL.indexOf('/business') != -1) {
			cvShopAudience = ReportSuiteID += ",amdvother20";
		}
		if (cvURL.indexOf('category:') != -1) {
			if ((cvURL.indexOf('graphic+card') != -1) || (cvURL.indexOf('processor') != -1) || (cvURL.indexOf('motherboard') != -1)) {
				if (cvShopAudience == "") ReportSuiteID += ",amdvother22";
				ReportSuiteID += ",amdvother07";
			} else if ((cvURL.indexOf('notebook') != -1) ||(cvURL.indexOf('desktop') != -1) ||(cvURL.indexOf('tablet') != -1)) {
				if (cvShopAudience == "") ReportSuiteID += ",amdvother19";
				ReportSuiteID += ",amdvother17";
			} else if (cvURL.indexOf('server') != -1) {
				if (cvShopAudience == "") ReportSuiteID += ",amdvother20";
				ReportSuiteID += ",amdvother03";
			} else if (cvURL.indexOf('workstation') != -1) {
				if (cvShopAudience == "") ReportSuiteID += ",amdvother20";
				ReportSuiteID += ",amdvother07,amdvother16";
			}
		}
	} else if ((cvHostName == "http://www.amd.com/us/as/sp2013/forums.amd.com") || (cvHostName == "http://www.amd.com/us/as/sp2013/devforums.amd.com")) {
		if ((cvURL.indexOf("forums.amd.com/devforum") != -1) || (cvHostName == "http://www.amd.com/us/as/sp2013/devforums.amd.com")) {
			ReportSuiteID = "amdvother21,amdvdev,amdvforums";
		} else if (cvURL.indexOf("forums.amd.com/forum") != -1) {
			if (typeof(cvIsDevforum) !== 'undefined') {
				if (cvIsDevforum == "yes") {
					ReportSuiteID = "amdvother21,amdvdev,amdvforums";
				}
			} else {
				ReportSuiteID = "amdvsupport,amdvforums";
			}
		} else if (cvURL.indexOf("forums.amd.com/game") != -1) {
			ReportSuiteID = "amdvother18,amdvother22,amdvgame,amdvforums";
		} else {
			ReportSuiteID = "amdvsupport,amdvother22,amdvforums";
		}
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/phx.corporate-ir.net" || cvHostName == "http://www.amd.com/us/as/sp2013/ir.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/quarterlyearnings.amd.com") {
		ReportSuiteID = "amdvother08,amdvother20,amdvother05";
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/products.amd.com") {
		ReportSuiteID = "amdvother07";
	} else if ((cvHostName == "http://www.amd.com/us/as/sp2013/search.amd.com") || (cvURL.indexOf('/search/') != -1)){
		ReportSuiteID = "amdvother02";
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/cmp.amd.com") {
		ReportSuiteID = "amdvcmp01";
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/shop-qa.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shop-auth.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shop-sit.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shopcontent-auth.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shop-new.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shopapp-sit.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shop-preprod.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shopcontent-auth-sit.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shopcontent-auth-dit.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shopapp-dit.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shop-dit.amd.com" || cvHostName == "http://www.amd.com/us/as/sp2013/shopapp-design.amd.com") {
		ReportSuiteID = "amdvtest-fd";
	} else if (cvHostName == "http://www.amd.com/us/as/sp2013/server.amd.com") {
			ReportSuiteID = "amdvother03,amdvother20,amdvbusiness"; //amdvother03 = AMD Server
	} else if ((cvHostName == "http://www.amd.com/us/as/sp2013/global.amd.com") || (cvHostName == "http://www.amd.com/us/as/sp2013/experience.amd.com") || (cvHostName == "http://www.amd.com/us/as/sp2013/amd.mkt6303.com") || (cvHostName == "www.pages03.net/advancedmicrodevicesinc")) {
		ReportSuiteID = "amdverror";
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/seamicro.com") != -1) {
		ReportSuiteID = "amdvother03,amdvother20,amdvother11,amdvbusiness";
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/amd3.c-3.com") != -1) {
		ReportSuiteID = "amdvembedded,amdvother20,amdvother12";
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/gizmosphere.org") != -1) {
		ReportSuiteID = "amdvembedded,amdvother21,amdvother25";
	} else if ((cvURL.indexOf("http://www.amd.com/us/as/sp2013/radeonramdisk.com") != -1) || (cvURL.indexOf("http://www.amd.com/us/as/sp2013/radeonmemory.com") != -1)) {
		ReportSuiteID = "amdvother07,amdvother22,amdvother14"; //amvother14 = AMD Ramdisk
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/fireprographics.com") != -1) {
		ReportSuiteID = "amdvother07,amdvother20,amdvother16,amdvbusiness"; //amdvother07 = AMD Component
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/survey.foreseeresults.com") != -1) {
		ReportSuiteID = "amdvother06";
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/amd4u.com") != -1) {
		ReportSuiteID = "amdvother18,amdvother22";
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/querycn.php") != -1) {
		ReportSuiteID = "amdvsupport,amdvother19";
	} else if ((cvURL.indexOf('/mynda/') != -1) || (cvURL.indexOf('http://www.amd.com/us/as/sp2013/mynda.amd.com') != -1)) {
		ReportSuiteID = "amdvother08,amdvother20,amdvnda,amdvbusiness";
	} else if ((cvHostName.indexOf("http://www.amd.com/us/as/sp2013/-auth2007.amd.com") !=-1) || (cvHostName.indexOf("http://www.amd.com/us/as/sp2013/-auth.amd.com") !=-1) || (cvHostName.indexOf("http://www.amd.com/us/as/sp2013/-new.amd.com") !=-1) || (cvHostName.indexOf("http://www.amd.com/us/as/sp2013/-prod.amd.com") !=-1) || (cvHostName.indexOf("localhost") !=-1) || (cvURL.indexOf(".hosted.jivesoftware.com") !=-1)) {
		ReportSuiteID = "amdvtest";
	} else if (cvURL.indexOf("cchecker") != -1){
		ReportSuiteID = "amdvother18,amdvother19";
	} else if ((cvURL.indexOf("amdcentral") != -1) || (cvURL.indexOf("singanet") != -1)){
		ReportSuiteID = "amdvcentral";
		if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/amdcentral.amd.com") != -1) {
			ReportSuiteID += ",amdvcentral-corp";
			if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/emea-amdcentral.amd.com") != -1) {ReportSuiteID += ",amdvcentral-emea";}
			else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/cn-amdcentral.amd.com") != -1) {ReportSuiteID += ",amdvcentral-cn";}
			else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/mys-amdcentral.amd.com") != -1) {ReportSuiteID += ",amdvcentral-mys";}
			else if ((cvURL.indexOf("http://www.amd.com/us/as/sp2013/sgp-amdcentral.amd.com") != -1) || (cvURL.indexOf("singanet") != -1)) {ReportSuiteID += ",amdvcentral-sgp";}
		}
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/videocentral.amd.com") != -1) {
		ReportSuiteID += ",amdvvideocentral";
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/searchamdcentral.amd.com") != -1) {
		ReportSuiteID += ",amdvcentral-search";
	} else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/connect.amd.com") != -1) {ReportSuiteID += "amdvconnect";
	} else if ((cvURL.indexOf('http://www.amd.com/us/as/sp2013/ati.com') != -1) || (cvURL.indexOf('file:') != -1) || (cvURL.indexOf('translate') != -1) || (cvURL.indexOf('http://www.amd.com/us/as/sp2013/proxy.ashx') != -1) || (cvURL.indexOf('webcache') != -1)) {
		ReportSuiteID = "amdvother30";
	} else {
		ReportSuiteID = "amdvother";
	}

	//if ((ReportSuiteID.indexOf("amdvtest") == -1) && (ReportSuiteID.indexOf("amdvother30") == -1) && (ReportSuiteID.indexOf("amdvother23") == -1)){

	if ((ReportSuiteID.indexOf("amdvtest") == -1) && (ReportSuiteID.indexOf("amdvcentral") == -1) && (ReportSuiteID.indexOf("amdvconnect") == -1) && (ReportSuiteID.indexOf("amdvother30") == -1) && (cvURL.indexOf('download/ccc') == -1) && (cvURL.indexOf('/ccc/') == -1)){
		if  (cvURL.indexOf('http://www.amd.com/us/as/sp2013/support.amd.com') == -1) { //exclude support.amd.com from regional RS
			/* Adding AMD Regional Accounts */
			if ((cvURL.indexOf("/cn/") != -1) || (cvURL.indexOf("zh-cn") != -1) || (cvURL.indexOf(".cn") != -1) || (cvURL.indexOf("http://www.amd.com/us/as/sp2013/querycn.php") != -1) || (cvURL.indexOf("/gc/") != -1)) {
				ReportSuiteID = "amdvregions-cn," + ReportSuiteID;	// AMD China
				cvCurrency = "CNY";
			} else if ((cvURL.indexOf("/tw/") != -1)  || (cvURL.indexOf("zh-tw") != -1)){
				ReportSuiteID = "amdvregions-tw," + ReportSuiteID;	// AMD Taiwan
				cvCurrency = "TWD";
			} else if ((cvURL.indexOf("/kr/") != -1) || (cvURL.indexOf("ko-kr") != -1)) {
				ReportSuiteID = "amdvmegaregion-apac,amdvregions-kr," + ReportSuiteID;	// AMD Korea
				cvCurrency = "KRW";
			} else if ((cvURL.indexOf("/jp/") != -1) || (cvURL.indexOf("ja-jp") != -1)){
				ReportSuiteID = "amdvmegaregion-apac,amdvregions-jp," + ReportSuiteID;	// AMD Japan
				cvCurrency = "JPY";
			} else if (cvURL.indexOf("/sg/") != -1) {
				ReportSuiteID = "amdvmegaregion-apac,amdvregions-sg," + ReportSuiteID;	// AMD Singapore
				cvCurrency = "SGD";
			} else if (cvURL.indexOf("/au/") != -1) {
				ReportSuiteID = "amdvmegaregion-apac,amdvregions-au," + ReportSuiteID;	// AMD Australia
				cvCurrency = "AUD";
			} else if (cvURL.indexOf("/my/") != -1) {
				ReportSuiteID = "amdvmegaregion-apac,amdvregions-my," + ReportSuiteID;	// AMD Malaysia
				cvCurrency = "MYR";
			} else if (cvURL.indexOf("/in/") != -1) {
				ReportSuiteID = "amdvmegaregion-apac,amdvregions-in," + ReportSuiteID;	// AMD India
				cvCurrency = "INR";
			} else if (cvURL.indexOf("/hk/") != -1) {
				ReportSuiteID = "amdvmegaregion-apac,amdvregions-hk," + ReportSuiteID;	// AMD Hong Kong (English)
				cvCurrency = "HKD";
			} else if (cvURL.indexOf("/apj/") != -1) {
				ReportSuiteID = "amdvmegaregion-apac," + ReportSuiteID;	// AMD APJ
			} else if ((cvURL.indexOf("/ca/") != -1) || (cvURL.indexOf("/us/") != -1) || (cvURL.indexOf("en-us") != -1) || (cvURL.indexOf("en-na") != -1) || (cvURL.indexOf("/na/") != -1)) {
				ReportSuiteID = "amdvmegaregion-na," + ReportSuiteID;	// AMD North America (English)
			} else if ((cvURL.indexOf("/la/") != -1) || (cvURL.indexOf("es-la") != -1) || (cvURL.indexOf("es-xl") != -1)) {
				ReportSuiteID = "amdvmegaregion-la,amdvregions-la," + ReportSuiteID;	// AMD Latin America (Espanol)
			} else if ((cvURL.indexOf("/br/") != -1) || (cvURL.indexOf("pt-br") != -1)) {
				ReportSuiteID = "amdvmegaregion-la,amdvregions-br," + ReportSuiteID;	// AMD Brazil (Portugues)
				cvCurrency = "BRL";
			} else if ((cvURL.indexOf("en-mx") != -1) || (cvURL.indexOf("es-mx") != -1)) {
				ReportSuiteID = "amdvmegaregion-la," + ReportSuiteID;	// AMD Mexico (English & Spanish)
				cvCurrency = "MXN";
			} else if ((cvURL.indexOf("/uk/") != -1) || (cvURL.indexOf("en-gb") != -1)) {
				ReportSuiteID = "amdvmegaregion-emea,amdvregions-uk," + ReportSuiteID;	// AMD United Kingdom (English)
				cvCurrency = "GBP";
			} else if ((cvURL.indexOf("/es/") != -1) || (cvURL.indexOf("es-es") != -1)) {
				ReportSuiteID = "amdvmegaregion-emea,amdvregions-es," + ReportSuiteID;	// AMD Spain (Espanol)
			} else if ((cvURL.indexOf("/fr/") != -1) || (cvURL.indexOf("fr-fr") != -1)) {
				ReportSuiteID = "amdvmegaregion-emea,amdvregions-fr," + ReportSuiteID;	// AMD France (Francais)
			} else if ((cvURL.indexOf("/de/") != -1) ||(cvURL.indexOf("de-de") != -1)) {
				ReportSuiteID = "amdvmegaregion-emea,amdvregions-de," + ReportSuiteID;	// AMD Germany
			} else if ((cvURL.indexOf("/it/") != -1) || (cvURL.indexOf("it-it") != -1)) {
				ReportSuiteID = "amdvmegaregion-emea,amdvregions-it," + ReportSuiteID;	// AMD Italy (Italiano)
			} else if ((cvURL.indexOf("/pl/") != -1) || (cvURL.indexOf("pl-pl") != -1)){
				ReportSuiteID = "amdvmegaregion-emea,amdvregions-pl," + ReportSuiteID;	// AMD Poland (Polski)
				cvCurrency = "PLN";
			} else if ((cvURL.indexOf("/ru/") != -1) || (cvURL.indexOf("ru-ru") != -1)){
				ReportSuiteID = "amdvmegaregion-emea,amdvregions-ru," + ReportSuiteID;	// AMD Russia (Russian)
				cvCurrency = "RUB";
			} else if ((cvURL.indexOf("/tr/") != -1) || (cvURL.indexOf("tr-tr") != -1)) {
				ReportSuiteID = "amdvmegaregion-emea,amdvregions-tr," + ReportSuiteID;	// AMD Turkey (Turkce)
				cvCurrency = "TRY";
			} else if (cvURL.indexOf("/emea/") != -1) {
				ReportSuiteID = "amdvmegaregion-emea," + ReportSuiteID;	// emea
			}
		}
		if (/,$/i.test(ReportSuiteID)) {ReportSuiteID = ReportSuiteID+"amdvglobal";}
		else {ReportSuiteID = ReportSuiteID+",amdvglobal";}
	}

	/* cross-visit attribution (CVA) tracking BOF */
	//check for cookie and get value
	cvCurrentDT = new Date();
	cvCurrentDTUTC = Date.UTC(cvCurrentDT.getUTCFullYear(), cvCurrentDT.getUTCMonth(), cvCurrentDT.getUTCDate(), cvCurrentDT.getUTCHours(), cvCurrentDT.getUTCMinutes(), cvCurrentDT.getUTCSeconds());
	cvExpiredDT = cvCurrentDTUTC-20*60000; //20 minutes older than current
	cvCVA=""; cvCVAReferrerDomainSplit=""; cvCVARefType="NA";
	cvCVA = cfGetCookie('c_sccva') || ""; //sccva = SiteCatalyst cross visit attribution
	if (cvCVA != "") { //cookie exists
		cvCVASplit = cvCVA.split(',');

		if (!cfis_int(cvCVASplit[0])){cvCVASplit[0]=cvCurrentDTUTC;}
		if (cvExpiredDT > cvCVASplit[0]) { //older than 20 minutes
			if (cfGetByteSize(cvCVA) > 950) {
				if(cvCVA.indexOf("CV")) {
					cvCVASplit = [cvCurrentDTUTC,"CV","Clear"];
				} else {
					cvCVASplit = [cvCurrentDTUTC,"Clear"];
				}
			}
			cvCVARefTypeResult = cfCVARefTypeCheck();
			cvCVASplit.shift(); //drop last time stamp
			cvCVA=cvCurrentDTUTC+","+cvCVASplit+","+cvCVARefTypeResult;
			cfSetCookie('c_sccva',cvCVA,365);
		} //else within 20 minutes, do nothing
	} else {
		//no cookie; first time visitor
		cvCVARefTypeResult = cfCVARefTypeCheck();
		cfSetCookie('c_sccva',cvCurrentDTUTC+","+cvCVARefTypeResult,365);
	}

	/* cross-visit attribution (CVA) tracking EOF */

	ReportSuiteID = ReportSuiteID.toLowerCase();
	if ((ReportSuiteID.indexOf("amdvmegaregion-emea") != -1) && cvCurrency =="USD") cvCurrency = "EUR";

	//TimedReading Config
	if (/.*amdcentral/i.test(cvURLCheck)) {
		//exclude
	} else if (/.*amdhomepage.aspx/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},18000);
	} else if (/.*support.*(download|download\/|ccc|download#\d)$/i.test(cvURLCheck)) {
		//exclude
	} else if (/.*(ccc|compatcheck).*/i.test(cvURLCheck)) {
		//exclude
	} else if (/.*experience.*notifyme.*/i.test(cvURLCheck)) {
		//exclude
	} else if (/.*support.*kb-articles.*/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},40000);
	} else if (/.*phx.corporate-ir.net/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},25000);
	} else if (ReportSuiteID.indexOf("amdvother08") != -1) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},18000);
	} else if (/.*shop.*shopamd.aspx/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},13000);
	} else if (/.*shop.*search.*/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},30000);
	} else if (/.*shop.*detail.*/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},8000);
	} else if (/.*community\.amd\.com.*/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},27000);
	} else if (/.*vision\/shop\/cool-apps.*/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},15000);
	} else if (/.*amdservercomparisontool.aspx/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},25000);
	} else if (/.*server-selector.aspx/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},23000);
	} else if (/.*wwwd\.amd\.com/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},25000);
	} else if (/.*search\.amd\.com/i.test(cvURLCheck)) {
		cvTimedReading = setTimeout(function(){cfTimedReading()},8000);
	} else {
		cvTimedReading = setTimeout(function(){cfTimedReading()},21000);
	}

	return [ReportSuiteID,cvCurrency];

}

function getUTCDate() {
	cvCurrentDT = new Date();
	return Date.UTC(cvCurrentDT.getUTCFullYear(), cvCurrentDT.getUTCMonth(), cvCurrentDT.getUTCDate(), cvCurrentDT.getUTCHours(), cvCurrentDT.getUTCMinutes(), cvCurrentDT.getUTCSeconds());
}

// CVA Cross Visit Attribution BOF

function cfCVAConversion(cvConversionType) {
// delete
	if (cvConversionType == "") cvConversionType = "UK";
	cvCVA = cfGetCookie('c_sccva') || ""; //sccva = SiteCatalyst cross visit attribution
	if (cvCVA != "") { //cookie exists
		cvCVASplit = cvCVA.split(',');
		if (!cfis_int(cvCVASplit[0])){cvCVASplit[0]=cvCurrentDTUTC;}
		if (cfGetByteSize(cvCVA) > 950) {
			if(cvCVA.indexOf("CV")) {
				cvCVASplit = [cvCurrentDTUTC,"CV","Clear"];
			} else {
				cvCVASplit = [cvCurrentDTUTC,"Clear"];
			}
		}
		cvCVASplit.shift(); //drop last time stamp
		cvCVAtoOSC=cvCVASplit+",CV|"+cvConversionType;
		cvCVA=cvCurrentDTUTC+","+cvCVAtoOSC;
		cfSetCookie('c_sccva',cvCVA,365);
		return cvCVAtoOSC;
	}
	else { //no cookie;
		cvCVARefTypeResult = cfCVARefTypeCheck();
		cvCVAtoOSC=cvCVARefTypeResult+",CV|"+cvConversionType;
		cvCVA=cvCurrentDTUTC+","+cvCVAtoOSC;
		cfSetCookie('c_sccva',cvCVA,365);
		return cvCVAtoOSC;
	}
}

function cfFormPassed(cvFormPassedName) {
	dataLayer.push({"event":"FormComplete","FormName":cvFormPassedName});
// may be able to remove using Form trigger.
// will need to keep empty function for pages making call
if (cvURL.substring(0, 5) != "https") {
	nodesForm = o.form;
	cvLinkType='Form|SP'; //Silverpop form
	if(typeof cvFormPassedName =='undefined') {
		if(typeof nodesForm.name !== 'undefined') cvFormName = nodesForm.name;
		else cvFormName = "error";
	} else cvFormName = cvFormPassedName;
	if (typeof(nodesForm.action) !== 'undefined') {Destination_URL_wParam = unescape(nodesForm.action.toLowerCase());}
	Destination_LID = Destination_Name = cvLinkType + "|" + cvFormName+"|Complete";
	Destination_LPOS = "Forms";

	s.prop12=cvURLCheck;
	s.prop13=s.eVar13=Destination_LID;
	s.prop14=s.eVar6=Destination_URL_wParam;
	s.prop15=Destination_LPOS;
	s.eVar20 = cfCVAConversion("FC");
	s.events = s.apl(s.events,"event6",",",1);
	s.events = s.apl(s.events,"event98",",",1);
	s.linkTrackEvents = s.events;
	s.prop25= ReportSuiteID.toLowerCase(); //"amdvother";

	if (/www.amd.com\/en-us\/solutions\/pro(|\/)$/i.test(cvURLCheck)) { //09-02-2014|11-23-2014
		if (Destination_LID.indexOf("Form|SP|Commercial-News-Solutions-Pro|Complete") != -1) {
			s.events = s.apl(s.events,"event62",",", 1);
			s.eVar20 = cfCVAConversion("MC");
		}
	}
	cfMediaMindClickTrack(Destination_LID, Destination_URL_wParam);

	if(cvDNTDomains.indexOf(cfUtility(cvURL,'server')) == -1) {
		s.visitorNamespace="amd";
		s.trackingServer="http://www.amd.com/us/as/sp2013/metrics.amd.com";
		s.trackingServerSecure="http://www.amd.com/us/as/sp2013/smetrics.amd.com";
	}
	cvSTL = 1;
	s.tl(this,'o',cvURL);
	s.events = s.linkTrackEvents = ''; cvSTL = 0;
	cfPause(250);
}
}


function cfCVARefTypeCheck() {
// delete - need to confirm
	//cvCVAReferrerURL=document.referrer.toLowerCase();
	//cvCVAReferrerDomain=cfUtility(document.referrer.toLowerCase(),'server');
	//cvCVAReferrerDomainSplit = cvCVAReferrerDomain.split('.');
//consider multiple cvCVARefType per instance

	//if (((cvCVAReferrerURL) && (cvCVAReferrerURL.indexOf("eloqua") != -1)) || (cvURL.indexOf("elq=") != -1)) {cvCVARefType="EM"; }
	//else if (cvURL.indexOf("s_kwcid=") != -1) {cvCVARefType="PS";}
	//else if (cvURL.indexOf("cmpid=") != -1) {
	//	if (cvURL.indexOf("cmpid=BC") != -1) {cvCVARefType="QR";}
	//	else {cvCVARefType="CP";}
	//}
	//else if ((cvCVAReferrerDomain) && (cvCVAReferrerDomain.indexOf("google") != -1)) {
	//	if (cvCVAReferrerDomain.indexOf("plus.google")!= -1) {cvCVARefType="SM";}
		//else if (cvURL.indexOf("mail.google")!= -1) {cvCVARefType="WM";}
	//	else {cvCVARefType="OS";}
	//}
	//else if ((cvCVAReferrerURL) && cvSearchDomains.indexOf(parseURL(cvCVAReferrerURL).host)) {cvCVARefType="OS";}
	//else if ((cvCVAReferrerURL) && cvSocialDomains.indexOf(parseURL(cvCVAReferrerURL).host)) {cvCVARefType="SM";}
	//else if ((cvCVAReferrerURL) && cvAMDShortDomains.indexOf(parseURL(cvCVAReferrerURL).host)) {cvCVARefType="LS";}
	//else if ((cvCVAReferrerURL) && (cvCVAReferrerURL.indexOf("file:") != -1)) {cvCVARefType="HD";}
	//else if (cvURL.indexOf("#sthashtag") != -1) {cvCVARefType="STCS";} //STCS = ShareThis CopyNShare
	return ""; //cvCVARefType;
}
// CVA Cross Visit Attribution EOF

//SP2013 shop BOF
function cfSetShopValues() { //function called onpostrender of shop item/detail page
if (cvURL.substring(0, 5) != "https") {
	if (typeof document.getElementsByName("shopUniqueID")[0] != "undefined") {
		s.prop32 = s.eVar32 = document.getElementsByName("shopUniqueID")[0].value;   // ProductID
		s.products = ";" + document.getElementsByName("shopUniqueID")[0].value;   // ProductID
	}
}
	// s.visitorNamespace="amd";
	// s.trackingServer="http://www.amd.com/us/as/sp2013/metrics.amd.com";
	// s.trackingServerSecure="http://www.amd.com/us/as/sp2013/smetrics.amd.com";

	// cvSTL = 1;
	// s.tl(this,'o',cvURL);
	// s.clearVars();
	// s.events = s.linkTrackEvents = ''; cvSTL = 0;

} //SP2013 shop EOF

//on-site search (OSS) BOF
cvtimer=1;
cvSavedSearchKeywordCheck = new Array();
cvSavedSearchKeywordCheck[0] = "";
cvSavedSearchKeywordCheck[1] = "";

function cfGetSearchKeywordFromURL(){
	cvSearchKeywordCheck=cfGetQParamwithHash("k",document.URL.toLowerCase());
	if(cvSearchKeywordCheck.indexOf("#s=") != -1) {
		cvSearchKeywordCheck=cvSearchKeywordCheck.substring(0,cvSearchKeywordCheck.indexOf("#s="));
	}
	if(cvSearchKeywordCheck.indexOf("#k=") != -1) {
		cvSearchKeywordCheck=cvSearchKeywordCheck.substring(cvSearchKeywordCheck.lastIndexOf("#k=")+3);
	}
	return cvSearchKeywordCheck;
}

function cfCheckSearchAjax() {
	if(document.getElementById('Result')) {
		cvSearchHTML = document.getElementById('Result').innerHTML;
		cvSearchURL = decodeURI(document.URL.toLowerCase());
		if ((cvSearchHTML == document.getElementById('Result').innerHTML) && cvCheckSearchAjax!='') {
			window.clearInterval(cvCheckSearchAjax);
			cvCheckSearchAjax = '';
			cfTrackSearch1();
		}
	}
}
//csKeyword, csSearchOriginationPage, csTotalCounts, csResultsRange, csPageNumber
function cfTrackSearch1() {
if (cvURL.substring(0, 5) != "https") {
	cvSavedSearchKeywordCheck[0] = cvSavedSearchKeywordCheck[1];
	cvSavedSearchKeywordCheck[1] = cfGetSearchKeywordFromURL();

	//if ((typeof csKeyword != 'undefined') && (cvLinkedSearch == 0)) {

	if (typeof csKeyword == 'undefined') {
		csKeyword = cfGetSearchKeywordFromURL();
			}
		if (typeof csKeyword != 'undefined') {	
		if (csKeyword != '') {
			s.prop6 = s.server; // Search Property ID
			s.prop55 = s.eVar55 = csKeyword.toLowerCase(); // Internal Search Terms
			if (typeof s.referrer != 'undefined') s.eVar39 = s.prop11 = s.referrer;
			if (typeof csTotalCounts != 'undefined') {
				if (csTotalCounts == 0) {
					// if($(".ms-srch-group-count")) {
					if($("*").hasClass("ms-srch-group-count")) {
						csTotalCounts=$( ".ms-srch-group-count" ).text().match(/\d/g).join("");
					}
				}
				s.prop36 = s.eVar36 = csTotalCounts; // Total Counts
			}

			if (cfCookieEnabled()) {
				cvCookieDURL = cfGetCookie('c_durl') || "";
				if (/shop\.amd\.com\/.*k=.*/i.test(cvCookieDURL)) {cvLinkedSearch=1;}
				else cvLinkedSearch=0;
				cfSetCookie('c_durl',"",1); //durl = cleared
			}

			if(cvSavedSearchKeywordCheck[0] == "" || ((cvSavedSearchKeywordCheck[0] != cvSavedSearchKeywordCheck[1]) && cvSavedSearchKeywordCheck[0] != "")) {
				s.events = s.apl(s.events,"event5",",",1); // On-Site Search (Counter)
				//if (cvSPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-counter',link:o }); }
				//if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-counter',link:o }); } )(); }

				//if (csTotalCounts == 0) {
				if ($( "#NoResult").length ==1) {
					s.prop9 = s.eVar9 = csKeyword.toLowerCase(); // Internal Search Failed Terms
					s.events = s.apl(s.events,"event12",",",1); // On-Site Search (Failed)
					//if (cvSPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-failed',link:o }); }
					//if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-failed',link:o }); } )(); }
				} else {
					s.events = s.apl(s.events,"event11",",",1); // On-Site Search (Success)
					//if (cvSPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-success',link:o }); }
					//if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-success',link:o }); } )(); }
				}
			}
			s.prop37 = s.eVar37 = Math.round(csPageNumber); // Results Page Number
			s.prop38 = s.eVar38 = csKeyword.toLowerCase() + "|" + csResultsRange; // Keywords | Results Range

			// On-Site Search (Advanced) Event
			//if (csAdvancedSearch == '1') s.events = s.apl(s.events,"event14",",",1);
			cvSTL = 1;
			s.tl(this,'o',cvURL);
			s.clearVars();
			s.events = s.linkTrackEvents = ''; cvSTL = 0;
		}
	}
}
}

function cfTrackSearch() {
//empty
}

/* Banner tracking BOF */
function cfBannerImpression(cvBIrs,cvBIprop,cvBIevar,cvBIevent,cvBIlnkname) {
/*
	//random_number = Math.floor((Math.random()*100)+1);
	//if (random_number < 1) //number is percent chance
	//{
		//var s=s_gi(cvBIrs);
	if (cvURLCheck.indexOf("/consumer/if-it-can-game") != -1) {
		s.account = ReportSuiteID;
		s.linkTrackEvents=cvBIevent;
		s.prop12=s.pageName;
		s.prop25 = ReportSuiteID;
		s.prop13=s.eVar26=cvBIlnkname;
		s.events=cvBIevent;

		if(cvDNTDomains.indexOf(cfUtility(cvURL,'server')) == -1) {
			s.visitorNamespace="amd";
			s.trackingServer="http://www.amd.com/us/as/sp2013/metrics.amd.com";
			s.trackingServerSecure="http://www.amd.com/us/as/sp2013/smetrics.amd.com";
		}

		cvSTL = 1;
		s.tl(this,'o',cvBIlnkname);
		s.events = s.linkTrackEvents = ''; cvSTL = 0;
	}

	//} //end random number condition

*/
}
/* Banner tracking EOF */

/* NotifyMe Error BOF */
function cfNotifyMeError(cvProduct) {
	dataLayer.push({"event":"NotifyMeError","NFMProductName":cvProduct});
	// remove for switch to GTM
	if (cvURL.substring(0, 5) != "https") {
	s.account = ReportSuiteID;
	s.linkTrackEvents="";
	s.prop12=s.pageName;
	s.prop25 = ReportSuiteID;
	s.prop13=s.eVar13="Form|SP|NotifyMe|"+cvProduct+"|Error";
	s.prop23 = s.eVar23 = s.referrer;
	s.events="";

	if(cvDNTDomains.indexOf(cfUtility(cvURL,'server')) == -1) {
		s.visitorNamespace="amd";
		s.trackingServer="http://www.amd.com/us/as/sp2013/metrics.amd.com";
		s.trackingServerSecure="http://www.amd.com/us/as/sp2013/smetrics.amd.com";
	}

	cvSTL = 1;
	s.tl(this,'o',cvURL);
	s.events = s.linkTrackEvents = ''; cvSTL = 0;
	// end of remove
	}
}
/* NotifyMe Error tracking EOF */


/* ShareThis BOF */
/*
function trackWithOmniture (event,service) {
	s.eVar54 = event+": "+service; //only event type "click" is supported; the service shared by user. e.g. facebook
}
if (typeof stLight != "undefined") {
	if (typeof stLight.subscribe === "function"){
		stLight.subscribe("click",trackWithOmniture); //register the callback function with sharethis
	}
}
*/
/* ShareThis EOF */

/* Custom Function for Referrer Tracking */
function cfNoReferrer (cvURL) {
	if (cvURL.substring(0, 5) != "https") {
	if (cvURL.indexOf("elq=") != -1) {
		s.referrer = "mail://eloqua.email.campaign/";
		cvReferrer = s.prop23 = s.eVar23 = s.referrer;
	}
	}
}

function cfReferrer (cvReferrer) {
	if (cvURL.substring(0, 5) != "https") {
	if (cvReferrer.indexOf("/plus.url.google.com/") != -1) {
		/* Clean up Google Plus referrer values */
		document.referrer=cvReferrer;
		s.cleanGP=new Function("var s=this,a=document.referrer,b='plus.url.go"+"ogle.com',c,d,e,g,i=0,p,referrer;if(a&&a.split('/')[2]==b){c=a.spli"+"t(/[\?|&]/);d=c[0].length;e=c[0].lastIndexOf('/');g=d>e?c[0].substr"+"ing(0,e+1):c[0];for(i=0;i<=c.length;i++)'url'==c[i].substring(0,3)&"+"&(p='?'+unescape(c[i]),i=c.length+1);g&&(this.s.referrer=p?g+p:g);}"+"return this.s.referrer")();

		cvReferrer=this.s.referrer;
	}
	else if (cvReferrer.indexOf("/links.global.amd.com/") != -1) {
		cvReferrer = document.referrer = s.referrer = "mail://eloqua.email.campaign/";
	}

	cvReferrer = cvReferrer.toLowerCase();

	if (cvReferrer && (cfPageName(cvReferrer) != s.pageName)) {
		s.prop23 = cfUtility(cvReferrer,'server');	// Referring Domain
		s.referrer = s.eVar23 = cvReferrer;	// Referring URL
		return this.s.referrer;
	}
	}
}

/* Custom Functions for Responsive Web (RW) BOF */
// http://blogs.adobe.com/digitalmarketing/digital-marketing/mobile/responsive-web-design-and-web-analytics/

// userAgent assignment
	cvUA="";
	if (navigator.userAgent) cvUA=navigator.userAgent;
	cvPlatformCategory="";

/* RW: Device by UA - eVar41 */
function cfDeviceByUA(){
	catergorize_tablets_as_desktops = false;  //If TRUE, tablets will be categorized as desktops
	catergorize_tvs_as_desktops = false;  //If TRUE, smartTVs will be categorized as desktops

	// Check if user agent is a smart TV - http://goo.gl/FocDk
	if (/GoogleTV|SmartTV|Internet.TV|NetCast|NETTV|AppleTV|boxee|Kylo|Roku|DLNADOC|CE\-HTML/i.test(cvUA))
		{cvPlatformCategory = "tv";}
	// Check if user agent is a TV Based Gaming Console
	else if (/Xbox|PLAYSTATION.3|Wii/i.test(cvUA))
		{cvPlatformCategory = "gaming console";}
	// Check if user agent is a Tablet
	else if(/iP(a|ro)d/i.test(cvUA) || /tablet/i.test(cvUA) && !(/RX-34/i.test(cvUA)) || /FOLIO/i.test(cvUA))
		{cvPlatformCategory = "tablet";}
	// Check if user agent is an Android Tablet
	else if (/Linux/i.test(cvUA) && /Android/i.test(cvUA) && !(/Fennec|mobi|HTC.Magic|HTCX06HT|Nexus.One|SC-02B|fone.945/i.test(cvUA)))
		{cvPlatformCategory = "tablet";}
	// Check if user agent is a Kindle or Kindle Fire
	else if (/Kindle/i.test(cvUA) || /Mac.OS/i.test(cvUA) && /Silk/i.test(cvUA))
		{cvPlatformCategory = "tablet";}
	// Check if user agent is a pre Android 3.0 Tablet
	else if (/GT-P10|SC-01C|SHW-M180S|SGH-T849|SCH-I800|SHW-M180L|SPH-P100|SGH-I987|zt180|HTC(.Flyer|\_Flyer)|Sprint.ATP51|ViewPad7|pandigital(sprnova|nova)|Ideos.S7|Dell.Streak.7|Advent.Vega|A101IT|A70BHT|MID7015|Next2|nook/i.test(cvUA) || /MB511/i.test(cvUA) && /RUTEM/i.test(cvUA))
		{cvPlatformCategory = "tablet";}
	// Check if user agent is unique Mobile User Agent
	else if (/BOLT|Fennec|Iris|Maemo|Minimo|Mobi|mowser|NetFront|Novarra|Prism|RX-34|Skyfire|Tear|XV6875|XV6975|Google.Wireless.Transcoder/i.test(cvUA))
		{cvPlatformCategory = "mobile";}
	// Check if user agent is an odd Opera User Agent - http://goo.gl/nK90K
	else if (/Opera/.test(cvUA) && /Windows.NT.5/i.test(cvUA) && /HTC|Xda|Mini|Vario|SAMSUNG\-GT\-i8000|SAMSUNG\-SGH\-i9/i.test(cvUA))
		{cvPlatformCategory = "mobile";}
	// Check if user agent is Windows Desktop
	else if (/Windows.(NT|XP|ME|9)/i.test(cvUA) && !(/Phone/i.test(cvUA)) || /Win(9|.9|NT)/i.test(cvUA))
		{cvPlatformCategory = "desktop";}
	// Check if agent is Mac Desktop
	else if (/Macintosh|PowerPC/i.test(cvUA) && !(/Silk/i.test(cvUA)))
		{cvPlatformCategory = "desktop";}
	// Check if user agent is a Linux Desktop
	else if (/Linux/.test(cvUA) && /X11/i.test(cvUA))
		{cvPlatformCategory = "desktop";}
	// Check if user agent is a Solaris, SunOS, BSD Desktop
	else if (/Solaris|SunOS|BSD/i.test(cvUA))
		{cvPlatformCategory = "desktop";}
	// Check if user agent is a Desktop BOT/Crawler/Spider
	else if (/Bot|Crawler|Spider|Yahoo|ia_archiver|Covario-IDS|findlinks|DataparkSearch|larbin|Mediapartners-Google|NG-Search|Snappy|Teoma|Jeeves|TinEye/i.test(cvUA) && !(/Mobile/i.test(cvUA)))
		{cvPlatformCategory = "bot/crawler/spider";}
	else if (cvUA=="") {cvPlatformCategory ="NoUA";}
	// Otherwise assume it is a Mobile Device
	else {cvPlatformCategory = "mobile";}

	// Categorize Tablets as desktops
	if (catergorize_tablets_as_desktops && cvPlatformCategory == "tablet")
		{cvPlatformCategory = "desktop";}

	// Categorize TVs as desktops
	if (catergorize_tvs_as_desktops && cvPlatformCategory == "tv")
		{cvPlatformCategory = "desktop";}

	cvDeviceByUA = cvPlatformCategory;
	return cvDeviceByUA;
} // End cfDeviceByUA function

// Returns true if desktop user agent is detected
function isDesktop(){
	cvDeviceByUA = cfDeviceByUA();
	if(cvDeviceByUA == "desktop"){return true;}
	return false;
}
// Returns true if tablet user agent is detected
function isTablet(){
	cvDeviceByUA = cfDeviceByUA();
	if(cvDeviceByUA == "tablet"){return true;}
	return false;
}
// Returns true if tablet user agent is detected
function isTV(){
	cvDeviceByUA = cfDeviceByUA();
	if(cvDeviceByUA == "tv"){return true;}
	return false;
}
// Returns true if mobile user agent is detected
function isMobile(){
	cvDeviceByUA = cfDeviceByUA();
	if(cvDeviceByUA == "mobile"){return true;}
	return false;
}



/* Custom Function for Link Tracking */
function cfLnkTrack (o,ReportSuiteID_Links,ReportSuiteID) {
	cvLinkType='';
	if (typeof o.tagName != 'undefined') cvTagName=o.tagName.toLowerCase();
	if (typeof o.parentNode != 'undefined') {
		cvPTagName=o.parentNode.tagName.toLowerCase();
		if (typeof o.parentNode.parentNode != 'undefined' && o.parentNode.tagName.toLowerCase() != 'html') {
			if (typeof o.parentNode.parentNode.tagName != 'undefined') {
				cvPPTagName=o.parentNode.parentNode.tagName.toLowerCase();
				cvPPClassName=o.parentNode.parentNode.className.toLowerCase();
			}
			if (typeof o.parentNode.parentNode.parentNode != 'undefined' && o.parentNode.parentNode.tagName.toLowerCase() != 'html') {
				if (typeof o.parentNode.parentNode.parentNode.tagName != 'undefined') {
					cvPPPTagName=o.parentNode.parentNode.parentNode.tagName.toLowerCase();
					cvPPPClassName=o.parentNode.parentNode.parentNode.className.toLowerCase();
				}
			}
		}
	}
	if (cvTagName=='a') {
		cvLinkType='Link|A';
		if (typeof(o.href) !== 'undefined') {Destination_URL_wParam = o.href.toLowerCase();}
		if (typeof o.className !=="undefined") {
			if ((o.className.indexOf("menu-item")!=-1) || (o.className.indexOf("ms-core-listMenu-item")!=-1) || (o.className.indexOf("menu-item-text")!=-1) || (o.className.indexOf("static")!=-1)) {
				cvLinkType='Link|LeftNavlinks';
			}
		}
		if (typeof cvPPPClassName !=="undefined") {
			if (cvPPPClassName =="breadcrumb") {
				cvLinkType='Link|Breadcrumb';
			}
		}
		try{cvFoundChild=cfFindChild(o,"IMG");} catch(err){}
		if (typeof cvFoundChild != "undefined") {
			cvLinkType='Link|IMG';
			if (typeof(cvFoundChild.src) !== 'undefined') {
				cvImageSRC = cfUtility(cvFoundChild.src,'channel')+cfUtility(cvFoundChild.src,'filename');
				cvImageSRC = cvImageSRC.replace(/(publishingimages.photography)/i,"...");
				cvImageSRC = cvImageSRC.replace(/(publishingimages.public)/i,"");
				cvImageSRC = cvImageSRC.replace(/(flash|style library|graphic_illustrations)/i,"...");
				cvImageSRC = cvImageSRC.replace(/webbannerjpeg/i,"...");
				cvImageSRC = cvImageSRC.replace(/(now.eloqua.com|images.global.amd.com\/eloquaimages\/clients\/advancedmicrodevicesamdmae\/({|%7b).*(}|%7d)_)/i,"...");
			}
			if (typeof(cvFoundChild.alt) !== 'undefined') {
				Destination_LID = "IMG|ALT|"+cvImageSRC+"|"+unescape(cvFoundChild.alt);
			} else if (typeof(cvFoundChild.name) !== 'undefined') {
				Destination_LID = "IMG|NAME|"+cvImageSRC+"|"+unescape(cvFoundChild.name);
			} else if (typeof(cvFoundChild.id) !== 'undefined') {
				Destination_LID = "IMG|ID|"+cvImageSRC+"|"+unescape(cvFoundChild.id);
			} else if (typeof(cvFoundChild.src) !== 'undefined'){
				Destination_LID = "IMG|SRC|"+cvImageSRC;
			}
			if (typeof(o.name) !== 'undefined') {Destination_Name = unescape(o.name);}

		} //EOF cvFoundChild=cfFindChild(o,"IMG");
		else if (typeof cvPPPTagName != 'undefined') {
			if (cvPPPTagName=='div') {
				if (cvPPPClassName.indexOf("where_to_buy") != -1) {
					cvLinkType='Link|Shop'; //Shop WTB
				} else if (cvPPPClassName.indexOf("navlinks") != -1) {
					cvLinkType='Link|HeaderNavlinks';
				}
			}
		}
		if  (/((en-us|de-de|en-gb|es-es|es-xl|fr-fr|it-it|ja-jp|ko-kr|pl-pl|pt-br|ru-ru|tr-tr|zh-cn|zh-tw)|(en-us\/|de-de\/|en-gb\/|es-es\/|es-xl\/|fr-fr\/|it-it\/|ja-jp\/|ko-kr\/|pl-pl\/|pt-br\/|ru-ru\/|tr-tr\/|zh-cn\/|zh-tw\/))$/i.test(cvURL)) {
			if (($(o).parents('[class*="Hero"]').length) || ($(o).parents('[class*="hero"]').length)){
				Destination_LPOS = "Hero";
			}
			else if ($(o).parents('[class*="tile"]').length) {
				$('.homepageTiles>div').each(function(){var link = $(this).find('a'); link.attr("title", "Tile" + ($(this).index() + 1));})
				if(o.title!="undefined")	Destination_LPOS = o.title;
				else Destination_LPOS = "Tile";
			}
			else if (/shop.*amd.com/i.test(cvURL)) { //shop home
				if ($(o).parents('[class="homeResellers"]').length) {
					Destination_LPOS = "homeResellers";
					// if ($(o).parents('[class*="homeResellerLogo"]').length) {
						// $('.homeResellerLogo').each(function(){var link = $(this).find('a'); link.attr("title", "homeResellerLogo" + ($(this).index() + 1));})
						// if(o.title!="undefined")	Destination_LPOS += "|" +o.title;
						// else Destination_LPOS += "|homeResellerLogo";
					// }
				}
				else if ($(o).parents('[class="homeComputers"]').length) {
					Destination_LPOS = "homeComputers";
				}
				else if ($(o).parents('[class="homeComponents"]').length) {
					Destination_LPOS = "homeComponents";
				}
			}
		}
		else if (parseUri(cvURL).host != "http://www.amd.com/us/as/sp2013/community.amd.com" && $(o).parents('[class*="productDetailHero2"]').length) {
			// $('.productDetailHero2').each(function(){var link = $(this).find('a'); link.attr("title", "Hero2Promo" + ($(this).index() + 1));})
			$('.productDetailHero2').each(function(){var link = $(this).find('a'); link.attr("title", "Hero2Promo" + ($(this).index() - 1));})
			if(o.title!="undefined")	Destination_LPOS = o.title;
			else Destination_LPOS = "Hero2Promo";
		}
		else if (parseUri(cvURL).host != "http://www.amd.com/us/as/sp2013/community.amd.com" && $(o).parents('[class*="gamePromo"]').length) {
			if ($(o).parents('[class="gamePromoOne"]').length) Destination_LPOS = "GamePromo1";
			else if ($(o).parents('[class="gamePromoTwo"]').length) Destination_LPOS = "GamePromo2";
			else if ($(o).parents('[class="gamePromoThree"]').length) Destination_LPOS = "GamePromo3";
			else Destination_LPOS = "GamePromo";
		}
	}

	else if (typeof o.parentNode != 'undefined') {
		if (cvTagName=='area' && cvPTagName=='map') {
			cvLinkType='Link|IMGMAP';
		}
		else if (cvTagName=='div' && unescape(o.className)=='accordiontitle'){
			cvLinkType='Link|AccordionTitle';
		}
		else if (cvTagName=='button') {
			cvLinkType='Link|Button';
		}
	}

	if (!cfIsNullOrWhitespace(cvLinkType)) {
		if (cvLinkType=='Link|A') {
			if (typeof o.href != 'undefined') Destination_URL_wParam = unescape(o.href.toLowerCase());
			cvDestination_URL = Destination_URL_wParam.split("?")[0];
			if (Destination_URL_wParam.indexOf("/node/") != -1) {cvISseamicro = 1;} else {cvISseamicro = 0;} //seamicro drupal link
			Destination_LID = '';
			if (unescape(o.innerHTML) != '') {
				if (Destination_LID=="") {
					if (Destination_URL_wParam.indexOf("http://www.amd.com/us/as/sp2013/cdn.bluestacks.com") != -1) {
						var cvDestination_URL = Destination_URL_wParam.split("?")[0];
						var cvDestination_URL_ext = cfUtility(cvDestination_URL,"ext");
						if (!cfIsNullOrWhitespace(cvDestination_URL_ext) && (cvDownloadExtentions.indexOf(cvDestination_URL_ext) != -1)) {
							Destination_LID = cfUtility(Destination_URL_wParam,"filename");
						}
					} else if (o.className == 'hero_button') {
						Destination_LID = "Btn-hero|"+unescape(o.innerHTML)+"|"+cfUtility(cvDestination_URL,"filename");
						if (typeof(o.name) !== 'undefined') {Destination_LID+="|"+o.name};
					} else if (/(.*)_?button/i.test(o.className)) {
						cvButtonColor=o.className.match(/(.*)_?button/i);
						Destination_LID = "Btn-"+cvButtonColor[1]+"|"+unescape(o.innerHTML);
					} else if ((typeof o.parentNode!='undefined') && (o.parentNode.tagName.toLowerCase()=='div' && /(.*)button.*/i.test(o.parentNode.className))) {
							cvButtonColor=o.parentNode.className.match(/(.*)button.*/i);
							Destination_LID = "Btn-"+cvButtonColor[0]+"|"+unescape(o.innerHTML);
					} else if ((typeof o.parentNode!='undefined') && (o.parentNode.tagName.toLowerCase()=='li' && /tab\d.*/i.test(o.parentNode.className))) {
							Destination_LID = "Tab"+"|"+o.parentNode.className.match(/tab\d.*/i)+"|"+unescape(o.innerHTML);
					} else if ((typeof o.parentNode!='undefined') && (typeof o.parentNode.parentNode!='undefined') &&(typeof o.parentNode.parentNode.className!='undefined') && (o.parentNode.parentNode.className.indexOf("addthis") != -1)) {
									Destination_LID = "AddThis"+"|"+unescape(o.innerHTML);
									Destination_LPOS = "AddThis";
					} else Destination_LID = unescape(o.innerHTML); //gets anchor text
				}
			}
			else if (unescape(o.parentNode.className) != '') { //AR 3.2 to accomodate <DIV><A></A></DIV>
				Destination_LID = (unescape(o.parentNode.className));
			}
			else if (unescape(o.className) != '') { //AR 3.2 to accomodate <DIV><A></A></DIV>
				Destination_LID = (unescape(o.className));
			}
			else { Destination_LID = Destination_URL_wParam; }

			if((Destination_LID=='undefined') && (((typeof s.eVar13!='undefined') && s.eVar13!='') || ((typeof s.prop13!='undefined') && s.prop13!=''))) {
				if ((s.prop13) != '')  Destination_LID = s.prop13;
				if ((s.eVar13) != '')  Destination_LID = s.eVar13;
			}
			if (cvISseamicro == 1) {
				Destination_LID = cfUtility(Destination_URL_wParam,"filename")+"|"+Destination_LID; //prepend with seamicro drupal node ID
			}
			// Destination_Name = unescape(o.name);
			Destination_Name = decodeURIComponent(o.name);
			Destination_ID = unescape(o.id); //AR 3.0 added "ID" attribute for LID and LPOS tracking
			if (cfGetQParam(Destination_URL_wParam,"promo")){
				Destination_LID=Destination_LID+"|promo:"+cfGetQParam(Destination_URL_wParam,"promo");
			}
		} else if (cvLinkType=='Link|IMGMAP') {
			Destination_URL_wParam = unescape(o.href.toLowerCase());
			Destination_Name = unescape(o.href);
			Destination_LID = "IMG|MAP|"+Destination_Name;
		} else if (cvLinkType=='Link|Shop') {
			Destination_URL_wParam = unescape(o.href.toLowerCase());
			Destination_Name = unescape(o.href);
			Destination_LID = "Shop|WTB|"+unescape(o.innerHTML)+"|"+cfUtility(Destination_URL_wParam,"server");
			Destination_LPOS = "Shop|WTB";
		} else if (cvLinkType=='Link|HeaderNavlinks') {
			Destination_URL_wParam = unescape(o.href.toLowerCase());
			Destination_Name = unescape(o.href);
			Destination_LID = "HeaderNavLinks|"+unescape(o.innerHTML);
			Destination_LPOS = "HeaderNavLinks";
		} else if (cvLinkType=='Link|LeftNavlinks') {
			Destination_URL_wParam = unescape(o.href.toLowerCase());
			Destination_Name = unescape(o.href);
			Destination_LID = "LeftNavlinks|"+unescape(o.innerHTML);
			Destination_LPOS = "LeftNavlinks";
		} else if (cvLinkType=='Link|Breadcrumb') {
			Destination_URL_wParam = unescape(o.href.toLowerCase());
			Destination_Name = unescape(o.href);
			Destination_LID = "Breadcrumb|"+unescape(o.innerHTML);
			Destination_LPOS = "Breadcrumb";
		} else if (cvLinkType=='Link|IMG') {
			if (typeof o.href != 'undefined') Destination_URL_wParam = unescape(o.href.toLowerCase());
			cvDestination_URL = Destination_URL_wParam.split("?")[0];
		} else if (cvLinkType=='Link|AccordionTitle') {
			Destination_URL_wParam = "AccordionTitle";
			Destination_LID = unescape(removeHTMLTags(o.innerHTML.toLowerCase()));
			Destination_Name = unescape(removeHTMLTags(o.innerHTML.toLowerCase()));
			Destination_LPOS = "AccordionTitle";
		} else if (cvLinkType=='Link|Button') { //4p server
			Destination_LID = Destination_Name = o.id;
			Destination_URL_wParam = cvURL;
		}

		var Destination_Server = cfUtility(Destination_URL_wParam,"server");
		var Destination_Domain = ","+cfUtility(Destination_URL_wParam,"domain");
		Destination_URL_wParam_Original = Destination_URL_wParam;
		Destination_URL_wParam = cfClean(Destination_URL_wParam);
		Destination_URL = Destination_URL_wParam.split("?")[0];
		var Destination_LPOS; var cvIsThisADownload = 0;
		//AR 3.0 added "ID" attribute for LID and LPOS tracking
		if (!cfIsNullOrWhitespace(Destination_ID) && cfIsNullOrWhitespace(Destination_Name)) {
			Destination_Name = Destination_ID;
		}
		//AR 2.7: Destination_Name validation to track &lid and &lpos in URL parameters if no "name" attribute is defined
		if ((Destination_Name == "undefined" || cfIsNullOrWhitespace(Destination_Name)) && (cvAMDWebProperties.indexOf(Destination_Domain) != -1) && ((Destination_URL_wParam.indexOf("lid=") !=-1) || (Destination_URL_wParam.indexOf("lpos=") !=-1) )) {


			Destination_Name = Destination_URL_wParam;
		}
		var DADapp=0;
		if ((Destination_Name == "undefined" || cfIsNullOrWhitespace(Destination_Name)) && (cvURL.indexOf("appid=dad") != -1)) {
			DADapp=1;
		}
		if (Destination_Name) {
			Destination_Name = Destination_Name.replace("?","&");
			Destination_Name = Destination_Name.replace("&LinkID=","&lid="); //fix for LinkID vs. lid
			Destination_Name = Destination_Name.replace("&linkID=","&lid="); //fix for LinkID vs. lid
			Destination_Name = Destination_Name.replace("&amp;LinkID=","&lid="); //fix for LinkID vs. lid
			Destination_Name = Destination_Name.replace("&amp;linkID=","&lid="); //fix for LinkID vs. lid
			Destination_Name = Destination_Name.replace("#lid=","&lid=");
			Destination_Name = Destination_Name.replace("&amp;lid=","&lid="); //AR 2.3: fix for MOSS lid rewrite issue
			Destination_Name = Destination_Name.replace("&amp;lpos=","&lpos="); //AR 2.3: fix for MOSS lpos rewrite issue
			Destination_Name = Destination_Name.replace("#lpos=","&lpos=");
			Destination_Name_Split = Destination_Name.split("&");
			for (var i = 0; i < Destination_Name_Split.length; i = i + 1) {
				if ((Destination_Name_Split[i].toLowerCase().indexOf("channelid=") == -1) && (Destination_Domain != "http://www.amd.com/us/as/sp2013/,en25.com") && (Destination_Server != "http://www.amd.com/us/as/sp2013/link.global.amd.com")) {
				//AR 3.1 added filter to exclude LID tagging via URL for "channelid=" (salesedge portal issue)
				// added filter to exclude eloqua redirect domain containing lid
					if (Destination_Name_Split[i].toLowerCase().indexOf("lid=") != -1) {
						Destination_LID = Destination_Name_Split[i].substring(4, Destination_Name_Split[i].length); // Extract LID (Link ID) from name="&lid="
						if (o.tagName=='IMG' && o.parentNode.tagName=='A') {
							Destination_LID = Destination_LID + " | " + cfRight(unescape(o.src),97-Destination_LID.length);
						}
					}
					if (Destination_Name_Split[i].toLowerCase().indexOf("lpos=") != -1) Destination_LPOS = Destination_Name_Split[i].substring(5, Destination_Name_Split[i].length);// Extract LPOS (Link Position) from name="&lpos="
				} // close AR 3.1 if == -1
			}
		}

		var cvDownloadFileExt = cfUtility(Destination_URL,"ext"); //AR 2.6: validate Null or Whitespace for URL
		if (!cfIsNullOrWhitespace(cvDownloadFileExt) && (cvNonDownloadExtensions.indexOf(cvDownloadFileExt) == -1)  && (cvDownloadExtentions.indexOf(cvDownloadFileExt) != -1)) {
		//if extension on URL exists AND the extension is NOT in the exclude list AND the extension IS in the
		//download extension list, then set to 1
		//AR 2.5: removed !=0 condition to enable download tracking of exe files.
		//&& cvDownloadExtentions.indexOf(cfUtility(Destination_URL,"ext")) != 0)
		//AR 2.6: false means extension exists; -1 means it exists as download file extension
			cvIsThisADownload = 1;
		}
		else if ((!cfIsNullOrWhitespace(cvElqAssetType)) && (cvDownloadExtentions.indexOf(cvElqAssetType) != -1)) {
			cvIsThisADownload = 1;
			cvDownloadFileExt = cvElqAssetType;
		}
		Destination_LID = removeHTMLTags(Destination_LID);
		/* Custom Link, Exit Link, Download Link */
		if (((cvAMDWebProperties.indexOf(Destination_Domain) != -1) && (cvIsThisADownload != 1)) || (Destination_Domain.indexOf("undefined") != -1)) {
			s.linkType = "o"; // Internal Links
		} else if ((cvAMDWebProperties.indexOf(Destination_Domain) != -1) && (cvIsThisADownload == 1)) {
			s.linkType = "d"; // Download Links
		} else {
			s.linkType = "e"; // External Links
		}
		s.linkName = Destination_LID; s.linkURL = Destination_URL;

		// Non Revenue Partner Referral (NRPPR) BOF
		if (s.linkType=="e") {
			if(Destination_LPOS !="PPRlink" && Destination_LPOS !="RTPPRLink"){
				Destination_CoreDomain= parseURL(Destination_URL_wParam_Original);
				if(Destination_URL_wParam_Original.indexOf("linksynergy") != -1){
					Destination_URL_wParam_Original_Split = Destination_URL_wParam_Original.split(/.*http/);
					// Destination_CoreDomain=parseURL("http"+Destination_URL_wParam_Original_Split[1]);
					Destination_CoreDomain=parseURL(decodeURIComponent("http"+Destination_URL_wParam_Original_Split[1]));
				}

				SourceURL_CoreDomain=parseURL(cvURL);
					if ((cvPartnerExcludeDomains.indexOf(Destination_CoreDomain.domain) == -1)  && ((cvSourceExcludeDomains.indexOf(SourceURL_CoreDomain.host) == -1) || (cvURL.indexOf("support.amd.com\/en-us\/download\/ccc") != -1))){
					var cvNRPPR='';
					if (/sites.amd.com.*server.*(hp|dell|cray|ibm|acer).*/i.test(cvURL)) {
						cvNRPPR="Server|"+cvURL.match(/hp|dell|cray|ibm|acer/i);
					}
					else if (/sites.amd.*business.*north-america-partners.*/i.test(cvURL) || /www.amd.com.*server.*series-processors.*/i.test(cvURL)) {
						cvNRPPR="NAServer|";
					}
					else if (/sites.amd.*business.*desktop.*(lenovo|dell).*/i.test(cvURL)) {
						cvNRPPR="OEM|Desktop|"+cvURL.match(/lenovo|dell/i);
					}
					else if (/sites.amd.*business.*notebooks.*(lenovo|dell|hp).*/i.test(cvURL)) {
						cvNRPPR="OEM|Notebook|"+cvURL.match(/lenovo|dell|hp/i);
					}
					else if (/VISION[|]CoolApps[|]DownloadNow.*/i.test(Destination_LID)) {
						cvAppZoneCategory = cfUtility(cvURL,"filename").split(".");
						cvNRPPR="AppZone|"+"|"+cvAppZoneCategory[0];
					}
					else if (/vision.*cool-apps.*/i.test(cvURL)) {
						cvAppZoneCategory = cfUtility(cvURL,"filename").split(".");
						cvAppZoneRegion = cfUtility(cvURL,'channel').split("/");
						cvNRPPR="AppZone|"+cvAppZoneCategory[0]+"|"+cvAppZoneRegion[2].toUpperCase();
					}
					else if (/sites.amd.com.*game\/games\/pages(.*)/i.test(cvURL) || /www.amd.com.*markets\/game(.*)/i.test(cvURL)) {
						//cvGameTitle = cfUtility(cvURL,"filename").split(".");
						cvURL_fragment = cvURL.split('/');
						cvGameTitle = cvURL_fragment[cvURL_fragment.length - 2];
						cvNRPPR="Game|"+cvGameTitle;
					}
					else if (/hp-envy-iicg/i.test(cvURL)) {
						cvNRPPR="HP-Envy-IICG";
					}
					else if (/if-it-can-game/i.test(cvURL)) {
						cvNRPPR="IICG";
					}
					else if (cvURL.indexOf("products/processors/desktop/a-series-apu") != -1) {
						Destination_LID="Shop|WTB|APU|"+Destination_LID;
						Destination_LPOS = "Shop|WTB";
					}
					else if ((cvURL.indexOf("http://www.amd.com/us/as/sp2013/developer.amd.com") != -1) || (cvURL.indexOf("http://www.amd.com/us/as/sp2013/wwwd.amd.com") != -1)){
						cvNRPPR="Developer";
					}
					else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/community.amd.com") != -1) {
						cvNRPPR="Community";
					}
					else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/subscriptions.amd.com") != -1) {
						cvNRPPR="Subscription-Email";
					}
					else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/experience.amd.com") != -1) {
						cvNRPPR="Experience";
					}
					else if (cvURL.indexOf("promo") != -1) {
						cvNRPPR="Promo";
					}
					else if (cvURL.indexOf("innovations") != -1) {
						cvNRPPR="Innovations";
					}
					else if (cvURL.indexOf("markets") != -1) {
						cvNRPPR="Markets";
					}
					else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/radeonmemory.com") != -1) {
						cvNRPPR="RadeonMemory";
					}
					else if (cvURL.indexOf("graphics/desktop") != -1) {
						cvNRPPR="Graphics|Desktop";
					}
					else if (cvURL.indexOf("graphics/workstation") != -1) {
						cvNRPPR="Graphics|Workstation";
					}
					else if (cvURL.indexOf("processors/desktop") != -1) {
						cvNRPPR="Processors|Desktop";
					}
					else if (cvURL.indexOf("processors/notebook-tablet") != -1) {
						cvNRPPR="Processors|Notebook-Tablet";
					}
					else if (cvURL.indexOf("processors") != -1) {
						cvNRPPR="Processors";
					}
					else if (cvURL.indexOf("products/chipsets") != -1) {
						cvNRPPR="Chipsets";
					}
					else if (cvURL.indexOf("products/embedded") != -1) {
						cvNRPPR="Embedded";
					}
					else if (cvURL.indexOf("products/solid-state-drives") != -1) {
						cvNRPPR="SSD";
					}
					else if (cvURL.indexOf("game") != -1) {
						cvNRPPR="Game";
					}
					else if (cvURL.indexOf("solutions/software-partners") != -1) {
						cvNRPPR="Solutions|Software";
					}
					else if (cvURL.indexOf("solutions/servers") != -1) {
						cvNRPPR="Solutions|Server";
					}
					else if (cvURL.indexOf("server") != -1) {
						cvNRPPR="Server";
					}
					else if (cvURL.indexOf("solutions/tablets") != -1) {
						cvNRPPR="Solutions|Tablets";
					}
					else if (cvURL.indexOf("solutions/workstations") != -1) {
						cvNRPPR="Solutions|Workstations";
					}
					else if (cvURL.indexOf("solutions/pro") != -1) {
						cvNRPPR="Solutions|Pro";
					}
					else if (cvURL.indexOf("solutions/laptops") != -1) {
						cvNRPPR="Solutions|Laptops";
					}
					else if (cvURL.indexOf("solutions/desktops") != -1) {
						cvNRPPR="Solutions|Desktops";
					}
					else if (cvURL.indexOf("partners/distributor-locator") != -1) {
						cvNRPPR="Partners|Distributor";
					}
					else if (cvURL.indexOf("partners/global-oems") != -1) {
						cvNRPPR="Partners|Global OEM";
					}
					else if (cvURL.indexOf("partners/north-america-oems") != -1) {
						cvNRPPR="Partners|NA OEM";
					}
					else if (cvURL.indexOf("partners/reseller-locator") != -1) {
						cvNRPPR="Partners|Reseller";
					}
					else if (cvURL.indexOf("microsoft") != -1) {
						cvNRPPR="Business|Microsoft";
					}
					else if (cvURL.indexOf("business") != -1) {
						cvNRPPR="Business";
					}
					else if (cvURL.indexOf("consumer/where-to-buy") != -1) {
						cvNRPPR="Consumer|WTB";
					}
					else if (cvURL.indexOf("press-releases") != -1) {
						cvNRPPR="Press Release";
					}
					else if (cvURL.indexOf("who-we-are") != -1) {
						cvNRPPR="Who We Are";
					}
					else if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/amd4u.com") != -1) {
						cvNRPPR="AMD4U";
					}
					else if (/\/www\.amd\.com\/(\D\D-\D\D)\//i.test(s.pageName)) {
						cvNRPPR="homepage:"+ s.pageName.match(/\D\D-\D\D/i);
					}
					else if (cvURL.indexOf("support.amd.com\/en-us\/download\/ccc") != -1) {
						cvNRPPR="CCC";
					}
					else {
						cvNRPPR="ERROR";
					}

					if (cvNRPPR && cvNRPPR != '') {
						if (cvPartnerExcludeDestination_LID.indexOf(Destination_LID) == -1) {
							cvNRPPR = "PartnerRef|"+cvNRPPR+"|"+Destination_LID+"|"+Destination_CoreDomain.host;
							s.eVar13 = s.prop13 = Destination_LID = cvNRPPR;
							if (cvNRPPR.indexOf("AppZone") !=-1) {
								s.eVar20 = cfCVAConversion("AZ");
								s.events = s.apl(s.events,"event80",",",1);
								if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.eVar13,type:'appzone',link:o }); } )(); }
							}
							else if(cvNRPPR.indexOf("ERROR") == -1){
								s.eVar20 = cfCVAConversion("NR");
								s.events = s.apl(s.events,"event99",",",1);
								if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.eVar13,type:'nrppr',link:o }); } )(); }
							}
						}
					}
				}
			}
		} // Non Revenue Partner Referral (NRPPR) EOF

		if (ReportSuiteID && ReportSuiteID != '') s.prop25 = ReportSuiteID; // Clicks by Report Suites
		if (s.pageName && s.pageName != '') s.prop12 = s.pageName; // Clicks by Page URL
		if (Destination_LID && Destination_LID != '') s.eVar13 = s.prop13 = Destination_LID; // Clicks by Link ID
		if (Destination_URL && Destination_URL != '') s.prop14 = Destination_URL;  // Clicks by Link URL - No Params
		if (DADapp==1) {s.prop13 = "DAD|" + s.prop13; s.eVar13 = s.prop13; s.prop14 = "DAD|" + s.prop14; Destination_LPOS = "Driver AutoDetect (DAD)";}
		if (Destination_URL_wParam && Destination_URL_wParam != '') s.eVar6 = Destination_URL_wParam; // Clicks by Link Full URL - With Params
		if (cfCookieEnabled()) {
			if (/shop\.amd\.com\/.*k=.*/i.test(Destination_URL_wParam)) { //Shop OSS linked search vs. user search
				cfSetCookie('c_durl',Destination_URL_wParam,1); //durl = destination URL
			}
				cfSetCookie('c_rurl',cvURL,-15); //rurl = current URL as Referring URL; negative expiry value denotes seconds not days
		}

		if (Destination_LPOS && Destination_LPOS != '') {
			Destination_LPOS = removeHTMLTags(Destination_LPOS);
			if ((Destination_LPOS.indexOf("RankRelevancy|") != -1) || (Destination_LPOS.indexOf("Promoted|") != -1)) { //search result rank
				cvSearchResultRank1 = Destination_LPOS.split("|");
				cvSearchResultRank2 = cvSearchResultRank1[1].split("_");
				cvSearchResultRankPosition = parseInt(cvSearchResultRank2[0])+1;
				Destination_LPOS = cvSearchResultRank1[0]+"|"+cvSearchResultRankPosition;
				if (csKeyword != '') {
					s.prop5 = s.eVar5 = csKeyword.toLowerCase(); // Internal Search Term result clicked
				}
				s.events = s.apl(s.events,"event13",",",1); // CTR on Search Results
				//if (cvSPwebtrack==1) {return ewt.track({name:s.eVar5+'|'+Destination_LPOS,type:'oss-resultclick',link:o }); }
				//if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.eVar5+'|'+Destination_LPOS,type:'oss-resultclick',link:o }); } )(); }
				if (s.events.indexOf("event5,event11,") != -1) s.events=s.events.replace("event5,event11,","");
			}
			s.prop15 = s.eVar15 = Destination_LPOS; // Clicks by Link Position
		}


		if(cvURL.indexOf("experience.amd.com/feedback") != -1) {
			if(typeof Destination_LPOS=="undefined") Destination_LPOS="";
			s.prop15 = s.eVar15 = Destination_LPOS="SurveyTY|"+Destination_LPOS;
		}

		if (Destination_LID && Destination_LID != '' && Destination_LID.indexOf("|Complete") != -1) {
			s.eVar20 = cfCVAConversion("FC");
			s.events = s.apl(s.events,"event98",",",1); // form completes
		}
		s.events = s.apl(s.events,"event6",",", 1); // Link Clicks Event
		//if (cvSPwebtrack==1) {return ewt.track({name:Destination_LID+'|'+cvURLCheck,type:'link-click',link:o }); }
		if (cvSPwebtrack==1) {(function(){return ewt.track({name:Destination_LID+'|'+cvURLCheck,type:'link-click',link:o }); } )(); }

		/* Download Tracking */
		if (cvIsThisADownload) {
			cvIsDownloadDomain = 0;
			for (i=0;i < cvDriverDownloadDomains.length;i++) { //AR 2.8 checks if download file belongs to a download domain
				if (Destination_URL.indexOf(cvDriverDownloadDomains[i]) != -1) {
					cvIsDownloadDomain = 1;
				}
			}
			if (cvNonDownloadExtensions.indexOf(cvDownloadFileExt) == -1) { // driver or asset only if extension is not on exlude list
				if (((cvIsDownloadDomain) && (Destination_URL_wParam.indexOf('driver') != -1)) && (cvDriverExtensions.indexOf(cvDownloadFileExt) != -1)) {
					//AR 2.8 assign LID
					Destination_LID = "DD|";
					if (Destination_URL_wParam.indexOf('processor') != -1) {
						Destination_LID += "Processor"; }
					else if (Destination_URL_wParam.indexOf('embedded') != -1) {
						Destination_LID += "Embedded"; }
					else if (Destination_URL_wParam.indexOf('chipsetmotherboard') != -1) {
						Destination_LID += "Chipset"; }
					else if (Destination_URL_wParam.indexOf('fire') != -1) { //firepro, firegl, firemv
						Destination_LID += "GPU-WS"; }
					else {
						Destination_LID += "GPU-Other"; }

					if (Destination_URL_wParam.indexOf('linux') != -1) {
						Destination_LID += "-Linux|"; }
					else {
						Destination_LID += "|"; }

					Destination_LID += cfUtility(Destination_URL,'filename');
					s.linkName = Destination_LID;
					s.eVar13 = s.prop13 = Destination_LID;
					s.eVar20 = cfCVAConversion("DD");
					s.events = s.apl(s.events,"event15",",", 1);	// Driver Downloads Event (event15)
					//if (cvSPwebtrack==1) {return ewt.track({name:Destination_LID,type:'download-driver',link:o }); }
					if (cvSPwebtrack==1) {(function(){return ewt.track({name:Destination_LID,type:'download-driver',link:o }); } )(); }
				} else {
					s.eVar20 = cfCVAConversion("AD");
					s.events = s.apl(s.events,"event4",",", 1);		// Asset Downloads Event (event4)
					s.prop15 = s.eVar15 = Destination_LPOS = "AD|"+cfUtility(Destination_URL,'filename');
					if (((s.prop14.indexOf("the_amd_story") != -1) || (s.prop14.indexOf("the-amd-story") != -1)) && (s.prop14.indexOf(".pdf") != -1)) {
						s.eVar20 = cfCVAConversion("AS");
						s.events = s.apl(s.events,"event32",",", 1);		//AMD Story (event32)
					}
					//if (cvSPwebtrack==1) {return ewt.track({name:'AD|'+Destination_LID,type:'download-asset',link:o }); }
					if (cvSPwebtrack==1) {(function(){return ewt.track({name:'AD|'+Destination_LID,type:'download-asset',link:o }); } )(); }
				}
			}
		}

		// Custom Shop Now Link Tracking on Shop.amd.com SNC
		if (Destination_LPOS == "PPRlink" || Destination_LPOS =="RTPPRLink") {
			Destination_LPOS = "PPRlink";
			s.eVar20 = cfCVAConversion("PR");
			s.linkTrackVars = s.linkTrackVars + ",prop30,prop31,prop32,prop33,eVar20,eVar30,eVar31,eVar32,eVar33,eVar34,products";
			s.linkTrackEvents = s.linkTrackEvents + ",event9,purchase";
			cvShop_LinkID = Destination_LID.split("|");
			s.prop30 = s.prop12;
			s.eVar30 = s.eVar8;
			//s.prop30 = s.eVar30 = cvShop_LinkID[1];   // PropertyID
			s.prop31 = s.eVar31 = cvShop_LinkID[2];   // RetailerID
			if ((typeof document.getElementsByName("shopUniqueID")[0] != "undefined") && (typeof document.getElementsByName("shopUniqueID")[0].value != "undefined")) {
				s.prop32 = s.eVar32 = document.getElementsByName("shopUniqueID")[0].value;   // ProductID
			} else {
				s.prop32 = s.eVar32 = cvShop_LinkID[3];   // ProductID
			}
			s.prop33 = s.eVar33 = cvShop_LinkID[4].replace(',','.');   // Price
			//s.currencyCode = "\"" + cvShop_LinkID[5] + "\"";
			//s.eVar34 = cvShop_LinkID[0] + "|" + s.prop30 + "|" + s.prop31 + "|" + s.prop32;
			s.eVar34 = cvShop_LinkID[0] + "|" + cvShop_LinkID[1] + "|" + s.prop31 + "|" + s.prop32;
			s.eVar13 = s.prop13 = s.eVar34+"|"+s.prop33+"|"+cvShop_LinkID[5];
			s.events = s.apl(s.events,"event9",",", 1);   // Shop Now Event

			// s.Products custom coding
			s.events = s.apl(s.events,"purchase",",", 1);   // Shop Now Event
			//s.products = ";" + cvShop_LinkID[3] + ";1;" + cvShop_LinkID[4].replace(',','.');
			s.products = ";" + s.prop32 + ";1;" + cvShop_LinkID[4].replace(',','.');
			if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.eVar34+"|"+s.eVar33,type:'shop-now',link:o }); } )(); }

			// VWO tracking BOF
			// if(typeof(_vis_opt_top_initialize) == "function") {
				// _vis_opt_goal_conversion(203); // Code for Custom Goal: PPR Click
				// _vis_opt_pause(500);
			// }
		}

		//review tracking
		if (Destination_URL_wParam.indexOf("reviews") != -1) {
			if (typeof ctProdView != "undefined" && ctProdView != "" && ctProdView != "undefined") {
				s.eVar32 = "Review|"+ ctProdView;
			} else {
				s.eVar32 = "Review|"+ cfUtility(Destination_URL_wParam,'filename');
			}
			s.events = s.apl(s.events,"event60",",", 1);   // View Reviews Event
			//if (cvSPwebtrack==1) {return ewt.track({name:s.eVar32,type:'shop-review',link:o }); }
			if (cvSPwebtrack==1) {(function(){return ewt.track({name:s.eVar32,type:'shop-review',link:o }); } )(); }
		}
	}

	//nFusion Marin/Sizmek tracking BOF
			// Marin codes
			// s.events = s.apl(s.events,"event61",",", 1); s.eVar20 = cfCVAConversion("MS"); //shop
			// s.events = s.apl(s.events,"event62",",", 1); s.eVar20 = cfCVAConversion("MC"); //contact
			// s.events = s.apl(s.events,"event63",",", 1);  s.eVar20 = cfCVAConversion("MI"); //information

	//if(typeof s.eVar47 != 'undefined') {  //only track Marin events if mkwid exists
		if(1==0){}//blank if
		else if (cvURL.indexOf("aw-cl-global-11111119") != -1) { //06-27-2014|12-31-2014
			if (/shop\.amd\.com\/.*AD785KXBJABOX/i.test(Destination_URL_wParam)) {
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (/shop\.amd\.com\/.*R9290XENFC/i.test(Destination_URL_wParam)) {
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (/.*hp.*va_r10104_go_eliteamd/i.test(Destination_URL_wParam)) {
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (/.*338868/i.test(Destination_URL_wParam)) { //http://click.linksynergy.com/fs-bin/click?id=kmZjHBYatgE&subid=&offerid=338868.1&type=10&tmpid=13127&u1=IICGLP&RD_PARM1=http://www.bestbuy.com/site/hp-envy-touchsmart-15-6-touch-screen-laptop-amd-a10-series-6gb-memory-750gb-hard-drive-natural-silver/5342009.p?id%3D1219120209729%2526skuId%3D5342009%2526st%3D5342009%2526cp%3D1%2526lp%3D1%2526ref%3D186%2526loc%3D28
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (/.*342314/i.test(Destination_URL_wParam)) { //http://click.linksynergy.com/fs-bin/click?id=kmZjHBYatgE&subid=&offerid=342314.1&type=10&tmpid=13127&u1=IICGLP&RD_PARM1=http%253A%252F%252Fwww.bestbuy.com%252Fsite%252Fasus-essentio-desktop-12gb-memory-2tb-hard-drive%252F5818001.p%253Fid%253D1219159703994%2526skuId%253D5818001%2526st%253D5818001%2526cp%253D1%2526lp%253D1%2526ref%253D186%2526loc%253D42
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (/.*462529/i.test(Destination_URL_wParam)) { //http://www.officedepot.com/a/products/462529/HP-Pavilion-Convertible-Laptop-Computer-With/
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (/.*223073/i.test(Destination_URL_wParam)) { //http://linksynergy.walmart.com/fs-bin/click?id=kmZjHBYatgE&subid=&offerid=223073.1&type=10&tmpid=1082&u1=IICGLP&RD_PARM1=http://www.walmart.com/ip/HP-Ash-Silver-13.3-Pavilion-X360-13-a019wm-Laptop-PC-with-AMD-Quad-Core-A6-6310-Processor-4GB-Memory-Touchscreen-500GB-Hard-Drive-and-Windows-8.1/36561196
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			// else if (Destination_LID.indexOf("Shop Now") != -1) {
				// s.events = s.apl(s.events,"event61",",", 1);
				// s.eVar20 = cfCVAConversion("MS");
			// }
			else if (Destination_LID.indexOf("Btn-orange|See what else the AMD APU Can do") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			// one entry in scode_yt.js
		}
		else if((cvURL.indexOf("en-us/markets/if-it-can-game") != -1)  || (cvURL.indexOf("hp-envy-iicg") != -1)){
			if (/(looking to buy|shop|partnerref)/i.test(Destination_LID)) {
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (/(Check my system now)/i.test(Destination_LID)) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			else if (/(please tell us how)/i.test(Destination_LID)) {
				s.events = s.apl(s.events,"event62",",", 1);
				s.eVar20 = cfCVAConversion("MC");
			}
		}
		else if(cvURL.indexOf("innovations/software-technologies/eyefinity") != -1){
			if (/(check out amd radeon.*r9 series gpus)/i.test(Destination_LID)) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
		}
		else if (/solutions\/(desktops|laptops)/i.test(cvURL)) {
			if (/(where to buy)/i.test(Destination_LID)) {
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (/(contact us)/i.test(Destination_LID)) {
				s.events = s.apl(s.events,"event62",",", 1);
				s.eVar20 = cfCVAConversion("MC");
			}
			else if (/(get details about the hp)/i.test(Destination_LID)) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
		}
		else if (/www.amd.com\/en-us\/solutions\/pro(|\/)$/i.test(cvURLCheck)) { //09-02-2014|11-23-2014
			if (Destination_LID.indexOf("Btn-orange_|More on PRO Performance") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			else if (Destination_LID.indexOf("Btn-orange_|More on PRO security, manageability, and more") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			else if (Destination_URL_wParam.indexOf("/www.amd.com/en-us/solutions/pro/security-manageability") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			else if (Destination_LID.indexOf("PartnerRef|Solutions|Pro|More Details|hp") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			else if (Destination_LID.indexOf("Form|SP|Commercial-News-Solutions-Pro|Complete") != -1) {
				//cfFormPassed
			}
		}
		else if(cvURL.indexOf("en-gb/markets/if-it-can-game") != -1){
			if (Destination_LID.indexOf("Shop JLP") != -1){
				s.events = s.apl(s.events,"event61",",", 1);
				s.eVar20 = cfCVAConversion("MS");
			}
			else if (Destination_LID.indexOf("See the trailer") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			else if (Destination_URL_wParam.indexOf("/gesture-control") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			else if (Destination_URL_wParam.indexOf("/wireless-display") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
			else if (Destination_URL_wParam.indexOf("face-login") != -1) {
				s.events = s.apl(s.events,"event63",",", 1);
				s.eVar20 = cfCVAConversion("MI");
			}
		}

	//nFusion Marin/Sizmek tracking EOF

	if(Destination_LID!=tmpDestination_LID) {
		cfMediaMindClickTrack(Destination_LID, Destination_URL_wParam);
		cfFloodLightClickTrack(Destination_LID, Destination_URL_wParam);
		tmpDestination_LID=Destination_LID;
	}

	if (Destination_LID != "") {
		clearTimeout(cvTimedReading);
	}
}

//Google Analytics DriverInstalled
	if (cvURL.indexOf("driverinstalled") != -1) {
	//http://www.amd.com/di?vid1=v1111&vid2=v2222&vid3=v3333&vid4=v4444&did1=d1111&did2=d2222&did3=d3333&did4=d4444&ssvid1=ssv1111&ssvid2=ssv2222&ssvid3=ssv3333&ssvid4=ssv4444&ssid1=ss1111&ssid2=ss2222&ssid3=ss3333&ssid4=ss4444&pid1=p1111&pid2=p2222&pid3=p3333&pid4=p4444
	//https://secure.amd.com/di?vid1=v1RadeonÃƒÆ'Ã†'Ãƒâ€šÃ‚Â¢"ÃƒÆ'Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ HD 1111*&vid2=v2RadeonÃƒÆ'Ã†'Ãƒâ€šÃ‚Â¢"ÃƒÆ'Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ HD 2222*&vid3=v3RadeonÃƒÆ'Ã†'Ãƒâ€šÃ‚Â¢"ÃƒÆ'Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ HD 3333*&vid4=v4RadeonÃƒÆ'Ã†'Ãƒâ€šÃ‚Â¢"ÃƒÆ'Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ HD 4444*&did1=d1111&did2=d2222&did3=d3333&did4=d4444&ssvid1=ssv1111&ssvid2=ssv2222&ssvid3=ssv3333&ssvid4=ssv4444&ssid1=ss1111&ssid2=ss2222&ssid3=ss3333&ssid4=ss4444&pid1=p1111&pid2=p2222&pid3=p3333&pid4=p4444
		cvDILang = cvLang || "";
		cvDILangURLwww = "en-us"; //set default
		cvDILangURL = "en"; //set default

		if (cvDILang != "") {
			if (cvDILang.indexOf("en") !=-1) {cvDILangURL="en"; cvDILangURLwww="en-us";}
			else if (cvDILang.indexOf("de") !=-1) {cvDILangURL="de"; cvDILangURLwww="de-de";}
			else if (cvDILang.indexOf("es") !=-1) {cvDILangURL="es"; cvDILangURLwww="es-es";}
			else if (cvDILang.indexOf("fr") !=-1) {cvDILangURL="fr"; cvDILangURLwww="fr-fr";}
			else if (cvDILang.indexOf("it") !=-1) {cvDILangURL="it"; cvDILangURLwww="it-it";}
			else if (cvDILang.indexOf("pl") !=-1) {cvDILangURL="pl"; cvDILangURLwww="pl-pl";}
			else if (cvDILang.indexOf("pt") !=-1) {cvDILangURL="br"; cvDILangURLwww="pt-br";}
			else if (cvDILang.indexOf("ru") !=-1) {cvDILangURL="ru"; cvDILangURLwww="ru-ru";}
		//	else if (cvDILang.indexOf("sv") !=-1) cvDILangURL="se";
			else if (cvDILang.indexOf("tr") !=-1) {cvDILangURL="tr"; cvDILangURLwww="tr-tr";}
			else if (cvDILang.indexOf("cn") !=-1) {cvDILangURL="cn"; cvDILangURLwww="zh-cn";}
			else if (cvDILang.indexOf("tw") !=-1) {cvDILangURL="tw"; cvDILangURLwww="zh-tw";}
			else if (cvDILang.indexOf("ko") !=-1) {cvDILangURL="kr"; cvDILangURLwww="ko-kr";}
			else if (cvDILang.indexOf("ja") !=-1) {cvDILangURL="jp"; cvDILangURLwww="ja-jp";}
		}
		//cvDILangURLwww = (cvDILangURL=="en") ? "en-us" : cvDILangURL;
		cvDILangURLwww="en-us"; //temp fix for missing languages. remove this line to go back to languages.

		if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/driverinstalled/index.html") != -1) { // cvDI = Driver Installed
			//s.eVar20 = cfCVAConversion("DI"); //add DI to CVA
			sAA.events += ",event55"; // Driver Installed (DI) Event
			cvEloquaUID = cvDIRirectURL = "";
			cvDIRegistered = "no";
			cvURL=cvURL.toLowerCase();
			var cvParamsV = [], cvParamsD = [], cvParamsSSV = [], cvParamsSS = [], cvParamsP = [];
			cvDIparams = cfUtility(cvURL, 'parametersonly') || "";
			cvDIparams=cvDIparams.toLowerCase();
			cvDIparams=cvDIparams.replace(/[^.\sa-zA-Z0-9&?=,_-]/g,'');

			cvParamsV[0] = "vid1=" + (parseUri(cvURL).queryKey["vid1"] || "");
			cvParamsV[1] = "vid2=" + (parseUri(cvURL).queryKey["vid2"] || "");
			cvParamsV[2] = "vid3=" + (parseUri(cvURL).queryKey["vid3"] || "");
			cvParamsV[3] = "vid4=" + (parseUri(cvURL).queryKey["vid4"] || "");
			cvParamsP[0] = "pid1=" + (parseUri(cvURL).queryKey["pid1"] || "");
			cvParamsP[1] = "pid2=" + (parseUri(cvURL).queryKey["pid2"] || "");
			cvParamsP[2] = "pid3=" + (parseUri(cvURL).queryKey["pid3"] || "");
			cvParamsP[3] = "pid4=" + (parseUri(cvURL).queryKey["pid4"] || "");
			cvParamsD[0] = "did1=" + (parseUri(cvURL).queryKey["did1"] || "");
			cvParamsD[1] = "did2=" + (parseUri(cvURL).queryKey["did2"] || "");
			cvParamsD[2] = "did3=" + (parseUri(cvURL).queryKey["did3"] || "");
			cvParamsD[3] = "did4=" + (parseUri(cvURL).queryKey["did4"] || "");
			cvParamsSSV[0] = "ssvid1=" + (parseUri(cvURL).queryKey["ssvid1"] || "");
			cvParamsSSV[1] = "ssvid2=" + (parseUri(cvURL).queryKey["ssvid2"] || "");
			cvParamsSSV[2] = "ssvid3=" + (parseUri(cvURL).queryKey["ssvid3"] || "");
			cvParamsSSV[3] = "ssvid4=" + (parseUri(cvURL).queryKey["ssvid4"] || "");
			cvParamsSS[0] = "ssid1=" + (parseUri(cvURL).queryKey["ssid1"] || "");
			cvParamsSS[1] = "ssid2=" + (parseUri(cvURL).queryKey["ssid2"] || "");
			cvParamsSS[2] = "ssid3=" + (parseUri(cvURL).queryKey["ssid3"] || "");
			cvParamsSS[3] = "ssid4=" + (parseUri(cvURL).queryKey["ssid4"] || "");

			sAA.eVar17=cvParamsV.join(",");
			sAA.eVar18=cvParamsP.join(",");
			sAA.eVar19=cvParamsD.join(",");
			sAA.eVar21=cvParamsSSV.join(",");
			sAA.eVar22=cvParamsSS.join(",");
			
			sAA.eVar17a=cvParamsV.join("|");
			sAA.eVar18a=cvParamsP.join("|");
			sAA.eVar19a=cvParamsD.join("|");
			sAA.eVar21a=cvParamsSSV.join("|");
			sAA.eVar22a=cvParamsSS.join("|");
			

			if (cfCookieEnabled()) {
				cvDI = cfGetCookie('c_scdi') || ""; //scdi = SiteCatalyst Driver Installed

				if (cvDI != "") { //cookie exists
					cvDISplit = cvDI.split(',');

					var i,cvElement,cvElementValue;
					for (i=0;i<cvDISplit.length;i++)
					{
						cvElement=cvDISplit[i].substr(0,cvDISplit[i].indexOf("="));
						cvElementValue=cvDISplit[i].substr(cvDISplit[i].indexOf("=")+1) || "";
						cvElement=cvElement.replace(/^\s+|\s+$/g,"");
						if(cvElement=="reg") cvDIRegistered=cvElementValue;
					}
				}
			}

			cvDICookieValue = "reg="+cvDIRegistered+",lang="+cvDILang+","+s.eVar17+","+s.eVar18+","+s.eVar19+","+s.eVar21+","+s.eVar22;
			cvDICookieValue=cvDICookieValue.replace(/[^.\sa-zA-Z0-9=,_-]/g,'');
			if (cvDIparams.indexOf("?") != -1) {
				cvDIparams+="&lang="+cvDILang;
			} else {
				cvDIparams+="?lang="+cvDILang;
			}

			if(cvDIRegistered=="yes") {
				cvDIRedirectURL="http://www.amd.com/"+cvDILangURLwww+"/who-we-are/subscriptions/catalyst-software-installed"; //"http://www.amd.com/products/technologies/amd-catalyst/Pages/catalyst-software-installed.aspx";


			} else {
				cvDIRedirectURL="http://experience.amd.com/driver-installed/"+cvDILangURL;
			}
			if (!cfCookieEnabled()) {cvDIRedirectURL+=cvDIparams;}
			else {cfSetCookie('c_scdi',cvDICookieValue,365);}
			dataLayer.push({
	'VID':sAA.eVar17a||'',
	'PID':sAA.eVar18a||'',
	'DID':sAA.eVar19a||'',
	'SSVID':sAA.eVar21a||'',
	'SSID':sAA.eVar22a||'',
    'event' : 'DriverInstalled',
    'eventCallback' : function() {
        window.location.href=cvDIRedirectURL;
    }
});
			//window.location.href=cvDIRedirectURL;
		}
		if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/driverinstalled/thankyou.html") != -1) { //opt-in thank you (http://subscriptions.amd.com/driverinstalled/thankyou.html)
			cvEloquaUID = cvDISplit = ""
			cvDIRegistered = "yes";
			cvDIparams = cfUtility(cvURL, 'parametersonly') || "";
			//cvDIRedirectURL="http://www.amd.com/"+cvDILangURLwww+"/aboutamd/subscriptions/Pages/thankyou.aspx"+cvDIparams;
			cvDIRedirectURL="http://www.amd.com/"+cvDILangURLwww+"/who-we-are/subscriptions/thank-you"+cvDIparams;
			cvDI = cfGetCookie('c_scdi') || ""; //scdi = SiteCatalyst Driver Installed

			if (cvDI != "") { //cookie exists
				cvDISplit = cvDI.split(',');
				cvDISplit.shift(); //drop reg value
			}
			cvDICookieValue = "reg=yes,"+cvDISplit;
			cfSetCookie('c_scdi',cvDICookieValue,365);
			dataLayer.push({
	'VID':sAA.eVar17a||'',
	'PID':sAA.eVar18a||'',
	'DID':sAA.eVar19a||'',
	'SSVID':sAA.eVar21a||'',
	'SSID':sAA.eVar22a||'',
    'event' : 'DriverInstalled',
    'eventCallback' : function() {
        window.location.href=cvDIRedirectURL;
    }
});
		}

	}
	
//Google Analytics Compatibility Checker
//https://secure.amd.com/acc
//http://www.amd.com/us/as/cchecker/index.html
	if (cvURL.indexOf("cchecker/") != -1) {
		if(parseUri(cvURL).queryKey["lang"]) {
			cvCCLang = parseUri(cvURL).queryKey["lang"];
		} else cvCCLang = cvLang || "";
		cvCCLangURL = "us"; //set default

		if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/cchecker/index.html") != -1) { // cvCC = Compatibility Checker
			cvEloquaUID = cvCCRirectURL = "";
			cvCCRegistered = "no";
			cvURL=cvURL.toLowerCase();
			cvCCparams = parseUri(cvURL).query || "";
			cvCCparams=cvCCparams.toLowerCase();
			cvCCparams=cvCCparams.replace(/[^.\sa-zA-Z0-9&?=,_-]/g,'');
			cvCCGC = (parseUri(cvURL).queryKey["gesturecontrol"] || 0);
			cvCCFL = (parseUri(cvURL).queryKey["facelogin"] || 0);
			cvCCAZ = (parseUri(cvURL).queryKey["appzone"] || 0);
			cvCCSM = (parseUri(cvURL).queryKey["screenmirror"] || 0);
			cvCCCPU = (parseUri(cvURL).queryKey["cpu"] || "");
			cvCCNordic = (parseUri(cvURL).queryKey["nordic"] || 0);

			if (cfCookieEnabled()) {
				cvCC = cfGetCookie('c_sccc') || ""; //sccc = SiteCatalyst Compatibility Checker

				if (cvCC != "") { //cookie exists
					cvCCSplit = cvCC.split(',');

					var i,cvElement,cvElementValue;
					for (i=0;i<cvCCSplit.length;i++)
					{
						cvElement=cvCCSplit[i].substr(0,cvCCSplit[i].indexOf("="));
						cvElementValue=cvCCSplit[i].substr(cvCCSplit[i].indexOf("=")+1) || "";
						cvElement=cvElement.replace(/^\s+|\s+$/g,""); //clean leading and trailing spaces
						if(cvElement=="reg") cvCCRegistered=cvElementValue;
						if(cvElement=="gc" & cvElementValue==1) cvCCGC=1;
						if(cvElement=="fl" & cvElementValue==1) cvCCFL=1;
						if(cvElement=="az" & cvElementValue==1) cvCCAZ=1;
						if(cvElement=="sm" & cvElementValue==1) cvCCSM=1;
						if(cvElement=="cpu" & cvCCCPU=="" & cvElementValue!="") cvCCCPU=cvElementValue; //if cookie has value, don't overwrite with blank

					}
				}
			}

			cvCCCookieValue = "reg="+cvCCRegistered+",lang="+cvCCLang+",cpu="+cvCCCPU+",gc="+cvCCGC+",fl="+cvCCFL+",az="+cvCCAZ+",sm="+cvCCSM;
			cvCCCookieValue=cvCCCookieValue.replace(/[^.\sa-zA-Z0-9=,_-]/g,'');
			if (cvCCparams.indexOf("?") != -1) {
				cvCCparams+="&lang="+cvCCLang;
			} else {
				cvCCparams+="?lang="+cvCCLang;
			}
			if(cvCCRegistered=="yes") {
				cvCCRedirectURL="http://experience.amd.com/compatibility-checker-installed/en/";
			} else {
				cvCCLangURL = cvCCLangURL.toUpperCase();
				if (cvCCLangURL == "US") cvCCLangURL="";
				cvCCRedirectURL="http://experience.amd.com/compatibility-checker-installed/en/";
			}

			//Nordic cchecker
			if (cvCCNordic==1){
				if ((cvCCLang.indexOf("no") !=-1) || (cvCCLang.indexOf("nn") !=-1)) cvCCRedirectURL="http://experience.amd.com/compatibility-checker-installed/nordic/no/";
				else if (cvCCLang.indexOf("da") !=-1) cvCCRedirectURL="http://experience.amd.com/compatibility-checker-installed/nordic/dk/";
				else if (cvCCLang.indexOf("sv") !=-1) cvCCRedirectURL="http://experience.amd.com/compatibility-checker-installed/nordic/se/";
				else if (cvCCLang.indexOf("fi") !=-1) cvCCRedirectURL="http://experience.amd.com/compatibility-checker-installed/nordic/fi/";
				else cvCCRedirectURL="http://experience.amd.com/compatibility-checker-installed/nordic/en/";
			}

			if (!cfCookieEnabled()) {cvCCRedirectURL+=cvCCparams;}
			else {cfSetCookie('c_sccc',cvCCCookieValue,365);}

			window.location.href=cvCCRedirectURL;
		}
		if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/cchecker/thankyou.html") != -1) { //opt-in thank you (http://subscriptions.amd.com/cchecker/thankyou.html)
			cvEloquaUID = cvCCSplit = ""
			cvCCRegistered = "yes";
			cvCCparams = cfUtility(cvURL, 'parametersonly') || "";
			cvCCRedirectURL="../../../en-us.htm"/*tpa=http://www.amd.com/*/;
			cvCC = cfGetCookie('c_sccc') || ""; //sccc = SiteCatalyst Compatibility Checker

			if (cvCC != "") { //cookie exists
				cvCCSplit = cvCC.split(',');
				cvCCSplit.shift(); //drop reg value
			}
			cvCCCookieValue = "reg=yes,"+cvCCSplit;
			cfSetCookie('c_sccc',cvCCCookieValue,365);
			window.location.href=cvCCRedirectURL;
		}
	}

	

function cfTimedReading(){ //Content Consumption
if (cvURL.substring(0, 5) != "https") {
	s.account = ReportSuiteID;

	s.prop12 = s.pageName;
	s.prop25 = ReportSuiteID;
	if(cvDNTDomains.indexOf(cfUtility(cvURL,'server')) == -1) {
		s.visitorNamespace="amd";
		s.trackingServer="http://www.amd.com/us/as/sp2013/metrics.amd.com";
		s.trackingServerSecure="http://www.amd.com/us/as/sp2013/smetrics.amd.com";
	}
	//s.eVar20 = cfCVAConversion("CC");
	s.events = s.linkTrackEvents = 'event33';
	cvSTL = 1;
	s.tl(this,'o',cvURL);
	clearTimeout(cvTimedReading);
	s.events = s.linkTrackEvents = ''; cvSTL = 0;
	//if (cvSPwebtrack==1) {return ewt.track({name:'CC|'+s.prop12,type:'content-consumed',link:o }); }
	if (cvSPwebtrack==1) {(function(){return ewt.track({name:'CC|'+s.prop12,type:'content-consumed',link:o }); } )(); }
}
}

function cvSendOption(id,selectedid){
if (cvURL.substring(0, 5) != "https") {
	var cvSelectedID = selectedid;
	var cvSelectID = id;
	Destination_LID = s.prop13=s.eVar13=cvSelectID + "|" + cvSelectedID;
	Destination_URL_wParam = s.prop14 = s.prop12 = s.eVar6 = cvURL;
	s.events = s.linkTrackEvents = "event6";
	//s.prop25="amdvother";
	s.prop25=ReportSuiteID;
	if(cvDNTDomains.indexOf(cfUtility(cvURL,'server')) == -1) {
		s.visitorNamespace="amd";
		s.trackingServer="http://www.amd.com/us/as/sp2013/metrics.amd.com";
		s.trackingServerSecure="http://www.amd.com/us/as/sp2013/smetrics.amd.com";
	}
	cvSTL = 1;
	s.tl(this,'o',cvURL);
	s.events = s.linkTrackEvents = ''; cvSTL = 0;
}
}

//migrate as variable if needed
function cfPageName(cvURL,cvParamInPageName,cvParamToInclude) {
	// if cvParamInPageName = 1, it will include parameters in the s.pageName
	if (cvParamInPageName == 1) {
	    if (cvParamToInclude != "") {
	        var cvParam_Modified = "?";
	        cvParam_Split = cvParamToInclude.toLowerCase().split(",");
	        for (var i = 0; i < cvParam_Split.length; i = i + 1) {
				if (cfGetQParam(cvURL,cvParam_Split[i])) cvParam_Modified = cvParam_Modified + cvParam_Split[i] + "=" + cfGetQParam(cvURL,cvParam_Split[i]) + "&";
	        }
	        cvParam_Modified = cvParam_Modified.substring(0, cvParam_Modified.length - 1);
	        var cvPageName = cfUtility(cvURL,'channel') + cfUtility(cvURL,'filename') + cvParam_Modified;
	    } else {
	        var cvPageName = cfUtility(cvURL,'channel') + cfUtility(cvURL,'filenameparameters');
	    }
	} else {
	    var cvPageName = cfUtility(cvURL,'channel') + cfUtility(cvURL,'filename');
	}
	/* ----- IF PageName is Over 100 Chars ----- */
	if (cvPageName.length > 100) cvPageName = "/" + cfUtility(cvURL,'server') + "/..." + cfRight(cvPageName.replace(cfUtility(cvURL,'server') + "/", ""), 100 - (cfUtility(cvURL,'server').length + 5));
	return cvPageName;
}

function cfGetSubstringIndex(str, substring, n) {
    var times = 0, index = null;

    while (times < n && index !== -1) {
        index = str.indexOf(substring, index+1);
        times++;
    }

    return index;
}

function cfUtility(cvURL,cvAction) {
//	cvURL = decodeURI(cvURL);
//	cvURL = unescape(cvURL.toLowerCase());
	switch (cvAction) {
	case "server":	// Example: sites.amd.com, www.hp.com
		var a = cvURL.split(/\/+/g)[1];
		if (typeof(a) !== 'undefined') {
			var b = a.split("."); if (b.length == 2) { var c = 'www.' + a; } else { c = a; } TheResult = c;
		} else {
			TheResult = "";
		}
		break;
	case "domain":	// Example: amd.com, hp.com
		var a = cfUtility(cvURL,"server")
		if (typeof(a) !== 'undefined') {
			var b  = a.split("."); b_len = b.length;
			if (b_len == 4) {
				var TheResult = String(b[b_len - 3] + '.' + b[b_len - 2] + '.' + b[b_len - 1]);
			} else {
				var TheResult = String(b[b_len - 2] + '.' + b[b_len - 1]);
			}
		} else {
			TheResult = "";
		}
		break;
	case "coredomain":	// Example: amd, hp
		var a = cfUtility(cvURL,"domain") //amd.com, hp.com
		if (typeof(a) !== 'undefined') {
			var b  = a.split(".");
			var TheResult=b[0];
		} else {
			TheResult = "";
		}
		break;
	case "channel":
		var a = cvURL.split("?")[0]; a = a.replace("http://","/"); a = a.replace("https://","/");
		var b = a.substring(a.lastIndexOf('/')+1);
		a = a.substring(0,a.length-b.length);
		a = a.replace(cvURL.split(/\/+/g)[1],cfUtility(cvURL,"server"));
		TheResult = a;
		break;
	case "filename":
		var a = cvURL.split("?")[0]; var b = a.substring(a.lastIndexOf('/')+1);
		TheResult = b;
		break;
	case "filenameparameters":
		var cvParamPos = cvURL.indexOf("?"); if (cvParamPos != -1) { var cvParam = cvURL.substring(cvParamPos); } else { var cvParam = ""; }
		TheResult = cfUtility(cvURL,"filename") + cvParam;
		break;
	case "parametersonly":
		var cvParamPos = cvURL.indexOf("?"); if (cvParamPos != -1) { var cvParam = cvURL.substring(cvParamPos); } else { var cvParam = ""; }
		TheResult = cvParam;
		break;
	case "se":
		var cvReferrer_Server = cvURL.split(/\/+/g)[1]; // www.google.ca, www.yahoo.com
		var cvReferrer_Server_Splitted = cvReferrer_Server.split("."); // google.ca, yahoo.ca
		cvReferrer_Server_Splitted_Length = cvReferrer_Server_Splitted.length;
		var TheResult = String('.' + cvReferrer_Server_Splitted[cvReferrer_Server_Splitted_Length - 2] + '.');
		break;
	case "ext":
		var TheResult = cvURL.substring(cvURL.lastIndexOf('/') + 1, cvURL.length).substring(cvURL.substring(cvURL.lastIndexOf('/') + 1, cvURL.length).lastIndexOf('.') + 1, cvURL.substring(cvURL.lastIndexOf('/') + 1, cvURL.length).length);
		break;
	default:
		var TheResult = "";
	}
	return TheResult;
}

//nFusion MediaMind (MM)/Sizmek BOF
	// Marin codes
	// s.events = s.apl(s.events,"event61",",", 1); s.eVar20 = cfCVAConversion("MS"); //shop
	// s.events = s.apl(s.events,"event62",",", 1); s.eVar20 = cfCVAConversion("MC"); //contact
	// s.events = s.apl(s.events,"event63",",", 1);  s.eVar20 = cfCVAConversion("MI"); //information

function cfMediaMindClickTrack(cvDestination_LID, cvDestination_URL_wParam) {
	if (cvURLCheck.indexOf("experience.amd.com/hp/hp-envy-iicg") != -1) { //03-14-2014|05-04-2014
		if (cvDestination_LID.indexOf("if-it-can-game-battlefield-4-command-imagine.png|Battlefield 4 Graphic") != -1) {
			mmConversionTag(466714, this);
		}
		else if (cvDestination_LID.indexOf("consumerclient/hpenvy-500px_ani.gif|HP Envy TouchSmart 15 Sleekbook") != -1) {
			mmConversionTag(466715, this);
		}
		else if (cvDestination_LID.indexOf("PartnerRef|bestbuy.com|Btn-orange|Shop now") != -1) {
			mmConversionTag(466716, this);
		}
		else if (cvDestination_LID.indexOf("IMG|ALT|/subscriptions.amd.com/images/logo/logo_bestbuy_125x90.jpg|Bestbuy") != -1) {
			mmConversionTag(466717, this);
		}
	}
	if (cvURLCheck.indexOf("aw-cl-global-11111119") != -1) { //06-27-2014|12-31-2014
		if (/shop\.amd\.com\/.*AD785KXBJABOX/i.test(cvDestination_URL_wParam)) {
			mmConversionTag(512774, this);
		}
		else if (/shop\.amd\.com\/.*R9290XENFC/i.test(cvDestination_URL_wParam)) {
			mmConversionTag(512775, this);
		}
		else if (/.*hp.*va_r10104_go_eliteamd/i.test(cvDestination_URL_wParam)) {
			mmConversionTag(512776, this);
		}
		else if (/.*338868/i.test(cvDestination_URL_wParam)) { //http://click.linksynergy.com/fs-bin/click?id=kmZjHBYatgE&subid=&offerid=338868.1&type=10&tmpid=13127&u1=IICGLP&RD_PARM1=http://www.bestbuy.com/site/hp-envy-touchsmart-15-6-touch-screen-laptop-amd-a10-series-6gb-memory-750gb-hard-drive-natural-silver/5342009.p?id%3D1219120209729%2526skuId%3D5342009%2526st%3D5342009%2526cp%3D1%2526lp%3D1%2526ref%3D186%2526loc%3D28
			mmConversionTag(498792, this);
		}
		else if (/.*342314/i.test(cvDestination_URL_wParam)) { //http://click.linksynergy.com/fs-bin/click?id=kmZjHBYatgE&subid=&offerid=342314.1&type=10&tmpid=13127&u1=IICGLP&RD_PARM1=http%253A%252F%252Fwww.bestbuy.com%252Fsite%252Fasus-essentio-desktop-12gb-memory-2tb-hard-drive%252F5818001.p%253Fid%253D1219159703994%2526skuId%253D5818001%2526st%253D5818001%2526cp%253D1%2526lp%253D1%2526ref%253D186%2526loc%253D42
			mmConversionTag(528365, this);
		}
		else if (/.*462529/i.test(cvDestination_URL_wParam)) { //http://www.officedepot.com/a/products/462529/HP-Pavilion-Convertible-Laptop-Computer-With/
			mmConversionTag(528367, this);
		}
		else if (/.*223073/i.test(cvDestination_URL_wParam)) { //http://linksynergy.walmart.com/fs-bin/click?id=kmZjHBYatgE&subid=&offerid=223073.1&type=10&tmpid=1082&u1=IICGLP&RD_PARM1=http://www.walmart.com/ip/HP-Ash-Silver-13.3-Pavilion-X360-13-a019wm-Laptop-PC-with-AMD-Quad-Core-A6-6310-Processor-4GB-Memory-Touchscreen-500GB-Hard-Drive-and-Windows-8.1/36561196
			mmConversionTag(528368, this);
		}
		// one entry in scode_yt.js
	}
	if (/www.amd.com\/en-us\/solutions\/pro(|\/)$/i.test(cvURLCheck)) { //09-02-2014|11-23-2014
		if (cvDestination_LID.indexOf("Btn-orange_|More on PRO Performance") != -1) {
			mmConversionTag(529313, this);
		}
		else if (cvDestination_LID.indexOf("Btn-orange_|More on PRO security, manageability, and more") != -1) {
			mmConversionTag(529314, this);
		}
		else if (cvDestination_URL_wParam.indexOf("/www.amd.com/en-us/solutions/pro/security-manageability") != -1) {
			mmConversionTag(529315, this);
		}
		else if (cvDestination_LID.indexOf("PartnerRef|Solutions|Pro|More Details|hp") != -1) {
			mmConversionTag(529316, this);
		}
		else if (cvDestination_LID.indexOf("Form|SP|Commercial-News-Solutions-Pro|Complete") != -1) {
			mmConversionTag(529317, this);
		}
	}
	if (cvURLCheck.indexOf("en-us/markets/if-it-can-game") != -1) { //10-20-2014|12-31-2014
		if (/PartnerRef|IICG|.*bestbuy/i.test(cvDestination_LID)) {
			if (/.*hp-envy-touchsmart/i.test(cvDestination_URL_wParam)) {
				mmConversionTag(562223, this); //LEGO Batman
			}
		}
		else if (cvDestination_LID.indexOf("See the trailor") != -1) {
			mmConversionTag(562225, this); //LEGO Batman
		}
		else if (cvDestination_LID.indexOf("Check to see if your system qualifies") != -1) {
			mmConversionTag(562227, this);
		}
		else if (cvDestination_URL_wParam.indexOf("/gesture-control") != -1) {
			mmConversionTag(562229, this);
		}
		else if (cvDestination_URL_wParam.indexOf("/wireless-display") != -1) {
			mmConversionTag(562230, this);
		}
		else if (cvDestination_URL_wParam.indexOf("face-login") != -1) {
			mmConversionTag(562231, this);
		}
	}
	if (cvURLCheck.indexOf("en-gb/markets/if-it-can-game") != -1) { //10-28-2014|12-31-2014
		if (cvDestination_LID.indexOf("Shop JLP") != -1){
			mmConversionTag(564252, this);
		}
		else if (cvDestination_LID.indexOf("See the trailer") != -1) {
			mmConversionTag(564253, this);
		}
		else if (cvDestination_URL_wParam.indexOf("/gesture-control") != -1) {
			mmConversionTag(564255, this);
		}
		else if (cvDestination_URL_wParam.indexOf("/wireless-display") != -1) {
			mmConversionTag(564256, this);
		}
		else if (cvDestination_URL_wParam.indexOf("face-login") != -1) {
			mmConversionTag(564257, this);
		}
	}
} //close function cfMediaMindClickTrack

function cfFloodLightClickTrack(cvDestination_LID, cvDestination_URL_wParam) {
//callStaticFL('source', 'type', 'cat','u1','u2');

	if (cvURLCheck.indexOf("en-gb/solutions/pro") != -1) {
		if (/(Btn-orange|PartnerRef)/i.test(cvDestination_LID)) {
			callStaticFL('4498677', 'cover0', 'AMDPR0','en','uk');
		}
	}
	if (cvURLCheck.indexOf("fr-fr/solutions/pro") != -1) {
		if (/(Btn-orange|PartnerRef)/i.test(cvDestination_LID)) {
			callStaticFL('4498677', 'cover0', 'AMDPR00','fr','fr');
		}
	}
} //close function cfFloodLightClickTrack



//page load tracking
if (cvMMconversionTag>0) {
	mmConversionTag(cvMMconversionTag, this);
}
//nFusion MediaMind (MM)/Sizmek EOF


//Visual Website Optimizer Code BOF

cvExcludeVWO = 0;
cvExcludeVWODomains = ["localhost","-auth","-prod","-qa","-sit","-dit","-preprod","-design","-new","http://www.amd.com/us/as/sp2013/fpp.amd.com","http://www.amd.com/us/as/sp2013/app.amd.com"];
if (document.referrer.indexOf("visualwebsiteoptimizer.com/heatmap") !=-1) {
	cvExcludeVWO = 1;
} else {
	for (i=0;i < cvExcludeVWODomains.length;i++) {
		if (unescape(document.URL).indexOf(cvExcludeVWODomains[i]) != -1) {
			cvExcludeVWO = 1;
		}
	}
}
if (cvExcludeVWO == 0) {_load_js('../vwo/vwo_1.js'/*tpa=http://www.amd.com/us/as/vwo/vwo_1.js*/);}

//Visual Website Optimizer Code EOF

if ((cvSurveyURLCheck.toLowerCase().indexOf("ccc") == -1) && (cvURLCheck.toLowerCase().indexOf("compatcheck") == -1) && (cvURLCheck.toLowerCase().indexOf("cchecker") == -1)) { //do not edit; exclude from banners (i.e. CCC, ccc-apu, compatcheck, cchecker)
	//Survey code BOF
	if ((cvSurveyURLCheck.indexOf("/us/") != -1) || (cvSurveyURLCheck.indexOf("en-us") != -1)) {
		_load_js('survey.js'/*tpa=http://www.amd.com/us/as/sp2013/survey.js*/);
	}

	//YouTube tracking
	if (typeof jQuery != 'undefined') {
		//_load_js('scode_yt.js'/*tpa=http://www.amd.com/us/as/sp2013/scode_yt.js*/);
	}

	//ForeSee Survey
	if (/(www|sites|support|search|server|global|subscriptions|developer|community|shop|products|experience)\.amd\.com.*/i.test(cvURLCheck)) {
		_load_js('../foresee/foresee-trigger.js'/*tpa=http://www.amd.com/us/as/foresee/foresee-trigger.js*/);
	}

	//Cookie prompt
	//if ((/(www|sites|support|search|community|shop|products|global|server|experience)\.amd\.com.*/i.test(cvURLCheck)) || (cvURLCheck.indexOf("http://www.amd.com/us/as/sp2013/amd.mkt6303.com") != -1)) {
	if (/(www|sites|support|search|products|shop|partner)\.amd\.com.*/i.test(cvURLCheck)) {
		_load_js('../../../Style Library/js/cookie-prompt.js'/*tpa=http://www.amd.com/Style%20Library/js/cookie-prompt.js*/);
	}

	//StumbleUpon
	if (/(www|shop|experience)\.amd\.com.*/i.test(cvURLCheck)) {
		//_load_js('http://cdn.insidesocial.com/static/js/amd/a.min.js#c55544050c6c4377b14cbea50b75a01b');
	}
} //do not edit; exclude from banners (i.e. CCC, ccc-apu, compatcheck)

//Google Tag Manager (GTM)
	//AMD
	//if (/(www)\.amd\.com.*\/solutions\//i.test(cvURLCheck)) cvGTMID = "GTM-W2VXZ8"; //AMD
	//if (/(www)\.amd\.com.*\/solutions\//i.test(cvURLCheck)) cvGTMID = "GTM-T42PP2"; //AMD GTM v2
	//else if (/(search|shop)\.amd\.com.*/i.test(cvURLCheck)) cvGTMID = "GTM-T42PP2"; //AMD GTM v2
	cvGTMID = "GTM-T42PP2"; //AMD GTM v2
	//explido UK
	//if ((cvURLCheck== "http://www.amd.com/en-gb") || (cvURLCheck== "http://www.amd.com/en-gb/") || (/www\.amd\.com\/en-gb\/solutions.*[pro|business].*/i.test(cvURLCheck))) cvGTMID = "GTM-T22RGN";
	//explido FR
	//else if ((cvURLCheck== "http://www.amd.com/fr-fr") || (cvURLCheck== "http://www.amd.com/fr-fr/") || (/www\.amd\.com\/fr-fr\/solutions.*[pro|business].*/i.test(cvURLCheck)))  cvGTMID = "GTM-TV28KT";

	if (cvGTMID) {
		_load_js('gtm.js'/*tpa=http://www.amd.com/us/as/sp2013/gtm.js*/);
	}	//EOF GTM

//VECC: www.amd.com/us/vision/shop/cool-apps/ccc/pages/index.aspx; new: support.amd.com/en-us/download/ccc-APU
//Catalyst Pro CC: /sites.amd.com/us/business/ccc/pages/index.aspx
//CCC: /support.amd.com/en-us/download/ccc

//UTILITY FUNCTIONS
function cfGetByteSize(s) { //CVA - delete
	return encodeURIComponent("" + s).length;
}

function cfis_int(value) { //CVA - delete
  if(!isNaN(value) && (parseFloat(value) == parseInt(value))){
	  return true;
  } else {return false;}
}

function cfHideshow(elementid) { //VideoFX - delete
	if (!document.getElementById) return;
	if (elementid.style.display=="block") elementid.style.display="none";
	else elementid.style.display="block";
}

function cfClean(cvURL) { // use parseURL instead - delete
	if (cvURL) {
		cvURL = cvURL.replace("http://","/");
		cvURL = cvURL.replace("https://","/");
	}
	return cvURL;
}

function cfTrim(stringToTrim) { //not used - delete
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function parseURL(url){ // delete (replaced with parseUri)

// https://www.facebook.com/amd
// Object {
    // domain : "http://www.amd.com/us/as/sp2013/www.facebook.com",
    // host : "facebook",
    // path : "amd",
    // protocol : "https",
    // subdomain : "www",
    // tld : "com"
// }

    parsed_url = {}

    if ( url == null || url.length == 0 )
        return parsed_url;
    protocol_i = url.indexOf('://');
    parsed_url.protocol = url.substr(0,protocol_i);
    remaining_url = url.substr(protocol_i + 3, url.length);
    domain_i = remaining_url.indexOf('/');
    domain_i = domain_i == -1 ? remaining_url.length - 1 : domain_i;
    parsed_url.domain = remaining_url.substr(0, domain_i);
    parsed_url.path = domain_i == -1 || domain_i + 1 == remaining_url.length ? null : remaining_url.substr(domain_i + 1, remaining_url.length);

    domain_parts = parsed_url.domain.split('.');
    switch ( domain_parts.length ){
        case 2:
          parsed_url.subdomain = null;
          parsed_url.host = domain_parts[0];
          parsed_url.tld = domain_parts[1];
          break;
        case 3:
          parsed_url.subdomain = domain_parts[0];
          parsed_url.host = domain_parts[1];
          parsed_url.tld = domain_parts[2];
          break;
        case 4:
          parsed_url.subdomain = domain_parts[0];
          parsed_url.host = domain_parts[1];
          parsed_url.tld = domain_parts[2] + '.' + domain_parts[3];
          break;
    }

    parsed_url.parent_domain = parsed_url.host + '.' + parsed_url.tld;

    return parsed_url;
}

//get URL paramter values - delete (replaced with parseUri)
function cfParamValue(cvURL,name) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec(cvURL);
  if( results == null )
    return "";
  else
    return results[1];
}

function cfGetQParam(a, b) {	// Custom Function to Get Query Parameters - delete (replaced with parseUri)
	var c = a.indexOf('?'); var d = a.indexOf('#');
	if (c < 0) { return ""; }
	var e = a.substr(c + 1);
	if (d > 0) { e = a.substring(c + 1, d); }
	var f = e.split('&');
	for (var i = 0; i < f.length; i++) {
		var g = f[i].split('=');
		g[0] = unescape(g[0]);
		if (g[0] == b) {
			g[1] = unescape(g[1]);
			if (g[1].indexOf('"') > -1) {
				var h = /"/g;
				g[1] = g[1].replace(h, '\\"')
			}
			if (g[1].indexOf('+') > -1) {
				var j = /\+/g;
				g[1] = g[1].replace(j, ' ')
			}
			return g[1]
		}
	}
	return ""
}

/* Custom Functions for Responsive Web (RW) EOF */

function cfFindChild(cvNode,cvTag) { //node to search for children, tagname being searched
	var cvChildren = cvNode.children;
	for(var i=0; i < cvChildren.length; i++) {
		if(cvChildren[i].tagName.toLowerCase() == cvTag.toLowerCase()){return cvChildren[i];}
		else {
			cvFosterChildren=cfFindChild(cvChildren[i],cvTag);
			if (typeof cvFosterChildren != "undefined"){
				return cvFosterChildren;
			}
		}
	}
}

//***********************************************************************

/* cookie functions BOF */
function cfCookieEnabled() {
	var cvCookieEnabled = (navigator.cvCookieEnabled) ? true : false;
	if (typeof navigator.cvCookieEnabled == "undefined" && !cvCookieEnabled) {
		document.cookie="testcookie";
		cvCookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
	}
	return (cvCookieEnabled);
}

function cfSetCookie(c_name,value,exdays) { //negative expiry value denotes seconds not days
	if (exdays>=0) {
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	}
	else if (exdays<0) {
		var exdate=new Date();
		exdate.setSeconds(exdate.getSeconds() - exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	}
	document.cookie=c_name + "=" + c_value + ";path=/;domain=" + cfUtility(cvURL,'domain');
}

function cfGetCookie(c_name) {
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++) 	{
	  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
	  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
	  x=x.replace(/^\s+|\s+$/g,"");
	  if (x==c_name) 		{
		return unescape(y);
		}
	  }
}
/* cookie functions EOF */

/* LightBoxWindow BOF */
	function LightBoxWindow (survey_url,width,height) {
		if(typeof jQuery != "undefined")
		{
			var lightboxHTML = "<div id='overlay'></div><div id='lightbox' style='top: 20px; width: "+width+"px; padding: 1px; position: absolute; top:25px;left:25px;'><a href='#' id='popClose'>Close</a></div>";
			$(lightboxHTML).appendTo('body');
			$('#lightbox').append("<iframe height='"+height+"px' width='"+width+"px' id='surveyframe' src='" + survey_url + "'></iframe>");
			$('#overlay').css('opacity', '0.8');
			$("#popClose, #overlay").click(function() {
				$('#lightbox, #overlay').remove();
			});
			$(document).keyup(function(e){
				if(e.keyCode === 27)
				$('#lightbox, #overlay').remove();
			});
		}
		else
		{
			window.open ( survey_url, "", "toolbar=no,toolbar=0,location=no,location=0,status=no,status=0,menubar=no,menubar=0,scrollbars=yes,scrollbars=1,resizable=yes,resizable=1,height="+height+",width="+width );
		}
	}

	function LightBoxWindow2 (page_url,width,height,redirect_url) {
		if(typeof jQuery != "undefined")
		{
			document.domain = "http://www.amd.com/us/as/sp2013/amd.com";
			var lightboxHTML2 = "<div id='overlay'></div><div id='lightbox' style='top: 20px; width: "+width+"px; padding: 1px; position: absolute; top:25px;left:25px;'><a href='#' id='popClose'>Close</a></div>";
			$(lightboxHTML2).appendTo('body');
			$('#lightbox').append("<iframe height='"+height+"px' width='"+width+"px' id='pageframe' src='" + page_url + "'></iframe>");
			$('#overlay').css('opacity', '0.8');
			$("#popClose, #overlay").click(function(e) {
				window.location = redirect_url;
				window.location.href = redirect_url;
				$(location).attr("href",redirect_url);
				e.preventDefault();
				closeLightBoxWindow2();
			});
			$(document).keyup(function(e){
				if(e.keyCode === 27)
				window.location = redirect_url;
				window.location.href = redirect_url;
				$(location).attr("href",redirect_url);
				closeLightBoxWindow2();
			});
		}
		else
		{
			var theDiv = document.createElement("div");
			theDiv.setAttribute("id","divBackground");
			theDiv.setAttribute("style","position:absolute; top:0px; left:0px;background-color:black; z-index:100;opacity: 0.8;filter:alpha(opacity=60); -moz-opacity: 0.8; overflow:hidden; display:none");
			document.body.appendChild(theDiv);
			LoadModal();
			
			thePopUp = window.open ( page_url, "", "toolbar=no,toolbar=0,location=no,location=0,status=no,status=0,menubar=no,menubar=0,scrollbars=yes,scrollbars=1,resizable=yes,resizable=1,height="+height+",width="+width );
			thePopUp.focus();

	}
	}

function LoadModal() {
	var bcgDiv = document.getElementById("divBackground");
			bcgDiv.style.display="block";
			if (bcgDiv != null)
			{
					if (document.body.clientHeight > document.body.scrollHeight)
					{
							bcgDiv.style.height = document.body.clientHeight + "px";
					}
					else
					{
							bcgDiv.style.height = document.body.scrollHeight + "px" ;
					}
					bcgDiv.style.width = "100%";
			}
}



	function closeLightBoxWindow2() {
		$('#lightbox, #overlay').remove();
	}
/* LightBoxWindow BOF */



function cfGetQParamwithHash(b,a,c) {	// Custom Function to Get Query Parameters with Hash for OSS
var e;c||(c="&");if(b&&a&&(a=""+a,e=a.indexOf("?"),e>=0&&(a=c+a.substring(e+1)+c,e=a.indexOf(c+b+"="),e>=0&&(a=a.substring(e+c.length+b.length+1),e=a.indexOf(c),e>=0&&
(a=a.substring(0,e)),a.length>0))))return s.unescape(a);return""
}

function cfLeft(str, n){
	if (n <= 0) { return ""; } else if (n > String(str).length) { return str; } else { return String(str).substring(0,n); }
}

function cfRight(str, n){
    if (n <= 0) { return ""; } else if (n > String(str).length) { return str; } else { var iLen = String(str).length; return String(str).substring(iLen, iLen - n); }
}

function cfIsNullOrWhitespace(value) { // Returns true if string value is empty or whitespace characters only.
	var re = /^\s+$/; // Regular Expression for matching whitespace characters
	return (((value == null) || (value.length == 0)) || re.test(value)); // Is value empty or whitespace?
}

function cfPause(d){for(var e=new Date,d=e.getTime()+d;e.getTime()<d;)e=new Date;}

function removeHTMLTags(strInputCode) {
	if (strInputCode) {
		strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1) {
			return (p1 == "lt") ? "<" : ">";
		});
		var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
		return strTagStrippedText;
	}
}

/* Responsive Web (RW) */
//Rendered Experiences (with specific break points)
function cfRWLayout() {
	cvRWLayout = "unknown";
	cvDocWidth = document.documentElement.clientWidth;
	if (cvDocWidth < 640) {cvRWLayout="small";}
	else if (cvDocWidth >= 640 && cvDocWidth <= 1000) {cvRWLayout="medium";}
	else if (cvDocWidth > 1000) {cvRWLayout="large";}
	return cvRWLayout;
}
/* RW: Screen Size - eVar43 */
//Screen Size
function cfWinSize() {
	cvWinSize=document.documentElement.clientWidth + 'x' + document.documentElement.clientHeight;
	return cvWinSize;
}

function cfDetectLanguage() {
	cvSysLanguage = window.navigator.userLanguage || window.navigator.language || "";
	cvSysLanguage = cvSysLanguage.toLowerCase();
	return cvSysLanguage;
}

function getCurrentTimeUTC() {
    var tmLoc = new Date();
    return tmLoc.getTime();
}

function cfSPwebtrack(theName,theType,theLinkObject) {
		if (cvOldLinkClickObject != theLinkObject) {
			cvOldLinkClickObject = theLinkObject;
			return ewt.track({'name':"GA"+theName,'type':theType,'link':theLinkObject });
		}
}

//on-site search (OSS) BOF
cvGAtimer=1;
cvGASavedSearchKeywordCheck = new Array();
cvGASavedSearchKeywordCheck[0] = "";
cvGASavedSearchKeywordCheck[1] = "";

function cfGAGetSearchKeywordFromURL(){
	// cvGASearchKeywordCheck=cfGAGetQParamwithHash("k",document.URL.toLowerCase());
	cvGASearchKeywordCheck=parseUri(cvURL).queryKey["k"] +"#"+parseUri(cvURL).anchor;
	if(cvGASearchKeywordCheck.indexOf("#s=") != -1) {
		cvGASearchKeywordCheck=cvGASearchKeywordCheck.substring(0,cvGASearchKeywordCheck.indexOf("#s="));
	}
	if(cvGASearchKeywordCheck.indexOf("#k=") != -1) {
		cvGASearchKeywordCheck=cvGASearchKeywordCheck.substring(cvGASearchKeywordCheck.lastIndexOf("#k=")+3);
	}
	return cvGASearchKeywordCheck;
}

function cfGACheckSearchAjax() {
	if(document.getElementById('Result')) {
		cvGASearchHTML = document.getElementById('Result').innerHTML;
		cvGASearchURL = decodeURI(document.URL.toLowerCase());
		if ((cvGASearchHTML == document.getElementById('Result').innerHTML) && cvGACheckSearchAjax!='') {
			window.clearInterval(cvGACheckSearchAjax);
			cvGACheckSearchAjax = '';
			cfGATrackSearch1();
		}
	}
}
function cfGACheckSearchAjax1() {

	if(document.getElementById('Result')) {
		cvGASearchHTML = document.getElementById('Result').innerHTML;
		cvGASearchURL = decodeURI(document.URL.toLowerCase());
		if ((cvGASearchHTML == document.getElementById('Result').innerHTML) && cvGACheckSearchAjax!='') {
			window.clearInterval(cvGACheckSearchAjax);
			cvGACheckSearchAjax = '';
			cfGATrackSearch2();
		}
	}
}
//csKeyword, csSearchOriginationPage, csTotalCounts, csResultsRange, csPageNumber
function cfGATrackSearch1() {
	cvGASavedSearchKeywordCheck[0] = cvGASavedSearchKeywordCheck[1];
	cvGASavedSearchKeywordCheck[1] = cfGAGetSearchKeywordFromURL();

	//if ((typeof csKeyword != 'undefined') && (cvGALinkedSearch == 0)) {	
	if (typeof csKeyword == 'undefined') {
		csKeyword = cfGAGetSearchKeywordFromURL();
	}
	if (typeof csKeyword != 'undefined') {
		if (csKeyword != '') {
			sAA.prop6 = parseUri(cvURL).host; // Search Property ID
			sAA.prop55 = sAA.eVar55 = csKeyword.toLowerCase(); // Internal Search Terms
			if (typeof sAA.referrer != 'undefined') sAA.eVar39 = sAA.prop11 = sAA.referrer;
			if (typeof csTotalCounts != 'undefined') {
				if (csTotalCounts == 0) {
					// if($(".ms-srch-group-count")) {
					if($("*").hasClass("ms-srch-group-count")) {
						csTotalCounts=$( ".ms-srch-group-count" ).text().match(/\d/g).join("");
					}
				}
				sAA.prop36 = sAA.eVar36 = csTotalCounts; // Total Counts
			}

			if (cfCookieEnabled()) {
				cvGACookieDURL = cfGetCookie('c_durl') || "";
				if (/shop\.amd\.com\/.*k=.*/i.test(cvGACookieDURL)) {cvGALinkedSearch=1;}
				else cvGALinkedSearch=0;
				cfSetCookie('c_durl',"",1); //durl = cleared
			}

			if(cvGASavedSearchKeywordCheck[0] == "" || ((cvGASavedSearchKeywordCheck[0] != cvGASavedSearchKeywordCheck[1]) && cvGASavedSearchKeywordCheck[0] != "")) {
				// s.events = s.apl(s.events,"event5",",",1); // On-Site Search (Counter)
				cvOSSEvents += ",event5"; // On-Site Search (Counter)
				//if (cvGASPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-counter',link:o }); }
				//if (cvGASPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-counter',link:o }); } )(); }

				//if (csTotalCounts == 0) {
				if ($( "#NoResult").length ==1) {
					sAA.prop9 = sAA.eVar9 = csKeyword.toLowerCase(); // Internal Search Failed Terms
					// s.events = s.apl(s.events,"event12",",",1); // On-Site Search (Failed)
					cvOSSEvents += ",event12"; // On-Site Search (Failed)
					//if (cvGASPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-failed',link:o }); }
					//if (cvGASPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-failed',link:o }); } )(); }
				} else {
					// s.events = s.apl(s.events,"event11",",",1); // On-Site Search (Success)
					cvOSSEvents += ",event11"; // On-Site Search (Success)
					//if (cvGASPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-success',link:o }); }
					//if (cvGASPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-success',link:o }); } )(); }
				}
			}
			sAA.prop37 = sAA.eVar37 = Math.round(csPageNumber); // Results Page Number
			sAA.prop38 = sAA.eVar38 = csKeyword.toLowerCase() + "|" + csResultsRange; // Keywords | Results Range

			// ga('create', 'UA-XXXXX-X');
			// ga('send', 'event', 'category-subscribe', 'action-subscribe', 'label-subscribe');

			//data layer event name is the GTM trigger event name
			dataLayer.push({
				'event': 'search',
				// 'eventAction': cvOSSEvents,
				// 'eventLabel': csTotalCounts,
				// 'eventValue': sAA.prop38,
				'OSS-keyword':sAA.prop55||'',
				'OSS-propertyID':sAA.prop6||'',
				'OSS-referrer':sAA.prop11||'',
				'OSS-totalCount':sAA.prop36||'0',
				'OSS-events':cvOSSEvents||'',
				'OSS-failedKeyword':sAA.prop9||'',
				'OSS-pageNumber':sAA.prop37||'',
				'OSS-keywordRange':sAA.prop38||''
			});
			// On-Site Search (Advanced) Event
			//if (csAdvancedSearch == '1') s.events = s.apl(s.events,"event14",",",1);
			// cvGASTL = 1;
			// s.tl(this,'o',cvURL);
			// s.clearVars();
			// sAA.events = sAA.linkTrackEvents = ''; cvGASTL = 0;
		}
	}
}

function cfGATrackSearch2() {
	cvGASavedSearchKeywordCheck[0] = cvGASavedSearchKeywordCheck[1];
	cvGASavedSearchKeywordCheck[1] = cfGAGetSearchKeywordFromURL();

	//if ((typeof csKeyword != 'undefined') && (cvGALinkedSearch == 0)) {	
	if (typeof csKeyword == 'undefined') {
		csKeyword = cfGAGetSearchKeywordFromURL();
	}
	if (typeof csKeyword != 'undefined') {
		if (csKeyword == '') {
			csKeyword = "TBD";
		}
			sAA.prop6 = parseUri(cvURL).host; // Search Property ID
			sAA.prop55 = sAA.eVar55 = csKeyword.toLowerCase(); // Internal Search Terms
			if (typeof sAA.referrer != 'undefined') sAA.eVar39 = sAA.prop11 = sAA.referrer;
			if (typeof csTotalCounts != 'undefined') {
				if (csTotalCounts == 0) {
					// if($(".ms-srch-group-count")) {
					if($("*").hasClass("ms-srch-group-count")) {
						csTotalCounts=$( ".ms-srch-group-count" ).text().match(/\d/g).join("");
					}
				}
				sAA.prop36 = sAA.eVar36 = csTotalCounts; // Total Counts
			}

			if (cfCookieEnabled()) {
				cvGACookieDURL = cfGetCookie('c_durl') || "";
				if (/shop\.amd\.com\/.*k=.*/i.test(cvGACookieDURL)) {cvGALinkedSearch=1;}
				else cvGALinkedSearch=0;
				cfSetCookie('c_durl',"",1); //durl = cleared
			}

			if(cvGASavedSearchKeywordCheck[0] == "" || ((cvGASavedSearchKeywordCheck[0] != cvGASavedSearchKeywordCheck[1]) && cvGASavedSearchKeywordCheck[0] != "")) {
				// s.events = s.apl(s.events,"event5",",",1); // On-Site Search (Counter)
				cvOSSEvents += ",event5"; // On-Site Search (Counter)
				//if (cvGASPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-counter',link:o }); }
				//if (cvGASPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-counter',link:o }); } )(); }

				//if (csTotalCounts == 0) {
				if ($( "#NoResult").length ==1) {
					sAA.prop9 = sAA.eVar9 = csKeyword.toLowerCase(); // Internal Search Failed Terms
					// s.events = s.apl(s.events,"event12",",",1); // On-Site Search (Failed)
					cvOSSEvents += ",event12"; // On-Site Search (Failed)
					//if (cvGASPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-failed',link:o }); }
					//if (cvGASPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-failed',link:o }); } )(); }
				} else {
					// s.events = s.apl(s.events,"event11",",",1); // On-Site Search (Success)
					cvOSSEvents += ",event11"; // On-Site Search (Success)
					//if (cvGASPwebtrack==1) {return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-success',link:o }); }
					//if (cvGASPwebtrack==1) {(function(){return ewt.track({name:s.prop6+'|'+s.prop55,type:'oss-success',link:o }); } )(); }
				}
			}
			sAA.prop37 = sAA.eVar37 = Math.round(csPageNumber); // Results Page Number
			sAA.prop38 = sAA.eVar38 = csKeyword.toLowerCase() + "|" + csResultsRange; // Keywords | Results Range

			// ga('create', 'UA-XXXXX-X');
			// ga('send', 'event', 'category-subscribe', 'action-subscribe', 'label-subscribe');

			//data layer event name is the GTM trigger event name
			dataLayer.push({
				'event': 'shopsearch',
				// 'eventAction': cvOSSEvents,
				// 'eventLabel': csTotalCounts,
				// 'eventValue': sAA.prop38,
				'SS-keyword':sAA.prop55||'',
				'SS-propertyID':sAA.prop6||'',
				'SS-referrer':sAA.prop11||'',
				'SS-totalCount':sAA.prop36||'',
				'SS-events':cvOSSEvents||'',
				'SS-failedKeyword':sAA.prop9||'',
				'SS-pageNumber':sAA.prop37||'',
				'SS-keywordRange':sAA.prop38||''
			});
			// On-Site Search (Advanced) Event
			//if (csAdvancedSearch == '1') s.events = s.apl(s.events,"event14",",",1);
			// cvGASTL = 1;
			// s.tl(this,'o',cvURL);
			// s.clearVars();
			// sAA.events = sAA.linkTrackEvents = ''; cvGASTL = 0;
		
	}
}


function cfGATrackSearch() {
//empty
}

cvGASearchTrackCnt=0;
if ((parseUri(cvURL).queryKey["k"]) || (cvURL.indexOf("#k=") != -1) || (cvURL.indexOf("http://www.amd.com/us/as/sp2013/shop.amd.com") != -1) ||(typeof csKeyword != 'undefined')) {
	if (cvGASearchTrackCnt==1){
		if (cvGASearchURL != decodeURI(document.URL.toLowerCase())) {
			cvGASearchTrackCnt=0;
		}
	}
	if ((cvGASearchTrackCnt==0) && (cvGACheckSearchAjax == '')){
		// console.log('before set: '+cvGACheckSearchAjax);
		if (cvURL.indexOf("http://www.amd.com/us/as/sp2013/shop.amd.com") != -1) {
		cvGACheckSearchAjax=setInterval(function(){cfGACheckSearchAjax1()},250);
		} else {
			cvGACheckSearchAjax=setInterval(function(){cfGACheckSearchAjax()},250);
		}
		// console.log('after set: '+cvGACheckSearchAjax);
		cvGASearchTrackCnt=1;
	}
}
//on-site search (OSS) EOF