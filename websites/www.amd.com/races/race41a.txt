<script id = "race41a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race41={};
	myVars.races.race41.varName="Lu_Id_li_1__onmouseout";
	myVars.races.race41.varType="@varType@";
	myVars.races.race41.repairType = "@RepairType";
	myVars.races.race41.event1={};
	myVars.races.race41.event2={};
	myVars.races.race41.event1.id = "Lu_DOM";
	myVars.races.race41.event1.type = "onDOMContentLoaded";
	myVars.races.race41.event1.loc = "Lu_DOM_LOC";
	myVars.races.race41.event1.isRead = "False";
	myVars.races.race41.event1.eventType = "@event1EventType@";
	myVars.races.race41.event2.id = "Lu_Id_li_3";
	myVars.races.race41.event2.type = "onmouseout";
	myVars.races.race41.event2.loc = "Lu_Id_li_3_LOC";
	myVars.races.race41.event2.isRead = "True";
	myVars.races.race41.event2.eventType = "@event2EventType@";
	myVars.races.race41.event1.executed= false;// true to disable, false to enable
	myVars.races.race41.event2.executed= false;// true to disable, false to enable
</script>

