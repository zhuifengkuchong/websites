<script id = "race89a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race89={};
	myVars.races.race89.varName="Function[18628].guid";
	myVars.races.race89.varType="@varType@";
	myVars.races.race89.repairType = "@RepairType";
	myVars.races.race89.event1={};
	myVars.races.race89.event2={};
	myVars.races.race89.event1.id = "Lu_Id_li_1";
	myVars.races.race89.event1.type = "onmouseover";
	myVars.races.race89.event1.loc = "Lu_Id_li_1_LOC";
	myVars.races.race89.event1.isRead = "False";
	myVars.races.race89.event1.eventType = "@event1EventType@";
	myVars.races.race89.event2.id = "Lu_Id_li_16";
	myVars.races.race89.event2.type = "onmouseover";
	myVars.races.race89.event2.loc = "Lu_Id_li_16_LOC";
	myVars.races.race89.event2.isRead = "True";
	myVars.races.race89.event2.eventType = "@event2EventType@";
	myVars.races.race89.event1.executed= false;// true to disable, false to enable
	myVars.races.race89.event2.executed= false;// true to disable, false to enable
</script>

