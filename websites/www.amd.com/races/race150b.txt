<script id = "race150b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race150={};
	myVars.races.race150.varName="Function[18628].guid";
	myVars.races.race150.varType="@varType@";
	myVars.races.race150.repairType = "@RepairType";
	myVars.races.race150.event1={};
	myVars.races.race150.event2={};
	myVars.races.race150.event1.id = "Lu_Id_li_21";
	myVars.races.race150.event1.type = "onmouseout";
	myVars.races.race150.event1.loc = "Lu_Id_li_21_LOC";
	myVars.races.race150.event1.isRead = "True";
	myVars.races.race150.event1.eventType = "@event1EventType@";
	myVars.races.race150.event2.id = "Lu_Id_li_1";
	myVars.races.race150.event2.type = "onmouseover";
	myVars.races.race150.event2.loc = "Lu_Id_li_1_LOC";
	myVars.races.race150.event2.isRead = "False";
	myVars.races.race150.event2.eventType = "@event2EventType@";
	myVars.races.race150.event1.executed= false;// true to disable, false to enable
	myVars.races.race150.event2.executed= false;// true to disable, false to enable
</script>

