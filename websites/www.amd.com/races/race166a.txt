<script id = "race166a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race166={};
	myVars.races.race166.varName="Function[18628].guid";
	myVars.races.race166.varType="@varType@";
	myVars.races.race166.repairType = "@RepairType";
	myVars.races.race166.event1={};
	myVars.races.race166.event2={};
	myVars.races.race166.event1.id = "Lu_Id_li_1";
	myVars.races.race166.event1.type = "onmouseover";
	myVars.races.race166.event1.loc = "Lu_Id_li_1_LOC";
	myVars.races.race166.event1.isRead = "False";
	myVars.races.race166.event1.eventType = "@event1EventType@";
	myVars.races.race166.event2.id = "Lu_Id_li_29";
	myVars.races.race166.event2.type = "onmouseout";
	myVars.races.race166.event2.loc = "Lu_Id_li_29_LOC";
	myVars.races.race166.event2.isRead = "True";
	myVars.races.race166.event2.eventType = "@event2EventType@";
	myVars.races.race166.event1.executed= false;// true to disable, false to enable
	myVars.races.race166.event2.executed= false;// true to disable, false to enable
</script>

