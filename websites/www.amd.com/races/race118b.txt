<script id = "race118b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race118={};
	myVars.races.race118.varName="Function[18628].guid";
	myVars.races.race118.varType="@varType@";
	myVars.races.race118.repairType = "@RepairType";
	myVars.races.race118.event1={};
	myVars.races.race118.event2={};
	myVars.races.race118.event1.id = "Lu_Id_li_5";
	myVars.races.race118.event1.type = "onmouseout";
	myVars.races.race118.event1.loc = "Lu_Id_li_5_LOC";
	myVars.races.race118.event1.isRead = "True";
	myVars.races.race118.event1.eventType = "@event1EventType@";
	myVars.races.race118.event2.id = "Lu_Id_li_1";
	myVars.races.race118.event2.type = "onmouseover";
	myVars.races.race118.event2.loc = "Lu_Id_li_1_LOC";
	myVars.races.race118.event2.isRead = "False";
	myVars.races.race118.event2.eventType = "@event2EventType@";
	myVars.races.race118.event1.executed= false;// true to disable, false to enable
	myVars.races.race118.event2.executed= false;// true to disable, false to enable
</script>

