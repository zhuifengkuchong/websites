<script id = "race74b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race74={};
	myVars.races.race74.varName="Lu_Id_li_36__onmouseout";
	myVars.races.race74.varType="@varType@";
	myVars.races.race74.repairType = "@RepairType";
	myVars.races.race74.event1={};
	myVars.races.race74.event2={};
	myVars.races.race74.event1.id = "Lu_Id_li_36";
	myVars.races.race74.event1.type = "onmouseout";
	myVars.races.race74.event1.loc = "Lu_Id_li_36_LOC";
	myVars.races.race74.event1.isRead = "True";
	myVars.races.race74.event1.eventType = "@event1EventType@";
	myVars.races.race74.event2.id = "Lu_DOM";
	myVars.races.race74.event2.type = "onDOMContentLoaded";
	myVars.races.race74.event2.loc = "Lu_DOM_LOC";
	myVars.races.race74.event2.isRead = "False";
	myVars.races.race74.event2.eventType = "@event2EventType@";
	myVars.races.race74.event1.executed= false;// true to disable, false to enable
	myVars.races.race74.event2.executed= false;// true to disable, false to enable
</script>

