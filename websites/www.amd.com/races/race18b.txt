<script id = "race18b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race18={};
	myVars.races.race18.varName="Lu_Id_li_9__onmouseover";
	myVars.races.race18.varType="@varType@";
	myVars.races.race18.repairType = "@RepairType";
	myVars.races.race18.event1={};
	myVars.races.race18.event2={};
	myVars.races.race18.event1.id = "Lu_Id_li_16";
	myVars.races.race18.event1.type = "onmouseover";
	myVars.races.race18.event1.loc = "Lu_Id_li_16_LOC";
	myVars.races.race18.event1.isRead = "True";
	myVars.races.race18.event1.eventType = "@event1EventType@";
	myVars.races.race18.event2.id = "Lu_DOM";
	myVars.races.race18.event2.type = "onDOMContentLoaded";
	myVars.races.race18.event2.loc = "Lu_DOM_LOC";
	myVars.races.race18.event2.isRead = "False";
	myVars.races.race18.event2.eventType = "@event2EventType@";
	myVars.races.race18.event1.executed= false;// true to disable, false to enable
	myVars.races.race18.event2.executed= false;// true to disable, false to enable
</script>

