<script id = "race42a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race42={};
	myVars.races.race42.varName="Lu_Id_li_1__onmouseout";
	myVars.races.race42.varType="@varType@";
	myVars.races.race42.repairType = "@RepairType";
	myVars.races.race42.event1={};
	myVars.races.race42.event2={};
	myVars.races.race42.event1.id = "Lu_DOM";
	myVars.races.race42.event1.type = "onDOMContentLoaded";
	myVars.races.race42.event1.loc = "Lu_DOM_LOC";
	myVars.races.race42.event1.isRead = "False";
	myVars.races.race42.event1.eventType = "@event1EventType@";
	myVars.races.race42.event2.id = "Lu_Id_li_4";
	myVars.races.race42.event2.type = "onmouseout";
	myVars.races.race42.event2.loc = "Lu_Id_li_4_LOC";
	myVars.races.race42.event2.isRead = "True";
	myVars.races.race42.event2.eventType = "@event2EventType@";
	myVars.races.race42.event1.executed= false;// true to disable, false to enable
	myVars.races.race42.event2.executed= false;// true to disable, false to enable
</script>

