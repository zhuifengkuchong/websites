jQuery.noConflict();
(function($) {
  $(function(){
    
    $('.adserv a').after('<p style="color: #666;font-size: 10px;font-family: sans-serif;text-align: center;">ADVERTISEMENT</p>');
    
    var windowSize = "width=1024,height=600,scrollbars=yes,menubar=yes,location=yes,toolbar=yes,status=yes,menubar=yes,resizable=yes";
    
    $('area[title~="Visit"]').click( function (event){
      var url = $(this).attr("href");
      var windowName = $(this).attr("name");
      window.open(url, windowName, windowSize);
      return false;
    });
  });
})(jQuery);