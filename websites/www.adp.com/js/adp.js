// JavaScript Document
$(document).ready(function() {
	equalHeight($(".section-options > li a"));
	var ichild = 1;
	$('#nav-top>li').each(
		function(){            
        	$(this).addClass('nth-child-'+ichild);
			ichild++;
      });
	$("#media + .submenu>ul").addClass("no-margin");     
	$('#col-content .table-4cols div.table-row:first-child').addClass('first-child');  
    $('#col-content .table-2cols .table-row div.table-col:first-child, #col-content .table-4cols .table-row div.table-col:first-child, #col-content .table-2cols.equal-cols .table-row div.table-col:first-child, #col-body .career-information .career-information-box:first-child').addClass('first-child');
	$('#col-content .table-2cols .table-row div.table-col:last-child, #col-content .table-4cols .table-row div.table-col:last-child, #nav-footer ul li:last-child, #col-body .us-row .table-row .row-box:last-child').addClass('last-child');
/* DropDown */
	try {
		$(".select-stylized.w134 select").msDropDown();
	} catch(e) {
		/*alert(e.message);*/
	}
	try {
		$(".select-stylized.w82 select").msDropDown();
	} catch(e) {
		/*alert(e.message);*/
	}
	try {
		$(".select-stylized select.ctrl-max, .select-stylized.w134 select, .select-stylized.w82 select").msDropDown();
	} catch(e) {
		/*alert(e.message);*/
	}
	$('#nav-top li').hover(
			function() {				
				$(this).children('a').css('background','url(/images/backgrounds/bg-left-hover-menu.gif) no-repeat left top');
				$(this).children('a').css('color','#2c2c2c');
				$(this).children('.submenu').show();
			},
			function(){
				$(this).children('a').css('background','none');	
				$(this).children('a').css('color','#fff');
				$(this).children('.submenu').hide();				
			}
		);
	$('#tn-hide').removeClass('tn-loading');
    $('#learnmoreurl').removeClass('tn-loading');	
/******/
});
/*Add Function to Windows Event Load*/
addEvent(window,'load',addCSSLineDouble);
addEvent(window,'load',lblControler);
addEvent(window,'load',fixHeightContainers);
addEvent(window,'load',applyRoundBorders);

if(navigator.appVersion.indexOf('MSIE 6')!=-1){
	addEvent(window,'load',initOverList);
	addEvent(window,'load',applyPNGFilters);
}

function addCSSLineDouble(){
	var _lhn=document.getElementById('lhn');
	if(_lhn!=null){
		for(i=0;i<_lhn.childNodes.length;i++){
			var _node=_lhn.childNodes[i];
			if(_node!=null && _node.tagName=='LI'){
				for(j=0;j<_node.childNodes.length;j++){
					var _tagA = _node.childNodes[j];
					if(_tagA.tagName=='A' && _tagA.offsetHeight > 28 ){
						_tagA.className+=' line-double';
					}
				}
			}
		}
		/*var _Atags = _lhn.getElementsByTagName('a');
		for(i=0;i<_Atags.length;i++){
			if(_Atags[i].className.indexOf('only-child')!=-1 ){
				if( _Atags[i].offsetHeight > 28 ){
					_Atags[i].className+=' line-double';
				}
			}
		}*/
	}
}

var roundClasses = ['http://www.adp.com/js/ul.right-links.li','div.box-round'];

/* round borders */
function applyRoundBorders(){
	for(var j=0;j<roundClasses.length;j++){
		var tagDesc=roundClasses[j].split(".");
		var containers=getElementsByClassName(document, tagDesc[0], tagDesc[1]);
		for(var i=0;i<containers.length;i++){
			if(tagDesc.length==2){
				var _tleft = document.createElement("span");
				_tleft.className="rl-tleft";
				containers[i].appendChild(_tleft);
				var _tright= document.createElement("span");
				_tright.className="rl-tright";
				containers[i].appendChild(_tright);
				var _bleft = document.createElement("span");
				_bleft.className="rl-bleft";
				containers[i].appendChild(_bleft);
				var _bright = document.createElement("span");
				_bright.className="rl-bright";
				containers[i].appendChild(_bright);
			}else if(tagDesc.length>2){
				var childs = containers[i].childNodes;
				for(var k=0;k<childs.length;k++){
					if(childs[k].tagName == tagDesc[2].toUpperCase()){
						var _tleft = document.createElement("span");
						_tleft.className="rl-tleft";
						childs[k].appendChild(_tleft);
						var _tright= document.createElement("span");
						_tright.className="rl-tright";
						childs[k].appendChild(_tright);
						var _bleft = document.createElement("span");
						_bleft.className="rl-bleft";
						childs[k].appendChild(_bleft);
						var _bright = document.createElement("span");
						_bright.className="rl-bright";
						childs[k].appendChild(_bright);
					}
				}
			}
		}
	}
}

/* for height of columns */
function fixHeightContainers() {
	var rowBoxes = getElementsByClassName(document, "div", "table-row");
	for(var i=0;i<rowBoxes.length;i++){
		var divs = new Array();			
		for(var j=0; j < rowBoxes[i].childNodes.length; j++){
			if(rowBoxes[i].childNodes[j].tagName == "DIV"){
				divs.push(rowBoxes[i].childNodes[j]);
			}
		}
		if(divs.length>1){
			setTall(divs);
		}
	}
}
function setTall() {	
		divs = arguments[0];
		// Let's determine the maximum height out of all columns specified
		var maxHeight = 0;
		for (var i = 0; i < divs.length; i++) {
			if (divs[i].offsetHeight > maxHeight) maxHeight = divs[i].offsetHeight;
		}
		
		for (var i = 0; i < divs.length; i++) {
			divs[i].style.height = maxHeight + 'px';
			
			if (divs[i].offsetHeight > maxHeight) {
				divs[i].style.height = (maxHeight - (divs[i].offsetHeight - maxHeight)) + 'px';
				
			}
			
		}
}

function equalHeight(group) {
	var tallest = 0;
	group.each(function() {
		var thisHeight = $(this).height();
		if(thisHeight > tallest) {
			tallest = thisHeight;
		}
	});
	group.height(tallest);
}

/* for PNG Images in IE 6 */
function applyPNGFilters(){
	if( navigator.appVersion.indexOf('MSIE 6')!=-1 && document.body.filters){
		for(var i=0; i<document.images.length; i++) {
			var img = document.images[i];
			var imgName = img.src.toUpperCase()
			if ((imgName.substring(imgName.length-3, imgName.length) == "PNG" ) || ( imgName.indexOf('.ASHX')!=-1)){
				var imgID = (img.id) ? "id='" + img.id + "' " : "";
				var imgClass = (img.className) ? "class='" + img.className + "' " : "";
				var imgTitle = (img.title) ? 'title="' + img.title + '" ' : 'title="' + img.alt + '" ';
				var imgStyle = "display:inline-block;" + img.style.cssText;
				if (img.align == "left") imgStyle = "float:left;" + imgStyle;
				if (img.align == "right") imgStyle = "float:right;" + imgStyle;
				if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle;
				var strNewHTML = "<span " + imgID + imgClass + imgTitle
				+ " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";"
				+ "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
				+ "(src=\'" + img.src + "\', sizingMethod='scale');\"></span>";
				img.outerHTML = strNewHTML;
				i = i-1;
			}
		}
	}
} 

/* for LI event hover*/
/*var navMainLI = 'country';*/
function initOverList(){
   if(!document.getElementsByTagName) return;
   var LIs, listItem;
	/*var nav = document.getElementById(navMainLI);*/
	var topNav = document.getElementsByTagName('LI');
   for(var i=0; i < topNav.length; i++) {
		var liItem = topNav[i];
      if(liItem!=null){
			liItem.onmouseover=function(){
				if(this.className!=null){
					this.className+=' hover';
				}else{
					this.className='hover';
				}
			}
			liItem.onmouseout=function(){
				this.className=this.className.replace('hover','');
			};
      }		
		//console.debug("%d %o %o", i, liItem, aTag);
   }
}

//http://www.alistapart.com/articles/makingcompactformsmoreaccessible
function initOverLabels () {
  if (!document.getElementById) return;

  var labels, id, field;

  // Set focus and blur handlers to hide and show 
  // LABELs with 'overlabel' class names.
  labels = document.getElementsByTagName('label');
  //console.debug("labels", labels);
  for (var i = 0; i < labels.length; i++) {
    //console.debug("over label found %b",(labels[i].className.indexOf('overlabel') == 0) );
    if (labels[i].className.indexOf('overlabel ') == 0) {
		 
      // Skip labels that do not have a named association
      // with another field.
      id = labels[i].htmlFor || labels[i].getAttribute('for');
      if (!id || !(field = document.getElementById(id))) {
        continue;
      }

      // Change the applied class to hover the label 
      // over the form field.
	  //console.debug("before find and replace: %s",labels[i].className);
      labels[i].className = labels[i].className.replace('overlabel','overlabel-apply');
	  //console.debug("after replace: %s",labels[i].className);

// Hide any fields having an initial value.
      if (field.value !== '') {
        hideLabel(field.getAttribute('id'), true);
      }

      // Set handlers to show and hide labels.
		field.onfocus = function(){
			hideLabel(this.getAttribute('id'), true);
      };
      field.onblur = function(){
			if (this.value === '') {
				hideLabel(this.getAttribute('id'), false);
			}
      };

      // Handle clicks to LABEL elements (for Safari).
      labels[i].onclick = function() {
        	var id, field;
        	id = this.getAttribute('for');
        	if (id && (field = document.getElementById(id))) {
         	field.focus();
        	}
      };
    }
  }
};

function hideLabel (field_id, hide) {
  var field_for;
  var labels = document.getElementsByTagName('label');
  for (var i = 0; i < labels.length; i++) {
    field_for = labels[i].htmlFor || labels[i].getAttribute('for');
    if (field_for == field_id) {
      //labels[i].style.textIndent = (hide) ? '-2000px' : '0px';
		labels[i].style.display = (hide) ? 'none' : 'block';
      return true;
    }
  }
}

var loaded = false;
function lblControler()
{
	if(!loaded){
		initOverLabels(); 
		loaded = true;
	}
	setTimeout(initOverLabels, 100);
}

// firebug degradiation
if (! ("console" in window) || !("firebug" in console)) {
    var names = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml", "group"
                 , "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];
    window.console = {};
    for (var i = 0; i <names.length; ++i) window.console[names[i]] = function() {};
}

// utility functions 
function endBubble(evt)
{
    if(evt.cancelBubble == false){ 
        evt.cancelBubble = true; 
        evt.returnValue = false;
        
     }
	 if (evt.stopPropagation) {
	    evt.stopPropagation(); 
	    evt.preventDefault();
    	
	 }
}

/**
 * This function allows the programmer to retrieve objects biased on the class name you 
 * want to retrieve. Similar to the GetElementByID and GetElementByTagName.
 * @member UtilNS
 * @param {object} oElm The object you want to search, could be a whole document or something more specific.
 * @param {string} strTagName The string containing the name of the tag name i.e. "a", "div", "span" what ever the class name is attached to.
 * If no tag is given it will search all elements.
 * @param {string} strClassName The string containing the class name you're searching for
 * @returns An array filled with the class names found in the search
 * @type Array
 * {@link  http://www.robertnyman.com/2005/11/07/the-ultimate-getelementsbyclassname/#more-256 getElementsByClassName} Utility Function
 */


function getElementsByClassName(oElm, strTagName, strClassName){
	var arrElements = (strTagName == "*" && document.all)? document.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	strClassName = strClassName.replace(/\-/g, "\\-");
	var oRegExp = new RegExp("(^|\\s)" + strClassName + "(\\s|$)");
	var oElement;
	for(var i=0; i<arrElements.length; i++){
		oElement = arrElements[i];		
		if(oRegExp.test(oElement.className)){
			arrReturnElements.push(oElement);
		}	
	}
	return arrReturnElements;
}

function addEvent(_target,_event,_function){
	if (_target.addEventListener)
		_target.addEventListener(_event, _function, false );
	else{
		_target.attachEvent('on'+_event, _function);
		ieModel = true;	
	}	
}