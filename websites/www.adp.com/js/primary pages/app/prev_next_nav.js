;(function(){

  App.Modules.PrevNextNav = function(){};

  $.extend( App.Modules.PrevNextNav.prototype,
    {

      start : function()
        {

          this.getElems();
          this.bindEvents();

        },

      getElems : function()
        {

          this.$_prevNextNavs = $('.prev-next-nav');

        },

      bindEvents : function()
        {

          this.$_prevNextNavs.each( function( i, prevNextNav )
            {

              var $_prevNextNav = $(prevNextNav);

              $_prevNextNav.find('.prev-item').bind('click', function( e, args )
                {
                  $_prevNextNav.removeClass('at-end');
                  $_prevNextNav.trigger('gotoPrevItem', args);
                });

              $_prevNextNav.find('.next-item').bind('click', function( e, args )
                {
                  $_prevNextNav.removeClass('at-start');
                  $_prevNextNav.trigger('gotoNextItem', args);
                });

              $_prevNextNav.bind('atStart', function()
                {
                  $_prevNextNav.removeClass('at-end');
                  $_prevNextNav.addClass('at-start');
                });

              $_prevNextNav.bind('atEnd', function()
                {
                  $_prevNextNav.removeClass('at-start');
                  $_prevNextNav.addClass('at-end');
                });

              $_prevNextNav.bind('inBetween', function()
                {
                  $_prevNextNav.removeClass('at-start').removeClass('at-end');
                });

              $_prevNextNav.trigger('atStart');

            });


        }


    });

})();