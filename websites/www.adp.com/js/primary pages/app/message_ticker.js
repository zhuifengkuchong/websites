; (function () {

    App.Modules.MessageTicker = function () { };

    $.extend(App.Modules.MessageTicker.prototype,
    {

        currentMessageIndex: 0,

        start: function () {
            this.getElems();
            this.initMessageNav();
            this.setScrollTimeout();
            if ($.browser.msie && parseInt($.browser.version, 10) < 9) {
                this.$_messages.hide();
                $(this.$_messages[0]).show();
            }

        },

        getElems: function () {

            this.$_messageTicker = $('#MessageTicker');
            this.$_messages = this.$_messageTicker.find('.message-item');
            this.$_prevNextNav = this.$_messageTicker.find('.prev-next-nav');
            this.$_messageScroller = this.$_messageTicker.find('.message-ticker-scroller');

            this.messageHeight = this.$_messages.first().outerHeight();

        },

        initMessageNav: function () {

            this.$_prevNextNav.bind('gotoPrevItem', $.proxy(this.gotoPrevItem, this));
            this.$_prevNextNav.bind('gotoNextItem', $.proxy(this.gotoNextItem, this));
            this.gotoPrevItem(); //Goto the first item

        },

        setScrollTimeout: function () {

            setTimeout($.proxy(function () {

                this.$_prevNextNav.find('.next-item').trigger('click', { automated: true });

            }, this), 5000);

        },

        gotoPrevItem: function (e, args) {

            args = args || {};

            this.oldMessageIndex = this.currentMessageIndex;
            this.currentMessageIndex -= 1;
            this.gotoMessage(args);

            if (args.automated) { this.setScrollTimeout(); }

        },

        gotoNextItem: function (e, args) {

            args = args || {};

            this.oldMessageIndex = this.currentMessageIndex;
            this.currentMessageIndex += 1;

            this.gotoMessage(args);

            if (args.automated) { this.setScrollTimeout(); }
        },

        gotoMessage: function (args) {

            if (args.automated && this.currentMessageIndex == this.$_messages.length) {
                this.currentMessageIndex = -1;
            }

            this.currentMessageIndex = this.currentMessageIndex > -1 ? this.currentMessageIndex : 0;
            this.currentMessageIndex = this.currentMessageIndex < this.$_messages.length ? this.currentMessageIndex : this.$_messages.length - 1;

            if ($.browser.msie && parseInt($.browser.version, 10) < 9) {

                $(this.$_messages[this.oldMessageIndex]).hide();
                $(this.$_messages[this.currentMessageIndex]).show();

            } else {

                $(this.$_messages[this.oldMessageIndex]).animate({
                    opacity: 0
                }, 500, $.proxy(function () {

                    this.$_messages.css({
                        'display': 'none'
                    });

                    $(this.$_messages[this.currentMessageIndex])
                .css({
                    'display': 'block'
                })
                .animate({
                    opacity: 1
                }, 500);

                }, this));

            }

            if (this.currentMessageIndex === 0) { this.$_prevNextNav.trigger('atStart'); }
            if (this.currentMessageIndex === this.$_messages.length - 1) { this.$_prevNextNav.trigger('atEnd'); }

        }

    });

})();