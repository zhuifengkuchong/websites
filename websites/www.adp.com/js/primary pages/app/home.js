; (function () {

    var Home, $_heroNav, $_heroContent, $_heroMessagePagination,
      $_heroImages, nextTimeout;

    App.Modules.Home = function () { };

    $.extend(App.Modules.Home.prototype,
    {
        start: function () {

            this.getElems();
            this.initHeroNav();
            this.initHeroMessages();

        },

        getElems: function () {

            this.$_heroNav = $_heroNav = $('#HeroNav');
            this.$_heroContent = $_heroContent = $('#HeroContent');
            this.$_heroMessagePagination = $_heroMessagePagination = $('#HeroMessagePagination');
            this.$_heroMessages = $_heroMessages = $('#HeroMessages');
            this.$_heroImages = $_heroImages = $('#HeroImages');
        },

        initHeroNav: function () {

            var openContent, $_navLinks;

            openContent = function (navLink, $_openContent) {

                if ($_openContent) {
                    $_openContent.removeClass('open');
                }

                $(navLink).closest('li').addClass('open');

            };

            $_navLinks = this.$_heroNav.find('a.hero-nav-link');

            $_navLinks.bind('mouseenter', function () {

                var $_openContent, navLink;

                $_openContent = $_heroNav.find('http://www.adp.com/js/primary pages/app/li.open');
                navLink = this;

                if ($_openContent.length) {
                    navLink.openTimeout = setTimeout(function () { openContent(navLink, $_openContent); }, 150);
                }
                else {
                    openContent(navLink, false);
                }

                clearTimeout(nextTimeout);

            });

            $_navLinks.bind('mouseleave', function () {

                if (this.openTimeout) { clearTimeout(this.openTimeout); }

            });

            this.$_heroNav.find('a.close-nav-content').bind('click', function () {
                $(this).closest('http://www.adp.com/js/primary pages/app/li.open').removeClass('open');
            });

            this.$_heroContent.bind('mouseleave', function () {
                $_heroNav.find('http://www.adp.com/js/primary pages/app/li.open').removeClass('open');
            });

        },

        initHeroMessages: function () {

            var $_messages, messageWidth, navItem, $_navItems, activeIndex,
              $_currentMessage, setNextTimeout, $_heroMessage, $_currentMsg,
              $_heroImages, imageWidth, scrollToMessage, currentImg_i, msgMaxHeight;

            $_messages = this.$_heroMessages.find('.hero-message');
            $_heroImages = [];
            $_heroMessages = [];
            $_navItems = [];
            currentMsg_i = -1;
            currentImg_i = -1;
            $_currentImg = false;
            $_currentMsg = false;
            msgMaxHeight = 0;

            this.$_heroImages.find('.hero-image').each(function (i, img) {
                var $_img = $(img);

                if (i === 0) {
                    $_img.css({
                        'z-index': 200
                    });
                }
                $_heroImages.push($(img));
            });

            $_messages.each(function (i, msg) {

                var $_msg, msgHeight;

                $_msg = $(msg);
                msgHeight = $_msg.height();

                if (msgMaxHeight < msgHeight) {
                    msgMaxHeight = msgHeight;
                }

                $_msg.css({
                    opacity: 0,
                    display: 'none'
                });

                $_heroMessages.push($_msg);

            });

            this.$_heroMessages.css({
                height: msgMaxHeight
            });

            this.$_heroMessages.animate({
                left: 0
            }, 500);

            scrollToMessage = function (e, args) {

                var nextImg_i;

                args = args || {};

                nextMsg_i = $(e.target).closest('.hero-page-link')[0].targetMessage_i;

                if (nextMsg_i >= $_messages.length) {
                    nextMsg_i = 0;
                }

                if (currentMsg_i > -1) {

                    $_heroMessages[currentImg_i].animate({
                        opacity: 0
                    }, 500, function () {
                        $(this).css({ 'display': 'none' });
                    });

                    $_navItems[currentMsg_i].removeClass('active');

                }

                setTimeout(function () {

                    scrollToNextImage(nextMsg_i, function () {
                        $_heroMessages[nextMsg_i]
                    .css({
                        'display': 'block'
                    }).animate({
                        opacity: 1
                    }, 500);

                        $_navItems[nextMsg_i].addClass('active');

                    });

                }, 350);

                currentMsg_i = nextMsg_i;

                if (args.automated) { setNextTimeout(); }
                else { clearTimeout(nextTimeout); }

            };

            scrollToNextImage = function (nextImg_i, callback) {

                var $_nextImg, $_currentImg;

                if (currentImg_i > -1) {

                    $.each($_heroImages, function (i, $_img) {
                        $_img.css({
                            'z-index': 50
                        });
                    });

                    $_currentImg = $_heroImages[currentImg_i];

                    $_currentImg.css({
                        'z-index': 100
                    });

                }

                $_nextImg = $_heroImages[nextImg_i];

                $_nextImg.css({
                    'z-index': 200,
                    'left': '100%'
                });

                $_nextImg.animate({
                    'left': 0
                }, 750, callback);

                currentImg_i = nextImg_i;

            };

            setNextTimeout = function () {
                if (nextTimeout) { clearTimeout(nextTimeout); }
                nextTimeout = setTimeout(function () {

                    var nextIndex = activeIndex + 1;

                    nextIndex = nextIndex < $_messages.length ? nextIndex : 0;
                    $_navItems[nextIndex].trigger('click', { automated: true });

                    activeIndex = nextIndex;

                }, 5000);
            };

            for (var i = 0; i < $_messages.length; i++) {

                $_navItem = $('<a class="hero-page-link' + (i === 0 ? ' active' : '') + '"><i class="page-dot"></i></a>');
                $_navItem[0].targetMessage_i = i;

                $_navItem.bind('click', scrollToMessage);

                this.$_heroMessagePagination.append($_navItem);

                $_navItems.push($_navItem);

            }

            activeIndex = 0;

            $_navItems[activeIndex].trigger('click', { automated: true });


        }

    });

})();