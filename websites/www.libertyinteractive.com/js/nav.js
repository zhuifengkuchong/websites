function initPage()
{
	var navRoot = document.getElementById("navigation");
	var lis = navRoot.getElementsByTagName("li");
	for (var i=0; i<lis.length; i++)
	{
		if (lis[i].parentNode.id == "navigation")
		{
			lis[i].onmouseover = function()
			{
				this.className += " hover";
               	var cul = this.getElementsByTagName("ul");
                if (cul){
                	for (var j=0; j<cul.length; j++){
                        cul[j].style.width = cul[j].parentNode.offsetWidth + "px";
                    }
                }

			}
			lis[i].onmouseout = function()
			{
				this.className = this.className.replace("hover","");
			}
		}
	}
}

if (window.attachEvent && !window.opera)
	attachEvent("onload", initPage);
