function chromeNudgeDelayed() {
    if (navigator && navigator.userAgent && /chrome/.test(navigator.userAgent.toLowerCase())) {
        setTimeout(function () {
            if (!_spBodyOnLoadCalled) {
                _spBodyOnLoadWrapper();
            }
        }, 250);
    }
}
Sys.Application.add_init(chromeNudgeDelayed);