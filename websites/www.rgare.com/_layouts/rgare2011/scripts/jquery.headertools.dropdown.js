/* initialize the dropdown */
function ActivateDropDowns() {
    $("div.headerDropDown div.headerDropDownButton").click(function (e) {
        $(this).parent().children("ul.headerDropDownList").slideToggle("slow", slideToggleCallBack);
        e.stopImmediatePropagation();
    });

    function slideToggleCallBack() {
        $(this).is(":visible") ? $("#arrow").addClass("active") : $("#arrow").removeClass("active");
    }

    $(document).click(function () {
        if ($("ul.headerDropDownList").is(":visible")) {
            $("ul.headerDropDownList").slideUp("slow", slideToggleCallBack);
        }
    });

    $("ul.headerDropDownList").click(function (e) {
        e.stopPropagation();
    });
}

_spBodyOnLoadFunctionNames.push("ActivateDropDowns");
