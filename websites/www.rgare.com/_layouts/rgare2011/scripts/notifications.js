// notification code

function ShowNotification() {

    var $alert = $('div.alertMessage');
    if ($alert.length && $alert.html().length > 0) {
        var alerttimer = window.setTimeout(function () {
            $alert.trigger('click');
        }, 3000);
        $alert.slideDown(600, "swing").click(function () {
            window.clearTimeout(alerttimer);
            $alert.slideUp(600, "swing");
        });
    }
}

_spBodyOnLoadFunctionNames.push("ShowNotification");	