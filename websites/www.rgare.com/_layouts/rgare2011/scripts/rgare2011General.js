// this fixes the default resizing behavior
// via http://kyleschaeffer.com/sharepoint/sharepoint-2010-scrolling/
function FixRibbonAndWorkspaceDimensions() {
    ULSxSy:;
    g_frl = true;
    var elmRibbon = GetCachedElement("s4-ribbonrow");
    var elmWorkspace = GetCachedElement("s4-workspace");
    var elmTitleArea = GetCachedElement("s4-titlerow");
    var elmBodyTable = GetCachedElement("s4-bodyContainer");
    if (!elmRibbon || !elmWorkspace || !elmBodyTable) {
        return;
    }
    if (!g_setWidthInited) {
        var setWidth = true;
        if (elmWorkspace.className.indexOf("s4-nosetwidth") > -1)
            setWidth = false;
        g_setWidth = setWidth;
        g_setWidthInited = true;
    }
    else {
        var setWidth = g_setWidth;
    }
    var baseRibbonHeight = RibbonIsMinimized() ? 0 : 90;
    var ribbonHeight = baseRibbonHeight + g_wpadderHeight;
    if (GetCurrentEltStyle(elmRibbon, "visibility") == "hidden") {
        ribbonHeight = 0;
    }

    // Override default resizing behavior
    // -- adds padding to the top of the "s4-workspace" <div> if the ribbon exists and has content
    // -- allows the ribbon to be positioned using CSS instead of JavaScript (more accessible)
    // -- checks to see if the page is inside a "no-ribbon" dialog
    if (elmRibbon.children.length > 0 && document.getElementsByTagName("html")[0].className.indexOf('ms-dialog-nr') == -1) {
        elmWorkspace.style.paddingTop = ribbonHeight + 'px';
    }
}

var EditMode = false;

// get url parameters
var urlParams = {};
(function () {
    var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&=]+)=?([^&]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
        q = window.location.search.substring(1);

    while (e = r.exec(q))
        urlParams[d(e[1])] = d(e[2]);
})();

function InitMap() {
    $("area").mouseover(function () {
        $('#map').attr("src", $(this).attr("hoversrc"));
        //	      $('#officeLocation').html($(this).attr("title"));
    }).mouseout(function () {
        //	      $('#officeLocation').html("");
        $('#map').attr("src", "../images/map/AllCountries.gif"/*tpa=http://www.rgare.com/_layouts/rgare2011/images/map/AllCountries.gif*/);
    });

}

_spBodyOnLoadFunctionNames.push("InitMap");

function addArrowToPeopleLinks() {
    $("h4.ms-rteElement-H4B a").append("<span class=\"linkArrow\"> &raquo;</span>");
}

_spBodyOnLoadFunctionNames.push("addArrowToPeopleLinks");


jQuery.fn.extend({
    highlight: function (search, insensitive, hls_class) {
        //var regex = new RegExp(search.replace(/([-.*+?^${}()|[\]\/\\])/g, "\\$1") + "(?!([^<]+)?>))", insensitive ? "ig" : "g");
        var regex = new RegExp("(<[^>]*>)|(\\b" + search.replace(/([-.*+?^${}()|[\]\/\\])/g, "\\$1") + ")", insensitive ? "ig" : "g");
        return this.html(this.html().replace(regex, function (a, b, c) {
            return (a.charAt(0) == "<") ? a : "<span class=\"" + hls_class + "\">" + c + "</span>";
        }));
    }
});

function highlightSearchQuery() {
    if (urlParams["search"] != null && urlParams["search"] != "")
        $("#bodyContentInner").highlight(urlParams["search"], 1, "searchTerm");
}

_spBodyOnLoadFunctionNames.push("highlightSearchQuery");

/*
function rose_replace() {
$(this).html($(this).html().replace('ROSE', 'ROSE<sup>&reg;</sup>'));
}
_spBodyOnLoadFunctionNames.push("rose_replace");
*/

/******************************
Google Analytics
******************************/
// determine if we're on one of the extranets
var _gaq = _gaq || [];
var subsite = window.location.pathname.split('/')[1];

if (subsite == "underwritingconnection") {
    _gaq.push(['_setAccount', 'UA-8312514-6']); // underwriting connection
}
else if (subsite == "rose") {
    _gaq.push(['_setAccount', 'UA-8312514-14']);  //rose
}
else if (subsite == "quest") {
    _gaq.push(['_setAccount', 'UA-8312514-16']);  //quest
}
else {
    _gaq.push(['_setAccount', 'UA-8312514-1']);    //rgare.com
}

_gaq.push(['_trackPageview']);
(function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www/') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();

/* document ready event */
jQuery(document).ready(function () {

    // cause of some browsers appending versus rewriting, clear out the html, then display current year in footer.
    var d = new Date();
    jQuery("#copyrightYear").html("");
    jQuery("#copyrightYear").html(d.getFullYear());
    d = null;
});