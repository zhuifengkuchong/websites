<script id = "race4b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race4={};
	myVars.races.race4.varName="Tree[0x7fadfcfe6458]:ctl00_PlaceHolderSearchArea_SearchBox_ctl03";
	myVars.races.race4.varType="@varType@";
	myVars.races.race4.repairType = "@RepairType";
	myVars.races.race4.event1={};
	myVars.races.race4.event2={};
	myVars.races.race4.event1.id = "ctl00_PlaceHolderSearchArea_SearchBox_S622C1022_InputKeywords";
	myVars.races.race4.event1.type = "onfocus";
	myVars.races.race4.event1.loc = "ctl00_PlaceHolderSearchArea_SearchBox_S622C1022_InputKeywords_LOC";
	myVars.races.race4.event1.isRead = "True";
	myVars.races.race4.event1.eventType = "@event1EventType@";
	myVars.races.race4.event2.id = "ctl00_PlaceHolderSearchArea_SearchBox_ctl03";
	myVars.races.race4.event2.type = "ctl00_PlaceHolderSearchArea_SearchBox_ctl03__parsed";
	myVars.races.race4.event2.loc = "ctl00_PlaceHolderSearchArea_SearchBox_ctl03_LOC";
	myVars.races.race4.event2.isRead = "False";
	myVars.races.race4.event2.eventType = "@event2EventType@";
	myVars.races.race4.event1.executed= false;// true to disable, false to enable
	myVars.races.race4.event2.executed= false;// true to disable, false to enable
</script>

