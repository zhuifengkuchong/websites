cpreview = function(){
	this.outline_callouts = function(){
		var calls = this.byclass('callout');
		for(i=0; i < calls.length; i++){
			if(calls[i].innerHTML == ''){
				calls[i].innerHTML = 'Available Callout Space &nbsp; (<a style="color:#006688;" target="_blank" href="'+this.admin_path+'mod/page/&id='+this.current+'&v=do">Edit</a>)';
				calls[i].style.padding = "20px";
				calls[i].style.margin = "10px 5px 10px 0";
				calls[i].style.border = "1px solid #ccc";
				calls[i].style.color = "#555";
				calls[i].style.background = "#fff";
			}
		}
	}
	this.byclass =  function(classname, node) {
	    if(!node) node = document.getElementsByTagName("body")[0];
	    var a = [];
	    var re = new RegExp('\\b' + classname + '\\b');
	    var els = node.getElementsByTagName("*");
	    for(var i=0,j=els.length; i<j; i++)
	        if(re.test(els[i].className))a.push(els[i]);
	    return a;
	}
}

