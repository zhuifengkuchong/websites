<script id = "race22b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race22={};
	myVars.races.race22.varName="Object[4104].triggered";
	myVars.races.race22.varType="@varType@";
	myVars.races.race22.repairType = "@RepairType";
	myVars.races.race22.event1={};
	myVars.races.race22.event2={};
	myVars.races.race22.event1.id = "navi-diversity";
	myVars.races.race22.event1.type = "onmouseover";
	myVars.races.race22.event1.loc = "navi-diversity_LOC";
	myVars.races.race22.event1.isRead = "True";
	myVars.races.race22.event1.eventType = "@event1EventType@";
	myVars.races.race22.event2.id = "Lu_window";
	myVars.races.race22.event2.type = "onload";
	myVars.races.race22.event2.loc = "Lu_window_LOC";
	myVars.races.race22.event2.isRead = "False";
	myVars.races.race22.event2.eventType = "@event2EventType@";
	myVars.races.race22.event1.executed= false;// true to disable, false to enable
	myVars.races.race22.event2.executed= false;// true to disable, false to enable
</script>

