<script id = "race16b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race16={};
	myVars.races.race16.varName="newsStoryCont-navi-2__onclick";
	myVars.races.race16.varType="@varType@";
	myVars.races.race16.repairType = "@RepairType";
	myVars.races.race16.event1={};
	myVars.races.race16.event2={};
	myVars.races.race16.event1.id = "newsStoryCont-navi-2";
	myVars.races.race16.event1.type = "onclick";
	myVars.races.race16.event1.loc = "newsStoryCont-navi-2_LOC";
	myVars.races.race16.event1.isRead = "True";
	myVars.races.race16.event1.eventType = "@event1EventType@";
	myVars.races.race16.event2.id = "Lu_DOM";
	myVars.races.race16.event2.type = "onDOMContentLoaded";
	myVars.races.race16.event2.loc = "Lu_DOM_LOC";
	myVars.races.race16.event2.isRead = "False";
	myVars.races.race16.event2.eventType = "@event2EventType@";
	myVars.races.race16.event1.executed= false;// true to disable, false to enable
	myVars.races.race16.event2.executed= false;// true to disable, false to enable
</script>

