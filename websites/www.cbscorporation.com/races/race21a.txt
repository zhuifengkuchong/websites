<script id = "race21a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race21={};
	myVars.races.race21.varName="Object[4104].triggered";
	myVars.races.race21.varType="@varType@";
	myVars.races.race21.repairType = "@RepairType";
	myVars.races.race21.event1={};
	myVars.races.race21.event2={};
	myVars.races.race21.event1.id = "Lu_window";
	myVars.races.race21.event1.type = "onload";
	myVars.races.race21.event1.loc = "Lu_window_LOC";
	myVars.races.race21.event1.isRead = "False";
	myVars.races.race21.event1.eventType = "@event1EventType@";
	myVars.races.race21.event2.id = "navi-news";
	myVars.races.race21.event2.type = "onmouseover";
	myVars.races.race21.event2.loc = "navi-news_LOC";
	myVars.races.race21.event2.isRead = "True";
	myVars.races.race21.event2.eventType = "@event2EventType@";
	myVars.races.race21.event1.executed= false;// true to disable, false to enable
	myVars.races.race21.event2.executed= false;// true to disable, false to enable
</script>

