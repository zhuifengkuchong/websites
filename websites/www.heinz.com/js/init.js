function initPng()
{
	var images = document.getElementsByTagName("img");
	if (images){
		for (var i = 0; i < images.length; i++){
			if ((images[i].src.indexOf(".png")) != -1){
				var srcname = images[i].src.replace(new RegExp('(.*)\/(.*)?\.png'),"$1/$2");
				images[i].parentNode.style.display = "inline-block"
				images[i].style.visibility = "hidden";
				images[i].parentNode.style.filter = "progid:dximagetransform.microsoft.alphaimageloader(src='"+ srcname +".png',sizingmethod='crop');"
			}
		}
	}
}

if (window.attachEvent && !window.opera){
	window.attachEvent("onload",initPng);
}