<script id = "race24a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race24={};
	myVars.races.race24.varName="Lu_Id_a_25__onmouseover";
	myVars.races.race24.varType="@varType@";
	myVars.races.race24.repairType = "@RepairType";
	myVars.races.race24.event1={};
	myVars.races.race24.event2={};
	myVars.races.race24.event1.id = "Lu_DOM";
	myVars.races.race24.event1.type = "onDOMContentLoaded";
	myVars.races.race24.event1.loc = "Lu_DOM_LOC";
	myVars.races.race24.event1.isRead = "False";
	myVars.races.race24.event1.eventType = "@event1EventType@";
	myVars.races.race24.event2.id = "Lu_Id_a_25";
	myVars.races.race24.event2.type = "onmouseover";
	myVars.races.race24.event2.loc = "Lu_Id_a_25_LOC";
	myVars.races.race24.event2.isRead = "True";
	myVars.races.race24.event2.eventType = "@event2EventType@";
	myVars.races.race24.event1.executed= false;// true to disable, false to enable
	myVars.races.race24.event2.executed= false;// true to disable, false to enable
</script>

