function make_menu(ob,hor_vert,menu_level){
if (!ob && !ob.childNodes) return false;
if (!menu_level) {
        ob.style.position='absolute'
        ob.style.visibility='visible'
        }
var dx=0;
	for  (var i=0;i<ob.childNodes.length;i++){
         var ob_in=ob.childNodes[i];
		 if (hor_vert && ob_in.style) {
			ob_in.style.position='absolute';
			if (dx) {
				ob_in.style.left=ob_in.style.left+dx+'px';
			}
			dx+=ob_in.childNodes[0].offsetWidth;
         }

		if (ob_in.getElementsByTagName && ob_in.getElementsByTagName("div").item(0)) {
			ob_menu=ob_in.getElementsByTagName("div").item(0);
			ob_menu.style.position='absolute';
			ob_menu.style.visibility='hidden';
			
			//correction position
			
			if (hor_vert) {
					ob_menu.style.left=0+'px';
					ob_menu.style.top=ob_in.offsetHeight+'px';
					}
			else {
					ob_menu.style.left=ob_in.childNodes[0].offsetWidth+'px';
					ob_menu.style.top=ob_in.offsetTop+'px';
					}

			ob_in.onmouseover=function() {
					this_object=this.getElementsByTagName("div").item(0);
					this_object.style.visibility='visible';
					}

			ob_in.onmouseout=function(){
					this_object=this.getElementsByTagName("div").item(0);
					this_object.style.visibility='hidden';
					}
			
			make_menu(ob_menu,0,menu_level+1);
		}
	} // end for
}

function create_wsc_menu() {
        if (document.getElementById('wsc-vert-menu'))
                make_menu(document.getElementById('wsc-vert-menu'),0,0);
        if (document.getElementById('wsc-hor-menu'))
                make_menu(document.getElementById('wsc-hor-menu'),1,0);
}