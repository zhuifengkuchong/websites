$(document).ready(function () {

    var newCategorySelected = undefined;
    var newDateTimeSelected = undefined;

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var min = today.getMinutes();

    if (dd < 10) {
        dd = '0' + dd  //Force 2 digit by adding 0 to the left
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    if (hh < 10) {
        hh = '0' + hh
    }

    if (min < 10) {
        min = '0' + min
    }

    today = yyyy + '/' + mm + '/' + dd + " " + hh + ":" + min;

    $("#newShowMore").hide();

    $("#newCategoryFilter").on("change", function (event) {
        newCategorySelected = "";
        listNews(event, true);
    });

    $("#newDateTimeFilter").on("change", function (event) {
        listNews(event, true);
    });

    $("#newShowMore").on("click", function (event) {
        listNews(event, false);
    });

    $("#newSearchInput").on("input", function (event) {
        filterListedNews(event);
    });

    $("#newSearch").on("click", function (event) {
        filterListedNews(event);
    });

    function listNews(event, first) {

        if (event != null) {
            event.preventDefault();
        }

        var iterationNumber;
        if (first) {
            iterationNumber = 0;
        } else {
            iterationNumber = $("#newList").data("iterationnumber");
            iterationNumber++;
        }
        $("#newList").data("iterationnumber", iterationNumber);

        var newDateTime = $("#newDateTimeFilter").val();
        if (!newDateTime || 0 === newDateTime.trim().length) {
            return;
        }

        var newCategory = $("#newCategoryFilter option:selected").val();

        if (newCategory == newCategorySelected && newDateTime == newDateTimeSelected && iterationNumber == 0) {
            return;
        }

        newCategorySelected = newCategory;
        newDateTimeSelected = newDateTime;

        var itemId = $("#newCategoryFilter").data("item-id");

        var dataJson = { 'itemId': itemId, 'newCategory': newCategory, 'newDateTime': newDateTime, 'iterationNumber': iterationNumber };

        $.ajax({
            type: 'POST',
            url: '/api/sitecore/NewsList/ListNews',
            dataType: 'json',
            data: JSON.stringify(dataJson),
            contentType: "application/json; charset=utf-8",
            context: this,
            success: function (response) {

                // List of News
                var arrayResponse = $.parseJSON(response);

                var news = [];

                if (first) {
                    $("#newList").empty();
                }

                // Loop through each New and create the node
                $.each(arrayResponse, function (index, new1) {
                    var liClass = "newListed" + (new1.Image != "" ? "" : " newListed-image");
                    var descriptionShort = new1.Description.length > 50 ? new1.Description.substring(0, 50) + "..." : new1.Description;
                    news.push(
                        "<li class='" + liClass + "' data-title='" + new1.Title + "'>" +
                        "    " + (new1.Image != "" ? "<div class='imageEvent'><img src='" + new1.Image + "'/>" + (descriptionShort != "" ? "<div class='imageEventTitle'>" + descriptionShort + "</div>" : "") + "</div>" : "") +
                        "    <div class='infoEvent'>" +
                        "        <h5>  <a href='" + new1.LinkUrl + "' target='_parent'>" + new1.Title + "</a> </h5>" +
                        "        <span>" + new1.DateTime + "</span>" +
                        "        <p>" +
                        "            " + new1.Description +
                        "        </p>" +
                        "        <div class='social'>" +
                        "            <script type='text/javascript' src='//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54be6f6551f8aaca' async='async'></script>" +
                        "        </div>" +
                        "    </div>" +
                        "</li>"
                    );
                });

                // Add News to list node
                $("#newList").append(news);

                // Show or hide the Show More button
                if (arrayResponse.length == $("#newList").data("maximumnumberofnews")) {
                    $("#newShowMore").show();
                } else {
                    $("#newShowMore").hide();
                }

            },
            error: function () {
                alert('The news could not be loaded.');
            }
        });

    } // End function listNews()

    function filterListedNews(event) {

        event.preventDefault();

        var newSearchInput = $("#newSearchInput").val().trim();

        if (!newSearchInput || 0 === newSearchInput.length || $("#newSearchInput").data("value") === newSearchInput) {
            $(".newListed").show();
        } else {
            $(".newListed").hide();
            $(".newListed").filter(function () { return $(this).data("title").toLowerCase().indexOf(newSearchInput.toLowerCase()) > -1; }).show();
        }

    } // End function filterListedNews(event)

    window.setTimeout(showEvents, 0001);
    function showEvents() {
        $("#newDateTimeFilter").val(today);
        $("#newDateTimeFilter").blur();
        //$("#newSearchInput").focus();
        $("#newSearchInput").blur();
        listNews(null, true);
    }

}); // End $(document).ready(...)
