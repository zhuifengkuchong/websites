$(document).ready(function () {

    $("div.storySelector > ul > li > a.storySelector").on("click", function (event) {
        event.preventDefault();
        $(".storySelector").parent().removeClass("selected");
        $(this).parent().addClass("selected");
        var storyType = $(this).data("story-type");

        if (category == null) {
            category = '';
        }



        var dataJson = { 'filter': storyType, 'count': 0, 'category': category };

        $.ajax({
            type: 'POST',
            url: '/api/sitecore/StoriesProvider/GetStories',
            dataType: 'json',
            data: JSON.stringify(dataJson),
            contentType: "application/json; charset=utf-8",
            context: this,
            success: function (response) {

               
                var stories = [];
              

                $.each($.parseJSON(response), function (index, story) {

  if (readMoreText == null) {
                    readMoreText = story.LinkText;
                }
                    stories.push(
                        "<div class='storyListItem'>" +
                        "    <div class='newsBandimg'>" +
                        "        <img src='" + story.Image + "' />" +
                            "            <a href='" + story.LinkUrl + "' target='_parent'>" + story.Title + "</a>" +
                       "    </div>" +
                        "    <div class='newsItemsDescription'>" +
                        "        <h3>" + story.Title + "</h3>" +
                        "        <p>" + story.Description +
                        "            <a href='" + story.LinkUrl + "' target='_parent'>" + readMoreText + "</a>" +
                        "        </p>" +
                        "    </div>" +
                        "</div>"
                    );
                });

                $("#storyList").empty().append(stories);

            },
            error: function () {
                alert('The stories could not be loaded.');
            }
        });

    }); // End $(".storySelector").on("click",...)

    

}); // End $(document).ready(...)
