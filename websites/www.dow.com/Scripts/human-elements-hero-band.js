$(document).ready(function () {
    $("#eventCategoryFilter").on("change", function (event) {
        listHeroes(event, true);
    });


    function listHeroes(event, first) {

        if (event !== null) {
            event.preventDefault();
        }

        var newMarket = $("#eventCategoryFilter option:selected").val();

        var itemId = $("#eventCategoryFilter").data("item-id");

        var dataJson = { 'itemId': itemId, 'newMarket': newMarket };

        $.ajax({
            type: 'POST',
            url: '/api/sitecore/HumanElementsHeroBand/Heroes',
            dataType: 'json',
            data: JSON.stringify(dataJson),
            contentType: "application/json; charset=utf-8",
            context: this,
            success: function (response) {

                // List of News
                var arrayResponse = $.parseJSON(response);

                var heroesList = [];

                // Loop through each New and create the node
                $.each(arrayResponse, function (index, heroe) {
                    heroesList.push(
                        "<li>" +
                        "   <div class='infobox'>" +
                        "       <div class='imageContent'>" +
                        "           <img src='" + heroe.Image + "'/>" +
                        "       </div>" +
                        "       <div class='more'>" +
                        "           <div class='moreContent'>" +
                        "               <h4>" + heroe.Title + "</h4>" +
                        "               <p>" + heroe.Description + "</p>" +
                        "           </div>" +
                        "       </div>" +
                        "       <a href='" + heroe.LinkUrl + "'></a>" +
                        "   </div>" +
                        "</li>"
                    );
                });

                $("#heroUl").empty().append(heroesList);

                for (i = 0; i < $('.contentGrid').children().length; i++) {
                    if (i > 7) {
                        $($('.contentGrid').children()[i]).addClass('hidden');
                        $('.btnShowMore').removeClass('hidden');
                    }
                    updatePages();
                }
                
                if ($('.contentGrid').children().length <= 8 && $('.contentGrid').children().length >= 1) {
                    $('.btnShowMore').hide();
                    $('.pages p').show();
                }
                else {
                    $('.btnShowMore').show();
                    $('.pages p').show();
                }

                if ($('.contentGrid').children().length == 0) {
                    $('.pages p').hide();
                    $('.btnShowMore').hide();
                }
                
            },
            error: function () {
                alert('The news could not be loaded.');
            }

        });

    }

    function updatePages() {
        var totalItems = $('.contentGrid').children().length;
        var shownItems = $('.contentGrid').children().not('.hidden').length;
        
        $('.pages p').text('Showing 1-' + shownItems + ' of ' + totalItems);
    }
});