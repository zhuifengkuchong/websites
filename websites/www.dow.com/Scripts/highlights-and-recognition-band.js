$(document).ready(function () {
    
    showAwardsOfTheYear();

    $("#filterSelect").on("change", function () {
        showAwardsOfTheYear();
    });

    function showAwardsOfTheYear() {
        var year = $("#filterSelect option:selected").val();
        $(".awardItem").hide();
        $(".awardItem[data-year='" + year + "']").show();
    }

});
