$(document).ready(function () {
    
    function setContactUsFormSelect(value) {
        if (value == null || $("#contactUsFormSelect").find("[value='" + value + "']").length == 0) {
            value = "";
        }
        $("#contactUsFormSelect")
            .val(value)
            .removeAttr("selected")
            .find("[value='" + value + "']")
            .attr("selected", true)
            .change();
    }

    setContactUsFormSelect(window.location.pathname + window.location.search);

    $("#contactUsFormSelect").on("change", function (e) {
        e.preventDefault();
        var url = $("#contactUsFormSelect option:selected").val();
        window.location.replace(url);
    });

    $(".btnContentNext button").on("click", function (e) {
        e.preventDefault();
        var container = $(this).parents(".contactusForm").first();
        var id = container.data("id");
        var step = container.data("step");
        var nextstep = step == 0 ? 1 : 0;
        container.hide();
        $("div.contactusForm[data-id='" + id + "'][data-step='" + nextstep + "']").show();
    });
    
});
