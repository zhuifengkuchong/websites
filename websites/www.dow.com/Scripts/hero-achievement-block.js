$(document).ready(function () {

    $(".tabsMenuRef a").click(function (event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tabItemRef").not(tab).css("display", "none");
        $(tab).fadeIn();

        var number = $(this).data("number");
        $(".tabImageItemRef").hide();
        $(".tab-image-" + number).show();
    });

});
