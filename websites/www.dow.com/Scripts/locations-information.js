$(document).ready(function () {

    var regionParameter = getUrlParameter("region");
    var countryParameter = getUrlParameter("country");

    if (regionParameter != null || countryParameter != null) {
        if (regionParameter != null) {
            $(".dropdownRegion select").val(regionParameter).removeAttr("selected").find("[value='" + regionParameter + "']").attr("selected", true).change();
        }
        if (countryParameter != null) {
            $(".dropdownCountry select").val(countryParameter).removeAttr("selected").find("[value='" + countryParameter + "']").attr("selected", true).change();
        }
        listLocations(null, $(".locationsGo"));
    }

    $(".locationsGo").on("click", function (event) {
        event.preventDefault();
        var targetUrl = $(".targetUrl").first().val();
        if (targetUrl == null) {
            targetUrl = '';
        }
        if ($(this).closest(".locationsInformationContent").length === 0) {
            var region = $(this).parent().parent().find(".dropdownRegion select option:selected").val();
            if (region == undefined || region == null) {
                region = "";
            }
            var country = $(this).parent().parent().find(".dropdownCountry select option:selected").val();
            if (country == undefined || country == null) {
                country = "";
            }
            window.location.href = getNewHref(targetUrl, "region=" + region + "&country=" + country);
        }
        else {
            listLocations(event, $(this));
        }
    });

    function listLocations(event, thisElement) {

        if (event != null) {
            event.preventDefault();
        }

        var region = thisElement.parent().parent().find(".dropdownRegion select option:selected").val();
        var country = thisElement.parent().parent().find(".dropdownCountry select option:selected").val();

        if ((region == null || region == "") && regionParameter != null && regionParameter != "") {
            region = regionParameter;
            regionParameter = "";
        }
        if ((country == null || country == "") && countryParameter != null && countryParameter != "") {
            country = countryParameter;
            countryParameter = "";
        }

        var dataJson = { 'region': region, 'country': country };

        $.ajax({
            type: 'POST',
            url: '/api/sitecore/DowOfficeInfo/ListLocations',
            dataType: 'json',
            data: JSON.stringify(dataJson),
            contentType: "application/json; charset=utf-8",
            context: this,
            success: function (response) {

                // List of Locations returned
                var arrayResponse = $.parseJSON(response);

                // Node in where load the list of Locations
                var locationsList = $("#locationsList");

                // Clear node of list of Locations
                locationsList.empty();

                if (arrayResponse.length > 0) {

                    var countryTag;
                    var stateTag;
                    var cityAndOfficeTag;
                    var tmpTag1;
                    var tmpTag2;
                    var tmpTag3;
                    var tmpTag4;
                    var previousCountry = arrayResponse[0].CountryTitle + "@";
                    var previousState;

                    // Loop through each Location, and load it into the node of list of Locations
                    $.each(arrayResponse, function (index, location) {

                        // If it is an Office of a new Country, then create a node
                        if (location.CountryTitle != previousCountry) {

                            previousCountry = location.CountryTitle;
                            previousState = location.StateTitle + "@";

                            countryTag = $("<div class='dowOfficeInfoContent'></div>");
                            countryTag.appendTo(locationsList);

                            tmpTag1 = $("<div class='level1'></div>");
                            tmpTag1.appendTo(countryTag);

                            tmpTag2 = $("<div class='level1Content'></div>");
                            tmpTag2.appendTo(tmpTag1);

                            tmpTag3 = $("<h3>" + location.CountryTitle + "</h3>");
                            tmpTag3.appendTo(tmpTag2);

                            if (location.CountryLinkText != "") {

                                tmpTag3 = $("<div class='link'>");
                                tmpTag3.appendTo(tmpTag2);

                                tmpTag4 = $("<p>" + location.CountryLinkText + "</p>");
                                tmpTag4.appendTo(tmpTag3);

                                tmpTag4 = $("<a href='" + location.CountryLinkUrl + "'>English</a>");
                                tmpTag4.appendTo(tmpTag3);

                            }

                        }

                        // If it is an Office of a new State, then create a node
                        if (location.StateTitle != previousState) {

                            previousState = location.StateTitle;

                            stateTag = $("<div class='level2'></div>");
                            stateTag.appendTo(countryTag);
                            
                            tmpTag1 = $("<div class='level2Content'></div>");
                            tmpTag1.appendTo(stateTag);

                            if (location.StateLinkText != "") {

                                tmpTag2 = $("<h4>" + location.StateTitle + "</h4>");
                                tmpTag2.appendTo(tmpTag1);

                                tmpTag2 = $("<div class='link'></div>");
                                tmpTag2.appendTo(tmpTag1);

                                tmpTag3 = $("<p>" + location.StateLinkText + "</p>");
                                tmpTag3.appendTo(tmpTag2);

                                tmpTag3 = $("<a href='" + location.StateLinkUrl + "'>English</a>");
                                tmpTag3.appendTo(tmpTag2);

                            }

                        }

                        // Create the City and Office node
                        cityAndOfficeTag = $("<div class='level3'></div>");
                        cityAndOfficeTag.appendTo(stateTag);

                        tmpTag1 = $("<div class='level3Content row'></div>");
                        tmpTag1.appendTo(cityAndOfficeTag);

                        tmpTag2 = $("<div class='level3InnerContent title col-xs-12 col-md-4'></div>");
                        tmpTag2.appendTo(tmpTag1);

                        tmpTag3 = $("<h5>" + location.CityTitle + "</h5>");
                        tmpTag3.appendTo(tmpTag2);

                        tmpTag3 = $("<p>" + location.CityDescription + "</p>");
                        tmpTag3.appendTo(tmpTag2);

                        tmpTag2 = $("<div class='level3InnerContent info col-xs-12 col-md-4'></div>");
                        tmpTag2.appendTo(tmpTag1);

                        tmpTag3 = $("<p>" + location.OfficeTitle + "</p>");
                        tmpTag3.appendTo(tmpTag2);

                        tmpTag3 = $("<p>" + location.OfficeAddress + "</p>");
                        tmpTag3.appendTo(tmpTag2);

                        tmpTag2 = $("<div class='level3InnerContent contact'></div>");
                        tmpTag2.appendTo(tmpTag1);

                        tmpTag3 = $("<div class='level3Contact'></div>");
                        tmpTag3.appendTo(tmpTag2);
                        
                        if (location.OfficeTelephone != "")
                        {
                            if (location.OfficeTelephoneText != "")
                            {
                                tmpTag4 = $("<p> " + location.OfficeTelephoneText + ": " + location.OfficeTelephone + "</p>");
                                tmpTag4.appendTo(tmpTag3);
                            }
                            else {
                                tmpTag4 = $("<p>Tel: " + location.OfficeTelephone + "</p>");
                                tmpTag4.appendTo(tmpTag3)
                            }                            
                        }                        
                        
                        tmpTag3 = $("<div class='level3Contact'></div>");
                        tmpTag3.appendTo(tmpTag2);

                        if (location.OfficeFax != "" )
                        {
                            if (location.OfficeFaxText != "")
                            {
                                tmpTag4 = $("<p>" + location.OfficeFaxText + ": " + location.OfficeFax + "</p>");
                                tmpTag4.appendTo(tmpTag3);
                            }
                            else {
                                tmpTag4 = $("<p>Fax: " + location.OfficeFax + "</p>");
                                tmpTag4.appendTo(tmpTag3);
                            }
                        }                       

                    }); // End $.each(arrayResponse, ...)

                } // End if (arrayResponse.length > 0)

            },
            error: function () {
                alert('The locations could not be loaded.');
            }

        }); // End $.ajax(...)

    } // End function listLocations(event, first)

}); // End $(document).ready(...)
