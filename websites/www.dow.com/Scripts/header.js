$(document).ready(function () {

    var url = location.pathname.toLowerCase();

    $("nav.menuContentNav > ul > li > a").each(function () {
        var attrHref = $(this).attr("href").toLowerCase();
        var existsUrl = url.indexOf(attrHref) >= 0;
        if (existsUrl) {
            $(this).parent().addClass("selected").siblings().removeClass("selected");
        }
    });

});
