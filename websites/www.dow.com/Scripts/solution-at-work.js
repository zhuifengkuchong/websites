$(document).ready(function () {

    if ($('#productVariant').length === 0) { // Temp "if" to Product Variant rendering
        $(".triggerRef").eq(0).addClass("activeItemList");
    }

    $(".triggerRef").on("click", function (e) {
        
        if ($('#productVariant').length === 0) { // Temp "if" to Product Variant rendering
            e.preventDefault();
            $(".triggerRef").removeClass("activeItemList");
            $(this).addClass("activeItemList");
            var panels = $(".containerSolutionRef");
            var panelID = $(this).index();
            panels.hide();
            $(".containerSolutionRef").eq(panelID).show();
        }

    });

});