$(document).ready(function () {
    $("#marketsDropdown").on("change", function (event) {
        listHeroes(event, true);
    });


    function listHeroes(event, first) {

        if (event !== null) {
            event.preventDefault();
        }

        var newMarket = $("#marketsDropdown option:selected").val();

        var itemId = $("#marketsDropdown").data("item-id");

        var dataJson = { 'itemId': itemId, 'newMarket': newMarket };

        $.ajax({
            type: 'POST',
            url: '/api/sitecore/HEHCarousel/Heroes',
            dataType: 'json',
            data: JSON.stringify(dataJson),
            contentType: "application/json; charset=utf-8",
            context: this,
            success: function (response) {

                // List of News
                var arrayResponse = $.parseJSON(response);

                var heroesList = [];

                // Loop through each New and create the node
                $.each(arrayResponse, function (index, new1) {
                    heroesList.push(
                        "<li class='itemRef'>" +                    
                        "   <div class='itemContent'>" +
                        "       <img alt='' src='" + new1.Image + "'/>" +
                        "       <a href='" + new1.LinkUrl + "'></a>" +    
                        "   </div>" +
                        "</li>"
                    );
                });

                $("#heroUl").empty().append(heroesList);
                slideaction(1);

            },
            error: function () {
                alert('The news could not be loaded.');
            }
        });

    }
});