$(document).ready(function () {
    
    $(".facetSelector").on("click", function (event) {
        filterMediaType(event, $(this).data("title"));
    });

    $("#mediaGalleryCategoryFilter").on("change", function (event) {
        listMedia(event);
    });

    $("#mediaGalleryTimeFilter").on("change", function (event) {
        listMedia(event);
    });

    $("#mediaSearchInput").on("input", function (event) {
        filterlistedMedia(event);
        updateFeaturedMediasListVisualization();
        updateMediaCategoryRowListVisualization();
    });

    $("#mediaSearch").on("click", function (event) {
        filterlistedMedia(event);
        updateFeaturedMediasListVisualization();
        updateMediaCategoryRowListVisualization();
    });

    // Show/Hide Video and/or Image sections according by selection (All/Videos/Images)
    function filterMediaType(event, title) {

        event.preventDefault();

        if (title == undefined || title == "" || title == "All") {
            $(".mediaGalleryTop").show();
        }
        else
        {
            $(".mediaGalleryTop").hide();
            $(".mediaGalleryTop." + title.toLowerCase()).show();
        }

    } // End function filterMediaType(event, object)

    // Refresh Video and Image lists according by Category and Time filters
    function listMedia(event) {
        
        event.preventDefault();
        
        // Update "Featured Videos" (AJAX Call 1) and "Featured Images" (AJAX Call 2)
            
        var categoryItemsId = $(".featuredVideosList").data("item-id");
        var mediaCategory = $("#mediaGalleryCategoryFilter option:selected").val();
        var mediaDays = $("#mediaGalleryTimeFilter option:selected").val();
        var mediasType = ["video", "image"];
        var isFeatured = "true";

        $.each(mediasType, function(index, mediaType) {

            var dataJson = { 'categoryItemsId': categoryItemsId, 'mediaCategory': mediaCategory, 'mediaDays': mediaDays, 'mediaType': mediaType, 'isFeatured': isFeatured};
    
            $.ajax({
                type: 'POST',
                url: '/api/sitecore/MediaGallery/ListMedias',
                dataType: 'json',
                data: JSON.stringify(dataJson),
                contentType: "application/json; charset=utf-8",
                context: this,
                success: function (response) {

                    var arrayResponse = $.parseJSON(response);

                    var medias = [];
                    var ulClass;

                    // Loop through each Media and create the li node
                    if (mediaType == "video") {
                        ulClass = "featuredVideosList";
                        $.each(arrayResponse, function (index, media) {
                            medias.push(
                                "<li class='mediaGalleryListed featured " + mediaType + "' data-title='" + media.Title + "'>" +
                                "	<div class='itemContent'>" +
                                "		<a href='" + media.LinkUrl + "' target='_parent'>" + media.LinkText + "<img src='" + media.Image + "?h=164&amp;la=en&amp;w=250' alt='videoplaceholder' width='250' height='164'/></a>" +
                                "		<div class='infoItem'>" +
                                "			<h3>" + media.Title + "</h3>" +
                                "			<p>" + media.Uploader + "</p>" +
                                "			<p class='time'>" + media.Length + "</p>" +
                                "		</div>" +
                                "	</div>" +
                                "</li>"
                            );
                        });
                    }
                    else
                    {
                        ulClass = "featuredImagesList";
                        $.each(arrayResponse, function (index, media) {
                            medias.push(
                                "<li class='mediaGalleryListed containerRef featured " + mediaType + "' data-title='" + media.Title + "'>" +
                                "	<div class='itemContent openModalRef' data-type='image'>" +
                                "		<img src='" + media.Image + "?h=294&amp;la=en&amp;w=679' alt='' width='679' height='294'/>" +
                                "		<div class='infoItem'>" +
                                "			<h3 class='titleRef'>" + media.Title + "</h3>" +
                                "		</div>" +
                                "	</div>" +
                                "</li>"
                            );
                        });
                    }

                    // Add the li nodes to the ul node
                    $("." + ulClass).empty().append(medias);
                    
                    // Updated visualization according by filter
                    filterlistedMedia(event);
                    updateFeaturedMediasListVisualization();

                },
                error: function () {
                    alert('The featured medias could not be loaded.');
                }
            });

        }); // End $.each(mediasType, function(index, mediaType)

        // Update "Video Category Row" (AJAX Call 3) and "Image Category Row" (AJAX Call 4)

        categoryItemsId = $("#mediaGalleryCategoryFilter").data("items-id");
        mediaCategory = $("#mediaGalleryCategoryFilter option:selected").val();
        mediaDays = $("#mediaGalleryTimeFilter option:selected").val();
        mediasType = ["video", "image"];
        isFeatured = "false";

        $.each(mediasType, function(index, mediaType) {

            var dataJson = { 'categoryItemsId': categoryItemsId, 'mediaCategory': mediaCategory, 'mediaDays': mediaDays, 'mediaType': mediaType, 'isFeatured': isFeatured};
    
            $.ajax({
                type: 'POST',
                url: '/api/sitecore/MediaGallery/ListMedias',
                dataType: 'json',
                data: JSON.stringify(dataJson),
                contentType: "application/json; charset=utf-8",
                context: this,
                success: function (response) {

                    var arrayResponse = $.parseJSON(response);

                    var isVideo = mediaType == "video";
                    var ulClass = isVideo ? "videoCategoryRowList" : "imageCategoryRowList";
                    var liClass = isVideo ? "mediaGalleryRowVideo" : "mediaGalleryRowImage";

                    // Empty list node
                    $("." + ulClass).empty();

                    // Loop through each Media, create the li node and add it to the ul node
                    $.each(arrayResponse, function (index, media) {
                        $("." + ulClass).filter(function() { return $(this).data("id") === media.Category; }).append(
                            "<li class='mediaGalleryListed itemRef " + mediaType + " " + liClass + "' data-title='" + media.Title + "' style='width: 419px;'>" +
                            "	<div class='itemContent'>" +
                            "		<img src='" + media.Image + "' alt='placeholder' width='250' height='164'/>" +
                            "       <a href='" + media.LinkUrl + "' target='_parent'>" + media.LinkText + "</a>" +
                            "		<p class='titleRef'>" + media.Title + "</p>" +
                            "	</div>" +
                            "</li>"
                        );
                    });

                    // Updated visualization according by filter
                    filterlistedMedia(event);
                    updateMediaCategoryRowListVisualization();

                },
                error: function () {
                    alert('The media category row could not be loaded.');
                }
            });

        }); // End $.each(mediasType, function(index, mediaType)

    } // End function listMedia(event)

    // Show/Hide Videos and Images according by search box filter
    function filterlistedMedia(event) {

        event.preventDefault();

        var mediaSearchInput = $("#mediaSearchInput").val().trim();

        if (!mediaSearchInput || 0 === mediaSearchInput.length || $("#mediaSearchInput").data("value") === mediaSearchInput) {
            $(".mediaGalleryListed").show();
        } else {
            $(".mediaGalleryListed").hide();
            $(".mediaGalleryListed[data-title*='" + mediaSearchInput + "']").show();
        }

        $(".mediaGalleryListed.featured").removeClass("highlight");
        $(".mediaGalleryListed.video.featured:visible").first().addClass("highlight");
        $(".mediaGalleryListed.image.featured:visible").first().addClass("highlight");

    } // End function filterlistedMedia(event)

    // Show/Hide categories of the featured media according if its contains any media
    function updateFeaturedMediasListVisualization() {

        var ulTags = ["featuredVideosList", "featuredImagesList"];

        $.each(ulTags, function (index, ulTag) {
            if ($("." + ulTag + " > li").length > 0) {
                $("." + ulTag).parent().parent().show();
            }
            else {
                $("." + ulTag).parent().parent().hide();
            }
        });

    } // End updateFeaturedMediasListVisualization()

    // Show/Hide categories of the media category row according if its contains any media
    function updateMediaCategoryRowListVisualization() {

        var ulTags = ["videoCategoryRowList", "imageCategoryRowList"];

        $.each(ulTags, function (index, ulTags2) {
        
            var ulTags2DataId = [];
        
            $.each($("." + ulTags2).toArray(), function (index, ulTag) {
				ulTags2DataId.push(ulTag.dataset.id);
            });
			
            $.each(ulTags2DataId, function (index, ulTagDataId) {
				var ulTag = $("." + ulTags2).filter(function() { return $(this).data("id") === ulTagDataId; });
                if (ulTag.children("li").length > 0) {
                    ulTag.parent().parent().show();
                }
                else
                {
                    ulTag.parent().parent().hide();
                }
            });
        
        });

    } // End function updateMediaCategoryRowListVisualization()

}); // End $(document).ready(...)
