
Date.prototype.getMonthName = function (lang) {
    lang = lang && (lang in Date.locale) ? lang : 'en';
    return Date.locale[lang].month_names[this.getMonth()];
};

Date.prototype.getMonthNameShort = function (lang) {
    lang = lang && (lang in Date.locale) ? lang : 'en';
    return Date.locale[lang].month_names_short[this.getMonth()];
};

Date.locale = {
    en: {
        month_names: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        month_names_short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    }
};

function pad(number, size) {
    var numberPadded = number + "";
    while (numberPadded.length < size) {
        numberPadded = "0" + numberPadded;
    }
    return numberPadded;
}

function getUrlParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
}

function getUrlHash() {
    var hash = location.hash;
    return hash.indexOf('#') == -1 ? "" : hash.substring(parseInt(hash.indexOf('#')) + 1);
}

function getNewHref(newPathname, newParameters) {
//function getNewHref(newPathname, extraParameters) {

    var origin = window.location.protocol + "//" + window.location.hostname;

    var port = window.location.port ? ":" + window.location.port : "";

    //var originalParameters = window.location.href.indexOf("?") > -1 ? window.location.href.substr(window.location.href.indexOf("?") + 1) : "";
    //var parameters = "";
    //var parametersSeparator = "?";
    //if (extraParameters.length > 0) {
    //    parameters += parametersSeparator + extraParameters;
    //    parametersSeparator = "&";
    //}
    //if (originalParameters.length > 0) {
    //    parameters += parametersSeparator + originalParameters;
    //}
    var parameters = (newParameters != null && newParameters != "") ? "?" + newParameters : "";

    return origin + port + newPathname + parameters;

}
