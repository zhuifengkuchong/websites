$(document).ready (function(){
    // search page tabs
    $(".tabsFilterContent ul li").eq(0).addClass("selected");
    $(".searchTools a").on("click", function(event){
        event.preventDefault();
        var selectedItem = $(".tabsFilterContent ul li.selected").index();
        if(selectedItem >= 0){
            showTools(selectedItem);
        }
        else{
            showTools(0);
        }
    });
    
    $(".tabsFilterContent ul li").on("click", function(event){
        $(".tabsFilterContent ul li").removeClass("selected");
        $(this).addClass("selected");
        var selectedItem = $(this).index();
        if( !($(".filters").is(":hidden")) ){
            showTools(selectedItem);
        }
    });
    
    $(".clearAll a, .hideSearchTools a").on("click", function(event){
        event.preventDefault();
        $(".filters").slideUp( "slow", function() {
            $(".filtersContent").hide();
            $(".searchTools").show(); //show link
            $(".hideSearchTools").hide(); //hide link
        });
    });
});

function showTools(selectedItem){
    $(".searchTools").hide();//hide link
    $(".hideSearchTools").show();//show link
    $(".filtersContent").hide();
    $(".filters").hide();
    $(".filtersContent").eq(selectedItem).show();
    $(".filters").slideDown( "slow", function() {
    });
}
