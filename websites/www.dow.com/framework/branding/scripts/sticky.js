var heightHeader = 0;
function sizeHeader() {
    heightHeader = $(".globalHeader").outerHeight();
    $("body").css({ "padding-top": heightHeader });
}
function clearsizeHeader() {
    $("body").css({ "padding-top": "" });
}

$(document).ready(function () {
    if ($(window).width() > 767) {
        sizeHeader();
        clearStickys();
        setwaypoints();
    }
    $(window).resize(function () {
        if ($(window).width() > 767) {
            sizeHeader();
            clearStickys();
            setwaypoints();
        } else {
            clearsizeHeader();
            clearStickys();
        }
    });
});

$(window).load(function () {
    if ($(window).width() > 767) {
        sizeHeader();
        clearStickys();
        setwaypoints();
    } else {
        clearsizeHeader();
        clearStickys();
    }
});

function clearStickys() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

    } else {
        if ($('.stykyRef').length > 0) {
            $(".stykyNew").remove();
            $("#divFixEmpty0").waypoint(function (direction) { this.disable });//reset
            $("#divFixEmpty1").waypoint(function (direction) { this.disable });//reset
            $("#divFixEmpty0").waypoint(function (direction) { this.destroy });//reset
            $("#divFixEmpty1").waypoint(function (direction) { this.destroy });//reset
            $(".stykyRef").css({ "position": "", "top": "" });//reset
            $(".globalHeader").removeClass("disableNotch");
        }
    }
}

//fixed searchBand and breadcrumb
function setwaypoints() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

    } else {
        if ($('.stykyRef').length > 0) {
            $('.stykyRef').each(function (index) {
                $(this).after("<div class='stykyNew' style='position: relative; float: left; width:100%; height:0px' id='divFixEmpty" + index + "'></div>");
            });

            $('.stykyRef').each(function (index) {
                var elementStiky = $(this);
                var indice = index;
                var fixHeightActual = heightHeader;
                $('.stykyRef').each(function (index) {//sumo el alto de los elementos anteriores (+ header)
                    if (index < indice) {
                        fixHeightActual += $(this).outerHeight();
                    }
                });

                $("#divFixEmpty" + indice).waypoint(function (direction) {

                    if (direction === "up") {
                        elementStiky.css({ "position": "relative", "top": 0 });
                        $("#divFixEmpty" + indice).css({ "height": "0px" })
                        $(".globalHeader").removeClass("disableNotch");
                    }
                    if (direction === "down") {
                        elementStiky.css({ "position": "fixed", "top": fixHeightActual });
                        $("#divFixEmpty" + indice).css({ "height": elementStiky.outerHeight() });
                        $(".globalHeader").addClass("disableNotch");
                    }
                }, { offset: fixHeightActual });

            });
        }
    }

}

