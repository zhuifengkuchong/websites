(function (window, $) {

    'use strict';

    /**
     * The contact form, steps, controls.
     */
    var contactForm = $('#contactForm'),
        steps = contactForm.find('.steps .step'),
        behalfForm = contactForm.find('.on-behalf-form'),
        informationForm = contactForm.find('.contact-information-form'),
        htmlBody = $('html, body'),
        headerOffset = $('.globalHeader').outerHeight() + $('.breadcrumbRow').outerHeight(),
        captchaChecked = false;

    // Sitecore Bug - Form data doesn`t have a mechanism for custom form data.
    contactForm.prop({ enctype: 'multipart/form-data' });

    /**
     * Check if the form is for someone else, or for you.
     */
    contactForm.on('change', '.on-behalf-question :radio', function (e) {
        var input = $(e.currentTarget),
            parent = input.closest('.step');

        parent.hide();

        if (input.val().toLowerCase() === 'true') {
            behalfForm.show().find('.btn-back').data({ goto: 0 });
        } else {
            informationForm.show().find('.btn-back').data({ goto: 0 });

            // clear behalf form if no is clicked.
            behalfForm.find(':text, [data-type="email"]').val('');
            behalfForm.find('select').prop({ selectedIndex: 0 });
            behalfForm.find(':text, [data-type="email"], select').closest('.required').removeClass('has-error');
        }

        // clear input.
        input.prop({ checked: false });

        // scroll to the top of the step.
        stepScrollToTop();
    });

    /**
     * Change steps
     */
    contactForm.on('click', '.btn-change-step', function (e) {
        e.preventDefault();

        var button = $(e.currentTarget),
            currentStep = +button.closest('.step').data('step'),
            gotoStep = +button.data('goto'),
            gotoStepDom = contactForm.find('.step[data-step="' + gotoStep + '"]');

        // hide all steps.
        steps.hide();

        // show the target step.
        gotoStepDom.show();

        // scroll to the start of the step.
        stepScrollToTop();

        // change the back button step.
        if (currentStep === 1 && gotoStep === 2) {
            gotoStepDom.find('.btn-back').data({ goto: 1 });
        }

        stepProceed();
    });

    /**
     * Shiv for overnight shipping.
     */
    contactForm.on('change', '#SampleOverNightShipment', function (e) {
        var overnightDom = $('.nature-of-request-sample-section-overnight-shippment');

        $(e.currentTarget).val().toLowerCase() === 'true' ?
            overnightDom.show() :
            overnightDom.hide().find(':text').val('');
    });

    /**
     * On change of a checkbox, check for a section that matches the id of the checkbox.
     */
    contactForm.on('change', '.step :checkbox', function (e) {
        var checkbox = $(e.currentTarget),
            parent = checkbox.closest('.step'),
            section = checkbox.data('id') || checkbox.prop('id');

        // snake case the section and add -section from the end. convert to jquery object.
        section = convertToSnakeCase(section) + '-section';

        // convert to jquery object
        section = $('.' + section);

        // find the snake case section and show it if checked.
        if (checkbox.is(':checked')) {
            section.show().find('[class*="col-"]:not(.skip-require)').addClass('required');
        } else {
            if (!checkbox.hasClass('always-visible')) {
                section.hide();
                section.find(':text, [data-type="email"], [type="number"]').val('');
                section.find('select').prop({ selectedIndex: 0 });
                section.find(':checkbox').prop({ checked: false });
            }

            if (!contactForm.find('.always-visible:checked').length) {
                section.find('[class*="col-"]').removeClass('required');
                section.find('.has-error').removeClass('has-error');
            }
        }

        stepProceed();
    });

    /**
     * Validation on blur.
     */
    contactForm.on('blur', '.required:visible :text, .required:visible textarea, .required:visible [data-type="email"], .required:visible [type="number"], .required:visible select', function (e) {
        var visibleStep = contactForm.find('.step:visible'),
            control = $(e.currentTarget),
            parent = control.closest('.required'),
            isEmpty = !control.val().length;

        parent.removeClass('has-error')

        if (isEmpty) {
            parent.addClass('has-error');
        } else {
            // check if the input it an email.
            if (typeof control.data('type') !== 'undefined' && control.data('type').toLowerCase() === 'email') {
                if (!isValidEmail(control.val())) {
                    parent.addClass('has-error');
                } else {
                    // does email match confirm email?
                    var allEmails = visibleStep.find('.required [data-type="email"]');

                    allEmails.closest('.required').removeClass('has-error');

                    // if emails don't match add error to both email fields.
                    if (allEmails.eq(0).val() !== allEmails.eq(1).val()) {
                        allEmails.closest('.required').addClass('has-error');
                    }
                }
            }
        }

        stepProceed();
    });

    /**
     * Empty Input validation.
     */
    contactForm.on('keypress', '.required:visible :text, .required:visible textarea, .required:visible [data-type="email"], .required:visible [type="number"] .required:visible select', function () {
        stepProceed();
    });

    /**
     * Check the required section for input.
     */
    contactForm.on('change', '.required-section :checkbox:visible', function (e) {
        stepProceed();
    });

    /**
     * Convert camelcase to snakecase.
     */
    function convertToSnakeCase(str) {
        return str.replace(/([A-Z])/g, function (str) {
            return '-' + str.toLowerCase();
        }).substr(1);
    }

    /**
     * Scroll to the top of each step.
     */
    function stepScrollToTop() {
        htmlBody.animate({ scrollTop: contactForm.find('.step:visible').offset().top - headerOffset });
    }

    /**
     * Checks to see if the visible step has empty required inputs.
     */
    function stepHasEmptyInputs() {
        return contactForm.find('.step:visible').find('.required:visible :text, .required:visible textarea, .required:visible [data-type="email"], .required:visible [type="number"], .required:visible select').map(function (index, el) {
            return !$(el).val().length ? true : null;
        }).length ? true : false;
    }

    /**
     * Checks to see if the visible step has any errors.
     */
    function stepHasErrors() {
        return contactForm.find('.step:visible').find('.has-error:visible').length ? true : false;
    }

    /**
     * Check to see if the visible step has empty required checkboxes.
     */
    function stepHasEmptyCheckboxes() {
        var emptyCheckboxes = true,
            checked = contactForm.find('.required-section:visible');

        if (checked.length) {
            var checkboxes = checked.find(':checkbox:checked');

            if (checkboxes.length) {
                emptyCheckboxes = false;

                checkboxes.each(function (index, el) {
                    var section = contactForm.find('.' + convertToSnakeCase($(el).prop('id')) + '-section');

                    if (section.length) {
                        var childCheckboxes = section.find(':checkbox');

                        if (childCheckboxes.length) {
                            emptyCheckboxes = childCheckboxes.filter(':checked').length ? false : true;
                        }
                    }
                });
            }
        } else {
            // we aren't on a step that involves required section and checkboxes.
            emptyCheckboxes = false;
        }

        return emptyCheckboxes;
    }

    /**
     * Checks if an email is valid.
     */
    function isValidEmail(email) {
        var emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return emailRegex.test(email);
    }

    /**
     * Checks to see if the viswible step has empty inputs or errors.
     */
    function canStepProceed() {
        var canProceed = !stepHasEmptyInputs() &&
                         !stepHasErrors() &&
                         !stepHasEmptyCheckboxes();

        if (contactForm.find('.step:visible .captcha').length) {
            return canProceed && captchaChecked;
        }

        return canProceed;
    }

    /**
     * Toggles the next button if the user is clear to go to the next step.
     */
    function stepProceed() {
        contactForm.find('.step:visible .btn-next').prop({ disabled: !canStepProceed() });
    }

    /**
     * Checks to see if recaptcha has been checked.
     */
    window.verifyCallback = function (response) {
        captchaChecked = true;

        stepProceed();
    };
})(window, jQuery);