function equalHeight() {
    var content = $(".equalHeightRef");
    var items = content.find(".itemRef");
    var totalItems = items.length;
    items.css({ "height": "auto" });// reseteo para responsive

    var itemsByLine = Math.ceil(content.width() / items.outerWidth());

    if (itemsByLine > 1) {
        //var trows = Math.ceil(totalItems/itemsByLine);
        var subindexiterated = 1;
        var temporalindex = 0;
        var temporalheight = 0;
        var heightselect;
        items.each(function (index) {
            if (index <= (subindexiterated * itemsByLine) - 1) { // (1*3)-1=2 inicialmente ->> (2*3)-1=5 -->
                $(this).css({ "height": "auto" });
                heightselect = $(this).outerHeight();
                if (heightselect > temporalheight) {
                    temporalheight = heightselect;
                }
                if (index == (subindexiterated * itemsByLine) - 1) {//ultima iteracion
                    //aca reasingo el alto a los primeros tres
                    items.each(function (index) { // de o a 3
                        //console.log("temporalindex: "+temporalindex);
                        if (index >= temporalindex && index <= (subindexiterated * itemsByLine) - 1) {
                            $(this).css({ "height": temporalheight });
                        }
                    });
                    //for(var i= temporalindex; i <= (subindexiterated*itemsByLine)-1; i++){
                    //    $(".article-content article").eq(i).css({"height":temporalheight});
                    //}
                    temporalindex = (subindexiterated * itemsByLine);
                    subindexiterated++;
                    temporalheight = 0;
                }
            }
        });
    }
}