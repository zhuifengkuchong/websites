$(document).ready(function () {
    simpleModal();
    equalHeight();
    slideaction(1);
    // recent presentation
    $(".collapseRef").on("click", function (event) {
        if ($(this).find(".video").is(":hidden")) {
            $(this).parent().find(".video").slideUp("slow", function () { });
            $(this).find(".video").slideDown("slow", function () { });
        }
    });

    //clear input empty
    $('.clearInputRef').each(function () {
        var default_value = this.value;
        $(this).focus(function () { if (this.value == default_value) this.value = ''; });
        $(this).blur(function () { if (this.value == '') this.value = default_value; });
    });
    // focusout close input
    $('.clearInputRef').on("focusout", function () {
        $(this).parent().find(".dropDownSelect").fadeOut();
    });

    //globalHeader --> icons
    $(".globalHeader .icons").on("click", function (event) {
        event.stopPropagation();
        if ($(this).find(".dropDown").is(":hidden")) {// si esta oculto lo muestro
            $(".globalHeader .icons .ico").removeClass("selected");
            $(this).find(".ico").addClass("selected");
            $(".globalHeader .icons .dropDown").hide();
            $(this).find(".dropDown").fadeIn(300, function () {
            });
        }
        else 
        {
            if ($('.ico:hover', this).length > 0) {// si esta abierto y esta sobre el icono
                $(".globalHeader .icons .ico").removeClass("selected");
                $(".globalHeader .icons .dropDown").fadeOut(100, function () { });
            }

        }
    });
    $('html').on("click", function (event) {
        $(".globalHeader .icons .ico").removeClass("selected");
        $(".globalHeader .icons .dropDown").fadeOut(100, function () { });
    });

    $(window).resize(function () {
        createScrollUp();//scroll
        //$("#breadcrumbRowEmpty").remove();
        //$("#searchBandEmpty").remove();
        slideaction(1);
        equalHeight();
    });

});

$(window).load(function () {
    slideaction(1);
    equalHeight();
});
    //scrollUp event list
    var scrollActualPosition = 0;

    function scrollUp(point) {
        if (point != "undefined" && $("." + point + "").length > 0) {
            var element = $("." + point + "");
            var position = element.offset();
            position = position.top;

        } else {
            var position = 0;
        }
        $('html, body').animate({ scrollTop: position }, 'slow');
    }
    function checkscrollUp(element) {
        if ($(".scrollUp").length > 0) {// si existe el boton

            if ($(document).height() > $(window).height()) {//si hay scroll

                $(".scrollUp").on("click", function (event) {
                    scrollUp(element);// validar en el caso de que no exista
                });

                if (element != "undefined" && $("." + element + "").length > 0) {//checkeo si hay y existe el parametro

                    var element = $("." + element + "");

                    var positionElement = element.offset();
                    positionElement = positionElement.top;

                    if (($(document).height() - $(window).height()) > positionElement) {//si la posicion del destino permitiria scrollearse

                        $(window).on("scroll", function (event) {// listener scroll
                            scrollActualPosition = $(window).scrollTop();
                            if (scrollActualPosition > positionElement) {
                                $(".scrollUp").fadeIn();
                            } else {
                                $(".scrollUp").fadeOut();
                            }
                        });
                    }
                }
                else {// si no hay parametro va arriba
                    $(".scrollUp").fadeIn();
                    scrollUp();
                }

            }
            else {
                $(".scrollUp").hide();
            }
        }
    }//end checkscrollUp function

    function createScrollUp() {
        if ($(".scrollUpRender").length > 0) {
            var html;
            var html = "<div class='scrollUp'><a href='#' title='gotop'>scroll up</a></div>"
            $(".layoutWrapper").append(html);
            $(".scrollUp").hide();
            checkscrollUp("scrollUpRender");
        }
    }
    createScrollUp();



