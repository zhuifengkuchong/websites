function slideaction(IdFirstShow) {
    $('.carouselContentRef').each(function (indice) {
        if (!IdFirstShow) { var IdShow = 1; } else { var IdShow = IdFirstShow };
        var thisCarousel = $(this);
        var listInnerContent = $(this).find('.carouselInnerContentRef');
        var items = $(this).find(".itemRef").filter(':visible');//Only items visible
        var totalItems = items.length;

        //outerWidth("true") suma margin pero falla en algunas versiones de jquery: width() da el ancho con border y padding
        if (isNaN(parseInt(items.css('padding-right')))) { var paddingRightItems = 0; } else { var paddingRightItems = parseInt(items.css('padding-right')); }
        if (isNaN(parseInt(items.css('padding-left')))) { var paddingLeftItems = 0; } else { var paddingLeftItems = parseInt(items.css('padding-left')); }
        //if(isNaN(parseInt(items.css('border-left')))){var borderLeftItems = 0;}else{var borderLeftItems = parseInt(items.css('border-left'));}
        //if(isNaN(parseInt(items.css('border-right')))){var borderRightItems = 0;}else{var borderRightItems = parseInt(items.css('border-right'));}
        var marginItems = paddingRightItems + paddingLeftItems;

        thisCarousel.css({ "width": "" , "height": ""});//resetea
        items.css({ "width": "" });//resetea
        listInnerContent.css({ "width": "", "left": "", "height": "" });//resetea
        var anchoItems = items.outerWidth(); items.css("width", anchoItems); // pasa porcentaje del css a pixeles.

        var anchoCarousel = thisCarousel.width();
        thisCarousel.css({ "width": (anchoCarousel - marginItems) });//first
        var anchoFullListInnerContent = (totalItems * anchoItems); listInnerContent.css("width", anchoFullListInnerContent);//second


        var altoItems = listInnerContent.outerHeight();
        listInnerContent.css({ "height": altoItems });
        thisCarousel.css({ "height": altoItems });//to center de arrows

        var indent = anchoItems; //esto indica si se mueve de uno o varios items
        if (IdShow > 1) { leftposition = indent * -(IdShow - 1); } else { leftposition = 0; }
        listInnerContent.css({ "left": leftposition });
        var cantidadmostrada = Math.round(anchoCarousel / anchoItems);
        thisCarousel.parent().find(".nextRef").css({ "display": "none" })//resetea es para luego mostrar/ocultar las flechas si  no tiene suficientes elementos, ver si guardo posicion al resize
        thisCarousel.parent().find(".prevRef").css({ "display": "none" })//resetea

        if (totalItems > cantidadmostrada) {
            thisCarousel.parent().find(".nextRef").unbind('click'); //reseat
            thisCarousel.parent().find(".prevRef").unbind('click');//reseat

            /*for (i = 0; i < cantidadmostrada; i++) {
				$(listInnerContent).css('width',(totalItems+cantidadmostrada)*indent + indent);
				$(listInnerContent).append($(items[i]).clone().addClass('cloneelement'));
			};*/
            thisCarousel.parent().find(".nextRef").click(function (e) {
                e.preventDefault(); nextslide();
            });
            thisCarousel.parent().find(".prevRef").click(function (e) {
                e.preventDefault(); backslide();
            });

            //var speed=500;
            var isAnimating = false;

            var html = "<ul class='paginatorslide'>";
            for (i = 0; i < totalItems; i++) {
                // test html+="<li style='width:"+ anchoItems +"px'><p data-id='"+ (i+1) +"'>" + (i+1);
                html += "<li><p data-id='" + (i + 1) + "'>" + (i + 1);
                html += "</p></li>";
            }
            html += "</ul>"
            //test thisCarousel.find(".carouselInnerContentRef").append(html);
            thisCarousel.find(".paginatorslide").remove();
            thisCarousel.append(html);

            thisCarousel.parent().find(".paginatorslide li p").bind("click", function (e) {
                changeslide($(this).attr("data-id"));
            });

            resetarrow();

        }	//fin if

        //RESET ARROW
        function resetarrow() {
            thisCarousel.find('.paginatorslide li').addClass('paginatorslide-inactivo');
            thisCarousel.find('.paginatorslide li').eq(IdShow - 1).removeClass('paginatorslide-inactivo');
            if (IdShow >= ((totalItems - cantidadmostrada) + 1)) { thisCarousel.parent().find(".nextRef").css({ "display": "none" }); }
            else {thisCarousel.parent().find(".nextRef").css({ "display": "block" }); }
            if (IdShow === 1) { thisCarousel.parent().find(".prevRef").css({ "display": "none" }); }
            else { thisCarousel.parent().find(".prevRef").css({ "display": "block" }); }
        }
        //NEXT ARROW FUNCTION
        function nextslide() {
            if (!isAnimating) {
                if (IdShow < totalItems) {
                    IdShow++;
                    resetarrow();
                    $(listInnerContent).animate({
                        left: "-=" + indent,
                        y: 0,
                        queue: true
                    }, 600, "swing", function () { isAnimating = false; });
                    isAnimating = true;
                }

            }
        }
        function backslide() {
            if (!isAnimating) {
                if (IdShow > 1) {
                    IdShow--;
                    resetarrow();
                    $(listInnerContent).animate({
                        left: "+=" + indent,
                        y: 0,
                        queue: true
                    }, 600, "swing", function () { isAnimating = false; });
                    isAnimating = true;
                }
            }
        }
        function changeslide(number) {
            if (!isAnimating) {
                if (number != IdShow) {
                    IdShow = parseInt(number);
                    resetarrow();
                    $(listInnerContent).animate({
                        left: (indent * -(number - 1)),
                        y: 0,
                        queue: true
                    }, 500, "swing", function () { isAnimating = false; });
                    isAnimating = true;
                }
            }
        }

    }); //fin each
}; //fin function


