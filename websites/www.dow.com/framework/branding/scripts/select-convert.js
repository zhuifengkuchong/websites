$(document).ready(function () {
    pluginSelectConvert();
    function pluginSelectConvert() {
        $('select').not('.notjs').each(function (indice) {
            var currentselect = $(this);
            var placeholder;
            var itemplaceholder;
            //PLACEHOLDER
            getPlaceholder();
            function getPlaceholder() {
                // si existe selected en el option
                if (currentselect.find("option").filter('[selected]').length === 1) {
                    placeholder = currentselect.find("option:selected").text();//aca si uso option:selected por que se que existe un item selected y no hay manera que sea el primero si tiene un selected
                    itemplaceholder = currentselect.find("option:selected").index();
                    //console.log("case 1. placeholder: "+ placeholder + "; itemplaceholder: " + itemplaceholder);
                }
                    // si no existe select controlo que exista un value vacio, si hay mas de uno paso al otro if
                else if (currentselect.find("option").filter('[value=""]').length === 1) {
                    placeholder = currentselect.find("option").filter('[value=""]').text();
                    itemplaceholder = currentselect.find("option").filter('[value=""]').index();
                    //console.log("case 2. placeholder: "+placeholder + "; itemplaceholder: " + itemplaceholder);
                }
                else {//si no hay selected ni value vacio o hay mas de un value vacio
                    placeholder = currentselect.find("option").eq(0).text();
                    itemplaceholder = 0;
                    //console.log("case 3. placeholder: "+placeholder + "; itemplaceholder: " + itemplaceholder);
                }
            }
            // END PLACEHOLDER

            // CONSTRUCTOR SELECT HTML
            currentselect.hide();

            convertCustomSelect();
            function convertCustomSelect() {
                if (currentselect.next().attr("class") === "selectConvert") {
                    currentselect.next().remove();
                }
                var html = '<div class="selectConvert"><div class="inputUL"><p>' + htmlEscape(placeholder) + '</p></div>';//podria obtenerlo por el indice (itemplaceholder)
                html += "<ul>";

                currentselect.find('option').each(function (indice) {//genero los li
                    //if ($(this).val() != "") { //oculto el item del placeholder si el value es vacio, revisar por que podria ser todos vacios los value, no deberia ser obligatorio
                       html += '<li data-id=' + indice + '>' + htmlEscape($(this).text()) + '</li>';
                   // }
                });
                html += "</ul></div>";
                currentselect.after(html);
            }

            // END CONSTRUCTOR SELECT HTML

            // oculto el item seleccionado, muestro todos los demas y muestro el ul
            currentselect.next().find(".inputUL").on("click", function () {
                $(this).parent().attr("tabindex", -1).focus();//seteo el focus al div contenedor
                currentselect.next().find("li").show();
                var itemactivo = currentselect.find("option:selected").index();
                currentselect.next().find("li[data-id=" + itemactivo + "]").hide();
                currentselect.next().find("ul").toggle();
            });

            //actualizo el select y oculto el ul
            currentselect.next().find("li").on("click", function () {
                var indice = $(this).attr("data-id");
                var data = $(this).text();
                currentselect.find("option").removeAttr("selected");
                currentselect.find("option").eq(indice).attr("selected", "selected");
                currentselect.next().find("p").text(data);
                currentselect.next().find("ul").hide();
                currentselect.find("option").eq(indice).change();//pasa el evento para que lo detecte el on change del select
            });

            // focusout close
            currentselect.next().on("focusout", function () {
                currentselect.next().find("ul").hide();
            });

            //detecto el focus original por el teclado
            /*currentselect.on("focusin", function () {
                currentselect.blur();
                $(document).keydown(function (e) {
                    //currentselect.next().find("ul").show();
                    //currentselect.next().focus();
                    if (e.keyCode === 40) {
                        e.preventDefault();
                        console.log("a")

                    //    currentselect.next().find("ul li").eq(0).hover();
                    }
                    if (e.keyCode === 9) {
                        currentselect.next().focus();
                    }
                });

            });*/

            
            //detecto si externamente cambio el select, si es asi vuelvo a armarlo
            currentselect.on("change", function (event) {
                pluginSelectConvert();
            });
        });
    }
    function htmlEscape(str) {//para evitar el problema cuando hacen < select a option > 
        return String(str)
                .replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');
    }

});
