<script id = "race1a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race1={};
	myVars.races.race1.varName="[0x7f37131e61c0].load";
	myVars.races.race1.varType="@varType@";
	myVars.races.race1.repairType = "@RepairType";
	myVars.races.race1.event1={};
	myVars.races.race1.event2={};
	myVars.races.race1.event1.id = "_script_i.js";
	myVars.races.race1.event1.type = "onclick";
	myVars.races.race1.event1.loc = "_script_i.js_LOC";
	myVars.races.race1.event1.isRead = "False";
	myVars.races.race1.event1.eventType = "@event1EventType@";
	myVars.races.race1.event2.id = "_script_insight.min.js";
	myVars.races.race1.event2.type = "onclick";
	myVars.races.race1.event2.loc = "_script_insight.min.js_LOC";
	myVars.races.race1.event2.isRead = "False";
	myVars.races.race1.event2.eventType = "@event2EventType@";
	myVars.races.race1.event1.executed= false;// true to disable, false to enable
	myVars.races.race1.event2.executed= false;// true to disable, false to enable
</script>

