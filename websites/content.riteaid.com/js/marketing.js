/*! jquery.cookie v1.4.1 | MIT */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?a(require("jquery")):a(jQuery)}(function(a){function b(a){return h.raw?a:encodeURIComponent(a)}function c(a){return h.raw?a:decodeURIComponent(a)}function d(a){return b(h.json?JSON.stringify(a):String(a))}function e(a){0===a.indexOf('"')&&(a=a.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return a=decodeURIComponent(a.replace(g," ")),h.json?JSON.parse(a):a}catch(b){}}function f(b,c){var d=h.raw?b:e(b);return a.isFunction(c)?c(d):d}var g=/\+/g,h=a.cookie=function(e,g,i){if(void 0!==g&&!a.isFunction(g)){if(i=a.extend({},h.defaults,i),"number"==typeof i.expires){var j=i.expires,k=i.expires=new Date;k.setTime(+k+864e5*j)}return document.cookie=[b(e),"=",d(g),i.expires?"; expires="+i.expires.toUTCString():"",i.path?"; path="+i.path:"",i.domain?"; domain="+i.domain:"",i.secure?"; secure":""].join("")}for(var l=e?void 0:{},m=document.cookie?document.cookie.split("; "):[],n=0,o=m.length;o>n;n++){var p=m[n].split("="),q=c(p.shift()),r=p.join("=");if(e&&e===q){l=f(r,g);break}e||void 0===(r=f(r))||(l[q]=r)}return l};h.defaults={},a.removeCookie=function(b,c){return void 0===a.cookie(b)?!1:(a.cookie(b,"",a.extend({},c,{expires:-1})),!a.cookie(b))}});



function createBanner(){
var headline ='<h2>Introducing <span class="wp-intro-logo">wellness+ Plenti</span></h2>',
message = '<h3>One card. Two kinds of Points.</h3>',
btn = '<div class="alert-btn red-textbt-big"><a href="https://content.riteaid.com/wellness">Learn More</a></div>',
close = '<a class="close"> </a>';


$('#common-advertisement-area').before('<div class="alert plenti-intro-banner">'+close+'<div class="info">'+headline+message+'</div>'+btn+'</div>');

}



// Generic Sale Banner - Create banner on DOM ready, add class and specify link
function createSaleBanner(){
$('#common-advertisement-area').before('<div class="alert sunny-sale"><a class="close"></a><a href="https://www.riteaid.com/external-redirect?program=ecommerce&returnTo=online-deals" class="shop-now-novis"></a></div>');

}






$(document).ready(function(){
var x = location.search;
if (x=="?referrer=vv"){
var heading ='<p>Thank you for your interest in Video Values. As of 4/26, the program has ended. But there are still many more ways to save in-store and at riteaid.com with <strong>wellness+ with Plenti.</strong></p>';
var h2 = '<h2>With <strong>wellness+ with Plenti</strong> you get all these great benefits:</h2>';
var list = '<li>Load and redeem valuable coupons for instant savings when you checkout with <strong>Load2Card</strong></li><li>Earn up to 20% off almost the entire store for a year<sup>1</sup>.</li><li>Earn Plenti points on things you buy every day at Plenti partners, including Rite Aid</li><li>Use Plenti points for savings at Rite Aid and certain Plenti partners<sup>2</sup></li>';
var btn = '<div class="red-textbt"><a href="https://content.riteaid.com/wellness">Learn More</a></div>';
var disclaimer = '<small><sup>1.</sup> Certain restrictions apply. See complete <strong>wellness+ with Plenti</strong> program terms & conditions.<sup>2.</sup> Bronze, silver, and gold discounts will remain active through the end of the next calendar year. Members must present their wellness+ card, Plenti card or alternate ID at checkout to receive any discount. When used with a sale priced item the customer will receive the lower of the discount price or sale price. </small>';
$('#common-advertisement-area').after('<div class="vid-val-temp"><div class="vid-values-discontinued"><h2 class="vv-wp-logo"></h2>'+heading+h2+'<ul>'+list+'</ul>'+btn+disclaimer+'</div><div class="temp-overlay"></div></div>');

setTimeout(function(){
$('.vid-val-temp').fadeOut().remove();
},30000);

}



// Set Cookies for Banner
if($.cookie("noti") !== "closed"){
setTimeout(function(){
createSaleBanner();
},1500);
}

$('a.close').live('click',function(){
$.cookie("noti","closed",{expires:''});
$('.alert').addClass('animated fadeOutUp');
$('.alert').fadeOut('500');

});



/** Left Nav Link Fixes **/
$('ul.leftnav-level1 li a[href="https://content.riteaid.com/rediclinic"]').attr('href','/external-redirect?program=ecommerce&returnTo=info/pharmacy/pharmacy-services/rediclinic');
$('ul.leftnav-level2 li a[href="https://content.riteaid.com/rediclinic"]').attr('href','/external-redirect?program=ecommerce&returnTo=info/pharmacy/pharmacy-services/rediclinic');
$('ul.leftnav-level1 li a[href="https://content.riteaid.com/healthspot"]').attr('href','/external-redirect?program=ecommerce&returnTo=info/pharmacy/pharmacy-services/healthspot');
$('ul.leftnav-level2 li a[href="https://content.riteaid.com/healthspot"]').attr('href','/external-redirect?program=ecommerce&returnTo=info/pharmacy/pharmacy-services/healthspot');

$('ul.leftnav-level1 li a[href="https://content.riteaid.com/wellness/plenti"]').attr('href','/external-redirect?program=ecommerce&returnTo=info/plenti');
$('ul.leftnav-level2 li a[href="https://content.riteaid.com/wellness/plenti"]').attr('href','/external-redirect?program=ecommerce&returnTo=info/plenti');

// Fix Kidcents Link
$('.wcontent-9.d-boxes a').attr('target','_blank');


// Temp Slider Fixes


// Deals Slider Stuff
$('#first').append('<a href="https://content.riteaid.com/external-redirect?program=ecommerce&returnTo=medicine-health/home-health-care" style="background-image:url(https://s3.amazonaws.com/shopdot/Home+Page+Slider/Shop_Slider05_690x388_150518.png);" alt="$20 OFF & Free shipping. HomeHealth Care Sale, with your purchase of $100 or more of home health care products*. Use promo code: HOMEHEALTH. Shop home health care."></a>');
$('#second').append('<a href="https://content.riteaid.com/external-redirect?program=ecommerce&returnTo=personal-care/skin-care/sunscreen-tanning/bogo-free--bogo-50-off" style="background-image:url(https://s3.amazonaws.com/shopdot/Home+Page+Slider/store-slider-sunscreen690x388.jpg);" alt="Sunscreen & Tanning Super Sale! Buy one, get one FREE. Buy one, get one 50% OFF. with card."></a>');
$('#fourth').append('<li id="fourth"><a href="https://content.riteaid.com/external-redirect?program=ecommerce&returnTo=vitamins-supplements/rite-aid--bogo-free" style="background-image:url(https://s3.amazonaws.com/shopdot/Home+Page+Slider/vitamin-store-slider0524-690x388.jpg);" alt="Rite Aid Brand Vitamins and Supplements Super Sale. Buy on, get one Free. with card. Shop Vitamin BOGOs."></a></li>');
$('#fifth').append('<a href="https://content.riteaid.com/external-redirect?program=ecommerce&returnTo=beauty/online-deals" style="background-image:url(https://s3.amazonaws.com/shopdot/Home+Page+Slider/shop-slider-beautt690x388.jpg);" ></a>');


// Fix that Button
if ( $.browser.webkit ) {
$('#pharmacytextbox   .jqTransformInputWrapper').attr('style', 'width: 266px !important');
}




// Update Menu

// Temp Fixes
$('#sixth-module div').attr('style','background-image:url(https://s3-us-west-2.amazonaws.com/radot/Home/Home+Page+Slider/ADT_Slider_background.jpg);');


});