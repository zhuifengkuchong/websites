var irxmlstockquote = new Array();
irxmlstockquote[0]={
  "ask":105.170,
  "bid":105.150,
  "change":-0.790,
  "companyname":"The Clorox Company",
  "datetime":new Date(2015, 6-1, 8, 13, 0, 25),
  "dividend":0.770,
  "eps":4.520,
  "exchange":"NYSE",
  "high":106.340,
  "lastdatetime":new Date(2015, 6-1, 8, 13, 0, 25),
  "lastprice":105.180,
  "longname":"The Clorox Company",
  "low":105.040,
  "open":106.000,
  "pchange":-0.745,
  "pe":24.000,
  "previousclose":105.970,
  "shares":131178,
  "shortname":"The Clorox Company",
  "ticker":"CLX",
  "trades":3460,
  "volume":396100,
  "yearhigh":112.700,
  "yearlow":86.030,
  "yield":2.907
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";