var irxmlstockquote = new Array();
irxmlstockquote[0]={
  "ask":38.290,
  "bid":38.270,
  "change":-0.570,
  "companyname":"Joy Global Inc.",
  "datetime":new Date(2015, 6-1, 8, 13, 4, 38),
  "dividend":0.200,
  "eps":2.740,
  "exchange":"NYSE",
  "high":38.820,
  "lastdatetime":new Date(2015, 6-1, 8, 13, 4, 38),
  "lastprice":38.280,
  "longname":"Joy Global Inc.",
  "low":38.190,
  "open":38.720,
  "pchange":-1.467,
  "pe":0,
  "previousclose":38.850,
  "shares":97375,
  "shortname":"Joy Global",
  "ticker":"JOY",
  "trades":5429,
  "volume":660100,
  "yearhigh":65.360,
  "yearlow":37.770,
  "yield":2.059
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";