var irxmlstockquote = new Array();
irxmlstockquote[0]={
  "ask":103.280,
  "bid":103.240,
  "change":-0.605,
  "companyname":"Sempra Energy",
  "datetime":new Date(2015, 6-1, 8, 12, 6, 58),
  "dividend":0.700,
  "eps":5.480,
  "exchange":"NYSE",
  "high":104.310,
  "lastdatetime":new Date(2015, 6-1, 8, 12, 6, 58),
  "lastprice":103.265,
  "longname":"Sempra Energy",
  "low":103.080,
  "open":104.310,
  "pchange":-0.582,
  "pe":19.000,
  "previousclose":103.870,
  "shares":247580,
  "shortname":"Sempra Energy",
  "ticker":"SRE",
  "trades":4896,
  "volume":686400,
  "yearhigh":116.300,
  "yearlow":96.130,
  "yield":2.696
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";