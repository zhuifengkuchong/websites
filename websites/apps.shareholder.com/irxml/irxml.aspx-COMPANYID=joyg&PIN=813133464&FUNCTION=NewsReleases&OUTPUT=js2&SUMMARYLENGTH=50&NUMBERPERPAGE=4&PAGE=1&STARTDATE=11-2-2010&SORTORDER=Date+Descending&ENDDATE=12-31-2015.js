var irxmlnewsreleases = new Array();
irxmlnewsreleases[0]={
  "attachmentfileid":834111,
  "attachmentfilekey":"d3a8c36d-ca5e-4432-942e-1e036053ac58",
  "attachmentfilename":"http://apps.shareholder.com/irxml/JOY_News_2015_6_5_General.pdf",
  "attachmentfilesize":7909,
  "id":1,
  "releasedate":new Date(2015, 6-1, 5, 09, 49, 0),
  "releaseid":916704,
  "releasetype":"General",
  "subtitle":"",
  "summary":"MILWAUKEE--(BUSINESS WIRE)--\n\n\n\n      Joy Global I",
  "title":"Joy Global Inc. to Present at the William Blair Growth Stock Conference",
  "totalrecords":99,
  "year":"2015"
};
irxmlnewsreleases[1]={
  "attachmentfileid":833773,
  "attachmentfilekey":"edec3031-6fdd-47d0-91f0-dfaef4a5f2dc",
  "attachmentfilename":"http://apps.shareholder.com/irxml/JOY_News_2015_6_4_Financial.pdf",
  "attachmentfilesize":58558,
  "id":2,
  "releasedate":new Date(2015, 6-1, 4, 06, 0, 0),
  "releaseid":916432,
  "releasetype":"Financial",
  "subtitle":"",
  "summary":"MILWAUKEE--(BUSINESS WIRE)--\n\n\n\n      Joy Global I",
  "title":"Joy Global Announces Second Quarter Fiscal 2015 Operating Results and Hard Rock Mining Acquisition",
  "totalrecords":99,
  "year":"2015"
};
irxmlnewsreleases[2]={
  "attachmentfileid":831401,
  "attachmentfilekey":"db464c83-d70c-4358-b578-366a84df65dd",
  "attachmentfilename":"http://apps.shareholder.com/irxml/JOY_News_2015_5_21_General.pdf",
  "attachmentfilesize":6938,
  "id":3,
  "releasedate":new Date(2015, 5-1, 21, 16, 0, 0),
  "releaseid":914487,
  "releasetype":"General",
  "subtitle":"",
  "summary":"MILWAUKEE--(BUSINESS WIRE)--\n      Joy Global Inc.",
  "title":"Joy Global Inc. Declares Quarterly Dividend of $0.20 Per Share",
  "totalrecords":99,
  "year":"2015"
};
irxmlnewsreleases[3]={
  "attachmentfileid":831211,
  "attachmentfilekey":"d9846b1d-3455-45c5-a8f5-cd8fa905f89e",
  "attachmentfilename":"http://apps.shareholder.com/irxml/JOY_News_2015_5_21_General.pdf",
  "attachmentfilesize":8270,
  "id":4,
  "releasedate":new Date(2015, 5-1, 21, 09, 12, 0),
  "releaseid":914423,
  "releasetype":"General",
  "subtitle":"",
  "summary":"MILWAUKEE--(BUSINESS WIRE)--\n\n\n\n      Joy Global I",
  "title":"Joy Global Inc. Announces Fiscal Second Quarter 2015 Earnings Release and Conference Call Information",
  "totalrecords":99,
  "year":"2015"
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";