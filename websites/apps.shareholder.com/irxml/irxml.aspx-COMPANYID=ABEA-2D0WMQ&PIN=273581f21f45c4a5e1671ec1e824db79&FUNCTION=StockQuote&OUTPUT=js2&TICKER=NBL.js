var irxmlstockquote = new Array();
irxmlstockquote[0]={
  "ask":44.300,
  "bid":44.290,
  "change":-0.300,
  "companyname":"Noble Energy Incorporated",
  "datetime":new Date(2015, 6-1, 8, 13, 2, 59),
  "dividend":0.180,
  "eps":2.740,
  "exchange":"NYSE",
  "high":44.640,
  "lastdatetime":new Date(2015, 6-1, 8, 13, 2, 59),
  "lastprice":44.290,
  "longname":"Noble Energy Inc.",
  "low":44.110,
  "open":44.340,
  "pchange":-0.673,
  "pe":18.000,
  "previousclose":44.590,
  "shares":387005,
  "shortname":"Noble Energy",
  "ticker":"NBL",
  "trades":16050,
  "volume":2513900,
  "yearhigh":79.630,
  "yearlow":41.010,
  "yield":1.615
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";