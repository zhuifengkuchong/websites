var irxmlstockquote = new Array();
irxmlstockquote[0]={
  "ask":23.920,
  "bid":23.910,
  "change":-0.310,
  "companyname":"Jabil Circuit, Inc.",
  "datetime":new Date(2015, 6-1, 8, 11, 58, 36),
  "dividend":0.080,
  "eps":0.310,
  "exchange":"NYSE",
  "high":24.340,
  "lastdatetime":new Date(2015, 6-1, 8, 11, 58, 36),
  "lastprice":23.920,
  "longname":"Jabil Circuit, Inc.",
  "low":23.890,
  "open":24.270,
  "pchange":-1.279,
  "pe":0,
  "previousclose":24.230,
  "shares":193762,
  "shortname":"Jabil",
  "ticker":"JBL",
  "trades":3307,
  "volume":439300,
  "yearhigh":24.950,
  "yearlow":18.030,
  "yield":1.321
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";