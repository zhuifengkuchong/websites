var isMobile = navigator.userAgent.toLowerCase().match(/(iphone|ipod|ipad|android|blackberry|opera mini|iemobile|kindle|silk|mobile)/);

var $j = typeof jQuery === 'undefined' ? null : jQuery.noConflict();

$j(function () {

    $j('body').addClass('jump');

    if (isMobile) {
        $j('html').addClass('mobile');
    } else {
        $j('html').addClass('no-mobile');
    }
    
});