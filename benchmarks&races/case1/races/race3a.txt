<script id = "race3a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race3={};
	myVars.races.race3.varName="Window[26].imagesList";
	myVars.races.race3.varType="@varType@";
	myVars.races.race3.repairType = "@RepairType";
	myVars.races.race3.event1={};
	myVars.races.race3.event2={};
	myVars.races.race3.event1.id = "image2";
	myVars.races.race3.event1.type = "onload";
	myVars.races.race3.event1.loc = "image2_LOC";
	myVars.races.race3.event1.isRead = "True";
	myVars.races.race3.event1.eventType = "@event1EventType@";
	myVars.races.race3.event2.id = "image3";
	myVars.races.race3.event2.type = "onload";
	myVars.races.race3.event2.loc = "image3_LOC";
	myVars.races.race3.event2.isRead = "False";
	myVars.races.race3.event2.eventType = "@event2EventType@";
	myVars.races.race3.event1.executed= false;// true to disable, false to enable
	myVars.races.race3.event2.executed= false;// true to disable, false to enable
</script>

