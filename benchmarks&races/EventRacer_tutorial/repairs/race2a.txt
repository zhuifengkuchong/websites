<script id = "race1a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race1={};
	myVars.races.race1.varName="init";
	myVars.races.race1.varType="fn";//fn,var,element
	myVars.races.race1.repairType = "2A"
	myVars.races.race1.event1={};
	myVars.races.race1.event2={};
	myVars.races.race1.event1.id = "Lu_Id_script_1";
	myVars.races.race1.event1.type = "Lu_Id_script_1__parsed";
	myVars.races.race1.event1.loc = "Lu_Id_script_1_LOC";
	myVars.races.race1.event1.eventType = "parsing_static";	
	myVars.races.race1.event2.id = "Lu_Id_img_1";
	myVars.races.race1.event2.type = "onload";
	myVars.races.race1.event2.loc = "Lu_Id_img_1_LOC";
	myVars.races.race1.event2.eventType = "eh_static";
	myVars.races.race1.event1.executed= false;// true to disable, false to enable
	myVars.races.race1.event2.executed= false;// true to disable, false to enable
</script>

