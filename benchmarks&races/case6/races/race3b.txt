<script id = "race3b" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race3={};
	myVars.races.race3.varName="Window[26].button2Clicked";
	myVars.races.race3.varType="@varType@";
	myVars.races.race3.repairType = "@RepairType";
	myVars.races.race3.event1={};
	myVars.races.race3.event2={};
	myVars.races.race3.event1.id = "Lu_Id_button_2";
	myVars.races.race3.event1.type = "onclick";
	myVars.races.race3.event1.loc = "Lu_Id_button_2_LOC";
	myVars.races.race3.event1.isRead = "True";
	myVars.races.race3.event1.eventType = "@event1EventType@";
	myVars.races.race3.event2.id = "Lu_Id_script_2";
	myVars.races.race3.event2.type = "Lu_Id_script_2__parsed";
	myVars.races.race3.event2.loc = "Lu_Id_script_2_LOC";
	myVars.races.race3.event2.isRead = "False";
	myVars.races.race3.event2.eventType = "@event2EventType@";
	myVars.races.race3.event1.executed= false;// true to disable, false to enable
	myVars.races.race3.event2.executed= false;// true to disable, false to enable
</script>

