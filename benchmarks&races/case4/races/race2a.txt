<script id = "race2a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race2={};
	myVars.races.race2.varName="Tree[0x7faf67c72458]:outputButton1";
	myVars.races.race2.varType="@varType@";
	myVars.races.race2.repairType = "@RepairType";
	myVars.races.race2.event1={};
	myVars.races.race2.event2={};
	myVars.races.race2.event1.id = "outputButton1";
	myVars.races.race2.event1.type = "outputButton1__parsed";
	myVars.races.race2.event1.loc = "outputButton1_LOC";
	myVars.races.race2.event1.isRead = "False";
	myVars.races.race2.event1.eventType = "@event1EventType@";
	myVars.races.race2.event2.id = "Lu_Id_button_1";
	myVars.races.race2.event2.type = "onclick";
	myVars.races.race2.event2.loc = "Lu_Id_button_1_LOC";
	myVars.races.race2.event2.isRead = "True";
	myVars.races.race2.event2.eventType = "@event2EventType@";
	myVars.races.race2.event1.executed= false;// true to disable, false to enable
	myVars.races.race2.event2.executed= false;// true to disable, false to enable
</script>

