<script id = "race1a" class = "Lu_Inst">
	if(!myVars) {var myVars = {};}
	if(!myVars.races) {myVars.races= {};}
	myVars.races.race1={};
	myVars.races.race1.varName="Window[26].fn";
	myVars.races.race1.varType="@varType@";
	myVars.races.race1.repairType = "@RepairType";
	myVars.races.race1.event1={};
	myVars.races.race1.event2={};
	myVars.races.race1.event1.id = "Lu_Id_script_1";
	myVars.races.race1.event1.type = "Lu_Id_script_1__parsed";
	myVars.races.race1.event1.loc = "Lu_Id_script_1_LOC";
	myVars.races.race1.event1.isRead = "False";
	myVars.races.race1.event1.eventType = "@event1EventType@";
	myVars.races.race1.event2.id = "button1";
	myVars.races.race1.event2.type = "onclick";
	myVars.races.race1.event2.loc = "button1_LOC";
	myVars.races.race1.event2.isRead = "True";
	myVars.races.race1.event2.eventType = "@event2EventType@";
	myVars.races.race1.event1.executed= false;// true to disable, false to enable
	myVars.races.race1.event2.executed= false;// true to disable, false to enable
</script>

